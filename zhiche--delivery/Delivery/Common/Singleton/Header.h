//
//  Header.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#ifndef Header_h
#define Header_h

#define screenWidth [UIScreen mainScreen].bounds.size.width
#define screenHeight [UIScreen mainScreen].bounds.size.height
#define Main_Width [UIScreen mainScreen].bounds.size.width
#define Main_height [UIScreen mainScreen].bounds.size.height
#define Font(a)     [UIFont systemFontOfSize:a * kWidth];
#pragma mark 宽高比cc

#define kWidth  screenWidth/320.0
#define kHeight screenHeight/568.0


//颜色
#define Color_RGB(a,b,c,d) [UIColor colorWithRed:(a)/255.0 green:(b)/255.0 blue:(c)/255.0 alpha:(d)]

#define WhiteColor [UIColor whiteColor]
#define BlackColor [UIColor blackColor]

//#define LineGrayColor [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1]

#define YellowColor  Color_RGB(253,131,42,1)//主题黄
#define sectionHeadView  Color_RGB(226,226,226,1)

#define HomeBackColor   Color_RGB(247,247,247,1)

#define RedColor  Color_RGB(254,81,62,1)//状态红
#define GrayColor  Color_RGB(236,236,236,1)//headerView 灰
#define LineGrayColor  Color_RGB(210,210,210,1)//分割线 灰
#define fontGrayColor  Color_RGB(157,157,157,1)// 字体浅灰
#define littleBlackColor  Color_RGB(79,79,79,1)// 字体浅黑
#define carScrollColor   Color_RGB(119, 119, 119, 1)//宝马－2辆
#define rearrangeBtnColor   Color_RGB(254, 242, 227, 1)//重新发布
#define BlackTitleColor Color_RGB(14, 14, 14, 1)//发布信息 黑色体 地址、时间、车型
#define BtnBorderColor  Color_RGB(253,217,185,1)// 按钮外环
//80, 80, 80
#define BtnTitleColor  Color_RGB(80,80,80,1)// 下单发运时间按钮
#define AddressSCtitleColor Color_RGB(109, 109, 109,1)//地址删除按钮
#define FieldPlacerColor  Color_RGB(181, 181,188,1)//下单发运 汽车数量背景字体
#define AddCarColor  Color_RGB(34,34,34,1)//增加车辆
#define AddCarNameBtnColor  Color_RGB(199,199,199,1)//车名称

#define headerBottomColor  Color_RGB(252,252,252,1) //运单上下颜色
#define BillColor  Color_RGB(222,222,222,1) //我的账单

//#define GrayColor Color_RGB(149,149,149,1)
#define RGBACOLOR(a,b,c,d) Color_RGB(a,b,c,d)
//
#define headerTitleColor Color_RGB(255, 79, 61,1)//查看报价上面的分钟
#define headerBackViewColor Color_RGB(252, 252, 252,1)//查看报价上面背景颜色
//查询运费 温馨提示
#define shippingTitleColor Color_RGB(143, 143, 143,1)

//评论
#define CommentTitleColor Color_RGB(187, 187, 187,1)

//字体大小
#define FontOfSize9  9.0
#define FontOfSize10  10.0
#define FontOfSize11  11.0
#define FontOfSize12  12.0
#define FontOfSize13  13.0
#define FontOfSize14  14.0
#define FontOfSize15  15.0
#define FontOfSize17  17.0
#define FontOfSize19  19.0



//测试

//#define Main_interface @"http://www.huiyunche.cn/kyleuat/"
//#define Main_interface @"http://192.168.199.224:9090/"
//#define Main_interface @"http://220.249.50.163/kyleuat/"
//#define Main_interface @"http://220.249.50.163/kylebussuat/"


//开发的
//#define Main_interface @"http://220.249.50.163/kyle/"
//#define Main_interface @"http://220.249.50.163/kylebuss/"

//正式
//#define Main_interface @"http://www.huiyunche.cn/kyle/"

//正式环境

#if CeShi
// 测试版需要执行的代码
//#define Main_interface @"http://119.57.140.26/kylebussuat/"
#define Main_interface @"http://192.168.199.135:8080/kyle-buss/"


#elif KaiFa
//#define Main_interface @"http://192.168.199.199:8080/kyle-buss/"

//#define Main_interface @"http://119.57.140.26/kylebuss/"
#define Main_interface @"http://192.168.199.135:8080/kyle-buss/"

//#define Main_interface @"http://192.168.199.199:8080/kyle-buss/"
//#define Main_interface @"http://www.huiyunche.cn/kyle/"

#else

#define Main_interface @"https://kylebuss.huiyunche.cn/"
//#define Main_interface @"http://119.57.140.26/kylebuss/"
//#define Main_interface @"http://192.168.199.135:8080/"

//#define Main_interface @"http://119.57.140.26/kylebuss/"



#endif

#define application_json   @"application/json"

#define STR_F2(p1, p2)     [NSString stringWithFormat:@"%@%@", p1, p2]
#define STR_F3(p1, p2,p3)     [NSString stringWithFormat:@"%@%@%@", p1, p2,p3]
#define STR_F4(p1, p2, p3,p4) [NSString stringWithFormat:@"%@%@%@%@", p1, p2, p3,p4]




#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
/*
 web
*/
//测试更新
#define versionURL    STR_F2(Main_interface,@"version/ios/latest")



//客服电话
#define iphoneNumber   [NSString stringWithFormat:@"telprompt://400-6706082"]
//运输说明
#define transport_explain_webURL  STR_F2(Main_interface,@"templates/agreement/veneer-transport-explain.html")
//软件使用协议
#define software_agreement_webURL  STR_F2(Main_interface,@"templates/agreement/veneer-software-agreement.html")
//运输服务协议
#define transport_agreement_webURL  STR_F2(Main_interface,@"templates/agreement/veneer-transport-agreement.html")
//联系我们
#define contact_us_webURL  STR_F2(Main_interface,@"templates/agreement/contact-us.html")
//版本介绍
#define versions_introduce_webURL  STR_F2(Main_interface,@"templates/agreement/versions-introduce.html")




//查询在线司机
#define onlinedriver_Url STR_F2(Main_interface,@"complex/onlineinfo")
//轮播图
#define banner_list_Url STR_F2(Main_interface,@"banner/list")

//我的账单kyle/pay/list
#define mypay_Url STR_F2(Main_interface,@"pay/list")

//同城url
//trailer/list
#define trailer_list STR_F2(Main_interface,@"trailer/list")
//查询费用
#define fees_list STR_F2(Main_interface,@"fees/query")
//城市list
#define intracity_list STR_F2(Main_interface,@"intracity/list")
//区
#define area_country_list STR_F2(Main_interface,@"area/country")
//确定下单
#define cworder_save_list STR_F2(Main_interface,@"cworder/save")
//查询商户帐号
#define user_isexisted STR_F2(Main_interface,@"user/isexisted")


//评价
#define valuationStatus_Url STR_F2(Main_interface,@"order/queryByOrderId")

//保存评价
#define saveValuation_Url STR_F2(Main_interface,@"order/saveValuation")



//查询用户默认地址
#define addressFault_Url STR_F2(Main_interface,@"useraddress/default")
//用户常用地址新增
#define  addressAdd_Url   STR_F2(Main_interface,@"useraddress/create")

//用户常用地址新增 同城
#define  CD_addressAdd_Url   STR_F2(Main_interface,@"useraddress/add")
//用户常用地址编辑 同城
#define  CD_addressEdit_Url   STR_F2(Main_interface,@"useraddress/update")
//查询所有市区
#define  CD_addressListArea_Url   STR_F2(Main_interface,@"area/listArea")







//useraddress/delete/{id}
//用户常用地址删除
#define  addressDelete_Url   STR_F2(Main_interface,@"useraddress/delete/")
//用户常用地址编辑
#define  addressEdit_Url   STR_F2(Main_interface,@"useraddress/modify")
//用户常用地址设置成默认 useraddress/setdefault
#define address_Yes_Fault_Url     STR_F2(Main_interface,@"useraddress/setdefault")
//用户常用地址列表
#define  addressUser_Url   STR_F2(Main_interface,@"useraddress/list")
//查询物流产品
#define  product_Url   STR_F2(Main_interface,@"product/nlist")
// 查询运费
#define  feesShipping_Url   STR_F2(Main_interface,@"fees/shipping")
//查询地址省份
#define  provinces_Url   STR_F2(Main_interface,@"area/provinces")
//查询地址区
#define  counties_Url   STR_F2(Main_interface,@"area/counties")
//地址搜索
#define  search_Url   STR_F2(Main_interface,@"area/search")

//车型模糊查找
#define  vehicle_search_Url   STR_F2(Main_interface,@"vehicle/search")

//创建订单
#define Order_Addorder_Url STR_F2(Main_interface,@"order/create")

//发布订单
#define Order_Publish_Url STR_F2(Main_interface,@"order/publish")
//查询发布订单
#define Order_Published_Url STR_F2(Main_interface,@"order/published")

//报价列表
#define Order_Quote_Url STR_F2(Main_interface,@"quote/list/")
//预定报价（选中某个报价的司机）
#define Order_Quote_Reserve_Url STR_F2(Main_interface,@"quote/reserve")

//查询省份地址
#define Select_Provincelist_Url STR_F2(Main_interface,@"area/selectprovincelist")

//查询账单列表
#define Find_BillList_Url STR_F2(Main_interface,@"/bill/list")
//#define Find_BillList_Url STR_F2(Main_interface,@"/bill/appscores")

//查询账单列表(分类后)
#define bill_totallist_Url STR_F2(Main_interface,@"/bill/totallist")


//查询账单明细列表
#define Find_BillDetails_Url STR_F2(Main_interface,@"/bill/appscores")


#define Find_Bill_list_Url STR_F2(Main_interface,@"/bill/appdetails")



//账单结算
#define Find_settlement_Url STR_F2(Main_interface,@"/bill/settlement")

//查询支付历史
#define Select_Paylist_Url STR_F2(Main_interface,@"pay/list")

#define Order_publish STR_F2(Main_interface,@"order/publish")
//我要接车
#define Order_receipt STR_F2(Main_interface,@"order/receipt")

//支付宝支付 kyle/pay/alipay
#define Order_alipay STR_F2(Main_interface,@"pay/alipay")
//微信支付
#define Order_wx STR_F2(Main_interface,@"pay/wx")

// 登录成功返回token
#define  login_token @"login_token"

#define  application_json @"application/json"

/**
 订单

 */
//重新发布
#define Mine_Order STR_F2(Main_interface,@"order/publish")

//待支付订单详情
#define payment_info STR_F2(Main_interface,@"quote/paymentinfo")
//订单详情
#define order_detail STR_F2(Main_interface,@"order/")



//取消订单
#define Mine_Order_Cancel STR_F2(Main_interface,@"order/cancel")
/**

 支付

 */
//阿里
#define pay_byAli STR_F2(Main_interface,@"pay/alipay")
//微信
#define pay_byWx STR_F2(Main_interface,@"pay/wx")

/**
  个人信息
 */
//个人信息

#define user_info STR_F2(Main_interface,@"user")

//个人信息更改
#define user_modify STR_F2(Main_interface,@"user/modify")

//七牛token
#define qiniu_token  STR_F2(Main_interface,@"qiniu/upload/ticket")

//检测token是否过期
#define token_check  STR_F2(Main_interface,@"user/check")

/**
 用户常用车型列表
 */

#define user_vehicle_list  STR_F2(Main_interface,@"uservehicle/list")

//新建车型
#define user_vehicle_create  STR_F2(Main_interface,@"uservehicle/create")

//车型列表
#define brand_list STR_F2(Main_interface,@"brand/list")
//车系列表
#define vehicle_list STR_F2(Main_interface,@"vehicle/list")
//用户默认地址
#define userAddress_default  STR_F2(Main_interface,@"useraddress/setdefault")
//用户地址更新
#define userAddress_update STR_F2(Main_interface,@"useraddress/modify")

//开取发票
#define invoice_create  STR_F2(Main_interface,@"invoice/create")

//开票规则
#define invoice_explain STR_F2(Main_interface,@"templates/cms/invoice-explain.html")

//关于我们
#define  aboutUS_web_Url STR_F2(Main_interface,@"templates/agreement/about.html")
//用户注册 服务协议
#define templates_Url STR_F2(Main_interface,@"templates/agreement/user-protocol-view.html")

//车辆运输服务协议
#define  transport_agreement  STR_F2(Main_interface,@"templates/agreement/transport-agreement.html")
//运输说明
#define  transport_explain  STR_F2(Main_interface,@"templates/agreement/transport-explain.html")
//退款说明
#define refund_explain STR_F2(Main_interface,@"templates/cms/refund-explain.html")
//新手上路
#define  newman_start STR_F2(Main_interface,@"cms/userstart")

//登录
//#define login_codeLogin STR_F2(Main_interface,@"login/userlogin")
#define login_codeLogin STR_F2(Main_interface,@"login/namelogin")



//物流信息
#define logistics_info STR_F2(Main_interface,@"logisticsinfo/list")
//确定收车
#define receive_vehicle STR_F2(Main_interface,@"order/receivevehicle")
//确认交车
#define receive_receipt STR_F2(Main_interface,@"order/confirmReceipt")

//提车信息列表详情
#define order_exinfo STR_F2(Main_interface,@"order/exinfo")
//交车信息列表详情
#define order_deinfo STR_F2(Main_interface,@"order/deinfo")
//提车信息列表
#define order_exlist STR_F2(Main_interface,@"order/exlist")
//交车信息列表
#define order_delist STR_F2(Main_interface,@"order/delist")
//物流跟踪
//#define order_sorder STR_F2(Main_interface,@"order/sorder")
#define order_sorder STR_F2(Main_interface,@"order/trail")

//物流详情
#define order_logistiks STR_F2(Main_interface,@"serviceordertrail/trail")


//评价
#define score_create STR_F2(Main_interface,@"evaluate/create")
//查看评分
#define score_list STR_F2(Main_interface,@"evaluate/list")
//投诉
#define complain_create STR_F2(Main_interface,@"complain/create")
//积分查询
#define score_details STR_F2(Main_interface,@"scores/details")
//积分规则
#define score_rule STR_F2(Main_interface,@"templates/agreement/integral-rule.html")



/**
 整体cell 高度
 */
#define cellHeight 43 * kHeight
#define buttonheight 30 * kHeight

#define Button_Height 40 * kHeight


#endif /* Header_h */












