//
//  Common.h
//  GL
//
//  Created by 王亚陆 on 16/1/12.
//  Copyright © 2016年 知车. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//#import "GiFHUD.h"
typedef void(^DownloadFinishedBlock)(id responseObj);
typedef void(^DownloadFailedBlock)(NSString *errorMsg);
typedef NS_ENUM(NSInteger, item_position) {
    item_left = 0,
    item_right
};

@interface Common : NSObject

-(UIImageView*)createUIImage:(NSString *)title andDelay:(float)delay;


//tableview 多余的线隐藏
+(void)setExtraCellLindeHidder:(UITableView*)tableView;

//设置导航专用按钮
- (void)customNavigationItem:(UIViewController *)target position: (item_position)position title:(NSString *)title image:(UIImage *)image action:(SEL)action;
-(UIView*)customNavigationItem:(UIViewController *)target action:(SEL)action;


//缓存
- (void)writeToLocal:(NSData *)data;
- (NSData *)readLocal;

//断网重新加载
-(UIView*)noInternet:(NSString *)title and:(UIViewController *)target action:(SEL)action andCGreck:(CGRect)fram;
//text 自适应宽高
-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color;


//http get 请求
+(void)requestWithUrlString:(NSString *)urlString contentType:(NSString *)type finished:(DownloadFinishedBlock)finished failed:(DownloadFailedBlock)failedBlock;

//基本的post请求
-(void)afPostRequestWithUrlString:(NSString *)urlString parms:(NSDictionary *)dic finishedBlock:(DownloadFinishedBlock)finished failedBlock:(DownloadFailedBlock)failed;

+(void)afPostRequestWithUrlString:(NSString *)urlString parms:(NSDictionary *)dic finishedBlock:(DownloadFinishedBlock)finished failedBlock:(DownloadFailedBlock)failed;

//上传文件(图片)的方法
//dic 中放除图片以外的其他参数, data 图片转换的NSData key 为图片对应的key：pis

- (void)afUploadImageWithUrlString:(NSString *)urlString parms:(NSDictionary *)dic  imageData:(NSData *)data imageKey:(NSString *)key finishedBlock:(DownloadFinishedBlock)finished failedBlock:(DownloadFailedBlock)failed;


//创建单例方法
/**
 *  <#Description#>
 *
 *  @param phone     手机号
 *  @param account   账号
 *  @param secret    密码
 *  @param token     用户标示符
 *  @param isRemembe 是否记住密码
 */
+(void)writePhone:(NSString *)phone writeAccount:(NSString *)account writeSecret:(NSString *)secret writeToken:(NSString *)token remember:(BOOL)isRemembe;

//返回token
+(NSDictionary*)backToken;

//插件大小适配
+(CGRect)myRectWithX:(CGFloat)x Y:(CGFloat)y width:(CGFloat)width height:(CGFloat)height;

//将NSDictionary或者NSArray转化为NSString
+ (NSString*)JSONString:(id)theDic;

// 将字典或者数组转化为字符串
+ (id)theData:(NSString*)JSONString;

//动画
-(void)transition:(UIView*)view AndTime:(float)time;

//正则判断vin
+ (BOOL) validateUserVin:(NSString *)str;
//正则判断手机号
+ (BOOL) validateUserPhone:(NSString *)str;

//判断是否登录
+(void)isLogin:(UIViewController *)target andPush:(UIViewController *)next;

+ (BOOL) validateUserAge:(NSString *)str;
+(BOOL)compareDate:(NSString*)date01 withDate:(NSString*)date02;

+(NSDictionary *)txtChangToJsonWithString:(NSString *)string;
+(void)showAlertViewAndAnimated:(BOOL)animated andString:(NSString *)string;

@end
