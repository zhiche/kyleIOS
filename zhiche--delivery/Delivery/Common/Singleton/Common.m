//
//  Common.m
//  GL
//
//  Created by 王亚陆 on 16/1/12.
//  Copyright © 2016年 知车. All rights reserved.
//

#import "Common.h"
//#import "AFNetworking.h"
#import <AFNetworking.h>
#import "UIImageView+WebCache.h"
#import "Header.h"
#import <Masonry.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "MBProgressHUD.h"
#import "LoginViewController.h"

@implementation Common
{
    DownloadFinishedBlock _finishedBlock;
    DownloadFailedBlock _failedBlock;
}

//+(void)showGifWithText:(NSString *)text
//{
//    [GiFHUD setGifWithImageName:@"pika.gif"];
//    
//    if ([text isEqualToString:@""]) {
//        [GiFHUD setTitle:@"我很快，等我。。。。"];
//    }
//    else
//    {
//        [GiFHUD setTitle:text];
//    }
//    [GiFHUD showWithOverlay];
//}
//+(void)disMissDD
//{
//    [GiFHUD dismiss];
//}

//提示框
-(UIImageView*)createUIImage:(NSString *)title andDelay:(float)delay {
    
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.tag = 88;

    label.textAlignment = NSTextAlignmentCenter;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:FontOfSize15];
    label.textColor = [UIColor whiteColor];
    CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    
    // 名字的H
    CGFloat nameH = size.height;
    // 名字的W
    CGFloat nameW = size.width;
    
    
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(FontOfSize15);

    UIImageView * imageCollect = [[UIImageView alloc]init];
    imageCollect.image = [UIImage imageNamed:@"project_product_collect_jiaohu"];
    imageCollect.frame = CGRectMake(0, 0, 90+label.frame.size.width, 41);
    imageCollect.alpha = 0.5;
    imageCollect.tag = 100;
    
    
    [imageCollect addSubview:label];
    //收藏成功提示
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(imageCollect.mas_centerX);
        make.centerY.equalTo(imageCollect.mas_centerY);
    }];
    
    [self performSelector:@selector(CollectDissMi:) withObject:imageCollect afterDelay:delay];
    

    return imageCollect;
}
-(void)CollectDissMi:(UIImageView*)sender{
    
    [sender  removeFromSuperview];
}

//text自适应宽高
-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor *)color{
    
        UILabel * label =[[UILabel alloc]init];
        label.text = title;
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = color;
        UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];
    
        CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
        // 名字的H
        CGFloat nameH = size.height;
        // 名字的W
        CGFloat nameW = size.width;
    
        label.textAlignment = NSTextAlignmentLeft;
        label.frame =CGRectMake(0, 0, nameW, nameH);
        label.font = Font(font)
        return label;
}




//tableview多余的线隐藏
+(void)setExtraCellLindeHidder:(UITableView *)tableView
{
    UIView *view=[UIView new];
    view.backgroundColor=[UIColor clearColor];
    [tableView setTableFooterView:view];
}

//设置导航专用按钮
-(void)customNavigationItem:(UIViewController *)target position:(item_position)position title:(NSString *)title image:(UIImage *)image action:(SEL)action{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    btn.frame = CGRectMake(0, 0, 60, 30);
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setBackgroundImage:image forState:UIControlStateNormal];
    [btn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    if (position == item_left) {
        target.navigationItem.leftBarButtonItem = item;
    } else {
        target.navigationItem.rightBarButtonItem = item;
    }
}


-(UIView*)noInternet:(NSString *)title and:(UIViewController *)target action:(SEL)action andCGreck:(CGRect)frame
{
    
    UIView * backview = [[UIView alloc]initWithFrame:frame];
    backview.backgroundColor = [UIColor whiteColor];
    UILabel * label = [[UILabel alloc]init];
    label.text = @"当前网络不可用，请检查网络配置";
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
    CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    label.textColor = RGBACOLOR(149, 149, 149, 1);
    // 名字的H
    CGFloat nameH = size.height;
    // 名字的W
    CGFloat nameW = size.width;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(12.0f);
    [backview addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo (backview.mas_centerX);
        make.centerY.mas_equalTo (backview.mas_centerY);
    }];
    
    UIImageView * imageBack = [[UIImageView alloc]init];
    imageBack.image = [UIImage imageNamed:@"jiaohu_duanwang_pic"];
    [backview addSubview:imageBack];
    [imageBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo (label.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(76, 75));
        make.bottom.mas_equalTo(label.mas_top).with.offset(-10);
    }];

    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"jiaohu_duanwang_juxing"] forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor whiteColor];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [btn setTitle:@"刷新重试" forState:UIControlStateNormal];
    btn.layer.cornerRadius = 5;
    btn.layer.borderWidth = 0.5;
    btn.layer.borderColor = fontGrayColor.CGColor;
    [btn setTitleColor:BlackColor forState:UIControlStateNormal];
    [backview addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo (label.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(100, 35));
        make.top.mas_equalTo(label.mas_bottom).with.offset(10);
    }];

    return backview;
}


-(UIView*)customNavigationItem:(UIViewController *)target action:(SEL)action{
    
    UIView * NaView =[[UIView alloc]initWithFrame:CGRectMake(0, 20, Main_Width, 36)];
    UIButton * btn1 =[UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(14, 8, 28, 28);
    [btn1 setBackgroundImage:[UIImage imageNamed:@"detials_project_top_back"] forState:UIControlStateNormal];
    btn1.tag = 1;
    [btn1 addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [NaView addSubview:btn1];
    UIButton * btn2 =[UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(Main_Width-14-28, 8, 28, 28);
    [btn2 setBackgroundImage:[UIImage imageNamed:@"detials_project_top_share"] forState:UIControlStateNormal];
    btn2.tag = 2;
    [btn2 addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [NaView addSubview:btn2];
    UIButton * btn3 =[UIButton buttonWithType:UIButtonTypeCustom];
    btn3.frame = CGRectMake(Main_Width-14-28-28-15, 8, 28, 28);
    [btn3 setBackgroundImage:[UIImage imageNamed:@"detials_project_top_collect"] forState:UIControlStateNormal];
    btn3.tag = 3;
    [btn3 addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [NaView addSubview:btn3];

    return NaView;
}



- (void)writeToLocal:(NSData *)data {
    NSString *path = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@.txt",NSStringFromClass([self class])];
    [data writeToFile:path atomically:YES];
}

- (NSData *)readLocal {
    NSString *path = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@.txt",NSStringFromClass([self class])];
    NSLog(@"path:%@",path);
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        return [NSData dataWithContentsOfFile:path];
    }
    return nil;
}


//get 请求
+(void)requestWithUrlString:(NSString *)urlString contentType:(NSString *)type finished:(DownloadFinishedBlock)finishedBlock failed:(DownloadFailedBlock)failedBlock{
    
    AFHTTPRequestOperationManager *manager  = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:type,nil];
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy defaultPolicy];
    securityPolicy.allowInvalidCertificates = YES;
    securityPolicy.validatesDomainName = NO;
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSString *string = [user objectForKey:login_token];
    
    if (string.length > 0) {
        
        [manager.requestSerializer setValue:string forHTTPHeaderField:@"Authorization"];
        
    }
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //可以进行一些其他操作，比如将网络数据NSData写到本地路径
        //执行一些其他操作后，再调用自己的block，方便对程序进行扩展
        
        
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
        NSString *string = [user objectForKey:login_token];
        
        if (string.length > 0) {
            
            [manager.requestSerializer setValue:string forHTTPHeaderField:@"Authorization"];
            
        }
        
        NSString * messageCode = [NSString stringWithFormat:@"%@",responseObject[@"messageCode"]];

        
        if (![responseObject[@"success"] boolValue] && [messageCode isEqualToString:@"5004"]) {
            
            LoginViewController *loginVC = [[LoginViewController alloc]init];
            UINavigationController * loginNC = [[UINavigationController alloc]initWithRootViewController:loginVC];
            UIViewController *viewC = [[[UIApplication sharedApplication] keyWindow] rootViewController];
#pragma mark push方法跟这一样，只是变变写法
            [viewC presentViewController:loginNC animated:YES completion:nil];
            
            
        } else {
            
            finishedBlock(responseObject);
        }


    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //获取请求头 状态码  400 500 502 503 504

        if (operation.response.statusCode == 404) {
            
            NSLog(@"404");
        }
        if (operation.response.statusCode == 502) {
            
            NSLog(@"502");
        }

        //当出现错误信息是 弹出提示框
//        [Common showAlertViewAndAnimated:YES andString:error.localizedDescription];
        [Common showAlertViewAndAnimated:YES andString:@"系统在维护中"];


        failedBlock(error.localizedDescription);
    }];
}

+(void)afPostRequestWithUrlString:(NSString *)urlString parms:(NSDictionary *)dic finishedBlock:(DownloadFinishedBlock)finished failedBlock:(DownloadFailedBlock)failed{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy defaultPolicy];
    securityPolicy.allowInvalidCertificates = YES;
    securityPolicy.validatesDomainName = NO;
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSString *string = [user objectForKey:login_token];
    
    if (string.length > 0) {
        [manager.requestSerializer setValue:string forHTTPHeaderField:@"Authorization"];
    }
    
    [manager POST:urlString parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //调用自己定义的block
        NSMutableDictionary * dicObj =[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];

        NSString * messageCode = [NSString stringWithFormat:@"%@",dicObj[@"messageCode"]];
        
        if (![dicObj[@"success"] boolValue] && [messageCode isEqualToString:@"5004"]) {
            
            LoginViewController *loginVC = [[LoginViewController alloc]init];
            UINavigationController * loginNC = [[UINavigationController alloc]initWithRootViewController:loginVC];
            UIViewController *viewC = [[[UIApplication sharedApplication] keyWindow] rootViewController];
#pragma mark push方法跟这一样，只是变变写法
            [viewC presentViewController:loginNC animated:YES completion:nil];
            
        } else {
            
            finished(responseObject);
        }
        
    
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //当出现错误信息是 弹出提示框
//        [Common showAlertViewAndAnimated:YES andString:error.localizedDescription];
        [Common showAlertViewAndAnimated:YES andString:@"系统在维护中"];

        //捕捉错误信息
        failed(error.localizedDescription);
    
    }];
}



+(void)showAlertViewAndAnimated:(BOOL)animated andString:(NSString *)string
{
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(100, 100, 100, 100)];
    view.backgroundColor = [UIColor blackColor];
    [window addSubview:view];
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, window.frame.size.width/2.0, 20)];
    label.text = string;
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:15];
    label.numberOfLines = 0;
    label.textAlignment = NSTextAlignmentCenter;
    CGRect rect = [label.text boundingRectWithSize:CGSizeMake(window.frame.size.width/2.0, 200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil];
    label.frame = CGRectMake(15, 5, rect.size.width + 20, rect.size.height + 10);
    view.frame = CGRectMake(0, 0, rect.size.width + 50, rect.size.height + 20);
    [view addSubview:label];
    view.layer.cornerRadius = 10;
    
    view.center = window.center;
    
    if (animated) {
        view.alpha = 0;
        [window addSubview:view];
        
        [UIView animateWithDuration:0.3 animations:^{
            view.alpha = 0.78;
        } completion:^(BOOL finished) {
            
        
        }];
    }
    else {
        [window addSubview:view];
    }
    
    //延迟执行
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [view removeFromSuperview];
        
    });
    
}


//post 请求
-(void)afPostRequestWithUrlString:(NSString *)urlString parms:(NSDictionary *)dic finishedBlock:(DownloadFinishedBlock)finished failedBlock:(DownloadFailedBlock)failed{
    if (_finishedBlock!=finished) {
        _finishedBlock = nil;
        _finishedBlock = finished;
    }
    if (_failedBlock!=failed) {
        _failedBlock = nil;
        _failedBlock = failed;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy defaultPolicy];
    securityPolicy.allowInvalidCertificates = YES;
    securityPolicy.validatesDomainName = NO;
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSString *string = [user objectForKey:login_token];

    if (string.length > 0) {
        
        [manager.requestSerializer setValue:string forHTTPHeaderField:@"Authorization"];

    }

    [manager POST:urlString parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
    
        NSMutableDictionary * dicObj =[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];

        NSString * messageCode = [NSString stringWithFormat:@"%@",dicObj[@"messageCode"]];

        if (![dicObj[@"success"] boolValue] && [messageCode isEqualToString:@"5004"]) {
            
            LoginViewController *loginVC = [[LoginViewController alloc]init];
            UINavigationController * loginNC = [[UINavigationController alloc]initWithRootViewController:loginVC];
            UIViewController *viewC = [[[UIApplication sharedApplication] keyWindow] rootViewController];
#pragma mark push方法跟这一样，只是变变写法
            [viewC presentViewController:loginNC animated:YES completion:nil];
            
            
        } else {
            
            //调用自己定义的block
            _finishedBlock(responseObject);
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        
        if (operation.response.statusCode == 404) {
            
            NSLog(@"404");
        }
        if (operation.response.statusCode == 502) {
            
            NSLog(@"502");
        }

        
        //当出现错误信息是 弹出提示框
//        [Common showAlertViewAndAnimated:YES andString:error.localizedDescription];
        [Common showAlertViewAndAnimated:YES andString:@"系统在维护中"];

        
        //捕捉错误信息
        _failedBlock(error.localizedDescription);
    }];
}



+(CGRect)myRectWithX:(CGFloat)x Y:(CGFloat)y width:(CGFloat)width height:(CGFloat)height;
{
    CGFloat myx=[UIScreen mainScreen].bounds.size.width/375.0;
    CGFloat myy=[UIScreen mainScreen].bounds.size.height/667.0;
    return CGRectMake(x*myx, y*myy, width*myx, height*myy);
}
//登录
+(void)writePhone:(NSString *)phone writeAccount:(NSString *)account writeSecret:(NSString *)secret  writeToken:(NSString *)token remember:(BOOL)isRemembe
{
    NSUserDefaults * userDefaults=[NSUserDefaults standardUserDefaults];
    if (isRemembe) {
        if (account&&secret) {
            [userDefaults setObject:account forKey:@"account"];
            [userDefaults setObject:secret forKey:@"secret"];
        }
        if (phone) {
            [userDefaults setObject:phone forKey:@"phone"];
        }
    }
    else
    {
        if (account) {
            [userDefaults setObject:account forKey:@"account"];
        }
        if (phone) {
            [userDefaults setObject:phone forKey:@"phone"];
        }
    }
    
    [userDefaults setObject:token forKey:@"token"];
    [userDefaults synchronize];
    NSArray * arr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * path = [arr objectAtIndex:0];
    NSLog(@"userDefaults:%@",path);
}
+(NSDictionary*)backToken
{
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    
    NSUserDefaults *UserDefaults =[NSUserDefaults standardUserDefaults];
    NSString*string=[UserDefaults objectForKey:@"token"];
    [dic setObject:string forKey:@"token"];
    return dic;
}


//将NSDictionary或者NSArray转化为NSString
+(NSString*)JSONString:(id)theDic;
{
    //保存数组
    NSError *errArr;
    NSData *dataArr=[NSJSONSerialization dataWithJSONObject:theDic options:NSJSONWritingPrettyPrinted error:&errArr];
    NSString *jsonArr=[[NSString alloc]initWithData:dataArr encoding:NSUTF8StringEncoding];
    
    return jsonArr;
}
// 将字典或者数组转化为字符串
+(id)theData:(NSString*)JSONString;
{
    //字符串转化成data类型
    NSData *dataDic=[JSONString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *errDic;
    
    //将data类型转化成字典
    id jsonObjectDic = [NSJSONSerialization JSONObjectWithData:dataDic
                                                       options:NSJSONReadingAllowFragments
                                                         error:&errDic];
    
    return jsonObjectDic;
}


-(void)transition:(UIView*)view AndTime:(float)time{
    CATransition * transition = [CATransition animation];
    transition.duration = time;
    transition.type = kCATransitionFromTop;
    transition.subtype = kCATransitionFromRight;
    [view.window.layer addAnimation:transition forKey:nil];
}

+(BOOL)validateUserVin:(NSString *)str{
    
    NSRegularExpression *regularexpression = [[NSRegularExpression alloc]
                                            initWithPattern:@"^([123469JKLRSTVWYZ][a-zA-Z0-9^IiOoQq]{7}([0-9]|[Xx])[1-9A-Y^IiOoQq][a-zA-Z0-9^IiOoQq][a-zA-Z0-9]{6})$"
                                              options:NSRegularExpressionCaseInsensitive
                                              error:nil];
    NSUInteger numberofMatch = [regularexpression numberOfMatchesInString:str
                                                                  options:NSMatchingReportProgress
                                                                    range:NSMakeRange(0, str.length)];
    if(numberofMatch > 0)
    {
        return YES;
    }
    return NO;
}
//校验用户手机号码
+ (BOOL) validateUserPhone:(NSString *)str
{
    NSRegularExpression *regularexpression = [[NSRegularExpression alloc]
                                              initWithPattern:@"^1[3|4|5|7|8][0-9][0-9]{8}$"
                                              options:NSRegularExpressionCaseInsensitive
                                              error:nil];
    NSUInteger numberofMatch = [regularexpression numberOfMatchesInString:str
                                                                  options:NSMatchingReportProgress
                                                                    range:NSMakeRange(0, str.length)];
    if(numberofMatch > 0)
    {
        return YES;
    }
    return NO;
}

//
+ (BOOL) validateUserAge:(NSString *)str {
    NSRegularExpression *regularexpression = [[NSRegularExpression alloc]
                                              initWithPattern:@"^([1-9]\\d{0,5}|1000000)$"
                                              options:NSRegularExpressionCaseInsensitive
                                              error:nil];
    NSUInteger numberofMatch = [regularexpression numberOfMatchesInString:str
                                                                  options:NSMatchingReportProgress
                                                                    range:NSMakeRange(0, str.length)];
    
    if(numberofMatch > 0)
    {
        NSLog(@"%@ isNumbericString: YES", str);
        return YES;
    }
    
    NSLog(@"%@ isNumbericString: NO", str);
    return NO;
}

+(void)isLogin:(UIViewController *)target andPush:(UIViewController *)next
{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *string = [userDefaults objectForKey:@"login"];
    

    if (string == nil ) {
        
        LoginViewController *loginVC = [[LoginViewController alloc]init];
        UINavigationController * loginNC = [[UINavigationController alloc]initWithRootViewController:loginVC];
        
        [target presentViewController:loginNC animated:YES completion:nil];
        
    }  else {
        [target.navigationController pushViewController:next animated:YES];
    }
}
+(BOOL)compareDate:(NSString*)date01 withDate:(NSString*)date02{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDate *dt1 = [[NSDate alloc] init];
    NSDate *dt2 = [[NSDate alloc] init];
    dt1 = [df dateFromString:date01];
    dt2 = [df dateFromString:date02];
    NSComparisonResult result = [dt1 compare:dt2];
    switch (result)
    {
            //date02比date01大
        case NSOrderedAscending: return YES; break;
            //date02比date01小
        case NSOrderedDescending: return NO; break;
            //date02=date01
        case NSOrderedSame: return YES; break;
        default: NSLog(@"erorr dates %@, %@", dt2, dt1); break;
    }
}
//获取txt文件数据
+(NSDictionary *)txtChangToJsonWithString:(NSString *)string
{
    NSString *path = [[NSBundle mainBundle]pathForResource:string ofType:@"txt"];
    
    
    NSString *conters = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSData *data = [conters dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary*dic=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    return  dic;
    
}

@end
