//
//  UncaughtExceptionHandler.m
//  Game
//
//  Created by WangYue on 13-7-17.
//  Copyright (c) 2013年 ntstudio.imzone.in. All rights reserved.
//


#import "MyUncaughtExceptionHandler.h"
#import <UIKit/UIKit.h>



NSString * applicationDocumentsDirectory()
{
    return [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingString:@"Exception.txt"];
}

void UncaughtExceptionHandler(NSException * exception);

void UncaughtExceptionHandler(NSException * exception)
{
    NSArray * arr = [exception callStackSymbols];
    NSString * reason = [exception reason];
    NSString * name = [exception name];
    NSString * url = [NSString stringWithFormat:@"========异常错误报告========\nname:%@\nreason:\n%@\ncallStackSymbols:\n%@",name,reason,[arr componentsJoinedByString:@"\n"]];
//    NSString * path = [applicationDocumentsDirectory() stringByAppendingPathComponent:@"Exception.txt"];
    NSString *path = applicationDocumentsDirectory();
    [url writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
	NSString *urlStr = [NSString stringWithFormat:@"mailto:lzt2015@126.com?subject=客户端bug报告&body=很抱歉应用出现故障,感谢您的配合!发送这封邮件可协助我们改善此应用<br>"
						"错误详情:<br>%@<br>--------------------------<br>%@<br>---------------------<br>%@",
						name,reason,[arr componentsJoinedByString:@"<br>"]];
	
	NSURL *url2 = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	[[UIApplication sharedApplication] openURL:url2];
    
    NSLog(@"arr:%@ \n reason:%@ \n name:%@",arr,reason,name);
    
    
}



@implementation MyUncaughtExceptionHandler



//void UncaughtExceptionHandler(NSException * exception)
//{
//    NSArray * arr = [exception callStackSymbols];
//    NSString * reason = [exception reason];
//    NSString * name = [exception name];
//    NSString * url = [NSString stringWithFormat:@"========异常错误报告========\nname:%@\nreason:\n%@\ncallStackSymbols:\n%@",name,reason,[arr componentsJoinedByString:@"\n"]];
//    NSString * path = [applicationDocumentsDirectory() stringByAppendingPathComponent:@"Exception.txt"];
//    [url writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
//    
//    NSString *urlStr = [NSString stringWithFormat:@"mailto:wy@91goal.com?subject=客户端bug报告&body=很抱歉应用出现故障,感谢您的配合!发送这封邮件可协助我们改善此应用<br>"
//                        "错误详情:<br>%@<br>--------------------------<br>%@<br>---------------------<br>%@",
//                        name,reason,[arr componentsJoinedByString:@"<br>"]];
//    
//    NSURL *url2 = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//    [[UIApplication sharedApplication] openURL:url2];
//    
//    NSLog(@"arr:%@ \n reason:%@ \n name:%@",arr,reason,name);
//    
//    
//    
//    NSString *string = [NSString stringWithFormat:@"/sys/saveErrorLog.json"];
//    string = [kURL stringByAppendingString:string];
//    NSURL *urlS = [NSURL URLWithString:string];
//    
//    //    username  mobile  systemType  systemVersion    errorContent
//    //    代码实现获得应用的Verison号：
//    //
//    //    [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
//    //    或
//    //    [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
//    //
//    
//    Personal *person = [NSKeyedUnarchiver unarchiveObjectWithFile:userNamekeyPath];
//    
//    ASIFormDataRequest *request = [[ASIFormDataRequest alloc]initWithURL:urlS];
//    
//    [request setStringEncoding:NSUTF8StringEncoding];
//    [request setRequestMethod:@"POST"];
//    [request setDelegate:self];
//    
//    [request setPostValue:person.nameTF forKey:@"username"];
//    [request setPostValue:person.numberTF forKey:@"mobile"];
//    [request setPostValue:[[UIDevice currentDevice] name] forKey:@"systemType"];
//    [request setPostValue:[[UIDevice currentDevice]systemVersion ] forKey:@"systemVersion"];
//    [request setPostValue:reason forKey:@"errorContent"];
//    [request setPostValue:person.current_key forKey:@"current_des_key"];
//    
//    
//    [request setDidFinishSelector:@selector(successInfo:)];
//    [request setDidFailSelector:@selector(failInfo:)];
//    [request startAsynchronous];
//    
//    
//    
//}
//

-(NSString *)applicationDocumentsDirectory
{
    return [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingString:@"Exception.txt"];
}

+ (void)setDefaultHandler
{
    NSSetUncaughtExceptionHandler(&UncaughtExceptionHandler);
    
}

+ (NSUncaughtExceptionHandler *)getHandler
{
    return NSGetUncaughtExceptionHandler();
}

+ (void)TakeException:(NSException *)exception
{
    NSArray * arr = [exception callStackSymbols];
    NSString * reason = [exception reason];
    NSString * name = [exception name];
    NSString * url = [NSString stringWithFormat:@"========异常错误报告========\nname:%@\nreason:\n%@\ncallStackSymbols:\n%@",name,reason,[arr componentsJoinedByString:@"\n"]];
//    NSString * path = [applicationDocumentsDirectory() stringByAppendingPathComponent:@"Exception.txt"];
    NSString *path = applicationDocumentsDirectory();
    [url writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];

    NSLog(@"%s:%d %@", __FUNCTION__, __LINE__, url);
}

@end

