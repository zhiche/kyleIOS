//
//  DefineNavigation.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/22.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DefineNavigation : UINavigationController<UINavigationControllerDelegate, UIGestureRecognizerDelegate>

@end
