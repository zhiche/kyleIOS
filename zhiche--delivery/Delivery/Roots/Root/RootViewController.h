//
//  RootViewController.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UITabBarController
+(id)defaultsTabBar;
+ (void)deallocTabbar;

@property (nonatomic,copy) NSString *rootString;

-(void)setTabBarHidden:(BOOL)Bool;

-(void)pressButton:(UIButton *)btn;

@end
