//
//  RootViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "RootViewController.h"
#import "HomeViewController.h"
#import "PriceViewController.h"
#import "InstallViewController.h"
#import "LoginViewController.h"
#import <Masonry.h>
#import "NavViewController.h"
#import "MineOrderVC.h"//订单
#define threeWidth  Main_Width/3
#import "Header.h"
#import "DefineNavigation.h"
#import "Common.h"


static RootViewController *rootVC;
@interface RootViewController ()

@property (nonatomic) BOOL tabBarIsShow;
@property (nonatomic,strong) NSMutableArray *buttonArray;
@property (nonatomic,strong) UIImageView *backImageView;
@property (nonatomic,strong) NSMutableArray * labelArray;

@end

@implementation RootViewController

+ (void)deallocTabbar
{
    rootVC = nil;
}

+(id)defaultsTabBar
{
    if (rootVC == nil) {
        rootVC = [[RootViewController alloc]init];
    }
    
    return  rootVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.buttonArray = [NSMutableArray array];
    self.labelArray = [NSMutableArray array];
    self.tabBarIsShow = YES;
    _rootString = @"1";
    self.tabBar.hidden = YES;

    self.view.backgroundColor = WhiteColor;
    HomeViewController *homeVC = [[HomeViewController alloc]init];
    DefineNavigation *homeNC = [[DefineNavigation alloc]initWithRootViewController:homeVC];

    
    MineOrderVC *orderVC = [[MineOrderVC alloc]init];
    DefineNavigation *orderNC = [[DefineNavigation alloc]initWithRootViewController:orderVC];
    
    
    InstallViewController *installVC = [[InstallViewController alloc]init];
    DefineNavigation *installNC = [[DefineNavigation alloc]initWithRootViewController:installVC];
    
    self.viewControllers = @[homeNC,orderNC,installNC];
    
    [self setCustomTabbarItem];
}
-(void)setCustomTabbarItem
{
    self.backImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, screenHeight - 48*kHeight, screenWidth, 48*kHeight)];
    UIImageView * imageline = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, 0.5*kHeight)];
    imageline.backgroundColor = AddCarNameBtnColor;
    [self.backImageView addSubview:imageline];
    
    self.backImageView.backgroundColor = [UIColor whiteColor];
//    self.backImageView.image = [UIImage imageNamed:@"wireframe_bottom"];
    
    self.backImageView.userInteractionEnabled = YES;
    [self.view addSubview:self.backImageView];
    
    NSArray *titleArray = @[@"首页",
                            @"我的订单",
                            @"商户中心"];
    CGFloat space = screenWidth / 3.0;
    for (int i = 0; i < titleArray.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        NSString * imageNormal = [NSString stringWithFormat:@"home_bottom_%d0",i+1];
        NSString * imageSelected =[NSString stringWithFormat:@"home_bottom_%d1",i+1];
        [button setImage:[UIImage imageNamed:imageNormal] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:imageSelected] forState:UIControlStateSelected];
        button.tag = 1001+i+100;
//        NSString *titleString = [NSString stringWithFormat:@"%@",titleArray[i]];
//        [button setTitle:titleString forState:UIControlStateNormal];
//        [button setTitleColor:GrayColor forState:UIControlStateNormal];
//        button.titleLabel.font = Font(15);
        UIButton * buttonBig = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonBig.tag = 1001+i;
        
        [buttonBig addTarget:self action:@selector(pressButton:) forControlEvents:UIControlEventTouchUpInside];
//        buttonBig.backgroundColor = [UIColor cyanColor];
//        button.tag = 1001 + i;
        [self.backImageView addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.backImageView.mas_top).with.offset(8*kHeight);
            make.left.mas_equalTo(self.backImageView.mas_left).with.offset(40*kWidth+i*space);
            make.size.mas_equalTo(CGSizeMake(22*kWidth, 21*kHeight));
        }];
        [self.backImageView addSubview:buttonBig];
        [buttonBig mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.backImageView.mas_top).with.offset(8*kHeight);
            make.left.mas_equalTo(self.backImageView.mas_left).with.offset(threeWidth*i);
            make.size.mas_equalTo(CGSizeMake(threeWidth, 46*kHeight));
        }];
        

        UILabel * label = [self createUIlabel:titleArray[i] andFont:FontOfSize13 andColor:littleBlackColor];
        label.text = titleArray[i];
        if (i == 0) {
            button.selected = YES;
            [button setTitleColor:BlackColor forState:UIControlStateNormal];
            label.textColor = YellowColor;
        }
        [self.backImageView addSubview:label];

        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(button.mas_bottom).with.offset(2*kHeight);
            make.centerX.mas_equalTo(button.mas_centerX);
        }];
        [self.labelArray addObject:label];
        [self.buttonArray addObject:button];
    }
}

-(void)selectButtonWithIndex:(NSInteger)index
{
    UIButton *btn = (UIButton *)self.buttonArray[index];
    [self pressButton:btn];
}

-(void)setTabBarHidden:(BOOL)Bool
{
    if (Bool) {
        [self hideTabBar];
    } else {
        [self showTabBar];
    }
}

-(void)hideTabBar
{
    if (!self.tabBarIsShow) {
        return;
    }
        
        [UIView animateWithDuration:0.35 animations:^{
            self.backImageView.frame = CGRectMake(0, screenHeight, screenWidth, 48*kHeight);
        }];
    
    self.tabBarIsShow = NO;
}

-(void)showTabBar
{
    if (self.tabBarIsShow) {
        return;
    }
    [UIView animateWithDuration:0.35 animations:^{
        self.backImageView.frame = CGRectMake(0, screenHeight - 48*kHeight, screenWidth, 48*kHeight);
        
//        self.tabBarController.tabBar.hidden = NO;
//        self.hidesBottomBarWhenPushed = NO;

 
    }];
    
    self.tabBarIsShow = YES;
}


-(void)pressButton:(UIButton *)sender
{
    
    if (sender.tag - 1001 >= 4) {
        return;
    }
    
    if (sender.tag - 1001 != 1) {
        _rootString = @"1";
    }
    
    self.selectedIndex = sender.tag - 1001;
    for (UILabel * label in self.labelArray) {
        label.textColor = littleBlackColor;
    }
    UILabel * label = self.labelArray[self.selectedIndex];
    label.textColor = YellowColor;
    
    NSInteger smallBtn = sender.tag + 100;
    for (UIButton *tempBtn in self.buttonArray) {
        if (tempBtn.tag == smallBtn) {
            tempBtn.selected = YES;
            
            [tempBtn setTitleColor:BlackColor forState:UIControlStateNormal];

        } else {
            tempBtn.selected = NO;
            [tempBtn setTitleColor:GrayColor forState:UIControlStateNormal];

        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *string = [defaults objectForKey:@"login"];
//    if (string == nil) {
//        
//        LoginViewController *loginVC = [[LoginViewController alloc]init];
//        
//        UINavigationController *loginNC = [[UINavigationController alloc]initWithRootViewController:loginVC];
//        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            
//            [self presentViewController:loginNC animated:YES completion:nil];
//            
//        });
//   
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentCenter;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}
@end
