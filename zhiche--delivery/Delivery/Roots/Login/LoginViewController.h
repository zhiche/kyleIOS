//
//  LoginViewController.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TopBackView;
@interface LoginViewController : UIViewController
@property (nonatomic,strong) TopBackView *topBackView;

@property (nonatomic,copy) NSString *backToFirst;

@end
