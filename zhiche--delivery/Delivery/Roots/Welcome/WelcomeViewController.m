//
//  WelcomeViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "WelcomeViewController.h"
#import "LoginViewController.h"
#import <Masonry.h>
@interface WelcomeViewController ()

@property (nonatomic,strong) UIScrollView *scrollView;


@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.scrollView = [[UIScrollView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    
    for (int i =0; i<1; i++) {
        UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth * i, 0, screenWidth, screenHeight)];
        NSString * nameStr = [[NSString alloc]initWithFormat:@"qidong%d.png",i+1];
        imageView.image = [UIImage imageNamed:nameStr];
        if (i==3) {
            UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setTitle:@"" forState:UIControlStateNormal];
            [btn setImage:[UIImage imageNamed:@"unpress"] forState:UIControlStateNormal];
            btn.tag = 100;
            [btn addTarget:self action:@selector(pressBtn) forControlEvents:UIControlEventTouchUpInside];
            [btn setImage:[UIImage imageNamed:@"press"] forState:UIControlStateHighlighted];

            imageView.userInteractionEnabled = YES;
            [imageView addSubview:btn];
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(imageView.mas_centerX);
                make.bottom.equalTo(imageView.mas_bottom).with.offset(-77*kHeight);
                make.size.mas_equalTo(CGSizeMake(160*kWidth, 38*kHeight));
            }];
        }
        [self.scrollView addSubview:imageView];
    }
    
    [self pressBtn];
    self.scrollView.contentOffset = CGPointMake(0, 0);
    self.scrollView.contentSize = CGSizeMake(screenWidth * 4, screenHeight);
    self.scrollView.contentSize = CGSizeMake(screenWidth, screenHeight);

    self.scrollView.bounces = NO;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.scrollView];
}

-(void)pressBtn
{
    //保存为已经进入app
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"firstIn"];
    [[NSUserDefaults standardUserDefaults]synchronize];//及时保存 不等到按home键就保存内容
    

    
    //延迟执行
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        
        if (self.callBack) {
            _callBack();
        };

        
    });

    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
