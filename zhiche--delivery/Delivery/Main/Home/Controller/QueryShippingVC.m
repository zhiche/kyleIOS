//
//  QueryShippingVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/8/22.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "QueryShippingVC.h"
#import <Masonry.h>
#import "Common.h"
#import "WKProgressHUD.h"
#import "QueryShippingCell.h"
#import "Header.h"
#import "ActionSheetPicker.h"
#import "PlaceOrderVC.h"
#import "RootViewController.h"
#import "area.h"
#import "WKProgressHUD.h"
#import "QueryCarCell.h"
#import "QueryAddressVC.h"
#import "YLLabel.h"
#import "SevenSwitch.h"
#define ImageSeleteTag 100
#define TimeBtnTag 200
#define PickCarTimeTag 300
#define AddressBtnTag 400
#define tableviewTag 600
#define carTableViewTag 700
#define InforSwitchTag 30000

#define MySetValue(dict, value) ((!dict[value] || [dict[value] isKindOfClass:[NSNull class]])?@"":dict[value])

@interface QueryShippingVC ()
{
    UIImageView * nav;
    Common * com;
    UIScrollView * scroll;
    NSMutableDictionary * dataSouceDic;
//    UITableView * tableview;
//    UITableView * carTableView;
    NSMutableArray * carInfor;
    area * AREA;
    RootViewController * TabBar;
    WKProgressHUD *hud;
    int framHigth;
    UILabel * arriveTime;
    UIButton * promptBtn;
    UILabel * distanceName;
    UIImageView * arriveTimeimage;
}

@property (nonatomic,strong) NSMutableArray * carInforBtnArr;
@property (nonatomic,strong) NSMutableArray * carInforNameArr;
@property (nonatomic,strong) UITextField * CarPromptField;

@property (nonatomic,strong) NSArray * timeChoose;
@property (nonatomic,strong) NSString * atime;//提车时间
@property (nonatomic,strong) NSString * adate;//提车日期
@property (nonatomic,strong) NSString * citycode1;//市编码
@property (nonatomic,strong) NSString * citycode2;//

@property (nonatomic,strong) NSString * estValue;//车辆估值
@property (nonatomic,strong) NSString * isPick;//是否上门提车
@property (nonatomic,strong) NSString * isDeliv;//是否送车上门


@property (nonatomic,strong) UIButton * QueryBtn;//查询
@property (nonatomic,strong) UIButton * GoShippingBtn;//去托运
@property (nonatomic,strong) NSString * carType;//车型
@property (nonatomic,strong) UIView * backview;

@property (nonatomic,strong) UIView * backview1;
@property (nonatomic,strong) UILabel * address1;
@property (nonatomic,strong) UILabel * address2;

@property (nonatomic,strong) UIView * backview2;
@property (nonatomic,strong) UIView * backview3;
@property (nonatomic,strong) UIView * backview30;
@property (nonatomic,strong) UIView * backview31;
@property (nonatomic,strong) UIView * backview4;


@property (nonatomic,strong) UILabel * arriveTimeLabel;//到达时间
@property (nonatomic,strong) UILabel * unitPriceLabel;//单台价格
@property (nonatomic,strong) UIView * unitPriceLine;//横线
@property (nonatomic,strong) UILabel * disUnitPriceLabel;//单台活动价格
@property (nonatomic,strong) UILabel * entirePriceLabel;//整车价格
@property (nonatomic,strong) UIView * entirePriceLine;//横线

@property (nonatomic,strong) UILabel * disEntirePriceLabel;//整车活动价格
@property (nonatomic,strong) UILabel * distance;//运距
@property (nonatomic,strong) UILabel * descLabel;//活动说明


@property (nonatomic, strong) NSMutableArray *provienceArray;
@property (nonatomic, strong) NSArray *cityArray;

@property (nonatomic,strong) UIView * warnView;
@end

@implementation QueryShippingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GrayColor;
    nav = [self createNav:@"查询运费"];
    [self.view addSubview:nav];
    TabBar = [RootViewController defaultsTabBar];

    [self Init];
    [self createUI];

    
}

-(void)Init{
    
    com = [[Common alloc]init];
    AREA = [[area alloc]init];
    _atime = [[NSString alloc]init];
    _adate = [[NSString alloc]init];
    _carType = [[NSString alloc]init];
    dataSouceDic = [[NSMutableDictionary alloc]init];
    _carInforBtnArr = [[NSMutableArray alloc]init];
    _carInforNameArr = [[NSMutableArray alloc]init];

    _estValue = @"";//车辆估值
    _isPick = @"0";//是否上门提车
    _isDeliv = @"0";//是否送车上门
    
    framHigth = 1;
    _timeChoose = @[@"0点-12点",
                    @"12点-24点"];
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy-MM-dd"];
    _adate = [dateformatter stringFromDate:senddate];
    
    _carType = @"S";//标准轿车:S, 标准SUV:M, 大型SUV:L, 超大型车:EX

    
    NSDate *date = [NSDate date]; // 获得时间对象
    NSDateFormatter  *dateformatterTime=[[NSDateFormatter alloc] init];
    [dateformatterTime setDateFormat:@"HH:mm:ss"];
    NSString * string = [dateformatterTime stringFromDate:date];
    int  b = [[string substringWithRange:NSMakeRange(0,2)] intValue];
    if (b>=0&&b<=12) {
        _atime = @"0";
    }else{
        _atime = @"1";
    }

}
-(void)viewWillAppear:(BOOL)animated{
    [TabBar setTabBarHidden:YES];
    
}

-(void)createDate{
    
    
    if (_citycode1.length == 0 ||_citycode2.length == 0) {
        [self createUIAlertControllerWithTitle:@"提示" and:@"请选择输入完整查询地址"];
    }else{
        if (_estValue.length == 0) {
            [self createUIAlertControllerWithTitle:@"提示" and:@"请输入车辆估值"];
        }else{
            if ([_citycode1 isEqualToString:_citycode2]) {
                [self createUIAlertControllerWithTitle:@"提示" and:@"请选择不同区域地址"];
            }else{
                hud = [WKProgressHUD showInView:self.view withText:@"加载中" animated:YES];
                NSString * url = [NSString stringWithFormat:@"%@?depacode=%@&destcode=%@&vclass=%@&adate=%@&atime=%@&estValue=%@&isPick=%@&isDeliv=%@",feesShipping_Url,_citycode1,_citycode2,_carType,_adate,_atime,_estValue,_isPick,_isDeliv];
                [Common requestWithUrlString:url contentType:@"application/json" finished:^(id responseObj){
                    
                    NSString * success = [NSString stringWithFormat:@"%@",responseObj[@"success"]];
                    if ([success isEqualToString:@"1"]) {
                        dataSouceDic = responseObj[@"data"][0];
//                        _backview30.hidden = NO;
                        _backview31.hidden = NO;
                        promptBtn.hidden = NO;
                        arriveTime.hidden = NO;
                        distanceName.hidden = NO;
                        arriveTimeimage.hidden = NO;
                        UIView * line = (UIView*)[_backview3 viewWithTag:1111];
                        line.hidden = NO;
                        [self upFramAndView];
                    }
                    else{
                        
                    [self createUIAlertControllerWithTitle:@"提示" and:responseObj[@"message"]];
                    }
                    [hud dismiss:YES];
                    
                } failed:^(NSString *errorMsg) {
                    NSLog(@"%@",errorMsg);
                    [hud dismiss:YES];
                }];
            }
        }
    }
}

-(void)upFramAndView{
    
    _GoShippingBtn.hidden = NO;
    _backview4.hidden = NO;
    scroll.contentSize = CGSizeMake(Main_Width, Main_height+250*kHeight);
    [UIView animateWithDuration:0.3 animations:^{
        scroll.contentOffset = CGPointMake(0, 300*kHeight);
    } completion:nil];

    

    

    [_backview mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(Main_height+250*kHeight);
    }];
    
    [_backview3 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(172*kHeight);
    }];

    [_QueryBtn mas_updateConstraints:^(MASConstraintMaker *make) {

        make.top.mas_equalTo(_backview3.mas_bottom).with.offset(_backview4.frame.size.height+_backview31.frame.size.height+10*kHeight);
    }];
    


    NSString * endTime = [NSString stringWithFormat:@"%@",[dataSouceDic[@"endTime"] substringWithRange:NSMakeRange(0, 10)]];
    _arriveTimeLabel.text = endTime;
    _distance.text = [NSString stringWithFormat:@"%.3fkm",[dataSouceDic[@"distance"] floatValue]];
    _unitPriceLabel.text = [NSString stringWithFormat:@"￥%.2f",[dataSouceDic[@"unitPrice"] floatValue]];
    _entirePriceLabel.text = [NSString stringWithFormat:@"￥%.2f",[dataSouceDic[@"entirePrice"] floatValue]];
//
//
    NSString * discount = [NSString stringWithFormat:@"%@",dataSouceDic[@"activity"][@"discount"]];
    //优惠
    if ([ discount isEqualToString:@"1"]) {
        
        [_backview31 mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backview.mas_left);
            make.top.equalTo(_backview3.mas_bottom).with.offset(10*kHeight);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 95*kHeight));
        }];

        _unitPriceLabel.textColor = fontGrayColor;
        _entirePriceLabel.textColor = fontGrayColor;
        _unitPriceLine.hidden = NO;
        _entirePriceLine.hidden = NO;
        
        _disUnitPriceLabel.text = [NSString stringWithFormat:@"￥%.2f",[dataSouceDic[@"disUnitPrice"] floatValue]];

        _disEntirePriceLabel.text = [NSString stringWithFormat:@"￥%.2f",[dataSouceDic[@"disEntirePrice"] floatValue]];
        _descLabel.text =  [NSString stringWithFormat:@"%@",dataSouceDic[@"activity"][@"desc"]];
    }else{
        _unitPriceLabel.textColor = BlackTitleColor;
        _entirePriceLabel.textColor = BlackTitleColor;
        _unitPriceLine.hidden = YES;
        _entirePriceLine.hidden = YES;
    }


}

-(void)createUI{
    
    scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, Main_Width,Main_height-64)];
    scroll.contentOffset = CGPointMake(0, 0);
    scroll.contentSize = CGSizeMake(Main_Width, Main_height-64+129*kHeight);

    self.automaticallyAdjustsScrollViewInsets =NO;
    scroll.bounces = NO;
    scroll.userInteractionEnabled = YES;
    scroll.showsHorizontalScrollIndicator = NO;
    scroll.showsVerticalScrollIndicator = NO;
    scroll.delegate = self;
    [self.view addSubview:scroll];
    
    _backview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, Main_height+317*kHeight)];
    _backview.backgroundColor = GrayColor;
    _backview.userInteractionEnabled = YES;
    [scroll addSubview:_backview];
    [_backview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(scroll.mas_left);
        make.top.mas_equalTo(scroll.mas_top);
        make.size.mas_equalTo(CGSizeMake(Main_Width, Main_height-64+129*kHeight));
    }];
    _backview1 = [[UIView alloc]init];
    _backview1.backgroundColor = WhiteColor;
    [_backview addSubview:_backview1];
    [_backview1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left);
        make.top.mas_equalTo(_backview.mas_top);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 86*kHeight));
    }];
    _backview2 = [[UIView alloc]init];
    _backview2.backgroundColor = WhiteColor;
    [_backview addSubview:_backview2];
    [_backview2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left);
        make.top.mas_equalTo(_backview1.mas_bottom).with.offset(10*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 258*kHeight));
    }];
    _backview3 = [[UIView alloc]init];
    _backview3.backgroundColor = WhiteColor;
    [_backview addSubview:_backview3];
    [_backview3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left);
        make.top.mas_equalTo(_backview2.mas_bottom).with.offset(10*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 86*kHeight));
    }];

    
//    tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 64*kHeight, Main_Width, 86*kHeight) style:UITableViewStylePlain];
//    [tableview setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//    tableview.tag = tableviewTag;
//    tableview.delegate = self;
//    tableview.dataSource = self;
//    tableview.userInteractionEnabled = NO;
//    [tableview setTableFooterView:[UIView new]];
//    [_backview addSubview:tableview];
//    [tableview mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_backview.mas_left).with.offset(0);
//        make.top.mas_equalTo(_backview3.mas_bottom);
//        make.size.mas_equalTo(CGSizeMake(Main_Width, 0*kHeight));
//    }];
    
    
    
    
    [self createViewBack1];
    [self createViewBack2];
    [self createViewBack3];
    [self createWarnView];
    

    


}
-(void)createViewBack1{
    
    UIImageView * SmallImage1 = [[UIImageView alloc]init];
    SmallImage1.image = [UIImage imageNamed:@"Address_Cell_fache"];
    [_backview1 addSubview:SmallImage1];
    [SmallImage1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left).with.offset(20*kWidth);
        make.centerY.mas_equalTo(_backview1.mas_top).with.offset(21.5*kHeight);
        make.size.mas_equalTo(CGSizeMake(12*kWidth, 16*kHeight));
    }];
    UILabel * cityName1 = [com createUIlabel:@"起运地" andFont:FontOfSize13 andColor:FieldPlacerColor];
    [_backview1 addSubview:cityName1];
    [cityName1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(SmallImage1.mas_right).with.offset(16*kWidth);
        make.centerY.mas_equalTo(_backview1.mas_top).with.offset(21.5*kHeight);
    }];
    UIView * viewSline1 = [[UIView alloc]init];
    viewSline1.backgroundColor = LineGrayColor;
    [_backview1 addSubview:viewSline1];
    [viewSline1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(SmallImage1.mas_top);
        make.left.mas_equalTo(_backview1.mas_left).with.offset(40*kWidth);
        make.bottom.mas_equalTo(SmallImage1.mas_bottom);
        make.width.mas_equalTo(0.5);
    }];
    UIView * viewHline = [[UIView alloc]init];
    viewHline.backgroundColor = LineGrayColor;
    [_backview1 addSubview:viewHline];
    [viewHline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_backview1.mas_top).with.offset(43*kHeight);
        make.left.mas_equalTo(_backview1.mas_left).with.offset(18*kWidth);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
    UIImageView * RightImage1 = [[UIImageView alloc]init];
    RightImage1.image = [UIImage imageNamed:@"common_list_arrows_more"];
    [_backview1 addSubview:RightImage1];
    [RightImage1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backview1.mas_right).with.offset(-15*kWidth);
        make.centerY.mas_equalTo(SmallImage1.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(8*kWidth, 13*kHeight));
    }];
    
    _address1 = [com createUIlabel:@"" andFont:FontOfSize13 andColor:BlackTitleColor];
    [_backview1 addSubview:_address1];
    [_address1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(RightImage1.mas_left).with.offset(-10*kWidth);
        make.centerY.mas_equalTo(SmallImage1.mas_centerY);
    }];

    UIImageView * SmallImage2 = [[UIImageView alloc]init];
    SmallImage2.image = [UIImage imageNamed:@"Address_Cell_shouche"];
    [_backview1 addSubview:SmallImage2];
    [SmallImage2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left).with.offset(20*kWidth);
        make.centerY.mas_equalTo(_backview1.mas_top).with.offset(64.5*kHeight);
        make.size.mas_equalTo(CGSizeMake(12*kWidth, 16*kHeight));
    }];
    
    UIButton * btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn1 addTarget:self action:@selector(pressBtnArea:) forControlEvents:UIControlEventTouchUpInside];
    btn1.tag = AddressBtnTag;
    [_backview1 addSubview:btn1];
    [btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left);
        make.centerY.mas_equalTo(SmallImage1.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
    }];
    
    UILabel * cityName2 = [com createUIlabel:@"目的地" andFont:FontOfSize13 andColor:FieldPlacerColor];
    [_backview1 addSubview:cityName2];
    [cityName2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(SmallImage1.mas_right).with.offset(16*kWidth);
        make.centerY.mas_equalTo(_backview1.mas_top).with.offset(64.5*kHeight);
    }];
    
    UIView * viewSline2 = [[UIView alloc]init];
    viewSline2.backgroundColor = LineGrayColor;
    [_backview1 addSubview:viewSline2];
    [viewSline2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(SmallImage2.mas_top);
        make.left.mas_equalTo(_backview1.mas_left).with.offset(40*kWidth);
        make.bottom.mas_equalTo(SmallImage2.mas_bottom);
        make.width.mas_equalTo(0.5);
    }];
    UIImageView * RightImage2 = [[UIImageView alloc]init];
    RightImage2.image = [UIImage imageNamed:@"common_list_arrows_more"];
    [_backview1 addSubview:RightImage2];
    [RightImage2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backview1.mas_right).with.offset(-15*kWidth);
        make.centerY.mas_equalTo(SmallImage2.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(8*kWidth, 13*kHeight));
    }];
    _address2 = [com createUIlabel:@"" andFont:FontOfSize13 andColor:BlackTitleColor];
    [_backview1 addSubview:_address2];
    [_address2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(RightImage2.mas_left).with.offset(-10*kWidth);
        make.centerY.mas_equalTo(SmallImage2.mas_centerY);
    }];

    UIButton * btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn2 addTarget:self action:@selector(pressBtnArea:) forControlEvents:UIControlEventTouchUpInside];
    btn2.tag = AddressBtnTag+1;
    [_backview1 addSubview:btn2];
    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left);
        make.centerY.mas_equalTo(SmallImage2.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
    }];
}

-(void)pressBtnArea:(UIButton*)sender{
    
    
    
    QueryAddressVC * address = [[QueryAddressVC alloc]init];
    
    if (sender.tag == AddressBtnTag) {
        address.addresstype = @"0";
    }else{
        address.addresstype = @"1";
    }
    
    [self.navigationController pushViewController:address animated:YES];
    
    
//    [backAddressDic setObject:@"" forKey:@"provinceName"];
//    [backAddressDic setObject:@"" forKey:@"provinceCode"];
//    [backAddressDic setObject:@"" forKey:@"cityName"];
//    [backAddressDic setObject:@"" forKey:@"cityCode"];
//    [backAddressDic setObject:@"" forKey:@"countyName"];
//    [backAddressDic setObject:@"" forKey:@"countyCode"];
//    
    

    //返回选中的
    address.callAddressBack=^(NSMutableDictionary * dicBlock,NSString * addresstype){

        if ([addresstype isEqualToString:@"0"]) {
            
            NSString * address1 = nil;
            if (![dicBlock[@"provinceName"] isEqualToString:dicBlock[@"cityName"]]) {
                
            address1 = [NSString stringWithFormat:@"%@%@%@",dicBlock[@"provinceName"],dicBlock[@"cityName"],dicBlock[@"countyName"]];
            }else{
            
            address1 = [NSString stringWithFormat:@"%@%@",dicBlock[@"provinceName"],dicBlock[@"countyName"]];
        }
            _address1.text = address1;
            _citycode1 = dicBlock[@"countyCode"];
        }else{
            
            NSString * address2 = nil;
            if (![dicBlock[@"provinceName"] isEqualToString:dicBlock[@"cityName"]]) {
                
                address2 = [NSString stringWithFormat:@"%@%@%@",dicBlock[@"provinceName"],dicBlock[@"cityName"],dicBlock[@"countyName"]];
            }else{
                
                address2 = [NSString stringWithFormat:@"%@%@",dicBlock[@"provinceName"],dicBlock[@"countyName"]];
            }
            _address2.text = address2;
            _citycode2 = dicBlock[@"countyCode"];

        }
    };
    
}

- (void)selectAddress:(id)sender{
    
    UIPickerView *pick = sender;
    NSInteger row = [pick selectedRowInComponent:0];
    NSDictionary *dict = [_provienceArray objectAtIndex:row];
    NSString *provinceName = MySetValue(dict, @"provinceName");//省名称
    NSString *provinceCode = MySetValue(dict, @"provinceCode");//省编码
    
    NSInteger row1 = [pick selectedRowInComponent:1];
    NSDictionary *dict1 = [_cityArray objectAtIndex:row1];
    NSString *cityName = MySetValue(dict1, @"cityName");//市名称
    NSString *citycode = MySetValue(dict1, @"cityCode");//市编码
    
    NSString *provinceName1 = [NSString stringWithFormat:@"%@",provinceName];
    NSString *cityName1 = [NSString stringWithFormat:@"%@",cityName];
    
    NSString * btnName = nil;
    if ([provinceName1 isEqualToString:cityName1]) {
        btnName = [NSString stringWithFormat:@"%@",provinceName1];
    }else{
        btnName = [NSString stringWithFormat:@"%@ %@",provinceName1,cityName1];
    }

    if (pick.tag == 500) {
        _address1.text = btnName;
        _citycode1 = citycode;
    }else{
        _address2.text = btnName;
        _citycode2 = citycode;
    }
}

-(void)createViewBack2{
    
    UIView * carView = [[UIView alloc]init];
    carView.backgroundColor = headerBackViewColor;
    [_backview2 addSubview:carView];
    [carView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview2.mas_left);
        make.top.mas_equalTo(_backview2.mas_top);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
    }];
    UIView * Carline = [[UIView alloc]init];
    Carline.backgroundColor = LineGrayColor;
    [carView addSubview:Carline];
    [Carline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(carView.mas_bottom);
        make.left.mas_equalTo(carView.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    UILabel * CarModel = [com createUIlabel:@"车辆信息" andFont:FontOfSize14 andColor:BlackTitleColor];
    [carView addSubview:CarModel];
    [CarModel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(carView.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(carView.mas_top).with.offset(21.5*kHeight);
    }];
    
    UIButton * warnBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [warnBtn setImage:[UIImage imageNamed:@"warn"] forState:UIControlStateNormal];
    [warnBtn addTarget:self action:@selector(pressWarnBtn) forControlEvents:UIControlEventTouchUpInside];
    [_backview2 addSubview:warnBtn];
    [warnBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backview2.mas_right).with.offset(-25*kWidth);
        make.centerY.equalTo(CarModel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(22*kWidth, 22*kHeight));
    }];
    
    NSArray * carInforArr = @[@"标准轿车",
                              @"普通SUV",
                              @"大型SUV",
                              @"超大型车"];
    
    for (int i=1; i<5; i++) {
        
        UIImageView * image = [[UIImageView alloc]init];
        image.image = [UIImage imageNamed:[NSString stringWithFormat:@"car_infor_imges_%d",i]];
        image.contentMode = UIViewContentModeScaleAspectFit;
        [_backview2 addSubview:image];
        [image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backview2.mas_left).with.offset((i-1)*Main_Width/4+14*kWidth);
            make.bottom.equalTo(_backview2.mas_top).with.offset(90*kHeight);
        }];
        UILabel * label = [com createUIlabel:carInforArr[i-1] andFont:FontOfSize12 andColor:BtnTitleColor];
        
        [_carInforNameArr addObject:label];
        
        [_backview2 addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(image.mas_centerX);
            make.top.equalTo(image.mas_bottom).with.offset(8*kHeight);
        }];
        
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = 100+i;
        [btn addTarget:self action:@selector(pressInforBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_carInforBtnArr addObject:btn];
        [_backview2 addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backview2.mas_left).with.offset((i-1)*Main_Width/4+14*kWidth);
            make.top.equalTo(_backview2.mas_top).with.offset(43*kHeight);
            make.size.mas_equalTo(CGSizeMake(Main_Width/4, 80*kHeight));
        }];
        if (i==1) {
            label.textColor = YellowColor;
            btn.selected = YES;
        }
    }
    
    
    UIView * Timeline = [[UIView alloc]init];
    Timeline.backgroundColor = LineGrayColor;
    [_backview2 addSubview:Timeline];
    [Timeline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_backview2.mas_top).with.offset(129*kHeight);
        make.left.mas_equalTo(_backview2.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
    
    UILabel * carMoney = [com createUIlabel:@"车辆估值" andFont:FontOfSize12 andColor:BtnTitleColor];
    [_backview2 addSubview:carMoney];
    [carMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(Timeline.mas_bottom).with.offset(21.5*kHeight);
    }];
    
    _CarPromptField =[self createFieldMoney:@"" andTag:1 andFont:FontOfSize14];
    _CarPromptField.tag = 1000;
    _CarPromptField.textAlignment = NSTextAlignmentRight;
    [_CarPromptField addTarget:self action:@selector(safeFieldWithText:) forControlEvents:UIControlEventEditingChanged];
    _CarPromptField.textAlignment = NSTextAlignmentRight;
    _CarPromptField.placeholder = @"必填(用于保险费计算)";
    _CarPromptField.returnKeyType = UIReturnKeyDone;
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, FieldPlacerColor.CGColor);
    _CarPromptField.font = Font(FontOfSize12);
    _CarPromptField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [_backview addSubview:_CarPromptField];
    
    UILabel * promptLabel = [com createUIlabel:@"万元" andFont:FontOfSize12 andColor:BtnTitleColor];
    [_backview2 addSubview:promptLabel];

    
    UIView * Timeline1 = [[UIView alloc]init];
    Timeline1.backgroundColor = LineGrayColor;
    [_backview2 addSubview:Timeline1];
    [Timeline1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(Timeline.mas_bottom).with.offset(43*kHeight);
        make.left.mas_equalTo(_backview2.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];


//    NSArray * arr = @[@"上门提车 (收取相关费用)",@"送车上门 (收取相关费用)"];
    NSArray * arr = @[@"上门提车",
                      @"送车上门"];

    for (int i=0; i<arr.count; i++) {
        UIView * view = [[UIView alloc]init];
        view.backgroundColor = WhiteColor;
        view.frame = CGRectMake(0, 0, Main_Width, 43*kHeight);
        [_backview2 addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(Timeline1.mas_bottom).with.offset(43*i*kHeight);
            make.left.mas_equalTo(_backview2.mas_left);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
        }];

        
        UILabel * label = [com createUIlabel:@"" andFont:FontOfSize12 andColor:BtnTitleColor];
        [view addSubview:label];
        if (i==0) {
            UIView * line = [[UIView alloc]init];
            line.backgroundColor = LineGrayColor;
            [view addSubview:line];
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(view.mas_bottom);
                make.left.equalTo(view.mas_left).with.offset(0*kWidth);
                make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
            }];
        }
        NSString * string = [NSString stringWithFormat:@"%@",arr[i]];
//        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
//        //上门提车
//        [str addAttribute:NSForegroundColorAttributeName value:BtnTitleColor range:NSMakeRange(0,5)];
//        [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:FontOfSize14*kWidth] range:NSMakeRange(0, 5)];
//        //(收取相关费用)
//        [str addAttribute:NSForegroundColorAttributeName value:FieldPlacerColor range:NSMakeRange(5,8)];
//        [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:FontOfSize14*kWidth] range:NSMakeRange(5, 8)];
//        label.attributedText = str;
        label.text = string;
        [view  addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(view.mas_centerY);
            make.left.equalTo(view.mas_left).with.offset(18*kWidth);
        }];
        
        SevenSwitch *mySwitch = [[SevenSwitch alloc] initWithFrame:CGRectZero];
        mySwitch.tag = InforSwitchTag+i;
        mySwitch.center = CGPointMake(self.view.bounds.size.width-40*kWidth, view.bounds.size.height * 0.5);
        [mySwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
        mySwitch.on = NO;
        //        NSLog(@"height%fwidth%f",mySwitch.frame.size.height,mySwitch.frame.size.width);
        mySwitch.onColor = YellowColor;
        [view  addSubview:label];
        [view addSubview:mySwitch];

        if (i==0) {
            [promptLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(mySwitch.mas_right);
                make.centerY.mas_equalTo(carMoney.mas_centerY);
            }];
            [_CarPromptField mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(carMoney.mas_centerY).with.offset(0.5*kHeight);
                make.right.mas_equalTo(promptLabel.mas_left);
                make.width.mas_equalTo(Main_Width-175*kWidth);
                make.height.mas_equalTo(43*kHeight);
            }];

            
        }
    }
    
    
    
    
}


-(void)switchChanged:(UISwitch*)mySwitch{
    if (mySwitch.tag == InforSwitchTag) {
        if (mySwitch.on) {
            NSLog(@"1开");
            _isPick = @"1";
//            [dicUrl setObject:@"1" forKey:@"ispick"];
        }else{
            NSLog(@"1关");
            _isPick = @"0";

//            [dicUrl setObject:@"0" forKey:@"ispick"];
        }
    }else{
        if (mySwitch.on) {
            NSLog(@"2开");
            _isDeliv = @"1";
//            [dicUrl setObject:@"1" forKey:@"isdeliv"];
        }else{
            NSLog(@"2关");
            _isDeliv = @"0";
//            [dicUrl setObject:@"0" forKey:@"isdeliv"];
        }
    }
}



-(void)safeFieldWithText:(UITextField*)field{
    
    //    estvalue
    if ([self textField:field shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:@""]) {
        _estValue = field.text;
    }
}
//点击return按钮 键盘隐藏 这是协议中的方法
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //键盘隐藏 也就是让键盘取消第一响应者身份
    [textField resignFirstResponder];
    return YES;
}


-(void)pressInforBtn:(UIButton*)sender{
    
    
    NSInteger index = sender.tag - 101;
    if (!sender.selected) {
        for (int i=0; i<_carInforBtnArr.count; i++) {

            UIButton * btn = _carInforBtnArr[i];
            btn.selected = NO;
            UILabel * label = _carInforNameArr[i];
            label.textColor = BtnTitleColor;
        }
    }
    sender.selected = YES;
    UILabel * label = _carInforNameArr[index];
    label.textColor = YellowColor;
    //标准轿车:S, 标准SUV:M, 大型SUV:L, 超大型车:EX

    NSArray * cartype = @[@"S",
                          @"M",
                          @"L",
                          @"EX"];
    _carType = cartype[index];
    
}




-(void)createCarTableView{
    
    
//    carTableView = [[UITableView alloc]init];
//    [carTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//    carTableView.tag = carTableViewTag;
//    carTableView.scrollEnabled = NO;
//    carTableView.delegate = self;
//    carTableView.dataSource = self;
//
    
    [_backview2 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left);
        make.top.mas_equalTo(_backview1.mas_bottom).with.offset(10*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 86*kHeight+43*6*kHeight));
    }];
    
    carInfor = [[NSMutableArray alloc]init];
    for (int i = 0;i<4;i++) {
        
        if (i==0) {
            [carInfor addObject:@"1"];
        }else{
            [carInfor addObject:@"0"];
        }
    }
}
-(void)createViewBack3{
    
    UIView * timeView = [[UIView alloc]init];
    timeView.backgroundColor = headerBackViewColor;
    [_backview3 addSubview:timeView];
    [timeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview3.mas_left);
        make.top.mas_equalTo(_backview3.mas_top);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
    }];
    
    UIView * Timeline = [[UIView alloc]init];
    Timeline.backgroundColor = LineGrayColor;
    [timeView addSubview:Timeline];
    [Timeline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(timeView.mas_bottom);
        make.left.mas_equalTo(timeView.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    

    
    UILabel * TimeLabel = [com createUIlabel:@"时间" andFont:FontOfSize14 andColor:BlackTitleColor];
    [timeView addSubview:TimeLabel];
    [TimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeView.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(timeView.mas_top).with.offset(21.5*kHeight);
    }];
    
    UIImageView * Timeimage = [[UIImageView alloc]init];
    Timeimage.image = [UIImage imageNamed:@"Address_Cell_songda"];
    [_backview3 addSubview:Timeimage];
    [Timeimage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview3.mas_left).with.offset(18*kWidth);
        make.centerY.equalTo(_backview3.mas_top).with.offset(64.5*kHeight);
        make.size.mas_equalTo(CGSizeMake(17*kWidth, 13*kHeight));
    }];
    UILabel * timeLabel = [com createUIlabel:@"提车时间" andFont:FontOfSize12 andColor:BtnTitleColor];
    [_backview3 addSubview:timeLabel];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(Timeimage.mas_right).with.offset(10*kWidth);
        make.centerY.equalTo(_backview3.mas_top).with.offset(64.5*kHeight);
    }];
    
    UIButton * btnDate = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDate setTitle:_adate forState:UIControlStateNormal];
    [btnDate setTitleColor:BtnTitleColor forState:UIControlStateNormal];
    [btnDate addTarget:self action:@selector(pressTimeBtn:) forControlEvents:UIControlEventTouchUpInside];
    btnDate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    btnDate.tag = TimeBtnTag;
    btnDate.titleLabel.font = Font(FontOfSize12);
    [_backview3 addSubview:btnDate];
    [btnDate mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeLabel.mas_right).with.offset(15*kWidth);
        make.centerY.equalTo(timeLabel.mas_centerY);
        make.width.mas_equalTo(Main_Width/3);
    }];
    
    NSString * btnlabel = nil;
    if ([_atime isEqualToString:@"0"]) {
        btnlabel = @"0点-12点";
    }else{
        btnlabel = @"12点-24点";
    }

    UIButton * btnTime = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnTime setTitle:btnlabel forState:UIControlStateNormal];
    [btnTime setTitleColor:BtnTitleColor forState:UIControlStateNormal];
    [btnTime addTarget:self action:@selector(pressPickCarTimeBtn:) forControlEvents:UIControlEventTouchUpInside];
    btnTime.tag = TimeBtnTag + 1;
    btnTime.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btnTime.titleLabel.font = Font(FontOfSize12);
//    btnTime.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_backview3 addSubview:btnTime];
    [btnTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(btnDate.mas_right).with.offset(15*kWidth);
        make.centerY.equalTo(timeLabel.mas_centerY);
        make.width.mas_equalTo(Main_Width/3);
    }];
    
//    _backview30 = [[UIView alloc]init];
//    _backview30.backgroundColor = [UIColor cyanColor];
//    _backview30.hidden = YES;
//    [_backview3 addSubview:_backview30];
//    [_backview30 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_backview.mas_left);
//        make.top.equalTo(_backview3.mas_top).with.offset(86*kHeight);
//        make.size.mas_equalTo(CGSizeMake(Main_Width, 86*kHeight));
//    }];
    
    UIView * Timeline00 = [[UIView alloc]init];
    Timeline00.backgroundColor = LineGrayColor;
    [_backview3 addSubview:Timeline00];
    [Timeline00 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_backview3.mas_top).with.offset(86*kHeight);
        make.left.mas_equalTo(_backview3.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    UIView * Timeline0 = [[UIView alloc]init];
    Timeline0.backgroundColor = LineGrayColor;
    Timeline0.hidden = YES;
    Timeline0.tag = 1111;
    [_backview3 addSubview:Timeline0];
    [Timeline0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_backview3.mas_top).with.offset(129*kHeight);
        make.left.mas_equalTo(_backview3.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    arriveTimeimage = [[UIImageView alloc]init];
    arriveTimeimage.image = [UIImage imageNamed:@"icon_infor_Arrival_date_imges"];
    arriveTimeimage.hidden = YES;
    [_backview3 addSubview:arriveTimeimage];
    [arriveTimeimage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview3.mas_left).with.offset(18*kWidth);
        make.centerY.equalTo(Timeimage.mas_centerY).with.offset(43*kHeight);
        make.size.mas_equalTo(CGSizeMake(18*kWidth, 18*kHeight));
    }];

    
    arriveTime = [com createUIlabel:@"预计到达日期" andFont:FontOfSize12 andColor:BtnTitleColor];
    arriveTime.hidden = YES;
    [_backview3 addSubview:arriveTime];
    
    [arriveTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeLabel.mas_left);
        make.centerY.equalTo(Timeline00.mas_top).with.offset(21.5*kHeight);
    }];
    
    _arriveTimeLabel = [com createUIlabel:@"" andFont:FontOfSize12 andColor:BtnTitleColor];
    
    [_backview3 addSubview:_arriveTimeLabel];
    [_arriveTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(btnDate.mas_centerX);
        make.centerY.equalTo(arriveTime.mas_centerY);
    }];

    promptBtn = [self createBtn:@"" andBorderColor:nil andFont:13.0 andTitleColor:nil andBackColor:nil];
    promptBtn.hidden = YES;
    [promptBtn setImage:[UIImage imageNamed:@"jinggao"] forState:UIControlStateNormal];
    promptBtn.layer.borderWidth = 0;
    promptBtn.layer.cornerRadius = 7.5*kHeight;
    [_backview3 addSubview:promptBtn];
    [promptBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_arriveTimeLabel.mas_right).with.offset(25*kWidth);
        make.left.equalTo(btnTime);
        make.centerY.mas_equalTo(arriveTime.mas_centerY);
        make.size.mas_equalTo(CGSizeMake( 16*kWidth, 16*kHeight));
    }];
    
    UIButton * bigPromptBtn = [self createBtn:@"" andBorderColor:nil andFont:13.0 andTitleColor:nil andBackColor:nil];
    [bigPromptBtn addTarget:self action:@selector(pressPromptBtn:) forControlEvents:UIControlEventTouchUpInside];
    bigPromptBtn.layer.borderWidth = 0;
    [_backview3 addSubview:bigPromptBtn];
    [bigPromptBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_arriveTimeLabel.mas_right).with.offset(25*kWidth);
        make.centerY.mas_equalTo(arriveTime.mas_centerY);
        make.size.mas_equalTo(CGSizeMake( 50*kWidth, 30*kHeight));
    }];
    distanceName = [com createUIlabel:@"里程数" andFont:FontOfSize12 andColor:BtnTitleColor];
    distanceName.hidden = YES;
    [_backview3 addSubview:distanceName];
    [distanceName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(arriveTime.mas_left);
        make.centerY.equalTo(arriveTime.mas_centerY).with.offset(43*kHeight);
    }];

    
    _distance = [com createUIlabel:@"" andFont:FontOfSize12 andColor:BtnTitleColor];
    [_backview3 addSubview:_distance];
    [_distance mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(btnDate.mas_centerX);
        make.centerY.equalTo(arriveTime.mas_centerY).with.offset(43*kHeight);
    }];


    
    
    _backview31 = [[UIView alloc]init];
    _backview31.backgroundColor = WhiteColor;
    [_backview addSubview:_backview31];
    [_backview31 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left);
        make.top.equalTo(_backview3.mas_bottom).with.offset(10*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 86*kHeight));
    }];
    _backview31.hidden = YES;

    
    UIView * Timeline1 = [[UIView alloc]init];
    Timeline1.backgroundColor = LineGrayColor;
    [_backview31 addSubview:Timeline1];
    [Timeline1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_backview31.mas_top).with.offset(43*kHeight);
        make.left.mas_equalTo(_backview31.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    
    
    UILabel * onePrice = [com createUIlabel:@"单台价格" andFont:FontOfSize12 andColor:BtnTitleColor];
    UILabel * allPrice = [com createUIlabel:@"整车价格" andFont:FontOfSize12 andColor:BtnTitleColor];

    [_backview31 addSubview:onePrice];
    [_backview31 addSubview:allPrice];

    [onePrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview31.mas_left).with.offset(18*kWidth);
        make.centerY.equalTo(_backview31.mas_top).with.offset(21.5*kHeight);
    }];
    [allPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview31.mas_left).with.offset(18*kWidth);
        make.centerY.equalTo(_backview31.mas_top).with.offset(43*kHeight+21.5*kHeight);
    }];

    
    
    _unitPriceLabel = [com createUIlabel:@"" andFont:FontOfSize12 andColor:BlackTitleColor];
    _disUnitPriceLabel = [com createUIlabel:@"" andFont:FontOfSize12 andColor:YellowColor];
    _entirePriceLabel = [com createUIlabel:@"" andFont:FontOfSize12 andColor:BlackTitleColor];
    _disEntirePriceLabel = [com createUIlabel:@"" andFont:FontOfSize12 andColor:YellowColor];
    


    [_backview31 addSubview:_unitPriceLabel];
    [_backview31 addSubview:_disUnitPriceLabel];
    [_backview31 addSubview:_entirePriceLabel];
    [_backview31 addSubview:_disEntirePriceLabel];

    _descLabel = [com createUIlabel:@"" andFont:FontOfSize9 andColor:YellowColor];
    [_backview31 addSubview:_descLabel];
    [_descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_disEntirePriceLabel.mas_right);
        make.bottom.equalTo(_backview31.mas_bottom).with.offset(-5*kHeight);
    }];

    
    _unitPriceLine = [[UIView alloc]init];
    _entirePriceLine = [[UIView alloc]init];
    _unitPriceLine.backgroundColor = fontGrayColor;
    _entirePriceLine.backgroundColor = fontGrayColor;
    
    _unitPriceLine.hidden = YES;
    _entirePriceLine.hidden = YES;
    
    [_unitPriceLabel addSubview:_unitPriceLine];
    [_unitPriceLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_unitPriceLabel.mas_left);
        make.right.equalTo(_unitPriceLabel.mas_right);
        make.centerY.mas_equalTo(_unitPriceLabel.mas_centerY);
        make.height.mas_equalTo(1);
    }];
    
    
    [_entirePriceLabel addSubview:_entirePriceLine];
    [_entirePriceLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_entirePriceLabel.mas_left);
        make.right.equalTo(_entirePriceLabel.mas_right);
        make.centerY.mas_equalTo(_entirePriceLabel.mas_centerY);
        make.height.mas_equalTo(1);
    }];

    [_unitPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(onePrice.mas_right).with.offset(52*kWidth);
        make.centerY.equalTo(onePrice.mas_centerY);
    }];
    [_disUnitPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_unitPriceLabel.mas_right).with.offset(52*kWidth);
        make.centerY.equalTo(_unitPriceLabel.mas_centerY);
    }];
    [_entirePriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(allPrice.mas_right).with.offset(54*kWidth);
        make.centerY.equalTo(allPrice.mas_centerY);
    }];
    [_disEntirePriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_entirePriceLabel.mas_right).with.offset(52*kWidth);
        make.centerY.equalTo(_entirePriceLabel.mas_centerY);
    }];
    
}

-(void)pressPromptBtn:(UIButton*)btn{
    
    
    _warnView = [self createWarnBack:2];
    // 当前顶层窗口
    UIWindow *window = [[UIApplication sharedApplication].windows lastObject];
    // 添加到窗口
    [window addSubview:_warnView];
}

-(void)createWarnView{
    
    _backview4 = [[UIView alloc]init];
    _backview4.backgroundColor = GrayColor;

    _backview4.hidden = YES;
    [_backview addSubview:_backview4];
    [_backview4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left);
        make.top.equalTo(_backview31.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 80*kHeight));
    }];
    
    
    UIImageView * imageWarn = [[UIImageView alloc]init];
    imageWarn.image = [UIImage imageNamed:@"warn1"];
    [_backview4 addSubview:imageWarn];
    
    [imageWarn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview4.mas_left).with.offset(25*kWidth);
        make.centerY.equalTo(_backview4.mas_top).with.offset(24*kHeight);
        make.size.mas_equalTo(CGSizeMake(15*kWidth, 15*kHeight));
    }];

    UILabel * label1 = [com createUIlabel:@"温馨提示:" andFont:FontOfSize11 andColor:shippingTitleColor];
    UILabel * label2 = [[UILabel alloc]init];
    label2.text = @"次日达不支持上门提车和送车上门，急速达不支持偏远地区(西藏、新疆、云南、海南、内蒙部分地区)。";
    label2.textColor = shippingTitleColor;
    label2.numberOfLines = 2;
    label2.font = Font(FontOfSize11)
    UILabel * label3 = [[UILabel alloc]init];
//    label3.text = @"运费不包括提车费用、交车费用、保险、发票等附加服务费用,实际运价根据您真实托运需求会有所变化。";
    
    label3.text = @"此处运费为不含税价格，下单时若需发票会另行收取开票费用。";
    label3.numberOfLines = 2;
    label3.font = Font(FontOfSize11)
    label3.textColor = shippingTitleColor;

    [_backview4 addSubview:label1];
//    [_backview4 addSubview:label2];
    [_backview4 addSubview:label3];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageWarn.mas_right).with.offset(4*kWidth);
        make.centerY.equalTo(_backview4.mas_top).with.offset(24*kHeight);
    }];

//    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(imageWarn.mas_left);
//        make.top.equalTo(label1.mas_bottom).with.offset(6*kHeight);
//        make.width.mas_equalTo(265*kWidth);
//    }];
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageWarn.mas_left);
        make.top.equalTo(label1.mas_bottom).with.offset(6*kHeight);
        make.width.mas_equalTo(265*kWidth);
    }];
    
    _QueryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_QueryBtn setTitle:@"查询" forState:UIControlStateNormal];
    [_QueryBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
    [_QueryBtn addTarget:self action:@selector(pressQueryBtn) forControlEvents:UIControlEventTouchUpInside];
    _QueryBtn.layer.cornerRadius = 5;
    _QueryBtn.layer.borderWidth = 0.5;
    _QueryBtn.layer.borderColor = YellowColor.CGColor;
    _QueryBtn.backgroundColor = YellowColor;
    [_backview addSubview:_QueryBtn];
    [_QueryBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_backview.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(293*kWidth, 40*kHeight));
        make.top.mas_equalTo(_backview3.mas_bottom).with.offset(20*kHeight);
    }];
    
    _GoShippingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _GoShippingBtn.hidden = YES;
    [_GoShippingBtn setTitle:@"去下单" forState:UIControlStateNormal];
    [_GoShippingBtn setTitleColor:YellowColor forState:UIControlStateNormal];
    [_GoShippingBtn addTarget:self action:@selector(pressGoShippingBtn) forControlEvents:UIControlEventTouchUpInside];
    _GoShippingBtn.layer.cornerRadius = 5;
    _GoShippingBtn.layer.borderWidth = 0.5;
    _GoShippingBtn.layer.borderColor = YellowColor.CGColor;
    _GoShippingBtn.backgroundColor = GrayColor;
    [_backview addSubview:_GoShippingBtn];
    [_GoShippingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_backview.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(293*kWidth, 40*kHeight));
        make.top.mas_equalTo(_QueryBtn.mas_bottom).with.offset(15*kHeight);
    }];

}

-(void)pressQueryBtn{
   
    [self createDate];
}
-(void)pressGoShippingBtn{
   
    PlaceOrderVC * place = [[PlaceOrderVC alloc]init];
    [self.navigationController pushViewController:place animated:YES];
}


-(void)pressCarBtnType1:(UIButton*)sender{
    

    _carType = @"M";
    UIImageView * image1 = (UIImageView*)[_backview2 viewWithTag:ImageSeleteTag];
    UIImageView * image2 = (UIImageView*)[_backview2 viewWithTag:ImageSeleteTag+1];

    image1.image = [UIImage imageNamed:@"selected"];
    image2.image = [UIImage imageNamed:@"unselect"];
}
-(void)pressCarBtnType2:(UIButton*)sender{
   
    _carType = @"L";
    UIImageView * image1 = (UIImageView*)[_backview2 viewWithTag:ImageSeleteTag];
    UIImageView * image2 = (UIImageView*)[_backview2 viewWithTag:ImageSeleteTag+1];
    
    image2.image = [UIImage imageNamed:@"selected"];
    image1.image = [UIImage imageNamed:@"unselect"];
}

-(void)pressWarnBtn{
    

    _warnView = [self createWarnBack:1];
    // 当前顶层窗口
    UIWindow *window = [[UIApplication sharedApplication].windows lastObject];
    // 添加到窗口
    [window addSubview:_warnView];

}

-(void)pressWarnBack{
    
    [_warnView removeFromSuperview];
    
}
-(void)WarnBackTouch{
    
    [_warnView removeFromSuperview];
    
}
-(void)pressTimeBtn:(UIButton*)sender{
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    ActionSheetPicker *actionSheet = [[ActionSheetPicker alloc] initWithTarget:self successAction:@selector(selectOK:) cancelAction:nil origin:datePicker];
    datePicker.datePickerMode=UIDatePickerModeDate;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    //最小年月日（小于它，自动调回）
    NSDate* minDate = [NSDate date];
    //最大年月日（今天）
    NSDate* maxDate = [formatter dateFromString:@"2018-01-01"];
    datePicker.minimumDate = minDate;
    datePicker.maximumDate = maxDate;
    CGRect pickerFrame = CGRectMake(0, 40, actionSheet.viewSize.width, 216);
    [datePicker setFrame:pickerFrame];
    datePicker.datePickerMode = UIDatePickerModeDate;
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];//设置为简体中文显示
    datePicker.locale = locale;
    actionSheet.pickerView = datePicker;
    actionSheet.title = @"";
    UIButton *leftBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setTitle:@"取消"forState:UIControlStateNormal];
    leftBtn.frame=CGRectMake(0, 0, 50, 45);
    [leftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *leftIte=[[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    
    UIButton *rightBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
    rightBtn.frame=CGRectMake(0, 0, 50, 45);
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightIte=[[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    [actionSheet setDoneButton:rightIte];
    [actionSheet setCancelButton:leftIte];
    [actionSheet showActionSheetPicker];
}

-(void)pressPickCarTimeBtn:(UIButton*)btn{
    
    UIPickerView *education = [[UIPickerView alloc] init];
    ActionSheetPicker *actionSheet=[[ActionSheetPicker alloc]initWithTarget:self successAction:@selector(selectTime:) cancelAction:nil origin:education];
    CGRect pickerFrame = CGRectMake(0, 40, actionSheet.viewSize.width, 216);
    [education setFrame:pickerFrame];
    education.tag = PickCarTimeTag; //300
    education.delegate = self;
    education.dataSource = self;
    actionSheet.pickerView = education;
    actionSheet.title = @"标题";
    
    UIButton *leftBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setTitle:@"取消"forState:UIControlStateNormal];
    leftBtn.frame=CGRectMake(0, 0, 50, 45);
    [leftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *leftIte=[[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    
    UIButton *rightBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
    rightBtn.frame=CGRectMake(0, 0, 50, 45);
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightIte=[[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    [actionSheet setDoneButton:rightIte];
    [actionSheet setCancelButton:leftIte];
    [actionSheet showActionSheetPicker];
    
}

- (void)selectOK:(id)sender{
    if ([sender isKindOfClass:[UIDatePicker class]]) {
        
        UIDatePicker *picker = sender;
        NSDate * date = [picker date];
        //显示2015-07-23 的日期
        NSString *time = [[date description] substringWithRange:NSMakeRange(0, 10)];
        NSString *string = [NSString stringWithFormat:@"%@",time];
        UIButton * btn = (UIButton*)[_backview viewWithTag:TimeBtnTag];
        
        NSDate *  senddate=[NSDate date];
        
        NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
        [dateformatter setDateFormat:@"yyyy-MM-dd"];
        NSString * data = [dateformatter stringFromDate:senddate];
        
        
        NSDate *date1 = [NSDate date]; // 获得时间对象
        NSDateFormatter  *dateformatterTime=[[NSDateFormatter alloc] init];
        [dateformatterTime setDateFormat:@"HH:mm:ss"];
        NSString * string1 = [dateformatterTime stringFromDate:date1];
        int  b = [[string1 substringWithRange:NSMakeRange(0,2)] intValue];
        
        NSString * ati = nil;
        
        if (b>=0 &&b<=12) {
            //        _atime = @"0";
            ati = @"0";
        }else{
            //        _atime = @"1";
            ati = @"1";
        }
        
        UIButton * btn1 = (UIButton *)[_backview3 viewWithTag:TimeBtnTag+1];

        if ([Common compareDate:string withDate:data]) {
            [btn setTitle:data forState:UIControlStateNormal];
            if ([ati isEqualToString:@"0"]) {
                [btn1 setTitle:@"0点-12点" forState:UIControlStateNormal];
                _adate = data;
                _atime = @"0";
            }else{
                [btn1 setTitle:@"12点-24点" forState:UIControlStateNormal];
                _adate = data;
                _atime = @"1";
            }
            
        }else{
            [btn setTitle:string forState:UIControlStateNormal];
            _adate = string;
        }

    }
}
-(void)pressTimeBtn1:(UIButton *)sender{
    
    UIPickerView *education = [[UIPickerView alloc] init];
    ActionSheetPicker *actionSheet=[[ActionSheetPicker alloc]initWithTarget:self successAction:@selector(selectTime:) cancelAction:nil origin:education];
    CGRect pickerFrame = CGRectMake(0, 40, actionSheet.viewSize.width, 216);
    [education setFrame:pickerFrame];
    education.tag = sender.tag+2; //105 106
    education.delegate = self;
    education.dataSource = self;
    actionSheet.pickerView = education;
    actionSheet.title = @"标题";
    
    UIButton *leftBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setTitle:@"取消"forState:UIControlStateNormal];
    leftBtn.frame=CGRectMake(0, 0, 50, 45);
    [leftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *leftIte=[[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    
    UIButton *rightBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
    rightBtn.frame=CGRectMake(0, 0, 50, 45);
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightIte=[[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    [actionSheet setDoneButton:rightIte];
    [actionSheet setCancelButton:leftIte];
    [actionSheet showActionSheetPicker];
}

-(void)selectTime:(id)sender{
    
    UIPickerView *pick = sender;//105 106
    NSInteger row = [pick selectedRowInComponent:0];
    NSString *value=[_timeChoose objectAtIndex:row];
    NSString *education=[NSString stringWithFormat:@"%@",value];
    UIButton * btn = (UIButton *)[_backview3 viewWithTag:TimeBtnTag+1];
    UIButton * btn0 = (UIButton *)[_backview3 viewWithTag:TimeBtnTag];

    
    NSDate *date = [NSDate date]; // 获得时间对象
    NSDateFormatter  *dateformatterTime=[[NSDateFormatter alloc] init];
    [dateformatterTime setDateFormat:@"HH:mm:ss"];
    NSString * string = [dateformatterTime stringFromDate:date];
    int  b = [[string substringWithRange:NSMakeRange(0,2)] intValue];
    
    NSString * ati = nil;
    
    if (b>=0 &&b<=12) {
        //        _atime = @"0";
        ati = @"0";
    }else{
        //        _atime = @"1";
        ati = @"1";
    }

    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy-MM-dd"];
    NSString * data = [dateformatter stringFromDate:senddate];
    

    
    if ([data isEqualToString:btn0.titleLabel.text]&&[ati isEqualToString:@"1"]&&[value isEqualToString:@"0点-12点"]) {
        
        [self createUIAlertController:@"请选择正确时间"];
    }else{
        
        [btn setTitle:value forState:UIControlStateNormal];
        if ([value isEqualToString:@"0点-12点"]) {
            _atime = @"0";
        }else{
            _atime = @"1";
        }
    }
    

    
    
    NSLog(@"选择的时间点:%@",education);
}

#pragma mark UIPickerView dataSource

//点击每一行cell  跳出的pickerview  显示列数
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    
    
    if (pickerView.tag == 500 || pickerView.tag == 600) {
        return 2;
    }else{
        return 1;
    }
}

//显示pickerview 每个视图的行数
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{

    switch (pickerView.tag) {
        case 500:
            switch (component) {
                case 0:
                    return _provienceArray.count;
                    break;
                case 1:
                    return _cityArray.count;
                    break;
    
                default:
                    break;
            }
            break;
        case 600:
            switch (component) {
                case 0:
                    return _provienceArray.count;
                    break;
                case 1:
                    return _cityArray.count;
                    break;
                    
                default:
                    break;
            }
            break;
        case PickCarTimeTag:
            return _timeChoose.count;
            break;
        default:
            break;
    }
    
    return 0;
}
//显示每个pickerview  每行的具体内容
-(UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *lable  = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
    lable.adjustsFontSizeToFitWidth = YES;
    lable.textAlignment = NSTextAlignmentCenter;
    
    switch (pickerView.tag) {
        case 500:
            
            switch (component) {
                case 0:{
                    
                    NSDictionary *dict = [_provienceArray objectAtIndex:row];
                    lable.text = MySetValue(dict, @"provinceName");
                }
                    break;
                case 1:{
                    
                    NSDictionary *dict = [_cityArray objectAtIndex:row];
                    lable.text = MySetValue(dict, @"cityName");
                }
                    break;
                default:
                    break;
            }
            break;
        case 600:
            
            switch (component) {
                case 0:{
                    
                    NSDictionary *dict = [_provienceArray objectAtIndex:row];
                    lable.text = MySetValue(dict, @"provinceName");
                }
                    break;
                case 1:{
                    
                    NSDictionary *dict = [_cityArray objectAtIndex:row];
                    lable.text = MySetValue(dict, @"cityName");
                }
                    break;
                default:
                    break;
            }
            break;
        case PickCarTimeTag:
            lable.text = [_timeChoose objectAtIndex:row];
            break;
            
        default:
            break;
    }
    return lable;
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (pickerView.tag != PickCarTimeTag ) {
        NSInteger ss = [pickerView selectedRowInComponent:0];
        NSInteger dd = [pickerView selectedRowInComponent:1];
        
        switch (component) {
            case 0:
            {
                NSDictionary *dict = [_provienceArray objectAtIndex:row];
                NSString *code = MySetValue(dict, @"provinceName");
                if (code.length > 0)
                {
                    _cityArray = [AREA selectAreaWithFArea:code withTarea:nil withFarea:2];
                    [pickerView reloadComponent:1];
                    dd = 0;
                    NSDictionary *cityDict = [_cityArray objectAtIndex:dd];
                    NSString * cityFcode = MySetValue(cityDict, @"cityName");
                    if (cityFcode.length > 0)
                    {
                        //                    _areaArray = [AREA selectAreaWithFArea:code withTarea:cityFcode withFarea:3];
                        //                    [pickerView reloadComponent:2];
                    }
                }
            }
                break;
            case 1:
            {
                NSDictionary *dict = [_cityArray objectAtIndex:row];
                NSString *code = MySetValue(dict, @"cityName");
                
                NSDictionary * dics = [_provienceArray objectAtIndex:ss];
                NSString * str = MySetValue(dics, @"provinceName");
                NSLog(@"%@",code);
                
                NSLog(@"%@",str);
                if (code.length > 0) {
                    //                _areaArray = [AREA selectAreaWithFArea:str withTarea:code withFarea:3];
                    //                [pickerView reloadComponent:2];
                }
            }
                break;
            case 2:{
                
            }
                break;
                
            default:
                break;
        }
    }
}

//-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    NSString * str = @"string";
//    if (tableView.tag == tableviewTag) {
//        QueryShippingCell * cell =  [tableView dequeueReusableCellWithIdentifier:str];
//        if (cell ==nil) {
//            cell = [[QueryShippingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
//        }
//        
//        [cell showInfo:dataSouceArr[indexPath.row]];
//        
//        return cell;
//    }else{
//        
//        QueryCarCell * cell =  [tableView dequeueReusableCellWithIdentifier:str];
//
//        if (cell ==nil) {
//            cell = [[QueryCarCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
//        }
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//
//        cell.labelTitle.text = [NSString stringWithFormat:@"第%ld个",(long)indexPath.row];
//        if ([carInfor[indexPath.row] isEqualToString:@"1"]) {
//            cell.LeftImage.image = [UIImage imageNamed:@"selected"];
//        }else{
//            cell.LeftImage.image = [UIImage imageNamed:@"unselect"];
//        }
//        
//        if (indexPath.row == 3) {
//            cell.viewline.hidden = YES;
//        }
//        return cell;
//    }
//}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 43*kHeight;
//}
//
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    
//    
//    return  tableView.tag==tableviewTag ? dataSouceArr.count:4;
////    return 3;
//}

//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    
//    return  tableView.tag==tableviewTag ? 43*kHeight:0;
//
//}
//
//
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    
//    if (tableView.tag == carTableViewTag) {
//        
//        for (int i = 0; i<4; i++) {
//            [carInfor replaceObjectAtIndex:i withObject:@"0"];
//        }
//        [carInfor replaceObjectAtIndex:indexPath.row withObject:@"1"];
////        [carTableView reloadData];
//    }
//
//}
//
//-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    
//    UIView * view = [[UIView alloc]init];
//    view.backgroundColor = WhiteColor;
//    view.frame = CGRectMake(0, 0, Main_Width, 43*kHeight);
//    UIView * viewline = [[UIView alloc]init];
//    viewline.backgroundColor = GrayColor;
//    [view addSubview:viewline];
//    [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(view.mas_left).with.offset(12*kWidth);
//        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
//        make.top.mas_equalTo(view.mas_top);
//    }];
//    
//    UILabel * name = [com createUIlabel:@"时间" andFont:FontOfSize14 andColor:fontGrayColor];
//    UILabel * time = [com createUIlabel:@"运距(km)" andFont:FontOfSize14 andColor:fontGrayColor];
//    UILabel * price = [com createUIlabel:@"价格" andFont:FontOfSize14 andColor:fontGrayColor];
//    [view addSubview:name];
//    [view addSubview:time];
//    [view addSubview:price];
//    [name mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(view.mas_left).with.offset(70*kWidth);
//        make.centerY.mas_equalTo(view.mas_centerY);
//    }];
//    
//    [time mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(view.mas_centerX);
//        make.centerY.mas_equalTo(view.mas_centerY);
//    }];
//    [price mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(view.mas_right).with.offset(-35*kWidth);
//        make.centerY.mas_equalTo(view.mas_centerY);
//    }];
//    
//    return view;
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createUIAlertControllerWithTitle:(NSString*)HeaderTitle and:(NSString*)title
{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:HeaderTitle message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}
-(UIView*)createWarnBack:(int)number{
    
    UIView * view= [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, Main_height)];
    view.backgroundColor = RGBACOLOR(0, 0, 0, 0.3);
    view.userInteractionEnabled = YES;
    
    UITapGestureRecognizer * Tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(WarnBackTouch)];
    Tap.delegate = self;
    [view addGestureRecognizer:Tap];
    
    UIView * warnBackView = [[UIView alloc]init];
    warnBackView.backgroundColor = WhiteColor;
    warnBackView.layer.cornerRadius = 5;

    
    
    [view addSubview:warnBackView];
    [warnBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(view.mas_top).with.offset(130*kHeight);
        make.centerX.mas_equalTo(view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(250*kWidth, 300*kHeight));
    }];

    //503 × 190 pixels
    UIImageView * warnBackImage = [[UIImageView alloc]init];
    warnBackImage.image = [UIImage imageNamed:@"图层-0"];
    [warnBackView addSubview:warnBackImage];
    [warnBackImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(warnBackView.mas_centerX);
        make.top.mas_equalTo(warnBackView.mas_top);
        make.size.mas_equalTo(CGSizeMake(250*kWidth, 95*kHeight));
    }];
    
    UILabel * label1 = [com createUIlabel:@"温馨提示" andFont:FontOfSize14 andColor:WhiteColor];
    [warnBackImage addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(warnBackImage.mas_centerX);
        make.bottom.mas_equalTo(warnBackImage.mas_bottom).with.offset(-3*kHeight);
    }];
    
    
    UIView * line = [[UIView alloc]init];
    line.backgroundColor = GrayColor;
    [warnBackView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(warnBackView.mas_left);
        make.right.equalTo(warnBackView.mas_right);
        make.height.mas_equalTo(0.5*kHeight);
        make.top.mas_equalTo(warnBackView.mas_bottom).with.offset(-43*kHeight);
    }];
    
    
 
    UILabel * label2 = [[UILabel alloc]init];
    
    if (number == 1) {
        label2.text = @"车型:标准轿车(长*宽*高<=5*1.85*1.6,重量<=1.8t)普通SUV(长*宽*高<=4.9*1.9*1.7,重量<=2t)大型SUV(长*宽*高<=5.6*2.2*2,重量<=2.5t)超大型车(长*宽*高>5.6*2.2*2,重量>2.5t).\n\n提车时间默认当前日期当前时间所属的时间段,可以当前时间选择之后的任意日期的任意时间段;如果选择的时间段为12点-24点,预计到达时间往后延一天.";

    }else{
        label2.text = @"标准交付时间:运输距离<=500KM:提车1天,在途1天,第三天完成交车(下单时间晚于12点,提车时间从第二天起)运输距离>500KM:交付时间从提车时间起,共需(运距/500)天,最高不超过7天,偏远地区(西藏、新疆、云南、海南、内蒙部分地区)可能要视具体情况而定.\n\n提车时间默认当前日期当前时间所属的时间段,可以当前时间选择之后的任意日期的任意时间段;如果选择的时间段为12点-24点,预计到达时间往后延一天.";

        
    }
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:label2.text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:1*kHeight];//调整行间距
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [label2.text length])];
    label2.attributedText = attributedString;
    [label2 sizeToFit];
    
    label2.numberOfLines = 0;
    label2.font = Font(FontOfSize11);
    label2.textColor = littleBlackColor;
//    label2.textAlignment = NSTextAlignmentJustified;
    
    [warnBackView addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(warnBackView.mas_left).with.offset(8*kWidth);
        make.right.equalTo(warnBackView.mas_right).with.offset(-8*kHeight);
        make.height.mas_equalTo(160*kHeight);
        make.top.mas_equalTo(warnBackImage.mas_bottom).with.offset(1*kHeight);
    }];
    
    
    
    UILabel * label = [com createUIlabel:@"知道了" andFont:FontOfSize14 andColor:YellowColor];
    [warnBackView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(warnBackView.mas_centerX);
        make.centerY.mas_equalTo(line.mas_bottom).with.offset(21.5*kHeight);
    }];
    
    
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 10, 10);
    [btn addTarget:self action:@selector(pressWarnBack) forControlEvents:UIControlEventTouchUpInside];
    [warnBackView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(warnBackView.mas_left);
        make.right.mas_equalTo(warnBackView.mas_right);
        make.height.mas_equalTo(43*kHeight);
        make.bottom.mas_equalTo(warnBackView.mas_bottom);
    }];
    return view;
}

-(UITextField*)createFieldMoney:(NSString*)placeholder andTag:(NSInteger)tag andFont:(double)font{
    
    UITextField * field =[[UITextField alloc]init];
    //    field.frame = CGRectMake(94*kWidth, 0, Main_Width-200*kWidth, 43*kWidth);
    field.frame = CGRectMake(120*kWidth, 0, Main_Width-200*kWidth, 50*kWidth);
    field.delegate = self;
    field.userInteractionEnabled = YES;
    field.textAlignment = NSTextAlignmentLeft;
    field.placeholder = placeholder;
    return field;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if ([string isEqualToString:@"\n"])  //按会车可以改变
    {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    //1000000
    if ([toBeString length] > 7) {
        textField.text = [toBeString substringToIndex:7];
        [self createUIAlertController:@"整体估值输入不能超过1000000万"];
        return NO;
    }
    return YES;
}
-(void)createUIAlertController:(NSString*)title

{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}
-(UIButton*)createBtn:(NSString*)title andBorderColor:(UIColor*)BorderColor andFont:(CGFloat)Font andTitleColor:(UIColor*)TitleColor andBackColor:(UIColor*)BackColor {
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.layer.borderWidth = 0.5;
    btn.layer.cornerRadius = 5;
    btn.backgroundColor = BackColor;
    btn.layer.borderColor = BorderColor.CGColor;
    btn.titleLabel.font = Font(Font);
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:TitleColor forState:UIControlStateNormal];
    return btn;
}

/* 解决触摸手势和cell点击的冲突 */
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    if ([touch.view isEqual:_warnView]) {
        return YES;
    }
    return NO;
}

@end
