//
//  CDNewQueryAddressVC
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/11/17.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface CDNewQueryAddressVC : NavViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) NSString *addresstype;
@property (nonatomic,copy) void (^callAddressBack)(NSMutableDictionary * AddressDic,NSString * addresstype); //地址类型(0:发车地址，1:送达地址)

@property (nonatomic,copy) void (^callSearchAddressBack)(NSMutableDictionary * AddressDic,NSString * addresstype); //地址类型(0:发车地址，1:送达地址)

@property (nonatomic,strong) NSString * CityListName;
@property (nonatomic,strong) NSString * CityListCode;



@end
