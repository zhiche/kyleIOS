//
//  CDAddressCityVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 17/5/23.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface CDAddressCityVC : NavViewController  <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,copy) void (^callAddressCity)(NSString * addresstype);

@property (nonatomic,strong) NSString * address;

@end
