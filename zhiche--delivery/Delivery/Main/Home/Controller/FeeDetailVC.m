//
//  FeeDetailVC.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/10/9.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "FeeDetailVC.h"
#import "RootViewController.h"
#import "PayViewController.h"
#import "Common.h"
#import "WKProgressHUD.h"
#import "Model.h"
#import "CarInfoCell.h"
#import <Masonry.h>
@interface FeeDetailVC ()
@property (nonatomic,strong) UISwitch *scoreSwitch;
@property (nonatomic,strong) UILabel *totalL;//总运价
@property (nonatomic,strong) UILabel *scoreL;//积分抵用
@property (nonatomic,strong) UILabel *payL;//实付费用
@property (nonatomic,strong) UILabel *saleL;//折扣价

@property (nonatomic,strong) UILabel *eventL;//活动信息
@property (nonatomic,strong) WKProgressHUD *hud;


@property (nonatomic) BOOL isOn;
@end

@implementation FeeDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = WhiteColor;
    
    self.scrollView.contentInset = UIEdgeInsetsMake(-(116 * kHeight + cellHeight * 3 + 10), 0, 0, 0);
    self.scrollView.scrollEnabled = NO;
    
    
    [self initDataSource];
    
    
    [self.rotateButton addTarget:self action:@selector(rotateButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton * backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn addTarget:self action:@selector(pressBackBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.centerY.mas_equalTo(self.view.mas_top).with.offset(21.5*kHeight);
        make.size.mas_equalTo(CGSizeMake(50*kWidth, 44*kHeight));
    }];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;

    [self addSwipeRecognizer];

}

-(void)pressBackBtn{
    

    [[NSNotificationCenter defaultCenter] postNotificationName:@"BackVC" object:nil];
    
    [self.navigationController popViewControllerAnimated:YES];
 
}
#pragma mark - UIGestureRecognizerDelegate

- (void)addSwipeRecognizer
{
    // 初始化手势并添加执行方法
    UISwipeGestureRecognizer *swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(return)];
    
    // 手势方向
    swipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    
    // 响应的手指数
    swipeRecognizer.numberOfTouchesRequired = 1;
    
    // 添加手势
    [[self view] addGestureRecognizer:swipeRecognizer];
}

#pragma mark 返回上一级
- (void)return
{
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BackVC" object:nil];
    [self.navigationController popViewControllerAnimated:YES];

}

//按钮旋转
-(void)rotateButtonAction:(UIButton *)sender
{
    if (self.flag) {
        
        [UIView animateWithDuration:0.2 animations:^{
            self.arrowImageView.transform = CGAffineTransformMakeRotation(0);
            
            
        } completion:^(BOOL finished) {
            self.flag = NO;
            
            [self changeViewFrameWithInteger:0];
            
        }];
    } else {
        [UIView animateWithDuration:0.2 animations:^{
            self.arrowImageView.transform = CGAffineTransformMakeRotation(M_PI_2);
            
        } completion:^(BOOL finished) {
            self.flag = YES;
            
            [self changeViewFrameWithInteger:1];
        }];
    }
    
    
}
//根据table的展开来改变页面布局
-(void)changeViewFrameWithInteger:(NSInteger)integer
{
    
    //1、计算每个cell的高度 存储到self.heightArray中
    for (int i = 0;  i < self.dataArr.count; i++) {
        
        //    Model *model = [[Model alloc]init];
        //    model = self.dataArr[i];
        //
        //    NSString *string = model.vinList[0][@"vin"];
        //    for (int i = 1; i < model.vinList.count ; i ++) {
        //
        //        string = [string stringByAppendingString:[NSString stringWithFormat:@"\n%@ ",model.vinList[i][@"vin"]]];
        //
        //       }
        //
        //        model.contact = string;
        //
        //        CGFloat height = [CarInfoCell cellHeightWithModel:model];
        
        [self.heightArray addObject:@(56 * kHeight )];
    }
    
    
    [self.tableView reloadData];
    
    //2、cell展开时候计算总高度  关闭时候高度为0
    CGFloat height = 0;
    
    if (integer == 0) {
        
        [self.heightArray removeAllObjects];
        
    }
    
    for (int j = 0; j < self.heightArray.count; j++) {
        
        height += [self.heightArray[j] floatValue];
        
    }
    
    //3、改变view的frame
    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, screenWidth, cellHeight * 3 + height);
    
    self.feeTableView.frame = CGRectMake(0, CGRectGetMaxY(self.tableView.frame) + 10, screenWidth, self.feeTableView.frame.size.height);
    
    UIView *view0 = [self.view viewWithTag:700];
    view0.frame = CGRectMake(0, CGRectGetMaxY(self.feeTableView.frame) , screenWidth, view0.frame.size.height);
    
    UIView *view = [self.view viewWithTag:701];
    view.frame = CGRectMake(0, CGRectGetMaxY(view0.frame) + 10, screenWidth, view.frame.size.height);
    
    UIButton *button = [self.view viewWithTag:702];
    button.frame = CGRectMake(button.frame.origin.x, CGRectGetMaxY(view.frame) + 25, button.frame.size.width, button.frame.size.height);
    
    //    self.scrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(button.frame) + 20 + 64);
    
}


-(void)initDataSource
{
    //假数据
    //    NSDictionary *dic = [NSDictionary dictionary];
    //    dic =  [Common txtChangToJsonWithString:@"dingdan"];
    //
    //    if ([[dic[@"data"] allKeys] count] > 0) {
    //
    //        self.dataDictionary = [NSMutableDictionary dictionaryWithDictionary:dic[@"data"]];
    //
    //
    //        [self getValueString];
    //    }
    
    //接口数据
    
    NSString *urlString = nil;
    urlString = [NSString stringWithFormat:@"%@/%ld",order_detail,[self.orderId integerValue]];
    
    
    
    [Common requestWithUrlString:urlString contentType:application_json finished:^(id responseObj) {
        
        [self.hud dismiss:YES];
        
        if ([responseObj[@"success"] boolValue]) {
            self.dataDictionary = [NSMutableDictionary dictionaryWithDictionary:responseObj[@"data"]];
            
//            [self initThirdView];
            
            [self getValueString];
            
            if ([self.dataDictionary[@"scores"][@"amount"]floatValue] <= 0) {
                
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                button.frame = CGRectMake(0, 0, self.scoreSwitch.frame.size.width, self.scoreSwitch.frame.size.height);
                [self.scoreSwitch addSubview:button];
                [button addTarget:self action:@selector(noSwitch) forControlEvents:UIControlEventTouchUpInside];
                
            }
        } else {
            
            [WKProgressHUD popMessage:[NSString stringWithFormat:@"%@",responseObj[@"message"]] inView:self.view duration:1.5 animated:YES];
            
        }
        
        
    } failed:^(NSString *errorMsg) {
        [self.hud dismiss:YES];
        
        NSLog(@"%@",errorMsg);
    }];
    
    
}

//赋值
-(void)getValueString
{
    if ([self.dataDictionary[@"svehicles"] isEqual:[NSNull null]]) {
        
    } else {
        
        for (int i = 0; i < [self.dataDictionary[@"svehicles"] count]; i ++) {
            Model *model = [[Model alloc]init];
            [model setValuesForKeysWithDictionary:self.dataDictionary[@"svehicles"][i]];
            
            [self.dataArr addObject:model];
        }
        
        [self.tableView reloadData];
    }
    
    //订单编号
    self.numberLabel.text = [self backString:self.dataDictionary[@"orderCode"]];
    //起始地
    self.startLabel.text = [self backString:self.dataDictionary[@"departCityName"]];
    //目的地
    self.endLabel.text = [self backString:self.dataDictionary[@"receiptCityName"]];
    //出发时间
    self.startDate.text = [self backString:self.dataDictionary[@"deliveryDate"]];
    //结束时间
    self.endDateLabel.text = [self backString:self.dataDictionary[@"arriveDate"]];
    
    //汽车状态
    NSString *string ;
    
    
    if ([self.dataDictionary[@"isSencondhand"] boolValue]) {
        string = @"二手车";
        
    } else {
        string = @"新车";
        
    }
    
    if ([self.dataDictionary[@"isMobile"] boolValue]) {
        
        string = [string stringByAppendingString:@"   能动"];
        
    } else {
        string = [string stringByAppendingString:@"   不能动"];
        
    }
    
    
    
    self.carStautsLabel.text = string;
    //车辆总数
    
    int number = 0;
    if ([self.dataDictionary[@"svehicles"] isEqual:[NSNull null]]) {
        
    } else {
        
        for (int i = 0; i < [self.dataDictionary[@"svehicles"] count]; i ++) {
            
            number += [self.dataDictionary[@"svehicles"][i][@"amount"] intValue];
            
        }
    }
    
    self.amountLabel.text = [NSString stringWithFormat:@"%d辆",number];
    
    
    if ([self.dataDictionary[@"feeDetail"] count] > 0) {
        
        self.feeTableView.frame = CGRectMake(0, CGRectGetMaxY(self.tableView.frame) + 10, screenWidth, cellHeight * ([self.dataDictionary[@"feeDetail"] count] + 1));
        
        [self.feeTableView reloadData];
        
        [self initSecondView];
        
        [self initThirdView];
        
    }
    
/*
    
    for (int i = 0; i < [self.dataDictionary[@"feeDetail"] count]; i ++) {
        if ([self.dataDictionary[@"feeDetail"][i][@"feeName"] isEqualToString:@"提车费"]) {
            //提车费
            self.totalLabel.text = [self backMoney:self.dataDictionary[@"feeDetail"][i][@"cost"]];
        }
        
        if ([self.dataDictionary[@"feeDetail"][i][@"feeName"] isEqualToString:@"交车费"]) {
            //交车费
            self.payLabel.text = [self backMoney:self.dataDictionary[@"feeDetail"][i][@"cost"]];
            
        }
        
        if ([self.dataDictionary[@"feeDetail"][i][@"feeName"] isEqualToString:@"保险费"]) {
            //保险费
            self.unpayLabel.text = [self backMoney:self.dataDictionary[@"feeDetail"][i][@"cost"]];
        }
        
        if ([self.dataDictionary[@"feeDetail"][i][@"feeName"] isEqualToString:@"运输费"]) {
            //运输费
            self.scoreLabel.text = [self backMoney:self.dataDictionary[@"feeDetail"][i][@"cost"]];
        }
    }
 */
    
    
    //总运价
    self.totalL.text = [self backMoney:self.dataDictionary[@"orderCost"]];
    
    //
    if ([self.dataDictionary[@"discount"][@"discount"] boolValue]) {
        
        self.totalL.textColor = fontGrayColor;
        
        //添加删除线
        NSMutableAttributedString *newPrice = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",self.totalL.text]];
        [newPrice addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:NSMakeRange(0, newPrice.length)];
        self.totalL.attributedText = newPrice;
        
    }
    
    
    
    //折扣价
    self.saleL.text = [self backMoney:self.dataDictionary[@"discount"][@"cost"]];
    //积分抵用
    //    self.scoreL.text = [self backMoney:self.dataDictionary[@"scores"][@"amount"]];
    self.scoreL.text = @"￥0.00";
    
    //实付费用
    self.payL.text = [self backMoney:self.dataDictionary[@"actualCost"]];
    //活动
    self.eventL.text = [self backString:self.dataDictionary[@"discount"][@"desc"]];
    
    UILabel *label = [self.view viewWithTag:200];
    label.text = self.dataDictionary[@"scores"][@"actualPayText"];}

-(NSString *)backString:(NSString *)string
{
    
    if ([string isEqual:[NSNull null]]) {
        return @"";
    } else {
        return string;
    }
}

-(NSString *)backMoney:(NSString *)string
{
    NSString   *str = [NSString stringWithFormat:@"%.2f",[string floatValue]];
    
    if ([str isEqual:[NSNull null]]) {
        return @"";
    } else {
        return [NSString stringWithFormat:@"￥%@",str];
    }
    
    
}

-(void)initSecondView
{
    
    UIView *secondView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.feeTableView.frame) ,screenWidth , cellHeight)];
    secondView.tag = 700;
    secondView.backgroundColor = [UIColor whiteColor];
    
    UILabel *lineL = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, screenWidth, 0.5)];
    lineL.backgroundColor =  LineGrayColor;
    [secondView addSubview:lineL
     ];
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, 300, cellHeight)];
    label.text = @"可用积分100分抵用10.00元";
    label.font = Font(12);
    label.textColor = carScrollColor;
    [secondView addSubview:label];
    label.tag = 200;
    
    self.scoreSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(screenWidth - 18 - 50, CGRectGetMidY(label.frame) - 13.5, 50, 27)];
    [secondView addSubview:self.scoreSwitch];
    self.scoreSwitch.onTintColor = YellowColor;
    [self.scoreSwitch addTarget:self action:@selector(scoreSwitchAction:) forControlEvents:UIControlEventValueChanged];
    
    
    //控件不能设置frame，只能缩放比例
    //    self.scoreSwitch.transform = CGAffineTransformMakeScale(0.8, 0.8);
    
    [self.scrollView addSubview:secondView];
    
//    [self initThirdView];
}
-(void)noSwitch
{
    [WKProgressHUD popMessage:@"积分为0时不能使用积分支付" inView:self.view duration:1.5 animated:YES];
    
}

-(void)scoreSwitchAction:(UISwitch *)sender
{
    
    _isOn = sender.isOn;
    if (_isOn) {
        //使用积分支付
        
        CGFloat price ;
        price = [self.dataDictionary[@"scores"][@"amount"]floatValue];
        
        self.scoreL.text = [NSString stringWithFormat:@"-￥%.2f",[self.dataDictionary[@"scores"][@"amount"]floatValue] ];
        CGFloat pay = [self.dataDictionary[@"actualCost"]  floatValue] - price;
        
        self.payL.text = [NSString stringWithFormat:@"￥%.2f",pay];
        
        
        
    } else {
        
        //        self.scoreL.text = [self backMoney:self.dataDictionary[@"integralcost"]];
        self.scoreL.text = [self backMoney:self.dataDictionary[@"scores"][@"amount"]];
        self.scoreL.text = @"￥0.00";
        
        
        self.payL.text = [self backMoney:self.dataDictionary[@"actualCost"]];
        
    }
    
}

-(void)initThirdView
{
    
    NSLog(@"%@",self.dataDictionary[@"discount"][@"discount"]);
    
    if ([self.dataDictionary[@"discount"][@"discount"] boolValue]) {
        
        //有折扣的时候
        [self useScoreView];
        
    } else {
        
        //没有折扣的时候
        [self noUserScoreView];
    }

    
}

-(void)useScoreView
{
    UIView *thirdView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.feeTableView.frame) + cellHeight + 10,screenWidth , cellHeight * 4 - 40)];
    thirdView.tag = 701;
    thirdView.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:thirdView];
    
    //总运价
    UILabel *totalL = [self backLabelWithFrame:CGRectMake(18, 0, 100, cellHeight - 10) andString:@"总运价"];
    [thirdView addSubview:totalL];
    
    self.totalL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 200 - 18, 0, 200, cellHeight - 10)];
    self.totalL.font = Font(13);
    self.totalL.textColor = AddCarColor;
    //    self.totalL.text = @"￥1234.00";
    self.totalL.textAlignment = NSTextAlignmentRight;
    [thirdView addSubview:self.totalL];
    
    
    
    //折扣价
    UILabel *saleL = [self backLabelWithFrame:CGRectMake(18, cellHeight - 10, 100, cellHeight - 10) andString:@"折扣价"];
    [thirdView addSubview:saleL];
    
    self.saleL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 200 - 18, cellHeight - 10, 200, cellHeight - 10)];
    self.saleL.font = Font(13);
    self.saleL.textColor = AddCarColor;
    //    self.saleL.text = @"￥1200.00";
    self.saleL.textAlignment = NSTextAlignmentRight;
    
    [thirdView addSubview:self.saleL];
    
    //优惠
    self.eventL = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight * 2 -25, screenWidth - 18, 10)];
    self.eventL.textAlignment = NSTextAlignmentRight;
    //    self.eventL.text = @"优惠:活动期间价格8折，8月1日－12月31日";
    self.eventL.textColor = fontGrayColor;
    self.eventL.font = Font(10);
    [thirdView addSubview:self.eventL];
    //积分抵用
    UILabel *scoreL = [self backLabelWithFrame:CGRectMake(18, cellHeight * 2 - 20 , 100, cellHeight - 10) andString:@"积分抵用"];
    [thirdView addSubview:scoreL];
    
    self.scoreL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 200 - 18, cellHeight * 2 - 20, 200, cellHeight - 10)];
    self.scoreL.font = Font(13);
    self.scoreL.textColor = AddCarColor;
    //    self.scoreL.text = @"-￥10.00";
    self.scoreL.textAlignment = NSTextAlignmentRight;
    
    [thirdView addSubview:self.scoreL];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, CGRectGetMaxY(self.scoreL.frame), screenWidth - 18, 0.5)];
    label.backgroundColor = LineGrayColor;
    [thirdView addSubview:label];
    
    //实付费用
    UILabel *payL = [self backLabelWithFrame:CGRectMake(18, cellHeight * 3 - 30 , 100, cellHeight - 10) andString:@"实付费用"];
    [thirdView addSubview:payL];
    
    self.payL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 200 - 18, cellHeight * 3 - 30 , 200, cellHeight - 10)];
    self.payL.font = Font(13);
    self.payL.textColor = YellowColor;
    //    self.payL.text = @"￥1190.00";
    self.payL.textAlignment = NSTextAlignmentRight;
    
    [thirdView addSubview:self.payL];
    
    
    UIButton *confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmButton.tag = 702;
    [confirmButton setTitle:@"确认支付" forState:UIControlStateNormal];
    confirmButton.frame = CGRectMake(18, CGRectGetMaxY(thirdView.frame) + 25, screenWidth - 36, 40 * kHeight);
    [confirmButton setBackgroundColor:YellowColor];
    [confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [confirmButton addTarget:self action:@selector(confirmPayButton) forControlEvents:UIControlEventTouchUpInside];
    confirmButton.layer.cornerRadius = 5;
    
    [self.scrollView addSubview:confirmButton];
    
}

-(void)noUserScoreView
{
    UIView *thirdView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.feeTableView.frame) + cellHeight + 10,screenWidth , cellHeight * 3 - 30)];
    thirdView.tag = 701;
    thirdView.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:thirdView];
    
    //总运价
    UILabel *totalL = [self backLabelWithFrame:CGRectMake(18, 0, 100, cellHeight - 10) andString:@"总运价"];
    [thirdView addSubview:totalL];
    
    self.totalL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 200 - 18, 0, 200, cellHeight - 10)];
    self.totalL.font = Font(13);
    self.totalL.textColor = AddCarColor;
    //    self.totalL.text = @"￥1234.00";
    self.totalL.textAlignment = NSTextAlignmentRight;
    [thirdView addSubview:self.totalL];
    
    
    
    
    //折扣价
    UILabel *saleL = [self backLabelWithFrame:CGRectMake(18, cellHeight - 10, 100, cellHeight - 10) andString:@"折扣价"];
    //    [thirdView addSubview:saleL];
    
    self.saleL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 200 - 18, cellHeight - 10, 200, cellHeight - 10)];
    self.saleL.font = Font(13);
    self.saleL.textColor = AddCarColor;
    //    self.saleL.text = @"￥1200.00";
    self.saleL.textAlignment = NSTextAlignmentRight;
    
    //    [thirdView addSubview:self.saleL];
    
    //优惠
    self.eventL = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight * 2 -25, screenWidth - 18, 10)];
    self.eventL.textAlignment = NSTextAlignmentRight;
    //    self.eventL.text = @"优惠:活动期间价格8折，8月1日－12月31日";
    self.eventL.textColor = fontGrayColor;
    self.eventL.font = Font(10);
    //    [thirdView addSubview:self.eventL];
    //积分抵用
    UILabel *scoreL = [self backLabelWithFrame:CGRectMake(18, cellHeight - 10 , 100, cellHeight - 10) andString:@"积分抵用"];
    [thirdView addSubview:scoreL];
    
    self.scoreL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 200 - 18, cellHeight  - 10, 200, cellHeight - 10)];
    self.scoreL.font = Font(13);
    self.scoreL.textColor = AddCarColor;
    //    self.scoreL.text = @"-￥10.00";
    self.scoreL.textAlignment = NSTextAlignmentRight;
    
    [thirdView addSubview:self.scoreL];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, CGRectGetMaxY(self.scoreL.frame), screenWidth - 18, 0.5)];
    label.backgroundColor = LineGrayColor;
    [thirdView addSubview:label];
    
    //实付费用
    UILabel *payL = [self backLabelWithFrame:CGRectMake(18, cellHeight * 2 - 20 , 100, cellHeight - 10) andString:@"实付费用"];
    [thirdView addSubview:payL];
    
    self.payL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 200 - 18, cellHeight * 2 - 20 , 200, cellHeight - 10)];
    self.payL.font = Font(13);
    self.payL.textColor = YellowColor;
    //    self.payL.text = @"￥1190.00";
    self.payL.textAlignment = NSTextAlignmentRight;
    
    [thirdView addSubview:self.payL];
    
    
    UIButton *confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmButton.tag = 702;
    [confirmButton setTitle:@"确认支付" forState:UIControlStateNormal];
    confirmButton.frame = CGRectMake(18, CGRectGetMaxY(thirdView.frame) + 25, screenWidth - 36, 40 * kHeight);
    [confirmButton setBackgroundColor:YellowColor];
    [confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [confirmButton addTarget:self action:@selector(confirmPayButton) forControlEvents:UIControlEventTouchUpInside];
    confirmButton.layer.cornerRadius = 5;
    
    [self.scrollView addSubview:confirmButton];
    
}



-(void)confirmPayButton
{
    PayViewController *payVC = [[PayViewController alloc]init];
    
    payVC.moneyString = self.payL.text;
    payVC.isScore = self.isOn;
    payVC.orderid = self.orderId;
    
    [self.navigationController pushViewController:payVC animated:YES];
}

-(UILabel *)backLabelWithFrame:(CGRect)frame andString:(NSString *)string
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.text = string;
    label.textColor = fontGrayColor;
    label.font = Font(13);
    
    return label;
    
}



-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
