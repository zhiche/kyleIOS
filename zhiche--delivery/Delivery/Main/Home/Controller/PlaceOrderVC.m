 //
//  PlaceOrderVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "PlaceOrderVC.h"
#import <Masonry.h>
#import "AddressCell.h"
#import "CarCell.h"
#import "ActionSheetPicker.h"
#import "ChooseAddressVC.h"
#import "NewBtn.h"
#import "CarShipVC.h"
#import "ChoiceCarTypeVC.h"
#import "ChooseAddressVC.h"
#import "Common.h"
#import "PlaceOrderModel.h"
#import "HistoryCarStyleVC.h"
#import <MJRefresh.h>
#import "RootViewController.h"
#import <UIImageView+WebCache.h>
#import "FindQuoteVC.h"
#import "CustomTextField.h"
#import "AddCarTypeVC.h"
#import "SevenSwitch.h"
#import "placeAddressCell.h"
#import "KeepAddressVC.h"
#import "AddressVC.h"
#import "ConfirmCarVC.h"
#import "WKProgressHUD.h"
#import "FeeDetailVC.h"

#define AddressTag  10000
#define CarTag 20000
#define InforTableTag 30000
#define TimeTag 100
#define CommenTag 120
#define TatalLabelTag 333
#define PickCarTimeTag 555
#define PickCarImageTag 666
#define PickCarBtnTag 777
#define BigTag 50
#define SmallTag 60
@interface PlaceOrderVC ()<UIGestureRecognizerDelegate>
{
    UIImageView * nav;
    NSMutableArray * NumberArr;
    HistoryCarStyleVC *choiceVC;
    Common * com;
    PlaceOrderModel * PlaceModel;
    NSMutableDictionary * dicUrl;
    RootViewController * TabBar;
    BOOL addressYes;
    BOOL vinYes;
    BOOL carYes;
    BOOL pickCarHeight;
    int CarNumber;
    
    int SwitchNumber;
    CGFloat addressCellHight1;
    CGFloat addressCellHight2;
    int keepAddressNumber;
    BOOL pushIsOk;
    NSMutableArray * pickTimeData;
    NSMutableArray * pickProductArr;
    CGFloat addressHigth ;
    
    BOOL chooseAddressYesOrNo;
    
    UIImageView * chooseAdressImge;
    UIButton * chooseAddressBtn;
    
}
@property (nonatomic,strong) NSMutableArray *CarName;
@property (nonatomic,strong) NSMutableArray *RemoveArr;
@property (nonatomic,strong) UITableView * CarTable;
@property (nonatomic,strong) UITableView * AddressTable;
@property (nonatomic,strong) UITableView * InforTable;
@property (nonatomic,strong) NSMutableArray * dataSouce;
@property (nonatomic,strong) NSArray * timeChoose;
@property (nonatomic,strong) NSMutableArray * carInforArr;
@property (nonatomic,strong) NSMutableArray * headerViewArr;
@property (nonatomic,strong) NSMutableArray * headerCarNumberArr;
@property (nonatomic,strong) NSMutableArray * InforHeaderCarArr;
@property (nonatomic,strong) NSMutableArray * InforCellArr;
@property (nonatomic,strong) UIScrollView * scroll;
@property (nonatomic,strong) UIView * backview;
@property (nonatomic,strong) UIView * backview1;
@property (nonatomic,strong) UIView * backview2;
@property (nonatomic,strong) UIButton * btnTime;
@property (nonatomic,strong) UILabel * standardArrive;
@property (nonatomic,strong) UILabel * standardTime;
@property (nonatomic,strong) UILabel * speedyArrive;
@property (nonatomic,strong) UILabel * speedyTime;
@property (nonatomic,strong) UILabel * warnlabel;
@property (nonatomic,strong) UIView * warnView;

@property (nonatomic,strong) NSString * adate ;//提车日期
@property (nonatomic,strong) NSString * atime;//提车时间

@property (nonatomic,strong) NSMutableArray * timeChoose1;
@property (nonatomic,strong) NSMutableArray * timeChoose2;


@property (nonatomic,strong) UIView * backview3;
@property (nonatomic,strong) UIView * backview4;
@property (nonatomic,strong) UIView * backview5;
@property (nonatomic,strong) UIView * CarInformationView;
@property (nonatomic,strong) UITextField * CarPromptField;
@property (nonatomic,strong) UILabel * prompt1;
@property (nonatomic,strong) UILabel * prompt2;


@end

@implementation PlaceOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GrayColor;
    nav = [self createNav:@"城际运输"];
    [self.view addSubview:nav];
    chooseAddressYesOrNo = NO;

    choiceVC = [[HistoryCarStyleVC alloc]init];
    TabBar = [RootViewController defaultsTabBar];
    [self evaluate];
    [self initSomeing];
    [self createUI];
    [self createData];
    [self createInforHeaderView];
    [self.navigationController.interactivePopGestureRecognizer addTarget:self action:@selector(handleGesture)];
    
    //接受地址信息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeBackAddress:) name:@"BackAddressDic" object:nil];
    //返回删除信息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeVC:) name:@"BackVC" object:nil];

}
#pragma mark 初始化

-(void)initSomeing{
    
    com = [[Common alloc]init];
    PlaceModel = [[PlaceOrderModel alloc]init];
    _dataSouce = [[NSMutableArray alloc]init];
    
    _headerViewArr = [[NSMutableArray alloc]init];
    _headerCarNumberArr = [[NSMutableArray alloc]init];
    _InforHeaderCarArr = [[NSMutableArray alloc]init];
    _InforCellArr = [[NSMutableArray alloc]init];
    _InforCellArr = @[@"",@""].mutableCopy;
    _carInforArr = [[NSMutableArray alloc]init];
    NumberArr = [[NSMutableArray alloc]init];
    _CarName = [[NSMutableArray alloc]init];
    _RemoveArr = [[NSMutableArray alloc]init];
    dicUrl = [[NSMutableDictionary alloc]init];
    pickTimeData = [[NSMutableArray alloc]init];
    pickProductArr = [[NSMutableArray alloc]init];
    _timeChoose1 = [[NSMutableArray alloc]init];
    _timeChoose2 = [[NSMutableArray alloc]init];
    
    for (int i = 0; i<24; i++) {
        
        NSString * time = nil;
        if (i<10) {
            time = [NSString stringWithFormat:@"0%d",i];
        }else{
            time = [NSString stringWithFormat:@"%d",i];
        }
        [_timeChoose1 addObject:time];
    }
    for (int i= 0; i<60; i++) {
        
        NSString * time = nil;
        if (i<10) {
            time = [NSString stringWithFormat:@"0%d",i];
        }else{
            time = [NSString stringWithFormat:@"%d",i];
        }
        [_timeChoose2 addObject:time];
    }

    
    _adate = [[NSString alloc]init];
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy-MM-dd"];
    _adate = [dateformatter stringFromDate:senddate];
    
    _atime = [[NSString alloc]init];

    
    
    [dicUrl setObject:@"" forKey:@"departaddrid"];//发运地址id
    [dicUrl setObject:@"" forKey:@"receiptaddrid"];//目的地址id
    [dicUrl setObject:@"" forKey:@"deliverydate"];//提车日期
//    [dicUrl setObject:_atime forKey:@"deliveryTime"];//
    
    [dicUrl setObject:@"" forKey:@"arrivedate"];//到达日期
    [dicUrl setObject:@"" forKey:@"vehiclesbrands"];//cars
    [dicUrl setObject:@"1" forKey:@"ispick"];//是否上门提车
    [dicUrl setObject:@"1" forKey:@"isdeliv"];//是否送车上门
    [dicUrl setObject:@"0" forKey:@"issencondhand"];//新车
    [dicUrl setObject:@"1" forKey:@"ismobile"];//能动
    [dicUrl setObject:@"" forKey:@"estvalue"];//估值
    [dicUrl setObject:@"" forKey:@"productid"];//物流产品id

    [dicUrl setObject:@"F" forKey:@"isUrgent"];//是否加急
    [dicUrl setObject:@"10" forKey:@"transportType"];//运输方式
    
    
    [dicUrl setObject:@"false" forKey:@"isAppoint"];//是否预约
    [dicUrl setObject:@"ture" forKey:@"isTicket"];//是否开票

    
}

#pragma mark 初始化赋值

-(void)evaluate{
    
    addressYes = YES;
    vinYes = YES;
    carYes = YES;
    pushIsOk = YES;
    pickCarHeight = NO;
    CarNumber = 0;
    SwitchNumber = 0;
    addressCellHight1 = 43*kHeight;
    addressCellHight2 = 43*kHeight;
}

-(void)changeVC:(NSNotification *)notification{
    
    [dicUrl setObject:_carInforArr forKey:@"vehiclesbrands"];

//    [dicUrl setObject:@"" forKey:@"departaddrid"];//发运地址id
//    [dicUrl setObject:@"" forKey:@"receiptaddrid"];//目的地址id
//    [dicUrl setObject:@"" forKey:@"deliverydate"];//提车日期
//    [dicUrl setObject:@"" forKey:@"arrivedate"];//到达日期
//    [dicUrl setObject:@"" forKey:@"vehiclesbrands"];//cars
//    [dicUrl setObject:@"0" forKey:@"ispick"];//是否上门提车
//    [dicUrl setObject:@"0" forKey:@"isdeliv"];//是否送车上门
//    [dicUrl setObject:@"1" forKey:@"issencondhand"];//新车
//    [dicUrl setObject:@"1" forKey:@"ismobile"];//能动
//    [dicUrl setObject:@"" forKey:@"estvalue"];//估值

//    _CarPromptField.text = @"";
//    [_carInforArr removeAllObjects];
//    [_dataSouce removeAllObjects];
//    [self createData];
//    CarNumber = 0;
//    [self updateScrollFram:0];
//    [self updateCarAndScrollFram:0];
//    [_AddressTable reloadData];
//    [_CarTable reloadData];
}


-(void)changeBackAddress:(NSNotification *)notification{

    
    
    NSMutableDictionary * addressDic = [notification object];
    NSString * addresstype = [NSString stringWithFormat:@"%@",addressDic[@"addressType"]];
    NSString * address = [NSString stringWithFormat:@"%@",addressDic[@"address"]];
    addressCellHight1 = 43*kHeight;
    if ([addresstype isEqualToString:@"1"]) {
        for (int i = 0; i<_dataSouce.count; i++) {
            if ([_dataSouce[i][@"addressType"] isEqualToString:@"1"]&&address.length>0) {
                [_dataSouce replaceObjectAtIndex:i withObject:addressDic] ;
                addressCellHight2 = 100*kHeight;
            }
            NSString * address1 = [NSString stringWithFormat:@"%@",_dataSouce[i][@"address"]];
            if ([_dataSouce[i][@"addressType"] isEqualToString:@"0"]&&address1.length>0) {
                addressCellHight1 = 100*kHeight;
            }
        }
    }
    else{
        for (int i = 0; i<_dataSouce.count; i++) {
            if ([_dataSouce[i][@"addressType"] isEqualToString:@"0"]) {
                [_dataSouce replaceObjectAtIndex:i withObject:addressDic] ;
                addressCellHight1 = 100*kHeight;
            }else{
                NSString * address1 = [NSString stringWithFormat:@"%@",_dataSouce[i][@"address"]];
                if ([_dataSouce[i][@"addressType"] isEqualToString:@"1"]&&address1.length>0) {
                    addressCellHight2 = 100*kHeight;
                }else{
                    addressCellHight2 = 43*kHeight;
                }
            }
        }
    }
    [self judgementAddress];

    CGFloat totalHeight = addressCellHight1 +addressCellHight2;
    [self updateAddressTable:totalHeight];
    [_AddressTable reloadData];
}

-(void)handleGesture{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)createStarData{
    
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy-MM-dd"];
    NSString *  locationString=[dateformatter stringFromDate:senddate];
    
    
    NSDateFormatter  *dateformatterTime=[[NSDateFormatter alloc] init];
    [dateformatterTime setDateFormat:@"HH:mm:ss"];
    NSString * string = [dateformatterTime stringFromDate:senddate];
    
    
    
    
    chooseAdressImge = [[UIImageView alloc]init];
    chooseAdressImge.image = [UIImage imageNamed:@"zhuanhuan"];
    [_backview1 addSubview:chooseAdressImge];
    [chooseAdressImge mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left).with.offset(20*kWidth);
        make.centerY.mas_equalTo(_backview1.mas_centerY).with.offset(21.5*kHeight);
        make.size.mas_equalTo(CGSizeMake(9*kWidth, 11*kHeight));
    }];
    
    
    chooseAddressBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [chooseAddressBtn addTarget:self action:@selector(pressChooseBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    [_backview1 addSubview:chooseAddressBtn];
    [chooseAddressBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left).with.offset(15*kWidth);
        make.centerY.mas_equalTo(_backview1.mas_centerY).with.offset(21.5*kHeight);
        make.size.mas_equalTo(CGSizeMake(50*kWidth, 50*kHeight));
    }];


    
    
    
    
    
    if (_dataSouce.count==0) {
        
        NSMutableDictionary *departaddrid = [[NSMutableDictionary alloc]init];
        [departaddrid setValue:@"0" forKey:@"addressType"];
        [departaddrid setValue:@"" forKey:@"address"];
        [_dataSouce addObject:departaddrid];
        
        NSMutableDictionary *receiptaddrid = [[NSMutableDictionary alloc]init];
        [receiptaddrid setValue:@"1" forKey:@"addressType"];
        [receiptaddrid setValue:@"" forKey:@"address"];
        [_dataSouce addObject:receiptaddrid];
        
        chooseAdressImge.hidden = YES;
        chooseAddressBtn.hidden = YES;
        
        
    }else if (_dataSouce.count==1){
        // 0 是发货
        if ([_dataSouce[0][@"addressType"] isEqualToString:@"1"]) {
            [dicUrl setObject:_dataSouce[0][@"id"] forKey:@"receiptaddrid"];//目的地地址id 8-9
            NSMutableDictionary *departaddrid = [[NSMutableDictionary alloc]init];
            [departaddrid setValue:@"0" forKey:@"addressType"];
            [departaddrid setValue:@"" forKey:@"address"];
            addressCellHight2 = 100*kHeight;
            
            [_dataSouce insertObject:departaddrid atIndex:0];
//            [_dataSouce addObject:departaddrid];
            
            
            [chooseAdressImge mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(_backview1.mas_centerY).with.offset(21.5*kHeight);
            }];

            
            
            
            
        }else{
            [dicUrl setObject:_dataSouce[0][@"id"] forKey:@"departaddrid"];//发运地址id 2-5
            NSMutableDictionary *receiptaddrid = [[NSMutableDictionary alloc]init];
            [receiptaddrid setValue:@"1" forKey:@"addressType"];
            [receiptaddrid setValue:@"" forKey:@"address"];
            addressCellHight1 = 100*kHeight;
            [_dataSouce addObject:receiptaddrid];
            [chooseAdressImge mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(_backview1.mas_centerY).with.offset(21.5*kHeight);
            }];

        }
        
        chooseAdressImge.hidden = NO;
        chooseAddressBtn.hidden = NO;

    }
    else{
        addressCellHight1 = 100*kHeight;
        addressCellHight2 = 100*kHeight;
        
        
        NSString * addressType = [NSString stringWithFormat:@"%@",_dataSouce[0][@"addressType"]];
        if ([addressType isEqualToString:@"1"]) {
            
            [_dataSouce exchangeObjectAtIndex:0 withObjectAtIndex:1];
            
        }
        
        
        
        for (int i=0; i<_dataSouce.count; i++) {
            if ([_dataSouce[i][@"addressType"] isEqualToString:@"1"]) {
                [dicUrl setObject:_dataSouce[i][@"id"] forKey:@"receiptaddrid"];//目的地地址id 8-9
            }else{
                [dicUrl setObject:_dataSouce[i][@"id"] forKey:@"departaddrid"];//发运地址id 2-5
            }
        }
        chooseAdressImge.hidden = NO;
        chooseAddressBtn.hidden = NO;

    }
    addressHigth = 0;
    if (_dataSouce.count>0) {
        NSString * address1 =[NSString stringWithFormat:@"%@",_dataSouce[0][@"address"]];
        NSString * address2 =[NSString stringWithFormat:@"%@",_dataSouce[1][@"address"]];
        if (address1.length==0) {
            addressHigth = addressHigth+43*kHeight;
        }else{
            addressHigth = addressHigth+100*kHeight;
        }
        if (address2.length==0) {
            addressHigth = addressHigth+43*kHeight;
        }else{
            addressHigth = addressHigth+100*kHeight;
        }
    }else{
        addressHigth = 86*kHeight;
    }
    [_AddressTable mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(addressHigth);
    }];
    [_backview1 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(addressHigth+43*kHeight);
    }];
    
    [self judgementAddress];

    [self updateFristScrollFram:addressHigth];
    [_AddressTable reloadData];
    [_AddressTable mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(addressHigth);
    }];
    [_backview1 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(addressHigth+43*kHeight);
    }];

    [dicUrl setObject:locationString forKey:@"deliverydate"];//提车日期
    
    NSString *  b = [string substringWithRange:NSMakeRange(0,2)] ;
    NSString *  c = [string substringWithRange:NSMakeRange(2,3)] ;
    
    NSString * btnlabel = [NSString stringWithFormat:@"%@%@",b,c];
    [dicUrl setObject:btnlabel forKey:@"deliveryTime"];
}



-(void)pressChooseBtn:(UIButton*)sender{
    
    
    if (!chooseAddressYesOrNo) {
        chooseAddressYesOrNo = YES;
    }else{
        chooseAddressYesOrNo = NO;
    }
    
    NSString * add1 = [NSString stringWithFormat:@"%@",_dataSouce[0][@"addressType"]];
    NSMutableDictionary * dic0 = [[NSMutableDictionary alloc]initWithDictionary:_dataSouce[0]];

    
    
    
    NSString * add2 = [NSString stringWithFormat:@"%@",_dataSouce[1][@"addressType"]];
    NSMutableDictionary * dic1 = [[NSMutableDictionary alloc]initWithDictionary:_dataSouce[1]];


    
    [dic0 setObject:add2 forKey:@"addressType"];
    [dic1 setObject:add1 forKey:@"addressType"];
    
    
    [_dataSouce replaceObjectAtIndex:0 withObject:dic0];
    [_dataSouce replaceObjectAtIndex:1 withObject:dic1];
    
    
    addressHigth = 0;
    CGFloat addresscell0  = 0;
    CGFloat addresscell1  = 0;

    NSString * address1 =[NSString stringWithFormat:@"%@",_dataSouce[0][@"address"]];
    NSString * address2 =[NSString stringWithFormat:@"%@",_dataSouce[1][@"address"]];

    
    
    if (address2.length == 0 || address1.length == 0) {
        
        if (address1.length == 0) {
            
            addressHigth = addressHigth +43*kHeight;
            addresscell0 = 43*kHeight;
            
        }else{
            addressHigth = addressHigth +100*kHeight;
            addresscell0 = 100*kHeight;
        }
        
        if (address2.length == 0) {
            addressHigth = addressHigth + 43*kHeight;
            addresscell1 = 43*kHeight;
            
        }else{
            addressHigth = addressHigth + 100*kHeight;
            addresscell1 = 100*kHeight;
        }
        
    }
    
    
    
    if (address1.length !=0 && address2.length != 0) {
    
        addressHigth = 200*kHeight;
        addresscell1 = 100*kHeight;
        addresscell0 = 100*kHeight;
    
    
    }

    
    addressCellHight1 = addresscell0;
    addressCellHight2 = addresscell1;
        
    [_AddressTable mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(addressHigth);
    }];
    [_backview1 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(addressHigth+43*kHeight);
    }];

    [_AddressTable reloadData];

    
}


-(void)judgementAddress{
    
    BOOL AddressIsTwo = YES;
    for (int i = 0; i<_dataSouce.count; i++) {
        
        NSString * address = [NSString stringWithFormat:@"%@",_dataSouce[i][@"address"]];
        if (address.length == 0) {
            AddressIsTwo = NO;
            break;
        }
    }
    
    if (!AddressIsTwo) {
        [_backview2 mas_updateConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Main_Width, 86*kHeight));
        }];
        pickCarHeight = NO;
    }else{
        pickCarHeight = YES;
        NSString * depaId = nil;//提车地址id
        NSString * destId = nil;//收车地址id
        
        // 0 是发货 1是收车
//        if ([_dataSouce[0][@"addressType"] isEqualToString:@"1"]) {
        for (int i = 0; i<_dataSouce.count; i++) {
            
            if ([_dataSouce[i][@"addressType"] isEqualToString:@"1"]) {
                depaId = [NSString stringWithFormat:@"%@",_dataSouce[i][@"id"]];
            }else{
                destId = [NSString stringWithFormat:@"%@",_dataSouce[i][@"id"]];
            }
        }

        NSString * url = [NSString stringWithFormat:@"%@?depaId=%@&destId=%@&adate=%@&atime=%@",product_Url,depaId,destId,_adate,dicUrl[@"deliveryTime"]];

        
        [Common requestWithUrlString:url contentType:@"application/json" finished:^(id responseObj){

            pickProductArr = responseObj[@"data"];

            if ([pickProductArr count]>0) {
                [_backview2 mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.size.mas_equalTo(CGSizeMake(Main_Width, 43*4*kHeight));
                }];

                [self bestowData:responseObj[@"data"]];
            }
            
        } failed:^(NSString *errorMsg) {
            NSLog(@"%@",errorMsg);
        }];

    }
}


-(void)bestowData:(NSMutableArray*)arr{
    

    UILabel * label1 = (UILabel *)[_backview2 viewWithTag:52];
    UILabel * label2 = (UILabel *)[_backview2 viewWithTag:53];

    NSString * label11 = [NSString stringWithFormat:@"%@",arr[0][@"endTime"]];
    label1.text = [label11 substringWithRange:NSMakeRange(0, 10)];

    label2.text = [NSString stringWithFormat:@"%.3fkm",[arr[0][@"distance"] floatValue]];
    
    [dicUrl setObject:arr[0][@"productId"] forKey:@"productid"];
    [dicUrl setObject:arr[0][@"endTime"] forKey:@"arrivedate"];


    SwitchNumber = 2;
    [self updateScrollFram:_carInforArr.count];
}

-(void)createTableView:(NSString*)imagesStr{
    
    [_headerViewArr removeAllObjects];
    for (int i =0; i<self.CarName.count; i++) {
        UIView * SectionView = [[UIView alloc]init];
        SectionView.backgroundColor = [UIColor whiteColor];
        SectionView.tag = 900+i;
        SectionView.frame = CGRectMake(0, 0, Main_Width, 43*kHeight);
        
        UIImageView * SmallImage = [[UIImageView alloc]init];
//        NSURL * imageurl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",self.CarName[i][@"brandLogo"]]];
        SmallImage.contentMode = UIViewContentModeScaleAspectFit;

        SmallImage.backgroundColor = [UIColor redColor];
//        [SmallImage sd_setImageWithURL:imageurl placeholderImage:[UIImage imageNamed:@"car_bad"] options:SDWebImageCacheMemoryOnly];
        UILabel * CarName = [self createUIlabel:self.CarName[i][@"brandname"] andFont:FontOfSize14 andColor:BtnTitleColor];

        UIButton * DeleteBtn = [self createBtn:@"删除"];
        DeleteBtn.layer.borderWidth = 0;
        [DeleteBtn setTitleColor:YellowColor forState:UIControlStateNormal];
        
        UIButton * ReduceBtn = [self createBtn:@"-"];
        UIButton * AddBtn = [self createBtn:@"+"];
        ReduceBtn.titleLabel.font = Font(FontOfSize15);
        AddBtn.titleLabel.font = Font(FontOfSize15);

        UILabel * Number = [self createUIlabel:self.CarName[i][@"vehiclecount"] andFont:FontOfSize13 andColor:littleBlackColor];

        
        AddBtn.tag = 1200+i;
        ReduceBtn.tag =1100+i;
        Number.tag = 1000+i;
        DeleteBtn.tag = 1300+i;
        
        [SectionView addSubview:SmallImage];
        [SectionView addSubview: CarName];
        [SectionView addSubview:DeleteBtn];
        [SectionView addSubview: ReduceBtn];
        [SectionView addSubview: AddBtn];
        [SectionView addSubview: Number];
        
        
        [SmallImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(SectionView.mas_left).with.offset(18*kWidth);
            make.centerY.mas_equalTo(SectionView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(22*kWidth, 22*kHeight));
        }];
        [CarName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(SmallImage.mas_right).with.offset(12*kWidth);
            make.centerY.mas_equalTo(SmallImage.mas_centerY);
            make.width.mas_equalTo(Main_Width/2.0-20*kWidth);
        }];
        [DeleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(SectionView.mas_right).with.offset(-15*kWidth);
            make.centerY.mas_equalTo(SmallImage.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(30*kWidth, 43*kHeight));
        }];
        [AddBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(DeleteBtn.mas_left).with.offset(-10*kWidth);
            make.centerY.mas_equalTo(SmallImage.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(20*kWidth, 20*kHeight));
        }];
        [Number mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(AddBtn.mas_left).with.offset(0);
            make.centerY.mas_equalTo(SmallImage.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(20*kWidth, 20*kHeight));
        }];
        [ReduceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(Number.mas_left).with.offset(0);
            make.centerY.mas_equalTo(SmallImage.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(20*kWidth, 20*kHeight));
        }];
        
        
        UIView * Hline =[[UIView alloc]init];
        Hline.backgroundColor = RGBACOLOR(149, 149, 149, 1);
        [SectionView addSubview:Hline];
        [Hline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(ReduceBtn.mas_right);
            make.top.equalTo(ReduceBtn.mas_top);
            make.size.mas_equalTo(CGSizeMake(30*kWidth, 0.5));
        }];
        UIView * Hline1 =[[UIView alloc]init];
        Hline1.backgroundColor = RGBACOLOR(149, 149, 149, 1);
        [SectionView addSubview:Hline1];
        [Hline1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(ReduceBtn.mas_right);
            make.bottom.equalTo(ReduceBtn.mas_bottom);
            make.size.mas_equalTo(CGSizeMake(30*kWidth, 0.5));
        }];
        [_headerViewArr addObject:SectionView];
    }
}
-(void)createData{
    
    [Common requestWithUrlString:addressFault_Url contentType:@"application/json" finished:^(id responseObj){
        NSMutableArray * arr =[[NSMutableArray alloc]init];
        arr =  [NSMutableArray arrayWithArray:responseObj[@"data"]];
        _dataSouce = nil;
        _dataSouce = arr.mutableCopy;
        [self createStarData];
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
}
-(void)viewWillAppear:(BOOL)animated{
    [TabBar setTabBarHidden:YES];
    pushIsOk = YES;
    
}


-(void)createUI{

    _scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, Main_Width,Main_height-64*kHeight)];
    _scroll.contentOffset = CGPointMake(0, 0);
    _scroll.contentSize = CGSizeMake(Main_Width, Main_height);
    self.automaticallyAdjustsScrollViewInsets =NO;
    _scroll.bounces = NO;
    _scroll.userInteractionEnabled = YES;
    _scroll.showsHorizontalScrollIndicator = NO;
    _scroll.showsVerticalScrollIndicator = NO;
    _scroll.delegate = self;
    _scroll.minimumZoomScale = 1.0;
    _scroll.maximumZoomScale = 3.0;
    [self.view addSubview:_scroll];
    
    _backview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, Main_height)];
    _backview.backgroundColor = GrayColor;
    _backview.userInteractionEnabled = YES;
    [_scroll addSubview:_backview];
    [_backview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_scroll.mas_left);
        make.top.mas_equalTo(_scroll.mas_top);
        make.size.mas_equalTo(CGSizeMake(Main_Width, Main_height));
    }];
    _backview1 = [[UIView alloc]init];
    _backview1.backgroundColor = WhiteColor;
    [_backview addSubview:_backview1];
    [_backview1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left);
        make.top.mas_equalTo(_backview.mas_top).with.offset(0*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 129*kHeight));
    }];
    UIView * addressView = [[UIView alloc]init];
    addressView.backgroundColor = headerBackViewColor;
    [_backview1 addSubview:addressView];
    [addressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left);
        make.top.mas_equalTo(_backview1.mas_top);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
    }];

    UILabel * address = [com createUIlabel:@"地址" andFont:FontOfSize14 andColor:BlackTitleColor];
    [addressView addSubview:address];
    [address mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(addressView.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(addressView.mas_top).with.offset(21.5*kHeight);
    }];
    
    UIView * Addressline = [[UIView alloc]init];
    Addressline.backgroundColor = LineGrayColor;
    [addressView addSubview:Addressline];
    [Addressline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(addressView.mas_bottom);
        make.left.mas_equalTo(addressView.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    _AddressTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64*kHeight, Main_Width, 86*kHeight) style:UITableViewStylePlain];
    [_AddressTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _AddressTable.tag = AddressTag;
    _AddressTable.delegate = self;
    _AddressTable.dataSource = self;
    [_AddressTable setTableFooterView:[UIView new]];
    [_backview1 addSubview:_AddressTable];
    
    [_AddressTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left).with.offset(0);
        make.top.mas_equalTo(_backview1.mas_top).with.offset(43*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 86*kHeight));
    }];
    
    _backview2 = [[UIView alloc]init];
    _backview2.backgroundColor = WhiteColor;
    [_backview addSubview:_backview2];

    [_backview2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left).with.offset(0);
        make.top.mas_equalTo(_AddressTable.mas_bottom).with.offset(8*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 86*kHeight));
    }];

    UIView * timeView = [[UIView alloc]init];
    timeView.backgroundColor = headerBackViewColor;
    [_backview2 addSubview:timeView];
    [timeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview2.mas_left);
        make.top.mas_equalTo(_backview2.mas_top);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
    }];
    UIView * Timeline = [[UIView alloc]init];
    Timeline.backgroundColor = LineGrayColor;
    [timeView addSubview:Timeline];
    [Timeline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(timeView.mas_bottom);
        make.left.mas_equalTo(timeView.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    UILabel * TimeLabel = [com createUIlabel:@"时间" andFont:FontOfSize14 andColor:BlackTitleColor];
    [timeView addSubview:TimeLabel];
    [TimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeView.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(timeView.mas_top).with.offset(21.5*kHeight);
    }];

    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy-MM-dd"];
    NSString *  locationString=[dateformatter stringFromDate:senddate];
    UIImageView * Timeimage = [[UIImageView alloc]init];
    Timeimage.image = [UIImage imageNamed:@"Address_Cell_songda"];
    [_backview2 addSubview:Timeimage];
    [Timeimage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview2.mas_left).with.offset(18*kWidth);
        make.centerY.equalTo(_backview2.mas_top).with.offset(64.5*kHeight);
        make.size.mas_equalTo(CGSizeMake(17*kWidth, 13*kHeight));
    }];
    UILabel * timeLabel = [com createUIlabel:@"提车时间" andFont:FontOfSize14 andColor:fontGrayColor];
    [_backview2 addSubview:timeLabel];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(Timeimage.mas_right).with.offset(10*kWidth);
        make.centerY.equalTo(_backview2.mas_top).with.offset(64.5*kHeight);
    }];
    UIButton * btnDate = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDate setTitle:locationString forState:UIControlStateNormal];
    [btnDate setTitleColor:BtnTitleColor forState:UIControlStateNormal];
    [btnDate addTarget:self action:@selector(pressTimeBtn:) forControlEvents:UIControlEventTouchUpInside];
    btnDate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btnDate.tag = TimeTag;
    btnDate.titleLabel.font = Font(FontOfSize14);
    [_backview2 addSubview:btnDate];
    [btnDate mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeLabel.mas_right).with.offset(15*kWidth);
        make.centerY.equalTo(timeLabel.mas_centerY);
        make.width.mas_equalTo(Main_Width/3);
    }];
    
    NSDate *date = [NSDate date]; // 获得时间对象
    NSDateFormatter  *dateformatterTime=[[NSDateFormatter alloc] init];
    [dateformatterTime setDateFormat:@"HH:mm:ss"];
    NSString * string = [dateformatterTime stringFromDate:date];
    NSString *  b = [string substringWithRange:NSMakeRange(0,2)] ;
    NSString *  c = [string substringWithRange:NSMakeRange(2,3)] ;
    
    
    NSString * btnlabel = [NSString stringWithFormat:@"%@%@",b,c];
    [dicUrl setObject:btnlabel forKey:@"deliveryTime"];

    _btnTime = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnTime setTitle:btnlabel forState:UIControlStateNormal];
    [_btnTime setTitleColor:BtnTitleColor forState:UIControlStateNormal];
    [_btnTime addTarget:self action:@selector(pressPickCarTimeBtn:) forControlEvents:UIControlEventTouchUpInside];
    _btnTime.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _btnTime.titleLabel.font = Font(FontOfSize14);
    [_backview2 addSubview:_btnTime];
    [_btnTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(btnDate.mas_right);
        make.centerY.equalTo(timeLabel.mas_centerY);
        make.width.mas_equalTo(Main_Width/3);
    }];

    UIImageView * Timeimage1 = [[UIImageView alloc]init];
    Timeimage1.image = [UIImage imageNamed:@"Address_Cell_tiche"];
    [_backview2 addSubview:Timeimage1];
    [Timeimage1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview2.mas_left).with.offset(18*kWidth);
        make.centerY.equalTo(_backview2.mas_top).with.offset(64.5*kHeight+43*kHeight);
        make.size.mas_equalTo(CGSizeMake(17*kWidth, 13*kHeight));
    }];

    UILabel * timeLabel1 = [com createUIlabel:@"送达时间" andFont:FontOfSize14 andColor:fontGrayColor];
    timeLabel1.tag = 51;
    [_backview2 addSubview:timeLabel1];
    [timeLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(Timeimage1.mas_right).with.offset(10*kWidth);
        make.centerY.equalTo(_backview2.mas_top).with.offset(64.5*kHeight+43*kHeight);
    }];
    
    UILabel * arriveTime = [com createUIlabel:@"" andFont:FontOfSize14 andColor:BtnTitleColor];
    arriveTime.tag = 52;
    [_backview2 addSubview:arriveTime];
    [arriveTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(btnDate.mas_left);
        make.centerY.equalTo(timeLabel1.mas_centerY);
    }];
    
    UIView * Timeline1 = [[UIView alloc]init];
    Timeline1.backgroundColor = LineGrayColor;
    [timeView addSubview:Timeline1];
    [Timeline1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(timeLabel1.mas_centerY).with.offset(21.5*kHeight);
        make.left.mas_equalTo(Timeimage.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    
    UIImageView * Timeimage2 = [[UIImageView alloc]init];
    Timeimage2.image = [UIImage imageNamed:@"icon_infor_Mileage_imges"];
    [_backview2 addSubview:Timeimage2];
    [Timeimage2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview2.mas_left).with.offset(18*kWidth);
        make.centerY.equalTo(_backview2.mas_top).with.offset(64.5*kHeight+86*kHeight);
        make.size.mas_equalTo(CGSizeMake(15*kWidth, 15*kHeight));
    }];
    
    UILabel * timeLabel2 = [com createUIlabel:@"里程数" andFont:FontOfSize14 andColor:fontGrayColor];
    timeLabel2.tag = 51;
    [_backview2 addSubview:timeLabel2];
    [timeLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(Timeimage1.mas_right).with.offset(10*kWidth);
        make.centerY.equalTo(_backview2.mas_top).with.offset(64.5*kHeight+86*kHeight);
    }];

    UILabel * arriveDistance = [com createUIlabel:@"" andFont:FontOfSize14 andColor:BtnTitleColor];
    arriveDistance.tag = 53;
    [_backview2 addSubview:arriveDistance];
    [arriveDistance mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(arriveTime.mas_left);
        make.centerY.equalTo(timeLabel2.mas_centerY);
    }];

    
    UIButton * warnBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [warnBtn setImage:[UIImage imageNamed:@"warn"] forState:UIControlStateNormal];
    [warnBtn addTarget:self action:@selector(pressWarnBtn) forControlEvents:UIControlEventTouchUpInside];
    [_backview2 addSubview:warnBtn];
    [warnBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backview2.mas_right).with.offset(-20*kWidth);
//        make.left.equalTo(_btnTime.mas_left);
        make.centerY.equalTo(TimeLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(22*kWidth, 22*kHeight));
    }];

    _warnView = [self createWarnBack];

    // 当前顶层窗口
    UIWindow *window = [[UIApplication sharedApplication].windows lastObject];
    // 添加到窗口
    [window addSubview:_warnView];
    _warnView.hidden = YES;


    
    UIView * viewline =[[UIView alloc]init];
    viewline.backgroundColor = LineGrayColor;
    [_backview2 addSubview:viewline];
    [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview.mas_left).with.offset(18*kWidth);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        make.top.mas_equalTo(_backview2.mas_top).with.offset(43*2*kHeight);
    }];
    

    _backview3 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, 108*kHeight)];
    _backview3.backgroundColor = WhiteColor;
    [_backview addSubview:_backview3];
    [_backview3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left).with.offset(0);
        make.top.mas_equalTo(_backview2.mas_bottom).with.offset(8*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 108*kHeight));
    }];
    UIView * carView = [[UIView alloc]init];
    carView.backgroundColor = headerBackViewColor;
    [_backview3 addSubview:carView];
    [carView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview3.mas_left);
        make.top.mas_equalTo(_backview3.mas_top);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
    }];
    UIView * Carline = [[UIView alloc]init];
    Carline.backgroundColor = LineGrayColor;
    [carView addSubview:Carline];
    [Carline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(carView.mas_bottom);
        make.left.mas_equalTo(carView.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    UILabel * CarModel = [com createUIlabel:@"车辆信息" andFont:FontOfSize14 andColor:BlackTitleColor];
    [carView addSubview:CarModel];
    [CarModel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(carView.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(carView.mas_top).with.offset(21.5*kHeight);
    }];
    UIImageView * image = [[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"+"];
    [_backview3 addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview3.mas_left).with.offset(22*kWidth);
        make.size.mas_equalTo(CGSizeMake(40.5*kWidth, 40.5*kHeight));
        make.top.mas_equalTo(carView.mas_bottom).with.offset(13*kHeight);
    }];
    UILabel * labelSm1 = [com createUIlabel:@"选择托运车辆信息" andFont:FontOfSize13 andColor:FieldPlacerColor];
    [_backview3 addSubview:labelSm1];
    [labelSm1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(image.mas_right).with.offset(15*kWidth);
        make.top.mas_equalTo(carView.mas_bottom).with.offset(19*kHeight);
    }];
    UILabel * labelSm2 = [com createUIlabel:@"(可以选择多种车型的多辆车进行托运)" andFont:10.0 andColor:FieldPlacerColor];
    [_backview3 addSubview:labelSm2];
    [labelSm2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(image.mas_right).with.offset(15*kWidth);
        make.bottom.mas_equalTo(_backview3.mas_bottom).with.offset(-16*kHeight);
    }];
    UIButton * bigImage = [UIButton buttonWithType:UIButtonTypeCustom];
    bigImage.tag = 333;
    [bigImage addTarget:self action:@selector(pressCarAddBtn:) forControlEvents:UIControlEventTouchUpInside];
    [_backview3 addSubview:bigImage];
    [bigImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview3.mas_left);
        make.top.mas_equalTo(carView.mas_bottom).with.offset(13*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 55*kHeight));
    }];
    
    _CarTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, 43*0*kHeight) style:UITableViewStylePlain];
    [_CarTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _CarTable.tag = CarTag;
    _CarTable.delegate = self;
    _CarTable.dataSource = self;
    _CarTable.userInteractionEnabled = YES;
    _CarTable.scrollEnabled = NO;
    [_backview3 addSubview:_CarTable];
    [_CarTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview3.mas_left).with.offset(0);
        make.top.mas_equalTo(_backview3.mas_top).with.offset(43.5*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*0*kHeight));
    }];

    _backview4 = [[UIView alloc]init];
    _backview4.backgroundColor = WhiteColor;
    [_backview addSubview:_backview4];
    [_backview4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left).with.offset(0);
        make.top.mas_equalTo(_backview3.mas_bottom).with.offset(8*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 137*kHeight+86*kHeight));
    }];

    UILabel * carType = [com createUIlabel:@"汽车类型" andFont:FontOfSize14 andColor:BtnTitleColor];
    [_backview4 addSubview:carType];
    [carType mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview4.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_backview4.mas_top).with.offset(21.5*kHeight);
    }];
    UIButton * oldCar = [self createBtn:@"二手车" andBorderColor:LineGrayColor andFont:FontOfSize12 andTitleColor:littleBlackColor andBackColor:WhiteColor];
    oldCar.tag = 600;
    [oldCar addTarget:self action:@selector(pressOldOrNew:) forControlEvents:UIControlEventTouchUpInside];
    [_backview4 addSubview:oldCar];
    [oldCar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_backview.mas_right).with.offset(-15*kWidth);
        make.centerY.mas_equalTo(carType.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(52*kWidth, 23*kHeight));
    }];
    UIButton *  newCar = [self createBtn:@"新车" andBorderColor:YellowColor andFont:FontOfSize12 andTitleColor:YellowColor andBackColor:WhiteColor];
    [_backview4 addSubview:newCar];
    newCar.tag = 700;
    [newCar addTarget:self action:@selector(pressOldOrNew:) forControlEvents:UIControlEventTouchUpInside];
    
    [newCar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(oldCar.mas_left).with.offset(-8*kWidth);
        make.centerY.mas_equalTo(oldCar.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(52*kWidth, 23*kHeight));
    }];
    
    UIView * line40 =[[UIView alloc]init];
    line40.backgroundColor = LineGrayColor;
    [_backview4 addSubview:line40];
    [line40 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview4.mas_left).with.offset(18*kWidth);
        make.top.mas_equalTo(_backview4.mas_top).with.offset(43*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
    UILabel * carStatu = [com createUIlabel:@"汽车状态" andFont:FontOfSize14 andColor:BtnTitleColor];
    [_backview4 addSubview:carStatu];
    [carStatu mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview4.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(line40.mas_bottom).with.offset(21.5*kHeight);
    }];
    UIButton * canRun = [self createBtn:@"不能动" andBorderColor:LineGrayColor andFont:FontOfSize12 andTitleColor:littleBlackColor andBackColor:WhiteColor];
    canRun.tag = 800;
    [canRun addTarget:self action:@selector(pressRunOrNotRun:) forControlEvents:UIControlEventTouchUpInside];
    [_backview4 addSubview:canRun];
    [canRun mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_backview4.mas_right).with.offset(-15*kWidth);
        make.centerY.mas_equalTo(carStatu.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(52*kWidth, 23*kHeight));
    }];
    UIButton *  notRun = [self createBtn:@"能动" andBorderColor:YellowColor andFont:FontOfSize12  andTitleColor:YellowColor andBackColor:WhiteColor];
    [_backview4 addSubview:notRun];
    notRun.tag = 900;
    [notRun addTarget:self action:@selector(pressRunOrNotRun:) forControlEvents:UIControlEventTouchUpInside];
    
    [notRun mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(canRun.mas_left).with.offset(-8*kWidth);
        make.centerY.mas_equalTo(canRun.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(52*kWidth, 23*kHeight));
    }];

    UIView * line41 = [[UIView alloc]init];
    line41.backgroundColor = GrayColor;
    [_backview4 addSubview:line41];
    [line41 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview4.mas_left);
        make.top.mas_equalTo(line40.mas_bottom).with.offset(43*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 8*kHeight));
    }];

    UILabel * carMoney = [com createUIlabel:@"整体估值" andFont:FontOfSize14 andColor:BtnTitleColor];
    [_backview4 addSubview:carMoney];
    [carMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(line41.mas_bottom).with.offset(21.5*kHeight);
    }];
    
    _CarPromptField =[self createFieldMoney:@"" andTag:1 andFont:FontOfSize14];
    _CarPromptField.tag = 1000;
    _CarPromptField.textAlignment = NSTextAlignmentRight;
    [_CarPromptField addTarget:self action:@selector(safeFieldWithText:) forControlEvents:UIControlEventEditingChanged];
    [_CarPromptField addTarget:self action:@selector(safeFieldWithText1:) forControlEvents:UIControlEventEditingDidBegin];

    _CarPromptField.textAlignment = NSTextAlignmentRight;
    _CarPromptField.placeholder = @"必填(用于保险费计算)";
    _CarPromptField.returnKeyType = UIReturnKeyDone;
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, FieldPlacerColor.CGColor);
    _CarPromptField.font = Font(FontOfSize14);
    _CarPromptField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [_backview addSubview:_CarPromptField];
    [_CarPromptField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(carMoney.mas_centerY).with.offset(0.5*kHeight);
        make.left.mas_equalTo(carMoney.mas_right).with.offset(30*kWidth);
        make.width.mas_equalTo(Main_Width-175*kWidth);
        make.height.mas_equalTo(43*kHeight);
    }];
    
    UIButton * promptBtn = [self createBtn:@"" andBorderColor:nil andFont:13.0 andTitleColor:nil andBackColor:nil];
    [promptBtn setImage:[UIImage imageNamed:@"jinggao"] forState:UIControlStateNormal];
    promptBtn.layer.borderWidth = 0;
    promptBtn.layer.cornerRadius = 7.5*kHeight;
    [_backview4 addSubview:promptBtn];
    [promptBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backview4.mas_right).with.offset(-20*kWidth);
        make.centerY.mas_equalTo(carMoney.mas_centerY);
        make.size.mas_equalTo(CGSizeMake( 16*kWidth, 16*kHeight));
    }];
    
    UIButton * bigPromptBtn = [self createBtn:@"" andBorderColor:nil andFont:13.0 andTitleColor:nil andBackColor:nil];
    [bigPromptBtn addTarget:self action:@selector(pressPromptBtn:) forControlEvents:UIControlEventTouchUpInside];
    bigPromptBtn.layer.borderWidth = 0;
    [_backview4 addSubview:bigPromptBtn];
    [bigPromptBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backview.mas_right);
        make.centerY.mas_equalTo(carMoney.mas_centerY);
        make.size.mas_equalTo(CGSizeMake( 50*kWidth, 30*kHeight));
    }];
    UILabel * promptLabel = [com createUIlabel:@"万元" andFont:FontOfSize14 andColor:BtnTitleColor];
    [_backview4 addSubview:promptLabel];
    [promptLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(promptBtn.mas_left).with.offset(-5*kWidth);
        make.centerY.mas_equalTo(carMoney.mas_centerY);
    }];
    
    _prompt1 = [com createUIlabel:@"输入范围在1万到100亿之间" andFont:FontOfSize10 andColor:YellowColor];
    _prompt2 = [com createUIlabel:@"轿车估值用来购买保险，如果与实际车值不符，按照不足额保险处理及承担欺诈后果。" andFont:8.0 andColor:carScrollColor];
    _prompt1.hidden = YES;
    _prompt2.hidden = YES;
    [_backview addSubview:_prompt1];
    [_backview addSubview:_prompt2];
    [_prompt1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_backview.mas_right).with.offset(-15*kWidth);
        make.top.mas_equalTo(_backview4.mas_bottom).with.offset(5*kHeight);
    }];
    [_prompt2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_backview.mas_right).with.offset(-7*kWidth);
        make.top.mas_equalTo(_prompt1.mas_bottom).with.offset(5*kHeight);
    }];
    
    
    
    UIView * Shipline =[[UIView alloc]init];
    Shipline.backgroundColor = LineGrayColor;
    [_backview4 addSubview:Shipline];
    [Shipline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview4.mas_left).with.offset(18*kWidth);
        make.top.mas_equalTo(carMoney.mas_centerY).with.offset(21.5*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    //运输方式  是否加急
    
    UILabel * carShip = [com createUIlabel:@"运输方式" andFont:FontOfSize14 andColor:BtnTitleColor];
    [_backview4 addSubview:carShip];
    [carShip mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview4.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(Shipline.mas_centerY).with.offset(21.5*kHeight);
    }];
    UIButton * smallCar = [self createBtn:@"救援车" andBorderColor:LineGrayColor andFont:FontOfSize11 andTitleColor:littleBlackColor andBackColor:WhiteColor];
    smallCar.tag = SmallTag;
    [smallCar addTarget:self action:@selector(pressBigOrSmall:) forControlEvents:UIControlEventTouchUpInside];
    [_backview4 addSubview:smallCar];
    [smallCar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_backview.mas_right).with.offset(-15*kWidth);
        make.centerY.mas_equalTo(carShip.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(52*kWidth, 23*kHeight));
    }];
    UIButton *  bigCar= [self createBtn:@"大板车" andBorderColor:YellowColor andFont:FontOfSize11 andTitleColor:YellowColor andBackColor:WhiteColor];
    [_backview4 addSubview:bigCar];
    bigCar.tag = BigTag;
    [bigCar addTarget:self action:@selector(pressBigOrSmall:) forControlEvents:UIControlEventTouchUpInside];
    
    [bigCar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(smallCar.mas_left).with.offset(-8*kWidth);
        make.centerY.mas_equalTo(smallCar.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(52*kWidth, 23*kHeight));
    }];

    
    UIView * quetline =[[UIView alloc]init];
    quetline.backgroundColor = LineGrayColor;
    [_backview4 addSubview:quetline];
    [quetline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview4.mas_left).with.offset(18*kWidth);
        make.top.mas_equalTo(Shipline.mas_bottom).with.offset(43*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    
    
    
    
    UILabel * label = [com createUIlabel:@"是否加急" andFont:FontOfSize14 andColor:BtnTitleColor];
    [_backview4 addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(quetline.mas_centerY).with.offset(21.5*kHeight);
        make.left.equalTo(_backview4.mas_left).with.offset(18*kWidth);
    }];

    //是否加急
    
    SevenSwitch *mySwitch = [[SevenSwitch alloc] initWithFrame:CGRectZero];
    mySwitch.tag = InforTableTag+200;
//    mySwitch.backgroundColor = RedColor;
    mySwitch.center = CGPointMake(self.view.bounds.size.width-40*kWidth, 43*5*kHeight-15*kHeight);
    [mySwitch addTarget:self action:@selector(switchQuet:) forControlEvents:UIControlEventValueChanged];
    mySwitch.on = NO;
    //        NSLog(@"height%fwidth%f",mySwitch.frame.size.height,mySwitch.frame.size.width); 30 50
    mySwitch.onColor = YellowColor;
    [_backview4 addSubview:mySwitch];
    

    
    
    
    
    
    
    
    
    
    _backview5 = [[UIView alloc]init];
    _backview5.backgroundColor = WhiteColor;
    [_backview addSubview:_backview5];
    [_backview5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_backview.mas_centerX);
        make.top.mas_equalTo(_backview4.mas_bottom).with.offset(8*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 129*kHeight));
    }];
    UIView * carInforView = [[UIView alloc]init];
    carInforView.backgroundColor = headerBackViewColor;
    [_backview5 addSubview:carInforView];
    [carInforView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview5.mas_left);
        make.top.mas_equalTo(_backview5.mas_top);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
    }];
    UIView * CarInforline = [[UIView alloc]init];
    CarInforline.backgroundColor = LineGrayColor;
    [carInforView addSubview:CarInforline];
    [CarInforline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(carInforView.mas_bottom);
        make.left.mas_equalTo(carInforView.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    UILabel * labelInfor = [com createUIlabel:@"其他信息" andFont:FontOfSize14 andColor:BlackTitleColor];
    [carInforView addSubview:labelInfor];
    [labelInfor mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(carInforView.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(carInforView.mas_top).with.offset(21.5*kHeight);
    }];
    _InforTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, 86*kHeight) style:UITableViewStylePlain];
    [_InforTable setTableFooterView:[UIView new]];
    _InforTable.delegate = self;
    _InforTable.dataSource = self;
    _InforTable.tag = InforTableTag;
    [_backview5 addSubview:_InforTable];
    [_InforTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_backview.mas_centerX);
        make.top.mas_equalTo(carInforView.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 86*kHeight));
    }];
    
    UIView * shipBtnView = [[UIView alloc]init];
    shipBtnView.backgroundColor = GrayColor;
    [self.view addSubview:shipBtnView];
    [shipBtnView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_backview.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 70*kHeight));
        make.bottom.mas_equalTo(self.view.mas_bottom);
    }];

    UIButton * ShipBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [ShipBtn setTitle:@"确定发车" forState:UIControlStateNormal];
    [ShipBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
    [ShipBtn addTarget:self action:@selector(pressShipBtn:) forControlEvents:UIControlEventTouchUpInside];
    ShipBtn.layer.cornerRadius = 5;
    ShipBtn.layer.borderWidth = 0.5;
    ShipBtn.layer.borderColor = YellowColor.CGColor;
    ShipBtn.backgroundColor = YellowColor;
    [shipBtnView addSubview:ShipBtn];
    [ShipBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(shipBtnView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(293*kWidth, 40*kHeight));
        make.top.mas_equalTo(shipBtnView.mas_top).with.offset(15*kHeight);
    }];
}



-(void)switchQuet:(UISwitch*)mySwitch{
    

    if (mySwitch.on) {
        
        [dicUrl setObject:@"T" forKey:@"isUrgent"];//是否加急

        NSLog(@"1开");
        //            [dicUrl setObject:@"1" forKey:@"ispick"];
    }else{
        [dicUrl setObject:@"F" forKey:@"isUrgent"];//是否加急

        NSLog(@"1关");
        //            [dicUrl setObject:@"0" forKey:@"ispick"];
    }
}


-(void)pressBigOrSmall:(UIButton*)sender{
    
    UIButton * big = (UIButton*)[self.view viewWithTag:BigTag];
    UIButton * small = (UIButton*)[self.view viewWithTag:SmallTag];
    if (sender.tag == BigTag) {
        big.layer.borderColor = YellowColor.CGColor;
        [big setTitleColor:YellowColor forState:UIControlStateNormal];
        small.layer.borderColor = LineGrayColor.CGColor;
        [small setTitleColor:littleBlackColor forState:UIControlStateNormal];
        [dicUrl setObject:@"10" forKey:@"transportType"];
    }else{
        small.layer.borderColor = YellowColor.CGColor;
        [small setTitleColor:YellowColor forState:UIControlStateNormal];
        big.layer.borderColor = LineGrayColor.CGColor;
        [big setTitleColor:littleBlackColor forState:UIControlStateNormal];
        [dicUrl setObject:@"20" forKey:@"transportType"];
    }

    
}

-(void)pressWarnBack{
    
    _warnView.hidden = YES;
    
}
-(void)WarnBackTouch{
    
    _warnView.hidden = YES;
    
}
-(void)pressOldOrNew:(UIButton*)sender{
    //issencondhand  2 新车  1 旧车
    UIButton * old = (UIButton*)[self.view viewWithTag:600];
    UIButton * new = (UIButton*)[self.view viewWithTag:700];
    if (sender.tag == 600) {
        old.layer.borderColor = YellowColor.CGColor;
        [old setTitleColor:YellowColor forState:UIControlStateNormal];
        new.layer.borderColor = LineGrayColor.CGColor;
        [new setTitleColor:littleBlackColor forState:UIControlStateNormal];
        [dicUrl setObject:@"1" forKey:@"issencondhand"];
    }else{
        new.layer.borderColor = YellowColor.CGColor;
        [new setTitleColor:YellowColor forState:UIControlStateNormal];
        old.layer.borderColor = LineGrayColor.CGColor;
        [old setTitleColor:littleBlackColor forState:UIControlStateNormal];
        [dicUrl setObject:@"0" forKey:@"issencondhand"];
    }
}

-(void)pressWarnBtn{
    
  _warnView.hidden = NO;
    
}



-(void)pressPickCarBtn:(UIButton*)sender{
    
    
    for (int i=0; i<_InforHeaderCarArr.count; i++) {
        UIView * view = _InforHeaderCarArr[i];
        SevenSwitch *mySwitch = (SevenSwitch*)[view viewWithTag:InforTableTag+100+i];
        mySwitch.userInteractionEnabled = YES;
    }

    UIImageView * image1 = (UIImageView*)[_backview2 viewWithTag:PickCarImageTag];
    UIImageView * image2 = (UIImageView*)[_backview2 viewWithTag:PickCarImageTag+1];
    if (sender.tag == PickCarBtnTag) {
        image1.image = [UIImage imageNamed:@"selected"];
        image2.image = [UIImage imageNamed:@"unselect"];
        
        NSString * productid = [NSString stringWithFormat:@"%@",pickProductArr[0][@"productId"]];
        NSString * endTime = [NSString stringWithFormat:@"%@",pickProductArr[0][@"endTime"]];
        NSString * productName = [NSString stringWithFormat:@"%@",pickProductArr[0][@"productName"]];
        if ([productName isEqualToString:@"次日达"]) {
            for (int i=0; i<_InforHeaderCarArr.count; i++) {
                UIView * view = _InforHeaderCarArr[i];
                SevenSwitch *mySwitch = (SevenSwitch*)[view viewWithTag:InforTableTag+100+i];
                mySwitch.on = NO;
                [dicUrl setObject:@"0" forKey:@"ispick"];
                [dicUrl setObject:@"0" forKey:@"isdeliv"];
                mySwitch.userInteractionEnabled = NO;
            }
        }
        [dicUrl setObject:productid forKey:@"productid"];
        [dicUrl setObject:endTime forKey:@"arrivedate"];
    }else{
        image2.image = [UIImage imageNamed:@"selected"];
        image1.image = [UIImage imageNamed:@"unselect"];
        
        NSString * productid = [NSString stringWithFormat:@"%@",pickProductArr[1][@"productId"]];
        NSString * endTime = [NSString stringWithFormat:@"%@",pickProductArr[1][@"endTime"]];
        NSString * productName = [NSString stringWithFormat:@"%@",pickProductArr[1][@"productName"]];
        [dicUrl setObject:productid forKey:@"productid"];
//        [dicUrl setObject:[endTime substringWithRange:NSMakeRange(0, 10)] forKey:@"arrivedate"];
        [dicUrl setObject:endTime forKey:@"arrivedate"];

        if ([productName isEqualToString:@"次日达"]) {
            
            for (int i=0; i<_InforHeaderCarArr.count; i++) {
                UIView * view = _InforHeaderCarArr[i];
                SevenSwitch *mySwitch = (SevenSwitch*)[view viewWithTag:InforTableTag+100+i];
                mySwitch.userInteractionEnabled = NO;
                mySwitch.on = NO;
                [dicUrl setObject:@"0" forKey:@"ispick"];
                [dicUrl setObject:@"0" forKey:@"isdeliv"];
            }
        }
    }
}

-(void)pressRunOrNotRun:(UIButton*)sender{
    //ismobile  1 能动  0 不能
    UIButton * Run = (UIButton*)[self.view viewWithTag:900];
    UIButton * NotRun = (UIButton*)[self.view viewWithTag:800];
    if (sender.tag == 800) {
        
        NotRun.layer.borderColor = YellowColor.CGColor;
        [NotRun setTitleColor:YellowColor forState:UIControlStateNormal];
        Run.layer.borderColor = LineGrayColor.CGColor;
        [Run setTitleColor:littleBlackColor forState:UIControlStateNormal];
        [dicUrl setObject:@"0" forKey:@"ismobile"];
        
    }else{
        Run.layer.borderColor = YellowColor.CGColor;
        [Run setTitleColor:YellowColor forState:UIControlStateNormal];
        NotRun.layer.borderColor = LineGrayColor.CGColor;
        [NotRun setTitleColor:littleBlackColor forState:UIControlStateNormal];
        [dicUrl setObject:@"1" forKey:@"ismobile"];
    }
}

-(void)safeFieldWithText:(UITextField*)field{
    

//    estvalue
    if ([self textField:field shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:@""]) {
        [dicUrl setObject:field.text forKey:@"estvalue"];
    }
}
-(void)safeFieldWithText1:(UITextField*)field{
    
    [UIView animateWithDuration:0.3 animations:^{
        _scroll.contentOffset = CGPointMake(0, 438*kHeight);
    } completion:nil];

}
-(void)pressPromptBtn:(UIButton*)sender{

    if (!sender.selected) {
        _prompt1.hidden = NO;
        _prompt2.hidden = NO;
        sender.selected = YES;
        [_backview5 mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_backview4.mas_bottom).with.offset(35*kHeight);
        }];
        

        
    }else{
        _prompt1.hidden = YES;
        _prompt2.hidden = YES;
        sender.selected = NO;
        [_backview5 mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_backview4.mas_bottom).with.offset(8*kHeight);
        }];

    }
}

-(void)updataBtnStatu{
    //圆环btn按钮状态
    // 600二手 700新车 800不能动 900能动
    UIButton * btn1 = (UIButton*)[_backview4 viewWithTag:600];
    btn1.layer.borderColor = LineGrayColor.CGColor;
    [btn1 setTitleColor:littleBlackColor forState:UIControlStateNormal];
    btn1.backgroundColor = WhiteColor;
    
    UIButton * btn2 = (UIButton*)[_backview4 viewWithTag:700];
    btn2.layer.borderColor = YellowColor.CGColor;
    [btn2 setTitleColor:YellowColor forState:UIControlStateNormal];
    btn2.backgroundColor = WhiteColor;
    
    UIButton * btn3 = (UIButton*)[_backview4 viewWithTag:800];
    btn3.layer.borderColor = LineGrayColor.CGColor;
    [btn3 setTitleColor:littleBlackColor forState:UIControlStateNormal];
    btn3.backgroundColor = WhiteColor;
    
    UIButton * btn4 = (UIButton*)[_backview4 viewWithTag:900];
    btn4.layer.borderColor = YellowColor.CGColor;
    [btn4 setTitleColor:YellowColor forState:UIControlStateNormal];
    btn4.backgroundColor = WhiteColor;
    
    UIButton * btn5 = (UIButton*)[_backview4 viewWithTag:BigTag];
    UIButton * btn6 = (UIButton*)[_backview4 viewWithTag:SmallTag];
    btn5.layer.borderColor = YellowColor.CGColor;
    [btn5 setTitleColor:YellowColor forState:UIControlStateNormal];
    btn5.backgroundColor = WhiteColor;
    btn6.layer.borderColor = LineGrayColor.CGColor;
    [btn6 setTitleColor:littleBlackColor forState:UIControlStateNormal];
    btn6.backgroundColor = WhiteColor;

    
    
    
    //还原滑块状态
    for (int i=0; i<_InforHeaderCarArr.count; i++) {
        UIView * view = _InforHeaderCarArr[i];
        SevenSwitch *mySwitch = (SevenSwitch*)[view viewWithTag:InforTableTag+100+i];
        mySwitch.on = YES;
    }
    
    SevenSwitch *mySwitch = (SevenSwitch*)[_backview4 viewWithTag:InforTableTag+200];
    mySwitch.on = NO;

    
    
    [_CarTable reloadData];
}



-(void)pressCarAddBtn:(UIButton*)sender{
    
    AddCarTypeVC * addcartype = [[AddCarTypeVC alloc]init];
    addcartype.callBack = ^(NSMutableArray * carArr){
        
        
        for (int i=0; i<carArr.count; i++) {
            [_carInforArr addObject: carArr[i]];
        }
        CarNumber = (int)_carInforArr.count;
        [dicUrl setObject:_carInforArr forKey:@"vehiclesbrands"];
        [self refreshTatalCarsLabel];
        [self updateCarAndScrollFram:CarNumber];
        [self updateScrollFram:CarNumber];
    };
    if (_carInforArr.count <50) {
        [self.navigationController pushViewController:addcartype animated:YES];
    }else{
        [self createUIAlertController:@"添加车辆不能超过50台"];
    }
}

-(void)pressShipBtn:(UIButton*)sender{
    

    sender.userInteractionEnabled = NO;
    NSString * address1 = [NSString stringWithFormat:@"%@",_dataSouce[0][@"address"]];
    NSString * address2 = [NSString stringWithFormat:@"%@",_dataSouce[1][@"address"]];
    
    if (address1.length==0||address2.length==0) {
        sender.userInteractionEnabled = YES;
        [self createUIAlertController:@"请填完整地址信息"];

    }else{
        NSLog(@"receiptaddrid===%@",_dataSouce[0][@"id"]);
        NSLog(@"departaddrid===%@",_dataSouce[1][@"id"]);

 
        if (chooseAddressYesOrNo) {
            
            [dicUrl setObject:_dataSouce[1][@"id"] forKey:@"departaddrid"];
            [dicUrl setObject:_dataSouce[0][@"id"] forKey:@"receiptaddrid"];
            
        }else{
            [dicUrl setObject:_dataSouce[0][@"id"] forKey:@"departaddrid"];
            [dicUrl setObject:_dataSouce[1][@"id"] forKey:@"receiptaddrid"];
 
        }
        

        NSString * carinforLength = [NSString stringWithFormat:@"%@",dicUrl[@"vehiclesbrands"]];
        if (carinforLength.length == 0) {
            sender.userInteractionEnabled = YES;
            [self createUIAlertController:@"请选择车辆信息"];
        }else{
//            if ([Common compareDate:date1 withDate:date2]) {
                NSString * extvalue = [NSString stringWithFormat:@"%@",dicUrl[@"estvalue"]];
                if (![Common validateUserAge:extvalue]) {
                    sender.userInteractionEnabled = YES;
                    [self createUIAlertController:@"请填写正确汽车估值"];
                }else{
                    NSLog(@"%@",dicUrl);
                    NSMutableArray * carinforarr = dicUrl[@"vehiclesbrands"];
                    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:carinforarr options:NSJSONWritingPrettyPrinted error:nil];
                    NSString * jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
                    
                    [dicUrl setObject:jsonString forKey:@"vehiclesbrands"];
                    WKProgressHUD * hud = [WKProgressHUD showInView:self.view withText:@"请稍等" animated:YES];

                    [com afPostRequestWithUrlString:Order_Addorder_Url parms:dicUrl finishedBlock:^(id responseObj) {
                        NSMutableDictionary * dicObj =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];

                        if ([dicObj[@"success"] boolValue]) {
                            
                            FeeDetailVC *feeVC = [[FeeDetailVC alloc]init];
                            feeVC.orderId = dicObj[@"data"][@"id"];
                            ConfirmCarVC * confirmcarVC = [[ConfirmCarVC alloc]init];
                            confirmcarVC.moneyDic = dicObj[@"data"];
                            sender.userInteractionEnabled = YES;
                            [hud dismiss:YES];
                            [self.navigationController pushViewController:confirmcarVC animated:YES];
                            
                            confirmcarVC.callBack = ^(BOOL  yes){
                                if (yes) {
                                    [dicUrl setObject:@"" forKey:@"departaddrid"];//发运地址id
                                    [dicUrl setObject:@"" forKey:@"receiptaddrid"];//目的地址id
                                    [dicUrl setObject:@"" forKey:@"deliverydate"];//提车日期
                                    [dicUrl setObject:@"" forKey:@"deliveryTime"];
                                    [dicUrl setObject:@"" forKey:@"arrivedate"];//到达日期
                                    [dicUrl setObject:@"" forKey:@"vehiclesbrands"];//cars
                                    [dicUrl setObject:@"1" forKey:@"ispick"];//是否上门提车
                                    [dicUrl setObject:@"1" forKey:@"isdeliv"];//是否送车上门
                                    [dicUrl setObject:@"0" forKey:@"issencondhand"];//二手车
                                    [dicUrl setObject:@"1" forKey:@"ismobile"];//能动
                                    [dicUrl setObject:@"" forKey:@"estvalue"];//估值
                                    [dicUrl setObject:@"" forKey:@"productid"];//估值
                                    [dicUrl setObject:@"F" forKey:@"isUrgent"];//是否加急
                                    [dicUrl setObject:@"10" forKey:@"transportType"];//运输方式
                                    

                                    chooseAddressYesOrNo = NO;
                                    pickCarHeight = NO;
                                    _CarPromptField.text = @"";
                                    [self updataBtnStatu];
                                    [_carInforArr removeAllObjects];
                                    [_dataSouce removeAllObjects];
                                    [self createData];
                                    CarNumber = 0;
                                    [self updateScrollFram:0];
                                    [self updateCarAndScrollFram:0];
                                    [_AddressTable reloadData];
                                    [_CarTable reloadData];
                                }
                            };
                        }else{
                            [hud dismiss:YES];
                            [dicUrl setObject:_carInforArr forKey:@"vehiclesbrands"];
                            sender.userInteractionEnabled = YES;
                            [self createUIAlertController:dicObj[@"message"]];
                        }
                    } failedBlock:^(NSString *errorMsg) {
                        sender.userInteractionEnabled = YES;
                        [self createUIAlertController:errorMsg];
                        [dicUrl setObject:_carInforArr forKey:@"vehiclesbrands"];
//                        [hud dismiss:YES];
                    }];
                }
            }

    }
}

#pragma mark tableView cellForRowAtIndexPath

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *str=@"str";
    if (tableView.tag== AddressTag) {
        AddressCell * cell =  [tableView dequeueReusableCellWithIdentifier:str];
        if (cell ==nil) {
            cell = [[AddressCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }
        if (indexPath.row==0) {
            cell.SmallImage.image = [UIImage imageNamed:@"Address_Cell_fache"];
            for (int i =0; i<_dataSouce.count; i++) {
                NSString *addressType = [NSString stringWithFormat:@"%@", _dataSouce[i][@"addressType"]];
                NSString * address = [NSString stringWithFormat:@"%@",_dataSouce[i][@"address"]];
                // 0 是发货
                BOOL isZero = [addressType isEqualToString:@"0"];
                if (isZero && address.length!=0) {
                    PlaceModel = [PlaceOrderModel ModelWithDic:_dataSouce[i]];
                    NSString * cityName = nil;
                    
                    if ([PlaceModel.provinceName isEqualToString:PlaceModel.cityName]) {
                        
                        cityName = [NSString stringWithFormat:@"%@%@",PlaceModel.provinceName,PlaceModel.countyName];
                    }else{
                        cityName = [NSString stringWithFormat:@"%@%@%@",PlaceModel.provinceName,PlaceModel.cityName,PlaceModel.countyName];
                    }
                    
                    cell.CityName.text = cityName;
//                    cell.StoreName.text = PlaceModel.unitName;
                    cell.StoreName.text = @"";
                    cell.AddressName.text = PlaceModel.address;
                    NSString * people = [NSString stringWithFormat:@"%@ %@",PlaceModel.contact,PlaceModel.phone];
                    cell.PeopleName.text = people;
                    cell.viewSline.hidden = NO;
                    cell.CityName.textColor = littleBlackColor;
                    break;
                }else{
                    cell.viewSline.hidden = YES;
                    cell.CityName.text = @"起运地";
                    cell.CityName.textColor = FieldPlacerColor;
                    cell.StoreName.text = @"";
                    cell.AddressName.text = @"";
                    cell.PeopleName.text = @"";
                }
            }
        }
        else{
            cell.viewline.hidden = YES;
            cell.SmallImage.image = [UIImage imageNamed:@"Address_Cell_shouche"];
            for (int i =0; i<_dataSouce.count; i++) {
                NSString * address = [NSString stringWithFormat:@"%@",_dataSouce[i][@"address"]];
                if ([_dataSouce[i][@"addressType"] isEqualToString:@"1"]&&address.length!=0) {
                    PlaceModel = [PlaceOrderModel ModelWithDic:_dataSouce[i]];
                    NSString * cityName = nil;
                    
                    if ([PlaceModel.provinceName isEqualToString:PlaceModel.cityName]) {
                        
                        cityName = [NSString stringWithFormat:@"%@%@",PlaceModel.provinceName,PlaceModel.countyName];
                    }else{
                        cityName = [NSString stringWithFormat:@"%@%@%@",PlaceModel.provinceName,PlaceModel.cityName,PlaceModel.countyName];
                    }
                    
                    cell.CityName.text = cityName;
//                    cell.StoreName.text = PlaceModel.unitName;
                    cell.StoreName.text = @"";
                    cell.AddressName.text = PlaceModel.address;
                    NSString * people = [NSString stringWithFormat:@"%@ %@",PlaceModel.contact,PlaceModel.phone];
                    cell.PeopleName.text = people;
                    cell.viewSline.hidden = NO;
                    cell.CityName.textColor = littleBlackColor;
                    break;
                }else{
                    cell.viewSline.hidden = YES;
                    cell.CityName.text = @"目的地";
                    cell.CityName.textColor = FieldPlacerColor;
                    cell.StoreName.text = @"";
                    cell.AddressName.text = @"";
                    cell.PeopleName.text = @"";
                }
        }
    }
        if (_dataSouce.count==0) {
            cell.viewSline.hidden = YES;
        }
        [_AddressTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (tableView.tag == InforTableTag){
        
        placeAddressCell * cell =  [tableView dequeueReusableCellWithIdentifier:str];
        if (cell ==nil) {
            cell = [[placeAddressCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }
        return cell;
    }
    else{
        CarCell * cell =  [tableView dequeueReusableCellWithIdentifier:str];
        if (cell ==nil) {
            cell = [[CarCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSString * brandLogo = [NSString stringWithFormat:@"%@",_carInforArr[indexPath.row][@"brandLogo"]];
        cell.SmallImage.contentMode = UIViewContentModeScaleAspectFit;//图片缩放适应imageView大小
        [cell.SmallImage sd_setImageWithURL:[NSURL URLWithString:brandLogo] placeholderImage:[UIImage imageNamed:@"car_style1"]];
        cell.CarName.text = [NSString stringWithFormat:@"%@-%@",_carInforArr[indexPath.row][@"brandname"],_carInforArr[indexPath.row][@"vehiclename"]];
        cell.CarNumber.text = [NSString stringWithFormat:@"%@辆",_carInforArr[indexPath.row][@"vehiclecount"]];
        cell.deleteBtn.tag = indexPath.row;
        [cell.deleteBtn addTarget:self action:@selector(pressCarCellDeleteBtn:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
}

-(void)pressCarCellDeleteBtn:(UIButton *)sender{
    
    CarNumber --;
    [_carInforArr removeObjectAtIndex:sender.tag];
    if (_carInforArr.count==0) {
        [dicUrl setObject:@"" forKey:@"vehiclesbrands"];
    }
//    [self updateFristScrollFram:addressHigth];
//    [_CarTable reloadData];

    [self updateCarAndScrollFram:CarNumber];
    [self updateScrollFram:CarNumber];
    [_CarTable reloadData];
}



#pragma mark tableView dataSource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag == AddressTag) {
        if (indexPath.row == 0) {
            if (_dataSouce.count>0) {
                CGFloat height = 43*kHeight;
                for (int i=0; i<_dataSouce.count;i++) {
                    if ([_dataSouce[i][@"addressType"] isEqualToString:@"0"]&&![_dataSouce[i][@"address"] isEqualToString:@""]) {
                        height = addressCellHight1;
                    }
                }
                return height;
            }else{
                return 43*kHeight;
            }
        }else{
            if (_dataSouce.count>0) {
                CGFloat height = 43*kHeight;
                for (int i=0; i<_dataSouce.count;i++) {
                    if ([_dataSouce[i][@"addressType"] isEqualToString:@"1"]&&![_dataSouce[i][@"address"] isEqualToString:@""]) {
                        height = addressCellHight2;
                    }
                }
                return height;
            }else{
                return 43*kHeight;
            }
        }
    }else if (tableView.tag == InforTableTag){
        return 0;
    }else{
        return 43*kHeight;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return (tableView.tag == InforTableTag)?43*kHeight:0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView.tag == AddressTag)  return 2;
    else if (tableView.tag == InforTableTag)  return 2;
    else  return CarNumber;
}
//设置分区个数
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
   return (tableView.tag == InforTableTag)? 2:1;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag == AddressTag) {
        //跳转到地址选择列表  1:收车 0:发车
        if (pushIsOk) {
            pushIsOk = NO;
            (indexPath.row == 1)?[self navAddressView:@"1"]:[self navAddressView:@"0"];
        }
    }
}

#pragma mark tableView Header Footer

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView.tag == InforTableTag) {
        return _InforHeaderCarArr[section];
    }
    return nil;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    if (tableView.tag == CarTag) {
        UIView * view = [[UIView alloc]init];
        view.frame = CGRectMake(0, 0, Main_Width, 43*kHeight);
        UILabel * label = [com createUIlabel:@"添加车型" andFont:FontOfSize14 andColor:BlackTitleColor];
        [view addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(view.mas_centerX);
            make.centerY.mas_equalTo(view.mas_centerY);
        }];
        UIButton * AddBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        AddBtn.tag = 444;
        [AddBtn addTarget:self action:@selector(pressCarAddBtn:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:AddBtn];
        [AddBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(view.mas_centerX);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 40*kHeight));
            make.centerY.mas_equalTo(view.mas_centerY);
        }];
        
        UIImageView * addImage = [[UIImageView alloc]init];
        addImage.image = [UIImage imageNamed:@"jia"];
        [view addSubview:addImage];
        [addImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(label.mas_left).with.offset(-9*kWidth);
            make.size.mas_equalTo(CGSizeMake(17*kWidth, 17*kHeight));
            make.centerY.mas_equalTo(AddBtn.mas_centerY);
        }];
        return view;
    }
    else{
        UIView * view = [[UIView alloc]init];
        view.frame = CGRectMake(0, 0, 0, 0);
        return view;
    }
}
-(void)createInforHeaderView{
    
    NSArray * arr = @[@"上门提车 (收取相关费用)",
                      @"送车上门 (收取相关费用)"];
    for (int i=0; i<arr.count; i++) {
        UIView * view = [[UIView alloc]init];
        view.backgroundColor = WhiteColor;
        view.frame = CGRectMake(0, 0, Main_Width, 43*kHeight);
        UILabel * label = [com createUIlabel:@"" andFont:FontOfSize14 andColor:BtnTitleColor];
        [view addSubview:label];
        if (i==0) {
            UIView * line = [[UIView alloc]init];
            line.backgroundColor = LineGrayColor;
            [view addSubview:line];
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(view.mas_bottom);
                make.left.equalTo(view.mas_left).with.offset(18*kWidth);
                make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
            }];
        }
        NSString * string = [NSString stringWithFormat:@"%@",arr[i]];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
        //上门提车
        [str addAttribute:NSForegroundColorAttributeName value:BtnTitleColor range:NSMakeRange(0,5)];
        [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:FontOfSize14*kWidth] range:NSMakeRange(0, 5)];
        //(收取相关费用)
        [str addAttribute:NSForegroundColorAttributeName value:FieldPlacerColor range:NSMakeRange(5,8)];
        [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:FontOfSize14*kWidth] range:NSMakeRange(5, 8)];
        label.attributedText = str;
        [view  addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(view.mas_centerY);
            make.left.equalTo(view.mas_left).with.offset(18*kWidth);
        }];
        
        SevenSwitch *mySwitch = [[SevenSwitch alloc] initWithFrame:CGRectZero];
        mySwitch.tag = InforTableTag+100+i;
        mySwitch.center = CGPointMake(self.view.bounds.size.width-40*kWidth, view.bounds.size.height * 0.5);
        [mySwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
        mySwitch.on = YES;
//        NSLog(@"height%fwidth%f",mySwitch.frame.size.height,mySwitch.frame.size.width); 30 50
        mySwitch.onColor = YellowColor;
        [view  addSubview:label];
        [view addSubview:mySwitch];
        [_InforHeaderCarArr addObject:view];
    }
}

-(void)switchChanged:(UISwitch*)mySwitch{
    if (mySwitch.tag == InforTableTag+100) {
        if (mySwitch.on) {
            NSLog(@"1开");
            [dicUrl setObject:@"1" forKey:@"ispick"];
        }else{
            NSLog(@"1关");
            [dicUrl setObject:@"0" forKey:@"ispick"];
        }
    }else{
        if (mySwitch.on) {
            NSLog(@"2开");
            [dicUrl setObject:@"1" forKey:@"isdeliv"];
        }else{
            NSLog(@"2关");
            [dicUrl setObject:@"0" forKey:@"isdeliv"];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return (tableView.tag == CarTag)?43*kHeight:0;
}

-(void)judgementKeepAddress:(NSString*) addresstype{
    
    NSString * string = [NSString stringWithFormat:@"%@?addresstype=%@",addressUser_Url,addresstype];
    [Common requestWithUrlString:string contentType:@"application/json" finished:^(id responseObj) {
        
        NSMutableArray * addressArr = responseObj[@"data"];
        keepAddressNumber = (int)[addressArr count];

    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
}

#pragma mark 刷新数据和UI

-(void)updateScrollFram:(NSInteger)number{
    
    CGFloat totalHeight ;
    CGFloat addHegight = 0;
    if (pickCarHeight) {
        
     totalHeight = addressCellHight1+addressCellHight2;
        addHegight = 86*kHeight;
//        totalHeight = addressCellHight1+addressCellHight2;

        
    }else{//86  129
    
     totalHeight = addressCellHight1+addressCellHight2;
        addHegight = 0*kHeight;

    }
    [_backview mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(43*(_CarName.count+number+1)*kHeight+Main_height+totalHeight+70*kHeight+addHegight);
    }];
    _scroll.contentSize = CGSizeMake(Main_Width, Main_height+43*(_CarName.count+number+1)*kHeight+totalHeight+70*kHeight+addHegight);
}
-(void)updateFristScrollFram:(CGFloat)totalHeight{
    
    if (pickCarHeight) {
        
        totalHeight = totalHeight;
//        totalHeight = totalHeight+43*kHeight;
    }else{//86  129
        
        totalHeight = totalHeight;
        
    }
    [_backview mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(Main_height+totalHeight+120*kHeight);
    }];
    _scroll.contentSize = CGSizeMake(Main_Width, Main_height+totalHeight+110*kHeight+86*kHeight);
}

-(void)updateAddressTable:(CGFloat)totalHeight{
    
    [_AddressTable mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(totalHeight);
    }];
    [_backview1 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(totalHeight+43*kHeight);
    }];
}

-(void)updateCarAndScrollFram:(int)number{
    
    if (number==0) {
        [_CarTable mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0);
        }];
        [_backview3 mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(108*kHeight);
        }];
    }else{
        [_CarTable mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(43*(number+1)*kHeight);
        }];
        [_backview3 mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(43*(number+2)*kHeight);
        }];
    }
    [_CarTable reloadData];
    [_backview mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(43*(_CarName.count+SwitchNumber+number)*kHeight+Main_height+86*kHeight+addressHigth+86*kHeight);
    }];
    _scroll.contentSize = CGSizeMake(Main_Width, Main_height+43*(_CarName.count+SwitchNumber+number)*kHeight+86*kHeight+addressHigth+86*kHeight);
}

-(void)refreshTatalCarsLabel{
    NSInteger tatalLabel = 0;
    for (int i =0; i<_carInforArr.count; i++) {
        tatalLabel = tatalLabel+[_carInforArr[i][@"vins"] count];
    }
    NSString * Number = [NSString stringWithFormat:@"%ld辆",(long)tatalLabel];
    UILabel * label = (UILabel *)[_backview4 viewWithTag:TatalLabelTag];
    label.text = Number;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark UIPickerView

//提车时间 年月日
-(void)pressTimeBtn:(UIButton*)sender{
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    ActionSheetPicker *actionSheet = [[ActionSheetPicker alloc] initWithTarget:self successAction:@selector(selectOK:) cancelAction:nil origin:datePicker];
    datePicker.datePickerMode=UIDatePickerModeDate;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    //最小年月日（小于它，自动调回）
    NSDate* minDate = [NSDate date];
    //最大年月日（今天）
//    NSDate* maxDate = [formatter dateFromString:@"2019-01-01"];
    datePicker.minimumDate = minDate;
//    datePicker.maximumDate = maxDate;
    CGRect pickerFrame = CGRectMake(0, 40, actionSheet.viewSize.width, 216);
    [datePicker setFrame:pickerFrame];
    
    datePicker.datePickerMode = UIDatePickerModeDate;
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];//设置为简体中文显示
    datePicker.locale = locale;
    actionSheet.pickerView = datePicker;
    actionSheet.title = @"";
    
    UIButton *leftBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setTitle:@"取消"forState:UIControlStateNormal];
    leftBtn.frame=CGRectMake(0, 0, 50, 45);
    [leftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *leftIte=[[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    
    UIButton *rightBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
    rightBtn.frame=CGRectMake(0, 0, 50, 45);
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightIte=[[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    [actionSheet setDoneButton:rightIte];
    [actionSheet setCancelButton:leftIte];
    [actionSheet showActionSheetPicker];
}
- (void)selectOK:(id)sender{
    if ([sender isKindOfClass:[UIDatePicker class]]) {

        UIDatePicker *picker = sender;
        NSDate * date = [picker date];
        
        //显示2015-07-23 的日期
        NSString *time = [[date description] substringWithRange:NSMakeRange(0, 10)];
        NSString *string = [NSString stringWithFormat:@"%@",time];
        
        NSDate *  senddate=[NSDate date];
        NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
        [dateformatter setDateFormat:@"yyyy-MM-dd"];
        NSString *  locationString=[dateformatter stringFromDate:senddate];

        
        UIButton * btn = (UIButton*)[_backview viewWithTag:TimeTag];

        [btn setTitle:string forState:UIControlStateNormal];
        [dicUrl setObject:string forKey:@"deliverydate"];


        _adate = string;

        [self judgementAddress];

    }
}



-(void)selectTime:(id)sender{
    
    UIPickerView *pick = sender;//105 106
    NSInteger row = [pick selectedRowInComponent:0];
    NSInteger row1=[pick selectedRowInComponent:1];
    
    NSString *value=[_timeChoose1 objectAtIndex:row];
    NSString *value1=[_timeChoose2 objectAtIndex:row1];
    
    NSString *education=[NSString stringWithFormat:@"%@:%@",value,value1];
    NSLog(@"education===%@",education);
    NSDate *date = [NSDate date]; // 获得时间对象
    NSDateFormatter  *dateformatterTime=[[NSDateFormatter alloc] init];
    [dateformatterTime setDateFormat:@"HH:mm:ss"];
    NSString * string = [dateformatterTime stringFromDate:date];
    
    
    
    int  b = [[string substringWithRange:NSMakeRange(0,2)] intValue];
    int  c = [[string substringWithRange:NSMakeRange(2,2)] intValue];
    
    int d = [value intValue];
    int e = [value1 intValue];
    
    
    //显示2015-07-23 的日期
    NSString *time = [[date description] substringWithRange:NSMakeRange(0, 10)];
    NSString *NowDate = [NSString stringWithFormat:@"%@",time];
    
    if ([NowDate isEqualToString:dicUrl[@"deliverydate"]]) {
        
        if (d<b || e<c) {
            [self createUIAlertController:@"请选择正确时间"];
            return;
        }else{
            [_btnTime setTitle:education forState:UIControlStateNormal];
            [dicUrl setObject:education forKey:@"deliveryTime"];
        }
    }else{
        [_btnTime setTitle:education forState:UIControlStateNormal];
        [dicUrl setObject:education forKey:@"deliveryTime"];
    }
    
    
    [self judgementAddress];
    NSLog(@"选择的时间点:%@",education);
}


-(void)pressPickCarTimeBtn:(UIButton*)btn{
    
    UIPickerView *education = [[UIPickerView alloc] init];
    ActionSheetPicker *actionSheet=[[ActionSheetPicker alloc]initWithTarget:self successAction:@selector(selectTime:) cancelAction:nil origin:education];
    CGRect pickerFrame = CGRectMake(0, 40, actionSheet.viewSize.width, 216);
    [education setFrame:pickerFrame];
    education.tag = PickCarTimeTag; //105 106
    education.delegate = self;
    education.dataSource = self;
    actionSheet.pickerView = education;
    actionSheet.title = @"标题";
    UIButton *leftBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setTitle:@"取消"forState:UIControlStateNormal];
    leftBtn.frame=CGRectMake(0, 0, 50, 45);
    [leftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *leftIte=[[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    UIButton *rightBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
    rightBtn.frame=CGRectMake(0, 0, 50, 45);
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightIte=[[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    [actionSheet setDoneButton:rightIte];
    [actionSheet setCancelButton:leftIte];
    [actionSheet showActionSheetPicker];
}

#pragma mark UIPickerView dataSource

//点击每一行cell  跳出的pickerview  显示列数
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

//显示pickerview 每个视图的行数
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case 0:
        {
            return _timeChoose1.count;
        }
            break;
        case 1:
        {
            return _timeChoose2.count;
        }
        default:
            break;
    }
    
    return 0;}
//显示每个pickerview  每行的具体内容
-(UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *lable  = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
    lable.adjustsFontSizeToFitWidth = YES;
    lable.textAlignment = NSTextAlignmentCenter;
    if (component == 0) {
        lable.text = _timeChoose1[row];
    }else{
        lable.text = _timeChoose2[row];
    }
    return lable;

}

-(void)navAddressView:(NSString*)addresstype {
    
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    if ([addresstype isEqualToString:@"1"]) {
        [dic setObject:@"1" forKey:@"addresstype"];//收车
    }else{
        [dic setObject:@"0" forKey:@"addresstype"];//发车
    }
    NSLog(@"%@",addressUser_Url);
    NSString * string = [NSString stringWithFormat:@"%@?addresstype=%@",addressUser_Url,dic[@"addresstype"]];
    
    [Common requestWithUrlString:string contentType:@"application/json" finished:^(id responseObj) {
        NSMutableArray * dataSource = [[NSMutableArray alloc]init];
        dataSource = responseObj[@"data"];
        if (dataSource == nil || dataSource.count==0 ) {
            [self gotoAddAddressView:addresstype];
        }else {
            [self gotoListAddressView:addresstype];
        }
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
   
}
//跳转到添加页面
-(void)gotoAddAddressView:(NSString *)addresstype {
    AddressVC * address = [[AddressVC alloc]init];
    address.addresstype = addresstype;
    address.navtitle = @"2";
    __weak PlaceOrderVC * weekSelf = self;
    
    address.callBackAddAddress=^(NSMutableDictionary * dicBlock,NSString * addresstype){
        if ([addresstype isEqualToString:@"1"]) {
            for (int i = 0; i<_dataSouce.count; i++) {
                if ([weekSelf.dataSouce[i][@"addressType"] isEqualToString:@"1"]) {
                    [weekSelf.dataSouce replaceObjectAtIndex:i withObject:dicBlock] ;
                    addressCellHight2 = 100*kHeight;
                }
            }
        }
        else{
            NSLog(@"weekSelf.dataSouce==%@",weekSelf.dataSouce);
            for (int i = 0; i<weekSelf.dataSouce.count; i++) {
                if ([_dataSouce[i][@"addressType"] isEqualToString:@"0"]) {
                    [weekSelf.dataSouce replaceObjectAtIndex:i withObject:dicBlock] ;
                    addressCellHight1 = 100*kHeight;
                }
            }
        }
        
        CGFloat totalHeight = addressCellHight1 +addressCellHight2;
        [weekSelf judgementAddress];
        [self updateScrollFram:_carInforArr.count];
        [self updateAddressTable:totalHeight];
        NSLog(@"%f",_backview.frame.size.height);

//        [weekSelf.AddressTable reloadData];
    };
    
    [self.navigationController pushViewController:address animated:YES];
}
//跳转到list页面
-(void)gotoListAddressView:(NSString*)addresstype {
    ChooseAddressVC * address = [[ChooseAddressVC alloc]init];
    if ([addresstype isEqualToString:@"1"]) {
        address.addresstype = @"1"; //收货
    }else{
        address.addresstype = @"0"; //发货
    }
    
    __weak PlaceOrderVC * weekSelf = self;
    NSLog(@"weekSelf.dataSouce==%@",weekSelf.dataSouce);
    
    address.callRemoveBack = ^(NSString * removeAddress){
        
        for (int i =0; i<_dataSouce.count; i++) {
            
            NSLog(@"%@",weekSelf.dataSouce[i]);
            NSString * carID = [NSString stringWithFormat:@"%@",weekSelf.dataSouce[i][@"id"]];
            if ([carID isEqualToString:removeAddress]) {
                NSMutableDictionary *departaddrid = [[NSMutableDictionary alloc]init];
                if ([weekSelf.dataSouce[i][@"addressType"] isEqualToString:@"0"]){
                    [departaddrid setValue:@"0" forKey:@"addressType"];
                    [departaddrid setValue:@"" forKey:@"address"];
                    addressCellHight1 = 43*kHeight;
                }else{
                    [departaddrid setValue:@"1" forKey:@"addressType"];
                    [departaddrid setValue:@"" forKey:@"address"];
                    addressCellHight2 = 43*kHeight;
                }
                
                [_dataSouce replaceObjectAtIndex:i withObject:departaddrid];
                [weekSelf judgementAddress];
                
                
                addressHigth = addressCellHight1+addressCellHight2;
                [self updateAddressTable:addressHigth];
                pickCarHeight = NO;
                [self updateScrollFram:(int)_carInforArr.count];
                [weekSelf.AddressTable reloadData];
            }
        }
    };
    address.callBack=^(NSMutableDictionary * dicBlock,NSString * addresstype){
        
        // 0 是发货  1 是收
        if ([addresstype isEqualToString:@"1"]) {
            addressCellHight1 = 100*kHeight;
            for (int i = 0; i<_dataSouce.count; i++) {
                NSString * address = [NSString stringWithFormat:@"%@",_dataSouce[i][@"address"]];
                if (address.length == 0 && [_dataSouce[i][@"addressType"] isEqualToString:@"0"]) {
                    addressCellHight1 = 43*kHeight;
                }
                if ([weekSelf.dataSouce[i][@"addressType"] isEqualToString:@"1"]) {
                    [weekSelf.dataSouce replaceObjectAtIndex:i withObject:dicBlock] ;
                    addressCellHight2 = 100*kHeight;
                }
            }
        }
        else{
            NSLog(@"weekSelf.dataSouce==%@",weekSelf.dataSouce);
            addressCellHight2 = 100*kHeight;
            for (int i = 0; i<_dataSouce.count; i++) {
                NSString * address = [NSString stringWithFormat:@"%@",_dataSouce[i][@"address"]];
                if (address.length == 0 && [_dataSouce[i][@"addressType"] isEqualToString:@"1"]) {
                    addressCellHight2 = 43*kHeight;
                }
                if ([weekSelf.dataSouce[i][@"addressType"] isEqualToString:@"0"]) {
                    [weekSelf.dataSouce replaceObjectAtIndex:i withObject:dicBlock] ;
                    addressCellHight1 = 100*kHeight;
                }
            }
        }
        [weekSelf judgementAddress];
        
        addressHigth = addressCellHight1 +addressCellHight2;
        [self updateScrollFram:(int)_carInforArr.count];
        [self updateAddressTable:addressHigth];
        [weekSelf.AddressTable reloadData];
    };
    [self.navigationController pushViewController:address animated:YES];
}


#pragma mark 系统提示框  自定义键盘 自定义按钮 自定义文本
-(void)createUIAlertControllerWithTitle:(NSString*)HeaderTitle and:(NSString*)title
{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:HeaderTitle message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)createUIAlertController:(NSString*)title
{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[PlaceOrderVC class]]) {
                [self.navigationController popToViewController:controller animated:YES];
            }
        }
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}
//点击return按钮 键盘隐藏 这是协议中的方法
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //键盘隐藏 也就是让键盘取消第一响应者身份
    [textField resignFirstResponder];
    return YES;
}
-(CustomTextField*)createField:(NSString*)placeholder andTag:(NSInteger)tag andFont:(double)font{
    
    CustomTextField * field =[[CustomTextField alloc]init];
    field.frame = CGRectMake(0, 0, Main_Width-90*kWidth, 43*kWidth);
    field.delegate = self;
    field.userInteractionEnabled = YES;
    field.textAlignment = NSTextAlignmentLeft;
    CGFloat width = field.frame.size.width;
    CGFloat height = field.frame.size.height;
    [field placeholderRectForBounds:CGRectMake(width/2, 0, width, height)];
    return field;
}
-(NewBtn*)createBtn:(NSString*)title {
    
    NewBtn * btn = [NewBtn buttonWithType:UIButtonTypeCustom];
    btn.layer.borderWidth = 0.5;
    btn.layer.borderColor = littleBlackColor.CGColor;
    btn.titleLabel.font = Font(FontOfSize13);
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:BlackColor forState:UIControlStateNormal];
    return btn;
}
-(UIButton*)createBtn:(NSString*)title andBorderColor:(UIColor*)BorderColor andFont:(CGFloat)Font andTitleColor:(UIColor*)TitleColor andBackColor:(UIColor*)BackColor {
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.layer.borderWidth = 0.5;
    btn.layer.cornerRadius = 5;
    btn.backgroundColor = BackColor;
    btn.layer.borderColor = BorderColor.CGColor;
    btn.titleLabel.font = Font(Font);
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:TitleColor forState:UIControlStateNormal];
    return btn;
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];
    CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentCenter;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}
-(UITextField*)createFieldMoney:(NSString*)placeholder andTag:(NSInteger)tag andFont:(double)font{
    
    UITextField * field =[[UITextField alloc]init];
    //    field.frame = CGRectMake(94*kWidth, 0, Main_Width-200*kWidth, 43*kWidth);
    field.frame = CGRectMake(120*kWidth, 0, Main_Width-200*kWidth, 50*kWidth);
    field.delegate = self;
    field.userInteractionEnabled = YES;
    field.textAlignment = NSTextAlignmentLeft;
    field.placeholder = placeholder;
    return field;
}

/* 解决触摸手势和cell点击的冲突 */
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    if ([touch.view isEqual:_warnView]) {
        return YES;
    }
    return NO;
}

-(UIView*)createWarnBack{
    
   UIView * view= [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, Main_height)];
    view.backgroundColor = RGBACOLOR(0, 0, 0, 0.3);
    view.userInteractionEnabled = YES;
    
    UITapGestureRecognizer * Tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(WarnBackTouch)];
    Tap.delegate = self;
    [view addGestureRecognizer:Tap];
    
    UIView * warnBackView = [[UIView alloc]init];
    warnBackView.backgroundColor = WhiteColor;
    [view addSubview:warnBackView];
    [warnBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(view.mas_top).with.offset(130*kHeight);
        make.centerX.mas_equalTo(view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(250*kWidth, 280*kHeight));
        
    }];
    //503 × 190 pixels
    UIImageView * warnBackImage = [[UIImageView alloc]init];
    warnBackImage.image = [UIImage imageNamed:@"图层-0"];
    [warnBackView addSubview:warnBackImage];
    [warnBackImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(warnBackView.mas_centerX);
        make.top.mas_equalTo(warnBackView.mas_top);
        make.size.mas_equalTo(CGSizeMake(250*kWidth, 95*kHeight));
    }];
    
    UILabel * label1 = [com createUIlabel:@"温馨提示" andFont:FontOfSize14 andColor:WhiteColor];
    [warnBackImage addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(warnBackImage.mas_centerX);
        make.bottom.mas_equalTo(warnBackImage.mas_bottom).with.offset(-3*kHeight);
    }];
    
    
    UIView * line = [[UIView alloc]init];
    line.backgroundColor = GrayColor;
    [warnBackView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(warnBackView.mas_left);
        make.right.equalTo(warnBackView.mas_right);
        make.height.mas_equalTo(0.5*kHeight);
        make.top.mas_equalTo(warnBackView.mas_bottom).with.offset(-43*kHeight);
    }];
    
    
    UILabel * label2 = [[UILabel alloc]init];
    
    
    label2.text = @"运输距离 <= 500 KM:提车1天,在途1天,第三天完成交车(下单时间晚于12点,提车时间从第二天起);运输距离 > 500 KM:交付时间从提车时间起,共需(运距/500)天,最高不超过7天.\n\n提车时间默认当前日期当前时间所属的时间段,可以当前时间选择之后的任意日期的任意时间段;如果选择的时间段为12点-24点,预计到达时间往后延一天.";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:label2.text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:1*kHeight];//调整行间距
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [label2.text length])];
    label2.attributedText = attributedString;
    [label2 sizeToFit];
    label2.numberOfLines = 0;

    label2.font = Font(FontOfSize11);
    label2.textColor = littleBlackColor;
    [warnBackView addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(warnBackView.mas_left).with.offset(8*kWidth);
        make.right.equalTo(warnBackView.mas_right).with.offset(-8*kHeight);
        make.top.mas_equalTo(warnBackImage.mas_bottom).with.offset(3*kHeight);
    }];
    
    
    
    UILabel * label = [com createUIlabel:@"知道了" andFont:FontOfSize14 andColor:YellowColor];
    [warnBackView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(warnBackView.mas_centerX);
        make.centerY.mas_equalTo(line.mas_bottom).with.offset(21.5*kHeight);
    }];
    
    
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 10, 10);
    [btn addTarget:self action:@selector(pressWarnBack) forControlEvents:UIControlEventTouchUpInside];
    [warnBackView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(warnBackView.mas_left);
        make.right.mas_equalTo(warnBackView.mas_right);
        make.height.mas_equalTo(43*kHeight);
        make.bottom.mas_equalTo(warnBackView.mas_bottom);
    }];
    return view;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if ([string isEqualToString:@"\n"])  //按会车可以改变
    {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    //1000000
    if ([toBeString length] > 7) {
        textField.text = [toBeString substringToIndex:7];
        [self createUIAlertController:@"整体估值输入不能超过100亿"];
        return NO;
    }
    return YES;
}
@end
