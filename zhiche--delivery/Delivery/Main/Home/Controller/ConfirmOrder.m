//
//  ConfirmOrder.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/6/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ConfirmOrder.h"
#import "Common.h"
#import "QuoteCell.h"
#import <Masonry.h>
#import "FindQuoteVC.h"
#import "QuoteCarListCell.h"
#import "PayVC.h"
#import "AgreementWebVC.h"
#import "PayViewController.h"
#define AddressTableTag 100
#define CarTableTag 200
#define DriverTableTag 300
#define MoneyTableTag 400
@interface ConfirmOrder ()
{
    UIView * nav;
    UITableView * AddressTable;
    UITableView * CarTable;
    UITableView * driverTable;
    UITableView * moneyTable;

    NSMutableDictionary * dataSouce;
    UIScrollView * scroll;
    NSMutableArray * vehicles;
    NSMutableArray * driverArr;
    NSMutableArray * driverArr1;
    NSMutableArray * MoneyArr;
    NSMutableArray * MoneyArr1;


}
@property (nonatomic,strong) UIView * backview;
@property (nonatomic,strong) UIView * backview0;
@property (nonatomic,strong) UIView * backview1;
@property (nonatomic,strong) UIView * backview2;
@property (nonatomic,strong) UILabel * AllPriceLabel;
@property (nonatomic,strong) UILabel * DepositLabel;
@property (nonatomic,strong) UIButton * ClickBtn;

@end

@implementation ConfirmOrder
- (instancetype)init

{
    self = [super init];
    if (self) {
        self.dataSouceArr = [[NSMutableArray alloc]init];
        self.quoteid = [[NSString alloc]init];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    nav = [self createNav:@"确定支付"];
    [self.view addSubview:nav];
    
    dataSouce = [[NSMutableDictionary alloc]init];
    vehicles = [[NSMutableArray alloc]init];
    driverArr = @[@"姓名",
                  @"车牌",
                  @"电话",
                  @"车型"].mutableCopy;
    MoneyArr = @[@"提车费",
                 @"交车费",
                 @"运输费",
                 @"保险费",
                 @"平台服务费"].mutableCopy;
    
    [self createSroll];
    [self createTable];
}

-(void)createSroll{
    
    scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, Main_Width,Main_height-64)];
    scroll.contentOffset = CGPointMake(0, 0);
    scroll.contentSize = CGSizeMake(Main_Width,994*kHeight);
    self.automaticallyAdjustsScrollViewInsets =NO;
    scroll.bounces = NO;
    scroll.userInteractionEnabled = YES;
    //隐藏横向、纵向的滚动条
    scroll.showsHorizontalScrollIndicator = NO;
    scroll.showsVerticalScrollIndicator = NO;
    scroll.delegate = self;
    scroll.minimumZoomScale = 1.0;
    scroll.maximumZoomScale = 3.0;
    [self.view addSubview:scroll];
    
    //设置scrollView的内容视图
    _backview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, 994*kHeight)];
    _backview.userInteractionEnabled = YES;
    _backview.backgroundColor = GrayColor;
    [scroll addSubview:_backview];

}

-(void)createTable{
    
    
    
    AddressTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Width,89*2*kHeight) style:UITableViewStylePlain];
    [AddressTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    AddressTable.tag = AddressTableTag;
    AddressTable.delegate = self;
    AddressTable.dataSource = self;
    AddressTable.showsHorizontalScrollIndicator = NO;
    AddressTable.showsVerticalScrollIndicator = NO;
    AddressTable.scrollEnabled = NO;
    [_backview addSubview:AddressTable];
    [AddressTable reloadData];

    [AddressTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview.mas_left);
        make.top.mas_equalTo(_backview.mas_top);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 135*kHeight));
    }];
    
    _backview0 = [[UIView alloc]init];
    _backview0.backgroundColor = WhiteColor;
    [_backview addSubview:_backview0];
    UILabel * carInfor = [self createUIlabel:@"车辆信息" andFont:FontOfSize13 andColor:BlackTitleColor];
    [_backview0 addSubview:carInfor];
    [carInfor mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview0.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_backview0.mas_top).with.offset(21.5*kHeight);
    }];
    UIView * viewline0 = [[UIView alloc]init];
    viewline0.backgroundColor = GrayColor;
    [_backview0 addSubview:viewline0];
    [viewline0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview0.mas_left);
        make.top.mas_equalTo(_backview0.mas_top).with.offset(42.5*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    
    
    CarTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Width,178*2*kHeight) style:UITableViewStylePlain];
//    [CarTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    CarTable.delegate = self;
    CarTable.tag = CarTableTag;
    CarTable.dataSource = self;
    CarTable.showsHorizontalScrollIndicator = NO;
    CarTable.showsVerticalScrollIndicator = NO;
    CarTable.scrollEnabled = NO;
    [_backview0 addSubview:CarTable];
    [CarTable reloadData];

    CarTable.backgroundColor = RedColor;
    vehicles = _dataSouceArr[0][@"vehicles"];
    
    
    driverArr1 = [[NSMutableArray alloc]init];
    NSMutableDictionary * dic = _dataSouceArr[1][@"quoteDetails"];
    
    [driverArr1 addObject:[self backString:dic[@"userName"]]];
    [driverArr1 addObject:[self backString:dic[@"truckNo"]]];
    [driverArr1 addObject:[self backString:dic[@"phone"]]];
    [driverArr1 addObject:[self backString:dic[@"truckTypeName"]]];

//
//    allFees = "1.02";
//    depositCost = "<null>";
//    destination = ggh;
//    formCurrentTime = "293\U5206\U949f\U524d";
//    id = 15;
//    phone = "150****8178";
//    position = "\U5317\U4eac\U5e02\U6d77\U6dc0\U533a";
//    quoteDetails =             (
//                                {
//                                    feeName = "\U8fd0\U8f93\U8d39";
//                                    id = 15;
//                                    money = 1;
//                                }
//                                );
//    quoteTime = "2016-07-20 10:51:14";
//    truckNo = "\U4eacBSBS**";
//    truckTypeName = "<null>";
//    userName = "\U5f20**";
    
    [_backview0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview.mas_left);
        make.top.mas_equalTo(AddressTable.mas_bottom).with.offset(10*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight*(vehicles.count+1)));
    }];

    [CarTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview0.mas_left);
        make.top.mas_equalTo(_backview0.mas_top).with.offset(43*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight*vehicles.count));
    }];
    
    _backview1 = [[UIView alloc]init];
    _backview1.backgroundColor = WhiteColor;
    [_backview addSubview:_backview1];
    [_backview1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview.mas_left);
        make.top.mas_equalTo(CarTable.mas_bottom).with.offset(10*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 215*kHeight));
    }];
    NSMutableArray * quotesArr = _dataSouceArr[0][@"quotes"];
    NSString * string = [NSString stringWithFormat:@"当前定位城市:%@",quotesArr[0][@"destination"]];
    UILabel * destination = [self createUIlabel:string andFont:FontOfSize13 andColor:AddCarColor];
    [_backview1 addSubview:destination];
    [destination mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview1.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_backview1.mas_top).with.offset(21.5*kHeight);
    }];
    UIView * viewline1 = [[UIView alloc]init];
    viewline1.backgroundColor = GrayColor;
    [_backview1 addSubview:viewline1];
    [viewline1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview1.mas_left);
        make.top.mas_equalTo(_backview1.mas_top).with.offset(43*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    

    driverTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Width,172*kHeight) style:UITableViewStylePlain];
//    [driverTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    driverTable.delegate = self;
    driverTable.dataSource = self;
    driverTable.tag = DriverTableTag;
    driverTable.showsHorizontalScrollIndicator = NO;
    driverTable.showsVerticalScrollIndicator = NO;
    driverTable.scrollEnabled = NO;
    [driverTable reloadData];
    [_backview1 addSubview:driverTable];
    [driverTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview1.mas_left);
        make.top.mas_equalTo(_backview1.mas_top).with.offset(43*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 172*kHeight));
    }];

    _backview2 = [[UIView alloc]init];
    _backview2.backgroundColor = WhiteColor;
    [_backview addSubview:_backview2];
    [_backview2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview.mas_left);
        make.top.mas_equalTo(_backview1.mas_bottom).with.offset(10*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 301*kHeight));
    }];
    
    UILabel * moneyLabel = [self createUIlabel:@"费用" andFont:FontOfSize13 andColor:AddCarColor];
    [_backview2 addSubview:moneyLabel];
    [moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview2.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_backview2.mas_top).with.offset(21.5*kHeight);
    }];
    UIView * viewline2 = [[UIView alloc]init];
    viewline2.backgroundColor = GrayColor;
    [_backview2 addSubview:viewline2];
    [viewline2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview2.mas_left);
        make.top.mas_equalTo(_backview2.mas_top).with.offset(43*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
    moneyTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Width,258*kHeight) style:UITableViewStylePlain];
    //    [driverTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    moneyTable.delegate = self;
    moneyTable.dataSource = self;
    moneyTable.tag = MoneyTableTag;
    moneyTable.showsHorizontalScrollIndicator = NO;
    moneyTable.showsVerticalScrollIndicator = NO;
    moneyTable.scrollEnabled = NO;
    [moneyTable reloadData];
    [_backview2 addSubview:moneyTable];
    [moneyTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview2.mas_left);
        make.top.mas_equalTo(_backview2.mas_top).with.offset(43*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 258*kHeight));
    }];
    



    
    
    
//    {
//        allFees = "1.03";
//        depositCost = "<null>";
//        destination = vg;
//        formCurrentTime = "200\U5206\U949f\U524d";
//        id = 5;
//        phone = "150****8178";
//        position = "\U5317\U4eac\U5e02\U6d77\U6dc0\U533a";
//        quoteDetails =                 (
//                                        {
//                                            feeName = "\U8fd0\U8f93\U8d39";
//                                            id = 5;
//                                            money = 1;
//                                        }
//                                        );
//        quoteTime = "2016-07-20 07:16:00";
//        truckNo = "\U4eacBSBS**";
//        truckTypeName = "<null>";
//        userName = "\U5f20**";
//    }

    


    
    

    

//    
//    UIView * viewline = [[UIView alloc]init];
//    viewline.backgroundColor = GrayColor;
//    [_backview addSubview:viewline];
//    [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(_backview.mas_left);
//        make.top.mas_equalTo(AddressTable.mas_bottom);
//        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
//    }];
//
//    UIView * backprice = [[UIView alloc]init];
//    backprice.backgroundColor = WhiteColor;
//    [_backview addSubview:backprice];
//    [backprice mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(_backview.mas_left);
//        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*3*kHeight));
//        make.top.mas_equalTo(AddressTable.mas_bottom).with.offset(0.5);
//    }];
//    
//
//    
//    UILabel * allpricelabel = [self createUIlabel:@"总金额" andFont:FontOfSize13 andColor:fontGrayColor];
//    [backprice addSubview:allpricelabel];
//    [allpricelabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(backprice.mas_left).with.offset(18*kWidth);
//        make.centerY.mas_equalTo(backprice.mas_top).with.offset(21.5*kHeight);
//    }];
//
//    NSString * price = [NSString stringWithFormat:@"¥%@",self.dataSouceArr[1][@"price"]];
//    self.AllPriceLabel = [self createUIlabel:price andFont:FontOfSize13 andColor:littleBlackColor];
//    [backprice addSubview:self.AllPriceLabel];
//    [self.AllPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(allpricelabel.mas_centerY);
//        make.left.mas_equalTo(allpricelabel.mas_right).with.offset(14*kHeight);
//    }];
//    
//    UIView * viewline1 = [[UIView alloc]init];
//    viewline1.backgroundColor = GrayColor;
//    [_backview addSubview:viewline1];
//    [viewline1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(_backview.mas_left);
//        make.top.mas_equalTo(viewline.mas_bottom).with.offset(43*kHeight);
//        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
//    }];
//
//    
//    UILabel * depositlabel = [self createUIlabel:@"定金" andFont:FontOfSize13 andColor:fontGrayColor];
//    [backprice addSubview:depositlabel];
//    [depositlabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(backprice.mas_left).with.offset(18*kWidth);
//        make.centerY.mas_equalTo(viewline1.mas_bottom).with.offset(21.5*kHeight);
//    }];
//
//    
//    NSString * deposit = [NSString stringWithFormat:@"¥%@",self.dataSouceArr[1][@"price"]];
//    self.DepositLabel = [self createUIlabel:deposit andFont:FontOfSize13 andColor:YellowColor];
//    [backprice addSubview:self.DepositLabel];
//    
//    
//    [self.DepositLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(depositlabel.mas_centerY);
//        make.left.mas_equalTo(depositlabel.mas_right).with.offset(14*kHeight);
//    }];
//    
//    UIView * viewline2 = [[UIView alloc]init];
//    viewline2.backgroundColor = GrayColor;
//    [backprice addSubview:viewline2];
//    [viewline2 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(backprice.mas_left);
//        make.top.mas_equalTo(viewline1.mas_bottom).with.offset(43*kHeight);
//        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
//    }];
//    
//    self.ClickBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [self.ClickBtn setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateSelected];
//    [self.ClickBtn setImage:[UIImage imageNamed:@"btn_unselect"] forState:UIControlStateNormal];
//    self.ClickBtn.selected = NO;
//    [self.ClickBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
//    [self.ClickBtn addTarget:self action:@selector(pressClickBtn:) forControlEvents:UIControlEventTouchUpInside];
//    [backprice addSubview:self.ClickBtn];
//    [self.ClickBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(backprice.mas_left).with.offset(18*kWidth);
//        make.centerY.mas_equalTo(viewline2.mas_bottom).with.offset(21.5*kHeight);
//        make.size.mas_equalTo(CGSizeMake(12*kWidth, 12*kHeight));
//    }];
//    
//    UILabel *label = [[UILabel alloc]init];
//    label.text = @"已阅读《运输说明》《退款规则》《服务协议》";
//    [label sizeToFit];
//    CGFloat length = label.frame.size.width;
//    UIButton * btnLabel = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btnLabel setTitle:@"已阅读《运输说明》《退款规则》《服务协议》" forState:UIControlStateNormal];
//    [btnLabel setTitleColor:YellowColor forState:UIControlStateNormal];
//    btnLabel.titleLabel.font = Font(FontOfSize12);
//    btnLabel.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//    [btnLabel addTarget:self action:@selector(pressBtnLabel) forControlEvents:UIControlEventTouchUpInside];
//    [backprice addSubview:btnLabel];
//    [btnLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.ClickBtn.mas_right).with.offset(15*kWidth);
//        make.centerY.mas_equalTo(self.ClickBtn.mas_centerY);
//        make.size.mas_equalTo(CGSizeMake(length+20*kWidth, 33*kHeight));
//    }];
//    
//    UIView * Hline1 =[[UIView alloc]init];
//    Hline1.backgroundColor = GrayColor;
//    [backprice addSubview:Hline1];
//    [Hline1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_backview.mas_left);
//        make.top.equalTo(btnLabel.mas_bottom).with.offset(10*kHeight);
//        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
//    }];
    
//    UIButton * ConfirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [ConfirmBtn addTarget:self action:@selector(pressConfirm) forControlEvents:UIControlEventTouchUpInside];
//    [ConfirmBtn setTitle:@"确定支付" forState:UIControlStateNormal];
//    [ConfirmBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
//    ConfirmBtn.backgroundColor = YellowColor;
//    ConfirmBtn.layer.borderWidth = 0.5;
//    ConfirmBtn.layer.cornerRadius = 5;
//    ConfirmBtn.layer.borderColor = YellowColor.CGColor;
//    [_backview addSubview:ConfirmBtn];
//    [ConfirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(_backview.mas_centerX);
//        make.top.equalTo(Hline1.mas_bottom).with.offset(10*kHeight);
//        make.size.mas_equalTo(CGSizeMake(Main_Width*0.75, 33*kHeight));
//    }];
}
-(void)pressConfirm{
    
//    PayVC * pay = [[PayVC alloc]init];
    
    if (self.ClickBtn.selected) {
        
        NSString * orderid = self.dataSouceArr[0][@"id"];
        NSString * quoteid = self.dataSouceArr[1][@"id"];
        Common * com = [[Common alloc]init];
        [dataSouce setObject:orderid forKey:@"orderid"];
        [dataSouce setObject:quoteid forKey:@"quoteid"];
        
        PayViewController * pay = [[PayViewController alloc]init];
        pay.orderid = orderid;
        [com afPostRequestWithUrlString:Order_Quote_Reserve_Url parms:dataSouce finishedBlock:^(id responseObj) {
            
            NSMutableDictionary * dicObj =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"%@",dicObj);
            if (dicObj[@"success"]) {
                [self.navigationController pushViewController:pay animated:YES];
            }
        } failedBlock:^(NSString *errorMsg) {
        }];
    }else{
        [self createUIAlertController:@"请阅读协议"];
    }
}
-(void)pressBtnLabel{
    NSLog(@"已阅读  《运输说明》 《退款规则《 服务协议》");
    
    AgreementWebVC * webVc = [[AgreementWebVC alloc]init];
    [self.navigationController pushViewController:webVc animated:YES];
}

-(void)pressClickBtn:(UIButton*)sender{
   
    UIButton * btn = (UIButton*)sender;
    if (btn.selected) {
        btn.selected = NO;
    }else{
        btn.selected = YES;
    }
}



-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *str=@"str";
    
    if (tableView.tag == AddressTableTag) {
        //运单cell
        QuoteCell * cell =  [tableView dequeueReusableCellWithIdentifier:str];
        if (cell ==nil) {
            cell = [[QuoteCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell showInfo:self.dataSouceArr[indexPath.row]];
        cell.view.frame = CGRectMake(0, 0, screenWidth, 178*kHeight);
        cell.Btn2.hidden = YES;
//        cell.Btn3.hidden = YES;
        cell.price.hidden = YES;
        return cell;
    }
    
    else if (tableView.tag == CarTableTag){
        QuoteCarListCell * cell = [tableView dequeueReusableCellWithIdentifier:str];
        if (cell == nil) {
            
            cell = [[QuoteCarListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }
        
        [cell showInfo:vehicles[indexPath.row]];

        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell;
    }
    else if (tableView.tag == DriverTableTag){
        QuoteCarListCell * cell = [tableView dequeueReusableCellWithIdentifier:str];
        if (cell == nil) {
            
            cell = [[QuoteCarListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }

        cell.carName.text = [NSString stringWithFormat:@"%@",driverArr[indexPath.row]];
        cell.carName.textColor = carScrollColor;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell;

    }
    
    else{
        QuoteCarListCell * cell = [tableView dequeueReusableCellWithIdentifier:str];
        if (cell == nil) {
            
            cell = [[QuoteCarListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }
        
        cell.carName.text = [NSString stringWithFormat:@"%@",MoneyArr[indexPath.row]];
        cell.carName.textColor = carScrollColor;

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return (tableView.tag == 100)?135*kHeight:43*kHeight;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    if (tableView.tag == AddressTableTag) {
        return 1;
    }
    else if (tableView.tag == DriverTableTag){
        return 4;
    }
    else if (tableView.tag == CarTableTag){
        return vehicles.count;
    }
    else{
        return 5;
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentRight;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}
-(void)createUIAlertController:(NSString*)title

{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}


-(NSString *)backString:(NSString *)string
{
    //    NSString * str = [NSString stringWithFormat:@"%@",string];
    if ([string isEqual:[NSNull null]]) {
        return @"";
    } else {
        return string;
    }
}

@end
