//
//  CDmoneyVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/11/15.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface CDmoneyVC : NavViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) NSMutableDictionary * CarMoneyDic;

@end
