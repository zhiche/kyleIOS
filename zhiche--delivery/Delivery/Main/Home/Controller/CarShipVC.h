//
//  CarShipVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface CarShipVC : NavViewController<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>


@property (nonatomic,strong) NSMutableArray * AddressArr;
@property (nonatomic,strong) NSMutableArray * CarArr;
@property (nonatomic,strong) NSMutableArray * CarAddress;
@property (nonatomic,strong) NSMutableDictionary * OrderCodeDic;
@property (nonatomic,strong) NSMutableDictionary * AllSource;

@end
