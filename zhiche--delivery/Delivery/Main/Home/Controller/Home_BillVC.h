//
//  Home_BillVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/10/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface Home_BillVC : NavViewController<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>


@property (nonatomic,strong) NSString *billType;
@property (nonatomic,strong) NSString * navName;
@end
