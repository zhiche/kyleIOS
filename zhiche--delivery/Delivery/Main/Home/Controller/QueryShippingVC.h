//
//  QueryShippingVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/8/22.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface QueryShippingVC : NavViewController<UIScrollViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>

@end
