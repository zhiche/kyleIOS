//
//  Bill_HM_VC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/12/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface Bill_HM_VC :  NavViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) NSString * orderID;
@property (nonatomic,strong) NSString * monthText;


@end
