//
//  CarShipVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "CarShipVC.h"
#import <Masonry.h>
#import "CarShipCell.h"
#import "Common.h"
#import "AddressCell.h"
#import "CarCell.h"
#import "PlaceOrderModel.h"
#import "FindQuoteVC.h"
#import <QuartzCore/QuartzCore.h>
#import <UIImageView+WebCache.h>

#define TimeTag 100
#define TableTag  200
#define SmallTabelTag 300
#define chooseTag 400
@interface CarShipVC ()
{
    UIImageView * nav;
    UIScrollView * scroll;
    UITableView * table;
    UITableView * CarTabel;
    NSMutableDictionary * dicUrl;
    PlaceOrderModel * PlaceModel;
    Common * Com;
}

@property (nonatomic,strong) UIView * BackView;
@property (nonatomic,strong) UIView * backview1;
@property (nonatomic,strong) UIView * backview3;
@property (nonatomic,strong) UIView * backview4;
@property (nonatomic,strong) NSMutableArray * headerViewArr;

@end

@implementation CarShipVC
- (instancetype)init

{
    self = [super init];
    if (self) {
        
        
        self.AddressArr = [[NSMutableArray alloc]init];
        self.CarArr = [[NSMutableArray alloc]init];
        self.CarAddress = [[NSMutableArray alloc]init];
        self.OrderCodeDic = [[NSMutableDictionary alloc]init];
        self.AllSource = [[NSMutableDictionary alloc]init];
        
    }
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    Com = [[Common alloc]init];
    nav = [self createNav:@"发布信息"];
    [self.view addSubview:nav];

    self.headerViewArr = [[NSMutableArray alloc]init];
    PlaceModel = [[PlaceOrderModel alloc]init];
    dicUrl = [[NSMutableDictionary alloc]init];
    
    [self createTableView];
    [self createScroll];
    [self createTable];
    [self createUI:self.AddressArr];
}
-(void)createUI:(NSMutableArray *)AddressArr{
    
    NSString * ordercode = [NSString stringWithFormat:@"运单号：%@",self.OrderCodeDic[@"ordercode"]];
    UILabel * label = [self createUIlabel:ordercode andFont:FontOfSize12 andColor:fontGrayColor];
    [_BackView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BackView.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_BackView.mas_top).with.offset(21.5*kHeight);
    }];

    UILabel * CarModel = [self createUIlabel:@"地址" andFont:FontOfSize14 andColor:BlackTitleColor];
    [_backview1 addSubview:CarModel];
    [CarModel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_backview1.mas_top).with.offset(21.5*kHeight);
    }];
    [table mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BackView.mas_left).with.offset(0);
        make.top.mas_equalTo(_backview1.mas_top).with.offset(43*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 200*kHeight));
    }];
    
    UIView * viewback2= [[UIView alloc]init];
    viewback2.backgroundColor = WhiteColor;
    [_BackView addSubview:viewback2];
    [viewback2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BackView.mas_left).with.offset(0);
        make.top.mas_equalTo(table.mas_bottom).with.offset(10*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 129*kHeight));
    }];

    
    UILabel * timelabel = [self createUIlabel:@"时间" andFont:FontOfSize14 andColor:BlackTitleColor];
    [viewback2 addSubview:timelabel];
    [timelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(viewback2.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(viewback2.mas_top).with.offset(21.5*kHeight);
    }];
    NSMutableArray * arr = [[NSMutableArray alloc]init];
    NSString * str1 = [NSString stringWithFormat:@"%@",self.AllSource[@"deliverydate"]] ;
    [arr addObject:str1];
    
    NSString * str2 = [NSString stringWithFormat:@"%@",self.AllSource[@"arrivedate"]] ;
    [arr addObject:str2];

    NSArray * arrTime = @[@"提车时间:",@"送达时间:"];
    for (int i=0; i<arrTime.count; i++) {
        
        UIImageView * image = [[UIImageView alloc]init];
        [viewback2 addSubview:image];
        [image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(viewback2.mas_left).with.offset(18*kWidth);
            make.centerY.equalTo(viewback2.mas_top).with.offset(64.5*kHeight+43*kHeight*i);
            make.size.mas_equalTo(CGSizeMake(17*kWidth, 13*kHeight));
        }];
        UILabel * time = [self createUIlabel:arrTime[i] andFont:FontOfSize13 andColor:fontGrayColor];
        [viewback2 addSubview:time];
        [time mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(image.mas_right).with.offset(10*kWidth);
            make.centerY.equalTo(viewback2.mas_top).with.offset(64.5*kHeight+43*kHeight*i);
        }];
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:arr[i] forState:UIControlStateNormal];

        [btn setTitleColor:littleBlackColor forState:UIControlStateNormal];
        btn.tag = TimeTag+i;
        btn.titleLabel.font = Font(FontOfSize13);
        [viewback2 addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(time.mas_right).with.offset(15*kWidth);
            make.centerY.equalTo(time.mas_centerY);
        }];
        UIImageView * imageline =[[UIImageView alloc]init];
        imageline.backgroundColor = GrayColor;
    
        if(i==1){
            imageline.tag = 50;
            image.image = [UIImage imageNamed:@"Address_Cell_songda"];
        }else{
            image.image = [UIImage imageNamed:@"Address_Cell_tiche"];
        }
        [viewback2 addSubview:imageline];
        [imageline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(viewback2.mas_left).with.offset(0);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
            make.top.mas_equalTo(viewback2.mas_top).with.offset(43*kHeight*(i+1));
        }];
    }
    

    NSInteger number = 0;
    for (int i =0; i<_CarArr.count; i++) {
        number = number+[_CarArr[i][@"vinArr"] count];
    }
    
    [_backview3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_BackView.mas_left).with.offset(0);
        make.top.mas_equalTo(viewback2.mas_bottom).with.offset(10*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*(number+_CarArr.count+2)*kHeight));
    }];
    
    UILabel * carModel = [self createUIlabel:@"车型" andFont:FontOfSize14 andColor:BlackTitleColor];
    [_backview3 addSubview:carModel];
    [carModel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BackView.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_backview3.mas_top).with.offset(21.5*kHeight);
    }];
    [CarTabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview3.mas_left).with.offset(0);
        make.top.mas_equalTo(_backview3.mas_top).with.offset(43*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*(number+_CarArr.count)*kHeight));
    }];
    
    NSString * Number = [NSString stringWithFormat:@"合计:%ld台",(long)number];
    UILabel * amount =[self createUIlabel:Number andFont:FontOfSize14 andColor: BlackColor];
    [_backview3 addSubview:amount];
    [amount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backview3.mas_right).with.offset(-15*kWidth);
        make.centerY.mas_equalTo(_backview3.mas_bottom).with.offset(-21.5*kHeight);
    }];
    
    
    UIImageView * imageline =[[UIImageView alloc]init];
    imageline.backgroundColor = GrayColor;
    [_backview3 addSubview:imageline];
    [imageline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview3.mas_left).with.offset(0);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        make.top.mas_equalTo(_backview3.mas_bottom).with.offset(-43*kHeight);
    }];
    
    
    [_backview4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_BackView.mas_left).with.offset(0);
        make.top.mas_equalTo(_backview3.mas_bottom).with.offset(10*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width,86*kHeight));
    }];

    UILabel * comment = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100*kWidth, 30*kHeight)];
    comment.text = @"备注";
    comment.font = Font(FontOfSize14);
    comment.textColor = BlackTitleColor;
    [_backview4 addSubview:comment];
    [comment mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BackView.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_backview4.mas_top).with.offset(21.5*kHeight);
    }];
    UIImageView * imageline1 =[[UIImageView alloc]init];
    imageline1.backgroundColor = GrayColor;
    [_backview4 addSubview:imageline1];
    [imageline1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview4.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        make.top.mas_equalTo(_backview4.mas_top).with.offset(43*kHeight);
    }];


    UITextField * field =[[UITextField alloc]initWithFrame:CGRectMake(18*kWidth, 0, Main_Width-18, 20*kHeight)];
    field.delegate = self;
    field.userInteractionEnabled = NO;
    field.textAlignment = NSTextAlignmentLeft;
    field.placeholder = self.AllSource[@"comment"];
    [field setFont:[UIFont fontWithName:@"STHeitiSC" size:FontOfSize9]];
    
    [_backview4 addSubview:field];
    [field mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview4.mas_left).with.offset(18*kWidth);
        make.size.mas_equalTo(CGSizeMake(Main_Width-18*kWidth, 43*kHeight));
        make.centerY.mas_equalTo(_backview4.mas_top).with.offset(64.5*kHeight);
    }];
    
    
    UIButton * ShipBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [ShipBtn setTitle:@"确定发布" forState:UIControlStateNormal];
    [ShipBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
    ShipBtn.backgroundColor = YellowColor;
    ShipBtn.layer.cornerRadius = 5;
    [ShipBtn addTarget:self action:@selector(pressShipBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:ShipBtn];
    [ShipBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(293*kWidth, 40*kHeight));
        make.top.mas_equalTo(_backview4.mas_bottom).with.offset(30*kHeight);
    }];
}
-(void)pressShipBtn{
    
//    NSLog(@"%@",self.OrderCodeDic[@"id"]);
    [dicUrl setObject:self.OrderCodeDic[@"id"] forKey:@"id"];
    [Com afPostRequestWithUrlString:Order_Publish_Url parms:dicUrl finishedBlock:^(id responseObj) {
        
        NSMutableDictionary * dicObj =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];
        
        if (dicObj[@"success"]) {
            FindQuoteVC * quote = [[FindQuoteVC alloc]init];
            [self.navigationController pushViewController:quote animated:YES];
        }else{
            NSLog(@"message==%@",dicObj[@"message"]);
        }
    } failedBlock:^(NSString *errorMsg) {
    }];
}
-(void)createScroll{
    
    scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, Main_Width,Main_height-64*kHeight)];
    scroll.backgroundColor = GrayColor;
    scroll.contentOffset = CGPointMake(0, 0);
    scroll.contentSize = CGSizeMake(Main_Width, Main_height*1.5);
    self.automaticallyAdjustsScrollViewInsets =NO;
    scroll.bounces = NO;
    //隐藏横向、纵向的滚动条
    scroll.showsHorizontalScrollIndicator = NO;
    scroll.showsVerticalScrollIndicator = NO;
    scroll.delegate = self;
    scroll.minimumZoomScale = 1.0;
    scroll.maximumZoomScale = 3.0;
    [self.view addSubview:scroll];
    
    [scroll mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(nav.mas_bottom).with.offset(0*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, Main_height-64*kHeight));
    }];
    //设置scrollView的内容视图
    _BackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, Main_height*kHeight*1.5)];
    _BackView.backgroundColor = GrayColor;
    [scroll addSubview:_BackView];
    
    _backview1 = [[UIView alloc]init];
    _backview1.backgroundColor = WhiteColor;
    [_BackView addSubview:_backview1];
    [_backview1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BackView.mas_left).with.offset(0);
        make.top.mas_equalTo(_BackView.mas_top).with.offset(43*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 243*kHeight));
    }];
    _backview3 = [[UIView alloc]init];
    _backview3.backgroundColor = WhiteColor;
    [_BackView addSubview:_backview3];
    _backview4 = [[UIView alloc]init];
    _backview4.backgroundColor = WhiteColor;
    [_BackView addSubview:_backview4];
    
}

-(void)createTable{
    
    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Main_Width, Main_height) style:UITableViewStylePlain];
    [table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table.userInteractionEnabled = NO;
    table.tag = TableTag;
    table.delegate = self;
    table.dataSource = self;
    [table reloadData];
    [_BackView addSubview:table];
    
    CarTabel = [[UITableView alloc]initWithFrame:CGRectMake(0, 200, Main_Width, Main_height) style:UITableViewStylePlain];
    [CarTabel setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    CarTabel.userInteractionEnabled = NO;
    CarTabel.tag = SmallTabelTag;
    CarTabel.delegate = self;
    CarTabel.dataSource = self;
    [CarTabel reloadData];
    [_backview3 addSubview:CarTabel];
    
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *str=@"str";
    
    if (tableView.tag== TableTag) {
        AddressCell * cell =  [tableView dequeueReusableCellWithIdentifier:str];
        if (cell ==nil) {
            cell = [[AddressCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }

        cell.RightImage.hidden = YES;
        
        PlaceModel = [PlaceOrderModel ModelWithDic:self.AddressArr[indexPath.row]];
        NSString * cityName = [NSString stringWithFormat:@"%@%@%@",PlaceModel.provinceName,PlaceModel.cityName,PlaceModel.countyName];
        cell.CityName.text = cityName;
        cell.StoreName.text = PlaceModel.unitName;
        cell.AddressName.text = PlaceModel.address;
        NSString * people = [NSString stringWithFormat:@"%@ %@",PlaceModel.contact,PlaceModel.phone];
        cell.PeopleName.text = people;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row ==0) {
            cell.SmallImage.image = [UIImage imageNamed:@"Address_Cell_fache"];
        }
        else{
            cell.SmallImage.image = [UIImage imageNamed:@"Address_Cell_shouche"];
        }
        return cell;
    }
    else{
        CarCell * cell =  [tableView dequeueReusableCellWithIdentifier:str];
        if (cell ==nil) {
            cell = [[CarCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }
//        NSMutableArray * vin = _CarArr[indexPath.section][@"vinArr"];
//        cell.field.textColor = fontGrayColor;
//        cell.field.text = vin[indexPath.row];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return (tableView.tag == TableTag?100*kHeight:45*kHeight);
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView.tag == TableTag) {
        
        return 2;
    }
    else{
        NSInteger number = [self.CarArr[section][@"vinArr"] count];
        return (self.CarArr.count>0)?number:0;
    }
}
//设置分区个数
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag == TableTag) {
        return 1;
    }else{
        return (_CarArr.count>0)?[_CarArr count]:1;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return (tableView.tag == TableTag)?0:43*kHeight;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView.tag == TableTag) {
        UIView * view = [[UIView alloc]init];
        view.frame = CGRectMake(0, 0, Main_Width, 0.5);
        return view;
    }
    
    else{
        if (_headerViewArr.count>0) {
            return _headerViewArr[section];
        }
    }
    return nil;
}

-(void)createTableView{
    
    [_headerViewArr removeAllObjects];
    for (int i =0; i<_CarArr.count; i++) {
        
        UIView * SectionView = [[UIView alloc]init];
        SectionView.backgroundColor = [UIColor whiteColor];
        SectionView.tag = 900+i;
        SectionView.frame = CGRectMake(0, 0, Main_Width, 43*kHeight);
        UIImageView * SmallImage = [[UIImageView alloc]init];
        
        NSURL * imageurl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",_CarArr[i][@"brandLogo"]]];
        SmallImage.contentMode = UIViewContentModeScaleAspectFit;

        [SmallImage sd_setImageWithURL:imageurl placeholderImage:[UIImage imageNamed:@"car_bad"] options:SDWebImageCacheMemoryOnly];
        NSString * name = [NSString stringWithFormat:@"%@",_CarArr[i][@"brandname"]];
        UILabel * CarName = [self createUIlabel:name andFont:FontOfSize13 andColor:BlackTitleColor];
        NSString * number = [NSString stringWithFormat:@"%@辆",_CarArr[i][@"vehiclecount"]];
        UILabel * Number = [self createUIlabel:number andFont:FontOfSize13 andColor:BlackTitleColor];
        
        Number.tag = 1000+i;
        
        [SectionView addSubview:SmallImage];
        [SectionView addSubview: CarName];
        [SectionView addSubview: Number];
        
        [SmallImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(SectionView.mas_left).with.offset(18*kWidth);
            make.centerY.mas_equalTo(SectionView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(22*kWidth, 22*kHeight));
        }];
        
        [CarName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(SectionView.mas_left).with.offset(54*kWidth);
            make.centerY.mas_equalTo(SmallImage.mas_centerY);
        }];
        
        [Number mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(SectionView.mas_right).with.offset(-15*kWidth);
            make.centerY.mas_equalTo(SmallImage.mas_centerY);
        }];
        UIImageView * imageline =[[UIImageView alloc]init];
        imageline.backgroundColor = GrayColor;;
        [SectionView addSubview:imageline];
        [imageline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(SectionView).with.offset(0);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
            make.top.mas_equalTo(SectionView.mas_top);
        }];
        [_headerViewArr addObject:SectionView];
    }
    
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentLeft;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
