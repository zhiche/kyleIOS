//
//  AboutVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/6/27.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "AboutVC.h"
#import "RootViewController.h"

@interface AboutVC ()
{
    UIImageView * nav;
    RootViewController * TabBar;

}
@end

@implementation AboutVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;

    
    nav = [self createNav:@"关于慧运车"];
    [self.view addSubview:nav];
    UIWebView * webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 64, Main_Width, Main_height-64)];
    webView.backgroundColor = WhiteColor;
    NSString * path = aboutUS_web_Url;
    NSURL * url = [NSURL URLWithString:path];
    NSURLRequest * request = [NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
    [self.view addSubview:webView];
    TabBar = [RootViewController defaultsTabBar];

}

-(void)viewWillAppear:(BOOL)animated{
    
    [TabBar setTabBarHidden:YES];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
