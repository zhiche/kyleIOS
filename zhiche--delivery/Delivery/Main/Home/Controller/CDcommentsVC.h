//
//  CDcommentsVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/11/15.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface CDcommentsVC : NavViewController<UITextViewDelegate>


@property (nonatomic,copy) void (^callCommentBack)(NSString * comment);
@property (nonatomic,strong) NSString * IsHave;

@end
