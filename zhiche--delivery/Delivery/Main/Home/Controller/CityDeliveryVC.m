//
//  CityDeliveryVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/11/15.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "CityDeliveryVC.h"
#import <Masonry.h>
#import "AddressCell.h"
#import "CarCell.h"
#import "ActionSheetPicker.h"
#import "ChooseAddressVC.h"
#import "NewBtn.h"
#import "CarShipVC.h"
#import "ChoiceCarTypeVC.h"
#import "ChooseAddressVC.h"
#import "Common.h"
#import "PlaceOrderModel.h"
#import "HistoryCarStyleVC.h"
#import <MJRefresh.h>
#import "RootViewController.h"
#import <UIImageView+WebCache.h>
#import "FindQuoteVC.h"
#import "CustomTextField.h"
#import "AddCarTypeVC.h"
#import "SevenSwitch.h"
#import "placeAddressCell.h"
#import "KeepAddressVC.h"
#import "ConfirmCarVC.h"
#import "WKProgressHUD.h"
#import "FeeDetailVC.h"
#import "CDcommentsVC.h"
#import "CDmoneyVC.h"
#import "HomeViewController.h"
#import "CDAddressVC.h"
#import "CustomTextField.h"
#define AddressTag  10000
#define CarTag 20000
#define InforTableTag 30000
#define TimeTag 100
#define CommenTag 120
#define TatalLabelTag 333
#define PickCarTimeTag 555
#define PickCarImageTag 666
#define PickCarBtnTag 777
@interface CityDeliveryVC ()<UIGestureRecognizerDelegate>
{
    UIImageView * nav;
    NSMutableArray * NumberArr;
    HistoryCarStyleVC *choiceVC;
    Common * com;
    PlaceOrderModel * PlaceModel;
    NSMutableDictionary * dicUrl;
    RootViewController * TabBar;
    BOOL addressYes;
    BOOL vinYes;
    BOOL carYes;
    BOOL pickCarHeight;
    int CarNumber;
    
    int SwitchNumber;
    CGFloat addressCellHight1;
    CGFloat addressCellHight2;
    int keepAddressNumber;
    BOOL pushIsOk;
    NSMutableArray * pickTimeData;
    NSMutableArray * pickProductArr;
    CGFloat addressHigth ;
    NSMutableArray * carTypeArr;
    CustomTextField * payShopperField;
    BOOL chooseAddressYesOrNo;
    UIImageView * chooseAdressImge;
    UIButton * chooseAddressBtn;
    
}

@property (nonatomic,strong) NSMutableArray * carInforBtnArr;
@property (nonatomic,strong) NSMutableArray * carInforNameArr;
@property (nonatomic,strong) NSMutableArray * carInforImageArr;

@property (nonatomic,strong) NSString * carType;//板车类型
@property (nonatomic,strong) NSMutableArray * cityListArr;//城市列表
@property (nonatomic,strong) NSMutableDictionary * CarMoneyDic;//运费
@property (nonatomic,strong) UILabel * carryMoneyLabel;//订单运费
@property (nonatomic,strong) UILabel * arriveDistance;//里程数
@property (nonatomic,strong) UILabel * arriveData;//到达日期
@property (nonatomic,strong) UILabel * arriveTime;//到达时间
@property (nonatomic,strong) UIButton * PickBtn;
@property (nonatomic,strong) UIButton * OrderBtn;


@property (nonatomic,strong) UILabel * addressCH;
@property (nonatomic,strong) NSMutableArray *CarName;
@property (nonatomic,strong) NSMutableArray *RemoveArr;
@property (nonatomic,strong) UITableView * CarTable;
@property (nonatomic,strong) UITableView * AddressTable;
@property (nonatomic,strong) UITableView * InforTable;
@property (nonatomic,strong) NSMutableArray * dataSouce;

@property (nonatomic,strong) NSMutableArray * timeChoose1;
@property (nonatomic,strong) NSMutableArray * timeChoose2;

@property (nonatomic,strong) NSMutableArray * carInforArr;
@property (nonatomic,strong) NSMutableArray * headerViewArr;
@property (nonatomic,strong) NSMutableArray * headerCarNumberArr;
@property (nonatomic,strong) NSMutableArray * InforHeaderCarArr;
@property (nonatomic,strong) NSMutableArray * InforCellArr;
@property (nonatomic,strong) UIScrollView * scroll;
@property (nonatomic,strong) UIView * backview;
@property (nonatomic,strong) UIView * backview0;
@property (nonatomic,strong) UIView * backview1;
@property (nonatomic,strong) UIView * backview2;
@property (nonatomic,strong) UIView * backview20;

@property (nonatomic,strong) UIButton * btnTime;
@property (nonatomic,strong) UIButton * btnTime1;
@property (nonatomic,strong) UILabel * standardArrive;
@property (nonatomic,strong) UILabel * standardTime;
@property (nonatomic,strong) UILabel * speedyArrive;
@property (nonatomic,strong) UILabel * speedyTime;
@property (nonatomic,strong) UILabel * warnlabel;
@property (nonatomic,strong) UIView * warnView;

@property (nonatomic,strong) NSString * adate ;//提车日期
@property (nonatomic,strong) NSString * atime;//提车时间
@property (nonatomic,strong) UIView * backview3;
@property (nonatomic,strong) UIView * backview30;//付款方式

@property (nonatomic,strong) UIView * backview4;//订单备注
@property (nonatomic,strong) UIView * backview5;
@property (nonatomic,strong) UIView * CarInformationView;
@property (nonatomic,strong) UITextField * CarPromptField;
@property (nonatomic,strong) UILabel * prompt1;
@property (nonatomic,strong) UILabel * prompt2;

@property (nonatomic,strong) UILabel * labelComment;//备注
@end

@implementation CityDeliveryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GrayColor;
    nav = [self createNav:@"同城配送"];
    [self.view addSubview:nav];
    
    choiceVC = [[HistoryCarStyleVC alloc]init];
    TabBar = [RootViewController defaultsTabBar];
    chooseAddressYesOrNo = NO;
    [self evaluate];
    [self initSomeing];
    [self createUI];

    
    [self createBackView0];
    [self createData];
//    [self createNavAddress];

    [self.navigationController.interactivePopGestureRecognizer addTarget:self action:@selector(handleGesture)];
    //接受地址信息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeBackAddress:) name:@"CityBackAddressDic" object:nil];
    //返回删除信息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeVC:) name:@"BackVC" object:nil];
    [Common requestWithUrlString:trailer_list contentType:@"application/json" finished:^(id responseObj){
        [carTypeArr removeAllObjects];
        
        carTypeArr =  [NSMutableArray arrayWithArray:responseObj[@"data"]];
        if (_carType.length == 0) {
            _carType = [NSString stringWithFormat:@"%@",carTypeArr[0][@"id"]];
        }
        
        [self CityListDate];
        
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
}

-(void)createNavAddress{
    
    
    UIImageView * imageRight = [[UIImageView alloc]init];
    imageRight.image = [UIImage imageNamed:@"箭头"];
    [nav addSubview:imageRight];
    [imageRight mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(nav.mas_right).with.offset(-10*kWidth);
        make.size.mas_equalTo(CGSizeMake(10*kWidth, 6*kHeight));
        make.centerY.mas_equalTo(nav.mas_centerY);
    }];

    
    
    _addressCH = [com createUIlabel:@"北京" andFont:FontOfSize11 andColor:WhiteColor];
    [nav addSubview:_addressCH];
    [_addressCH mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(imageRight.mas_left).with.offset(-5*kWidth);
        make.centerY.mas_equalTo(nav.mas_centerY);
    }];
    
    UIImageView * imageLeft = [[UIImageView alloc]init];
    imageLeft.image = [UIImage imageNamed:@"地址"];
    [nav addSubview:imageLeft];
    [imageLeft mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_addressCH.mas_left).with.offset(-5*kWidth);
        make.size.mas_equalTo(CGSizeMake(10*kWidth, 14*kHeight));
        make.centerY.mas_equalTo(nav.mas_centerY);
    }];


    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn addTarget:self action:@selector(pressAddressBtn) forControlEvents:UIControlEventTouchUpInside];
//    btn.backgroundColor = [UIColor cyanColor];
    [nav addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(nav.mas_right);
        make.size.mas_equalTo(CGSizeMake(100*kWidth, 30*kHeight));
        make.centerY.mas_equalTo(nav.mas_centerY);
    }];


}

-(void)pressAddressBtn{
    
    
    [self createUIAlertController:@"服务暂时只开通北京地区"];
    
}
#pragma mark 初始化

-(void)initSomeing{
    
    com = [[Common alloc]init];
    PlaceModel = [[PlaceOrderModel alloc]init];
    _dataSouce = [[NSMutableArray alloc]init];
    
    _headerViewArr = [[NSMutableArray alloc]init];
    _headerCarNumberArr = [[NSMutableArray alloc]init];
    _InforHeaderCarArr = [[NSMutableArray alloc]init];
    _InforCellArr = [[NSMutableArray alloc]init];
    _InforCellArr = @[@"",@""].mutableCopy;
    _carInforArr = [[NSMutableArray alloc]init];
    NumberArr = [[NSMutableArray alloc]init];
    _CarName = [[NSMutableArray alloc]init];
    _RemoveArr = [[NSMutableArray alloc]init];
    dicUrl = [[NSMutableDictionary alloc]init];
    pickTimeData = [[NSMutableArray alloc]init];
    pickProductArr = [[NSMutableArray alloc]init];
    _timeChoose1 = [[NSMutableArray alloc]init];
    _timeChoose2 = [[NSMutableArray alloc]init];
    _carInforBtnArr = [[NSMutableArray alloc]init];
    _carInforNameArr = [[NSMutableArray alloc]init];
    _carInforImageArr = [[NSMutableArray alloc]init];
    carTypeArr = [[NSMutableArray alloc]init];
    _cityListArr = [[NSMutableArray alloc]init];
    _CarMoneyDic = [[NSMutableDictionary alloc]init];
    
    _adate = [[NSString alloc]init];
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy-MM-dd"];
    _adate = [dateformatter stringFromDate:senddate];
    
    _atime = [[NSString alloc]init];
    NSDate *date = [NSDate date]; // 获得时间对象
    NSDateFormatter  *dateformatterTime=[[NSDateFormatter alloc] init];
    [dateformatterTime setDateFormat:@"HH:mm:ss"];
    NSString * string = [dateformatterTime stringFromDate:date];
    int  b = [[string substringWithRange:NSMakeRange(0,2)] intValue];
    
    if (b>=0 &&b<=12) {
        _atime = @"0";
    }else{
        _atime = @"1";
    }
    
    
    [dicUrl setObject:@"" forKey:@"depaId"];//发运地址id
    [dicUrl setObject:@"" forKey:@"destId"];//目的地址id
    [dicUrl setObject:@"" forKey:@"deliveryDate"];//提车日期
    [dicUrl setObject:@"" forKey:@"deliveryTime"];//提车时间
    [dicUrl setObject:@"" forKey:@"arriveDate"];//到达日期
    [dicUrl setObject:@"" forKey:@"arriveTime"];//到达时间
    [dicUrl setObject:@"" forKey:@"vehicleNum"];//车辆数
    [dicUrl setObject:@"" forKey:@"vehicles"];//车辆信息
    [dicUrl setObject:@"" forKey:@"comment"];//备注
    [dicUrl setObject:@"1" forKey:@"isVeneer"];//是否同城
    [dicUrl setObject:@"1" forKey:@"trailerId"];
    [dicUrl setObject:@"0" forKey:@"isAppoint"];//是否预约（0:否，1:是）
    
    [dicUrl setObject:@"20" forKey:@"transportType"];

    
    [dicUrl setObject:@(10) forKey:@"payRole"];//10:下单人付款 20:其他商户付款
    [dicUrl setObject:@"" forKey:@"phone"];//如果是20必填

}

#pragma mark 初始化赋值

-(void)evaluate{
    
    addressYes = YES;
    vinYes = YES;
    carYes = YES;
    pushIsOk = YES;
    pickCarHeight = NO;
    CarNumber = 0;
    SwitchNumber = 0;
    addressCellHight1 = 43*kHeight;
    addressCellHight2 = 43*kHeight;
}

-(void)changeVC:(NSNotification *)notification{
    
    [dicUrl setObject:_carInforArr forKey:@"vehicles"];

}


-(void)changeBackAddress:(NSNotification *)notification{
    
    
    
    NSMutableDictionary * addressDic = [notification object];
    NSString * addresstype = [NSString stringWithFormat:@"%@",addressDic[@"addressType"]];
    NSString * address = [NSString stringWithFormat:@"%@",addressDic[@"address"]];
    addressCellHight1 = 43*kHeight;
    if ([addresstype isEqualToString:@"1"]) {
        for (int i = 0; i<_dataSouce.count; i++) {
            if ([_dataSouce[i][@"addressType"] isEqualToString:@"1"]&&address.length>0) {
                [_dataSouce replaceObjectAtIndex:i withObject:addressDic] ;
                addressCellHight2 = 100*kHeight;
            }
            NSString * address1 = [NSString stringWithFormat:@"%@",_dataSouce[i][@"address"]];
            if ([_dataSouce[i][@"addressType"] isEqualToString:@"0"]&&address1.length>0) {
                addressCellHight1 = 100*kHeight;
            }
        }
    }
    else{
        for (int i = 0; i<_dataSouce.count; i++) {
            if ([_dataSouce[i][@"addressType"] isEqualToString:@"0"]) {
                [_dataSouce replaceObjectAtIndex:i withObject:addressDic] ;
                addressCellHight1 = 100*kHeight;
            }else{
                NSString * address1 = [NSString stringWithFormat:@"%@",_dataSouce[i][@"address"]];
                if ([_dataSouce[i][@"addressType"] isEqualToString:@"1"]&&address1.length>0) {
                    addressCellHight2 = 100*kHeight;
                }else{
                    addressCellHight2 = 43*kHeight;
                }
            }
        }
    }
    [self judgementAddress];
    
    CGFloat totalHeight = addressCellHight1 +addressCellHight2;
    [self updateAddressTable:totalHeight];
    [_AddressTable reloadData];
}

-(void)handleGesture{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)createStarData{
    
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy-MM-dd"];
    NSString *  locationString=[dateformatter stringFromDate:senddate];
    
    
    
    NSDateFormatter  *dateformatterTime=[[NSDateFormatter alloc] init];
    [dateformatterTime setDateFormat:@"HH:mm:ss"];
    NSString * string = [dateformatterTime stringFromDate:senddate];
    
    
    
    chooseAdressImge = [[UIImageView alloc]init];
    chooseAdressImge.image = [UIImage imageNamed:@"zhuanhuan"];
    [_backview1 addSubview:chooseAdressImge];
    [chooseAdressImge mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left).with.offset(20*kWidth);
        make.centerY.mas_equalTo(_backview1.mas_centerY).with.offset(21.5*kHeight);
        make.size.mas_equalTo(CGSizeMake(9*kWidth, 11*kHeight));
    }];
    
    
    chooseAddressBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [chooseAddressBtn addTarget:self action:@selector(pressChooseBtn) forControlEvents:UIControlEventTouchUpInside];
    
    [_backview1 addSubview:chooseAddressBtn];
    [chooseAddressBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left).with.offset(15*kWidth);
        make.centerY.mas_equalTo(_backview1.mas_centerY).with.offset(21.5*kHeight);
        make.size.mas_equalTo(CGSizeMake(50*kWidth, 50*kHeight));
    }];

    
    
    
    for (int i = 0; i<24; i++) {
        
        NSString * time = nil;
        if (i<10) {
            time = [NSString stringWithFormat:@"0%d",i];
        }else{
            time = [NSString stringWithFormat:@"%d",i];
        }
        [_timeChoose1 addObject:time];
    }
    for (int i= 0; i<60; i++) {
        
        NSString * time = nil;
        if (i<10) {
            time = [NSString stringWithFormat:@"0%d",i];
        }else{
            time = [NSString stringWithFormat:@"%d",i];
        }
        [_timeChoose2 addObject:time];
    }
//    _timeChoose = @[@"0点-12点",@"12点-24点"];
    if (_dataSouce.count==0) {
        
        NSMutableDictionary *departaddrid = [[NSMutableDictionary alloc]init];
        [departaddrid setValue:@"0" forKey:@"addressType"];
        [departaddrid setValue:@"" forKey:@"address"];
        [_dataSouce addObject:departaddrid];
        
        NSMutableDictionary *receiptaddrid = [[NSMutableDictionary alloc]init];
        [receiptaddrid setValue:@"1" forKey:@"addressType"];
        [receiptaddrid setValue:@"" forKey:@"address"];
        [_dataSouce addObject:receiptaddrid];
        
        chooseAdressImge.hidden = YES;
        chooseAddressBtn.hidden = YES;

    }else if (_dataSouce.count==1){
        // 0 是发货
        if ([_dataSouce[0][@"addressType"] isEqualToString:@"1"]) {
//            depaId	出发地id  destId目的地
            // 0 是发货 1是收车

            [dicUrl setObject:_dataSouce[0][@"id"] forKey:@"destId"];//目的地地址id 8-9
            NSMutableDictionary *departaddrid = [[NSMutableDictionary alloc]init];
            [departaddrid setValue:@"0" forKey:@"addressType"];
            [departaddrid setValue:@"" forKey:@"address"];
            addressCellHight2 = 100*kHeight;
            [_dataSouce insertObject:departaddrid atIndex:0];
        }else{
            [dicUrl setObject:_dataSouce[0][@"id"] forKey:@"depaId"];//发运地址id 2-5
            NSMutableDictionary *receiptaddrid = [[NSMutableDictionary alloc]init];
            [receiptaddrid setValue:@"1" forKey:@"addressType"];
            [receiptaddrid setValue:@"" forKey:@"address"];
            addressCellHight1 = 100*kHeight;
            [_dataSouce addObject:receiptaddrid];
        }
        chooseAdressImge.hidden = NO;
        chooseAddressBtn.hidden = NO;

    }
    else{
        addressCellHight1 = 100*kHeight;
        addressCellHight2 = 100*kHeight;
        
        NSString * addressType = [NSString stringWithFormat:@"%@",_dataSouce[0][@"addressType"]];
        if ([addressType isEqualToString:@"1"]) {
            
            [_dataSouce exchangeObjectAtIndex:0 withObjectAtIndex:1];
            
        }

        
        
        for (int i=0; i<_dataSouce.count; i++) {
            if ([_dataSouce[i][@"addressType"] isEqualToString:@"1"]) {
                [dicUrl setObject:_dataSouce[i][@"id"] forKey:@"destId"];//目的地地址id 8-9
            }else{
                [dicUrl setObject:_dataSouce[i][@"id"] forKey:@"depaId"];//发运地址id 2-5
            }
        }
        chooseAdressImge.hidden = NO;
        chooseAddressBtn.hidden = NO;

    }
    NSLog(@"dicUrl===%@",dicUrl);
    addressHigth = 0;
    if (_dataSouce.count>0) {
        NSString * address1 =[NSString stringWithFormat:@"%@",_dataSouce[0][@"address"]];
        NSString * address2 =[NSString stringWithFormat:@"%@",_dataSouce[1][@"address"]];
        if (address1.length==0) {
            addressHigth = addressHigth+43*kHeight;
        }else{
            addressHigth = addressHigth+100*kHeight;
        }
        if (address2.length==0) {
            addressHigth = addressHigth+43*kHeight;
        }else{
            addressHigth = addressHigth+100*kHeight;
        }
    }else{
        addressHigth = 86*kHeight;
    }
    
    
    [_AddressTable mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(addressHigth);
    }];
    [_backview1 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(addressHigth+43*kHeight);
    }];
    
//    [self judgementAddress];
    
    [self updateFristScrollFram:addressHigth];
    [_AddressTable reloadData];
    [_AddressTable mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(addressHigth);
    }];
    [_backview1 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(addressHigth+43*kHeight);
    }];
    
    [dicUrl setObject:locationString forKey:@"deliveryDate"];//提车日期
    
}



-(void)pressChooseBtn{
    
    if (!chooseAddressYesOrNo) {
        chooseAddressYesOrNo = YES;
    }else{
        chooseAddressYesOrNo = NO;
    }
    
    NSString * add1 = [NSString stringWithFormat:@"%@",_dataSouce[0][@"addressType"]];
    NSString * add2 = [NSString stringWithFormat:@"%@",_dataSouce[1][@"addressType"]];
    
    NSString * id1 = [NSString stringWithFormat:@"%@",_dataSouce[0][@"id"]];
    NSString * id2 = [NSString stringWithFormat:@"%@",_dataSouce[1][@"id"]];
    
    //    [self.dataSouce[0] setObject:add2 forKey:@"addressType"];
    //    [self.dataSouce[1] setObject:add1 forKey:@"addressType"];
    
    
    
    NSMutableDictionary * dic0 = [[NSMutableDictionary alloc]initWithDictionary:_dataSouce[0]];
    NSMutableDictionary * dic1 = [[NSMutableDictionary alloc]initWithDictionary:_dataSouce[1]];
    [dic0 setObject:add2 forKey:@"addressType"];
    [dic0 setObject:id2 forKey:@"id"];
    
    [dic1 setObject:add1 forKey:@"addressType"];
    [dic1 setObject:id1 forKey:@"id"];
    
    [_dataSouce replaceObjectAtIndex:1 withObject:dic0];
    [_dataSouce replaceObjectAtIndex:0 withObject:dic1];
    
    
    addressHigth = 0;
    CGFloat addresscell0  = 0;
    CGFloat addresscell1  = 0;
    
    NSString * address1 =[NSString stringWithFormat:@"%@",_dataSouce[0][@"address"]];
    NSString * address2 =[NSString stringWithFormat:@"%@",_dataSouce[1][@"address"]];
    
    
    
    if (address2.length == 0 || address1.length == 0) {
        
        if (address1.length == 0) {
            
            addressHigth = addressHigth +43*kHeight;
            addresscell0 = 43*kHeight;
            
        }else{
            addressHigth = addressHigth +100*kHeight;
            addresscell0 = 100*kHeight;
        }
        
        if (address2.length == 0) {
            addressHigth = addressHigth + 43*kHeight;
            addresscell1 = 43*kHeight;
            
        }else{
            addressHigth = addressHigth + 100*kHeight;
            addresscell1 = 100*kHeight;
        }
        
    }
    
    
    
    if (address1.length !=0 && address2.length != 0) {
        
        addressHigth = 200*kHeight;
        addresscell1 = 100*kHeight;
        addresscell0 = 100*kHeight;
        
        
    }
    
    
    addressCellHight1 = addresscell0;
    addressCellHight2 = addresscell1;
    
    [_AddressTable mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(addressHigth);
    }];
    [_backview1 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(addressHigth+43*kHeight);
    }];
    
    
    
    
    
    
    //    _dataSouce = arr;
    
    
    
    [_AddressTable reloadData];
    
    
}


-(void)judgementAddress{
    
    BOOL AddressIsTwo = YES;
    for (int i = 0; i<_dataSouce.count; i++) {
        
        NSString * address = [NSString stringWithFormat:@"%@",_dataSouce[i][@"address"]];
        if (address.length == 0) {
            AddressIsTwo = NO;
            break;
        }
    }
    
    if (!AddressIsTwo) {
//        [_backview2 mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.size.mas_equalTo(CGSizeMake(Main_Width, 86*kHeight));
//        }];
        pickCarHeight = NO;
    }else{
        pickCarHeight = YES;
        NSString * depaId = nil;//提车地址id
        NSString * destId = nil;//收车地址id
        
        // 0 是发货 1是收车
        //        if ([_dataSouce[0][@"addressType"] isEqualToString:@"1"]) {
        for (int i = 0; i<_dataSouce.count; i++) {
            
            if (![_dataSouce[i][@"addressType"] isEqualToString:@"1"]) {
                depaId = [NSString stringWithFormat:@"%@",_dataSouce[i][@"id"]];
            }else{
                destId = [NSString stringWithFormat:@"%@",_dataSouce[i][@"id"]];
            }
        }
        
        NSLog(@"depaId===%@destId==%@",depaId,destId);
       
//        if (depaId.length>0 &&destId.length>0 && CarNumber>0) {
            if (depaId.length>0 &&destId.length>0) {

            [dicUrl setObject:depaId forKey:@"depaId"];
            [dicUrl setObject:destId forKey:@"destId"];

            
            int carnumber = 0;
            if (_carInforArr.count>0) {
                
                for (int i=0; i<_carInforArr.count; i++) {
                    
                    carnumber = carnumber+ (int)[_carInforArr[i][@"vins"] count];
                }
            }
                NSString * deliveryDate = dicUrl[@"deliveryDate"];//提车日期
                NSString * deliveryTime = dicUrl[@"deliveryTime"];//提车时间

            NSString * url = [NSString stringWithFormat:@"%@?trailerId=%@&depaId=%@&destId=%@&vehicleNum=%d&deliveryDate=%@&deliveryTime=%@",fees_list,_carType,depaId,destId,carnumber,deliveryDate,deliveryTime];
            
                [dicUrl setObject:[NSString stringWithFormat:@"%d",carnumber] forKey:@"vehicleNum"];
                
            //查询运费
            [Common requestWithUrlString:url contentType:@"application/json" finished:^(id responseObj){
 
                if ([responseObj[@"success"] boolValue]) {
                    
                    _CarMoneyDic = responseObj[@"data"];
                    NSLog(@"%@",responseObj[@"message"]);
                    _carryMoneyLabel.text = [NSString stringWithFormat:@"￥%.2f",[_CarMoneyDic[@"actualTotal"] floatValue]];
                    
                    NSLog(@"%@",_carryMoneyLabel.text);
                    _arriveDistance.text = [NSString stringWithFormat:@"%@",_CarMoneyDic[@"distance"]];
                    _arriveData.text = [NSString stringWithFormat:@"%@",_CarMoneyDic[@"arrivalDate"]];
                    _arriveTime.text = [NSString stringWithFormat:@"%@",_CarMoneyDic[@"arrivalTime"]];
                    
                    [dicUrl setObject:_CarMoneyDic[@"arrivalDate"] forKey:@"arriveDate"];
                    [dicUrl setObject:_CarMoneyDic[@"arrivalTime"] forKey:@"arriveTime"];
                }else{
                    
                    [WKProgressHUD popMessage:responseObj[@"message"] inView:self.view duration:1.5 animated:YES];
                }
                
            } failed:^(NSString *errorMsg) {
                NSLog(@"%@",errorMsg);
            }];
        }
    }
}

-(void)bestowData:(NSMutableArray*)arr{
    
    
    UILabel * label1 = (UILabel *)[_backview2 viewWithTag:52];
    UILabel * label2 = (UILabel *)[_backview2 viewWithTag:53];
    
    NSString * label11 = [NSString stringWithFormat:@"%@",arr[0][@"endTime"]];
    label1.text = [label11 substringWithRange:NSMakeRange(0, 10)];
    
    label2.text = [NSString stringWithFormat:@"%.3fkm",[arr[0][@"distance"] floatValue]];
    label2.textColor = BtnTitleColor;
    [dicUrl setObject:arr[0][@"productId"] forKey:@"productid"];
    [dicUrl setObject:arr[0][@"endTime"] forKey:@"arriveDate"];
    
    
    SwitchNumber = 2;
    [self updateScrollFram:_carInforArr.count];
}


-(void)createData{
    
    
    NSString * addressURL = [NSString stringWithFormat:@"%@?isveneer=1&citycode=110100",addressFault_Url];
    
    [Common requestWithUrlString:addressURL contentType:@"application/json" finished:^(id responseObj){
        NSMutableArray * arr =[[NSMutableArray alloc]init];
        arr =  [NSMutableArray arrayWithArray:responseObj[@"data"]];
        _dataSouce = nil;
        _dataSouce = arr;
        [self createStarData];
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
}
-(void)viewWillAppear:(BOOL)animated{
    [TabBar setTabBarHidden:YES];
    pushIsOk = YES;
    
}


-(void)CityListDate{
    
    
    [Common requestWithUrlString:intracity_list contentType:@"application/json" finished:^(id responseObj){

        [_cityListArr removeAllObjects];
        
        _cityListArr = [NSMutableArray arrayWithArray:responseObj[@"data"]];
        
        if (_cityListArr.count !=0) {
            
            _addressCH.text = [NSString stringWithFormat:@"%@",_cityListArr[0][@"name"]];

        }

        [self judgementAddress];
        
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];

    
}


-(void)createBackView0{
    

    NSArray * carInforArr = @[@"普通板车",@"落地板车",@"厢式板车"];
    
    for (int i=1; i<4; i++) {
        
        UIImageView * image = [[UIImageView alloc]init];
        if (i==1) {
            image.image = [UIImage imageNamed:[NSString stringWithFormat:@"car_infor_imges2_%d",i]];
        }else{
            image.image = [UIImage imageNamed:[NSString stringWithFormat:@"car_infor_imges1_%d",i]];
        }
        image.contentMode = UIViewContentModeScaleAspectFit;
        [_carInforImageArr addObject:image];
        [_backview0 addSubview:image];
        [image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backview0.mas_left).with.offset((i-1)*Main_Width/3+30*kWidth);
            make.centerY.equalTo(_backview0.mas_centerY);
        }];
        //carInforArr[i-1][@"trailerName"]
        UILabel * label = [com createUIlabel:carInforArr[i-1] andFont:FontOfSize12 andColor:BtnTitleColor];
        
        [_carInforNameArr addObject:label];
        
        [_backview0 addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(image.mas_centerX);
            make.top.equalTo(image.mas_bottom).with.offset(8*kHeight);
        }];
        
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = 100+i;
        [btn addTarget:self action:@selector(pressInforBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_carInforBtnArr addObject:btn];
        [_backview0 addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backview0.mas_left).with.offset((i-1)*Main_Width/3+14*kWidth);
            make.centerY.equalTo(_backview0.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(Main_Width/3, 80*kHeight));
        }];
        if (i==1) {
            label.textColor = YellowColor;
            btn.selected = YES;
        }
    }
}



-(void)createUI{
    
    _scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, Main_Width,Main_height-64*kHeight)];
    _scroll.contentOffset = CGPointMake(0, 0);
    _scroll.contentSize = CGSizeMake(Main_Width, Main_height);
    self.automaticallyAdjustsScrollViewInsets =NO;
    _scroll.bounces = NO;
    _scroll.userInteractionEnabled = YES;
    _scroll.showsHorizontalScrollIndicator = NO;
    _scroll.showsVerticalScrollIndicator = NO;
    _scroll.delegate = self;
    _scroll.minimumZoomScale = 1.0;
    _scroll.maximumZoomScale = 3.0;
    
    _scroll.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;

    [self.view addSubview:_scroll];
    
    //整体back
    _backview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, Main_height)];
    _backview.backgroundColor = GrayColor;
    _backview.userInteractionEnabled = YES;
    [_scroll addSubview:_backview];
    [_backview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_scroll.mas_left);
        make.top.mas_equalTo(_scroll.mas_top);
        make.size.mas_equalTo(CGSizeMake(Main_Width, Main_height));
    }];
    _backview0 = [[UIView alloc]init];
    _backview0.backgroundColor = WhiteColor;
    [_backview addSubview:_backview0];
    [_backview0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left);
        make.top.mas_equalTo(_backview.mas_top).with.offset(0*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 86*kHeight));
    }];
    
    

    UIView * backview0line = [[UIView alloc]init];
    backview0line.backgroundColor = LineGrayColor;
    [_backview0 addSubview:backview0line];
    [backview0line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_backview0.mas_bottom);
        make.left.mas_equalTo(_backview0.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 3*kHeight));
    }];


    //地址模块
    _backview1 = [[UIView alloc]init];
    _backview1.backgroundColor = WhiteColor;
    [_backview addSubview:_backview1];
    [_backview1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left);
        make.top.mas_equalTo(_backview0.mas_bottom).with.offset(0*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 129*kHeight));
    }];
    UIView * addressView = [[UIView alloc]init];
    addressView.backgroundColor = headerBackViewColor;
    [_backview1 addSubview:addressView];
    [addressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left);
        make.top.mas_equalTo(_backview1.mas_top);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
    }];
    
    UILabel * address = [com createUIlabel:@"地址" andFont:FontOfSize14 andColor:BlackTitleColor];
    [addressView addSubview:address];
    [address mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(addressView.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(addressView.mas_top).with.offset(21.5*kHeight);
    }];
    
    UIView * Addressline = [[UIView alloc]init];
    Addressline.backgroundColor = LineGrayColor;
    [addressView addSubview:Addressline];
    [Addressline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(addressView.mas_bottom);
        make.left.mas_equalTo(addressView.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
    _AddressTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64*kHeight, Main_Width, 86*kHeight) style:UITableViewStylePlain];
    [_AddressTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _AddressTable.tag = AddressTag;
    _AddressTable.delegate = self;
    _AddressTable.dataSource = self;
    [_AddressTable setTableFooterView:[UIView new]];
    [_backview1 addSubview:_AddressTable];
    
    [_AddressTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left).with.offset(0);
        make.top.mas_equalTo(_backview1.mas_top).with.offset(43*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 86*kHeight));
    }];
    
    
    //时间模块
    _backview2 = [[UIView alloc]init];
    _backview2.backgroundColor = WhiteColor;
    [_backview addSubview:_backview2];
    
    [_backview2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left).with.offset(0);
        make.top.mas_equalTo(_AddressTable.mas_bottom).with.offset(8*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*3*kHeight));
    }];
    
    UIView * timeView = [[UIView alloc]init];
    timeView.backgroundColor = headerBackViewColor;
    [_backview2 addSubview:timeView];
    
    if (IS_IPHONE_6P) {
        [timeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backview2.mas_left);
            make.top.mas_equalTo(_backview2.mas_top);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 38*kHeight));
        }];
    }else{
        [timeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backview2.mas_left);
            make.top.mas_equalTo(_backview2.mas_top);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 40*kHeight));
        }];
    }
    
    UIView * Timeline = [[UIView alloc]init];
    Timeline.backgroundColor = LineGrayColor;
    [timeView addSubview:Timeline];
    [Timeline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(timeView.mas_bottom);
        make.left.mas_equalTo(timeView.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    //80  44
    
    self.PickBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.PickBtn setImage:[UIImage imageNamed:@"pressPickCar"] forState:UIControlStateSelected];
    [self.PickBtn setImage:[UIImage imageNamed:@"unPressPickCar"] forState:UIControlStateNormal];

    self.PickBtn.selected = YES;
    [self.PickBtn addTarget:self action:@selector(pressCarBtn) forControlEvents:UIControlEventTouchUpInside];
    [timeView addSubview:self.PickBtn];
    [self.PickBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(timeView.mas_top).with.offset(21.5*kHeight);
        make.left.mas_equalTo(timeView.mas_left);
        make.size.mas_equalTo(CGSizeMake(80*kWidth, 44*kHeight));
    }];
    
    
    self.OrderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.OrderBtn addTarget:self action:@selector(pressOrderCarBtn) forControlEvents:UIControlEventTouchUpInside];

    [self.OrderBtn setImage:[UIImage imageNamed:@"unPressOrder"] forState:UIControlStateNormal];
    [self.OrderBtn setImage:[UIImage imageNamed:@"PressOrder"] forState:UIControlStateSelected];
    self.OrderBtn.selected = NO;
    [timeView addSubview:self.OrderBtn];
    [self.OrderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(timeView.mas_top).with.offset(21.5*kHeight);
        make.left.mas_equalTo(self.PickBtn.mas_right);
        make.size.mas_equalTo(CGSizeMake(80*kWidth, 44*kHeight));
    }];

    

    
//    UILabel * TimeLabel = [com createUIlabel:@"时间" andFont:FontOfSize14 andColor:BlackTitleColor];
//    [timeView addSubview:TimeLabel];
//    [TimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(timeView.mas_left).with.offset(18*kWidth);
//        make.centerY.mas_equalTo(timeView.mas_top).with.offset(21.5*kHeight);
//    }];
    
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy-MM-dd"];
    NSString *  locationString=[dateformatter stringFromDate:senddate];
    
    UIImageView * Timeimage = [[UIImageView alloc]init];
    Timeimage.image = [UIImage imageNamed:@"Address_Cell_songda"];
    [_backview2 addSubview:Timeimage];
    [Timeimage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview2.mas_left).with.offset(18*kWidth);
        make.centerY.equalTo(_backview2.mas_top).with.offset(64.5*kHeight);
        make.size.mas_equalTo(CGSizeMake(17*kWidth, 13*kHeight));
    }];
    UILabel * timeLabel = [com createUIlabel:@"提车时间" andFont:FontOfSize14 andColor:fontGrayColor];
    [_backview2 addSubview:timeLabel];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(Timeimage.mas_right).with.offset(10*kWidth);
        make.centerY.equalTo(_backview2.mas_top).with.offset(64.5*kHeight);
    }];
    
    [dicUrl setObject:locationString forKey:@"deliveryDate"];//提车日期
    
    UIButton * btnDate = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDate setTitle:locationString forState:UIControlStateNormal];//fontGrayColor
    [btnDate setTitleColor:fontGrayColor forState:UIControlStateNormal];//BtnTitleColor
    [btnDate addTarget:self action:@selector(pressTimeBtn:) forControlEvents:UIControlEventTouchUpInside];
    btnDate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btnDate.tag = TimeTag;//提车时间
    btnDate.titleLabel.font = Font(FontOfSize14);
    btnDate.userInteractionEnabled = NO;
    [_backview2 addSubview:btnDate];
    [btnDate mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeLabel.mas_right).with.offset(15*kWidth);
        make.centerY.equalTo(timeLabel.mas_centerY);
        make.width.mas_equalTo(Main_Width/3);
    }];
    
    
    NSDate *date = [NSDate date]; // 获得时间对象
    NSDateFormatter  *dateformatterTime=[[NSDateFormatter alloc] init];
    [dateformatterTime setDateFormat:@"HH:mm:ss"];
    NSString * string = [dateformatterTime stringFromDate:date];
    NSString *  b = [string substringWithRange:NSMakeRange(0,2)] ;
    NSString *  c = [string substringWithRange:NSMakeRange(2,3)] ;

    
    NSString * btnlabel = [NSString stringWithFormat:@"%@%@",b,c];

    [dicUrl setObject:btnlabel forKey:@"deliveryTime"];

    _btnTime = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnTime setTitle:btnlabel forState:UIControlStateNormal];
    [_btnTime setTitleColor:fontGrayColor forState:UIControlStateNormal];
    [_btnTime addTarget:self action:@selector(pressPickCarTimeBtn:) forControlEvents:UIControlEventTouchUpInside];
    _btnTime.tag = TimeTag + 5;
    _btnTime.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _btnTime.titleLabel.font = Font(FontOfSize14);
    _btnTime.userInteractionEnabled = NO;

    [_backview2 addSubview:_btnTime];
    [_btnTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(btnDate.mas_right);
        make.centerY.equalTo(timeLabel.mas_centerY);
        make.width.mas_equalTo(Main_Width/3);
    }];
    
    UIImageView * Timeimage1 = [[UIImageView alloc]init];
    Timeimage1.image = [UIImage imageNamed:@"Address_Cell_tiche"];
    [_backview2 addSubview:Timeimage1];
    [Timeimage1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview2.mas_left).with.offset(18*kWidth);
        make.centerY.equalTo(_backview2.mas_top).with.offset(64.5*kHeight+43*kHeight);
        make.size.mas_equalTo(CGSizeMake(17*kWidth, 13*kHeight));
    }];
    
    UILabel * timeLabel1 = [com createUIlabel:@"送达时间" andFont:FontOfSize14 andColor:fontGrayColor];
    timeLabel1.tag = 51;
    [_backview2 addSubview:timeLabel1];
    [timeLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(Timeimage1.mas_right).with.offset(10*kWidth);
        make.centerY.equalTo(_backview2.mas_top).with.offset(64.5*kHeight+43*kHeight);
    }];
    
    
    _arriveData = [com createUIlabel:@"输入起运地、目的地后，平台自动计算" andFont:FontOfSize11 andColor:fontGrayColor];
    _arriveData.tag = 52;
    [_backview2 addSubview:_arriveData];
    [_arriveData mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(btnDate.mas_left);
        make.centerY.equalTo(timeLabel1.mas_centerY);
    }];
    
    _arriveTime = [com createUIlabel:@"" andFont:FontOfSize11 andColor:fontGrayColor];
    [_backview2 addSubview:_arriveTime];
    [_arriveTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_btnTime.mas_left);
        make.centerY.equalTo(timeLabel1.mas_centerY);
    }];
    
    
    
    UIView * Timeline1 = [[UIView alloc]init];
    Timeline1.backgroundColor = LineGrayColor;
    [timeView addSubview:Timeline1];
    [Timeline1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(timeLabel1.mas_centerY).with.offset(21.5*kHeight);
        make.left.mas_equalTo(Timeimage.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
    
    _backview20 = [[UIView alloc]init];
    _backview20.backgroundColor = WhiteColor;
    [_backview addSubview:_backview20];
    
    [_backview20 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left).with.offset(0);
        make.top.mas_equalTo(_backview2.mas_bottom).with.offset(8*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
    }];

    
    
    UIView * distanceView = [[UIView alloc]init];
    distanceView.backgroundColor = headerBackViewColor;
    [_backview20 addSubview:distanceView];
    [distanceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview2.mas_left);
        make.centerY.mas_equalTo(_backview20.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
    }];

    
    
    
    UIImageView * Timeimage2 = [[UIImageView alloc]init];
    Timeimage2.image = [UIImage imageNamed:@"icon_infor_Mileage_imges"];
    [_backview20 addSubview:Timeimage2];
    [Timeimage2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview20.mas_left).with.offset(18*kWidth);
        make.centerY.equalTo(_backview20.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(15*kWidth, 15*kHeight));
    }];
    
    UILabel * timeLabel2 = [com createUIlabel:@"里程数" andFont:FontOfSize14 andColor:fontGrayColor];
    timeLabel2.tag = 51;
    [_backview20 addSubview:timeLabel2];
    [timeLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(Timeimage1.mas_right).with.offset(10*kWidth);
        make.centerY.equalTo(_backview20.mas_centerY);
    }];
    
    _arriveDistance = [com createUIlabel:@"输入起运地、目的地后，平台自动计算" andFont:FontOfSize11 andColor:fontGrayColor];
    _arriveDistance.tag = 53;
    [_backview20 addSubview:_arriveDistance];
    [_arriveDistance mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(btnDate.mas_left);
        make.centerY.equalTo(timeLabel2.mas_centerY);
    }];
    
//    _warnView = [self createWarnBack];
    // 当前顶层窗口
//    UIWindow *window = [[UIApplication sharedApplication].windows lastObject];
//    // 添加到窗口
//    [window addSubview:_warnView];
//    _warnView.hidden = YES;
//    
    
    
    //添加车辆模块
    UIView * viewline =[[UIView alloc]init];
    viewline.backgroundColor = LineGrayColor;
    [_backview2 addSubview:viewline];
    [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview.mas_left).with.offset(18*kWidth);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        make.top.mas_equalTo(_backview2.mas_top).with.offset(43*2*kHeight);
    }];

    _backview3 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, 108*kHeight)];
    _backview3.backgroundColor = WhiteColor;
    [_backview addSubview:_backview3];
    [_backview3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left).with.offset(0);
        make.top.mas_equalTo(_backview20.mas_bottom).with.offset(8*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 108*kHeight));
    }];
    UIView * carView = [[UIView alloc]init];
    carView.backgroundColor = headerBackViewColor;
    [_backview3 addSubview:carView];
    [carView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview3.mas_left);
        make.top.mas_equalTo(_backview3.mas_top);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
    }];
    UIView * Carline = [[UIView alloc]init];
    Carline.backgroundColor = LineGrayColor;
    [carView addSubview:Carline];
    [Carline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(carView.mas_bottom);
        make.left.mas_equalTo(carView.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    UILabel * CarModel = [com createUIlabel:@"车辆信息" andFont:FontOfSize14 andColor:BlackTitleColor];
    [carView addSubview:CarModel];
    [CarModel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(carView.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(carView.mas_top).with.offset(21.5*kHeight);
    }];
    UIImageView * image = [[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"+"];
    [_backview3 addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview3.mas_left).with.offset(22*kWidth);
        make.size.mas_equalTo(CGSizeMake(40.5*kWidth, 40.5*kHeight));
        make.top.mas_equalTo(carView.mas_bottom).with.offset(13*kHeight);
    }];
    UILabel * labelSm1 = [com createUIlabel:@"选择托运车辆信息" andFont:FontOfSize13 andColor:FieldPlacerColor];
    [_backview3 addSubview:labelSm1];
    [labelSm1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(image.mas_right).with.offset(15*kWidth);
        make.top.mas_equalTo(carView.mas_bottom).with.offset(19*kHeight);
    }];
    UILabel * labelSm2 = [com createUIlabel:@"(可以选择多种车型的多辆车进行托运)" andFont:10.0 andColor:FieldPlacerColor];
    [_backview3 addSubview:labelSm2];
    [labelSm2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(image.mas_right).with.offset(15*kWidth);
        make.bottom.mas_equalTo(_backview3.mas_bottom).with.offset(-16*kHeight);
    }];
    UIButton * bigImage = [UIButton buttonWithType:UIButtonTypeCustom];
    bigImage.tag = 333;
    [bigImage addTarget:self action:@selector(pressCarAddBtn:) forControlEvents:UIControlEventTouchUpInside];
    [_backview3 addSubview:bigImage];
    [bigImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview3.mas_left);
        make.top.mas_equalTo(carView.mas_bottom).with.offset(13*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 55*kHeight));
    }];
    
    _CarTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, 43*0*kHeight) style:UITableViewStylePlain];
    [_CarTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _CarTable.tag = CarTag;
    _CarTable.delegate = self;
    _CarTable.dataSource = self;
    _CarTable.userInteractionEnabled = YES;
    _CarTable.scrollEnabled = NO;
    [_backview3 addSubview:_CarTable];
    [_CarTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview3.mas_left).with.offset(0);
        make.top.mas_equalTo(_backview3.mas_top).with.offset(43.5*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*0*kHeight));
    }];
    
    
    
    //付款方模块
    _backview30 = [[UIView alloc]init];
    _backview30.backgroundColor = WhiteColor;
    [_backview addSubview:_backview30];
    [_backview30 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left).with.offset(0);
        make.top.mas_equalTo(_backview3.mas_bottom).with.offset(8*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*3*kHeight));
    }];
    
    
    UILabel * payMan = [com createUIlabel:@"付款方:" andFont:FontOfSize14 andColor:BlackTitleColor];
    [_backview30 addSubview:payMan];
    
    [payMan mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview30.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_backview30.mas_top).with.offset(21.5*kHeight);
    }];
    UIView * payManline = [[UIView alloc]init];
    payManline.backgroundColor = LineGrayColor;
    [_backview30 addSubview:payManline];
    [payManline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_backview30.mas_top).with.offset(43*kHeight);
        make.left.mas_equalTo(_backview30.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    UIView * payManline1 = [[UIView alloc]init];
    payManline1.backgroundColor = LineGrayColor;
    [_backview30 addSubview:payManline1];
    [payManline1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_backview30.mas_top).with.offset(43*2*kHeight);
        make.left.mas_equalTo(_backview30.mas_left).with.offset(18*kWidth);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    
    UIImageView * image0 = [[UIImageView alloc]init];
    image0.image = [UIImage imageNamed:@"选中状态"];
    image0.tag = 1000;
    image0.layer.cornerRadius = 10*kHeight;
    [_backview30 addSubview:image0];
    [image0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(payMan.mas_left).with.offset(0);
        make.centerY.mas_equalTo(_backview30.mas_top).with.offset((21.5+43)*kHeight);
        make.size.mas_equalTo(CGSizeMake(14*kWidth, 14*kHeight));
    }];
    
    UILabel * label0 = [com createUIlabel:@"我来付款" andFont:FontOfSize12 andColor:fontGrayColor];
    [_backview30 addSubview:label0];
    [label0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(image0.mas_right).with.offset(11*kWidth);
        make.centerY.mas_equalTo(image0.mas_centerY);
    }];
    
    UIButton * btn0 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn0 addTarget:self action:@selector(pressBtn0) forControlEvents:UIControlEventTouchUpInside];
//    btn0.backgroundColor = RedColor;
    [_backview30 addSubview:btn0];
    [btn0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview30.mas_left);
        make.centerY.mas_equalTo(image0.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(100*kWidth, 40*kHeight));
    }];

    
    UIImageView * image1 = [[UIImageView alloc]init];
    image1.image = [UIImage imageNamed:@"默认状态"];
    image1.tag = 1001;
    image1.layer.cornerRadius = 10*kHeight;
    [_backview30 addSubview:image1];
    [image1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(payMan.mas_left);
        make.centerY.mas_equalTo(_backview30.mas_top).with.offset((86+21.5)*kHeight);
        make.size.mas_equalTo(CGSizeMake(14*kWidth, 14*kHeight));
    }];
    UILabel * label1 = [com createUIlabel:@"其他商户付款" andFont:FontOfSize12 andColor:fontGrayColor];
    [_backview30 addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(image1.mas_right).with.offset(11*kWidth);
        make.centerY.mas_equalTo(image1.mas_centerY);
    }];

    UIButton * btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn1 addTarget:self action:@selector(pressBtn1) forControlEvents:UIControlEventTouchUpInside];
//    btn1.backgroundColor = RedColor;
    [_backview30 addSubview:btn1];
    [btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview30.mas_left);
        make.centerY.mas_equalTo(image1.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(100*kWidth, 40*kHeight));
    }];

    payShopperField =[self createField:@"请输入商户账号" andTag:1 andFont:FontOfSize13];
    payShopperField.placeholder = @"请输入商户账号";
    payShopperField.userInteractionEnabled = NO;
    [_backview30 addSubview:payShopperField];
    [payShopperField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label1.mas_right).with.offset(10);
        make.centerY.mas_equalTo(label1.mas_centerY);
    }];
    

    //订单模块
    _backview4 = [[UIView alloc]init];
    _backview4.backgroundColor = WhiteColor;
    [_backview addSubview:_backview4];
    [_backview4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left).with.offset(0);
        make.top.mas_equalTo(_backview30.mas_bottom).with.offset(8*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 90*kHeight));
    }];
    
    UILabel * labelInfor = [com createUIlabel:@"订单备注" andFont:FontOfSize14 andColor:BlackTitleColor];
    [_backview4 addSubview:labelInfor];
    [labelInfor mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview4.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_backview4.mas_top).with.offset(21.5*kHeight);
    }];
    
    UIView * Inforline = [[UIView alloc]init];
    Inforline.backgroundColor = LineGrayColor;
    [_backview4 addSubview:Inforline];
    [Inforline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_backview4.mas_top).with.offset(43*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5*kHeight));
        make.left.mas_equalTo(_backview4.mas_left);
    }];
    
    UIImageView * RightImage= [[UIImageView alloc]init];
    RightImage.image = [UIImage imageNamed:@"common_list_arrows_more"];
    [_backview4 addSubview:RightImage];
    [RightImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backview4.mas_right).with.offset(-15*kWidth);
        make.centerY.mas_equalTo(labelInfor.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(8*kWidth, 13*kHeight));
    }];


    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn addTarget:self action:@selector(pressBtnComment) forControlEvents:UIControlEventTouchUpInside];
    
    [_backview4 addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(labelInfor.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(Main_Width-100, 30*kHeight));
        make.right.mas_equalTo(_backview4.mas_right).with.offset(0);
    }];
    _labelComment = [[UILabel alloc]init];
    _labelComment.textColor = fontGrayColor;
    _labelComment.font = Font(FontOfSize12);
    _labelComment.text = @"请司机务必验车，以及随车物品。";
    _labelComment.textAlignment = NSTextAlignmentLeft;
    _labelComment.numberOfLines = 2;
    [_backview4 addSubview:_labelComment];
    [_labelComment mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview4.mas_left).with.offset(18*kWidth);
        make.bottom.mas_equalTo(_backview4.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(Main_Width-36*kWidth, 50*kHeight));
    }];

    UIView * shipBtnView = [[UIView alloc]init];
    shipBtnView.backgroundColor = GrayColor;
    [self.view addSubview:shipBtnView];
    [shipBtnView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_backview.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 70*kHeight));
        make.bottom.mas_equalTo(self.view.mas_bottom);
    }];
    
    UIButton * ShipBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [ShipBtn setTitle:@"确定发车" forState:UIControlStateNormal];
    [ShipBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
    [ShipBtn addTarget:self action:@selector(pressShipBtn:) forControlEvents:UIControlEventTouchUpInside];
    ShipBtn.layer.cornerRadius = 5;
    ShipBtn.layer.borderWidth = 0.5;
    ShipBtn.layer.borderColor = YellowColor.CGColor;
    ShipBtn.backgroundColor = YellowColor;
    [shipBtnView addSubview:ShipBtn];
    [ShipBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(shipBtnView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(293*kWidth, 40*kHeight));
        make.top.mas_equalTo(shipBtnView.mas_top).with.offset(15*kHeight);
    }];
    

    _backview5 = [[UIView alloc]init];
    _backview5.backgroundColor = WhiteColor;
    [_backview addSubview:_backview5];
    [_backview5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview.mas_left).with.offset(0);
        make.top.mas_equalTo(_backview4.mas_bottom).with.offset(8*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
    }];
    
    
    UILabel * Moneylabel = [com createUIlabel:@"订单费用" andFont:FontOfSize14 andColor:BlackTitleColor];
    [_backview5 addSubview:Moneylabel];
    [Moneylabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview5.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_backview5.mas_top).with.offset(21.5*kHeight);
    }];

    UIImageView * RightImageMoney= [[UIImageView alloc]init];
    RightImageMoney.image = [UIImage imageNamed:@"common_list_arrows_more"];
    [_backview5 addSubview:RightImageMoney];
    [RightImageMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backview.mas_right).with.offset(-15*kWidth);
        make.centerY.mas_equalTo(Moneylabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(8*kWidth, 13*kHeight));
    }];

    
    _carryMoneyLabel = [self createUIlabel:@"￥0.00" andFont:FontOfSize14 andColor:YellowColor];
    [_backview5 addSubview:_carryMoneyLabel];
    [_carryMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(RightImageMoney.mas_left).with.offset(-10*kWidth);
        make.centerY.mas_equalTo(_backview5.mas_top).with.offset(21.5*kHeight);
    }];
    
    
    UIButton * btnMoney = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnMoney addTarget:self action:@selector(pressBtnMoney) forControlEvents:UIControlEventTouchUpInside];
    [_backview addSubview:btnMoney];
    [btnMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backview.mas_right).with.offset(-18*kWidth);
        make.size.mas_equalTo(CGSizeMake(200*kWidth, 40*kHeight));
        make.centerY.mas_equalTo(_backview4.mas_bottom).with.offset(21.5*kHeight);
    }];

}


-(void)pressBtn0{
    
    UIImageView * image0 = (UIImageView*)[_backview30 viewWithTag:1000];
    UIImageView * image1 = (UIImageView*)[_backview30 viewWithTag:1001];
    image0.image = [UIImage imageNamed:@"选中状态"];
    image1.image = [UIImage imageNamed:@"默认状态"];
    payShopperField.userInteractionEnabled = NO;
    
    [dicUrl setObject:@(10) forKey:@"payRole"];//10:下单人付款 20:其他商户付款
}
-(void)pressBtn1{
    
    UIImageView * image0 = (UIImageView*)[_backview30 viewWithTag:1000];
    UIImageView * image1 = (UIImageView*)[_backview30 viewWithTag:1001];
    image0.image = [UIImage imageNamed:@"默认状态"];
    image1.image = [UIImage imageNamed:@"选中状态"];
    payShopperField.userInteractionEnabled = YES;

    [dicUrl setObject:@(20) forKey:@"payRole"];//10:下单人付款 20:其他商户付款
}


-(void)pressCarBtn{
    
    _PickBtn.selected = YES;
    _OrderBtn.selected = NO;
    
    UIButton * btn1 = [_backview2 viewWithTag:TimeTag];
    UIButton * btn2 = [_backview2 viewWithTag:TimeTag+5];
    [btn1 setTitleColor:fontGrayColor forState:UIControlStateNormal];
    [btn2 setTitleColor:fontGrayColor forState:UIControlStateNormal];

    btn1.userInteractionEnabled = NO;
    btn2.userInteractionEnabled = NO;

    
    
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy-MM-dd"];
    NSString *  locationString=[dateformatter stringFromDate:senddate];
    

    
    [dicUrl setObject:locationString forKey:@"deliveryDate"];//提车日期
    [btn1 setTitle:locationString forState:UIControlStateNormal];
    
    NSDate *date = [NSDate date]; // 获得时间对象
    NSDateFormatter  *dateformatterTime=[[NSDateFormatter alloc] init];
    [dateformatterTime setDateFormat:@"HH:mm:ss"];
    NSString * string = [dateformatterTime stringFromDate:date];
    NSString *  b = [string substringWithRange:NSMakeRange(0,2)] ;
    NSString *  c = [string substringWithRange:NSMakeRange(2,3)] ;
    
    NSString * btnlabel = [NSString stringWithFormat:@"%@%@",b,c];
    
    [dicUrl setObject:btnlabel forKey:@"deliveryTime"];
    
    [btn2 setTitle:btnlabel forState:UIControlStateNormal];
    
    
    
    
}

-(void)pressOrderCarBtn{
    
    _PickBtn.selected = NO;
    _OrderBtn.selected = YES;
    UIButton * btn1 = [_backview2 viewWithTag:TimeTag];
    UIButton * btn2 = [_backview2 viewWithTag:TimeTag+5];
    btn1.userInteractionEnabled = YES;
    btn2.userInteractionEnabled = YES;
    [btn1 setTitleColor:BtnTitleColor forState:UIControlStateNormal];
    [btn2 setTitleColor:BtnTitleColor forState:UIControlStateNormal];
    


}



-(void)pressInforBtn:(UIButton*)sender{
    
    
    NSInteger index = sender.tag - 101;
    if (!sender.selected) {
        for (int i=0; i<_carInforBtnArr.count; i++) {
            
            UIButton * btn = _carInforBtnArr[i];
            btn.selected = NO;
            UILabel * label = _carInforNameArr[i];
            label.textColor = BtnTitleColor;
            UIImageView * image = _carInforImageArr[i];
            image.image = [UIImage imageNamed:[NSString stringWithFormat:@"car_infor_imges1_%d",i+1]];
        }
    }
    sender.selected = YES;
    UILabel * label = _carInforNameArr[index];
    label.textColor = YellowColor;
    UIImageView * image = _carInforImageArr[index];
    image.image = [UIImage imageNamed:[NSString stringWithFormat:@"car_infor_imges2_%ld",index+1]];
    
    
    //标准轿车:S, 标准SUV:M, 大型SUV:L, 超大型车:EX
//    NSArray * cartype = @[@"S",@"M",@"L",@"EX"];
    _carType = [NSString stringWithFormat:@"%@",carTypeArr[index][@"id"]];
    [dicUrl setObject:_carType forKey:@"trailerId"];
    
    [self judgementAddress];
}


//订单费用详情
-(void)pressBtnMoney{
    
    int money = [self.CarMoneyDic[@"actualTotal"] intValue];
    if (money>0) {
        CDmoneyVC * cdmoney = [[CDmoneyVC alloc]init];
        cdmoney.CarMoneyDic = _CarMoneyDic;
        [self.navigationController pushViewController:cdmoney animated:YES];
    }
    
}

//评论详情
-(void)pressBtnComment{
    
    
    CDcommentsVC * cdcomment = [[CDcommentsVC alloc]init];
    if (![_labelComment.text isEqualToString:@"请司机务必验车，以及随车物品。"]) {
        cdcomment.IsHave = _labelComment.text;
    }
    [self.navigationController pushViewController:cdcomment animated:YES];
    
    cdcomment.callCommentBack = ^(NSString * comment){
        _labelComment.text = comment;
        if (!comment||comment.length == 0) {
            _labelComment.text = @"请司机务必验车，以及随车物品。";
            [dicUrl setObject:@"" forKey:@"comment"];
        }else{
            [dicUrl setObject:comment forKey:@"comment"];
        }
    };
}


-(void)pressWarnBack{
    
    _warnView.hidden = YES;
    
}
-(void)WarnBackTouch{
    
    _warnView.hidden = YES;
}


-(void)pressWarnBtn{
    
    _warnView.hidden = NO;
    
}




-(void)safeFieldWithText1:(UITextField*)field{
    
    [UIView animateWithDuration:0.3 animations:^{
        _scroll.contentOffset = CGPointMake(0, 438*kHeight);
    } completion:nil];
    
}
-(void)pressPromptBtn:(UIButton*)sender{
    
    if (!sender.selected) {
        _prompt1.hidden = NO;
        _prompt2.hidden = NO;
        sender.selected = YES;
    }else{
        _prompt1.hidden = YES;
        _prompt2.hidden = YES;
        sender.selected = NO;

    }
}



-(void)pressCarAddBtn:(UIButton*)sender{
    
    AddCarTypeVC * addcartype = [[AddCarTypeVC alloc]init];
    addcartype.CityListName = @"123";

    addcartype.callBack = ^(NSMutableArray * carArr){
        
        
        for (int i=0; i<carArr.count; i++) {
            [_carInforArr addObject: carArr[i]];
        }
        CarNumber = (int)_carInforArr.count;
        [dicUrl setObject:_carInforArr forKey:@"vehicles"];

        [self judgementAddress];
        [self refreshTatalCarsLabel];
        [self updateCarAndScrollFram:CarNumber];
        [self updateScrollFram:CarNumber];
    };
    if (_carInforArr.count <50) {
        [self.navigationController pushViewController:addcartype animated:YES];
    }else{
        [self createUIAlertController:@"添加车辆不能超过50台"];
    }
}



//确定发车
-(void)pressShipBtn:(UIButton*)sender{

    NSString * arriveDate = dicUrl[@"arriveDate"];
    NSString * arriveTime = dicUrl[@"arriveTime"];
    if (arriveDate.length==0 || arriveTime.length==0) {
        
        [self createUIAlertController:@"请选择正确时间"];
        return;
    }

    NSString * address1 = [NSString stringWithFormat:@"%@",_dataSouce[0][@"address"]];
    NSString * address2 = [NSString stringWithFormat:@"%@",_dataSouce[1][@"address"]];
    
    if (address1.length==0||address2.length==0) {
        [self createUIAlertController:@"请填完整地址信息"];
        return;
    }else{
        
        
//        [dicUrl setObject:_dataSouce[1][@"id"] forKey:@"destId"];
//        [dicUrl setObject:_dataSouce[0][@"id"] forKey:@"depaId"];
        
        
        if (!chooseAddressYesOrNo) {
            
            [dicUrl setObject:_dataSouce[1][@"id"] forKey:@"destId"];
            [dicUrl setObject:_dataSouce[0][@"id"] forKey:@"depaId"];
            
        }else{
            [dicUrl setObject:_dataSouce[0][@"id"] forKey:@"destId"];
            [dicUrl setObject:_dataSouce[1][@"id"] forKey:@"depaId"];
            
        }

        
        
        
        NSString * carinforLength = [NSString stringWithFormat:@"%@",dicUrl[@"vehicles"]];
        if (carinforLength.length == 0) {
            [self createUIAlertController:@"请选择车辆信息"];
            return;
        }else{
            
            NSString * payRole = [NSString stringWithFormat:@"%@",dicUrl[@"payRole"]];
            NSString * phone = [NSString stringWithFormat:@"%@",dicUrl[@"phone"]];

            if ([payRole isEqualToString:@"20"]) {

                if (phone.length !=11) {
                    [self createUIAlertController:@"请填写支付账户"];
                    return;
                }
            }
                NSMutableArray * carinforarr = dicUrl[@"vehicles"];
                NSData * jsonData = [NSJSONSerialization dataWithJSONObject:carinforarr options:NSJSONWritingPrettyPrinted error:nil];
                NSString * jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
                
                [dicUrl setObject:jsonString forKey:@"vehicles"];
                WKProgressHUD * hud = [WKProgressHUD showInView:self.view withText:@"请稍等" animated:YES];
                [com afPostRequestWithUrlString:cworder_save_list parms:dicUrl finishedBlock:^(id responseObj) {
                    NSMutableDictionary * dicObj =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];
                    
                    if ([dicObj[@"success"] boolValue]) {
                        
                        [hud dismiss:YES];
                        [self createUIAlertControllerBack:@"订单发布成功"];
                        
                    }else{
                        [hud dismiss:YES];
                        [self createUIAlertController:[self backString: dicObj[@"message"]]];
                        [dicUrl setObject:_carInforArr forKey:@"vehicles"];
                    }

                } failedBlock:^(NSString *errorMsg) {
                    [self createUIAlertController:errorMsg];
                    [dicUrl setObject:_carInforArr forKey:@"vehicles"];
                }];
        }
    }
}

#pragma mark tableView cellForRowAtIndexPath

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *str=@"str";
    if (tableView.tag== AddressTag) {
        AddressCell * cell =  [tableView dequeueReusableCellWithIdentifier:str];
        if (cell ==nil) {
            cell = [[AddressCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }
        if (indexPath.row==0) {
            cell.SmallImage.image = [UIImage imageNamed:@"Address_Cell_fache"];
            for (int i =0; i<_dataSouce.count; i++) {
                NSString *addressType = [NSString stringWithFormat:@"%@", _dataSouce[i][@"addressType"]];
                NSString * address = [NSString stringWithFormat:@"%@",_dataSouce[i][@"address"]];
                // 0 是发货
                BOOL isZero = [addressType isEqualToString:@"0"];
                if (isZero && address.length!=0) {
                    

                    PlaceModel = [PlaceOrderModel ModelWithDic:_dataSouce[i]];
                    NSString * cityName = nil;
                    cityName = [NSString stringWithFormat:@"%@-%@",PlaceModel.cityName,PlaceModel.countyName];

                    cell.CityName.text = cityName;
                    cell.StoreName.text = @"";
                    cell.AddressName.text = PlaceModel.address;
                    NSString * people = [NSString stringWithFormat:@"%@ %@",PlaceModel.contact,PlaceModel.phone];
                    cell.PeopleName.text = people;
                    cell.viewSline.hidden = NO;
                    cell.CityName.textColor = littleBlackColor;
                    break;
                }else{
                    cell.viewSline.hidden = YES;
                    cell.CityName.text = @"起运地";
                    cell.CityName.textColor = FieldPlacerColor;
                    cell.StoreName.text = @"";
                    cell.AddressName.text = @"";
                    cell.PeopleName.text = @"";
                }
            }
        }
        else{
            cell.viewline.hidden = YES;
            cell.SmallImage.image = [UIImage imageNamed:@"Address_Cell_shouche"];
            for (int i =0; i<_dataSouce.count; i++) {
                NSString * address = [NSString stringWithFormat:@"%@",_dataSouce[i][@"address"]];
                if ([_dataSouce[i][@"addressType"] isEqualToString:@"1"]&&address.length!=0) {
                    
                    PlaceModel = [PlaceOrderModel ModelWithDic:_dataSouce[i]];
                    NSString * cityName = nil;
                    cityName = [NSString stringWithFormat:@"%@-%@",PlaceModel.cityName,PlaceModel.countyName];
                    
                    cell.CityName.text = cityName;
                    cell.StoreName.text = @"";
                    cell.AddressName.text = PlaceModel.address;
                    NSString * people = [NSString stringWithFormat:@"%@ %@",PlaceModel.contact,PlaceModel.phone];
                    cell.PeopleName.text = people;
                    cell.viewSline.hidden = NO;
                    cell.CityName.textColor = littleBlackColor;
                    break;
                }else{
                    cell.viewSline.hidden = YES;
                    cell.CityName.text = @"目的地";
                    cell.CityName.textColor = FieldPlacerColor;
                    cell.StoreName.text = @"";
                    cell.AddressName.text = @"";
                    cell.PeopleName.text = @"";
                }
            }
        }
        if (_dataSouce.count==0) {
            cell.viewSline.hidden = YES;
        }
        [_AddressTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (tableView.tag == InforTableTag){
        
        placeAddressCell * cell =  [tableView dequeueReusableCellWithIdentifier:str];
        if (cell ==nil) {
            cell = [[placeAddressCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }
        return cell;
    }
    else{
        CarCell * cell =  [tableView dequeueReusableCellWithIdentifier:str];
        if (cell ==nil) {
            cell = [[CarCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSString * brandLogo = [NSString stringWithFormat:@"%@",_carInforArr[indexPath.row][@"brandLogo"]];
        cell.SmallImage.contentMode = UIViewContentModeScaleAspectFit;//图片缩放适应imageView大小
        [cell.SmallImage sd_setImageWithURL:[NSURL URLWithString:brandLogo] placeholderImage:[UIImage imageNamed:@"car_style1"]];
        cell.CarName.text = [NSString stringWithFormat:@"%@-%@",_carInforArr[indexPath.row][@"brandname"],_carInforArr[indexPath.row][@"vehiclename"]];
        cell.CarNumber.text = [NSString stringWithFormat:@"%@辆",_carInforArr[indexPath.row][@"vehiclecount"]];
        cell.deleteBtn.tag = indexPath.row;
        [cell.deleteBtn addTarget:self action:@selector(pressCarCellDeleteBtn:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
}

-(void)pressCarCellDeleteBtn:(UIButton *)sender{
    
    CarNumber --;
    [_carInforArr removeObjectAtIndex:sender.tag];
    if (_carInforArr.count==0) {
        [dicUrl setObject:@"" forKey:@"vehiclesbrands"];
    }
    //    [self updateFristScrollFram:addressHigth];
    //    [_CarTable reloadData];
    
    [self judgementAddress];
    [self updateCarAndScrollFram:CarNumber];
    [self updateScrollFram:CarNumber];
    [_CarTable reloadData];
}



#pragma mark tableView dataSource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag == AddressTag) {
        if (indexPath.row == 0) {
            if (_dataSouce.count>0) {
                CGFloat height = 43*kHeight;
                for (int i=0; i<_dataSouce.count;i++) {
                    if ([_dataSouce[i][@"addressType"] isEqualToString:@"0"]&&![_dataSouce[i][@"address"] isEqualToString:@""]) {
                        height = addressCellHight1;
                    }
                }
                return height;
            }else{
                return 43*kHeight;
            }
        }else{
            if (_dataSouce.count>0) {
                CGFloat height = 43*kHeight;
                for (int i=0; i<_dataSouce.count;i++) {
                    if ([_dataSouce[i][@"addressType"] isEqualToString:@"1"]&&![_dataSouce[i][@"address"] isEqualToString:@""]) {
                        height = addressCellHight2;
                    }
                }
                return height;
            }else{
                return 43*kHeight;
            }
        }
    }else if (tableView.tag == InforTableTag){
        return 0;
    }else{
        return 43*kHeight;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return (tableView.tag == InforTableTag)?43*kHeight:0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView.tag == AddressTag)  return 2;
    else if (tableView.tag == InforTableTag)  return 2;
    else  return CarNumber;
}
//设置分区个数
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return (tableView.tag == InforTableTag)? 2:1;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag == AddressTag) {
        //跳转到地址选择列表  1:收车 0:发车
        if (pushIsOk) {
            pushIsOk = NO;
            (indexPath.row == 1)?[self navAddressView:@"1"]:[self navAddressView:@"0"];
        }
    }
}

#pragma mark tableView Header Footer

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView.tag == InforTableTag) {
        return _InforHeaderCarArr[section];
    }
    return nil;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    if (tableView.tag == CarTag) {
        UIView * view = [[UIView alloc]init];
        view.frame = CGRectMake(0, 0, Main_Width, 43*kHeight);
        UILabel * label = [com createUIlabel:@"添加车型" andFont:FontOfSize14 andColor:BlackTitleColor];
        [view addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(view.mas_centerX);
            make.centerY.mas_equalTo(view.mas_centerY);
        }];
        UIButton * AddBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        AddBtn.tag = 444;
        [AddBtn addTarget:self action:@selector(pressCarAddBtn:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:AddBtn];
        [AddBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(view.mas_centerX);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 40*kHeight));
            make.centerY.mas_equalTo(view.mas_centerY);
        }];
        
        UIImageView * addImage = [[UIImageView alloc]init];
        addImage.image = [UIImage imageNamed:@"jia"];
        [view addSubview:addImage];
        [addImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(label.mas_left).with.offset(-9*kWidth);
            make.size.mas_equalTo(CGSizeMake(17*kWidth, 17*kHeight));
            make.centerY.mas_equalTo(AddBtn.mas_centerY);
        }];
        return view;
    }
    else{
        UIView * view = [[UIView alloc]init];
        view.frame = CGRectMake(0, 0, 0, 0);
        return view;
    }
}



-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return (tableView.tag == CarTag)?43*kHeight:0;
}

-(void)judgementKeepAddress:(NSString*) addresstype{
    
    NSString * string = [NSString stringWithFormat:@"%@?addresstype=%@",addressUser_Url,addresstype];
    [Common requestWithUrlString:string contentType:@"application/json" finished:^(id responseObj) {
        
        NSMutableArray * addressArr = responseObj[@"data"];
        keepAddressNumber = (int)[addressArr count];
        
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
}

#pragma mark 刷新数据和UI

-(void)updateScrollFram:(NSInteger)number{
    
    CGFloat totalHeight ;
    if (pickCarHeight) {
        
        totalHeight = addressCellHight1+addressCellHight2;
        //        totalHeight = addressCellHight1+addressCellHight2;
    }else{//86  129
        
        totalHeight = addressCellHight1+addressCellHight2;
    }
    [_backview mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(43*(_CarName.count+number+2)*kHeight+Main_height+totalHeight+129*kHeight);
    }];
    _scroll.contentSize = CGSizeMake(Main_Width, Main_height+43*(_CarName.count+number+2)*kHeight+totalHeight+129*kHeight);
}
-(void)updateFristScrollFram:(CGFloat)totalHeight{
    
    if (pickCarHeight) {
        
        totalHeight = totalHeight;
        //        totalHeight = totalHeight+43*kHeight;
    }else{//86  129
        
        totalHeight = totalHeight;
    }
    [_backview mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(Main_height+totalHeight+110*kHeight+129*kHeight);
    }];
    _scroll.contentSize = CGSizeMake(Main_Width, Main_height+totalHeight+100*kHeight+129*kHeight);
}

-(void)updateAddressTable:(CGFloat)totalHeight{
    
    [_AddressTable mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(totalHeight);
    }];
    [_backview1 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(totalHeight+43*kHeight);
    }];
}

-(void)updateCarAndScrollFram:(int)number{
    
    if (number==0) {
        [_CarTable mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0);
        }];
        [_backview3 mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(108*kHeight);
        }];
    }else{
        [_CarTable mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(43*(number+1)*kHeight);
        }];
        [_backview3 mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(43*(number+2)*kHeight);
        }];
    }
    [_CarTable reloadData];
    [_backview mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(43*(_CarName.count+SwitchNumber+number)*kHeight+Main_height+86*kHeight+addressHigth+129*kHeight);
    }];
    _scroll.contentSize = CGSizeMake(Main_Width, Main_height+43*(_CarName.count+SwitchNumber+number)*kHeight+86*kHeight+addressHigth+129*kHeight);
}

-(void)refreshTatalCarsLabel{
    NSInteger tatalLabel = 0;
    for (int i =0; i<_carInforArr.count; i++) {
        tatalLabel = tatalLabel+[_carInforArr[i][@"vins"] count];
    }
    NSString * Number = [NSString stringWithFormat:@"%ld辆",(long)tatalLabel];
    UILabel * label = (UILabel *)[_backview4 viewWithTag:TatalLabelTag];
    label.text = Number;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark UIPickerView

//提车日期 年月日
-(void)pressTimeBtn:(UIButton*)sender{
    
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.tag = sender.tag;
    ActionSheetPicker *actionSheet = [[ActionSheetPicker alloc] initWithTarget:self successAction:@selector(selectOK:) cancelAction:nil origin:datePicker];
    datePicker.datePickerMode=UIDatePickerModeDate;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    //最小年月日（小于它，自动调回）
    NSDate* minDate = [NSDate date];
    NSDate *  senddate=[NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:senddate];
    //明天
    [components setDay:([components day])];
    
    
    //最大年月日（今天）
    NSDate* maxDate = [formatter dateFromString:@"2039-01-01"];
    datePicker.maximumDate = maxDate;
    if (sender.tag == TimeTag+1) {
        NSDate *beginningOfWeek = [gregorian dateFromComponents:components];
        datePicker.minimumDate = beginningOfWeek;
    }else{
        //提车日期只能选择今天 明天
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDate *currentDate = [NSDate date];
        NSDateComponents *comps = [[NSDateComponents alloc] init];
        [comps setDay:1];//设置最大时间为：当前时间推后十年
        NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
        
        datePicker.minimumDate = minDate;
        datePicker.maximumDate = maxDate;
        
    }
    CGRect pickerFrame = CGRectMake(0, 40, actionSheet.viewSize.width, 216);
    [datePicker setFrame:pickerFrame];
    
    datePicker.datePickerMode = UIDatePickerModeDate;
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];//设置为简体中文显示
    datePicker.locale = locale;
    actionSheet.pickerView = datePicker;
    actionSheet.title = @"";
    
    UIButton *leftBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setTitle:@"取消"forState:UIControlStateNormal];
    leftBtn.frame=CGRectMake(0, 0, 50, 45);
    [leftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *leftIte=[[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    
    UIButton *rightBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
    rightBtn.frame=CGRectMake(0, 0, 50, 45);
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightIte=[[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    [actionSheet setDoneButton:rightIte];
    [actionSheet setCancelButton:leftIte];
    [actionSheet showActionSheetPicker];
}
- (void)selectOK:(id)sender{
    
    if ([sender isKindOfClass:[UIDatePicker class]]) {
        
        UIDatePicker *picker = sender;
        UIButton * btn = (UIButton*)[_backview2 viewWithTag:TimeTag];
        UIButton * btn1 = (UIButton*)[_backview2 viewWithTag:TimeTag+1];
        

        if (picker.tag == TimeTag+1) {
            NSDate * date = [picker date];
            NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents *components = [gregorian components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
            //明天
            [components setDay:([components day])];
            NSDate *beginningOfWeek = [gregorian dateFromComponents:components];
            NSDateFormatter *dateday = [[NSDateFormatter alloc] init];
            [dateday setDateFormat:@"yyyy-MM-dd"];
            NSString * dataStri = [NSString stringWithFormat:@"%@",[dateday stringFromDate:beginningOfWeek]];;
            
            NSString *time = [dataStri substringWithRange:NSMakeRange(0, 10)];
            [btn1 setTitle:time forState:UIControlStateNormal];
            
            [dicUrl setObject:time forKey:@"arriveDate"];

        }else{
            //显示2015-07-23 的日期
            NSDate * date = [picker date];
            NSDateFormatter *dateday = [[NSDateFormatter alloc] init];
            [dateday setDateFormat:@"yyyy-MM-dd"];

            NSString * dataStri = [NSString stringWithFormat:@"%@",[dateday stringFromDate:date]];;

            //显示2015-07-23 的日期
            NSString *time = [dataStri substringWithRange:NSMakeRange(0, 10)];
            NSString *string = [NSString stringWithFormat:@"%@",time];
            [btn setTitle:string forState:UIControlStateNormal];
            [dicUrl setObject:string forKey:@"deliveryDate"];

        }
        
        [self judgementAddress];
        
    }
}


-(void)selectTime:(id)sender{
    
  
    UIPickerView *pick = sender;//105 106
    NSInteger row = [pick selectedRowInComponent:0];
    NSInteger row1=[pick selectedRowInComponent:1];

    NSString *value=[_timeChoose1 objectAtIndex:row];
    NSString *value1=[_timeChoose2 objectAtIndex:row1];

    NSString *education=[NSString stringWithFormat:@"%@:%@",value,value1];
    NSLog(@"education===%@",education);
    NSDate *date = [NSDate date]; // 获得时间对象
    NSDateFormatter  *dateformatterTime=[[NSDateFormatter alloc] init];
    [dateformatterTime setDateFormat:@"HH:mm:ss"];
    NSString * string = [dateformatterTime stringFromDate:date];
    
    
    
    int  b = [[string substringWithRange:NSMakeRange(0,2)] intValue];
    int  c = [[string substringWithRange:NSMakeRange(2,2)] intValue];

    int d = [value intValue];
    int e = [value1 intValue];
    
    
    //显示2015-07-23 的日期
    NSString *time = [[date description] substringWithRange:NSMakeRange(0, 10)];
    NSString *NowDate = [NSString stringWithFormat:@"%@",time];

    if ([NowDate isEqualToString:dicUrl[@"deliveryDate"]]) {
        
        if (d<b || e<c) {
            [self createUIAlertController:@"请选择正确时间"];
            return;
        }else{
            [_btnTime setTitle:education forState:UIControlStateNormal];
            [dicUrl setObject:education forKey:@"deliveryTime"];
        }
    }else{
        [_btnTime setTitle:education forState:UIControlStateNormal];
        [dicUrl setObject:education forKey:@"deliveryTime"];
    }

    [self judgementAddress];
    
    NSLog(@"选择的时间点:%@",education);
}

//提车时间  12：21
-(void)pressPickCarTimeBtn:(UIButton*)btn{
    
    
    UIPickerView *education = [[UIPickerView alloc] init];
    ActionSheetPicker *actionSheet=[[ActionSheetPicker alloc]initWithTarget:self successAction:@selector(selectTime:) cancelAction:nil origin:education];
    CGRect pickerFrame = CGRectMake(0, 40, actionSheet.viewSize.width, 216);
    [education setFrame:pickerFrame];
    education.tag = btn.tag; //105 106
    education.delegate = self;
    education.dataSource = self;
    actionSheet.pickerView = education;
    actionSheet.title = @"标题";
    UIButton *leftBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setTitle:@"取消"forState:UIControlStateNormal];
    leftBtn.frame=CGRectMake(0, 0, 50, 45);
    [leftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *leftIte=[[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    UIButton *rightBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
    rightBtn.frame=CGRectMake(0, 0, 50, 45);
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightIte=[[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    [actionSheet setDoneButton:rightIte];
    [actionSheet setCancelButton:leftIte];
    [actionSheet showActionSheetPicker];
}



#pragma mark UIPickerView dataSource

//点击每一行cell  跳出的pickerview  显示列数
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

//显示pickerview 每个视图的行数
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
//    return _timeChoose.count;
    switch (component) {
        case 0:
        {
            return _timeChoose1.count;
        }
            break;
        case 1:
        {
            return _timeChoose2.count;
        }
        default:
            break;
    }

    return 0;
}
//显示每个pickerview  每行的具体内容
-(UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *lable  = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
    lable.adjustsFontSizeToFitWidth = YES;
    lable.textAlignment = NSTextAlignmentCenter;
    if (component == 0) {
        lable.text = _timeChoose1[row];
    }else{
        lable.text = _timeChoose2[row];
    }
    return lable;
}

-(void)navAddressView:(NSString*)addresstype {
    
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    if ([addresstype isEqualToString:@"1"]) {
        [dic setObject:@"1" forKey:@"addresstype"];//收车
    }else{
        [dic setObject:@"0" forKey:@"addresstype"];//发车
    }
    NSLog(@"%@",addressUser_Url);
    NSString * string = [NSString stringWithFormat:@"%@?addresstype=%@&isVeneer=1",addressUser_Url,dic[@"addresstype"]];
    
    [Common requestWithUrlString:string contentType:@"application/json" finished:^(id responseObj) {
        NSMutableArray * dataSource = [[NSMutableArray alloc]init];
        dataSource = responseObj[@"data"];
        if (dataSource == nil || dataSource.count==0 ) {
            [self gotoAddAddressView:addresstype];
        }else {
            [self gotoListAddressView:addresstype];
        }
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];

}
//跳转到添加页面
-(void)gotoAddAddressView:(NSString *)addresstype {
    CDAddressVC * address = [[CDAddressVC alloc]init];
    address.addresstype = addresstype;
    address.navtitle = @"2";
    address.CityListName = _cityListArr[0][@"name"];
    address.CityListCode = _cityListArr[0][@"code"];
    __weak CityDeliveryVC * weekSelf = self;
    
    address.callBackAddAddress=^(NSMutableDictionary * dicBlock,NSString * addresstype){
        if ([addresstype isEqualToString:@"1"]) {
            for (int i = 0; i<_dataSouce.count; i++) {
                if ([weekSelf.dataSouce[i][@"addressType"] isEqualToString:@"1"]) {
                    [weekSelf.dataSouce replaceObjectAtIndex:i withObject:dicBlock] ;
                    addressCellHight2 = 100*kHeight;
//                    chooseAdressImge.hidden = NO;
//                    chooseAddressBtn.hidden = NO;

                }
            }
        }
        else{
            NSLog(@"weekSelf.dataSouce==%@",weekSelf.dataSouce);
            for (int i = 0; i<weekSelf.dataSouce.count; i++) {
                if ([_dataSouce[i][@"addressType"] isEqualToString:@"0"]) {
                    [weekSelf.dataSouce replaceObjectAtIndex:i withObject:dicBlock] ;
                    addressCellHight1 = 100*kHeight;
                }
            }
        }
        NSString * address0 = [NSString stringWithFormat:@"%@",_dataSouce[0][@"address"]];
        NSString * address1 = [NSString stringWithFormat:@"%@",_dataSouce[1][@"address"]];
        
        if (address0.length == 0 && address1.length == 0) {
            
            chooseAdressImge.hidden = YES;
            chooseAddressBtn.hidden = YES;
        }else{
            chooseAdressImge.hidden = NO;
            chooseAddressBtn.hidden = NO;
            
        }
        
        CGFloat totalHeight = addressCellHight1 +addressCellHight2;
//        [weekSelf judgementAddress];
        [self updateScrollFram:_carInforArr.count];
        [self updateAddressTable:totalHeight];
        NSLog(@"%f",_backview.frame.size.height);
        //        [weekSelf.AddressTable reloadData];
    };
    
    [self.navigationController pushViewController:address animated:YES];
}
//跳转到list页面
-(void)gotoListAddressView:(NSString*)addresstype {
    ChooseAddressVC * address = [[ChooseAddressVC alloc]init];
    address.CityListName = _cityListArr[0][@"name"];
    address.CityListCode = _cityListArr[0][@"code"];
    if ([addresstype isEqualToString:@"1"]) {
        address.addresstype = @"1"; //收货
    }else{
        address.addresstype = @"0"; //发货
    }
    
    __weak CityDeliveryVC * weekSelf = self;
    NSLog(@"weekSelf.dataSouce==%@",weekSelf.dataSouce);
    
    address.callRemoveBack = ^(NSString * removeAddress){
        
        for (int i =0; i<_dataSouce.count; i++) {
            
            NSLog(@"%@",weekSelf.dataSouce[i]);
            NSString * carID = [NSString stringWithFormat:@"%@",weekSelf.dataSouce[i][@"id"]];
            if ([carID isEqualToString:removeAddress]) {
                NSMutableDictionary *departaddrid = [[NSMutableDictionary alloc]init];
                if ([weekSelf.dataSouce[i][@"addressType"] isEqualToString:@"0"]){
                    [departaddrid setValue:@"0" forKey:@"addressType"];
                    [departaddrid setValue:@"" forKey:@"address"];
                    addressCellHight1 = 43*kHeight;
                }else{
                    [departaddrid setValue:@"1" forKey:@"addressType"];
                    [departaddrid setValue:@"" forKey:@"address"];
                    addressCellHight2 = 43*kHeight;
                }
                
                [_dataSouce replaceObjectAtIndex:i withObject:departaddrid];
                
                NSString * address0 = [NSString stringWithFormat:@"%@",_dataSouce[0][@"address"]];
                NSString * address1 = [NSString stringWithFormat:@"%@",_dataSouce[1][@"address"]];

                if (address0.length == 0 && address1.length == 0) {
                    
                        chooseAdressImge.hidden = YES;
                        chooseAddressBtn.hidden = YES;
                }else{
                    chooseAdressImge.hidden = NO;
                    chooseAddressBtn.hidden = NO;

                }
                
                [weekSelf judgementAddress];
                addressHigth = addressCellHight1+addressCellHight2;
                [self updateAddressTable:addressHigth];
                pickCarHeight = NO;
                [self updateScrollFram:(int)_carInforArr.count];
                [weekSelf.AddressTable reloadData];
            }
        }
    };
    address.callBack=^(NSMutableDictionary * dicBlock,NSString * addresstype){
        
        // 0 是发货  1 是收
        if ([addresstype isEqualToString:@"1"]) {
            addressCellHight1 = 100*kHeight;
            for (int i = 0; i<_dataSouce.count; i++) {
                NSString * address = [NSString stringWithFormat:@"%@",_dataSouce[i][@"address"]];
                if (address.length == 0 && [_dataSouce[i][@"addressType"] isEqualToString:@"0"]) {
                    addressCellHight1 = 43*kHeight;
                }
                if ([weekSelf.dataSouce[i][@"addressType"] isEqualToString:@"1"]) {
                    [weekSelf.dataSouce replaceObjectAtIndex:i withObject:dicBlock] ;
                    addressCellHight2 = 100*kHeight;
                }
            }
        }
        else{
            NSLog(@"weekSelf.dataSouce==%@",weekSelf.dataSouce);
            addressCellHight2 = 100*kHeight;
            for (int i = 0; i<_dataSouce.count; i++) {
                NSString * address = [NSString stringWithFormat:@"%@",_dataSouce[i][@"address"]];
                if (address.length == 0 && [_dataSouce[i][@"addressType"] isEqualToString:@"1"]) {
                    addressCellHight2 = 43*kHeight;
                }
                if ([weekSelf.dataSouce[i][@"addressType"] isEqualToString:@"0"]) {
                    [weekSelf.dataSouce replaceObjectAtIndex:i withObject:dicBlock] ;
                    addressCellHight1 = 100*kHeight;
                }
            }
        }
        [weekSelf judgementAddress];
        
        NSString * address0 = [NSString stringWithFormat:@"%@",_dataSouce[0][@"address"]];
        NSString * address1 = [NSString stringWithFormat:@"%@",_dataSouce[1][@"address"]];
        
        if (address0.length == 0 && address1.length == 0) {
            
            chooseAdressImge.hidden = YES;
            chooseAddressBtn.hidden = YES;
        }else{
            chooseAdressImge.hidden = NO;
            chooseAddressBtn.hidden = NO;
            
        }
        
        addressHigth = addressCellHight1 +addressCellHight2;
        [self updateScrollFram:(int)_carInforArr.count];
        [self updateAddressTable:addressHigth];
        [weekSelf.AddressTable reloadData];
    };
    [self.navigationController pushViewController:address animated:YES];
}


#pragma mark 系统提示框  自定义键盘 自定义按钮 自定义文本
-(void)createUIAlertControllerWithTitle:(NSString*)HeaderTitle and:(NSString*)title
{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:HeaderTitle message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)createUIAlertControllerBack:(NSString*)title
{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"返回首页" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[HomeViewController class]]) {
                [self.navigationController popToViewController:controller animated:YES];
            }
        }
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"继续发单" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [dicUrl setObject:@"" forKey:@"vehicleNum"];//车辆数
        [dicUrl setObject:@"" forKey:@"vehicles"];//车辆信息
        [dicUrl setObject:@"" forKey:@"comment"];//备注
        _labelComment.text = @"请司机务必验车，以及随车物品。";
//        _arriveDistance.text = @"输入起运地、目的地后，平台自动计算";
        _carryMoneyLabel.text = @"￥0.00";

        for (int i=0; i<_carInforBtnArr.count; i++) {
            
        UIButton * btn = _carInforBtnArr[i];
        btn.selected = NO;
        UILabel * label = _carInforNameArr[i];
        label.textColor = BtnTitleColor;
        UIImageView * image = _carInforImageArr[i];
        image.image = [UIImage imageNamed:[NSString stringWithFormat:@"car_infor_imges1_%d",i+1]];
    }
        
        UILabel * label = _carInforNameArr[0];
        label.textColor = YellowColor;
        UIImageView * image = _carInforImageArr[0];
        image.image = [UIImage imageNamed:[NSString stringWithFormat:@"car_infor_imges2_%d",1]];

        _carType = [NSString stringWithFormat:@"%@",carTypeArr[0][@"id"]];
        [dicUrl setObject:_carType forKey:@"trailerId"];
        
        [_carInforArr removeAllObjects];
        
        
        UIImageView * image0 = (UIImageView*)[_backview30 viewWithTag:1000];
        UIImageView * image1 = (UIImageView*)[_backview30 viewWithTag:1001];
        image1.image = [UIImage imageNamed:@"默认状态"];
        image0.image = [UIImage imageNamed:@"选中状态"];
        payShopperField.userInteractionEnabled = NO;
        payShopperField.text = @"";
        payShopperField.placeholder = @"请输入商户账号";
        
        UIButton * btn11 = [_backview2 viewWithTag:TimeTag];
        UIButton * btn22 = [_backview2 viewWithTag:TimeTag+5];
        [btn11 setTitleColor:fontGrayColor forState:UIControlStateNormal];
        [btn22 setTitleColor:fontGrayColor forState:UIControlStateNormal];

        [dicUrl setObject:@(10) forKey:@"payRole"];//10:下单人付款 20:其他商户付款

        
        chooseAddressYesOrNo = NO;
        [self judgementAddress];
        pickCarHeight = NO;
        _CarPromptField.text = @"";
        [self createData];
        CarNumber = 0;
        [self updateScrollFram:0];
        [self updateCarAndScrollFram:0];
        [_AddressTable reloadData];
        [_CarTable reloadData];

        
        
        _PickBtn.selected = YES;
        _OrderBtn.selected = NO;
        
        UIButton * btn1 = [_backview2 viewWithTag:TimeTag];
        UIButton * btn2 = [_backview2 viewWithTag:TimeTag+5];
        btn1.userInteractionEnabled = NO;
        btn2.userInteractionEnabled = NO;
        
        
        
        NSDate *  senddate=[NSDate date];
        NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
        [dateformatter setDateFormat:@"yyyy-MM-dd"];
        NSString *  locationString=[dateformatter stringFromDate:senddate];
        
        
        
        [dicUrl setObject:locationString forKey:@"deliveryDate"];//提车日期
        [btn1 setTitle:locationString forState:UIControlStateNormal];
        
        NSDate *date = [NSDate date]; // 获得时间对象
        NSDateFormatter  *dateformatterTime=[[NSDateFormatter alloc] init];
        [dateformatterTime setDateFormat:@"HH:mm:ss"];
        NSString * string = [dateformatterTime stringFromDate:date];
        NSString *  b = [string substringWithRange:NSMakeRange(0,2)] ;
        NSString *  c = [string substringWithRange:NSMakeRange(2,3)] ;
        
        NSString * btnlabel = [NSString stringWithFormat:@"%@%@",b,c];
        
        [dicUrl setObject:btnlabel forKey:@"deliveryTime"];
        
        [btn2 setTitle:btnlabel forState:UIControlStateNormal];
        
        
        
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)createUIAlertController:(NSString*)title
{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
//        for (UIViewController *controller in self.navigationController.viewControllers) {
//            if ([controller isKindOfClass:[PlaceOrderVC class]]) {
//                [self.navigationController popToViewController:controller animated:YES];
//            }
//        }
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}
//点击return按钮 键盘隐藏 这是协议中的方法
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //键盘隐藏 也就是让键盘取消第一响应者身份
    [textField resignFirstResponder];
    return YES;
}
-(CustomTextField*)createField:(NSString*)placeholder andTag:(NSInteger)tag andFont:(double)font{
    
    CustomTextField * field =[[CustomTextField alloc]init];
    field.frame = CGRectMake(0, 0, Main_Width-90*kWidth, 43*kWidth);
    field.delegate = self;
    field.userInteractionEnabled = YES;
    field.textAlignment = NSTextAlignmentLeft;
    CGFloat width = field.frame.size.width;
    CGFloat height = field.frame.size.height;
    [field placeholderRectForBounds:CGRectMake(width/2, 0, width, height)];
    return field;
}
-(NewBtn*)createBtn:(NSString*)title {
    
    NewBtn * btn = [NewBtn buttonWithType:UIButtonTypeCustom];
    btn.layer.borderWidth = 0.5;
    btn.layer.borderColor = littleBlackColor.CGColor;
    btn.titleLabel.font = Font(FontOfSize13);
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:BlackColor forState:UIControlStateNormal];
    return btn;
}
-(UIButton*)createBtn:(NSString*)title andBorderColor:(UIColor*)BorderColor andFont:(CGFloat)Font andTitleColor:(UIColor*)TitleColor andBackColor:(UIColor*)BackColor {
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.layer.borderWidth = 0.5;
    btn.layer.cornerRadius = 5;
    btn.backgroundColor = BackColor;
    btn.layer.borderColor = BorderColor.CGColor;
    btn.titleLabel.font = Font(Font);
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:TitleColor forState:UIControlStateNormal];
    return btn;
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];
    CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentCenter;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}
-(UITextField*)createFieldMoney:(NSString*)placeholder andTag:(NSInteger)tag andFont:(double)font{
    
    UITextField * field =[[UITextField alloc]init];
    //    field.frame = CGRectMake(94*kWidth, 0, Main_Width-200*kWidth, 43*kWidth);
    field.frame = CGRectMake(120*kWidth, 0, Main_Width-200*kWidth, 50*kWidth);
    field.delegate = self;
    field.userInteractionEnabled = YES;
    field.textAlignment = NSTextAlignmentLeft;
    field.placeholder = placeholder;
    return field;
}

/* 解决触摸手势和cell点击的冲突 */
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    if ([touch.view isEqual:_warnView]) {
        return YES;
    }
    return NO;
}

//提示背景
-(UIView*)createWarnBack{
    
    UIView * view= [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, Main_height)];
    view.backgroundColor = RGBACOLOR(0, 0, 0, 0.3);
    view.userInteractionEnabled = YES;
    
    UITapGestureRecognizer * Tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(WarnBackTouch)];
    Tap.delegate = self;
    [view addGestureRecognizer:Tap];
    
    UIView * warnBackView = [[UIView alloc]init];
    warnBackView.backgroundColor = WhiteColor;
    [view addSubview:warnBackView];
    [warnBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(view.mas_top).with.offset(130*kHeight);
        make.centerX.mas_equalTo(view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(250*kWidth, 280*kHeight));
        
    }];
    //503 × 190 pixels
    UIImageView * warnBackImage = [[UIImageView alloc]init];
    warnBackImage.image = [UIImage imageNamed:@"图层-0"];
    [warnBackView addSubview:warnBackImage];
    [warnBackImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(warnBackView.mas_centerX);
        make.top.mas_equalTo(warnBackView.mas_top);
        make.size.mas_equalTo(CGSizeMake(250*kWidth, 95*kHeight));
    }];
    
    UILabel * label1 = [com createUIlabel:@"温馨提示" andFont:FontOfSize14 andColor:WhiteColor];
    [warnBackImage addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(warnBackImage.mas_centerX);
        make.bottom.mas_equalTo(warnBackImage.mas_bottom).with.offset(-3*kHeight);
    }];
    
    
    UIView * line = [[UIView alloc]init];
    line.backgroundColor = GrayColor;
    [warnBackView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(warnBackView.mas_left);
        make.right.equalTo(warnBackView.mas_right);
        make.height.mas_equalTo(0.5*kHeight);
        make.top.mas_equalTo(warnBackView.mas_bottom).with.offset(-43*kHeight);
    }];
    
    UILabel * label2 = [[UILabel alloc]init];
    label2.text = @"运输距离 <= 500 KM:提车1天,在途1天,第三天完成交车(下单时间晚于12点,提车时间从第二天起);运输距离 > 500 KM:交付时间从提车时间起,共需(运距/500)天,最高不超过7天.\n\n提车时间默认当前日期当前时间所属的时间段,可以当前时间选择之后的任意日期的任意时间段;如果选择的时间段为12点-24点,预计到达时间往后延一天.";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:label2.text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:1*kHeight];//调整行间距
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [label2.text length])];
    label2.attributedText = attributedString;
    [label2 sizeToFit];
    label2.numberOfLines = 0;
    
    label2.font = Font(FontOfSize11);
    label2.textColor = littleBlackColor;
    [warnBackView addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(warnBackView.mas_left).with.offset(8*kWidth);
        make.right.equalTo(warnBackView.mas_right).with.offset(-8*kHeight);
        make.top.mas_equalTo(warnBackImage.mas_bottom).with.offset(3*kHeight);
    }];
    
 
    UILabel * label = [com createUIlabel:@"知道了" andFont:FontOfSize14 andColor:YellowColor];
    [warnBackView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(warnBackView.mas_centerX);
        make.centerY.mas_equalTo(line.mas_bottom).with.offset(21.5*kHeight);
    }];
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 10, 10);
    [btn addTarget:self action:@selector(pressWarnBack) forControlEvents:UIControlEventTouchUpInside];
    [warnBackView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(warnBackView.mas_left);
        make.right.mas_equalTo(warnBackView.mas_right);
        make.height.mas_equalTo(43*kHeight);
        make.bottom.mas_equalTo(warnBackView.mas_bottom);
    }];
    return view;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if ([string isEqualToString:@"\n"])  //按会车可以改变
    {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容

    if ([toBeString length] > 10 ) {
        
        textField.text = [toBeString substringToIndex:11];
        [self judgePayManNumber:[toBeString substringToIndex:11]];

        return NO;
    }else{
        [dicUrl setObject:toBeString  forKey:@"phone"];
    }

    
    return YES;
}



-(void)judgePayManNumber:(NSString*)Number{
    //user_isexisted
    NSString * string = [NSString stringWithFormat:@"%@/%@",user_isexisted,Number];
    [Common requestWithUrlString:string contentType:@"application/json" finished:^(id responseObj){

        if (![responseObj[@"success"] boolValue]) {
            [self createUIAlertController:responseObj[@"message"]];
            [dicUrl setObject:@"" forKey:@"phone"];//如果是20必填

        }else{
            [dicUrl setObject:Number forKey:@"phone"];//如果是20必填
        }
    
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
    
}
-(NSString *)backString:(NSString *)string
{
    
    if ([string isEqual:[NSNull null]]) {
        
        return @"";
        
    } else {
        return  string;
    }
    
}


@end
