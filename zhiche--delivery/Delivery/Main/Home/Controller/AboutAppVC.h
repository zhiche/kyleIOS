//
//  AboutAppVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/11/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"
@interface AboutAppVC : NavViewController<UITableViewDelegate,UITableViewDataSource>

@end
