//
//  PayVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/6/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface PayVC : NavViewController
@property (nonatomic,strong) NSString * orderid;
@property (nonatomic,strong) NSString * quoteid;
@end
