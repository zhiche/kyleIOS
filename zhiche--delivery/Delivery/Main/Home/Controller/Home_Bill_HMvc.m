//
//  Home_Bill_HMvc.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/12/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "Home_Bill_HMvc.h"
#import "Common.h"
#import <Masonry.h>
#import "RootViewController.h"
#import "BillTimeCell.h"
#import "BillSortCell.h"
#import "Bill_HM_VC.h"
#import <UIImageView+WebCache.h>
#import "Home_BillVC.h"
#define MySetValue(dict, value) ((!dict[value] || [dict[value] isKindOfClass:[NSNull class]])?@"":dict[value])

@interface Home_Bill_HMvc ()
{
    UIImageView * nav;
    Common * com;
    RootViewController * TabBar;
    UITableView * table;
    NSMutableArray * BillListArr;
    NSMutableDictionary * BillDetailsDic;
    NSInteger indexrow;
    NSString * isIntegral;
    UIButton * BillDetaile;
    UITableView * SortTable;
    NSMutableArray * sortArr;
    
    BOOL ShareIsOk;

}
@property (nonatomic,strong) UIView * backView1;
@property (nonatomic,strong) UIView * backView2;
@property (nonatomic,strong) UIView * backView20;
@property (nonatomic,strong) UIView * backView3;

@property (nonatomic,strong) UILabel * dataLabel;//日期
@property (nonatomic,strong) UILabel * orderNumberLabel;//订单数
@property (nonatomic,strong) UILabel * CarNumberLabel;  //发运车辆数
@property (nonatomic,strong) UILabel * MoneyLabel;//账单金额
@property (nonatomic,strong) UILabel * BalanceLabel;//结算金额


@end

@implementation Home_Bill_HMvc

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GrayColor;
    
    nav = [self createNav:@"我的账单"];
    
    [self.view addSubview:nav];
    self.ShareDic = [[NSMutableDictionary alloc]init];
    
    TabBar = [RootViewController defaultsTabBar];
    
    com = [[Common alloc]init];
    BillListArr = [[NSMutableArray alloc]init];
    BillDetailsDic = [[NSMutableDictionary alloc]init];
    sortArr = [[NSMutableArray alloc]init];
    
    indexrow = 0;
    isIntegral = @"0";
    ShareIsOk = NO;
    
    [self createBackView1];
    [self createBackView2];
    [self createBackView20];
    
    [self createBtn];
    [self createTable];
    [self createDate];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TabBar setTabBarHidden:YES];

}



-(void)createDate{
    
    NSString * url = [NSString stringWithFormat:@"%@",bill_totallist_Url];
    
    
    
    [Common requestWithUrlString:url contentType:@"application/json" finished:^(id responseObj){
        
        if ([responseObj[@"data"] count]>0) {
            BillDetaile.userInteractionEnabled = YES;
            //            [nullView removeFromSuperview];
            BillListArr = responseObj[@"data"][@"cBillOrdersVos"];
            sortArr = responseObj[@"data"][@"billTypes"];
            [self createUIdate:BillListArr[0]];
            
            [self createGetPointDate:[NSString stringWithFormat:@"%@",BillListArr[0][@"id"]]];
            
            if (BillListArr.count <4) {
                
                UIImageView * image = (UIImageView*)[_backView3 viewWithTag:1111];
                [image mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.size.mas_equalTo(CGSizeMake(74.5*kWidth, 44*kHeight*BillListArr.count));
                }];
                [table mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.size.mas_equalTo(CGSizeMake(70.5*kWidth, 40*kHeight*BillListArr.count));
                }];
            }
            [table reloadData];
            [SortTable reloadData];
            
        }else{
            
            [self createGetPointDate:[NSString stringWithFormat:@"0"]];
        }
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
}

-(void)createUIdate:(NSMutableDictionary*)dic{
    
    //        "id": 6320,
    //        "userId": 82,
    //        "userName": "纳尼",
    //        "userType": 40,
    //        "amount": 63067.00,
    //        "beginBalance": null,
    //        "status": 10,
    //        "beginDate": "2016-09-25 00:00:00",
    //        "monthText": "2016年10月",
    //        "endDate": "2016-10-24 23:59:59",
    //        "createTime": "2016-10-17 16:01:35",
    //        "updateTime": "2016-10-17 16:01:35",
    //        "orderAmount": 44,
    //        "carAmount": 2
    
    _MoneyLabel.text = [NSString stringWithFormat:@"%.2f",[dic[@"amount"] floatValue]];
    _orderNumberLabel.text = [NSString stringWithFormat:@"%@单",dic[@"orderAmount"]];
    _CarNumberLabel.text = [NSString stringWithFormat:@"%@辆",dic[@"carAmount"]];
    _dataLabel.text = [NSString stringWithFormat:@"%@",dic[@"monthText"]];
}


-(void)createGetPointDate:(NSString*)BillId{
    
    
    NSString * url = [NSString stringWithFormat:@"%@?pageNo=1&pageSize=10&billId=%@",Find_BillDetails_Url,BillId];
    [com afPostRequestWithUrlString:url parms:nil finishedBlock:^(id responseObj) {
        NSMutableDictionary * dicObj =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];
        
        if ([dicObj[@"success"] boolValue]) {
            //13691337000  15810610001
            
            NSString * billfiles = [NSString stringWithFormat:@"%@",dicObj[@"data"][@"billFiles"]];
            if (![billfiles isEqualToString:@"<null>"]) {
                
                ShareIsOk= YES;
                self.ShareDic = dicObj[@"data"][@"billFiles"];
            }
            
            BillDetailsDic = dicObj[@"data"];
            isIntegral = @"0";
            
            if ([BillDetailsDic[@"scoresVo"][@"actualPaid"] isEqual:[NSNull null]]) {
                
                _BalanceLabel.text = @"0.00";
            } else {
                _BalanceLabel.text = [NSString stringWithFormat:@"￥%.2f",[BillDetailsDic[@"scoresVo"][@"actualPaid"] floatValue]];
            }
        }
        
    } failedBlock:^(NSString *errorMsg) {
    }];
}


-(void)createBackView1{
    
    _backView1 = [[UIView alloc]init];
    _backView1.backgroundColor = WhiteColor;
    [self.view addSubview:_backView1];
    [_backView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.mas_equalTo(nav.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 174.5*kHeight));
    }];
    
    _dataLabel = [com createUIlabel:@"" andFont:FontOfSize12 andColor:carScrollColor];
    [_backView1 addSubview:_dataLabel];
    [_dataLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backView1.mas_left).with.offset(17*kWidth);
        make.centerY.mas_equalTo(_backView1.mas_top).with.offset(22*kHeight);
    }];
    
    
    
    UIImageView * image = [[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"icon_infor_data_imges"];
    [_backView1 addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backView1.mas_right).with.offset(-17*kWidth);
        make.centerY.mas_equalTo(_dataLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(17*kWidth, 18*kHeight));
    }];
    
    UIButton * btnTime = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnTime addTarget:self action:@selector(pressTimeBtn) forControlEvents:UIControlEventTouchUpInside];
    btnTime.tag = 100;
    [_backView1 addSubview:btnTime];
    [btnTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backView1.mas_right);
        make.centerY.mas_equalTo(_dataLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50*kWidth, 44*kHeight));
    }];
    
    UIButton * shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareBtn addTarget:self action:@selector(pressShareBtn) forControlEvents:UIControlEventTouchUpInside];
    [shareBtn setImage:[UIImage imageNamed:@"分享"] forState:UIControlStateNormal];
    shareBtn.tag = 100;
    [_backView1 addSubview:shareBtn];
    [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(image.mas_left);
        make.centerY.mas_equalTo(_dataLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50*kWidth, 44*kHeight));
    }];
    
    
    UIView * line = [[UIView alloc]init];
    line.backgroundColor = GrayColor;
    [_backView1 addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backView1.mas_left);
        make.centerY.mas_equalTo(_backView1.mas_top).with.offset(44*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
    UILabel * name = [com createUIlabel:@"本月账单金额汇总" andFont:FontOfSize13 andColor:AddCarColor];
    [_backView1 addSubview:name];
    [name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_backView1.mas_centerX);
        make.centerY.mas_equalTo(line.mas_bottom).with.offset(43*kHeight);
    }];
    
    _MoneyLabel = [com createUIlabel:@"0.00" andFont:23.0 andColor:YellowColor];
    [_backView1 addSubview:_MoneyLabel];
    [_MoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_backView1.mas_centerX);
        make.centerY.mas_equalTo(line.mas_bottom).with.offset(68*kHeight);
    }];
    
    UILabel * orderName = [com createUIlabel:@"订单：" andFont:FontOfSize11 andColor:AddCarColor];
    [_backView1 addSubview:orderName];
    [orderName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backView1.mas_left).with.offset(82*kWidth);
        make.centerY.mas_equalTo(_MoneyLabel.mas_centerY).with.offset(30*kHeight);
    }];
    _orderNumberLabel = [com createUIlabel:@"0" andFont:FontOfSize11 andColor:AddCarColor];
    [_backView1 addSubview:_orderNumberLabel];
    [_orderNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(orderName.mas_right);
        make.centerY.mas_equalTo(orderName.mas_centerY);
    }];
    UILabel * carName = [com createUIlabel:@"发运车辆：" andFont:FontOfSize11 andColor:AddCarColor];
    [_backView1 addSubview:carName];
    [carName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_orderNumberLabel.mas_right).with.offset(60*kWidth);
        make.centerY.mas_equalTo(_orderNumberLabel.mas_centerY);
    }];
    
    _CarNumberLabel = [com createUIlabel:@"0" andFont:FontOfSize11 andColor:AddCarColor];
    [_backView1 addSubview:_CarNumberLabel];
    [_CarNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(carName.mas_right);
        make.centerY.mas_equalTo(carName.mas_centerY);
    }];
}


-(void)pressShareBtn{
    

    
    if (!ShareIsOk) {
        [self createUIAlertController:@"本月暂无账单可分享"];
    }else{
        [self navRightBtnClick];
    }
    
//    if (self.ShareDic[@"desc"]) {
//        [self navRightBtnClick];
//    }else{
//        [self createUIAlertController:@"本月暂无账单可分享"];
//
//    }

}


-(void)createBackView2{
    
    _backView2 = [[UIView alloc]init];
    _backView2.backgroundColor = WhiteColor;
    [self.view addSubview:_backView2];
    [_backView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.mas_equalTo(_backView1.mas_bottom).with.offset(5*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
    }];
    
    
    UILabel * label1 = [com createUIlabel:@"本月总金额" andFont:FontOfSize12 andColor:carScrollColor];
    [_backView2 addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backView2.mas_left).with.offset(17*kWidth);
        make.centerY.mas_equalTo(_backView2.mas_centerY);
    }];
    
    _BalanceLabel = [com createUIlabel:@"" andFont:FontOfSize12 andColor:YellowColor];
    [_backView2 addSubview:_BalanceLabel];
    [_BalanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backView2.mas_right).with.offset(-15*kWidth);
        make.centerY.mas_equalTo(label1.mas_centerY);
    }];
}


-(void)createBackView20{

    _backView20 = [[UIView alloc]init];
    _backView20.backgroundColor = WhiteColor;
    [self.view addSubview:_backView20];
    [_backView20 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.mas_equalTo(_backView2.mas_bottom).with.offset(5*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*4*kHeight));
    }];
    
    UILabel * billSort = [com createUIlabel:@"分类账单" andFont:FontOfSize14 andColor:BlackTitleColor];
    [_backView20 addSubview:billSort];
    [billSort mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backView20.mas_left).with.offset(17*kWidth);
        make.centerY.mas_equalTo(_backView20.mas_top).with.offset(21.5*kHeight);
    }];
    
    
    UIView * line = [[UIView alloc]init];
    line.backgroundColor = GrayColor;
    [_backView20 addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backView20.mas_left);
        make.centerY.mas_equalTo(_backView20.mas_top).with.offset(44*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
    SortTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 0, 43*kHeight) style:UITableViewStylePlain];
    [SortTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    SortTable.layer.cornerRadius = 5;
    SortTable.showsVerticalScrollIndicator = NO;
    SortTable.showsHorizontalScrollIndicator = NO;
    SortTable.tag = 2000;
    SortTable.delegate = self;
    SortTable.dataSource = self;
    [_backView20 addSubview:SortTable];
    [SortTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backView20.mas_left);
        make.top.mas_equalTo(line.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight*3));
    }];
    
    /*
    NSArray * nameArr = @[@"同城配送",@"城际运输",@"人送代驾"].mutableCopy;
    NSArray * imageArr = @[@"同城",@"城际",@"人送"].mutableCopy;
    
    for (int i=0; i<3; i++) {
        
        UIImageView * image = [[UIImageView alloc]init];
        image.image = [UIImage imageNamed:imageArr[i]];
        [_backView20 addSubview:image];
        [image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backView20.mas_left).with.offset(17*kWidth);
            make.centerY.mas_equalTo(line.mas_bottom).with.offset(21.5*kHeight+43*i*kHeight);
            make.size.mas_equalTo(CGSizeMake(17*kWidth, 17*kHeight));
        }];
        
        UILabel * label = [com createUIlabel:nameArr[i] andFont:FontOfSize12 andColor:carScrollColor];
        [_backView20 addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(image.mas_right).with.offset(11*kWidth);
            make.centerY.mas_equalTo(image.mas_centerY);
        }];

        UIImageView * RightImage =[[UIImageView alloc]init];
        RightImage.image = [UIImage imageNamed:@"common_list_arrows_more"];
        RightImage.tag = 1000;
        [_backView20 addSubview:RightImage];
        [RightImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_backView20.mas_right).with.offset(-15*kWidth);
            make.centerY.mas_equalTo(label.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(8*kWidth, 13*kHeight));
        }];
    
        if (i<2) {
            UIView * line1 = [[UIView alloc]init];
            line1.backgroundColor = GrayColor;
            [_backView20 addSubview:line1];
            [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(image.mas_left);
                make.centerY.mas_equalTo(line.mas_bottom).with.offset(43*kHeight+43*kHeight*i);
                make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
            }];
        }
        
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = 100+i;
        [btn addTarget:self action:@selector(pressSortBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_backView20 addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(image.mas_left);
            make.centerY.mas_equalTo(image.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 40*kHeight));
        }];
    }
     */
}



-(void)pressSortBtn:(UIButton*)sender{
    
    
//    NSLog(@"%@",sortArr);
    switch (sender.tag) {
        case 100:
            NSLog(@"点击同城");
            break;
        case 101:
            NSLog(@"点击城际");
            break;
        case 102:
            NSLog(@"点击人送");
            break;
        default:
            break;
    }
}


-(void)pressBillDetailBtn{
    
    
    Bill_HM_VC * bill = [[Bill_HM_VC alloc]init];
    bill.orderID = BillListArr[indexrow][@"id"];
    bill.monthText = BillListArr[indexrow][@"monthText"];
    [self.navigationController pushViewController:bill animated:YES];

}

-(void)createBtn{
    
    
    BillDetaile = [UIButton buttonWithType:UIButtonTypeCustom];
    [BillDetaile setTitle:@"账单明细" forState:UIControlStateNormal];
    [BillDetaile setTitleColor:WhiteColor forState:UIControlStateNormal];
    BillDetaile.titleLabel.font = Font(FontOfSize11);
    //    BillDetaile.backgroundColor = [UIColor cyanColor];
    [BillDetaile addTarget:self action:@selector(pressBillDetailBtn) forControlEvents:UIControlEventTouchUpInside];
    [nav addSubview:BillDetaile];
    [BillDetaile mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(nav.mas_right);
        make.size.mas_equalTo(CGSizeMake(70*kWidth, 40*kHeight));
        make.centerY.mas_equalTo(nav.mas_centerY);
    }];
    
}

-(void)createTable{
    
    _backView3 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, Main_height)];
    _backView3.backgroundColor = RGBACOLOR(0, 0, 0, 0.3);
    _backView3.userInteractionEnabled = YES;
    _backView3.hidden = YES;
    UIWindow *window = [[UIApplication sharedApplication].windows lastObject];
    // 添加到窗口
    [window addSubview:_backView3];
    
    UITapGestureRecognizer * Tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dataBackDiss)];
    Tap.delegate = self;
    [_backView3 addGestureRecognizer:Tap];
    
    UIImageView * image = [[UIImageView alloc]init];
    image.userInteractionEnabled = YES;
    image.tag = 1111;
    image.image = [UIImage imageNamed:@"icon_infor_drop-down_imges"];
    [_backView3 addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_backView3.mas_right).with.offset(-5*kWidth);
        make.top.mas_equalTo(_backView3.mas_top).with.offset(90*kHeight);
        make.size.mas_equalTo(CGSizeMake(74.5*kWidth, 44*kHeight*4));
    }];
    
    
    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 0, 43*kHeight) style:UITableViewStylePlain];
    [table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table.layer.cornerRadius = 5;
    table.showsVerticalScrollIndicator = NO;
    table.showsHorizontalScrollIndicator = NO;
    table.tag = 1000;
    
    table.delegate = self;
    table.dataSource = self;
    [image addSubview:table];//149 × 196
    [table mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(image.mas_left).with.offset(1.5*kHeight);
        make.top.mas_equalTo(image.mas_top).with.offset(14*kHeight);
        make.size.mas_equalTo(CGSizeMake(69*kWidth, 38*kHeight*4));
    }];
}

-(void)dataBackDiss{
    
    _backView3.hidden = YES;
}

-(void)pressTimeBtn{
    
    
    if (_backView3.hidden) {
        _backView3.hidden = NO;
    }else{
        _backView3.hidden = NO;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag == 1000) {
        static NSString *str=@"str";
        BillTimeCell  * cell =  [tableView dequeueReusableCellWithIdentifier:str];
        if (cell ==nil) {
            cell = [[BillTimeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.title.text = [NSString stringWithFormat:@"%@",BillListArr[indexPath.row][@"monthText"]];
        if (indexPath.row == [BillListArr count]-1) {
            cell.viewline.hidden = YES;
        }
        return cell;

    }else{
        
        static NSString *str=@"string";
        BillSortCell * cell =  [tableView dequeueReusableCellWithIdentifier:str];
        if (cell ==nil) {
            cell = [[BillSortCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.titleLabel.text = sortArr[indexPath.row][@"billTypeName"];
        [cell.leftImage sd_setImageWithURL:[NSURL URLWithString:sortArr[indexPath.row][@"url"]] placeholderImage:[UIImage imageNamed:@"car_style1"]];
        if (sortArr.count -1 == indexPath.row) {
            cell.line.hidden = YES;
        }
        return cell;
    }
}

 
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (tableView.tag == 1000) {
        indexrow = indexPath.row;
        [self createUIdate:BillListArr[indexPath.row]];
        NSString * billID = [NSString stringWithFormat:@"%@",BillListArr[indexPath.row][@"id"]];
        [self createGetPointDate:billID];
        _backView3.hidden = YES;
    }else{
        
        Home_BillVC * bill = [[Home_BillVC alloc]init];
        bill.billType = sortArr[indexPath.row][@"billType"];
        bill.navName = sortArr [indexPath.row][@"billTypeName"];
        
        [self.navigationController pushViewController:bill animated:YES];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return  tableView.tag == 1000 ?40*kHeight :43*kHeight;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return tableView.tag == 1000 ? BillListArr.count : sortArr.count;

}

-(void)createUIAlertController:(NSString*)title
{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self createDate];
        
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/* 解决触摸手势和cell点击的冲突 */
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    if ([touch.view isEqual:_backView3]) {
        return YES;
    }
    return NO;
}


@end
