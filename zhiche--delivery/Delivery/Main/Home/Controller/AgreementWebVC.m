//
//  AgreementWebVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/6/27.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "AgreementWebVC.h"

@interface AgreementWebVC ()
{
    UIImageView * nav;
    
}

@end

@implementation AgreementWebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    nav = [self createNav:@"协议"];
    [self.view addSubview:nav];
    UIWebView * webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 64, Main_Width, Main_height-64)];
    //<2>添加显示数据的网址
    NSString * path = templates_Url;
    //<3>将字符串网址转化成NSURL
    NSURL * url = [NSURL URLWithString:path];
    //<4>将URL封装成NSURLRequest对象
    NSURLRequest * request = [NSURLRequest requestWithURL:url];
    //<5>将请求对象添加在webView视图上
    [webView loadRequest:request];
    //<6>将webView添加到当前视图上
    [self.view addSubview:webView];
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
