//
//  HomeViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "HomeViewController.h"
#import "Common.h"
#import "Header.h"
#import <Masonry.h>
#import "PlaceOrderVC.h"
#import "MineOrderVC.h"
#import "RootViewController.h"
#import "FindQuoteVC.h"
#import "PayMoneyVC.h"
#import "AboutVC.h"
#import <UIImageView+WebCache.h>
#import "ImageWebVC.h"
#import "QueryShippingVC.h"
#import "ReadyPayVC.h"
#import "Home_Bill_HMvc.h"
#import "CityDeliveryVC.h"
#import "LoginViewController.h"
#import "ServiceCenterVC.h"
#import "AboutAppVC.h"
#import "HomeWebVC.h"
#import "WKProgressHUD.h"
#import "testBtnVC.h"
#define TwoBtnWeith Main_Width/2
#define SixBtnWeith Main_Width/6
#define FourBtnHeith 111/2

@interface HomeViewController ()
{
    UIView * nav;
    Common * Com;
    //滚动视图的对象指针
    UIScrollView * ScrollView;
    //分页控件的对象指针
    UIPageControl * pageCtr;
    NSTimer * timer;
    NSMutableDictionary * dataDic;
    NSMutableArray * scrollArr;
}
@property (nonatomic,strong) UIView * backview1;
@property (nonatomic,strong) UILabel * bottomLabel;
@property (nonatomic,strong) UIImageView * LoginImage;
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;

    
    
    
//    self.title = @"首页";
    Com = [[Common alloc]init];
    self.view.backgroundColor = HomeBackColor;
    dataDic = [[NSMutableDictionary alloc]init];
    scrollArr = [[NSMutableArray alloc]init];

    [self creatScroll];
    [self createData];

    [self createUI];
    [self turnOnTimer];
    [self createNav];
    
    _LoginImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, Main_height)];
    _LoginImage.hidden = NO;
//    _LoginImage.backgroundColor = WhiteColor;
    _LoginImage.image = [UIImage imageNamed:@"6"];
    [self.view addSubview:_LoginImage];
    
    
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(100, 100, 300, 300);
    btn.backgroundColor = [UIColor cyanColor];
    [btn setTitle:@"测试" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(pressTestBtn) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:btn];
    
    
}


-(void)pressTestBtn{
    
    
    testBtnVC * test = [[testBtnVC alloc]init];
    
    [self.navigationController pushViewController:test animated:YES];
    
}


-(void)createData{
    
    NSString * string = [NSString stringWithFormat:@"%@?clienttype=30&apptype=20",banner_list_Url];
    [Common requestWithUrlString:string contentType:@"application/json" finished:^(id responseObj) {
        
        int success = [responseObj[@"success"] intValue];
        if (success == 1) {
            scrollArr = responseObj[@"data"];
            [self createScrollImage];
            [self createPageControl];
        }
        
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
}
-(void)createScrollImage{
    
    for (UIImageView * obj in ScrollView.subviews) {
        [obj removeFromSuperview];
    }

    NSMutableArray * imageArr = [[NSMutableArray alloc]init];
    for (int i=0; i<5; i++) {
        
        NSString * imageUrl = nil;
        
        if (i==0) {
  
            imageUrl = [NSString stringWithFormat:@"%@",scrollArr[2][@"picKey"]];
        }
      else  if (i==4) {
            imageUrl = [NSString stringWithFormat:@"%@",scrollArr[0][@"picKey"]];
      }else{
          imageUrl = [NSString stringWithFormat:@"%@",scrollArr[i-1][@"picKey"]];
      }

        [imageArr addObject:imageUrl];
        
    }
    
    for(int i =0;i<imageArr.count;i++)
    {
        //  3 1 2 3 1
        UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(Main_Width * i, 0, Main_Width, 164*kHeight)];
        NSString * string = [NSString stringWithFormat:@"%@",imageArr[i]];
        NSURL * imageUrl = [NSURL URLWithString:string];
        
        if (i == 0) {
            imageView.tag = 2;
        }else if (i == 1){
            imageView.tag = 0;
        }else if (i == 2){
            imageView.tag = 1;
        }else if (i == 3){
            imageView.tag = 2;
        }else{
            imageView.tag = 0;
        }
        
        imageView.userInteractionEnabled = YES;
        [imageView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"shouye_bad"]];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
        [imageView addGestureRecognizer:tap];
        [ScrollView addSubview:imageView];
    }
    
    
}

-(void)turnOnTimer{
    
    timer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(playImage) userInfo:nil repeats:YES];
    
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

-(void)playImage{
    // 3 1 2 3 1
    NSInteger number=ScrollView.contentOffset.x/Main_Width+1;
    // 1 2 3 4 5
    //   2 3 4 5 6
//    NSLog(@"%ld",(long)number);
    if (number== 5) {
        ScrollView.contentOffset = CGPointMake(1 * Main_Width, 0);
        number=1;
        pageCtr.currentPage= 0;
    }


//    [UIView animateWithDuration:0.3 animations:^{
//        
//        ScrollView.contentOffset = CGPointMake(number * Main_Width, 0);
//    } completion:nil];
//    
//    [ScrollView setContentOffset:CGPointMake(number * Main_Width, 0) animated:YES];
    
    [UIView animateWithDuration:0.3 animations:^{

        ScrollView.contentOffset = CGPointMake((number) * Main_Width, 0);
    } completion:nil];

    if (number>1&&number<4) {
        pageCtr.currentPage=number-1;
    }
    if (number == 4) {
        pageCtr.currentPage = 0;
    }

    

    
//    number++;
}

-(void)creatScroll{
    
    ScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, Main_Width, 164*kHeight)];
    ScrollView.contentOffset = CGPointMake(Main_Width, 0);
    ScrollView.contentSize = CGSizeMake(Main_Width * 5, 164*kHeight);
    self.automaticallyAdjustsScrollViewInsets =NO;
    ScrollView.bounces = NO;
    ScrollView.scrollsToTop = YES;
    ScrollView.showsHorizontalScrollIndicator = NO;
    ScrollView.showsVerticalScrollIndicator = NO;
    ScrollView.pagingEnabled = YES;
    ScrollView.delegate = self;

    [self.view addSubview:ScrollView];
//    if (scrollArr.count==0) {
//        UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, 164*kHeight)];
//        imageView.image = [UIImage imageNamed:@"shouye_bad"];
//        [ScrollView addSubview:imageView];
//        ScrollView.contentSize = CGSizeMake(Main_Width * 1, 164*kHeight);
//    }
}

-(void)tapAction:(UITapGestureRecognizer *)tap{
    
    ImageWebVC * imageweb = [[ImageWebVC alloc]init];
    NSString * urlImage = [NSString stringWithFormat:@"%@",scrollArr[tap.view.tag][@"picHref"]];
    if (urlImage.length>0) {
        imageweb.imageURL = urlImage;
        [self.navigationController pushViewController:imageweb animated:YES];
    }
}

-(void)createPageControl{
    
    pageCtr.backgroundColor=[UIColor clearColor];
    pageCtr.currentPage=0;
    pageCtr.numberOfPages = scrollArr.count;
    [pageCtr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(ScrollView.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(50*kWidth, 30*kHeight));
    }];
    [pageCtr addTarget:self action:@selector(pressPageControl:) forControlEvents:UIControlEventValueChanged];
}

-(void)pressPageControl:(id)sender{
    int pageNum =(int)pageCtr.currentPage;
    
    [UIView animateWithDuration:0.3 animations:^{
        ScrollView.contentOffset = CGPointMake(pageNum * Main_Width, 0);

    } completion:nil];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    CGPoint point = scrollView.contentOffset;

    pageCtr.currentPage = point.x / Main_Width;
    
//    NSLog(@"%f",point.x);
    // 3 1 2 3 1
    if(point.x == 0)
    {
        scrollView.contentOffset = CGPointMake(3 * Main_Width, 0);
    }
    else if (point.x == 4 * Main_Width)
    {
        scrollView.contentOffset = CGPointMake(1 * Main_Width, 0);
    }
 
}

-(void)createNav{
    nav = [self createNavView];
//    [self createNavTitle:@"首页"];
    [self createNavTitle:@"慧运车"];

    [self.view addSubview:nav];
}

-(void)pressRightBtn{
    
}

-(void)createUI{
    
    _backview1 = [[UIView alloc]init];
    _backview1.backgroundColor = WhiteColor;
    [self.view addSubview:_backview1];
    [_backview1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(ScrollView.mas_bottom).with.offset(32*kHeight);
        make.bottom.mas_equalTo(self.view.mas_bottom).with.offset(-82*kHeight);
        make.width.mas_equalTo(Main_Width);
    }];
    
    
    /*
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.tag = 1;
    [leftBtn setExclusiveTouch:YES];

    [leftBtn setBackgroundImage:[UIImage imageNamed:@"leftimage"] forState:UIControlStateNormal];
    
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"leftimageSelected"] forState:UIControlStateSelected];

    
    [leftBtn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [_backview1 addSubview:leftBtn];
    [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview1.mas_left).with.offset(4.5*kWidth);
        make.top.mas_equalTo(_backview1.mas_top).with.offset(5*kHeight);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-53 * kHeight);
        make.width.mas_equalTo(156 * kWidth);
    }];

    UIButton * rightBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn1.tag = 2;
    [rightBtn1 setExclusiveTouch:YES];

    [rightBtn1 setBackgroundImage:[UIImage imageNamed:@"rightimage1"] forState:UIControlStateNormal];
    
    [rightBtn1 setBackgroundImage:[UIImage imageNamed:@"rightimage1Selected"] forState:UIControlStateSelected];
    [rightBtn1 addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [_backview1 addSubview:rightBtn1];
    [rightBtn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_backview1.mas_right).with.offset(-4.5*kWidth);
        make.top.mas_equalTo(_backview1.mas_top).with.offset(5*kHeight);
        make.width.mas_equalTo(150 * kHeight);
    }];
    UIButton * rightBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn2.tag = 3;
    [rightBtn2 setExclusiveTouch:YES];

    
    [rightBtn2 setBackgroundImage:[UIImage imageNamed:@"rightimage2"] forState:UIControlStateNormal];
    
    [rightBtn2 setBackgroundImage:[UIImage imageNamed:@"rightimage2Selected"] forState:UIControlStateSelected];
    
    
    [rightBtn2 addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [_backview1 addSubview:rightBtn2];
    [rightBtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_backview1.mas_right).with.offset(-4.5*kWidth);
        make.top.mas_equalTo(rightBtn1.mas_bottom).with.offset(5*kHeight);
        make.size.equalTo(rightBtn1);
        make.bottom.mas_equalTo(leftBtn.mas_bottom);
    }];
    
*/
    pageCtr=[[UIPageControl alloc]init];
    [self.view addSubview:pageCtr];
    
    
    
    
    UIView * viewFirst = [[UIView alloc]init];
    viewFirst.backgroundColor = GrayColor;
    [_backview1 addSubview:viewFirst];
    [viewFirst mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left).with.offset(Main_Width/3);
        make.top.equalTo(_backview1.mas_top);
//        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        make.bottom.mas_equalTo(_backview1.mas_bottom);
        make.width.mas_equalTo(0.5);
    }];
    
    
    UIView * viewLine1 = [[UIView alloc]init];
    viewLine1.backgroundColor = GrayColor;
    [_backview1 addSubview:viewLine1];
    [viewLine1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left).with.offset((Main_Width/3)*2);
        make.top.equalTo(_backview1.mas_top);
        make.bottom.mas_equalTo(_backview1.mas_bottom);
        make.width.mas_equalTo(0.5);
    }];
    
    UIView * viewLine2 = [[UIView alloc]init];
    viewLine2.backgroundColor = GrayColor;
    [_backview1 addSubview:viewLine2];
    [viewLine2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_backview1.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
    NSArray * arrName = @[@"同城配送",
                          @"城际运输",
                          @"商户账单",
                          @"城际运价",
                          @"客户中心",
                          @"关于慧运车"].mutableCopy;
    for (int i=0; i<6; i++) {
        
        UIImageView * image = [[UIImageView alloc]init];
        NSString * imageName = [NSString stringWithFormat:@"home_image%d",i];
        image.image = [UIImage imageNamed:imageName];
        image.tag = 100+i;
        [_backview1 addSubview:image];
        UILabel * label = [Com createUIlabel:arrName[i] andFont:FontOfSize14 andColor:BlackColor];
        [_backview1 addSubview:label];
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = i+1;
        [btn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn addTarget:self action:@selector(buttonClickImage:) forControlEvents:UIControlEventTouchDown];
        [_backview1 addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(image);
            make.size.mas_equalTo(CGSizeMake(SixBtnWeith*2,FourBtnHeith*2));
        }];
        
        if (i<3) {
            
        [image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_backview1.mas_left).with.offset(SixBtnWeith*(i*2+1));
            make.centerY.equalTo(_backview1.mas_top).with.offset(FourBtnHeith*kHeight);
            make.size.mas_equalTo(CGSizeMake(40*kWidth, 25*kHeight));
        }];
        }else{
            int j = i-3;
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_backview1.mas_left).with.offset(SixBtnWeith*(j*2+1));
                make.centerY.equalTo(_backview1.mas_top).with.offset(3*FourBtnHeith*kHeight);
                make.size.mas_equalTo(CGSizeMake(40*kWidth, 25*kHeight));
            }];

        }
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(image.mas_centerX);
            make.top.mas_equalTo(image.mas_bottom).with.offset(15*kHeight);
        }];

        
        
        /*
        if (i==0) {
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_backview1.mas_left).with.offset(Main_Width/4);
                make.top.equalTo(_backview1.mas_top).with.offset(40*kHeight);
                make.size.mas_equalTo(CGSizeMake(47*kWidth, 38*kHeight));
            }];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(image.mas_centerX);
                make.top.mas_equalTo(image.mas_bottom).with.offset(15*kHeight);
            }];
        }else if (i==1){
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_backview1.mas_left).with.offset(Main_Width/4+Main_Width/2);
                make.top.equalTo(_backview1.mas_top).with.offset(40*kHeight);
                make.size.mas_equalTo(CGSizeMake(52*kWidth, 40*kHeight));
            }];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(image.mas_centerX);
                make.top.equalTo(image.mas_bottom).with.offset(17*kHeight);
            }];
        }else if (i==2){
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_backview1.mas_left).with.offset(Main_Width/4);
                make.top.equalTo(_backview1.mas_top).with.offset(185*kHeight);
                make.size.mas_equalTo(CGSizeMake(35*kWidth, 40.5*kHeight));
            }];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(image.mas_centerX);
                make.top.equalTo(image.mas_bottom).with.offset(16*kHeight);
            }];
        }else{
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_backview1.mas_left).with.offset(Main_Width/4+Main_Width/2);
                make.top.equalTo(_backview1.mas_top).with.offset(185*kHeight);
                make.size.mas_equalTo(CGSizeMake(34*kWidth, 41.5*kHeight));
            }];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(image.mas_centerX);
                make.top.equalTo(image.mas_bottom).with.offset(15*kHeight);
            }];
            
        }
         */
    }

    

/*
    UIView * viewFirst = [[UIView alloc]init];
    viewFirst.backgroundColor = GrayColor;
    [_backview1 addSubview:viewFirst];
    [viewFirst mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left);
        make.top.equalTo(_backview1.mas_top);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    
    UIView * viewLine1 = [[UIView alloc]init];
    viewLine1.backgroundColor = GrayColor;
    [_backview1 addSubview:viewLine1];
    [viewLine1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left);
        make.centerY.equalTo(_backview1.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    UIView * viewLine2 = [[UIView alloc]init];
    viewLine2.backgroundColor = GrayColor;
    [_backview1 addSubview:viewLine2];
    [viewLine2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_backview1.mas_centerX);
        make.top.equalTo(_backview1.mas_top);
        make.bottom.mas_equalTo(_backview1.mas_bottom);
        make.width.mas_equalTo(0.5);
    }];
    
    NSArray * arrName = @[@"同城配送",@"城际运输",@"城际运价",@"我的账单"].mutableCopy;
    for (int i=0; i<4; i++) {
        
        UIImageView * image = [[UIImageView alloc]init];
        NSString * imageName = [NSString stringWithFormat:@"home_image%d",i];
        image.image = [UIImage imageNamed:imageName];
        image.tag = 100+i;
        [_backview1 addSubview:image];
        UILabel * label = [Com createUIlabel:arrName[i] andFont:FontOfSize14 andColor:BlackColor];
        [_backview1 addSubview:label];
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = i+1;
        [btn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn addTarget:self action:@selector(buttonClickImage:) forControlEvents:UIControlEventTouchDown];
        [_backview1 addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(image);
            make.size.mas_equalTo(CGSizeMake(150*kWidth, 130*kHeight));
        }];
        if (i==0) {
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_backview1.mas_left).with.offset(Main_Width/4);
                make.top.equalTo(_backview1.mas_top).with.offset(40*kHeight);
                make.size.mas_equalTo(CGSizeMake(47*kWidth, 38*kHeight));
            }];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(image.mas_centerX);
                make.top.mas_equalTo(image.mas_bottom).with.offset(15*kHeight);
            }];
        }else if (i==1){
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_backview1.mas_left).with.offset(Main_Width/4+Main_Width/2);
                make.top.equalTo(_backview1.mas_top).with.offset(40*kHeight);
                make.size.mas_equalTo(CGSizeMake(52*kWidth, 40*kHeight));
            }];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(image.mas_centerX);
                make.top.equalTo(image.mas_bottom).with.offset(17*kHeight);
            }];
        }else if (i==2){
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_backview1.mas_left).with.offset(Main_Width/4);
                make.top.equalTo(_backview1.mas_top).with.offset(185*kHeight);
                make.size.mas_equalTo(CGSizeMake(35*kWidth, 40.5*kHeight));
            }];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(image.mas_centerX);
                make.top.equalTo(image.mas_bottom).with.offset(16*kHeight);
            }];
        }else{
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_backview1.mas_left).with.offset(Main_Width/4+Main_Width/2);
                make.top.equalTo(_backview1.mas_top).with.offset(185*kHeight);
                make.size.mas_equalTo(CGSizeMake(34*kWidth, 41.5*kHeight));
            }];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(image.mas_centerX);
                make.top.equalTo(image.mas_bottom).with.offset(15*kHeight);
            }];
            
        }
    }
    
 */
   /*
    UIButton * btn1 = [self createUIbutton:@"home_option_car" andTag:1];
    [btn1 addTarget:self action:@selector(pressBtn:) forControlEvents:UIControlEventTouchUpInside];
    [btn1 setImage:[UIImage imageNamed:@"home_option_car_pre"] forState:UIControlStateHighlighted];

    [_backview1 addSubview:btn1];
    [btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left).with.offset(8*kWidth);
        make.top.equalTo(ScrollView.mas_bottom).with.offset(8*kHeight);
        make.size.mas_equalTo(CGSizeMake(148*kWidth, 162*kHeight));
    }];
    UILabel * label1 = [Com createUIlabel:@"我要托运" andFont:16.0 andColor:fontGrayColor];
    [btn1 addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(btn1.mas_centerX);
        make.bottom.equalTo(btn1.mas_bottom).with.offset(-29*kHeight);
    }];

    
    UIButton * btn2 = [self createUIbutton:@"home_option_bill" andTag:2];
    [btn2 addTarget:self action:@selector(pressBtn:) forControlEvents:UIControlEventTouchUpInside];
    [btn2 setImage:[UIImage imageNamed:@"home_option_bill_pre"] forState:UIControlStateHighlighted];
    [_backview1 addSubview:btn2];
    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backview1.mas_right).with.offset(-8*kWidth);
        make.top.equalTo(ScrollView.mas_bottom).with.offset(8*kHeight);
        make.size.mas_equalTo(CGSizeMake(148*kWidth, 162*kHeight));
    }];
    UILabel * label2 = [Com createUIlabel:@"查看发单" andFont:16.0 andColor:fontGrayColor];
    [btn2 addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(btn2.mas_centerX);
        make.bottom.equalTo(btn2.mas_bottom).with.offset(-29*kHeight);
    }];


    UIButton * btn3 = [self createUIbutton:@"home_option_list" andTag:3];
    [btn3 addTarget:self action:@selector(pressBtn:) forControlEvents:UIControlEventTouchUpInside];
    [btn3 setImage:[UIImage imageNamed:@"home_option_list_pre"] forState:UIControlStateHighlighted];

    [_backview1 addSubview:btn3];
    [btn3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left).with.offset(8*kWidth);
        make.top.equalTo(btn1.mas_bottom).with.offset(8*kHeight);
        make.size.mas_equalTo(CGSizeMake(148*kWidth, 162*kHeight));
    }];
    UILabel * label3 = [Com createUIlabel:@"我的账单" andFont:16.0 andColor:fontGrayColor];
    [btn3 addSubview:label3];
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(btn3.mas_centerX);
        make.bottom.equalTo(btn3.mas_bottom).with.offset(-29*kHeight);
    }];

    
    UIButton * btn4 = [self createUIbutton:@"home_option_about" andTag:4];
    [btn4 addTarget:self action:@selector(pressBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    [btn4 setImage:[UIImage imageNamed:@"home_option_about_pre"] forState:UIControlStateHighlighted];
    [_backview1 addSubview:btn4];
    [btn4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backview1.mas_right).with.offset(-8*kWidth);
        make.top.equalTo(btn2.mas_bottom).with.offset(8*kHeight);
        make.size.mas_equalTo(CGSizeMake(148*kWidth, 162*kHeight));
    }];
    UILabel * label4 = [Com createUIlabel:@"关于慧运车" andFont:16.0 andColor:fontGrayColor];
    [btn4 addSubview:label4];
    [label4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(btn4.mas_centerX);
        make.bottom.equalTo(btn4.mas_bottom).with.offset(-29*kHeight);
    }];
    */

}

-(void)buttonClickImage:(UIButton*)sender{
    
    NSInteger tag = sender.tag;
    UIImageView * image = (UIImageView*)[self.view viewWithTag:tag+99];
    image.image = [UIImage imageNamed:[NSString stringWithFormat:@"shouye%ld",(long)tag]];
}



-(void)buttonClick:(UIButton*)sender{
    

    switch (sender.tag) {
        case 5:{

            //客户中心
            ServiceCenterVC * centervc = [[ServiceCenterVC alloc]init];
            [Common isLogin:self andPush:centervc];
        }
            break;

        case 6:{
            
            //关于慧运车
            AboutAppVC * appvc = [[AboutAppVC alloc]init];
            [Common isLogin:self andPush:appvc];
        }
            break;

        case 2:{
            PlaceOrderVC * placeOrder = [[PlaceOrderVC alloc]init];
            [Common isLogin:self andPush:placeOrder];
        }
            break;
//        case 2:{
//            //待付款
////            FindQuoteVC * quote = [[FindQuoteVC alloc]init];
////            [Common isLogin:self andPush:quote];
//            //待支付
//            ReadyPayVC *payVC = [[ReadyPayVC alloc]init];
//            [Common isLogin:self andPush:payVC];
//        }
//            break;
        case 4:{
        
            QueryShippingVC * shipping = [[QueryShippingVC alloc]init];
            [Common isLogin:self andPush:shipping];

        }
            break;
        case 3:{
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

            NSString * isBaseUser = [NSString stringWithFormat:@"%@",[userDefaults objectForKey:@"isBaseUser"]];
            
            if (![isBaseUser isEqualToString:@"yes"]) {
                [WKProgressHUD popMessage:@"您的账户等级无法查单商户账单" inView:self.view duration:1.5 animated:YES];
                
            }else{
                Home_Bill_HMvc * bill = [[Home_Bill_HMvc alloc]init];
                [Common isLogin:self andPush:bill];
            }
        }
            break;

        case 1:{
            
            CityDeliveryVC * bill = [[CityDeliveryVC alloc]init];
            [Common isLogin:self andPush:bill];
        }
            break;
        default:
            break;
    }
}

-(void)viewWillAppear:(BOOL)animated{

    
    

    [self.navigationController setNavigationBarHidden:YES];
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    for (int i=0; i<6; i++) {
        UIImageView * image = (UIImageView*)[self.view viewWithTag:100+i];
        image.image = [UIImage imageNamed:[NSString stringWithFormat:@"home_image%d",i]];
    }
    
    _LoginImage.hidden = YES;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *string = [userDefaults objectForKey:@"login"];
    
    if (string == nil ) {
        
        [rootVC setTabBarHidden:YES];
        _LoginImage.hidden = NO;
        
        //延迟执行
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            LoginViewController *loginVC = [[LoginViewController alloc]init];
            UINavigationController * loginNC = [[UINavigationController alloc]initWithRootViewController:loginVC];
            [self presentViewController:loginNC animated:YES completion:nil];
        });
    }else{
        [rootVC setTabBarHidden:NO];
    }
    
    
    NSString * strVersion= [NSString stringWithFormat:@"%@",versionURL];
    [Common requestWithUrlString:strVersion contentType:@"application/json" finished:^(id responseObj) {
        
        int success = [responseObj[@"success"] intValue];
        if (success == 1) {
            
            NSString * url = responseObj[@"data"][@"url"];
            NSString *version_url = [responseObj[@"data"][@"version"] stringByReplacingOccurrencesOfString:@"." withString:@""];
            
            NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
            NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
            NSString * stringVS = [app_Version stringByReplacingOccurrencesOfString:@"." withString:@""];
            
            int old = [stringVS intValue];
            int new = [version_url intValue];
            if (new>old) {
                [self createUIAlertController:responseObj[@"data"][@"releaseNote"] andUrl:url];
            }
        }
        
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
    
}


-(void)createUIAlertController:(NSString*)title andUrl:(NSString *)url
{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"版本更新" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"立即更新" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {


        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/cn/app/id1136973219?mt=8"]];
        
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}


-(UIButton*)createUIbutton:(NSString*)imageName andTag:(int)tag {
    
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = tag;
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    return btn;
}

-(void)createUIAlertController:(NSString*)title
{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"客服中心" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"拨打" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        dispatch_after(1, dispatch_get_main_queue(), ^{
            [self creatIhone];
        });
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)creatIhone{
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iphoneNumber]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
