//
//  AddCarTypeVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/13.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface AddCarTypeVC : NavViewController<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (nonatomic,copy) void (^callBack)(NSMutableArray * CarArr);


@property (nonatomic,strong) NSString * CityListName;//判断是否是同城

@end
