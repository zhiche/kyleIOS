//
//  NullDataView.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/26.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "NullDataView.h"

@implementation NullDataView

-(id)initWithFrame:(CGRect)frame andTitle:(NSString *)string andImageName:(NSString *)imageName{
    
    
    if (self = [super init]) {
        
        self.frame = frame;
        
        self.backgroundColor = WhiteColor;
        
        UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMidX(self.frame) - 28, 95 , 56, 53)];
        imageV.image = [UIImage imageNamed:imageName];
        [self addSubview:imageV];
        
        self.label = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(imageV.frame) + 14, screenWidth, 20)];
        self.label.text = string;
        self.label.textColor = BtnTitleColor;
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.font = Font(13);
        [self addSubview:self.label];
        
    }
    
    return self;
    
    
}

@end
