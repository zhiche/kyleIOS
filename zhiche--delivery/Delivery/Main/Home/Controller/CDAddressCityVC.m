//
//  CDAddressCityVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 17/5/23.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import "CDAddressCityVC.h"
#import <Masonry.h>
#import "Header.h"
#import "Common.h"

@interface CDAddressCityVC ()
{
    Common * com;
    UIImageView * nav;

    NSMutableArray * arrData;
    NSMutableArray * arrSection;
    UITableView * tableview;
}
@end

@implementation CDAddressCityVC

- (void)viewDidLoad {
    [super viewDidLoad];

    com = [[Common alloc]init];
    nav = [self createNav:@"选择城市"];
    [self.view addSubview:nav];
    
    arrData = [[NSMutableArray alloc]init];
    arrSection = [[NSMutableArray alloc]init];
    
    
    tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 64*kHeight, Main_Width, 86*kHeight) style:UITableViewStylePlain];
//    [tableview setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableview.delegate = self;
    tableview.dataSource = self;
    tableview.sectionIndexBackgroundColor= sectionHeadView;

    [self.view addSubview:tableview];
    [tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.mas_equalTo(nav.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(Main_Width,Main_height-64));
    }];

    
    
    [self initdata];
    
    
}


-(void)initdata{
    
    
    
    NSString * string = [NSString stringWithFormat:@"%@",CD_addressListArea_Url];
    [Common requestWithUrlString:string contentType:@"application/json" finished:^(id responseObj) {
        
        int success = [responseObj[@"success"] intValue];
        if (success == 1) {
            
            if ([responseObj[@"data"] count] > 0) {
                
                NSMutableArray * leftArr = [[NSMutableArray alloc]init];
                
                for (int i=0; i<[responseObj[@"data"] count]; i++) {
                    [leftArr addObject:responseObj[@"data"][i][@"initials"]];
                }
                
                NSArray * array = [NSArray arrayWithArray:leftArr];
                
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                
                for (NSNumber *number in array) {
                    [dict setObject:number forKey:number];
                }
                
                array = [dict allKeys];
                NSArray * array1 = [[NSArray alloc]init];
                array1 = [array sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                    
                    return [obj1 compare:obj2 options:NSNumericSearch];
                }];
                
                arrSection = (NSMutableArray*)array1;
                //section 字母排序
                NSMutableArray * arrZiMu = (NSMutableArray*)array1;
                
                for (int j=0; j<arrZiMu.count; j++) {
                    
                    NSMutableArray * arr = [[NSMutableArray alloc]init];
                    
                    for (int i=0; i<[responseObj[@"data"] count]; i++) {
                        
                        NSString * new = responseObj[@"data"][i][@"initials"];
                        
                        if ([new isEqualToString:arrZiMu[j]]) {
                            [arr addObject:responseObj[@"data"][i]];
                        }
                    }
                    [arrData addObject:arr];
                }
                [tableview reloadData];
            
            }
        }
            
                
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
            
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *str=@"str";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.textLabel.text = arrData[indexPath.section][indexPath.row][@"name"];
    
    
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return arrData.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return  [arrData[section] count];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 43*kHeight;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (self.callAddressCity) {
        self.callAddressCity(arrData[indexPath.section][indexPath.row][@"name"]);
    }
    [self.navigationController popViewControllerAnimated:YES];
    

    
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    if (arrSection.count>0) {
        UIView *view = [[UIView alloc]init];
        view.backgroundColor = sectionHeadView;
        view.frame = CGRectMake(0, 0, Main_Width, 35*kHeight);
        
        UILabel * label = [com createUIlabel:arrSection[section] andFont:FontOfSize14 andColor:BlackColor];
        [view addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(view.mas_left).with.offset(18*kWidth);
            make.centerY.equalTo(view.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(Main_Width/2, 35*kHeight));
        }];
        
        return view;
    }else{
        return nil;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    
    
    return 35*kHeight;
    
}


//表格的自动搜索功能
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    
    if (tableView == tableview) {
        NSMutableArray * arr = [[NSMutableArray alloc]init];
        //添加所有分区的名称
        //通过分区名称进行模糊查询
        for (int i = 0; i<arrSection.count; i++) {
            [arr addObject:[NSString stringWithFormat:@"%@",arrSection[i]]];
        }
        return arr;
    }
    return nil;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
