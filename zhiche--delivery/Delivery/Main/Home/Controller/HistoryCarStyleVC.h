//
//  HistoryCarStyleVC.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/6/13.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryCarStyleVC : UIViewController<UITextFieldDelegate>
/*
 str  车系＋车型
 str1 类别（SUV 或者商务）
 str2 长宽高
 str3 车系logo
 
 */
@property (nonatomic,copy) void (^callBack)(NSString *str,NSString *str1,NSString *str2,NSString *str3,NSString *str4);
@end
