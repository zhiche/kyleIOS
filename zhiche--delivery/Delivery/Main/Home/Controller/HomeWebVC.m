//
//  HomeWebVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/11/25.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "HomeWebVC.h"
#import <WebKit/WebKit.h>
#import "Common.h"
@interface HomeWebVC ()<WKUIDelegate,WKNavigationDelegate>
{
    UIImageView * nav;
    Common * com;
    
}

@end

@implementation HomeWebVC

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.webUrl = [[NSString alloc]init];
        self.titleLabel = [[NSString alloc]init];
    }
    return self;
}




- (void)viewDidLoad {
    [super viewDidLoad];

    nav = [self createNav:self.titleLabel];
    [self.view addSubview:nav];

    
    [self initWebViews];


}


-(void)initWebViews
{
    
    NSString *jScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
    
    WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    WKUserContentController *wkUController = [[WKUserContentController alloc] init];
    [wkUController addUserScript:wkUScript];
    
    WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
    wkWebConfig.userContentController = wkUController;
    
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64) configuration:wkWebConfig];
    
    NSURL  *url = [NSURL URLWithString:self.webUrl];
    
    //    UIWebView *webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64)];
    //隐藏数字下面的横线
    //    webView.dataDetectorTypes = UIDataDetectorTypeNone;
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    webView.navigationDelegate = self;
    webView.backgroundColor = [UIColor whiteColor];
    
    [webView loadRequest:request];
    
    [self.view addSubview:webView];
    

}

-(WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures

{
    
    NSLog(@"createWebViewWithConfiguration");
    
    if (!navigationAction.targetFrame.isMainFrame) {
        
        [webView loadRequest:navigationAction.request];
        
    }
    
    return nil;
    
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {

    
    
    NSString *jsToGetHTMLSource = @"document.getElementsByTagName('html')[0].innerHTML";//获取整个页面的HTMLstring
    [webView evaluateJavaScript:jsToGetHTMLSource completionHandler:^(id _Nullable HTMLsource, NSError * _Nullable error) {

        NSLog(@"%@",HTMLsource);
    }];
}

-(void)webView:(WKWebView* )webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    decisionHandler(WKNavigationActionPolicyAllow);
    
}



-(void)webView:(WKWebView* )webView didStartProvisionalNavigation:(WKNavigation* )navigation
{
    
    
    
    NSString *path= [webView.URL absoluteString];
    NSString * newPath = [path lowercaseString];
    
    if ([newPath hasPrefix:@"sms:"] || [newPath hasPrefix:@"tel:"]) {
        
        UIApplication * app = [UIApplication sharedApplication];
        if ([app canOpenURL:[NSURL URLWithString:newPath]]) {
            [app openURL:[NSURL URLWithString:newPath]];
        }
        return;
}}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
