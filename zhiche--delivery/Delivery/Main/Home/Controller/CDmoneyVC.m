//
//  CDmoneyVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/11/15.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "CDmoneyVC.h"
#import <Masonry.h>
#import "Common.h"
#import "FeeCell.h"

@interface CDmoneyVC ()
{
    UIImageView * nav;
    UITableView * table;
    Common * com;
    
}

@end

@implementation CDmoneyVC

- (instancetype)init

{
    self = [super init];
    if (self) {
        self.CarMoneyDic = [[NSMutableDictionary alloc]init];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    nav = [self createNav:@"费用详情"];
    [self.view addSubview:nav];
    self.view.backgroundColor = WhiteColor;
    com = [[Common alloc]init];
    
    
    [self createUI];
    
}

-(void)createUI{
    
    
    
    UIView * addressView = [[UIView alloc]init];
    addressView.backgroundColor = headerBackViewColor;
    [self.view addSubview:addressView];
    [addressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.mas_equalTo(nav.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
    }];
    
    UIView * Timeline1 = [[UIView alloc]init];
    Timeline1.backgroundColor = LineGrayColor;
    [addressView addSubview:Timeline1];
    [Timeline1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(addressView.mas_bottom);
        make.left.mas_equalTo(addressView.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    
    
    UILabel * address = [com createUIlabel:@"订单费用详情：" andFont:FontOfSize14 andColor:BlackTitleColor];
    [addressView addSubview:address];
    [address mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(addressView.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(addressView.mas_top).with.offset(21.5*kHeight);
    }];
    
    NSString * distanceStr = [NSString stringWithFormat:@"订单里程：%@",_CarMoneyDic[@"distance"]];
    UILabel * distance = [com createUIlabel:distanceStr andFont:FontOfSize14 andColor:YellowColor];
    [addressView addSubview:distance];
    [distance mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(addressView.mas_right).with.offset(-18*kWidth);
        make.centerY.mas_equalTo(addressView.mas_top).with.offset(21.5*kHeight);
    }];

    
    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 64*kHeight, Main_Width, 86*kHeight) style:UITableViewStylePlain];
    [table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table.userInteractionEnabled = NO;
    table.delegate = self;
    table.dataSource = self;
    [table setTableFooterView:[UIView new]];
    [self.view addSubview:table];
    
    [table mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).with.offset(0);
        make.top.mas_equalTo(addressView.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight*4));
    }];
    
    
    NSString * moneyStr = [NSString stringWithFormat:@"费用合计：￥%.2f",[self.CarMoneyDic[@"actualTotal"] floatValue]];
    UILabel * totalMoney = [com createUIlabel:moneyStr andFont:FontOfSize14 andColor:YellowColor];
    [self.view addSubview:totalMoney];
    [totalMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).with.offset(-18*kWidth);
        make.centerY.mas_equalTo(table.mas_bottom).with.offset(21.5*kHeight);
    }];


}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *str=@"str";
    FeeCell  * cell =  [tableView dequeueReusableCellWithIdentifier:str];
    if (cell ==nil) {
        cell = [[FeeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.nameLabel.text = self.CarMoneyDic[@"fees"][indexPath.row][@"feeName"];
    
    NSString * fee = [NSString stringWithFormat:@"￥%.2f",[self.CarMoneyDic[@"fees"][indexPath.row][@"actualAmount"] floatValue]];
    cell.feeLabel.text = fee;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 43*kHeight;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
//    return dataSouceArr.count;
        return [self.CarMoneyDic[@"fees"] count];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return 1;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
