//
//  KeepAddressVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/18.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface KeepAddressVC : NavViewController  <UITextFieldDelegate,UIActionSheetDelegate,UIAlertViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIScrollViewDelegate>

@property (nonatomic,strong) NSString * addresstype;  //0:发货地址，1:收货地址
@property (nonatomic,strong) NSString * navtitle; //标题  0 1 2
@property (nonatomic,strong) NSMutableDictionary * dataSouce;
@property (nonatomic,copy) void (^callBack)(NSMutableDictionary * AddressDic,NSString * addresstype); //地址类型(0:发车地址，1:送达地址)



@end

