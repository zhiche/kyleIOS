//
//  Home_BillVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/10/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "Home_BillVC.h"
#import "Common.h"
#import <Masonry.h>
#import "SevenSwitch.h"
#import "RootViewController.h"
#import "BillTimeCell.h"
#import "NullDataView.h"
#import "Bill_HM_VC.h"
@interface Home_BillVC ()
{
    UIImageView * nav;
    Common * com;
    RootViewController * TabBar;
    UITableView * table;
    SevenSwitch *mySwitch;
    NSMutableArray * BillListArr;
    NSMutableDictionary * BillDetailsDic;
    UIView * SwitchView;
    NSInteger indexrow;
    NSString * isIntegral;
    UIButton * ApplyBtn;
    NullDataView * nullView;
    UIButton * BillDetaile;

}

@property (nonatomic,strong) UIView * backView1;
@property (nonatomic,strong) UIView * backView2;
@property (nonatomic,strong) UIView * backView3;
@property (nonatomic,strong) UILabel * dataLabel;//日期
@property (nonatomic,strong) UILabel * orderNumberLabel;//订单数
@property (nonatomic,strong) UILabel * CarNumberLabel;  //发运车辆数
@property (nonatomic,strong) UILabel * MoneyLabel;//账单金额
@property (nonatomic,strong) UILabel * BalanceLabel;//结算金额
@property (nonatomic,strong) UILabel * pointLabel;//积分


@end

@implementation Home_BillVC


- (instancetype)init

{
    self = [super init];
    if (self) {
        self.billType = [[NSString alloc]init];
        self.navName = [[NSString alloc]init];
        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GrayColor;
    
    nav = [self createNav:self.navName];
    
    [self.view addSubview:nav];
    self.ShareDic = [[NSMutableDictionary alloc]init];

    TabBar = [RootViewController defaultsTabBar];
    SwitchView = [[UIView alloc]init];
    
    com = [[Common alloc]init];
    BillListArr = [[NSMutableArray alloc]init];
    BillDetailsDic = [[NSMutableDictionary alloc]init];
    indexrow = 0;
    isIntegral = @"0";
    [self createBackView1];
    [self createBackView2];
    [self createBtn];
    [self createTable];
    [self createDate];
    
//    nullView = [[NullDataView  alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64) andTitle:@"暂无账单" andImageName:@"形状"];
//    nullView.backgroundColor = GrayColor;
//    


}
-(void)viewWillAppear:(BOOL)animated{
    [TabBar setTabBarHidden:YES];

}


-(void)createDate{
    
    NSString * url = [NSString stringWithFormat:@"%@?billType=%@",Find_BillList_Url,self.billType];
    
    
    
    [Common requestWithUrlString:url contentType:@"application/json" finished:^(id responseObj){
        
        if ([responseObj[@"data"] count]>0) {
            BillDetaile.userInteractionEnabled = YES;
            mySwitch.userInteractionEnabled = YES;
//            [nullView removeFromSuperview];
            BillListArr = responseObj[@"data"];
            [self createUIdate:BillListArr[0]];
            
            [self createGetPointDate:[NSString stringWithFormat:@"%@",BillListArr[0][@"id"]]];
            
            if (BillListArr.count <4) {
                
                UIImageView * image = (UIImageView*)[_backView3 viewWithTag:1111];
                [image mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.size.mas_equalTo(CGSizeMake(74.5*kWidth, 44*kHeight*BillListArr.count));
                }];
                [table mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.size.mas_equalTo(CGSizeMake(70.5*kWidth, 40*kHeight*BillListArr.count));
                }];
            }
            [table reloadData];
        }else{
            
            [self createGetPointDate:[NSString stringWithFormat:@"0"]];
            BillDetaile.userInteractionEnabled = NO;
            ApplyBtn.backgroundColor = LineGrayColor;
            [ApplyBtn setTitleColor:carScrollColor forState:UIControlStateNormal];
            ApplyBtn.userInteractionEnabled = NO;
            ApplyBtn.layer.borderWidth = 0;
            mySwitch.userInteractionEnabled = NO;
            
        }
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];

}

-(void)createUIdate:(NSMutableDictionary*)dic{
    
    //        "id": 6320,
    //        "userId": 82,
    //        "userName": "纳尼",
    //        "userType": 40,
    //        "amount": 63067.00,
    //        "beginBalance": null,
    //        "status": 10,
    //        "beginDate": "2016-09-25 00:00:00",
    //        "monthText": "2016年10月",
    //        "endDate": "2016-10-24 23:59:59",
    //        "createTime": "2016-10-17 16:01:35",
    //        "updateTime": "2016-10-17 16:01:35",
    //        "orderAmount": 44,
    //        "carAmount": 2
    
    _MoneyLabel.text = [NSString stringWithFormat:@"%.2f",[dic[@"amount"] floatValue]];
    _orderNumberLabel.text = [NSString stringWithFormat:@"%@单",dic[@"orderAmount"]];
    _CarNumberLabel.text = [NSString stringWithFormat:@"%@辆",dic[@"carAmount"]];
    _dataLabel.text = [NSString stringWithFormat:@"%@",dic[@"monthText"]];
}


-(void)createGetPointDate:(NSString*)BillId{
    

    NSString * url = [NSString stringWithFormat:@"%@?pageNo=1&pageSize=10&billId=%@&billType=%@",Find_BillDetails_Url,BillId,self.billType];
    [com afPostRequestWithUrlString:url parms:nil finishedBlock:^(id responseObj) {
        NSMutableDictionary * dicObj =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];
        
        if ([dicObj[@"success"] boolValue]) {
            
            if (dicObj[@"data"][@"billFiles"]) {
                
                self.ShareDic = dicObj[@"data"][@"billFiles"];
            }
            
            BillDetailsDic = dicObj[@"data"];
            mySwitch.on = NO;
            isIntegral = @"0";
            
//            scoresVo =     {
//                actualPaid = 80469;
//                amount = 0;
//                billStatusShow = 0;
//                billstatus = 20;
//                isUserScores = 0;
//                scores = 0;
//                scoresPaid = 0;
//                scoresText = "<null>";
//            };

            
            if ([BillDetailsDic[@"scoresVo"][@"actualPaid"] isEqual:[NSNull null]]) {
                
                 _BalanceLabel.text = @"0.00";
            } else {
                _BalanceLabel.text = [NSString stringWithFormat:@"￥%.2f",[BillDetailsDic[@"scoresVo"][@"actualPaid"] floatValue]];
            }
            
            
            
            [self createPointDate];
        }

    } failedBlock:^(NSString *errorMsg) {
    }];
}


-(void)createPointDate{
//    "scoresVo": {
//        "scoresText": "积分共1000.00积分，本次使用1000.00积分抵扣¥20.00",
//        "scores": 1000.00,
//        "amount": 20.00,
//        "isUserScores": 0, // 是否显示使用积分 0: 显示 1:不显示
//        "scoresPaid": null,
//        "actualPaid": null,
//        "billStatusShow": 1  // 是否显示申请结算按钮 1:显示 0:不显示
//    },

    NSString * isUserScores = [NSString stringWithFormat:@"%@",BillDetailsDic[@"scoresVo"][@"isUserScores"]];
    if ([isUserScores isEqualToString:@"1"]) {
        _pointLabel.text = [NSString stringWithFormat:@"积分抵扣¥%.2f",[BillDetailsDic[@"scoresVo"][@"scoresPaid"] floatValue]];
        SwitchView.hidden = NO;
    }else{
        
        SwitchView.hidden = YES;
        _pointLabel.text = [NSString stringWithFormat:@"%@",BillDetailsDic[@"scoresVo"][@"scoresText"]];
    }
    
    NSString * billstatus = [NSString stringWithFormat:@"%@",BillDetailsDic[@"scoresVo"][@"billstatus"]];
    //"billstatus": 20 // 10 申请结算 20 结算中 30 已结算

    if ([billstatus isEqualToString:@"10"]) {
        ApplyBtn.backgroundColor = YellowColor;
        [ApplyBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
        [ApplyBtn setTitle:@"申请结算" forState:UIControlStateNormal];
        ApplyBtn.userInteractionEnabled = YES;
         ApplyBtn.layer.borderWidth = 0.5;
        _pointLabel.textColor = YellowColor;
        mySwitch.hidden = NO;

    }else if ([billstatus isEqualToString:@"20"]){
        ApplyBtn.backgroundColor = LineGrayColor;
        [ApplyBtn setTitleColor:carScrollColor forState:UIControlStateNormal];
        ApplyBtn.userInteractionEnabled = NO;
        [ApplyBtn setTitle:@"结算中...." forState:UIControlStateNormal];
        ApplyBtn.layer.borderWidth = 0;
        
        _pointLabel.textColor = carScrollColor;
        mySwitch.hidden = YES;
    }else if ([billstatus isEqualToString:@"30"]){
        ApplyBtn.backgroundColor = LineGrayColor;
        [ApplyBtn setTitleColor:carScrollColor forState:UIControlStateNormal];
        ApplyBtn.userInteractionEnabled = NO;
        [ApplyBtn setTitle:@"已结算" forState:UIControlStateNormal];
        ApplyBtn.layer.borderWidth = 0;
        _pointLabel.textColor = carScrollColor;
        mySwitch.hidden = YES;
    }
    else{
        ApplyBtn.backgroundColor = LineGrayColor;
        [ApplyBtn setTitleColor:carScrollColor forState:UIControlStateNormal];
        [ApplyBtn setTitle:@"申请结算" forState:UIControlStateNormal];
        ApplyBtn.userInteractionEnabled = NO;
        ApplyBtn.layer.borderWidth = 0;
        _pointLabel.textColor = YellowColor;
        mySwitch.hidden = YES;
    }
}

-(void)createBackView1{
    
    
    _backView1 = [[UIView alloc]init];
    _backView1.backgroundColor = WhiteColor;
    [self.view addSubview:_backView1];
    [_backView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.mas_equalTo(nav.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 174.5*kHeight));
    }];
    
    _dataLabel = [com createUIlabel:@"" andFont:FontOfSize12 andColor:carScrollColor];
    [_backView1 addSubview:_dataLabel];
    [_dataLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backView1.mas_left).with.offset(17*kWidth);
        make.centerY.mas_equalTo(_backView1.mas_top).with.offset(22*kHeight);
    }];

    
    
    UIImageView * image = [[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"icon_infor_data_imges"];
    [_backView1 addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backView1.mas_right).with.offset(-17*kWidth);
        make.centerY.mas_equalTo(_dataLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(17*kWidth, 18*kHeight));
    }];
    
    UIButton * btnTime = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnTime addTarget:self action:@selector(pressTimeBtn) forControlEvents:UIControlEventTouchUpInside];
    btnTime.tag = 100;
    [_backView1 addSubview:btnTime];
    [btnTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backView1.mas_right);
        make.centerY.mas_equalTo(_dataLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50*kWidth, 44*kHeight));
    }];
    
//    UIButton * shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [shareBtn addTarget:self action:@selector(pressShareBtn) forControlEvents:UIControlEventTouchUpInside];
//    [shareBtn setImage:[UIImage imageNamed:@"分享"] forState:UIControlStateNormal];
//    shareBtn.tag = 100;
//    [_backView1 addSubview:shareBtn];
//    [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(image.mas_left);
//        make.centerY.mas_equalTo(_dataLabel.mas_centerY);
//        make.size.mas_equalTo(CGSizeMake(50*kWidth, 44*kHeight));
//    }];

    
    
    
    UIView * line = [[UIView alloc]init];
    line.backgroundColor = GrayColor;
    [_backView1 addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backView1.mas_left);
        make.centerY.mas_equalTo(_backView1.mas_top).with.offset(44*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
    UILabel * name = [com createUIlabel:@"账单金额" andFont:FontOfSize13 andColor:AddCarColor];
    [_backView1 addSubview:name];
    [name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_backView1.mas_centerX);
        make.centerY.mas_equalTo(line.mas_bottom).with.offset(43*kHeight);
    }];
    
    _MoneyLabel = [com createUIlabel:@"0.00" andFont:23.0 andColor:YellowColor];
    [_backView1 addSubview:_MoneyLabel];
    [_MoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_backView1.mas_centerX);
        make.centerY.mas_equalTo(line.mas_bottom).with.offset(68*kHeight);
    }];


    UILabel * orderName = [com createUIlabel:@"订单：" andFont:FontOfSize11 andColor:AddCarColor];
    [_backView1 addSubview:orderName];
    [orderName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backView1.mas_left).with.offset(82*kWidth);
        make.centerY.mas_equalTo(_MoneyLabel.mas_centerY).with.offset(30*kHeight);
    }];
    _orderNumberLabel = [com createUIlabel:@"0" andFont:FontOfSize11 andColor:AddCarColor];
    [_backView1 addSubview:_orderNumberLabel];
    [_orderNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(orderName.mas_right);
        make.centerY.mas_equalTo(orderName.mas_centerY);
    }];
    UILabel * carName = [com createUIlabel:@"发运车辆：" andFont:FontOfSize11 andColor:AddCarColor];
    [_backView1 addSubview:carName];
    [carName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_orderNumberLabel.mas_right).with.offset(60*kWidth);
        make.centerY.mas_equalTo(_orderNumberLabel.mas_centerY);
    }];
    
    _CarNumberLabel = [com createUIlabel:@"0" andFont:FontOfSize11 andColor:AddCarColor];
    [_backView1 addSubview:_CarNumberLabel];
    [_CarNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(carName.mas_right);
        make.centerY.mas_equalTo(carName.mas_centerY);
    }];
}


-(void)pressShareBtn{
    
    if ([self.ShareDic isEqual:[NSNull class]]) {
        [self createUIAlertController:@"本月暂无账单可分享"];
    }else{
        [self navRightBtnClick];
    }

}


-(void)createBackView2{
    
    _backView2 = [[UIView alloc]init];
    _backView2.backgroundColor = WhiteColor;
    [self.view addSubview:_backView2];
    [_backView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.mas_equalTo(_backView1.mas_bottom).with.offset(5*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 88*kHeight));
    }];
    
    
    UILabel * label1 = [com createUIlabel:@"结算金额" andFont:FontOfSize12 andColor:carScrollColor];
    [_backView2 addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backView2.mas_left).with.offset(17*kWidth);
        make.centerY.mas_equalTo(_backView2.mas_top).with.offset(22*kHeight);
    }];
    
    _BalanceLabel = [com createUIlabel:@"" andFont:FontOfSize12 andColor:YellowColor];
    [_backView2 addSubview:_BalanceLabel];
    [_BalanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backView2.mas_right).with.offset(-15*kWidth);
        make.centerY.mas_equalTo(label1.mas_centerY);
    }];

    
    UIView * line = [[UIView alloc]init];
    line.backgroundColor = GrayColor;
    [_backView2 addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backView2.mas_left);
        make.centerY.mas_equalTo(_backView2.mas_top).with.offset(44*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    _pointLabel = [[UILabel alloc]init];
//    _pointLabel.font = Font(FontOfSize12);
    _pointLabel.font = [UIFont systemFontOfSize:12];
    _pointLabel.textColor = YellowColor;
//    _pointLabel.adjustsFontSizeToFitWidth = YES;
    _pointLabel.numberOfLines = 0;
    [_backView2 addSubview:_pointLabel];
    [_pointLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backView2.mas_left).with.offset(17*kWidth);
        make.centerY.mas_equalTo(_backView2.mas_top).with.offset(66*kHeight);
        make.width.mas_equalTo(Main_Width*0.75);
        make.height.mas_equalTo(cellHeight);
    }];


    mySwitch = [[SevenSwitch alloc] initWithFrame:CGRectZero];
    [mySwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    mySwitch.on = NO;
    //        NSLog(@"height%fwidth%f",mySwitch.frame.size.height,mySwitch.frame.size.width);
    mySwitch.onColor = YellowColor;
    [_backView2 addSubview:mySwitch];
    [mySwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backView2.mas_right).with.offset(-15*kWidth);
        make.centerY.mas_equalTo(_pointLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50, 30));
    }];
    
    SwitchView.hidden = YES;
    [mySwitch addSubview:SwitchView];
    [SwitchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(mySwitch.mas_centerX);
        make.centerY.mas_equalTo(mySwitch.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50, 30));

    }];

    
    
}

-(void)pressBillDetailBtn{
    
    Bill_HM_VC * bill = [[Bill_HM_VC alloc]init];
    bill.orderID = BillListArr[indexrow][@"id"];
    bill.monthText = BillListArr[indexrow][@"monthText"];
    NSLog(@"bill.orderID%@bill.monthText%@",bill.orderID,bill.monthText);
    [self.navigationController pushViewController:bill animated:YES];
}

-(void)createBtn{
    

    BillDetaile = [UIButton buttonWithType:UIButtonTypeCustom];
    [BillDetaile setTitle:@"账单明细" forState:UIControlStateNormal];
    [BillDetaile setTitleColor:WhiteColor forState:UIControlStateNormal];
    BillDetaile.titleLabel.font = Font(FontOfSize11);
//    BillDetaile.backgroundColor = [UIColor cyanColor];
    [BillDetaile addTarget:self action:@selector(pressBillDetailBtn) forControlEvents:UIControlEventTouchUpInside];
    [nav addSubview:BillDetaile];
    [BillDetaile mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(nav.mas_right);
        make.size.mas_equalTo(CGSizeMake(70*kWidth, 40*kHeight));
        make.centerY.mas_equalTo(nav.mas_centerY);
    }];

    ApplyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [ApplyBtn setTitle:@"申请结算" forState:UIControlStateNormal];
    [ApplyBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
    [ApplyBtn addTarget:self action:@selector(pressApplyBtn) forControlEvents:UIControlEventTouchUpInside];
    ApplyBtn.layer.cornerRadius = 5;
    ApplyBtn.layer.borderWidth = 0.5;
    ApplyBtn.layer.borderColor = YellowColor.CGColor;
    ApplyBtn.backgroundColor = YellowColor;
    [self.view addSubview:ApplyBtn];
    [ApplyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_backView2.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(293*kWidth, 40*kHeight));
        make.top.mas_equalTo(_backView2.mas_bottom).with.offset(15*kHeight);
    }];
}

-(void)createTable{
    
    
    _backView3 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, Main_height)];
    _backView3.backgroundColor = RGBACOLOR(0, 0, 0, 0.3);
    _backView3.userInteractionEnabled = YES;
    _backView3.hidden = YES;
    UIWindow *window = [[UIApplication sharedApplication].windows lastObject];
    // 添加到窗口
    [window addSubview:_backView3];

    UITapGestureRecognizer * Tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dataBackDiss)];
    Tap.delegate = self;
    [_backView3 addGestureRecognizer:Tap];

    UIImageView * image = [[UIImageView alloc]init];
    image.userInteractionEnabled = YES;
    image.tag = 1111;
    image.image = [UIImage imageNamed:@"icon_infor_drop-down_imges"];
    [_backView3 addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_backView3.mas_right).with.offset(-5*kWidth);
        make.top.mas_equalTo(_backView3.mas_top).with.offset(90*kHeight);
        make.size.mas_equalTo(CGSizeMake(74.5*kWidth, 44*kHeight*4));
    }];


    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 0, 43*kHeight) style:UITableViewStylePlain];
    [table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table.layer.cornerRadius = 5;
    table.showsVerticalScrollIndicator = NO;
    table.showsHorizontalScrollIndicator = NO;


    table.delegate = self;
    table.dataSource = self;
    [image addSubview:table];//149 × 196
    [table mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(image.mas_left).with.offset(1.5*kHeight);
        make.top.mas_equalTo(image.mas_top).with.offset(14*kHeight);
        make.size.mas_equalTo(CGSizeMake(69*kWidth, 38*kHeight*4));
    }];

}

-(void)dataBackDiss{
    
    _backView3.hidden = YES;
    
}

-(void)pressTimeBtn{
    

    if (_backView3.hidden) {
        _backView3.hidden = NO;
    }else{
        _backView3.hidden = NO;
    }
}

-(void)switchChanged:(UISwitch*)mySwitch1{

    CGFloat price = 0;
    
    if (mySwitch1.on) {
        isIntegral = @"1";
    
        price = [BillDetailsDic[@"scoresVo"][@"actualPaid"] floatValue] - [BillDetailsDic[@"scoresVo"][@"amount"] floatValue];
        
        _BalanceLabel.text = [NSString stringWithFormat:@"￥%.2f",price];
        NSLog(@"开");
    }else{
        isIntegral = @"0";
        _BalanceLabel.text = [NSString stringWithFormat:@"￥%.2f",[BillListArr[indexrow][@"amount"] floatValue]];

        NSLog(@"关");
    }
}


-(void)pressApplyBtn{
    
    
    NSLog(@"申请结算");
    
    //
    
    [self createUIAlertApplyBtn:@"是否进行结算申请"];
    
    
    
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *str=@"str";
    BillTimeCell  * cell =  [tableView dequeueReusableCellWithIdentifier:str];
    if (cell ==nil) {
        cell = [[BillTimeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    cell.title.text = [NSString stringWithFormat:@"%@",BillListArr[indexPath.row][@"monthText"]];
    if (indexPath.row == [BillListArr count]-1) {
        cell.viewline.hidden = YES;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    indexrow = indexPath.row;
    
    [self createUIdate:BillListArr[indexPath.row]];
    
    NSString * billID = [NSString stringWithFormat:@"%@",BillListArr[indexPath.row][@"id"]];
    [self createGetPointDate:billID];
    _backView3.hidden = YES;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 40*kHeight;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return BillListArr.count;
    
}

-(void)createUIAlertController:(NSString*)title
{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        [self createDate];
        
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)createUIAlertApplyBtn:(NSString*)title
{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
       
        NSString * BillId = [NSString stringWithFormat:@"%@",BillListArr[indexrow][@"id"]];
        NSString * url = [NSString stringWithFormat:@"%@?isIntegral=%@&billId=%@",Find_settlement_Url,isIntegral,BillId];
        
        [com afPostRequestWithUrlString:url parms:nil finishedBlock:^(id responseObj) {
            NSMutableDictionary * dicObj =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];
            
            
            [self createUIAlertController:dicObj[@"message"]];
            
            
        } failedBlock:^(NSString *errorMsg) {
        }];
        
    
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/* 解决触摸手势和cell点击的冲突 */
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    if ([touch.view isEqual:_backView3]) {
        return YES;
    }
    return NO;
}



@end
