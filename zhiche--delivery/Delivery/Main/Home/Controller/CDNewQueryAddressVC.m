//
//  CDNewQueryAddressVC
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/11/17.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "CDNewQueryAddressVC.h"
#import <Masonry.h>
#import "Common.h"
#import "area.h"
#import "QueryAddressCell.h"
#import "CustomTextField.h"
#define LeftTableTag  1000
#define RightTableTag  2000
#define SearchTableTag 3000
#define MySetValue(dict, value) ((!dict[value] || [dict[value] isKindOfClass:[NSNull class]])?@"":dict[value])


@interface CDNewQueryAddressVC ()
{
    UIImageView * nav;
    Common * com;
    NSMutableArray * dataSouceArr;
    UITableView * left_table;
    NSMutableArray * cityListArr;
    NSMutableDictionary * backAddressDic;
    area * AREA;
    NSInteger tableInteger;
    UIView * backview;
    

}
@property (nonatomic, strong) NSMutableArray *provienceArray;
@property (nonatomic, strong) NSMutableArray *cityArray;
@property (nonatomic,strong) NSMutableArray * HotCityArr;
@property (nonatomic) int lenght;

@end

@implementation CDNewQueryAddressVC

- (instancetype)init

{
    self = [super init];
    if (self) {
        self.CityListCode = [[NSString alloc]init];
        self.CityListName = [[NSString alloc]init];

    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    nav = [self createNav:@"选择城市"];
    [self.view addSubview:nav];
    self.view.backgroundColor = GrayColor;
    
    [self initTable];
    [self initData];
    
    _lenght = 0;
}

-(void)initTable{
    

    
    left_table = [[UITableView alloc]initWithFrame:CGRectMake(0, 64*kHeight, Main_Width, 86*kHeight) style:UITableViewStylePlain];
    [left_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    left_table.tag = LeftTableTag;
    left_table.delegate = self;
    left_table.dataSource = self;
    [self.view addSubview:left_table];
    [left_table mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).with.offset(0);
        make.top.mas_equalTo(nav.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(Main_Width, Main_height-64));
    }];



}

-(void)initData{
    
    tableInteger = 0;
    AREA = [[area alloc]init];
    dataSouceArr = [[NSMutableArray alloc]init];
    _HotCityArr = [[NSMutableArray alloc]init];
    com = [[Common alloc]init];
    _provienceArray = [[NSMutableArray alloc]init];
    backAddressDic = [[NSMutableDictionary alloc]init];
    
    [backAddressDic setObject:@"" forKey:@"provinceName"];
    [backAddressDic setObject:@"" forKey:@"provinceCode"];
    [backAddressDic setObject:@"" forKey:@"cityName"];
    [backAddressDic setObject:@"" forKey:@"cityCode"];
    [backAddressDic setObject:@"" forKey:@"countyName"];
    [backAddressDic setObject:@"" forKey:@"countyCode"];
   
    cityListArr = [[NSMutableArray alloc]init];
    
    [self initLeftDataSource];
    
}


-(void)initLeftDataSource
{
    

//    [backAddressDic setObject:_provienceArray[0][@"provinceName"] forKey:@"provinceName"];
//    [backAddressDic setObject:_provienceArray[0][@"provinceCode"] forKey:@"provinceCode"];
    [self initRightDataWith:self.CityListCode];
    
}

-(void)initRightDataWith:(NSString *)string
{
    NSString * url = [NSString stringWithFormat:@"%@/%@",area_country_list,self.CityListCode];
    
    [Common requestWithUrlString:url contentType:application_json finished:^(id responseObj) {
        NSLog(@"%@",responseObj[@"message"]);

        _cityArray = responseObj[@"data"];

        if (_cityArray.count>0) {
            [left_table reloadData];
        }
        
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *str=@"str";
    

    QueryAddressCell * cell =  [tableView dequeueReusableCellWithIdentifier:str];
        
    if (cell ==nil) {
        
        cell = [[QueryAddressCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
    }
    
    NSString *dict = _cityArray[indexPath.row][@"name"];
    cell.title.text = dict;
    cell.backgroundColor = RGBACOLOR(240,241,242,1);
    return cell;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    

    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    

    return [_cityArray  count];

}


//-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    
//    UIView * view = [[UIView alloc]init];
//    view.frame = CGRectMake(0, 0, Main_Width, 42*kHeight);
//    view.backgroundColor = sectionHeadView;
//    UILabel * label = [com createUIlabel:_cityArray[section][@"cityName"] andFont:FontOfSize14 andColor:BlackColor];
//    [view addSubview:label];
//    [label mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(view.mas_centerX);
//        make.centerY.equalTo(view.mas_centerY);
//    }];
//    return view;
//
//}

//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    
//    return 43*kHeight;
//}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 43*kHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    

    [backAddressDic setObject:_cityArray[indexPath.row][@"name"] forKey:@"countyName"];
    [backAddressDic setObject:_cityArray[indexPath.row][@"code"] forKey:@"countyCode"];

    
        
        if (self.callAddressBack) {
            self.callAddressBack(backAddressDic,self.addresstype);
        }
        [self.navigationController popViewControllerAnimated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
