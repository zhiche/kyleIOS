//
//  ConfirmCarVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/25.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface ConfirmCarVC : NavViewController<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) NSMutableDictionary * moneyDic;
@property (nonatomic,copy) void (^callBack)(BOOL  isBack);


@end
