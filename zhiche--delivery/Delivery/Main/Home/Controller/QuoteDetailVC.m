//
//  QuoteDetailVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/26.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "QuoteDetailVC.h"
#import "Common.h"
#import <Pingpp.h>
#import <Masonry.h>
#import "PayViewController.h"

@interface QuoteDetailVC ()

{
    Common * com;
}
@property (nonatomic,copy) NSString *cityName;

@property (nonatomic,strong) NSMutableDictionary *dataDictionary;
@property (nonatomic,strong) UIView *payView;
@property (nonatomic) BOOL isAgree;
@property (nonatomic) int rowNumber;
@property (nonatomic,strong) UILabel *moneyL;

@property (nonatomic,strong) UILabel * priceAllLabel;
@property (nonatomic,strong) UIView * priceAllLine;
@property (nonatomic,strong) UILabel * activityLabel;



@end

@implementation QuoteDetailVC

- (instancetype)init

{
    self = [super init];
    if (self) {
        self.quoteid = [[NSString alloc]init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataDictionary = [NSMutableDictionary dictionary];
    self.view.backgroundColor = [UIColor whiteColor];
    
    com = [[Common alloc]init];
    
    _priceAllLabel = [com createUIlabel:@"" andFont:FontOfSize12 andColor:carScrollColor];
    _priceAllLine = [[UIView alloc]init];
    _activityLabel = [com createUIlabel:@"" andFont:FontOfSize12 andColor:YellowColor];
    

    [self initDataSource];
    [self initSubView];
}




-(void)initSubView
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.priceView.frame) , screenWidth, cellHeight)];
    view.backgroundColor = WhiteColor;
    view.tag = 10000;
    [self.scrollView addSubview:view];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, screenWidth - 18, 0.5)];
    label.backgroundColor = LineGrayColor;
    [view addSubview:label];
    
    
//    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 165 * kWidth, 0, 70 * kWidth, cellHeight)];
//    label1.tag = 301;
//    label1.textColor = YellowColor;
//    label1.font = Font(16);
//    label1.text = @"总订单费";
//    
    self.moneyL = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_activityLabel.frame), 0 , 80 * kWidth, cellHeight)];
    self.moneyL.font = Font(16);
    self.moneyL.textColor = YellowColor;
    self.moneyL.text = @"";
    self.moneyL.textAlignment = NSTextAlignmentRight;
    [view addSubview:self.moneyL];
    
//    [view addSubview:label1];
    
//    UIButton *payButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    payButton.frame = CGRectMake(18, CGRectGetMaxY(view.frame) + 30, screenWidth - 36, Button_Height) ;
//    [payButton setTitle:@"确定支付" forState:UIControlStateNormal];
//    payButton.layer.cornerRadius = 5;
//    [payButton setTitleColor:WhiteColor forState:UIControlStateNormal];
//    payButton.backgroundColor = YellowColor;
//    [self.scrollView addSubview:payButton];
//    [payButton addTarget:self action:@selector(payButtonAction) forControlEvents:UIControlEventTouchUpInside];
//    payButton.titleLabel.font = Font(17);
//    
    
//    self.scrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(payButton.frame) + 64 + 20 );
    
    
    
}

-(void)payButtonAction
{
    PayViewController *payVC = [[PayViewController alloc]init];
    
    payVC.quoteid = self.quoteid;
    payVC.orderid = self.orderId;
    payVC.orderNumber = self.numberLabel.text;
    payVC.moneyString = self.moneyL.text;
    
    [self.navigationController pushViewController:payVC animated:YES];
}

-(void)initDataSource
{
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%@",order_detail,self.orderId];
    
    [Common requestWithUrlString:urlString contentType:application_json finished:^(id responseObj) {
        
        
        NSLog(@"%@",responseObj);

        if ([responseObj[@"success"] boolValue]) {
            
            
            self.dataDictionary = [NSMutableDictionary dictionaryWithDictionary:responseObj[@"data"]];
            
            [self getValueString];
            
        }
        
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
}


-(void)getValueString
{
    /**
     订单
     */
//    actualCost = 817;
//    arriveDate = "2016-07-27";
//    deliverPics = "<null>";
//    deliveryDate = "2016-07-27";
//    departCityName = "\U5929\U6d25\U5e02";
//    departProvinceName = "\U5929\U6d25\U5e02";
//    departUnit = gjsj;
//    detail = "\U5965\U8fea1\U8f86";
//    driverInfo = "<null>";
//    feeDetail =         {
//        extractcost = 0;
//        insurance = 30;
//        returncost = 0;
//        shippingfee = "786.38";
//        total = 817;
//    };
//    id = 17;
//    isDeliv = 0;
//    isPick = 0;
//    orderCode = O758168472534061056;
//    paidCost = "<null>";
//    payStatus = 10;
//    payStatusText = "\U672a\U4ed8\U6b3e";
//    postTimeRemaining = 0;
//    quoteCounts = 0;
//    receiptCityName = "\U90a2\U53f0\U5e02";
//    receiptPics =         (
//    );
//    receiptProvinceName = "\U6cb3\U5317\U7701";
//    receiptUnit = flsnd;
//    receivePics =         (
//    );
//    sendPics = "<null>";
//    status = 10;
//    statusText = "\U5df2\U53d1\U5e03";
//    unpaidCost = "<null>";
//    userId = 4;
//    vehicles =         (
//                        {
//                            amount = 1;
//                            brand =                 {
//                                id = "<null>";
//                                logo = "Audi.png";
//                            };
//                            brandName = "\U5965\U8fea";
//                            id = 23;
//                            vehicleName = allroad;
//                            vinList =                 (
//                                                       {
//                                                           color = "<null>";
//                                                           id = 24;
//                                                           vin = 11111111111111111;
//                                                       }
//                                                       );
//                        }
//                        );
//};

    //订单
    self.numberLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"orderCode"]];
    //状态
    
    //    self.statusLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"statusText"]];
    //起始地
    
    self.startLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"departCityName"]];
    //起始详情地址
    
    self.startDetailLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"departUnit"]];
    //起始日期
    
    self.startDate.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"deliveryDate"]];
    
    //目的地
    
    self.endLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"receiptCityName"]];
    //
    
    self.endDetailLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"receiptUnit"]];
    self.endDateLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"arriveDate"]];
    
    //车辆
    
    self.carLabel.text = @"";
    
    for (int i = 0; i < [self.dataDictionary[@"vehicles"] count]; i ++) {
        NSString *string = [NSString stringWithFormat:@"%@-%@-%@辆",self.dataDictionary[@"vehicles"][i][@"brandName"],self.dataDictionary[@"vehicles"][i][@"vehicleName"],self.dataDictionary[@"vehicles"][i][@"amount"]];
        
        self.carLabel.text = [self.carLabel.text stringByAppendingString:string];
        self.carLabel.text = [self.carLabel.text stringByAppendingString:@"   "];
        
    }
    
    NSLog(@"%lu",self.carLabel.text.length);
    self.scrollView.contentSize = CGSizeMake(self.carLabel.text.length, screenHeight * 1.2);
    
    
    
    /**
     物流
     */
    //    feeDetail =         {
    //        extractcost = 0;
    //        insurance = 30;
    //        returncost = 0;
    //        shippingfee = "786.38";
    //        total = 817;
    //    };

    
    
    //提车费
    
    self.totalLabel.text = [self backMoney:self.dataDictionary[@"feeDetail"][@"extractcost"]];
    // 交车费
    self.payLabel.text = [self backMoney:self.dataDictionary[@"feeDetail"][@"returncost"]];
    
    //保险费
    self.unpayLabel.text = [self backMoney:self.dataDictionary[@"feeDetail"][@"insurance"]];
    //运输费
    self.scoreLabel.text = [self backMoney:self.dataDictionary[@"feeDetail"][@"shippingfee"]];
    
    
    
    NSString * isdiscount = [NSString stringWithFormat:@"%@",_dataDictionary[@"feeDetail"][@"isdiscount"]];
    UIView * view = (UIView*)[self.view viewWithTag:10000];

    //优惠
    if ([isdiscount isEqualToString:@"T"]) {
        float total = [self.dataDictionary[@"feeDetail"][@"total"] floatValue];
        _priceAllLabel.text = [NSString stringWithFormat:@"￥%.2f",total];
        [view addSubview:_priceAllLabel];
        [_priceAllLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.moneyL.mas_left).with.offset(-16*kHeight);
            make.centerY.mas_equalTo(self.moneyL.mas_centerY);
        }];
        _priceAllLine.backgroundColor = BlackColor;
        [_priceAllLabel addSubview:_priceAllLine];
        [_priceAllLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_priceAllLabel.mas_left);
            make.right.equalTo(_priceAllLabel.mas_right);
            make.centerY.mas_equalTo(_priceAllLabel.mas_centerY);
            make.height.mas_equalTo(0.5);
        }];
        _activityLabel.text =  [NSString stringWithFormat:@"%@",self.dataDictionary[@"feeDetail"][@"desc"]];
        [view addSubview:_activityLabel];
        
        [_activityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view.mas_centerX);
            make.top.mas_equalTo(self.moneyL.mas_bottom).with.offset(3*kHeight);
        }];
    }
    self.moneyL.text = [self backMoney:self.dataDictionary[@"feeDetail"][@"actualcost"]];
    [self.moneyL sizeToFit];
    self.moneyL.frame = CGRectMake(screenWidth - 15 - self.moneyL.frame.size.width, 0, self.moneyL.frame.size.width, cellHeight);
    
    UILabel *label = (UILabel *)[self.view viewWithTag:301];
    label.frame = CGRectMake(CGRectGetMinX(self.moneyL.frame) - 70 * kWidth, 0, 70 * kWidth, cellHeight);
    
    //当前定位城市
//    self.cityLabel.text = [self backString:self.dataDictionary[@"quote"][@"position"]];
}


-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSString *)backString:(NSString *)string
{
    
    if ([string isEqual:[NSNull null]]) {
        return @"";
    } else {
        return string;
    }
}

-(NSString *)backMoney:(NSString *)string
{
    NSString   *str = [NSString stringWithFormat:@"%.2f",[string floatValue]];
    
    if ([str isEqual:[NSNull null]]) {
        return @"";
    } else {
        return [NSString stringWithFormat:@"￥%@",str];
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
