//
//  QuoteListVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/6/1.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface QuoteListVC : NavViewController<UITableViewDelegate,UITableViewDataSource>


@property (nonatomic,strong) NSString * orderID;

@end
