//
//  AddressVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/10.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "AddressVC.h"
#import <Masonry.h>
#import "Header.h"
#import "Common.h"
#import "ActionSheetPicker.h"
#import "area.h"
#import "PlaceOrderVC.h"
#import "ChooseAddressVC.h"
#import "WKProgressHUD.h"
#import "CityDeliveryVC.h"
#import "QueryAddressVC.h"
#import "CDNewQueryAddressVC.h"
#import <AMapSearchKit/AMapSearchKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <ContactsUI/ContactsUI.h>

#define AreaBtnTag 100
#define KeepBtnTag 200
#define FieldTag 500
#define kMaxLength 5

#define MySetValue(dict, value) ((!dict[value] || [dict[value] isKindOfClass:[NSNull class]])?@"":dict[value])

@interface AddressVC ()<AMapSearchDelegate,CNContactPickerDelegate>
{
    UIImageView * nav;
    Common * Com;
    area * AREA;
    NSMutableDictionary * dic;
    WKProgressHUD *hud;
     
    
}
//@property (nonatomic,strong) AMapSearchAPI * search;

@property (nonatomic,strong) UIView * backView;
@property (nonatomic,strong) UIButton * AreaBtn;
@property (nonatomic, strong) NSMutableArray *provienceArray;
@property (nonatomic, strong) NSArray *cityArray;
//@property (nonatomic, strong) NSArray *areaArray;
@property (nonatomic,assign) BOOL store;


@end

@implementation AddressVC

- (instancetype)init

{
    self = [super init];
    if (self) {
        self.dataSouce = [[NSMutableDictionary alloc]init];
        self.navtitle = [[NSString alloc]init];
        self.addresstype = [[NSString alloc]init];
        self.CityListCode = [[NSString alloc]init];
        self.CityListName = [[NSString alloc]init];
        
        
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = GrayColor;
    _backView = [[UIView alloc]init];
    _backView.backgroundColor = WhiteColor;
    [self.view addSubview:_backView];
    
    Com = [[Common alloc]init];
    AREA = [[area alloc]init];
    dic=[[NSMutableDictionary alloc]init];
    
    if ([_addresstype isEqualToString:@"1"]) {
        [dic setObject:@"1" forKey:@"addresstype"]; //地址类型(0:发车地址，1:送达地址)
    }else if ([_addresstype isEqualToString:@"0"]){
        [dic setObject:@"0" forKey:@"addresstype"]; //地址类型(0:发车地址，1:送达地址)
    }
    if ([_navtitle isEqualToString:@"1"]) {
        nav = [self createNav:@"收车地址编辑"];
    }else if ([_navtitle isEqualToString:@"0"]){
        nav = [self createNav:@"发车地址编辑"];
    }else{
        if ([_addresstype isEqualToString:@"1"]) {
            nav = [self createNav:@"新增收车地址"];
        }else{
            nav = [self createNav:@"新增发车地址"];
        }
    }
    [self.view addSubview:nav];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.mas_equalTo(nav.mas_bottom).with.offset(0);
        //        make.size.mas_equalTo(CGSizeMake(Main_Width, 215*kHeight));
        make.size.mas_equalTo(CGSizeMake(Main_Width, 172*kHeight));
        
    }];
    
    
    [dic setObject:@"" forKey:@"provincecode"];//省编码
    [dic setObject:@"" forKey:@"provincename"]; //省名称
    [dic setObject:@"" forKey:@"citycode"];//市编码
    [dic setObject:@"" forKey:@"cityname"]; //市名称
    [dic setObject:@"" forKey:@"countycode"];//县编码
    [dic setObject:@"" forKey:@"countyname"]; //县名称
    [dic setObject:@"" forKey:@"contact"];//联系人
    [dic setObject:@"" forKey:@"phone"]; //联系电话
    //    [dic setObject:@"" forKey:@"unitname"];//单位名称
    [dic setObject:@"" forKey:@"address"];//详细地址
    [dic setObject:@"F" forKey:@"isdefault"];//是否默认(F:否，T:是)
    
    [self createUI];
    
    //    [AMapServices sharedServices].apiKey =@"decee6229c378085197d527b8a20e32f";
    //    self.search = [[AMapSearchAPI alloc] init];
    //    self.search.delegate = self;
    //
    //    AMapInputTipsSearchRequest *request = [[AMapInputTipsSearchRequest alloc] init];
    //    request.keywords            = @"邢台学院";
    //    request.city                = @"邢台";
    //
    //    [self.search AMapInputTipsSearch: request];
    
}
-(void)onInputTipsSearchDone:(AMapInputTipsSearchRequest*)request response:(AMapInputTipsSearchResponse *)response{
    
    if (response.tips.count == 0)
    {
        return;
    }else{
        for (AMapTip *p in response.tips) {
            //把搜索结果存在数组
            
            //            NSLog(@"%@",p.address);
            NSLog(@"%@",p.name);
            
            
        }
        
        
    }
    
    
    //解析response获取POI信息，具体解析见 Demo
}



-(void)createUI{
    NSArray * arrName = nil;
    NSArray * FieldName =nil;
    if ([_addresstype isEqualToString:@"0"]) {
        //       arrName = @[@"区域",@"发车单位",@"联系人",@"手机号码",@"详细地址"];
        //        FieldName = @[@"请输入发车单位",@"请输入联系人",@"请输入联系电话",@"请输入详细地址",];
        arrName = @[@"区域",
                    @"联系人",
                    @"手机号码",
                    @"详细地址"];
        FieldName = @[@"请输入联系人",
                      @"请输入联系电话",
                      @"请输入详细地址",];
        
    }
    else{
        //        arrName = @[@"区域",@"收车单位",@"联系人",@"手机号码",@"详细地址"];
        //        FieldName = @[@"请输入收车单位",@"请输入联系人",@"请输入联系电话",@"请输入详细地址"];
        arrName = @[@"区域",
                    @"联系人",
                    @"手机号码",
                    @"详细地址"];
        FieldName = @[@"请输入联系人",
                      @"请输入联系电话",
                      @"请输入详细地址"];
        
    }
    
    if (_dataSouce[@"id"]) {
        //        FieldName = @[self.dataSouce[@"unitName"],self.dataSouce[@"contact"],self.dataSouce[@"phone"],self.dataSouce[@"address"]];
        FieldName = @[self.dataSouce[@"contact"],self.dataSouce[@"phone"],self.dataSouce[@"address"]];
        
        [dic setObject:_dataSouce[@"addressType"] forKey:@"addresstype"]; //地址类型(0:发车地址，1:送达地址)provinceName cityName cityCode provinceCode
        [dic setObject:self.dataSouce[@"contact"] forKey:@"contact"];//联系人
        [dic setObject:self.dataSouce[@"phone"] forKey:@"phone"]; //联系电话
        //        [dic setObject:self.dataSouce[@"unitName"] forKey:@"unitname"];//单位名称
        [dic setObject:self.dataSouce[@"address"] forKey:@"address"];//详细地址
        [dic setObject:@"F" forKey:@"isdefault"];//是否默认(F:否，T:是)
        [dic setObject:self.dataSouce[@"provinceName"] forKey:@"provincename"];
        [dic setObject:self.dataSouce[@"provinceCode"] forKey:@"provincecode"];
        [dic setObject:self.dataSouce[@"cityName"] forKey:@"cityname"];
        [dic setObject:self.dataSouce[@"cityCode"] forKey:@"citycode"];
        
        [dic setObject:self.dataSouce[@"countyCode"] forKey:@"countycode"];
        [dic setObject:self.dataSouce[@"countyName"] forKey:@"countyname"];
        
        [dic setObject:_dataSouce[@"id"] forKey:@"id"];
    }
    for (int i=0; i<arrName.count; i++) {
        UILabel * label = [self createUIlabel:arrName[i] andFont:FontOfSize14 andColor:fontGrayColor];
        [_backView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backView.mas_left).with.offset(18*kWidth);
            make.centerY.mas_equalTo(_backView.mas_top).with.offset(43*kHeight*i+21.5*kHeight);
        }];
        
        if (i==1) {
            
            UIImageView * RightImage =[[UIImageView alloc]init];
            RightImage.image = [UIImage imageNamed:@"common_list_arrows_more"];
            RightImage.tag = 1000;
            [_backView addSubview:RightImage];
            [RightImage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(_backView.mas_right).with.offset(-15*kWidth);
                make.centerY.mas_equalTo(label.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(8*kWidth, 13*kHeight));
            }];
            

            UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn addTarget:self action:@selector(pressNumber) forControlEvents:UIControlEventTouchUpInside];
//            btn.backgroundColor = [UIColor redColor];
            [_backView addSubview:btn];
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(_backView.mas_right).with.offset(-15*kWidth);
                make.centerY.mas_equalTo(label.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(40*kWidth, 30*kHeight));
            }];

        }
        
        
        if (arrName.count != i+1) {
            UIView * Hline =[[UIView alloc]init];
            Hline.backgroundColor = LineGrayColor;
            [_backView addSubview:Hline];
            [Hline mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(label.mas_left);
                make.bottom.mas_equalTo(_backView.mas_top).with.offset(43*kWidth*(i+1));
                make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
            }];
        }
        if (i==0) {
            NSString * string = nil;
            if (self.dataSouce[@"id"]) {

                NSString *countyName = [NSString stringWithFormat:@"%@",self.dataSouce[@"countyName"]];
                
                string = [NSString stringWithFormat:@"%@%@",self.CityListName,countyName];
                
            }else{
                string = [NSString stringWithFormat:@"%@-",self.CityListName];
            }
            
            if (self.CityListName.length==0) {
                
                if (self.dataSouce[@"id"]) {
                    
                    NSString *countyName = [NSString stringWithFormat:@"%@",self.dataSouce[@"countyName"]];//海淀区
                    NSString *provinceName = [NSString stringWithFormat:@"%@",self.dataSouce[@"provinceName"]];
                    NSString *cityName = [NSString stringWithFormat:@"%@",self.dataSouce[@"cityName"]];
                    
                    if ([provinceName isEqualToString:cityName]) {
                        
                        string = [NSString stringWithFormat:@"%@%@",cityName,countyName];
                    }else{
                        string = [NSString stringWithFormat:@"%@%@%@",provinceName,cityName,countyName];
                    }
                }else{
                    string = [NSString stringWithFormat:@"点击选择地址"];
                }
                
            }else{
                
                if (self.dataSouce[@"id"]) {
                    
                    NSString *countyName = [NSString stringWithFormat:@"%@",self.dataSouce[@"countyName"]];//海淀区
                    NSString *provinceName = [NSString stringWithFormat:@"%@",self.dataSouce[@"provinceName"]];
                    NSString *cityName = [NSString stringWithFormat:@"%@",self.dataSouce[@"cityName"]];
                    //                    NSLog(@"%@%@%@",provinceName,cityName,countyName);
                    string = [NSString stringWithFormat:@"%@-%@",self.CityListName,countyName];
                    
                }else{
                    string = [NSString stringWithFormat:@"%@-",self.CityListName];
                }
            }
            
            
            _AreaBtn = [self createBtn:string andTag:AreaBtnTag];
            _AreaBtn.titleLabel.font = Font(FontOfSize14);
            [_AreaBtn addTarget:self action:@selector(pressBtnArea) forControlEvents:UIControlEventTouchUpInside];
            [_backView addSubview:_AreaBtn];
            [_AreaBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(_backView.mas_left).with.offset(105*kWidth);
                make.centerY.mas_equalTo(label.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(Main_Width*0.75, 25*kHeight));
            }];
            
            UIImageView * RightImage =[[UIImageView alloc]init];
            RightImage.image = [UIImage imageNamed:@"common_list_arrows_more"];
            RightImage.tag = 1000;
            [_backView addSubview:RightImage];
            [RightImage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(_backView.mas_right).with.offset(-15*kWidth);
                make.centerY.mas_equalTo(_AreaBtn.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(8*kWidth, 13*kHeight));
            }];
        }
        else{
            
            if (_dataSouce[@"id"]) {
                UITextField * field = [self createField:FieldName[i-1] andTag:FieldTag+i-1 andFont:FontOfSize14];
                if (i==3) {
                    field.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                }
                field.text = FieldName[i-1];
                field.placeholder = 0;
                [_backView addSubview:field];
                [field mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(_backView.mas_left).with.offset(105*kWidth);
                    make.centerY.mas_equalTo(label.mas_centerY);
                    make.size.mas_equalTo(CGSizeMake(Main_Width/2, 25*kHeight));
                }];
            }else{
                UITextField * field = [self createField:FieldName[i-1] andTag:FieldTag+i-1 andFont:FontOfSize14];
                field.placeholder = FieldName[i-1];
                [_backView addSubview:field];
                [field mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(_backView.mas_left).with.offset(105*kWidth);
                    make.centerY.mas_equalTo(label.mas_centerY);
                    make.size.mas_equalTo(CGSizeMake(Main_Width/2, 25*kHeight));
                }];
            }
        }
    }
    
    
    NSString *titleS ;
    if ([self.backString isEqualToString:@"1"]) {
        titleS = @"新增地址";
    } else {
        titleS = @"保存并使用";
        
    }
    
    
    
    UIButton * ConfirmBtn = [self createBtn:titleS andTag:KeepBtnTag];
    ConfirmBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    ConfirmBtn.titleLabel.font = Font(FontOfSize17);
    [ConfirmBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
    [ConfirmBtn addTarget:self action:@selector(pressStoreBtn) forControlEvents:UIControlEventTouchUpInside];
    ConfirmBtn.backgroundColor = YellowColor;
    ConfirmBtn.layer.cornerRadius = 5;
    [self.view addSubview:ConfirmBtn];
    [ConfirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(293*kWidth, 40*kHeight));
        make.top.mas_equalTo(_backView.mas_bottom).with.offset(30*kHeight);
    }];
}




-(void)pressNumber{
    
    
    //让用户给权限,没有的话会被拒的各位
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if (status == CNAuthorizationStatusNotDetermined) {
        CNContactStore *store = [[CNContactStore alloc] init];
        [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if (error) {
                NSLog(@"weishouquan ");
            }else
            {
                NSLog(@"chenggong ");//用户给权限了
                CNContactPickerViewController * picker = [CNContactPickerViewController new];
                picker.delegate = self;
                picker.displayedPropertyKeys = @[CNContactPhoneNumbersKey];//只显示手机号
                [self presentViewController: picker  animated:YES completion:nil];
            }
        }];
    }
    
    if (status == CNAuthorizationStatusAuthorized) {//有权限时
        CNContactPickerViewController * picker = [CNContactPickerViewController new];
        picker.delegate = self;
        picker.displayedPropertyKeys = @[CNContactPhoneNumbersKey];
        [self presentViewController: picker  animated:YES completion:nil];
    }
    else{
        NSLog(@"您未开启通讯录权限,请前往设置中心开启");
    }
    
}
-(void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact{
    
    CNPhoneNumber * num = nil;
    NSString * name = nil;
    NSString * number = nil;
    
    if (contact.phoneNumbers.count >0) {
        num = contact.phoneNumbers[0].value;
        name = [NSString stringWithFormat:@"%@%@",contact.familyName,contact.givenName];
        number = [NSString stringWithFormat:@"%@",[num valueForKey:@"digits"]];
    }else{
        name = [NSString stringWithFormat:@"%@%@",contact.familyName,contact.givenName];
        number = @"";
    }
    
    
//    UITextField * field = [self createField:FieldName[i-1] andTag:FieldTag+i-1 andFont:FontOfSize14];

    UITextField * field0 = (UITextField*)[_backView viewWithTag:FieldTag ];
    UITextField * field1 = (UITextField*)[_backView viewWithTag:FieldTag+1];
    field0.text = name;
    field1.text = number;
    

    [dic setObject:name forKey:@"contact"];
    [dic setObject:number forKey:@"phone"];
    
    //    NSLog(@"%@",contact);
    
}



-(void)pressBtnArea{
    
    
    for (UITextField * field in _backView.subviews) {
        
        [field resignFirstResponder];
        
    }
    
    if (self.CityListName.length==0) {
        
        QueryAddressVC * address = [[QueryAddressVC alloc]init];
        [self.navigationController pushViewController:address animated:YES];
        //返回选中的
        address.callAddressBack=^(NSMutableDictionary * dicBlock,NSString * addresstype){
            
            
            NSString * address2 = nil;
            if (![dicBlock[@"provinceName"] isEqualToString:dicBlock[@"cityName"]]) {
                
                address2 = [NSString stringWithFormat:@"%@%@%@",dicBlock[@"provinceName"],dicBlock[@"cityName"],dicBlock[@"countyName"]];
            }else{
                
                address2 = [NSString stringWithFormat:@"%@%@",dicBlock[@"provinceName"],dicBlock[@"countyName"]];
            }
            [_AreaBtn setTitle:address2 forState:UIControlStateNormal];
            [dic setObject:dicBlock[@"cityCode"] forKey:@"citycode"];
            [dic setObject:dicBlock[@"cityName"] forKey:@"cityname"];
            [dic setObject:dicBlock[@"provinceCode"] forKey:@"provincecode"];
            [dic setObject:dicBlock[@"provinceName"] forKey:@"provincename"];
            [dic setObject:dicBlock[@"countyCode"] forKey:@"countycode"];
            [dic setObject:dicBlock[@"countyName"] forKey:@"countyname"];
        };
    }else{
        CDNewQueryAddressVC * address = [[CDNewQueryAddressVC alloc]init];
        address.CityListCode = self.CityListCode;
        address.CityListName = self.CityListName;
        
        [self.navigationController pushViewController:address animated:YES];
        //返回选中的
        address.callAddressBack=^(NSMutableDictionary * dicBlock,NSString * addresstype){
            
            
            NSString * address2 = nil;
            
            address2 = [NSString stringWithFormat:@"%@-%@",self.CityListName,dicBlock[@"countyName"]];
            
            [_AreaBtn setTitle:address2 forState:UIControlStateNormal];
            [dic setObject:dicBlock[@"countyCode"] forKey:@"countycode"];
            [dic setObject:dicBlock[@"countyName"] forKey:@"countyname"];
            [dic setObject:self.CityListCode forKey:@"citycode"];
            [dic setObject:self.CityListName forKey:@"cityname"];
            [dic setObject:@"1" forKey:@"isVeneer"];
            [dic setObject:_addresstype forKey:@"addresstype"];
            
        };
        
    }
    
    
    
    
    
    
    
    //    _provienceArray = nil;
    //    _cityArray = nil;
    //    _areaArray = nil;
    
    //    _provienceArray = [AREA selectAreaWithFArea:nil withTarea:nil withFarea:1];
    //    _cityArray = [AREA selectAreaWithFArea:@"北京市" withTarea:nil withFarea:2];
    //    _areaArray = [AREA selectAreaWithFArea:@"北京市" withTarea:@"北京市" withFarea:3];
    
    //    UIImageView * image = (UIImageView*)[self.view viewWithTag:1000];
    //    image.transform = CGAffineTransformMakeRotation(M_PI_2);
    //    UIPickerView *education = [[UIPickerView alloc] init];
    //    ActionSheetPicker *actionSheet=[[ActionSheetPicker alloc]initWithTarget:self successAction:@selector(selectOK:) cancelAction:nil origin:education];
    //
    //    CGRect pickerFrame = CGRectMake(0, 40, actionSheet.viewSize.width, 216);
    //    [education setFrame:pickerFrame];
    //    education.delegate = self;
    //    education.dataSource = self;
    //    //pickview  左选择按钮的文字
    //    UIButton *leftBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    //    [leftBtn setTitle:@"取消"forState:UIControlStateNormal];
    //    [leftBtn addTarget:self action:@selector(pressLiftBtn) forControlEvents:UIControlEventTouchUpInside];
    //    leftBtn.frame=CGRectMake(0, 0, 50, 45);
    //    [leftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //    UIBarButtonItem *leftIte=[[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    //    //pickview    右选择按钮的文字
    //    UIButton *rightBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    //    [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
    //    rightBtn.frame=CGRectMake(0, 0, 50, 45);
    //    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //    UIBarButtonItem *rightIte=[[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    //    //将按钮添加到pickerview 上
    //    [actionSheet setDoneButton:rightIte];
    //    [actionSheet setCancelButton:leftIte];
    //    actionSheet.pickerView = education;
    //    actionSheet.title = @"地址选择";
    //    [actionSheet showActionSheetPicker];
    //    NSLog(@"点击的是地区选择");
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component

{
    NSInteger ss = [pickerView selectedRowInComponent:0];
    NSInteger dd = [pickerView selectedRowInComponent:1];
    
    switch (component) {
        case 0:
        {
            NSDictionary *dict = [_provienceArray objectAtIndex:row];
            NSString *code = MySetValue(dict, @"provinceName");
            if (code.length > 0)
            {
                _cityArray = [AREA selectAreaWithFArea:code withTarea:nil withFarea:2];
                [pickerView reloadComponent:1];
                dd = 0;
                NSDictionary *cityDict = [_cityArray objectAtIndex:dd];
                NSString * cityFcode = MySetValue(cityDict, @"cityName");
                if (cityFcode.length > 0)
                {
                    //                    _areaArray = [AREA selectAreaWithFArea:code withTarea:cityFcode withFarea:3];
                    //                    [pickerView reloadComponent:2];
                }
            }
        }
            break;
        case 1:
        {
            NSDictionary *dict = [_cityArray objectAtIndex:row];
            NSString *code = MySetValue(dict, @"cityName");
            
            NSDictionary * dics = [_provienceArray objectAtIndex:ss];
            NSString * str = MySetValue(dics, @"provinceName");
            NSLog(@"%@",code);
            
            NSLog(@"%@",str);
            if (code.length > 0) {
                //                _areaArray = [AREA selectAreaWithFArea:str withTarea:code withFarea:3];
                //                [pickerView reloadComponent:2];
            }
        }
            break;
        case 2:{
            
        }
            break;
            
        default:
            break;
    }
}

-(void)pressLiftBtn{
    
    UIImageView * image = (UIImageView*)[self.view viewWithTag:1000];
    image.transform = CGAffineTransformMakeRotation(0);
}

- (void)selectOK:(id)sender{
    
    UIPickerView *pick = sender;
    NSInteger row = [pick selectedRowInComponent:0];
    NSDictionary *dict = [_provienceArray objectAtIndex:row];
    NSString *provinceName = MySetValue(dict, @"provinceName");//省名称
    NSString *provinceCode = MySetValue(dict, @"provinceCode");//省编码
    
    NSInteger row1 = [pick selectedRowInComponent:1];
    NSDictionary *dict1 = [_cityArray objectAtIndex:row1];
    NSString *cityName = MySetValue(dict1, @"cityName");//市名称
    NSString *citycode = MySetValue(dict1, @"cityCode");//市编码
    
    //    NSInteger row2 = [pick selectedRowInComponent:2];
    //    NSDictionary *dict2 = [_areaArray objectAtIndex:row2];
    //    NSString *countyName = MySetValue(dict2, @"countyName");//县名称
    //    NSString * countycode = MySetValue(dict2, @"countyCode");//县编码
    
    [dic setObject:provinceCode forKey:@"provincecode"];//省编码
    [dic setObject:provinceName forKey:@"provincename"]; //省名称
    [dic setObject:citycode forKey:@"citycode"];//市编码
    [dic setObject:cityName forKey:@"cityname"]; //市名称
    //    [dic setObject:countycode forKey:@"countycode"];//县编码
    //    [dic setObject:countyName forKey:@"countyname"]; //县名称
    //    NSString * btnName = [NSString stringWithFormat:@"%@%@%@",provinceName,cityName,countyName];
    NSString *provinceName1 = [NSString stringWithFormat:@"%@",provinceName];
    NSString *cityName1 = [NSString stringWithFormat:@"%@",cityName];
    
    NSString * btnName = nil;
    if ([provinceName1 isEqualToString:cityName1]) {
        btnName = [NSString stringWithFormat:@"%@",provinceName1];
    }else{
        btnName = [NSString stringWithFormat:@"%@%@",provinceName1,cityName1];
    }
    
    NSLog(@"%@%@",dic[@"provincename"],dic[@"cityname"]);
    [_AreaBtn setTitle:btnName forState:UIControlStateNormal];
    UIImageView * image = (UIImageView*)[self.view viewWithTag:1000];
    image.transform = CGAffineTransformMakeRotation(0);
}

//  跳出的pickerview  显示列数
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

//显示pickerview 每个视图的行数
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component

{
    switch (component) {
        case 0:
            return _provienceArray.count;
            break;
        case 1:
            return _cityArray.count;
            break;
            //                case 2:
            //                    return _areaArray.count;
            //                    break;
        default:
            break;
    }
    return 0;
}

//显示每个pickerview  每行的具体内容
-(UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view

{
    UILabel *lable  = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
    lable.adjustsFontSizeToFitWidth = YES;
    lable.textAlignment = NSTextAlignmentCenter;
    
    switch (component) {
        case 0:{
            NSDictionary *dict = [_provienceArray objectAtIndex:row];
            lable.text = MySetValue(dict, @"provinceName");
        }
            break;
        case 1:{
            
            NSDictionary *dict = [_cityArray objectAtIndex:row];
            lable.text = MySetValue(dict, @"cityName");
        }
            break;
            
            //        case 2:{
            //
            //            NSDictionary *dict = [_areaArray objectAtIndex:row];
            //            lable.text = MySetValue(dict, @"countyName");
            //        }
            //            break;
        default:
            break;
    }
    return lable;
}

-(void)pressStoreBtn{
    
    __weak AddressVC * weekself = self;
    weekself.store = YES;
    NSString * dataProvincename = [NSString stringWithFormat:@"%@",self.dataSouce[@"provinceName"]];
    NSString * dicProvincename = [NSString stringWithFormat:@"%@",dic[@"provinceName"]];
    BOOL isOK = [dataProvincename isEqualToString:dicProvincename];
    
    NSString * phone = dic[@"phone"];
    NSString * contact = dic[@"contact"];
    NSString * address = dic[@"address"];
    //    NSString * unitname = dic[@"unitname"];
    NSString * area = [NSString stringWithFormat:@"%@%@",dic[@"provincename"],dic[@"cityname"]];
    NSMutableArray * arr = [[NSMutableArray alloc]init];
    [arr addObject:area];
    //    [arr addObject:unitname];
    [arr addObject:contact];
    [arr addObject:phone];
    [arr addObject:address];
    
    
    NSMutableArray * alertName = nil;
    if ([_addresstype isEqualToString:@"1"]) {
        //        alertName = @[@"您没有选择地址",@"您没有填写收车单位",@"您没有填写联系人",@"您没有填写联系电话",@"您没有填写详细地址"].mutableCopy;
        alertName = @[@"您没有选择地址",
                      @"您没有填写联系人",
                      @"您没有填写联系电话",
                      @"您没有填写详细地址"].mutableCopy;
        
    }
    else{
        //        alertName = @[@"您没有选择地址",@"您没有填写发车单位",@"您没有填写联系人",@"您没有填写联系电话",@"您没有填写详细地址"].mutableCopy;
        alertName = @[@"您没有选择地址",
                      @"您没有填写联系人",
                      @"您没有填写联系电话",
                      @"您没有填写详细地址"].mutableCopy;
        
    }
    
    for (int i = 0; i<4; i++) {
        NSString * string = arr[i];
        if (string.length == 0) {
            weekself.store = NO;
            [self createUIAlertController:alertName[i]];
            break;
        }
    }
    
    NSString * url = nil;
    
    if (![self.navtitle isEqualToString:@"2"]) {
        
        url = addressEdit_Url;
    }else{
        url = addressAdd_Url;
    }
    if (weekself.store) {
        if([Common validateUserPhone:phone]){
            
            
            if (self.CityListName.length >0) {
                url =[NSString stringWithFormat:@"%@?isVeneer=1",url];
            }
            
            [Com afPostRequestWithUrlString:url parms:dic finishedBlock:^(id responseObj) {
                
                NSMutableDictionary * dicObj =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];
                
                if ([dicObj[@"success"] boolValue]) {
                    if(dicObj[@"data"] != nil){
                        hud = [WKProgressHUD popMessage:@"保存成功" inView:self.view duration:0.5 animated:YES];
                        
                        NSMutableDictionary * addressObject = [[NSMutableDictionary alloc]init];
                        
                        if ([_navtitle isEqualToString:@"1"] ||[_navtitle isEqualToString:@"0"]) {
                            addressObject[@"id"] = dic[@"id"];
                        }else{
                            addressObject[@"id"] = dicObj[@"data"];
                        }
                        addressObject[@"address"] = dic[@"address"];
                        addressObject[@"addressType"] = dic[@"addresstype"];
                        addressObject[@"cityCode"] = dic[@"citycode"];
                        addressObject[@"cityName"] = dic[@"cityname"];
                        addressObject[@"provinceCode"] = dic[@"provincecode"];
                        addressObject[@"provinceName"] = dic[@"provincename"];
                        
                        addressObject[@"countyCode"] = dic[@"countycode"];
                        addressObject[@"countyName"] = dic[@"countyname"];
                        
                        //                        addressObject[@"unitName"] = dic[@"unitname"];
                        addressObject[@"phone"] = dic[@"phone"];
                        addressObject[@"contact"] = dic[@"contact"];
                        
                        if(dicObj[@"data"] != nil){
                            if( _callBackAddAddress){
                                _callBackAddAddress(addressObject, self.addresstype);
                            }
                            [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                [hud dismiss:YES];
                                
                                
                                if (self.CityListName.length == 0) {
                                    
                                    if (![self.backString isEqualToString:@"1"]) {
                                        
                                        for (UIViewController *controller in self.navigationController.viewControllers) {
                                            if ([controller isKindOfClass:[PlaceOrderVC class]]) {
                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"BackAddressDic" object:addressObject];
                                                
                                                [self.navigationController popToViewController:controller animated:YES];
                                            }
                                        }
                                        
                                    } else {
                                        [self.navigationController popViewControllerAnimated:YES];
                                    }
                                    
                                }else{
                                    
                                    if (![self.backString isEqualToString:@"1"]) {
                                        
                                        for (UIViewController *controller in self.navigationController.viewControllers) {
                                            if ([controller isKindOfClass:[CityDeliveryVC class]]) {
                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"CityBackAddressDic" object:addressObject];
                                                [self.navigationController popToViewController:controller animated:YES];
                                            }
                                        }
                                    } else {
                                        [self.navigationController popViewControllerAnimated:YES];
                                    }
                                }
                            });
                        }
                    }else{
                        
                        [self createUIAlertController:dicObj[@"message"]];
                    }
                }else{
                    [self createUIAlertController:dicObj[@"message"]];
                }
            } failedBlock:^(NSString *errorMsg) {
            }];
        }else{
            [self createUIAlertController:@"您输入的手机号有误"];
        }
    }
}

-(void)createBackUIAlertController:(NSString*)title{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[ChooseAddressVC class]]) {
                [self.navigationController popToViewController:controller animated:YES];
            }
        }
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)createUIAlertController:(NSString*)title

{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor *)color{
    
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentRight;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font)
    return label;
}

-(UIButton*)createBtn:(NSString*)title andTag:(int)tag{
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:littleBlackColor forState:UIControlStateNormal];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btn.tag = tag;
    return btn;
}
-(void)textFieldWithText:(UITextField *)textField{
    //500 501 502 503
    UITextField * field = (UITextField*)textField;
    switch (textField.tag) {
        case 499://发车单位/收车单位
            if ([self textField:field shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:@""]) {
                [dic setObject:field.text forKey:@"unitname"]; //送达单位、收车单位
            }
            break;
        case 500: //联系人
            if ([self textField:field shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:@""]) {
                [dic setObject:field.text forKey:@"contact"];
            }
            break;
        case 501://联系电话
            if ([self textField:field shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:@""]) {
                [dic setObject:field.text forKey:@"phone"];
            }
            break;
        case 502://详细地址
            if ([self textField:field shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:@""]) {
                [dic setObject:field.text forKey:@"address"];
            }
            break;
        default:
            break;
    }
}

-(UITextField*)createField:(NSString*)placeholder andTag:(int)tag andFont:(double)font{
    UITextField * field =[[UITextField alloc]init];
    field.frame = CGRectMake(0, 0, Main_Width-91, 50);
    field.delegate = self;
    field.userInteractionEnabled = YES;
    field.textAlignment = NSTextAlignmentLeft;
    field.keyboardType = UIKeyboardTypeDefault;
    //    field.returnKeyType = UIReturnKeySend;
    field.tag = tag;
    field.placeholder =placeholder;
    //    field.text = placeholder;
    field.textColor = littleBlackColor;
    field.font =Font(font);
    [field addTarget:self action:@selector(textFieldWithText:) forControlEvents:UIControlEventEditingChanged];
    return field;
}


//触摸方法
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    for (UITextField * field in self.view.subviews) {
        [field resignFirstResponder];
    }
}
//点击return按钮 键盘隐藏 这是协议中的方法
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //键盘隐藏 也就是让键盘取消第一响应者身份
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{  //string就是此时输入的那个字符textField就是此时正在输入的那个输入框返回YES就是可以改变输入框的值NO相反
    
    if ([string isEqualToString:@"\n"])  //按会车可以改变
    {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    switch (textField.tag) {
        case 499://发车单位/收车单位
            if ([toBeString length] > 20) {
                textField.text = [toBeString substringToIndex:20];
                [self createUIAlertController:@"单位输入不能超过20个字符"];
                return NO;
            }
            break;
        case 500: //联系人
            if ([toBeString length] > 10) {
                textField.text = [toBeString substringToIndex:10];
                [self createUIAlertController:@"联系人输入不能超过10个字符"];
                return NO;
            }
            break;
        case 501://联系电话
            if ([toBeString length] > 11) {
                textField.text = [toBeString substringToIndex:11];
                [self createUIAlertController:@"联系电话输入不能超过11个字符"];
                return NO;
            }
            break;
        case 502://详细地址
            if ([toBeString length] > 50) {
                textField.text = [toBeString substringToIndex:50];
                [self createUIAlertController:@"详细地址输入不能超过50个字符"];
                return NO;
            }
            break;
        default:
            break;
    }
    
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
