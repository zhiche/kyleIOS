//
//  PlaceOrderVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface PlaceOrderVC : NavViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>

@end
