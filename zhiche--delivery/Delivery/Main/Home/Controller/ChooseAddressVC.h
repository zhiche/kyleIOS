//
//  ChooseAddressVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/10.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface ChooseAddressVC : NavViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) NSString *addresstype; //地址类型(0:发车地址，1:送达地址)
@property (nonatomic,copy) void (^callBack)(NSMutableDictionary * AddressDic,NSString * addresstype); //地址类型(0:发车地址，1:送达地址)
@property (nonatomic,copy) void (^callRemoveBack)(NSString * address);


@property (nonatomic,strong) NSString * CityListName;
@property (nonatomic,strong) NSString * CityListCode;


@end
