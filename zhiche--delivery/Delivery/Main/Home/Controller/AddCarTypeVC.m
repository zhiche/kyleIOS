//
//  AddCarTypeVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/13.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "AddCarTypeVC.h"
#import <Masonry.h>
#import "Common.h"
#import "HistoryCarStyleVC.h"
#import "AddCarVinCell.h"
#import "CustomTextField.h"
@interface AddCarTypeVC ()
{
    UIImageView * nav;
    UIScrollView * scroll;
    UITableView * table;
    NSMutableArray * carVinArr;
    Common * Com;
    HistoryCarStyleVC *choiceVC;
    NSMutableArray * BackDataArr;
   

}
@property (nonatomic,strong) NSMutableDictionary * dataDic;
@property (nonatomic,strong) UIView * BackView;
@property (nonatomic,strong) UIView * backview;
@property (nonatomic,strong) UIButton * carNameBtn;
@property (nonatomic,strong) UILabel * carNumberLabel;
@property (nonatomic,strong) UILabel * prompt1;
@property (nonatomic,strong) UILabel * prompt2;
@property (nonatomic,strong) UITextField * CarPromptField;
@property (nonatomic,strong) NSMutableArray * fieldArr;
@property (nonatomic)  int CarNumber;


@end

@implementation AddCarTypeVC

- (instancetype)init

{
    self = [super init];
    if (self) {
        self.CityListName = [[NSString alloc]init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNav];
    [self Init];
    [self createScroll];
    [self createUI];

}

-(void)Init{
    
    _CarNumber = 1;
    carVinArr = [[NSMutableArray alloc]init];
    Com = [[Common alloc]init];
    choiceVC = [[HistoryCarStyleVC alloc]init];
    BackDataArr= [[NSMutableArray alloc]init];
    _fieldArr = [[NSMutableArray alloc]init];
    _dataDic = [[NSMutableDictionary alloc]init];
    [_dataDic setObject:@"" forKey:@"brandid"];
    [_dataDic setObject:@"" forKey:@"brandname"];
    [_dataDic setObject:@"" forKey:@"vehicleid"];
    [_dataDic setObject:@"" forKey:@"vehiclename"];
    [_dataDic setObject:@"1" forKey:@"vehiclecount"];
    [_dataDic setObject:@"" forKey:@"brandLogo"];
    
    NSMutableDictionary * dic1 = [[NSMutableDictionary alloc]init];
    [dic1 setObject:@"" forKey:@"vin"];
    [dic1 setObject:@"" forKey:@"color"];
    NSMutableArray * arr = [[NSMutableArray alloc]init];
    [arr addObject:dic1];
    [_dataDic setObject:arr forKey:@"vins"];
    
    NSMutableDictionary * dic2 = [[NSMutableDictionary alloc]init];
    [dic2 setValue:@"" forKey:@"vin"];
    [carVinArr addObject:dic2];
    
}
-(void)createNav{
    
    nav = [self createNav:@"增加车辆"];
    
    self.isTwo = NO;

//    UIButton * keepBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [keepBtn setTitle:@"保存" forState:UIControlStateNormal];
//    keepBtn.titleLabel.font = Font(FontOfSize14);
//    [keepBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
//    [nav addSubview:keepBtn];
//    [keepBtn mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(nav.mas_right).with.offset(-15*kWidth);
//        make.top.mas_equalTo(nav.mas_top).with.offset(15);
//        make.size.mas_equalTo(CGSizeMake(30*kWidth, 18*kHeight));
//    }];
//    UIButton * bigBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [bigBtn addTarget:self action:@selector(pressNavKeep) forControlEvents:UIControlEventTouchUpInside];
//    [nav addSubview:bigBtn];
//    [bigBtn mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(nav.mas_right).with.offset(-15*kWidth);
//        make.centerY.mas_equalTo(nav.mas_top).with.offset(25*kHeight);
//        make.size.mas_equalTo(CGSizeMake(60*kWidth, 30*kHeight));
//    }];
    
    [self.view addSubview:nav];
    UIButton * backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn addTarget:self action:@selector(pressBackKeep) forControlEvents:UIControlEventTouchUpInside];
    [nav addSubview:backBtn];
    [self.view addSubview:backBtn];
    [backBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(nav.mas_left);
        make.centerY.mas_equalTo(nav.mas_top).with.offset(15*kHeight);
        make.size.mas_equalTo(CGSizeMake(60*kWidth, 40*kHeight));
    }];
}

-(void)pressBackKeep{
    
//    if (BackDataArr.count==0) {
//
//    }else{
//        if (self.callBack) {
//            self.callBack(BackDataArr);
//        }
//    }
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)pressNavKeep{
    
    NSLog(@"保存 直接传dataDic里面的数据");
    BOOL isKeep = YES;
    NSString * brandname = [NSString stringWithFormat:@"%@",_dataDic[@"brandname"]];
    NSMutableArray * arr = _dataDic[@"vins"];
    for (int i=0; i<arr.count; i++) {
        if (brandname.length == 0) {
            [self createUIAlertController:@"请选择车型"];
            isKeep = NO;
            return;
        }
//        NSString * vin = arr[i][@"vin"];
//        if (![Common validateUserVin:vin]) {
//            
//            NSString * string = [NSString stringWithFormat:@"车架号(%@)输入错误",vin];
//            [self createUIAlertController:string];
//            isKeep = NO;
//            break;
//        }
       
        NSString * vin = arr[i][@"vin"];
        if (vin.length < 6 ) {
            NSString * string = [NSString stringWithFormat:@"车架号(%@)位数输入错误，请输入至少后6位车架号",vin];
            [self createUIAlertController:string];
            isKeep = NO;
            return;
        }
//        if (_CityListName.length==0) {
//            if (vin.length < 6 ) {
//                NSString * string = [NSString stringWithFormat:@"车架号(%@)位数输入错误",vin];
//                [self createUIAlertController:string];
//                isKeep = NO;
//                return;
//            }
// 
//        }else{
//            if (vin.length < 6 &&vin.length!=0) {
//                NSString * string = [NSString stringWithFormat:@"车架号(%@)位数输入错误",vin];
//                [self createUIAlertController:string];
//                isKeep = NO;
//                return;
//            }
//        }
//
        if (vin.length == 0) {
            NSString * string = [NSString stringWithFormat:@"vin输入有误"];
            [self createUIAlertController:string];
            isKeep = NO;
            return;
        }
        
    }
    
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    for (int i=0; i<arr.count; i++) {
        
        NSString * string = arr[i][@"vin"];
        [dic setObject:@"vin" forKey:string];
    }
    
    if ([[dic allKeys] count] != arr.count) {
        [self createUIAlertController:@"vin输入重复"];
        isKeep = NO;
    }
    
    
    
    
    
    if (isKeep) {
        NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:self.dataDic];
        [BackDataArr addObject:dic];

        if (self.callBack) {
            self.callBack(BackDataArr);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)createScroll{
    
    scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, Main_Width,Main_height-64*kHeight)];
    scroll.backgroundColor = GrayColor;
    scroll.contentOffset = CGPointMake(0, 0);
    scroll.contentSize = CGSizeMake(Main_Width, Main_height-64*kHeight);
    self.automaticallyAdjustsScrollViewInsets =NO;
    scroll.bounces = NO;
    scroll.showsHorizontalScrollIndicator = NO;
    scroll.showsVerticalScrollIndicator = NO;
    scroll.delegate = self;
    scroll.minimumZoomScale = 1.0;
    scroll.maximumZoomScale = 3.0;
    [self.view addSubview:scroll];
    [scroll mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(nav.mas_bottom).with.offset(0*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, Main_height-64*kHeight));
    }];
    _BackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width,Main_height-64*kHeight)];
    UITapGestureRecognizer * single = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
    single.numberOfTouchesRequired = 1;
    single.numberOfTapsRequired = 1;
    [_BackView addGestureRecognizer:single];

    _BackView.backgroundColor = GrayColor;

    [scroll addSubview:_BackView];
}

-(void)tapAction:(UITapGestureRecognizer*)tap{
    
    for (UITextField * field in _fieldArr) {
        [field resignFirstResponder];
    }

}
-(void)createUI{
    
    _backview = [[UIView alloc]init];
    _backview.backgroundColor = WhiteColor;
    [_BackView addSubview:_backview];
    [_backview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_BackView.mas_left).with.offset(0);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 129*kHeight));
        make.top.mas_equalTo(_BackView.mas_top);
    }];

    for (int i =0; i<2; i++) {
        
        UIView * viewline1 = [[UIView alloc]init];
        viewline1.backgroundColor = GrayColor;
        [_backview addSubview:viewline1];
        [viewline1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_backview.mas_left).with.offset(18*kHeight);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5*kHeight));
            make.top.mas_equalTo(_backview.mas_top).with.offset(43*kHeight*(i+1));
        }];
    }
    
    UILabel * label1 = [Com createUIlabel:@"选择车型" andFont:FontOfSize13 andColor:AddCarColor];
    [_backview addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_backview.mas_top).with.offset(21.5*kHeight);
    }];
    
    _carNameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_carNameBtn setTitle:@"请选择" forState:UIControlStateNormal];
    [_carNameBtn setTitleColor:FieldPlacerColor forState:UIControlStateNormal];
    _carNameBtn.titleLabel.font = Font(13);
    _carNameBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_carNameBtn addTarget:self action:@selector(pressCarNameBtn) forControlEvents:UIControlEventTouchUpInside];
    [_backview addSubview:_carNameBtn];
    [_carNameBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview.mas_left).with.offset(89*kWidth);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
        make.top.mas_equalTo(_backview.mas_top);
    }];
    UIImageView * RightImage= [[UIImageView alloc]init];
    RightImage.image = [UIImage imageNamed:@"common_list_arrows_more"];
    [_backview addSubview:RightImage];
    [RightImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_backview.mas_right).with.offset(-15*kWidth);
        make.centerY.mas_equalTo(_carNameBtn.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(8*kWidth, 13*kHeight));
    }];

    
    UILabel * label2 = [Com createUIlabel:@"汽车数量" andFont:FontOfSize13 andColor:AddCarColor];
    [_backview addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_backview.mas_top).with.offset(21.5*kHeight+43*kHeight);
    }];
    
    _carNumberLabel = [Com createUIlabel:@"1" andFont:FontOfSize13 andColor:AddCarColor];
    [_backview addSubview:_carNumberLabel];
    [_carNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(label2.mas_right).with.offset(20*kWidth);
        make.centerY.mas_equalTo(label2.mas_centerY);
    }];
    
    UIButton * addBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [addBtn setBackgroundImage:[UIImage imageNamed:@"Car+"] forState:UIControlStateNormal];
    addBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [addBtn addTarget:self action:@selector(pressAddBtn) forControlEvents:UIControlEventTouchUpInside];
    [_backview addSubview:addBtn];
    [addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_backview.mas_right).with.offset(-15*kWidth);
        make.centerY.mas_equalTo(_carNumberLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(43*kWidth, 23*kHeight));
    }];
    
    UIButton * reduceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [reduceBtn setBackgroundImage:[UIImage imageNamed:@"Car-"] forState:UIControlStateNormal];
    [reduceBtn addTarget:self action:@selector(pressReduceBtn) forControlEvents:UIControlEventTouchUpInside];
    reduceBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [_backview addSubview:reduceBtn];
    [reduceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(addBtn.mas_left).with.offset(-8*kWidth);
        make.centerY.mas_equalTo(addBtn.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(43*kWidth, 23*kHeight));
    }];

    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, 43*kHeight) style:UITableViewStylePlain];
    [table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table.scrollEnabled =NO;
    table.delegate = self;
    table.dataSource = self;
    [_backview addSubview:table];
    [table mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backview.mas_left);
        make.top.mas_equalTo(label2.mas_centerY).with.offset(22*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*kHeight));
    }];

    UIButton * keepBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [keepBtn setTitle:@"保存" forState:UIControlStateNormal];
    [keepBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
    [keepBtn addTarget:self action:@selector(pressNavKeep) forControlEvents:UIControlEventTouchUpInside];
    keepBtn.layer.cornerRadius = 5;
    keepBtn.layer.borderWidth = 0.5;
    keepBtn.layer.borderColor = YellowColor.CGColor;
    keepBtn.backgroundColor = YellowColor;
    [self.view addSubview:keepBtn];
    [keepBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(293*kWidth, 40*kHeight));
        make.top.mas_equalTo(table.mas_bottom).with.offset(30*kHeight);
    }];
}


-(void)pressAddBtn{
    
    _CarNumber++;
    _carNumberLabel.text = [NSString stringWithFormat:@"%d",_CarNumber];
    
    NSMutableDictionary * dic1 = [[NSMutableDictionary alloc]init];
    [dic1 setObject:@"" forKey:@"vin"];
    [dic1 setObject:@"" forKey:@"color"];
    [_dataDic[@"vins"] addObject:dic1];

//    if (CarNumber < 12) {
//
//    }else{
//        [self createUIAlertController:@"数量不能大于12"];
//    }
    NSString * string = [NSString stringWithFormat:@"%d",_CarNumber];
    [_dataDic setObject:string forKey:@"vehiclecount"];
    [self upUIFram:_CarNumber];

}
-(void)pressReduceBtn{
    
    if (_CarNumber>1) {
        _CarNumber --;
        _carNumberLabel.text = [NSString stringWithFormat:@"%d",_CarNumber];
        [_dataDic[@"vins"] removeLastObject];
    }else{
        [self createUIAlertController:@"数量不能小于1"];
    }
    NSString * string = [NSString stringWithFormat:@"%d",_CarNumber];
    [_dataDic setObject:string forKey:@"vehiclecount"];
    
    [self upUIFram:_CarNumber];
}


-(void)upUIFram:(int)number{
    
    [_backview mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(129*kHeight+(_CarNumber-1)*43*kHeight);
    }];
    
    if (number>3) {
        scroll.contentSize = CGSizeMake(Main_Width,Main_height-64*kHeight+43*(number-3)*kHeight);
    }else{
        scroll.contentSize = CGSizeMake(Main_Width,Main_height-64*kHeight);
    }
    
    if (number >6) {
        _BackView.frame = CGRectMake(0, 0, Main_Width, Main_height-64*kHeight+43*(number-6)*kHeight);
    }else{
        _BackView.frame = CGRectMake(0, 0, Main_Width, Main_height-64*kHeight);
    }

    
    [table mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(43*_CarNumber*kHeight);
    }];
    
    [table reloadData];
}

-(void)pressCarNameBtn{
    
    __weak AddCarTypeVC * weekself = self;
    choiceVC.callBack = ^(NSString *str1,NSString *str2,NSString *str3,NSString *str4,NSString* str5){
        /*
         str  车系ID
         str1 车型ID
         str2 车系名称
         str3 品牌名称
         */
        NSLog(@"%@  %@ %@ %@ %@",str1,str2,str3,str4,str5);
        NSString *string = [NSString stringWithFormat:@"%@-%@",str3,str4];
        NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
        [dic setObject:string forKey:@"brandname"];
        [dic setObject:str1 forKey:@"brandid"];
        [dic setObject:str2 forKey:@"vehicleid"];
        [dic setObject:@(weekself.CarNumber) forKey:@"vehiclecount"];
        [dic setObject:str4 forKey:@"vehiclename"];
        [dic setObject:str5 forKey:@"brandLogo"];
        [weekself.dataDic setObject:str1 forKey:@"brandid"];
        [weekself.dataDic setObject:str3 forKey:@"brandname"];
        [weekself.dataDic setObject:str2 forKey:@"vehicleid"];
        [weekself.dataDic setObject:str4 forKey:@"vehiclename"];
        [weekself.dataDic setObject:@(weekself.CarNumber) forKey:@"vehiclecount"];
        [weekself.dataDic setObject:str5 forKey:@"brandLogo"];
        
        [weekself.carNameBtn setTitle:string forState:UIControlStateNormal];
        [weekself.carNameBtn setTitleColor:AddCarColor forState:UIControlStateNormal];
    };
    [self.navigationController pushViewController:choiceVC animated:YES];
    

}

//保存并增加
/*
-(void)pressKeepBtn{
    
    
    NSLog(@"_dataDic====%@",_dataDic);
    
    BOOL isKeep = YES;
    NSString * brandname = [NSString stringWithFormat:@"%@",_dataDic[@"brandname"]];

    NSMutableArray * arr = _dataDic[@"vins"];
    for (int i=0; i<arr.count; i++) {
        
        if (brandname.length == 0) {
            [self createUIAlertController:@"请选择具体车辆"];
            isKeep = NO;
            break;
        }
        NSString * extvalue = arr[i][@"extvalue"];
        NSString * vin = arr[i][@"vin"];
        if (![Common validateUserVin:vin]) {
            [self createUIAlertController:@"车架号(vin)错误"];
            isKeep = NO;
            break;
        }
        if (extvalue.length ==0) {
            [self createUIAlertController:@"请输入汽车估值"];
            isKeep = NO;

            break;
        }
        if (![Common validateUserAge:extvalue]) {
            [self createUIAlertController:@"汽车估值输入错误"];
            isKeep = NO;
            break;

        }
    }
    
    if (isKeep) {
        NSMutableDictionary *dic = self.dataDic.copy;
        [BackDataArr addObject:dic];
        
        [self UIfresh];
        [self datafresh];
        [self upUIFram:CarNumber];
    }
}
*/
-(void)UIfresh{
    
    _CarPromptField.text = @"";
    _carNumberLabel.text = @"1";
    [_carNameBtn setTitle:@"请选择" forState:UIControlStateNormal];
    
    UIButton * btn1 = (UIButton*)[_backview viewWithTag:100];
    btn1.layer.borderColor = LineGrayColor.CGColor;
    [btn1 setTitleColor:littleBlackColor forState:UIControlStateNormal];
    btn1.backgroundColor = WhiteColor;
    
    UIButton * btn2 = (UIButton*)[_backview viewWithTag:200];
    btn2.layer.borderColor = LineGrayColor.CGColor;
    [btn2 setTitleColor:YellowColor forState:UIControlStateNormal];
    btn2.backgroundColor = WhiteColor;
    
    UIButton * btn3 = (UIButton*)[_backview viewWithTag:300];
    btn3.layer.borderColor = LineGrayColor.CGColor;
    [btn3 setTitleColor:littleBlackColor forState:UIControlStateNormal];
    btn3.backgroundColor = WhiteColor;
    
    UIButton * btn4 = (UIButton*)[_backview viewWithTag:400];
    btn4.layer.borderColor = LineGrayColor.CGColor;
    [btn4 setTitleColor:YellowColor forState:UIControlStateNormal];
    btn4.backgroundColor = WhiteColor;
    

}
-(void)datafresh{
    
    [_dataDic removeAllObjects];
    
    [_dataDic setObject:@"" forKey:@"brandid"];
    [_dataDic setObject:@"" forKey:@"brandname"];
    [_dataDic setObject:@"" forKey:@"vehicleid"];
    [_dataDic setObject:@"" forKey:@"vehiclename"];
    [_dataDic setObject:@"1" forKey:@"vehiclecount"];
    [_dataDic setObject:@"" forKey:@"brandLogo"];
    
    NSMutableDictionary * dic1 = [[NSMutableDictionary alloc]init];
    [dic1 setObject:@"" forKey:@"vin"];
    [dic1 setObject:@"" forKey:@"color"];
    [dic1 setObject:@"N" forKey:@"issecondhand"];
    [dic1 setObject:@"Y" forKey:@"ismobile"];
    [dic1 setObject:@"" forKey:@"extvalue"];
    
    NSMutableArray * arr = [[NSMutableArray alloc]init];
    [arr addObject:dic1];
    [_dataDic setObject:arr forKey:@"vins"];
    _CarNumber = 1;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)safeFieldWithText:(CustomTextField*)textField{
    
    NSString * string = [NSString stringWithFormat:@"%@",textField.text];
    
    if ([self textField:textField shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:@""]) {
        for (int i=0; i<[_dataDic[@"vins"] count]; i++) {
            [_dataDic[@"vins"][i] setValue:string forKey:@"extvalue"];
        }
    }
}

-(void)textFieldWithText:(UITextField *)textField{
    //500 501 502 503
    NSString * string = [NSString stringWithFormat:@"%@",textField.text];
    NSInteger index = textField.tag;

//    NSMutableArray * arr = _dataDic[@"vins"];
//
//    NSMutableArray * arrVin = [[NSMutableArray alloc]init];
//    for (int i=0; i<arr.count; i++) {
//        
//        [arrVin addObject:arr[i][@"vin"]];
//    }
//    if ([arrVin containsObject:textField.text]) {
//        
//        [self createUIAlertController:@"vin重复输入" andTextIndex:index];
//    }else{
//    }
    if ([self textField:textField shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:@""]) {
        [_dataDic[@"vins"][index] setValue:string forKey:@"vin"];
    }

}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *str=@"str";
    AddCarVinCell * cell =  [tableView dequeueReusableCellWithIdentifier:str];
    if (cell ==nil) {
        cell = [[AddCarVinCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
    }
    //        NSMutableArray * vin = _CarName[indexPath.section][@"vins"];
    //        cell.field.text = vin[indexPath.row][@"vin"];
    if (indexPath.row == [_dataDic[@"vins"] count]-1) {
        cell.viewline1.hidden = YES;
    }else{
        cell.viewline1.hidden = NO;
    }
    cell.field.tag = indexPath.row;
    if (_CityListName.length>0) {
        cell.field.placeholder = @"请输入车架号";
    }
    cell.field.text = _dataDic[@"vins"][indexPath.row][@"vin"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.field addTarget:self action:@selector(textFieldWithText:) forControlEvents:UIControlEventEditingChanged];
    [_fieldArr addObject:cell.field];
    return cell;
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 43*kHeight;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _CarNumber;
}

-(void)createUIAlertController:(NSString*)title

{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
          }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)createUIAlertController:(NSString*)title andTextIndex:(NSInteger)Index

{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        UITextField * textfield = (UITextField*)[self.view viewWithTag:Index];
        textfield.text = @"";
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}


-(UIButton*)createBtn:(NSString*)title andBorderColor:(UIColor*)BorderColor andFont:(CGFloat)Font andTitleColor:(UIColor*)TitleColor andBackColor:(UIColor*)BackColor {
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.layer.borderWidth = 0.5;
    btn.layer.cornerRadius = 5;
    btn.backgroundColor = BackColor;
    btn.layer.borderColor = BorderColor.CGColor;
    btn.titleLabel.font = Font(Font);
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:TitleColor forState:UIControlStateNormal];
    return btn;
}
-(CustomTextField*)createField:(NSString*)placeholder andTag:(NSInteger)tag andFont:(double)font{
    
    CustomTextField * field =[[CustomTextField alloc]init];
    field.frame = CGRectMake(120*kWidth, 0, Main_Width-300*kWidth, 43*kWidth);
    field.delegate = self;
    field.userInteractionEnabled = YES;
    field.textAlignment = NSTextAlignmentLeft;
    CGFloat width = field.frame.size.width;
    CGFloat height = field.frame.size.height;
    //控制placeHolder的位置
    [field placeholderRectForBounds:CGRectMake(width/2, 0, width, height)];
    

    return field;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //键盘隐藏 也就是让键盘取消第一响应者身份
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{  //string就是此时输入的那个字符textField就是此时正在输入的那个输入框返回YES就是可以改变输入框的值NO相反
    
    if ([string isEqualToString:@"\n"])  //按会车可以改变
    {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
//    
//    if (textField.tag <20) {
//        
//        
    
        if ([toBeString length] > 17) {
            textField.text = [toBeString substringToIndex:17];
            //            [self createUIAlertController:@"vin输入不能超过17个字符"];
            return NO;
        }

        if (_CityListName.length>0 && [toBeString length] > 17) {
            
            textField.text = [toBeString substringToIndex:17];
            return NO;

        }else{
            
            if ([toBeString length] > 17) {
                textField.text = [toBeString substringToIndex:17];
                //            [self createUIAlertController:@"vin输入不能超过17个字符"];
                return NO;
            }
//        }
        
    }
    return YES;
}


@end
