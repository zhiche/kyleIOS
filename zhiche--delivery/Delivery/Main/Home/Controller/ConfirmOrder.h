//
//  ConfirmOrder.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/6/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface ConfirmOrder :  NavViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>

@property (nonatomic,strong) NSMutableArray * dataSouceArr;
@property (nonatomic,strong) NSString * quoteid;
@end
