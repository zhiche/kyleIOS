//
//  NullDataView.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/26.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NullDataView : UIView
-(id)initWithFrame:(CGRect)frame andTitle:(NSString *)string andImageName:(NSString*)imageName;
@property (nonatomic,strong) UILabel *label;
@property (nonatomic,strong) NSString * imageName;

@end
