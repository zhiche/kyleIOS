//
//  AboutAppVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/11/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "AboutAppVC.h"
#import "Common.h"
#import "RootViewController.h"
#import <Masonry.h>
#import "HomeWebVC.h"
@interface AboutAppVC ()
{
    UIImageView * nav;
    Common * com;
    RootViewController * TabBar;

}

@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSMutableArray *webUrlArray;
@property (nonatomic,strong) UITableView *tableView;


@end

@implementation AboutAppVC

- (void)viewDidLoad {
    [super viewDidLoad];
    nav = [self createNav:@"关于"];
    [self.view addSubview:nav];
    com = [[Common alloc]init];
    TabBar = [RootViewController defaultsTabBar];
    
    self.view.backgroundColor = GrayColor;
    
    self.dataArray = [NSMutableArray arrayWithObjects:@"版本介绍",@"联系我们", nil];
    [self initSubViews];
    
    [self initVS];
    
}

-(void)initVS{
    
    UILabel * label = [com createUIlabel:@"@知车科技" andFont:FontOfSize10 andColor:YellowColor];
    [self.view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.view.mas_bottom).with.offset(-10*kHeight);
    }];
    
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];

    NSString * stringVS = [NSString stringWithFormat:@"V%@",app_Version];

    UILabel * label1 = [com createUIlabel:stringVS andFont:FontOfSize10 andColor:YellowColor];
    [self.view addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(label.mas_top).with.offset(-5*kHeight);
    }];
    UIImageView * image = [[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"service_tubiao"];
    [self.view addSubview:image];
    
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(label1.mas_top).with.offset(-5*kHeight);
        make.size.mas_equalTo(CGSizeMake(43*kWidth, 15*kHeight));
    }];

}

-(void)viewWillAppear:(BOOL)animated{
    [TabBar setTabBarHidden:YES];
}

-(void)initSubViews
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64+15, Main_Width, Main_height-64) style:UITableViewStylePlain];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = GrayColor;
    [self.view addSubview:self.tableView];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return   self.dataArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *string = @"installCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:string];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:string];
    }
    
    cell.textLabel.text = self.dataArray[indexPath.row];
    cell.textLabel.font = Font(13);
    cell.textLabel.textColor = littleBlackColor;
    cell.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"personal_arrow"]];
    

    UIView * viewline = [[UIView alloc]init];
    viewline.backgroundColor = LineGrayColor;
    [cell.contentView addSubview:viewline];
    [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(cell.contentView.mas_right);
        make.bottom.mas_equalTo(cell.contentView.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
    
    return cell;
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return cellHeight;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.row == 0) {
        HomeWebVC * webvc = [[HomeWebVC alloc]init];
        webvc.webUrl = versions_introduce_webURL;
        webvc.titleLabel = @"版本介绍";
        [self.navigationController pushViewController:webvc animated:YES];
    }else{
        HomeWebVC * webvc = [[HomeWebVC alloc]init];
        webvc.webUrl = contact_us_webURL;
        webvc.titleLabel = @"联系我们";
        [self.navigationController pushViewController:webvc animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
