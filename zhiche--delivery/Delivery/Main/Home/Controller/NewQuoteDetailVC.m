//
//  NewQuoteDetailVC.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/9/20.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "NewQuoteDetailVC.h"
#import "Model.h"
#import "Common.h"
#import "CarInfoCell.h"
#import "Common.h"
#import <Masonry.h>
@interface NewQuoteDetailVC ()
{
    Common * com;
}
@property (nonatomic,strong) UILabel * priceAllLabel;
@property (nonatomic,strong) UIView * priceAllLine;
@property (nonatomic,strong) UILabel * activityLabel;


@end

@implementation NewQuoteDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    com = [[Common alloc]init];
    
    self.priceView.frame = CGRectMake(0, CGRectGetMaxY(self.tableView.frame) + 10, screenWidth, cellHeight * 5 + 5 +43*kHeight);
    self.scrollView.contentSize = CGSizeMake(screenWidth, screenHeight + 86*kHeight);
    
    _priceAllLabel = [com createUIlabel:@"" andFont:FontOfSize12 andColor:carScrollColor];
    _priceAllLine = [[UIView alloc]init];
    _activityLabel = [com createUIlabel:@"" andFont:FontOfSize12 andColor:YellowColor];


    [self initDataSource];

    
    [self.rotateButton addTarget:self action:@selector(rotateButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [self addSwipeRecognizer];
    

}
#pragma mark - UIGestureRecognizerDelegate

- (void)addSwipeRecognizer
{
    // 初始化手势并添加执行方法
    UISwipeGestureRecognizer *swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(return)];
    
    // 手势方向
    swipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    
    // 响应的手指数
    swipeRecognizer.numberOfTouchesRequired = 1;
    
    // 添加手势
    [[self view] addGestureRecognizer:swipeRecognizer];
}

#pragma mark 返回上一级
- (void)return
{

    [self.navigationController popViewControllerAnimated:YES];

}


//按钮旋转
-(void)rotateButtonAction:(UIButton *)sender
{
    if (self.flag) {
        
        [UIView animateWithDuration:0.2 animations:^{
            self.arrowImageView.transform = CGAffineTransformMakeRotation(0);
            
            
        } completion:^(BOOL finished) {
            self.flag = NO;
            
            [self changeViewFrameWithInteger:0];
            
        }];
    } else {
        [UIView animateWithDuration:0.2 animations:^{
            self.arrowImageView.transform = CGAffineTransformMakeRotation(M_PI_2);
            
        } completion:^(BOOL finished) {
            self.flag = YES;
            
            [self changeViewFrameWithInteger:1];
        }];
    }
}
//根据table的展开来改变页面布局
-(void)changeViewFrameWithInteger:(NSInteger)integer
{
 
    //1、计算每个cell的高度 存储到self.heightArray中
    for (int i = 0;  i < self.dataArr.count; i++) {
        
//        Model *model = [[Model alloc]init];
//        model = self.dataArr[i];
//        
//        NSString *string = model.vinList[0][@"vin"];
//        for (int i = 1; i < model.vinList.count ; i ++) {
//            
//            string = [string stringByAppendingString:[NSString stringWithFormat:@"\n%@ ",model.vinList[i][@"vin"]]];
//            
//        }
//        
//        model.contact = string;
//        
//        CGFloat height = [CarInfoCell cellHeightWithModel:model];
        
        [self.heightArray addObject:@(56 * kHeight )];
    }
    
    
    [self.tableView reloadData];
    
    //2、cell展开时候计算总高度  关闭时候高度为0
    CGFloat height = 0;
    
    if (integer == 0) {
        
        [self.heightArray removeAllObjects];
        
    }
    
    for (int j = 0; j < self.heightArray.count; j++) {
        
        height += [self.heightArray[j] floatValue];
        
    }
    
    //3、改变view的frame
    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, screenWidth, cellHeight * 3 + height);
    
    self.priceView.frame = CGRectMake(0, CGRectGetMaxY(self.tableView.frame) + 10, screenWidth, self.priceView.frame.size.height);
    
    
    self.scrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(self.priceView.frame) + 20 + 64 +86);
    
}


-(void)initDataSource
{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",order_detail,self.orderId];
    [Common requestWithUrlString:urlString contentType:application_json finished:^(id responseObj) {
        
        
        NSLog(@"%@",responseObj);
        
        if ([responseObj[@"success"] boolValue]) {
            
            self.dataDictionary = [NSMutableDictionary dictionaryWithDictionary:responseObj[@"data"]];
            
            [self getValueString];
            
        }
        
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
}


//赋值
-(void)getValueString
{
    
    if ([self.dataDictionary[@"svehicles"] isEqual:[NSNull null]]) {
        
    } else {
        
        for (int i = 0; i < [self.dataDictionary[@"svehicles"] count]; i ++) {
            Model *model = [[Model alloc]init];
            [model setValuesForKeysWithDictionary:self.dataDictionary[@"svehicles"][i]];
            
            [self.dataArr addObject:model];
        }
        [self.tableView reloadData];
    }
   
    //订单编号
    self.numberLabel.text = [self backString:self.dataDictionary[@"orderCode"]];
    //起始地
    self.startLabel.text = [self backString:self.dataDictionary[@"departCityName"]];
    //目的地
    self.endLabel.text = [self backString:self.dataDictionary[@"receiptCityName"]];
    //出发时间
    self.startDate.text = [self backString:self.dataDictionary[@"deliveryDate"]];
    //结束时间
    self.endDateLabel.text = [self backString:self.dataDictionary[@"arriveDate"]];
    
    //汽车状态
    NSString *string ;
       if ([self.dataDictionary[@"isSencondhand"] boolValue]) {
        string = @"二手车";
        
    } else {
        string = @"新车";
    }
    
    if ([self.dataDictionary[@"isMobile"] boolValue]) {
        
        string = [string stringByAppendingString:@"   能动"];
        
    } else {
        string = [string stringByAppendingString:@"   不能动"];
        
    }

    
    self.carStautsLabel.text = string;
    //车辆总数
    int number = 0;
    
    if ([self.dataDictionary[@"svehicles"] isEqual:[NSNull null]]) {
        
    } else {
    
    for (int i = 0; i < [self.dataDictionary[@"svehicles"] count]; i ++) {
        
        number += [self.dataDictionary[@"svehicles"][i][@"amount"] intValue];
        
    }
    }
    
    self.amountLabel.text = [NSString stringWithFormat:@"%d辆",number];
    
    
    for (int i = 0; i < [self.dataDictionary[@"feeDetail"] count]; i ++) {
        if ([self.dataDictionary[@"feeDetail"][i][@"feeName"] isEqualToString:@"提车费"]) {
            //提车费
            self.totalLabel.text = [self backMoney:self.dataDictionary[@"feeDetail"][i][@"cost"]];
        }
        
        if ([self.dataDictionary[@"feeDetail"][i][@"feeName"] isEqualToString:@"交车费"]) {
            //交车费
            self.payLabel.text = [self backMoney:self.dataDictionary[@"feeDetail"][i][@"cost"]];
            
        }
        
        if ([self.dataDictionary[@"feeDetail"][i][@"feeName"] isEqualToString:@"保险费"]) {
            //保险费
            self.unpayLabel.text = [self backMoney:self.dataDictionary[@"feeDetail"][i][@"cost"]];
        }
        
        if ([self.dataDictionary[@"feeDetail"][i][@"feeName"] isEqualToString:@"运输费"]) {
            //运输费
            self.scoreLabel.text = [self backMoney:self.dataDictionary[@"feeDetail"][i][@"cost"]];
        }
    }
    

    float moneyAll = [self.dataDictionary[@"discount"][@"cost"] floatValue];
    UILabel * allMoney = [com createUIlabel:[NSString stringWithFormat:@"￥%.2f",moneyAll] andFont:FontOfSize14 andColor:YellowColor];
    [self.priceView addSubview:allMoney];
    [allMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.priceView.mas_right).with.offset(-15*kHeight);
        make.centerY.mas_equalTo(self.priceView.mas_bottom).with.offset(-21.5*kHeight);
    }];
    NSString * isdiscount = [NSString stringWithFormat:@"%@",self.dataDictionary[@"discount"][@"discount"]];
    NSLog(@"self.dataDictionary==%@",self.dataDictionary);
    //优惠
    if ([ isdiscount isEqualToString:@"1"]) {
        
        float total = [self.dataDictionary[@"orderCost"] floatValue];
        _priceAllLabel.text = [NSString stringWithFormat:@"￥%.2f",total];
        [self.priceView addSubview:_priceAllLabel];
        [_priceAllLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(allMoney.mas_left).with.offset(-16*kHeight);
            make.centerY.mas_equalTo(allMoney.mas_centerY);
        }];
        
        _priceAllLine.backgroundColor = BlackColor;
        [_priceAllLabel addSubview:_priceAllLine];
        [_priceAllLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_priceAllLabel.mas_left);
            make.right.equalTo(_priceAllLabel.mas_right);
            make.centerY.mas_equalTo(_priceAllLabel.mas_centerY);
            make.height.mas_equalTo(0.5);
        }];
        _activityLabel.text =  [NSString stringWithFormat:@"%@",self.dataDictionary[@"discount"][@"desc"]];
        [self.scrollView addSubview:_activityLabel];
        [_activityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.scrollView.mas_centerX);
            make.top.mas_equalTo(self.priceView.mas_bottom).with.offset(9*kHeight);
        }];
    }
    


}

-(NSString *)backString:(NSString *)string
{
    
    if ([string isEqual:[NSNull null]]) {
        return @"";
    } else {
        return string;
    }
}

-(NSString *)backMoney:(NSString *)string
{
    NSString   *str = [NSString stringWithFormat:@"%.2f",[string floatValue]];
    
    if ([str isEqual:[NSNull null]]) {
        return @"";
    } else {
        return [NSString stringWithFormat:@"￥%@",str];
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
