//
//  HomeWebVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/11/25.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface HomeWebVC : NavViewController


@property (nonatomic,strong) NSString * titleLabel;
@property (nonatomic,strong) NSString * webUrl;

@end
