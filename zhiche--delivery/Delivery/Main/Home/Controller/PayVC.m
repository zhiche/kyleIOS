//
//  PayVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/6/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "PayVC.h"
#import <Masonry.h>
#import "Common.h"
#import <Pingpp.h>
#define kUrlScheme      @"demoapp001" // 这个是你定义的 URL Scheme，支付宝、微信支付、银联和测试模式需要。
#define kUrlWX  @"wx5f0250600950a712"
@interface PayVC ()
{
    UIView * nav;
    Common * com;
}
@property (nonatomic,strong) UIButton * ZFBbtn;
@property (nonatomic,strong) UIButton * WXbtn;
@property (nonatomic,strong) UIButton * PayBtn;

@end

@implementation PayVC
- (instancetype)init

{
    self = [super init];
    if (self) {
        self.orderid = [[NSString alloc]init];
        self.quoteid = [[NSString alloc]init];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    nav = [self createNav:@"确认报价"];
    [self.view addSubview:nav];
    com = [[Common alloc]init];
    [self createUI];
}
-(void)createUI{
    
    
    UILabel * address = [com createUIlabel:@"支付方式" andFont:14.0 andColor:RGBACOLOR(149, 149, 149, 1)];
    [self.view addSubview:address];
    [address mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).with.offset(14*kWidth);
        make.top.mas_equalTo(nav.mas_bottom).with.offset(10*kHeight);
    }];
    
    
    _ZFBbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_ZFBbtn addTarget:self action:@selector(pressZFBbtn:) forControlEvents:UIControlEventTouchUpInside];
    [_ZFBbtn setTitle:@"支付宝支付" forState:UIControlStateNormal];
    [_ZFBbtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _ZFBbtn.titleLabel.font = Font(14);
//    ZFBbtn.layer.borderWidth = 0.8;
//    ZFBbtn.layer.cornerRadius = 5;
    [self.view addSubview:_ZFBbtn];
    [_ZFBbtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.equalTo(address.mas_bottom).with.offset(10*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 53*kHeight));
    }];
    
    UIView * Hline1 =[[UIView alloc]init];
    Hline1.backgroundColor = RGBACOLOR(149, 149, 149, 1);
    [self.view addSubview:Hline1];
    [Hline1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(_ZFBbtn.mas_bottom).with.offset(5*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
    _WXbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_WXbtn addTarget:self action:@selector(pressWXbtn:) forControlEvents:UIControlEventTouchUpInside];
    [_WXbtn setTitle:@"微信支付" forState:UIControlStateNormal];
    _WXbtn.titleLabel.font = Font(14);
    [_WXbtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //    ZFBbtn.layer.borderWidth = 0.8;
    //    ZFBbtn.layer.cornerRadius = 5;
    [self.view addSubview:_WXbtn];
    [_WXbtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.equalTo(Hline1.mas_bottom).with.offset(5*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 53*kHeight));
    }];
    
    UIView * Hline2 =[[UIView alloc]init];
    Hline2.backgroundColor = RGBACOLOR(149, 149, 149, 1);
    [self.view addSubview:Hline2];
    [Hline2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(_WXbtn.mas_bottom).with.offset(5*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    UILabel * payMoney = [com createUIlabel:@"支付订金:¥273.00元" andFont:16.0 andColor:[UIColor redColor]];
    [self.view addSubview:payMoney];
    [payMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).with.offset(-14*kWidth);
        make.top.mas_equalTo(Hline2.mas_bottom).with.offset(15*kHeight);
    }];
    UIView * Hline3 =[[UIView alloc]init];
    Hline3.backgroundColor = RGBACOLOR(149, 149, 149, 1);
    [self.view addSubview:Hline3];
    [Hline3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(payMoney.mas_bottom).with.offset(15*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
     _PayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_PayBtn addTarget:self action:@selector(pressPaybtn:) forControlEvents:UIControlEventTouchUpInside];
    [_PayBtn setTitle:@"立即支付" forState:UIControlStateNormal];
    [_PayBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _PayBtn.backgroundColor = [UIColor greenColor];
    _PayBtn.layer.borderWidth = 0.8;
    _PayBtn.layer.cornerRadius = 5;
    [self.view addSubview:_PayBtn];
    [_PayBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.equalTo(Hline3.mas_bottom).with.offset(100*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width*0.75, 43*kHeight));
    }];

}
-(void)pressZFBbtn:(UIButton *)sender{
    
    _WXbtn.backgroundColor = [UIColor clearColor];
    if (sender.selected) {
        _ZFBbtn.backgroundColor = [UIColor clearColor];
        sender.selected = NO;
    }else{
        _ZFBbtn.backgroundColor = [UIColor grayColor];
        sender.selected = YES;
        _WXbtn.selected = NO;
    }
}
-(void)pressWXbtn:(UIButton *)sender{
    
    _ZFBbtn.backgroundColor = [UIColor clearColor];
    if (sender.selected) {
        _WXbtn.backgroundColor = [UIColor clearColor];
        sender.selected = NO;
    }else{
        _WXbtn.backgroundColor = [UIColor grayColor];
        sender.selected = YES;
        _ZFBbtn.selected = NO;

    }
}
-(void)pressPaybtn:(UIButton *)sender{
    
    
    NSString * urlString = nil;
    NSString * pathString = nil;
    if (_ZFBbtn.selected) {
        
        urlString = kUrlScheme;
        pathString = Order_alipay;
    }
    else{
        urlString = kUrlWX;
        pathString = Order_wx;
    }
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    [dic setObject:self.orderid forKey:@"orderid"];
    
    NSLog(@"%@",pathString);
    [com afPostRequestWithUrlString:pathString parms:dic finishedBlock:^(id responseObj) {
        
        NSMutableDictionary * dicObj =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];
        NSData * data = [NSJSONSerialization dataWithJSONObject:dicObj[@"data"] options:NSJSONWritingPrettyPrinted error:nil];
        NSString* charge = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        [Pingpp createPayment:charge
               viewController:self
                 appURLScheme:urlString
               withCompletion:^(NSString *result, PingppError *error) {
                   if ([result isEqualToString:@"success"]) {
                       NSLog(@"支付成功%@",result);
                       // 支付成功
                   } else {
                       // 支付失败或取消
                       NSLog(@"Error: code=%lu msg=%@", error.code, [error getMsg]);
                   }
               }];
        
    } failedBlock:^(NSString *errorMsg) {
    }];




    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
