//
//  ServiceCenterVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/11/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ServiceCenterVC.h"
#import "Common.h"
#import "RootViewController.h"
#import <Masonry.h>
#import "HomeWebVC.h"
@interface ServiceCenterVC ()
{
    UIImageView * nav;
    Common * com;
    RootViewController * TabBar;
}

@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSMutableArray *imagesArray;
@property (nonatomic,strong) NSMutableArray *webUrlArray;

@property (nonatomic,strong) UITableView *tableView;


@end

@implementation ServiceCenterVC

- (void)viewDidLoad {
    [super viewDidLoad];

    nav = [self createNav:@"客户中心"];
    [self.view addSubview:nav];
    TabBar = [RootViewController defaultsTabBar];
    self.view.backgroundColor = GrayColor;


    self.dataArray = [NSMutableArray arrayWithObjects:@"客服电话",@"车辆运输说明",@"运输服务协议",@"软件使用协议及隐私政策", nil];
    self.imagesArray = [NSMutableArray arrayWithObjects:@"service_call",@"service_say",@"service_xieyi",@"service_yinsi", nil];
    [self initSubViews];
}

-(void)viewWillAppear:(BOOL)animated{
    [TabBar setTabBarHidden:YES];
}


-(void)initSubViews
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64+15, Main_Width, Main_height-64) style:UITableViewStylePlain];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = GrayColor;
    [self.view addSubview:self.tableView];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return   self.dataArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *string = @"installCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:string];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:string];
    }
    
    cell.textLabel.text = self.dataArray[indexPath.row];
    cell.textLabel.font = Font(13);
    cell.textLabel.textColor = littleBlackColor;
    //        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator; //显示最右边的箭头
    
    cell.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"personal_arrow"]];
    
    cell.imageView.image = [UIImage imageNamed:self.imagesArray[indexPath.row]];

    
    UIView * viewline = [[UIView alloc]init];
    viewline.backgroundColor = LineGrayColor;
    [cell.contentView addSubview:viewline];
    [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(cell.contentView.mas_right);
        make.bottom.mas_equalTo(cell.contentView.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    
    
    return cell;
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return cellHeight;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.row == 0) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iphoneNumber]];
    }else if (indexPath.row == 1){
        HomeWebVC * webvc = [[HomeWebVC alloc]init];
        webvc.webUrl = transport_explain_webURL;
        webvc.titleLabel = @"车辆运输说明";
        [self.navigationController pushViewController:webvc animated:YES];
    }else if (indexPath.row == 2){
        HomeWebVC * webvc = [[HomeWebVC alloc]init];

        webvc.webUrl = transport_agreement_webURL;
        webvc.titleLabel = @"运输服务协议";
        [self.navigationController pushViewController:webvc animated:YES];
    
    }else if (indexPath.row == 3){
        HomeWebVC * webvc = [[HomeWebVC alloc]init];

        webvc.webUrl = software_agreement_webURL;
        webvc.titleLabel = @"软件使用协议及隐私政策";
        [self.navigationController pushViewController:webvc animated:YES];

    }else if (indexPath.row == 4){
        HomeWebVC * webvc = [[HomeWebVC alloc]init];

        webvc.webUrl = @"";
        webvc.titleLabel = @"用户指南";
        [self.navigationController pushViewController:webvc animated:YES];

    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
