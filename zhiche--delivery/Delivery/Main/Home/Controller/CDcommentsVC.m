//
//  CDcommentsVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/11/15.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "CDcommentsVC.h"
#import <Masonry.h>
#import "Common.h"
@interface CDcommentsVC ()
{
    UIImageView * nav;
    Common * com;
    

}
@property (nonatomic,strong) NSString * comment;
@property (nonatomic,strong) UILabel * textNumber;
@end

@implementation CDcommentsVC


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.IsHave = [[NSString alloc]init];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    nav = [self createNav:@"订单备注"];
    [self.view addSubview:nav];
    self.view.backgroundColor = WhiteColor;
    com = [[Common alloc]init];
    _textNumber = [com createUIlabel:@"剩余140字" andFont:FontOfSize12 andColor:BlackColor];
    
    
    UIButton * keepBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [keepBtn setTitle:@"保存" forState:UIControlStateNormal];
    [keepBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
    keepBtn.titleLabel.font = Font(FontOfSize14);
    //    BillDetaile.backgroundColor = [UIColor cyanColor];
    [keepBtn addTarget:self action:@selector(pressKeepBtn) forControlEvents:UIControlEventTouchUpInside];
    [nav addSubview:keepBtn];
    [keepBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(nav.mas_right).offset(-18);
        make.size.mas_equalTo(CGSizeMake(70*kWidth, 40*kHeight));
        make.centerY.mas_equalTo(nav.mas_centerY);
    }];
    keepBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    
    
    UITextView * field =[[UITextView alloc]init];
    //    field.frame = CGRectMake(94*kWidth, 0, Main_Width-200*kWidth, 43*kWidth);
    field.frame = CGRectMake(14*kWidth, 64*kHeight, Main_Width-28*kWidth, 200*kWidth);
    field.delegate = self;
    field.userInteractionEnabled = YES;
    
    field.backgroundColor = GrayColor;
    field.font = Font(FontOfSize12);
    field.textAlignment = NSTextAlignmentLeft;
    field.returnKeyType = UIReturnKeyDone;
    if (_IsHave.length >0) {
        field.text = _IsHave;
        _comment = _IsHave;
    }else{
        field.text = @"关于订单的额外信息，请在这里留言（限140字）。";
    }
    [self.view addSubview:field];
    
    [self.view addSubview:_textNumber];
//    _textNumber.backgroundColor = [UIColor cyanColor];
    [_textNumber mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(field.mas_right).offset(-5);
        make.bottom.mas_equalTo(field.mas_bottom).offset(-5);

    }];

    
}

-(void)pressKeepBtn{
    
    NSLog(@"保存");
    if (self.callCommentBack) {

        self.callCommentBack(_comment);
    }

    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:@"关于订单的额外信息，请在这里留言（限140字）。"]) {
        
        textView.text = @"";
        
    }
    
}

-(void)textViewDidChange:(UITextView *)textView{
    
    
    NSInteger number = [textView.text length];
    _comment = textView.text;
    if (number > 140) {
        
        number = 140;
        textView.text = [textView.text substringToIndex:number];
        [self createUIAlertController:@"留言限140字"];
    }
    _textNumber.text = [NSString stringWithFormat:@"剩余%ld字",140-number];
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (textView.text.length == 0) {
        
        textView.text = @"关于订单的额外信息，请在这里留言（限140字）。";
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createUIAlertController:(NSString*)title
{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark UITextViewDelegate

//如果输入超过规定的字数50，就不再让输入
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ( [ @"\n" isEqualToString: text])
    {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            [textView resignFirstResponder];

        }];
        
        return  NO;
    }
    else
    {
        return YES;
    }
}



@end
