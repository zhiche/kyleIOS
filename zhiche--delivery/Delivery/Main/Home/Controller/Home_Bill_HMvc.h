//
//  Home_Bill_HMvc.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/12/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface Home_Bill_HMvc : NavViewController<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>

@end
