//
//  AddressVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/10.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface AddressVC : NavViewController  <UITextFieldDelegate,UIActionSheetDelegate,UIAlertViewDelegate,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) NSString * addresstype;  //0:发货地址，1:收货地址
@property (nonatomic,strong) NSString * navtitle; //标题  0 1 2
@property (nonatomic,strong) NSString *backString;//判断返回哪个页面

@property (nonatomic,strong) NSString * CityListName;
@property (nonatomic,strong) NSString * CityListCode;

@property (nonatomic,strong) NSMutableDictionary * dataSouce;
@property (nonatomic,copy) void (^callBackAreaId)(NSString *str1,NSString *str2,NSString *str3,NSString *str4,NSString *str5,NSString *str6);
@property (nonatomic,copy) void (^callBackAddAddress)(NSMutableDictionary * AddressDic,NSString * addresstype); //地址类型(0:发车地址，1:送达地址)

@end
