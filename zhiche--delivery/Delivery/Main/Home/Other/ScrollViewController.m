//
//  ScrollViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/14.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//



#import "ScrollViewController.h"
#import <Masonry.h>
//#import "Common.h"
#import "UIImageView+WebCache.h"
@interface ScrollViewController ()
{
    UIScrollView*Scroll;
    UILabel * Number;
//    Common * com;
    NSMutableArray * pageNumberArr;
    int number;
}
@end

@implementation ScrollViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    com = [[Common alloc]init];
    
    
    [self creatScroll];
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.imageArr = [[NSArray alloc]init];
        self.indexNumber = 0;
    }
    return self;
}

-(void)creatScroll{
    
    
    self.view.backgroundColor =[UIColor blackColor];
    UIButton * backBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.backgroundColor = [UIColor whiteColor];
    [backBtn setBackgroundImage:[UIImage imageNamed:@"details_photo_back"] forState:UIControlStateNormal];
    backBtn.frame = CGRectMake(0, 0, 50, 50);
    [backBtn addTarget:self action:@selector(pressBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).with.offset(30);
        make.size.mas_equalTo(CGSizeMake(35, 22));
        make.left.mas_equalTo(self.view.mas_left).with.offset(20);
    }];
    
    UIButton * DownBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [DownBtn setBackgroundImage:[UIImage imageNamed:@"product_details_photo_icon_download"] forState:UIControlStateNormal];
    DownBtn.frame = CGRectMake(0, 0, 50, 50);
    [DownBtn addTarget:self action:@selector(DownBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:DownBtn];
    DownBtn.backgroundColor = [UIColor clearColor];
    [DownBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).with.offset(30);
        make.size.mas_equalTo(CGSizeMake(35, 35));
        make.right.mas_equalTo(self.view.mas_right).with.offset(-20);
    }];
    
    
    
    pageNumberArr = [[NSMutableArray alloc]init];
    Number = [[UILabel alloc]init];
    for (int i=0; i<self.imageArr.count; i++) {
        NSString* page =[NSString stringWithFormat:@"%d/%lu",i+1,(unsigned long)self.imageArr.count];
        [pageNumberArr  addObject:page];
    }
    
    number = self.indexNumber;
    Number.text = pageNumberArr[self.indexNumber];
    Number.textColor = [UIColor whiteColor];
    Number.font = Font(FontOfSize17);
    
    [self.view addSubview:Number];
    
    
    Scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 85,Main_Width,Main_height - 85 * 2)];
    Scroll.backgroundColor = [UIColor blackColor];
    Scroll.contentSize = CGSizeMake(screenWidth * self.imageArr.count,425 * kHeight);
    Scroll.contentOffset = CGPointMake(self.indexNumber*Main_Width,0);
    Scroll.bounces = NO;
    Scroll.showsHorizontalScrollIndicator = NO;
    Scroll.showsVerticalScrollIndicator = NO;
    Scroll.pagingEnabled = YES;
    Scroll.delegate = self;
    
    
    
    for (int i = 0; i<self.imageArr.count; i++) {
        UIImageView * image =[[UIImageView alloc]init];
        //                              WithFrame:CGRectMake(i*Main_Width,0, Scroll.frame.size.width, 425*CYheight)];
        [image sd_setImageWithURL:self.imageArr[i]];
        
        //        NSStringFromCGSize(image.image.size);
        UITapGestureRecognizer * single = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction)];
        //设置手指根数
        single.numberOfTouchesRequired = 1;
        //设置点击次数
        single.numberOfTapsRequired = 1;
        
        
        image.userInteractionEnabled = YES;
        [image addGestureRecognizer:single];
        [Scroll addSubview:image];
        [image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(Scroll.mas_left).with.offset(i*Main_Width+Main_Width/2);
            make.centerY.equalTo(Scroll.mas_centerY);
            make.width.mas_equalTo(Main_Width);
            make.height.mas_equalTo(Main_height - 85 * 2);
        }];
        
    }
    self.automaticallyAdjustsScrollViewInsets =NO;
    [self.view addSubview:Scroll];
    [Number mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.top.mas_equalTo(Scroll.mas_bottom).with.offset(42*kHeight);
    }];
}



-(void)tapAction{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}
-(void)pressBtn{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

-(void)DownBtn{
    
    UIImageView * imageView = [[UIImageView alloc]init];
    [imageView sd_setImageWithURL:[NSURL URLWithString:self.imageArr[number]]];
    
    
    UIImageWriteToSavedPhotosAlbum(imageView.image, self, @selector(imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:), nil);
}


- (void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    NSString *message = @"呵呵";
//    if (!error) {
//        message = @"成功保存到相册";
//        
//        UIImageView * backImage = [com createUIImage:@"保存成功" andDelay:1.5f];
//        [self.view addSubview:backImage];
//        [backImage mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerX.mas_equalTo(self.view.mas_centerX);
//            make.centerY.mas_equalTo(self.view.mas_centerY);
//        }];
//        
//        
//        
//    }else
//    {
//        
//        message = [error description];
//        UIImageView * backImage = [com createUIImage:message andDelay:1.0f];
//        [self.view addSubview:backImage];
//        [backImage mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerX.mas_equalTo(self.view.mas_centerX);
//            make.centerY.mas_equalTo(self.view.mas_centerY);
//        }];
//        
//    }
    NSLog(@"message is %@",message);
}
-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    // 名字的H
    CGFloat nameH = size.height;
    // 名字的W
    CGFloat nameW = size.width;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font)
    return label;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGPoint point = scrollView.contentOffset;
    number = point.x / Main_Width;
    Number.text = pageNumberArr[number];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

