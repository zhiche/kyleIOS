//
//  ScrollViewController.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/14.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScrollViewController : UIViewController<UIScrollViewDelegate>

@property (nonatomic,retain) NSArray * imageArr;

@property (nonatomic,assign) int indexNumber;

@end
