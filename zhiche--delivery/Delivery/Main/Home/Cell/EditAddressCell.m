//
//  EditAddressCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/6/27.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "EditAddressCell.h"
#import <Masonry.h>

@implementation EditAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
        
        UIView * viewline = [[UIView alloc]init];
        viewline.backgroundColor = LineGrayColor;
        [self.contentView addSubview:viewline];
        [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView.mas_top).with.offset(76*kHeight);
            make.left.mas_equalTo(self.contentView.mas_left);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        }];
        _backview = [[UIView alloc]init];
        _backview.backgroundColor = GrayColor;
        [self.contentView addSubview:_backview];
        [_backview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.contentView.mas_bottom).with.offset(1);

            make.left.mas_equalTo(self.contentView.mas_left);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 10*kHeight));
        }];
        
        _CityName = [self createUIlabel:@"" andFont:FontOfSize13 andColor:BlackTitleColor];
        _StoreName = [self createUIlabel:@"" andFont:FontOfSize13 andColor:BlackTitleColor];
        _AddressName = [self createUIlabel:@"" andFont:11 andColor:littleBlackColor];
        _PeopleName = [self createUIlabel:@"" andFont:11 andColor:littleBlackColor];
        _iphone = [self createUIlabel:@"" andFont:11 andColor:YellowColor];

        _leftImage = [[UIImageView alloc]init];
        _leftImage.image = [UIImage imageNamed:@"btn_unselect"];
//        [self.ClickBtn setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateSelected];
//        [self.ClickBtn setImage:[UIImage imageNamed:@"btn_unselect"] forState:UIControlStateNormal];
//        self.ClickBtn.selected = YES;
//        [self.ClickBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
//        [self.ClickBtn addTarget:self action:@selector(pressClickBtn:) forControlEvents:UIControlEventTouchUpInside];
//        [backprice addSubview:self.ClickBtn];

        [self.contentView addSubview:_leftImage];
        _MRbtn = [self createBtn:@"设为默认地址"];
        _MRbtn.titleLabel.font = Font(11);

        _MRbtn.layer.borderWidth = 0;
        _BJbtn = [self createBtn:@"编辑"];
        _BJbtn.backgroundColor = rearrangeBtnColor;
        [_BJbtn setTitleColor:YellowColor forState:UIControlStateNormal];
        _BJbtn.layer.borderColor = YellowColor.CGColor;
        _BJbtn.layer.borderWidth = 0.5;
        _BJbtn.layer.cornerRadius = 5;

        _SCbtn = [self createBtn:@"删除"];
        _SCbtn.backgroundColor = GrayColor;
        [_SCbtn setTitleColor:carScrollColor forState:UIControlStateNormal];
        _SCbtn.layer.borderColor = RGBACOLOR(186, 186, 186, 1).CGColor;
        _SCbtn.layer.borderWidth = 0.5;
        _SCbtn.layer.cornerRadius = 5;

        
        [self.contentView addSubview:_CityName];
        [self.contentView addSubview:_StoreName];
        [self.contentView addSubview:_AddressName];
        [self.contentView addSubview:_PeopleName];
        [self.contentView addSubview:_iphone];
        [self.contentView addSubview:_MRbtn];
        [self.contentView addSubview:_BJbtn];
        [self.contentView addSubview:_SCbtn];
        
        [_CityName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).with.offset(18*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_top).with.offset(20*kHeight);
        }];
        [_StoreName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_CityName.mas_right).with.offset(10*kWidth);
            make.centerY.mas_equalTo(_CityName.mas_centerY);
        }];
        
        [_AddressName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_CityName.mas_left);
            make.centerY.mas_equalTo(self.contentView.mas_top).with.offset(40*kHeight);
            make.width.mas_equalTo(Main_Width*0.7);
        }];
        [_PeopleName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_CityName.mas_left).with.offset(0);
            make.centerY.mas_equalTo(self.contentView.mas_top).with.offset(59*kHeight);
        }];
        [_iphone mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_PeopleName.mas_right).with.offset(14*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_top).with.offset(59*kHeight);
        }];
        [_MRbtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).with.offset(14*kWidth);
            make.centerY.mas_equalTo(self.SCbtn.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(130*kWidth, 30*kHeight));
        }];
        [_BJbtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.SCbtn.mas_centerY);
            make.right.mas_equalTo(self.SCbtn.mas_left).with.offset(-12*kWidth);
            make.size.mas_equalTo(CGSizeMake(58*kWidth, 23*kHeight));
        }];
        [_SCbtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).with.offset(-15*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_top).with.offset(97.5*kHeight);
            make.size.mas_equalTo(CGSizeMake(58*kWidth, 23*kHeight));
        }];
        [_leftImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(18*kWidth);
            make.centerY.mas_equalTo(_SCbtn.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(12*kWidth, 12*kHeight));
        }];
    }
    return self;
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentLeft;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}

-(NewBtn*)createBtn:(NSString*)title {
    
    NewBtn * btn = [NewBtn buttonWithType:UIButtonTypeCustom];
    btn.layer.borderWidth = 0.5;
    btn.layer.cornerRadius = 2;
    [btn setTitle:title forState:UIControlStateNormal];
    btn.titleLabel.font = Font(FontOfSize13);
    [btn setTitleColor:BlackColor forState:UIControlStateNormal];
    return btn;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
