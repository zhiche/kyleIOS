//
//  QueryShippingCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/8/22.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QueryShippingCell : UITableViewCell

@property (nonatomic,strong) UILabel * nameLable;
@property (nonatomic,strong) UILabel * timeLable;
@property (nonatomic,strong) UILabel * priceLable;


-(void)showInfo:(NSDictionary *)dic_info ;

@end
