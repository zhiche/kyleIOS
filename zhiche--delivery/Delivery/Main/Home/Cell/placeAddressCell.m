//
//  placeAddressCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/14.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "placeAddressCell.h"

@implementation placeAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
        
        _field =[[CustomTextField alloc]init];
        _field.frame = CGRectMake(9*kWidth, 0, Main_Width, 43*kWidth);
        _field.delegate = self;
        _field.userInteractionEnabled = YES;
        _field.textAlignment = NSTextAlignmentLeft;
        CGFloat width = _field.frame.size.width;
        CGFloat height = _field.frame.size.height;
        //控制placeHolder的位置
        [_field placeholderRectForBounds:CGRectMake(0*kWidth, 0, width, height)];
        [self.contentView addSubview:_field];
        
    }
    return self;
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
