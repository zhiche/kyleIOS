//
//  CarShipCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "CarShipCell.h"
#import <Masonry.h>
@implementation CarShipCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
        _SmallImage = [[UIImageView alloc]init];
        _SmallImage.backgroundColor = [UIColor cyanColor];
        _CarName = [self createUIlabel:@"比亚迪－速锐" andFont:FontOfSize14 andColor:BlackColor];
        _CarNumber = [self createUIlabel:@"3辆" andFont:FontOfSize14 andColor:BlackColor];
        [self.contentView addSubview:_SmallImage];
        [self.contentView addSubview:_CarNumber];
        [self.contentView addSubview:_CarName];
        [_SmallImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).with.offset(14*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(15*kWidth, 15*kHeight));
        }];
        [_CarName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_SmallImage.mas_right).with.offset(25*kWidth);
            make.centerY.mas_equalTo(_SmallImage.mas_centerY);
        }];
        [_CarNumber mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).with.offset(-20*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
        }];
    }
    return self;
}
-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentCenter;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}
@end















