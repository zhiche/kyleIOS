//
//  QuoteCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/28.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "QuoteCell.h"
#import <Masonry.h>
@implementation QuoteCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){


//        self.view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 225*kHeight)];
        self.view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 195*kHeight)];

        self.contentView.backgroundColor = GrayColor;
        self.view.backgroundColor = WhiteColor;
        [self.contentView addSubview:self.view];

        UIView * headerView1 = [[UIView alloc]init];
        headerView1.backgroundColor = headerBackViewColor;
//        headerView1.backgroundColor = RedColor;
        [self.contentView addSubview:headerView1];
        [headerView1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.view.mas_left);
            make.top.mas_equalTo(self.view.mas_top);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 44*kHeight));
        }];

        
        self.number = [self createUIlabel:@"订单号:" andFont:FontOfSize12 andColor:littleBlackColor];
        self.orderNumber = [self createUIlabel:@"" andFont:FontOfSize12 andColor:littleBlackColor];
//        self.timeImage = [[UIImageView alloc]init];
//        self.timeImage.image = [UIImage imageNamed:@"quote_cell_time"];
//        
//        self.time = [self createUIlabel:@"" andFont:FontOfSize12 andColor:headerTitleColor];
        self.time.textColor = RGBACOLOR(254, 110, 19, 1);
        [headerView1 addSubview:self.number];
        [headerView1 addSubview:self.orderNumber];
//        [headerView1 addSubview:self.time];
//        [headerView1 addSubview:self.timeImage];
        [self.number mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(headerView1.mas_left).with.offset(18*kWidth);
            make.top.mas_equalTo(headerView1.mas_top).with.offset(15*kHeight);
        }];
        [self.orderNumber mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.number.mas_right).with.offset(10*kWidth);
            make.top.mas_equalTo(headerView1.mas_top).with.offset(15*kHeight);
        }];
//        [self.time mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.mas_equalTo(headerView1.mas_right).with.offset(-14*kWidth);
//            make.top.mas_equalTo(headerView1.mas_top).with.offset(15*kHeight);
//        }];
//        [self.timeImage mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.mas_equalTo(self.time.mas_left).with.offset(-7*kWidth);
//            make.centerY.mas_equalTo(self.time.mas_centerY);
//            make.size.mas_equalTo(CGSizeMake(16*kWidth, 16*kHeight));
//        }];

//        self.scrollView = [[UIScrollView alloc]init];
//        self.scrollView.contentSize = CGSizeMake(screenWidth + 150*2*kWidth, 20*kHeight);
//        self.scrollView.alwaysBounceHorizontal = YES;
//        self.scrollView.showsHorizontalScrollIndicator = NO;
//        
//        self.carStyle = [self createUIlabel:@"" andFont:FontOfSize12 andColor:carScrollColor];
//        [self.scrollView addSubview:self.carStyle];
        self.carTypeAndNumber = [self createUIlabel:@"" andFont:FontOfSize14 andColor:littleBlackColor];
        //省市县
        self.startAddress = [self createUIlabel:@"" andFont:FontOfSize14 andColor:littleBlackColor];
        //发车单位 收车单位  长沙芒果库
        self.startDetailAddress = [self createUIlabel:@"" andFont:FontOfSize10 andColor:littleBlackColor];
        self.startDetailAddress.numberOfLines = 2;
        self.startTimeL = [self createUIlabel:@"" andFont:FontOfSize10 andColor:littleBlackColor];
        self.endAddress = [self createUIlabel:@"" andFont:FontOfSize14 andColor:littleBlackColor];
        //湖南益阳宝马4S店
        self.endDetailAddress = [self createUIlabel:@"" andFont:FontOfSize10 andColor:littleBlackColor];
        self.endDetailAddress.numberOfLines = 2;
        self.endTimeL = [self createUIlabel:@"" andFont:FontOfSize10 andColor:littleBlackColor];
        [self.view addSubview:self.startAddress];
        [self.view addSubview:self.startDetailAddress];
        [self.view addSubview:self.startTimeL];
        [self.view addSubview:self.endAddress];
        [self.view addSubview:self.endDetailAddress];
        [self.view addSubview:self.endTimeL];

        UIImageView * imageLogo = [[UIImageView alloc]init];
        imageLogo.image = [UIImage imageNamed:@"Quote_Cell_logo"];
        [self.view addSubview:imageLogo];
        [imageLogo mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.view.mas_centerX);
//            make.centerY.mas_equalTo(self.view.mas_top).with.offset(90*kHeight);
            make.centerY.mas_equalTo(self.view.mas_top).with.offset(70*kHeight);
        }];
        
        
        [self.startTimeL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.view.mas_left).with.offset(53*kHeight);
            make.top.mas_equalTo(self.startAddress.mas_bottom).with.offset(9*kHeight);
        }];
        [self.endTimeL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.view.mas_right).with.offset(-56*kHeight);
            make.centerY.mas_equalTo(self.startTimeL.mas_centerY);
        }];
        
        [self.startAddress mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.startTimeL.mas_centerX);
            make.top.mas_equalTo(self.view.mas_top).with.offset(59*kHeight);
            make.width.mas_equalTo(90*kWidth);
        }];
        
        [self.startDetailAddress mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.startAddress.mas_centerX);
            make.top.mas_equalTo(self.startAddress.mas_bottom).with.offset(9*kHeight);

        }];

        [self.endAddress mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.endTimeL.mas_centerX);
            make.centerY.mas_equalTo(self.startAddress.mas_centerY);
        }];
        [self.endDetailAddress mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.endAddress.mas_centerX);
            make.top.mas_equalTo(self.endAddress.mas_bottom).with.offset(9*kHeight);
            make.width.mas_equalTo(90*kWidth);

        }];
        
        UIView * viewline = [[UIView alloc]init];
        viewline.backgroundColor = LineGrayColor;
        [self.view addSubview:viewline];
        [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.view.mas_top).with.offset(45*kHeight);
            make.left.mas_equalTo(self.view.mas_left);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        }];

        
        _Btn2 = [self createBtn:@"取消订单" andbackcolor:GrayColor andbordercolor:GrayColor andtitlecolor:carScrollColor];
        
//        [_Btn2 setTitle:@"取消订单" forState:UIControlStateNormal];
//        [_Btn2 setTitleColor:carScrollColor forState:UIControlStateNormal];
//        _Btn2.backgroundColor =GrayColor;
//        _Btn2.layer.borderColor = GrayColor.CGColor;
//
        
        UIView * headerView2 = [[UIView alloc]init];
        headerView2.backgroundColor = headerBackViewColor;
        [self.view addSubview:headerView2];
        [headerView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.view.mas_left);
            make.bottom.mas_equalTo(self.view.mas_bottom);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 44*kHeight));
        }];
        
//        _Btn3 = [self createBtn:@"查看详情" andbackcolor:YellowColor andbordercolor:YellowColor andtitlecolor:WhiteColor];
        _line1=[[UIView alloc]init];
        _line1.backgroundColor = LineGrayColor;
        [self.view addSubview:_line1];
        _line2=[[UIView alloc]init];
        _line2.backgroundColor = LineGrayColor;
        [self.view addSubview:_line2];
        [headerView2 addSubview:_Btn2];
//        [headerView2 addSubview:_Btn3];
        [self.view addSubview:self.carTypeAndNumber];
        
//        [_Btn3 mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.mas_equalTo(self.view.mas_right).with.offset(-14*kWidth);
//            make.size.mas_equalTo(CGSizeMake(70*kWidth, 30*kHeight));
//            make.top.mas_equalTo(self.scrollView.mas_bottom).with.offset(8*kHeight);
//        }];


//        _quoteCounts = [self createUIlabel:@"(0)" andFont:11.0 andColor:WhiteColor];
//        [headerView2 addSubview:_quoteCounts];
//        [_quoteCounts mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.mas_equalTo(headerView2.mas_right).with.offset(-10*kWidth);
//            make.centerY.mas_equalTo(_Btn3.mas_centerY);
//        }];
//
        

  }
    return self;
}

-(void)showInfo:(NSDictionary *)dic_info{
    
 
//    {
//        actualCost = "<null>";
//        arriveDate = "2016-08-04";
//        deliveryDate = "2016-08-04";
//        departCityName = "\U5317\U4eac\U5e02";
//        departProvinceName = "<null>";
//        departUnit = "\U53d1\U8f66\U5355\U4f4d";
//        detail = "\U4fdd\U65f6\U63771\U8f86";
//        driverInfo = "<null>";
//        feeDetail =     {
//            extractcost = 0;
//            insurance = 0;
//            returncost = 0;
//            shippingfee = 0;
//            total = 0;
//        };
//        id = 66;
//        isDeliv = "<null>";
//        isPick = "<null>";
//        orderCode = O761081147704328192;
//        paidCost = "<null>";
//        payStatus = 10;
//        payStatusText = "\U672a\U4ed8\U6b3e";
//        postTimeRemaining = 0;
//        quoteCounts = 0;
//        receiptCityName = "\U9f50\U9f50\U54c8\U5c14\U5e02";
//        receiptPics =     (
//        );
//        receiptProvinceName = "<null>";
//        receiptUnit = Shouchedanwei;
//        receivePics =     (
//        );
//        status = 10;
//        statusText = "\U5df2\U53d1\U5e03";
//        unpaidCost = "<null>";
//        userId = "<null>";
    
    
    self.orderNumber.text = [NSString stringWithFormat:@"%@", dic_info[@"orderCode"]];
    
    
    NSString * startAddress = [NSString stringWithFormat:@"%@",dic_info[@"departCityName"]];
    self.startAddress.text = startAddress;//省市县
//    self.startDetailAddress.text = [NSString stringWithFormat:@"%@",dic_info[@"departUnit"]];//发车单位
    self.startTimeL.text = [NSString stringWithFormat:@"%@", dic_info[@"deliveryDate"]];//发车时间
    NSString * endAddress = [NSString stringWithFormat:@"%@",dic_info[@"receiptCityName"]];
    self.endAddress.text = endAddress;//省市县
//    self.endDetailAddress.text =  [NSString stringWithFormat:@"%@",dic_info[@"receiptUnit"]];//收车单位
    self.endTimeL.text = [NSString stringWithFormat:@"%@",dic_info[@"arriveDate"]];//到达时间
    

    int  status =  [dic_info[@"status"] intValue];
    if (status == 10) {
        [_Btn2 setTitle:@"取消订单" forState:UIControlStateNormal];
        [_Btn2 setTitleColor:carScrollColor forState:UIControlStateNormal];
        _Btn2.backgroundColor =GrayColor;
        _Btn2.layer.borderColor = GrayColor.CGColor;
    }else{
//        _Btn2 = [self createBtn:@"重新发布" andbackcolor:RGBACOLOR(254, 242, 227, 1) andbordercolor:YellowColor andtitlecolor:YellowColor];

        _Btn2.hidden = YES;
//        [_Btn2 setTitle:@"重新发布" forState:UIControlStateNormal];
//        [_Btn2 setTitleColor:YellowColor forState:UIControlStateNormal];
//        _Btn2.backgroundColor = rearrangeBtnColor;
//        _Btn2.layer.borderColor = YellowColor.CGColor;
    }
    [_line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).with.offset(0);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
//        make.top.mas_equalTo(self.view.mas_top).with.offset(135*kHeight);
          make.top.mas_equalTo(self.view.mas_top).with.offset(105*kHeight);

    }];
    [self.carTypeAndNumber mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(_line1.mas_bottom).offset(21.5*kHeight);
        make.left.mas_equalTo(self.view.mas_left).with.offset(18*kWidth);
    }];
    [_line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).with.offset(0);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        make.top.mas_equalTo(self.carTypeAndNumber.mas_centerY).with.offset(21.5*kHeight);
    }];

 
//    NSMutableArray * carstyle = dic_info[@"vehicles"];
    NSString * carName = dic_info[@"detail"];
//    for (int i=0; i<carstyle.count; i++) {
//        if (i==carstyle.count-1) {
//            carName = [carName stringByAppendingString:[NSString stringWithFormat:@"%@-%@-%@辆 ",carstyle[i][@"brandName"],carstyle[i][@"vehicleName"],carstyle[i][@"amount"]]];
//        }else{
//            carName = [carName stringByAppendingString:[NSString stringWithFormat:@"%@-%@-%@辆, ",carstyle[i][@"brandName"],carstyle[i][@"vehicleName"],carstyle[i][@"amount"]]];
//        }
//    }
    self.carTypeAndNumber.text = carName;
    self.carStyle.text = carName;
    self.carStyle.textAlignment = NSTextAlignmentLeft;
    [self.carStyle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.carTypeAndNumber.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(self.carTypeAndNumber.mas_centerY);
    }];
//    self.scrollView.contentSize = CGSizeMake(self.carStyle.frame.size.width, 20*kHeight);

//    NSString * stringlength = [NSString stringWithFormat:@"%@",dic_info[@"quoteCounts"]];
//    int number = [stringlength intValue];
//        [_Btn3 setTitle:[NSString stringWithFormat:@"查看详情"] forState:UIControlStateNormal];
//        [_Btn3 mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.right.mas_equalTo(self.view.mas_right).with.offset(-14*kWidth);
//            make.size.mas_equalTo(CGSizeMake(101*kWidth, 30*kHeight));
//            make.top.mas_equalTo(self.scrollView.mas_bottom).with.offset(8*kHeight);
//        }];

    
    [_Btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.view.mas_right).with.offset(-14*kWidth);
        make.size.mas_equalTo(CGSizeMake(70*kWidth, 30*kHeight));
        make.top.mas_equalTo(self.line2.mas_bottom).with.offset(8*kHeight);
    }];
}

-(NewBtn*)createBtn:(NSString*)title andbackcolor:(UIColor*)backcolor andbordercolor:(UIColor*)bordercolor andtitlecolor:(UIColor*)titlecolor {
    
    NewBtn * btn = [NewBtn buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:titlecolor forState:UIControlStateNormal];
    btn.titleLabel.font = Font(FontOfSize14);
    btn.backgroundColor = backcolor;

    btn.layer.borderWidth = 1;
    btn.layer.cornerRadius = 5;
    btn.layer.borderColor = bordercolor.CGColor;
//    btn.layer.borderColor = RGBACOLOR(149, 149, 149, 1).CGColor;
    return btn;
}


-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentCenter;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}





- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
