//
//  SmallCarTableCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "SmallCarTableCell.h"
#import <Masonry.h>

@implementation SmallCarTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
        _CarUnitPrice = [self createUIlabel:@"" andFont:FontOfSize14 andColor:BlackColor];
        _CarAllPrice = [self createUIlabel:@"" andFont:FontOfSize14 andColor:BlackColor];
        _CarLabel = [self createUIlabel:@"" andFont:FontOfSize14 andColor:BlackColor];
        [self.contentView addSubview:_CarAllPrice];
        [self.contentView addSubview:_CarUnitPrice];
        [self.contentView addSubview:_CarLabel];
        [_CarLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_left).with.offset(60*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
        }];
        [_CarUnitPrice mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.contentView.mas_centerX);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);

        }];
        [_CarAllPrice mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).with.offset(-20*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
        }];
    }
    return self;
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentRight;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}

@end
