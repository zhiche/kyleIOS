//
//  QuoteCarListCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/20.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "QuoteCarListCell.h"
#import <Masonry.h>
@implementation QuoteCarListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){

        self.carName = [self createUIlabel:@"" andFont:FontOfSize13 andColor:AddCarColor];
        self.carNumber = [self createUIlabel:@"" andFont:FontOfSize13 andColor:AddCarColor];
        [self.contentView addSubview:self.carName];
        [self.contentView addSubview:self.carNumber];
        [self.carName mas_makeConstraints:^(MASConstraintMaker *make) {

            make.left.mas_equalTo(self.contentView.mas_left).with.offset(18*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
        }];
        [self.carNumber mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right).with.offset(-15*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
        }];
    }
    return self;
}

-(void)showInfo:(NSDictionary *)dic_info{
    
    

    self.carName.text = [NSString stringWithFormat:@"%@",[self backString:dic_info[@"brandName"]]];
    self.carNumber.text = [NSString stringWithFormat:@"%@辆",[self backString:dic_info[@"amount"]]];

}




-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    // 名字的H
    CGFloat nameH = size.height;
    // 名字的W
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentRight;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}


-(NSString *)backString:(NSString *)string
{
    //    NSString * str = [NSString stringWithFormat:@"%@",string];
    if ([string isEqual:[NSNull null]]) {
        return @"";
    } else {
        return string;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
