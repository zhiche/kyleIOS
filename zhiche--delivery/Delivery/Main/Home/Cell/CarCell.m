//
//  CarCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/9.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "CarCell.h"
#import <Masonry.h>
@implementation CarCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
//@property (nonatomic,strong) UIImageView * SmallImage;
//@property (nonatomic,strong) UILabel * CarName;
//@property (nonatomic,strong) UIButton * ReduceBtn;
//@property (nonatomic,strong) UIButton * AddBtn;
//@property (nonatomic,strong) UILabel * Number;

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        

        self.contentView.backgroundColor = WhiteColor;
        _SmallImage = [[UIImageView alloc]init];
        [self.contentView addSubview:_SmallImage];
        [_SmallImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(20*kWidth);
            make.size.mas_equalTo(CGSizeMake(33*kWidth, 33*kHeight));
        }];
        _CarName = [self createUIlabel:@"宝马-宝马系列2系" andFont:FontOfSize13 andColor:AddCarColor];
        [self.contentView addSubview:_CarName];
        [_CarName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.left.mas_equalTo(_SmallImage.mas_right).with.offset(12*kWidth);
            make.width.mas_equalTo(Main_Width*0.65);
        }];
        UIImageView * image = [[UIImageView alloc]init];
        image.image = [UIImage imageNamed:@"delete"];
        [self.contentView addSubview:image];
        [image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.right.mas_equalTo(self.contentView.mas_right).with.offset(-15*kWidth);
            make.size.mas_equalTo(CGSizeMake(17*kWidth, 17*kHeight));
        }];

        _deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.contentView addSubview:_deleteBtn];
        [_deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.right.mas_equalTo(self.contentView.mas_right);
            make.size.mas_equalTo(CGSizeMake(50*kWidth, 30*kHeight));
        }];
        
        _CarNumber = [self createUIlabel:@"1辆" andFont:FontOfSize13 andColor:AddCarColor];
        [self.contentView addSubview:_CarNumber];
        [_CarNumber mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.right.mas_equalTo(image.mas_left).with.offset(-8*kWidth);
        }];
        
        UIView * Hline1 =[[UIView alloc]init];
        Hline1.backgroundColor = GrayColor;
        [self.contentView addSubview:Hline1];
        [Hline1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView.mas_centerX);
            make.top.mas_equalTo(self.contentView.mas_bottom).with.offset(0.5*kHeight);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        }];
        

  
    }
    return self;
    
}
-(NewBtn*)createBtn:(NSString*)title {
    
    NewBtn * btn = [NewBtn buttonWithType:UIButtonTypeCustom];
    
    return btn;
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    // 名字的H
    CGFloat nameH = size.height;
    // 名字的W
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentLeft;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}

-(CustomTextField*)createField:(NSString*)placeholder andTag:(NSInteger)tag andFont:(double)font{
    
    CustomTextField * field =[[CustomTextField alloc]init];
    field.frame = CGRectMake(94*kWidth, 0, Main_Width-200*kWidth, 43*kWidth);
    field.delegate = self;
    field.userInteractionEnabled = YES;
    field.textAlignment = NSTextAlignmentLeft;
    CGFloat width = field.frame.size.width;
    CGFloat height = field.frame.size.height;
    //控制placeHolder的位置
    [field placeholderRectForBounds:CGRectMake(width/2, 0, width, height)];
    return field;
}



@end
