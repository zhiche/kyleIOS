//
//  QuoteCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/28.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewBtn.h"
@interface QuoteCell : UITableViewCell

@property (nonatomic,strong) UIView  * view;
@property (nonatomic,strong) UILabel * number;
@property (nonatomic,strong) UILabel * orderNumber;
@property (nonatomic,strong) UILabel * time;

@property (nonatomic,strong) UILabel *startAddress;
@property (nonatomic,strong) UILabel *startDetailAddress;
@property (nonatomic,strong) UILabel *startTimeL;
@property (nonatomic,strong) UILabel *endAddress;
@property (nonatomic,strong) UILabel *endDetailAddress;
@property (nonatomic,strong) UILabel *endTimeL;
@property (nonatomic,strong) UILabel *carStyle;
@property (nonatomic,strong) UILabel *price;
@property (nonatomic,strong) NewBtn *Btn2;
//@property (nonatomic,strong) NewBtn *Btn3;
//@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) UILabel * carTypeAndNumber;
@property (nonatomic,strong) UIView * line1;
@property (nonatomic,strong) UIView * line2;
@property (nonatomic,strong) UIImageView * timeImage;
@property (nonatomic,strong) UILabel * quoteCounts;

-(void)showInfo:(NSDictionary *)dic_info ;



@end
