//
//  QuoteCarListCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/20.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuoteCarListCell : UITableViewCell

@property (nonatomic,strong) UILabel * carName;
@property (nonatomic,strong) UILabel * carNumber;

-(void)showInfo:(NSDictionary *)dic_info;

@end
