//
//  PlaceOrderModel.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/17.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlaceOrderModel : NSObject


@property (nonatomic,strong) NSString * provinceName;
@property (nonatomic,strong) NSString * cityName;
@property (nonatomic,strong) NSString * countyName;
@property (nonatomic,strong) NSString * unitName;

@property (nonatomic,strong) NSString * address;
@property (nonatomic,strong) NSString * contact;
@property (nonatomic,strong) NSString * phone;

+(instancetype)ModelWithDic:(NSMutableDictionary*)dic;

@end


