//
//  GetCarTimeCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/11.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GetCarTimeCell : UITableViewCell

@property (nonatomic,strong) UILabel *getCarLabel;
@property (nonatomic,strong) UILabel *getCarTimeLabel;

@property (nonatomic,strong) UIButton *picture1Button;
@property (nonatomic,strong) UIButton *picture2Button;
@property (nonatomic,strong) UIButton *picture3Button;


@end
