//
//  GetCarTimeCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/11.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "GetCarTimeCell.h"

@implementation GetCarTimeCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
        
    }
    
    return  self;
}

-(void)initSubViews
{
    self.getCarLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, 70  * kWidth, 20)];
    self.getCarLabel.text = @"提车时间:";
    self.getCarLabel.font = Font(14);
    self.getCarLabel.textColor = GrayColor;
    [self.contentView addSubview:self.getCarLabel];
    
    self.getCarTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.getCarLabel.frame), 5, 200, 20)];
    self.getCarTimeLabel.font = Font(15);
    self.getCarTimeLabel.text = @"2016-04-21 上午";
    [self.contentView addSubview:self.getCarTimeLabel];
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(self.getCarTimeLabel.frame) + 5, screenWidth - 60, 0.5)];
    label.backgroundColor = GrayColor;
    [self.contentView addSubview:label];
    
    self.picture1Button = [UIButton buttonWithType:UIButtonTypeCustom];
    self.picture1Button.frame = CGRectMake(35, CGRectGetMaxY(label.frame) + 5, 40, 40);
    self.picture1Button.backgroundColor = [UIColor cyanColor];
    self.picture1Button.layer.cornerRadius = 5;
    [self.contentView addSubview:self.picture1Button];
    
    self.picture2Button = [UIButton buttonWithType:UIButtonTypeCustom];
    self.picture2Button.frame = CGRectMake(CGRectGetMaxX(self.picture1Button.frame) + 10, CGRectGetMaxY(label.frame) + 5, 40, 40);
    self.picture2Button.backgroundColor = [UIColor cyanColor];
    self.picture2Button.layer.cornerRadius = 5;
    [self.contentView addSubview:self.picture2Button];
    
    self.picture3Button = [UIButton buttonWithType:UIButtonTypeCustom];
    self.picture3Button.frame = CGRectMake(CGRectGetMaxX(self.picture2Button.frame) + 10, CGRectGetMaxY(label.frame) + 5, 40, 40);
    self.picture3Button.backgroundColor = [UIColor cyanColor];
    self.picture3Button.layer.cornerRadius = 5;
    [self.contentView addSubview:self.picture3Button];
    
    
    UILabel *balckLable = [[UILabel alloc]initWithFrame:CGRectMake(14, CGRectGetMaxY(self.picture1Button.frame) + 5, screenWidth - 28, 0.5)];
    balckLable.backgroundColor = BlackColor;
    [self.contentView addSubview:balckLable];
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
