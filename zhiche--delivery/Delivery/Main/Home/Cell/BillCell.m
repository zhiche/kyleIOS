//
//  BillCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/26.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "BillCell.h"
#import <Masonry.h>
@implementation BillCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
        self.priceLabel = [self createUIlabel:@"-￥30000" andFont:FontOfSize12 andColor:YellowColor];
        self.dateLabel = [self createUIlabel:@"2016-5-10" andFont:FontOfSize12 andColor:fontGrayColor];
        self.orderLabel = [self createUIlabel:@"订单号：CO123456888888" andFont:FontOfSize12 andColor:littleBlackColor];

        self.addressLabel = [self createUIlabel:@"北京市-山西省-大同市" andFont:FontOfSize12 andColor:fontGrayColor];
        
        UIView * line = [[UIView alloc]init];
        line.backgroundColor = LineGrayColor;
        [self.contentView addSubview:line];
        
        [self.contentView addSubview:self.priceLabel];
        [self.contentView addSubview:self.dateLabel];
        [self.contentView addSubview:self.orderLabel];
        [self.contentView addSubview:self.addressLabel];
        
        [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).with.offset(16*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_top).with.offset(20*kHeight);
        }];
        [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(16*kWidth);
            make.centerY.mas_equalTo(self.priceLabel.mas_centerY).with.offset(18*kHeight);
        }];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(107*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(0.5, 38*kHeight));
        }];

        
        [self.orderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(line.mas_right).with.offset(20*kWidth);
            make.centerY.mas_equalTo(self.priceLabel.mas_centerY);
        }];
        [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.orderLabel.mas_left);
            make.centerY.mas_equalTo(self.dateLabel.mas_centerY);
        }];

        UIView * viewline = [[UIView alloc]init];
        viewline.backgroundColor = LineGrayColor;
        [self.contentView addSubview:viewline];
        [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right);
            make.bottom.mas_equalTo(self.contentView.mas_bottom);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        }];
    }
    return self;
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentRight;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
