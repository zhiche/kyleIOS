//
//  QuoteListCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/6/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "QuoteListCell.h"
#import <Masonry.h>
#import "Header.h"
@implementation QuoteListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

//@property (nonatomic,strong) UILabel * NowPlace;
//@property (nonatomic,strong) UILabel * Timelabel;
//@property (nonatomic,strong) UILabel * CityName;
//@property (nonatomic,strong) UILabel * CarName;
//@property (nonatomic,strong) UILabel * PhoneNumber;
//@property (nonatomic,strong) UILabel * CarType;
//@property (nonatomic,strong) UILabel * QuoteList;
//@property (nonatomic,strong) NewBtn * QutoteBtn;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 10*kHeight)];
        view.backgroundColor = GrayColor;
        [self.contentView addSubview:view];
        UIView * headerView = [[UIView alloc]init];
        headerView.backgroundColor = headerBackViewColor;
        [self.contentView addSubview:headerView];
        [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left);
            make.top.mas_equalTo(view.mas_bottom);
            make.size.mas_equalTo(CGSizeMake(screenWidth, 43*kHeight));
        }];

        //广东
        self.Destinationlabel = [self createUIlabel:@"" andFont:FontOfSize13 andColor:littleBlackColor];
        //石家庄
        self.NowPlace = [self createUIlabel:@"" andFont:FontOfSize10 andColor:carScrollColor];
        //五分钟前
        self.Timelabel = [self createUIlabel:@"" andFont:FontOfSize12 andColor:headerTitleColor];
        //张**
        self.Namelabel = [self createUIlabel:@"" andFont:FontOfSize14 andColor:littleBlackColor];
        //京A123
        self.CarName = [self createUIlabel:@"" andFont:FontOfSize14 andColor:littleBlackColor];
        //136********
        self.PhoneNumber = [self createUIlabel:@"" andFont:FontOfSize14 andColor:littleBlackColor];
        //¥2736.00
        self.QuoteList = [self createUIlabel:@"" andFont:FontOfSize14 andColor:YellowColor];
        //板车
        self.CarType = [self createUIlabel:@"" andFont:FontOfSize14 andColor:littleBlackColor];
        //预定
        self.QutoteBtn = [self createBtn:@"预定"];

        [headerView addSubview:self.Destinationlabel];
        [headerView addSubview:self.NowPlace];
        [headerView addSubview:self.Timelabel];

        [self.contentView addSubview:self.QutoteBtn];
        [self.contentView addSubview:self.Namelabel];
        [self.contentView addSubview:self.CarName];
        [self.contentView addSubview:self.PhoneNumber];
        [self.contentView addSubview:self.QuoteList];
        [self.contentView addSubview:self.CarType];
        
        
        UILabel * destinationlabel = [self createUIlabel:@"目的城市" andFont:FontOfSize13 andColor:fontGrayColor];
        [headerView addSubview:destinationlabel];
        [destinationlabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(headerView.mas_left).with.offset(18*kWidth);
            make.centerY.mas_equalTo(headerView.mas_top).with.offset(21.5*kHeight);
        }];

        [self.Destinationlabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(destinationlabel.mas_right).with.offset(14*kWidth);
            make.centerY.mas_equalTo(destinationlabel.mas_centerY);
        }];

        UIView * viewline = [[UIView alloc]init];
        viewline.backgroundColor = littleBlackColor;
        [headerView addSubview:viewline];
        [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.Destinationlabel.mas_right).with.offset(11*kWidth);
            make.centerY.mas_equalTo(destinationlabel.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(0.5, 15*kHeight));
        }];

        [self.NowPlace mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(viewline.mas_right).with.offset(11*kWidth);
            make.centerY.mas_equalTo(destinationlabel.mas_centerY);
        }];
        [self.Timelabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(headerView.mas_right).with.offset(-15*kWidth);
            make.centerY.mas_equalTo(self.NowPlace.mas_centerY);
        }];
        
        UIView * viewline1 = [[UIView alloc]init];
        viewline1.backgroundColor = GrayColor;
        [self.contentView addSubview:viewline1];
        [viewline1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left);
            make.top.equalTo(view.mas_bottom).with.offset(43*kHeight);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        }];

        UILabel *carlabel = [self createUIlabel:@"车牌" andFont:FontOfSize13 andColor:fontGrayColor];
        [self.contentView addSubview:carlabel];
        [carlabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(18*kWidth);
            make.top.mas_equalTo(viewline1.mas_bottom).with.offset(17*kHeight);
        }];
        [self.CarName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(carlabel.mas_centerY);
            make.left.mas_equalTo(carlabel.mas_right).with.offset(14*kHeight);
        }];
//        UIView * viewline2 = [[UIView alloc]init];
//        viewline2.backgroundColor = GrayColor;
//        [self.contentView addSubview:viewline2];
//        [viewline2 mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.contentView.mas_left);
//            make.top.equalTo(viewline1.mas_bottom).with.offset(43*kHeight);
//            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
//        }];
//        UILabel * nametype = [self createUIlabel:@"车型" andFont:FontOfSize13 andColor:fontGrayColor];
//        [self.contentView addSubview:nametype];
//        [nametype mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(self.contentView.mas_left).with.offset(18*kWidth);
//            make.centerY.mas_equalTo(viewline2.mas_bottom).with.offset(21.5*kHeight);
//        }];
        [self.CarType mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.CarName.mas_centerY);
            make.left.mas_equalTo(self.CarName.mas_right).with.offset(28*kHeight);
        }];
//        UIView * viewline3 = [[UIView alloc]init];
//        viewline3.backgroundColor = GrayColor;
//        [self.contentView addSubview:viewline3];
//        [viewline3 mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.contentView.mas_left);
//            make.top.equalTo(viewline2.mas_bottom).with.offset(43*kHeight);
//            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
//        }];
        
        UILabel * namelabel = [self createUIlabel:@"姓名" andFont:FontOfSize13 andColor:fontGrayColor];
        [self.contentView addSubview:namelabel];
        [namelabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(18*kWidth);
            make.top.mas_equalTo(carlabel.mas_bottom).with.offset(19*kHeight);
        }];
        [self.Namelabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(namelabel.mas_centerY);
            make.left.mas_equalTo(namelabel.mas_right).with.offset(14*kHeight);
        }];
        
        [self.PhoneNumber mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.Namelabel.mas_centerY);
            make.left.mas_equalTo(self.Namelabel.mas_right).with.offset(28*kHeight);
        }];
//        UIView * viewline4 = [[UIView alloc]init];
//        viewline4.backgroundColor = GrayColor;
//        [self.contentView addSubview:viewline4];
//        [viewline4 mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.contentView.mas_left);
//            make.top.equalTo(viewline3.mas_bottom).with.offset(43*kHeight);
//            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
//        }];
        
        
        UILabel * namequtote = [self createUIlabel:@"报价" andFont:FontOfSize13 andColor:fontGrayColor];
        [self.contentView addSubview:namequtote];
        [namequtote mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(18*kWidth);
            make.top.mas_equalTo(namelabel.mas_bottom).with.offset(19*kHeight);
        }];
        [self.QuoteList mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(namequtote.mas_centerY);
            make.left.mas_equalTo(namequtote.mas_right).with.offset(14*kHeight);
        }];
        self.QuoteList.textColor = YellowColor;
//        UIView * viewline5 = [[UIView alloc]init];
//        viewline5.backgroundColor = GrayColor;
//        [self.contentView addSubview:viewline5];
//        [viewline5 mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.contentView.mas_left);
//            make.top.equalTo(viewline4.mas_bottom).with.offset(43*kHeight);
//            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
//        }];

        [self.QutoteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right).with.offset(-15*kWidth);
            make.centerY.mas_equalTo(self.Namelabel.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(70*kWidth, 70*kHeight));
        }];
    }
    return self;
}

-(void)showInfo:(NSDictionary *)dic_info{

//    {
//        allFees = "0.13";
//        depositCost = "<null>";
//        destination = vg;
//        formCurrentTime = "195\U5206\U949f\U524d";
//        id = 23;
//        phone = "150****8178";
//        position = "\U5317\U4eac\U5e02\U6d77\U6dc0\U533a";
//        quoteDetails =     (
//                            {
//                                feeName = "\U8fd0\U8f93\U8d39";
//                                id = 23;
//                                money = "0.1";
//                            }
//                            );
//        quoteTime = "2016-07-20 05:45:13";
//        truckNo = "\U4eacBSBS**";
//        truckTypeName = "<null>";
//        userName = "<null>";
//    }


    
    NSString * position = [self backString:dic_info[@"position"]];
    self.NowPlace.text = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"当前定位城市:%@",position]];
    self.Timelabel.text = [NSString stringWithFormat:@"%@",[self backString:dic_info[@"formCurrentTime"]]];
    self.NowPlace.text = [NSString stringWithFormat:@"%@",[self backString:dic_info[@"destination"]]];
    self.CarName.text = [NSString stringWithFormat:@"%@",[self backString:dic_info[@"truckNo"]]];
    self.Namelabel.text = [NSString stringWithFormat:@"%@",[self backString:dic_info[@"userName"]]];
    self.PhoneNumber.text = [NSString stringWithFormat:@"%@",[self backString:dic_info[@"phone"]]];
    self.CarType.text = [NSString stringWithFormat:@"%@",[self backString:dic_info[@"truckTypeName"]]];
    self.QuoteList.text = [NSString stringWithFormat:@"￥%@",[self backString:dic_info[@"allFees"]]];
}


-(NewBtn*)createBtn:(NSString*)title {
    
    NewBtn * btn = [NewBtn buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:WhiteColor forState:UIControlStateNormal];
    btn.titleLabel.font = Font(FontOfSize17);
    btn.backgroundColor = YellowColor;
    btn.layer.borderWidth = 2;
    btn.layer.cornerRadius = 35*kHeight;
    btn.layer.borderColor = BtnBorderColor.CGColor;
    return btn;
}


-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    // 名字的H
    CGFloat nameH = size.height;
    // 名字的W
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentRight;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}


-(NSString *)backString:(NSString *)string
{
//    NSString * str = [NSString stringWithFormat:@"%@",string];
    if ([string isEqual:[NSNull null]]) {
        return @"";
    } else {
        return string;
    }
}

@end
