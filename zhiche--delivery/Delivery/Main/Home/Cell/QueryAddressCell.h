//
//  QueryAddressCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/9/9.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QueryAddressCell : UITableViewCell


@property (nonatomic,strong) UILabel * title;
@property (nonatomic,strong) UIView * viewline;
@end
