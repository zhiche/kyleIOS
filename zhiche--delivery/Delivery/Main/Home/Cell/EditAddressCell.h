//
//  EditAddressCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/6/27.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewBtn.h"

@interface EditAddressCell : UITableViewCell

@property (nonatomic,strong) UILabel * CityName;
@property (nonatomic,strong) UILabel * StoreName;
@property (nonatomic,strong) UILabel * AddressName;
@property (nonatomic,strong) UILabel * iphone;
@property (nonatomic,strong) UILabel * PeopleName;
@property (nonatomic,strong) UILabel * ChooseText;
@property (nonatomic,strong) UIView  * backview;
@property (nonatomic,strong) UIImageView * leftImage;
@property (nonatomic,strong) NewBtn * MRbtn;
@property (nonatomic,strong) NewBtn * BJbtn;
@property (nonatomic,strong) NewBtn * SCbtn;


@end
