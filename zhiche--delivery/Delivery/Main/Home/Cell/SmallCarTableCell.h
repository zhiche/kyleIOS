//
//  SmallCarTableCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SmallCarTableCell : UITableViewCell

@property (nonatomic,strong) UILabel * CarLabel;
@property (nonatomic,strong) UILabel * CarUnitPrice;
@property (nonatomic,strong) UILabel * CarAllPrice;

@end
