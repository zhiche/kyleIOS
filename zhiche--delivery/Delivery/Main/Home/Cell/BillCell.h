//
//  BillCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/26.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BillCell : UITableViewCell

@property (nonatomic,strong) UILabel * priceLabel;
@property (nonatomic,strong) UILabel * dateLabel;
@property (nonatomic,strong) UILabel * orderLabel;
@property (nonatomic,strong) UILabel * addressLabel;
@property (nonatomic,strong) UILabel * sortLabel;
@end
