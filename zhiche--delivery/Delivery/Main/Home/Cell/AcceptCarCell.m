//
//  AcceptCarCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/6/3.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "AcceptCarCell.h"
#import <Masonry.h>
@implementation AcceptCarCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
        self.view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 30*kHeight)];
        self.view.backgroundColor = RGBACOLOR(149, 149, 149, 0.2);
        self.number = [self createUIlabel:@"运单号:" andFont:FontOfSize14 andColor:BlackColor];
        self.orderNumber = [self createUIlabel:@"zc2713474894834747" andFont:FontOfSize14 andColor:BlackColor];
        self.imageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        if (self.imageBtn.selected) {
            self.imageBtn.backgroundColor = [UIColor blueColor];
        }else{
            self.imageBtn.backgroundColor = [UIColor whiteColor];
        }
        self.imageBtn.layer.cornerRadius=20/2.0;
        self.imageBtn.layer.masksToBounds=YES;
        UIView * Hline1 =[[UIView alloc]init];
        Hline1.backgroundColor = RGBACOLOR(149, 149, 149, 1);
        
        [self.view addSubview:Hline1];
        [self.view addSubview:self.imageBtn];
        [self.view addSubview:self.number];
        [self.view addSubview:self.orderNumber];
        [self.contentView addSubview:self.view];
        [Hline1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view.mas_left);
            make.top.equalTo(self.view.mas_top);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        }];
        [self.imageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.view.mas_left).with.offset(14*kWidth);
            make.centerY.mas_equalTo(self.view.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(20*kWidth, 20*kHeight));
        }];
        [self.number mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.imageBtn.mas_right).with.offset(14*kWidth);
            make.centerY.mas_equalTo(self.view.mas_centerY);
        }];
        [self.orderNumber mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.number.mas_right).with.offset(10*kWidth);
            make.centerY.mas_equalTo(self.view.mas_centerY);
        }];
        //省市县
        self.startAddress = [self createUIlabel:@"长沙" andFont:FontOfSize14 andColor:BlackColor];
        //发车单位 收车单位
        self.startDetailAddress = [self createUIlabel:@"长沙芒果库" andFont:FontOfSize14 andColor:BlackColor];
        self.startTimeL = [self createUIlabel:@"2016-01-09" andFont:FontOfSize14 andColor:BlackColor];
        self.endAddress = [self createUIlabel:@"湖南" andFont:FontOfSize14 andColor:BlackColor];
        self.endDetailAddress = [self createUIlabel:@"湖南益阳宝马4S店" andFont:FontOfSize14 andColor:BlackColor];
        self.endTimeL = [self createUIlabel:@"2016-01-20" andFont:FontOfSize14 andColor:BlackColor];
        [self.contentView addSubview:self.startAddress];
        [self.contentView addSubview:self.startDetailAddress];
        [self.contentView addSubview:self.startTimeL];
        [self.contentView addSubview:self.endAddress];
        [self.contentView addSubview:self.endDetailAddress];
        [self.contentView addSubview:self.endTimeL];
        
        [self.startAddress mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.contentView.mas_left).with.offset(Main_Width/4*kHeight);
            make.top.mas_equalTo(self.view.mas_bottom).with.offset(10*kHeight);
        }];
        [self.startDetailAddress mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.startAddress.mas_centerX);
            make.top.mas_equalTo(self.startAddress.mas_bottom).with.offset(10*kHeight);
        }];
        [self.startTimeL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.startAddress.mas_centerX);
            make.top.mas_equalTo(self.startDetailAddress.mas_bottom).with.offset(10*kHeight);
        }];
        [self.endAddress mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.contentView.mas_right).with.offset(-Main_Width/4*kHeight);
            make.top.mas_equalTo(self.view.mas_bottom).with.offset(10*kHeight);
        }];
        [self.endDetailAddress mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.endAddress.mas_centerX);
            make.top.mas_equalTo(self.endAddress.mas_bottom).with.offset(10*kHeight);
        }];
        
        [self.endTimeL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.endDetailAddress.mas_centerX);
            make.top.mas_equalTo(self.endDetailAddress.mas_bottom).with.offset(10*kHeight);
        }];
    }
    return self;
}

-(void)showInfo:(NSDictionary *)dic_info{
    
    self.startAddress.text =[NSString stringWithFormat:@"%@",dic_info[@"departCityName"]];
    self.startDetailAddress.text = [NSString stringWithFormat:@"%@",dic_info[@"departUnit"]];
    self.startTimeL.text = [NSString stringWithFormat:@"%@",dic_info[@"deliveryDate"]];
    self.endAddress.text = [NSString stringWithFormat:@"%@",dic_info[@"receiptCityName"]];
    self.endDetailAddress.text = [NSString stringWithFormat:@"%@",dic_info[@"receiptUnit"]];
    self.endTimeL.text = [NSString stringWithFormat:@"%@",dic_info[@"arriveDate"]];
    
}






- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentRight;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}


@end
