//
//  AddCarVinCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/13.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "AddCarVinCell.h"
#import <Masonry.h>
@implementation AddCarVinCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
        _viewline1 = [[UIView alloc]init];
        _viewline1.backgroundColor = GrayColor;
        [self.contentView addSubview:_viewline1];
        [_viewline1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(18*kWidth);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5*kHeight));
            make.top.mas_equalTo(self.contentView.mas_bottom).with.offset(-2);
        }];
       // AddCarColor fontGrayColor
        _CarVIN = [self createUIlabel:@"车架号" andFont:FontOfSize13 andColor:AddCarColor];
        [self.contentView addSubview:_CarVIN];
        [_CarVIN mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).with.offset(18*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY).with.offset(-1);
        }];
        _field =[self createField:@"请输入车架号" andTag:1 andFont:FontOfSize13];
        _field.placeholder = @"请输入车架号";
        [self.contentView addSubview:_field];
    }
    return self;
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    // 名字的H
    CGFloat nameH = size.height;
    // 名字的W
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentCenter;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}

-(CustomTextField*)createField:(NSString*)placeholder andTag:(NSInteger)tag andFont:(double)font{
    
    NSMutableAttributedString * placeH = [[NSMutableAttributedString alloc]initWithString:placeholder];
    
    CustomTextField * field =[[CustomTextField alloc]init];
    field.frame = CGRectMake(84*kWidth, 0, Main_Width-100*kWidth, 43*kWidth);
    field.delegate = self;
    field.font = Font(font);
    
    [field setValue:[UIFont boldSystemFontOfSize:font]forKeyPath:@"_placeholderLabel.font"];
    
    [placeH addAttribute:NSFontAttributeName
                       value:[UIFont boldSystemFontOfSize:16]
                       range:NSMakeRange(0, placeholder.length)];
    field.attributedPlaceholder = placeH;
    
    field.clearButtonMode = UITextFieldViewModeWhileEditing;
    field.userInteractionEnabled = YES;
    field.textAlignment = NSTextAlignmentLeft;
    field.returnKeyType = UIReturnKeyDone;
    CGFloat width = field.frame.size.width;
    CGFloat height = field.frame.size.height;
    //控制placeHolder的位置
    [field placeholderRectForBounds:CGRectMake(width/2, 0, width, height)];
    
    [field clearButtonRectForBounds:field.frame];
    
    return field;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
