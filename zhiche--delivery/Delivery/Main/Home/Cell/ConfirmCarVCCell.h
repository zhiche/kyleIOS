//
//  ConfirmCarVCCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/25.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmCarVCCell : UITableViewCell

@property (nonatomic,strong) UILabel * CarLabel;
@property (nonatomic,strong) UILabel * CarAllPrice;


@end
