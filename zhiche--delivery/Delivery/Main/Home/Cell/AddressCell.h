//
//  AddressCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/9.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface AddressCell : UITableViewCell

@property (nonatomic,strong) UILabel * CityName;
@property (nonatomic,strong) UILabel * StoreName;
@property (nonatomic,strong) UILabel * AddressName;
@property (nonatomic,strong) UILabel * PeopleName;
@property (nonatomic,strong) UILabel * ChooseText;
@property (nonatomic,strong) UIImageView * SmallImage;
@property (nonatomic,strong) UIImageView * RightImage;
@property (nonatomic,strong) UIView * viewSline;
@property (nonatomic,strong) UIView * viewline;
@end
