//
//  Bill_HM_Cell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/12/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "Bill_HM_Cell.h"
#import <Masonry.h>
#import "Header.h"
@implementation Bill_HM_Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){

        
        self.orderLabel = [self createUIlabel:@"订单号：CO123456888888" andFont:FontOfSize12 andColor:littleBlackColor];
        self.dateLabel = [self createUIlabel:@"2016-5-10" andFont:FontOfSize12 andColor:fontGrayColor];
        self.addressLabel = [self createUIlabel:@"北京市-山西省-大同市" andFont:FontOfSize12 andColor:fontGrayColor];
        
        self.priceLabel = [self createUIlabel:@"-￥30000" andFont:FontOfSize12 andColor:YellowColor];
        self.sortLabel = [self createUIlabel:@"" andFont:FontOfSize12 andColor:WhiteColor];
        self.sortLabel.textAlignment = NSTextAlignmentCenter;

        self.sortLabel.backgroundColor = YellowColor;
        self.sortLabel.layer.cornerRadius = 2.5;
        self.sortLabel.layer.masksToBounds = YES;
        UIView * line = [[UIView alloc]init];
        line.backgroundColor = LineGrayColor;
        [self.contentView addSubview:line];
        
        [self.contentView addSubview:self.priceLabel];
        [self.contentView addSubview:self.dateLabel];
        [self.contentView addSubview:self.orderLabel];
        [self.contentView addSubview:self.addressLabel];
        [self.contentView addSubview:self.sortLabel];

       _orderBtn = [NewBtn buttonWithType:UIButtonTypeCustom];
        [self.contentView addSubview:_orderBtn];
        [_orderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left);
            make.right.mas_equalTo(self.orderLabel.mas_right);
            make.top.mas_equalTo(self.contentView.mas_top);
            make.bottom.mas_equalTo(self.orderLabel.mas_bottom);

        }];

        
        
        
        
        [self.orderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(16*kWidth);
            make.top.mas_equalTo(self.contentView.mas_top).with.offset(5*kHeight);
        }];
        [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(16*kWidth);
            make.top.mas_equalTo(self.orderLabel.mas_bottom).with.offset(5*kHeight);
        }];
        
        [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.orderLabel.mas_left);
            make.top.mas_equalTo(self.dateLabel.mas_bottom).with.offset(5*kHeight);
        }];

        [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).with.offset(-16*kWidth);
            make.centerY.mas_equalTo(self.dateLabel.mas_centerY);
        }];
        
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left);
            make.centerY.mas_equalTo(self.contentView.mas_top).with.offset(70*kHeight);
            make.size.mas_equalTo(CGSizeMake(Main_height, 0.5*kHeight));
        }];
        [self.sortLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).with.offset(16*kWidth);
            make.centerY.mas_equalTo(line.mas_bottom).with.offset(16*kHeight);
//            make.width.mas_equalTo(50*kWidth);
        }];
        
        UIView * line1 = [[UIView alloc]init];
        line1.backgroundColor = GrayColor;
        [self.contentView addSubview:line1];
        [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left);
            make.bottom.mas_equalTo(self.contentView.mas_bottom);
            make.size.mas_equalTo(CGSizeMake(Main_height, 5*kHeight));
        }];

        

    }
    return self;
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentRight;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
