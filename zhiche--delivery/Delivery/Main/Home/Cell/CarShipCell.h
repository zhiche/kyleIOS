//
//  CarShipCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarShipCell : UITableViewCell
@property (nonatomic,strong) UIImageView * SmallImage;
@property (nonatomic,strong) UILabel * CarName;
@property (nonatomic,strong) UILabel * CarNumber;

@end
