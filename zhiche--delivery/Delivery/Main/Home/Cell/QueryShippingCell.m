//
//  QueryShippingCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/8/22.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "QueryShippingCell.h"
#import <Masonry.h>

@implementation QueryShippingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
        
        UIView * viewline = [[UIView alloc]init];
        viewline.backgroundColor = GrayColor;
        [self.contentView addSubview:viewline];
        [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).with.offset(37*kWidth);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
            make.top.mas_equalTo(self.contentView.mas_top);
        }];

        _nameLable = [self createUIlabel:@"标准达" andFont:FontOfSize12 andColor:BtnTitleColor];
        _timeLable = [self createUIlabel:@"2016年-07-07" andFont:FontOfSize12 andColor:BtnTitleColor];
        _priceLable = [self createUIlabel:@"￥1600.00" andFont:FontOfSize12 andColor:BtnTitleColor];
        [self.contentView addSubview:_nameLable];
        [self.contentView addSubview:_timeLable];
        [self.contentView addSubview:_priceLable];

        [_nameLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView.mas_left).with.offset(70*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
        }];
        [_timeLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView.mas_centerX);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
        }];
        [_priceLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).with.offset(-35*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
        }];
    }
    return self;
}


-(void)showInfo:(NSDictionary *)dic_info{


    
//    {
//        beginRange = 0;
//        coefficient = 1;
//        days = 2;
//    default = 1;
//        disPrice = 786;
//        distance = "0.001";
//        endRange = 500;
//        endTime = "2016-10-03 23:59:59";
//        expedited = 0;
//        fees =     (
//                    {
//                        cost = 248;
//                        discount = 248;
//                        feeName = "\U63d0\U8f66\U8d39";
//                        id = 1;
//                        isDiscount = 1;
//                    },
//                    {
//                        cost = 248;
//                        discount = 248;
//                        feeName = "\U4ea4\U8f66\U8d39";
//                        id = 2;
//                        isDiscount = 1;
//                    },
//                    {
//                        cost = 362;
//                        discount = 290;
//                        feeName = "\U8fd0\U8f93\U8d39";
//                        id = 4;
//                        isDiscount = 1;
//                    }
//                    );
//        id = 2;
//        kmPerDay = "<null>";
//        maxDays = "<null>";
//        price = 786;
//        productId = 2;
//        productName = "\U6807\U51c6\U8fbe";
//    }

    //2016-10-03 23:59:59
    //时间  运距 价格
    NSString * endTime = [NSString stringWithFormat:@"%@",dic_info[@"endTime"]];

    _nameLable.text = [endTime substringWithRange:NSMakeRange(0, 10)];
        
    _timeLable.text = [NSString stringWithFormat:@"%.2fkm",[dic_info[@"distance"] floatValue]];
    NSString * price = [NSString stringWithFormat:@"￥%.2f",[dic_info[@"disPrice"] floatValue]];
    _priceLable.text = price;


}


-(NSString *)backString:(NSString *)string
{
    //    NSString * str = [NSString stringWithFormat:@"%@",string];
    if ([string isEqual:[NSNull null]]) {
        return @"";
    } else {
        return string;
    }
}


-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentLeft;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
