//
//  placeAddressCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/14.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextField.h"

@interface placeAddressCell : UITableViewCell<UITextFieldDelegate>
@property (nonatomic,strong) CustomTextField * field;
@end
