//
//  BillSortCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/12/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "BillSortCell.h"
#import <Masonry.h>

@implementation BillSortCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
        
        self.leftImage = [[UIImageView alloc]init];
        [self.contentView addSubview:self.leftImage];
        [self.leftImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(18*kWidth);
            make.centerY.mas_equalTo(self.contentView);
            make.size.mas_equalTo(CGSizeMake(17*kWidth, 17*kHeight));
        }];
        self.titleLabel = [self createUIlabel:@"" andFont:FontOfSize12 andColor:BlackColor];
        [self.contentView addSubview:self.titleLabel];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.leftImage.mas_right).with.offset(11*kWidth);
            make.centerY.mas_equalTo(self.contentView);
        }];
        self.RightImage = [[UIImageView alloc]init];
        self.RightImage.image = [UIImage imageNamed:@"common_list_arrows_more"];
        [self.contentView addSubview:self.RightImage];
        [self.RightImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right).with.offset(-15*kWidth);
            make.centerY.mas_equalTo(self.contentView);
            make.size.mas_equalTo(CGSizeMake(8*kWidth, 13*kHeight));
        }];
        
        self.line = [[UIView alloc]init];
        self.line.backgroundColor = LineGrayColor;
        [self.contentView addSubview:self.line];
        [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.contentView.mas_bottom);
            make.left.mas_equalTo(self.leftImage.mas_left);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        }];

        
    }
    return self;
}
-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentRight;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
