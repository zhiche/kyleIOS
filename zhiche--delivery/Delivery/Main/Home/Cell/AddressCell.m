//
//  AddressCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/9.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "AddressCell.h"
#import <Masonry.h>
@implementation AddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){

        
        _viewline = [[UIView alloc]init];
        _viewline.backgroundColor = LineGrayColor;
        [self.contentView addSubview:_viewline];

        _viewSline = [[UIView alloc]init];
        _viewSline.backgroundColor = LineGrayColor;
        [self.contentView addSubview:_viewSline];


        
        _CityName = [self createUIlabel:@"" andFont:FontOfSize13 andColor:littleBlackColor];
        _StoreName = [self createUIlabel:@"" andFont:FontOfSize13 andColor:littleBlackColor];
        _AddressName = [self createUIlabel:@"" andFont:FontOfSize13 andColor:littleBlackColor];
        _PeopleName = [self createUIlabel:@"" andFont:FontOfSize13 andColor:littleBlackColor];
        _SmallImage = [[UIImageView alloc]init];
        
        [self.contentView addSubview:_CityName];
        [self.contentView addSubview:_StoreName];
        [self.contentView addSubview:_AddressName];
        [self.contentView addSubview:_PeopleName];
        [self.contentView addSubview:_SmallImage];
        [_CityName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_SmallImage.mas_right).with.offset(16*kWidth);
            make.top.mas_equalTo(self.contentView.mas_top).with.offset(14*kHeight);
        }];
        [_StoreName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_CityName.mas_right).with.offset(10*kWidth);
            make.width.mas_equalTo(Main_Width/2);
            make.centerY.mas_equalTo(_CityName.mas_centerY);
        }];
        
        [_SmallImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).with.offset(20*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(12*kWidth, 16*kHeight));
        }];
        [_AddressName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_CityName.mas_left).with.offset(0);
            make.centerY.mas_equalTo(_SmallImage.mas_centerY);
            
            make.width.mas_equalTo(Main_Width*0.7);
        }];
        [_PeopleName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_CityName.mas_left).with.offset(0);
            make.top.mas_equalTo(self.contentView.mas_top).with.offset(73*kHeight);
//            make.centerY.mas_equalTo(self.contentView.mas_centerY);

        }];
        _RightImage= [[UIImageView alloc]init];
        _RightImage.image = [UIImage imageNamed:@"common_list_arrows_more"];
        [self.contentView addSubview:_RightImage];
        [_RightImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).with.offset(-15*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(8*kWidth, 13*kHeight));
        }];
        
        [_viewSline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.CityName.mas_top);
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(40*kWidth);
            make.bottom.mas_equalTo(self.PeopleName.mas_bottom);
            make.width.mas_equalTo(0.5);
        }];
        
        
        [_viewline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.contentView.mas_bottom);
            make.left.mas_equalTo(self.CityName.mas_left);
            make.size.mas_equalTo(CGSizeMake(Main_Width-50*kWidth, 0.5));
        }];

        
        
//        [viewSline mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(self.contentView.mas_top).with.offset(14*kHeight);
//            make.left.mas_equalTo(self.contentView.mas_left).with.offset(40*kWidth);
//            make.bottom.mas_equalTo(self.contentView.mas_bottom).with.offset(14*kHeight);
//            make.width.mas_equalTo(0.5);
//        }];

    }
    return self;
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentLeft;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
