//
//  MineCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/9.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MineOrderModel.h"

@interface MineCell : UITableViewCell
@property (nonatomic,strong) UIImageView *logoImageV;//logo图片
@property (nonatomic,strong) UILabel *orderNumberL;//订单编号
@property (nonatomic,strong) UILabel *timeL;//时间

@property (nonatomic,strong) UILabel *startAddress;//起始地点
@property (nonatomic,strong) UILabel *startDetailAddress;//起始详细地点

@property (nonatomic,strong) UILabel *endAddress;//终止地点
@property (nonatomic,strong) UILabel *endDetailAddress;//终止详细地点

@property (nonatomic,strong) UILabel *startTimeL;//起始时间
@property (nonatomic,strong) UILabel *endTimeL;//终止时间

@property (nonatomic,strong) UILabel *carStyleL;//车型
@property (nonatomic,strong) UILabel *numberL;

@property (nonatomic,strong) UIButton *deleteButton;
@property (nonatomic,strong) UIButton *detailButton;
@property (nonatomic,strong) UIButton *repostButton;

@property (nonatomic,strong) MineOrderModel *model;

@end
