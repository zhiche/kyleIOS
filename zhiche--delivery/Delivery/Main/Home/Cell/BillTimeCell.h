//
//  BillTimeCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/10/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BillTimeCell : UITableViewCell
@property (nonatomic,strong) UILabel * title;
@property (nonatomic,strong) UIView * viewline;

@end
