//
//  Bill_HM_Cell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/12/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewBtn.h"

@interface Bill_HM_Cell : UITableViewCell
@property (nonatomic,strong) UILabel * priceLabel;
@property (nonatomic,strong) UILabel * dateLabel;
@property (nonatomic,strong) UILabel * orderLabel;
@property (nonatomic,strong) UILabel * addressLabel;
@property (nonatomic,strong) UILabel * sortLabel;
@property (nonatomic,strong) NewBtn * orderBtn;
@end
