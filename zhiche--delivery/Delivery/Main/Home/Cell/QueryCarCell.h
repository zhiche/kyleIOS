//
//  QueryCarCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/9/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QueryCarCell : UITableViewCell

@property (nonatomic,strong) UIImageView * LeftImage;
@property (nonatomic,strong) UILabel * labelTitle;
@property (nonatomic,strong) UIView * viewline;
@end
