//
//  CDAddressCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/11/29.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDAddressCell : UITableViewCell
@property (nonatomic,strong) UILabel * title;
@property (nonatomic,strong) UILabel * address;

@property (nonatomic,strong) UIView * viewline;


@end
