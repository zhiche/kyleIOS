//
//  CarCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/9.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewBtn.h"
#import "CustomTextField.h"

@interface CarCell : UITableViewCell<UITextFieldDelegate>

@property (nonatomic,strong) UIImageView * SmallImage;
@property (nonatomic,strong) UILabel * CarName;
@property (nonatomic,strong) UILabel * CarNumber;
@property (nonatomic,strong) UIButton * deleteBtn;
@end
