//
//  AcceptCarCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/6/3.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AcceptCarCell : UITableViewCell
@property (nonatomic,strong) UIView  * view;
@property (nonatomic,strong) UIButton * imageBtn;
@property (nonatomic,strong) UILabel * number;
@property (nonatomic,strong) UILabel * orderNumber;

@property (nonatomic,strong) UILabel *startAddress;
@property (nonatomic,strong) UILabel *startDetailAddress;
@property (nonatomic,strong) UILabel *startTimeL;
@property (nonatomic,strong) UILabel *endAddress;
@property (nonatomic,strong) UILabel *endDetailAddress;
@property (nonatomic,strong) UILabel *endTimeL;


-(void)showInfo:(NSDictionary *)dic_info ;

@end
