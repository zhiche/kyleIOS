//
//  ConfirmCarVCCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/25.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ConfirmCarVCCell.h"
#import <Masonry.h>
@implementation ConfirmCarVCCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
        _CarAllPrice = [self createUIlabel:@"" andFont:FontOfSize13 andColor:carScrollColor];
        _CarLabel = [self createUIlabel:@"" andFont:FontOfSize13 andColor:carScrollColor];
        [self.contentView addSubview:_CarAllPrice];
        [self.contentView addSubview:_CarLabel];
        [_CarLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).with.offset(18*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
        }];
        [_CarAllPrice mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).with.offset(-15*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
        }];
        
    }
    return self;
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentRight;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
