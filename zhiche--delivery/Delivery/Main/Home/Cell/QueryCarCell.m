//
//  QueryCarCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/9/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "QueryCarCell.h"
#import <Masonry.h>
@implementation QueryCarCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
        
        self.viewline = [[UIView alloc]init];
        self.viewline.backgroundColor = GrayColor;
        [self.contentView addSubview:self.viewline];
        [self.viewline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(18*kWidth);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
            make.top.mas_equalTo(self.contentView.mas_bottom);
        }];

        self.LeftImage = [[UIImageView alloc]init];
        self.LeftImage.image = [UIImage imageNamed:@"unselect"];
        self.labelTitle = [self createUIlabel:@"" andFont:FontOfSize13 andColor:AddCarColor];
        [self.contentView addSubview:self.LeftImage];
        [self.contentView addSubview:self.labelTitle];

        [self.LeftImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(18*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(18*kWidth, 18*kHeight));
        }];
        [self.labelTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.LeftImage.mas_right).with.offset(15*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
        }];
    }
    return self;
}



-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    // 名字的H
    CGFloat nameH = size.height;
    // 名字的W
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentRight;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}


-(NSString *)backString:(NSString *)string
{
    //    NSString * str = [NSString stringWithFormat:@"%@",string];
    if ([string isEqual:[NSNull null]]) {
        return @"";
    } else {
        return string;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
