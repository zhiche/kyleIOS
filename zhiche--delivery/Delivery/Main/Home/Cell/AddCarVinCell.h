//
//  AddCarVinCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/13.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextField.h"

@interface AddCarVinCell : UITableViewCell<UITextFieldDelegate>
@property (nonatomic,strong) UILabel * CarVIN;
@property (nonatomic,strong) CustomTextField * field;
@property (nonatomic,strong) UIView * viewline1;
@end
