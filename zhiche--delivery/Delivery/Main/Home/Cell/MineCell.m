//
//  MineCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/9.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "MineCell.h"
#import <Masonry.h>

@implementation MineCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
        
    }
    
    return  self;
}

-(void)initSubViews
{
    //企业logo
   self.logoImageV = [[UIImageView alloc]initWithFrame:CGRectMake(60 * kWidth, 5, 40 * kWidth, 20)];
    self.logoImageV.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:self.logoImageV];
    
    //订单编号
    self.orderNumberL = [self backLabelWithFrame:CGRectMake((screenWidth - 140)/2, 5, 140, 20) andFont:15];
    self.orderNumberL.text = @"OD201604190001";
    self.orderNumberL.textAlignment = NSTextAlignmentCenter;
    self.orderNumberL.textColor = [UIColor blueColor];
    
    [self.orderNumberL sizeToFit];
    UILabel *lineL = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.orderNumberL.frame) - 3, self.orderNumberL.frame.size.width, 0.5)];
    [self.orderNumberL addSubview:lineL];
    lineL.backgroundColor = [UIColor blueColor];
    
    //用时label
    self.timeL = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(self.orderNumberL.frame) - 10, 5, 100, 20) andFont:15];
    self.timeL.text = @"27:30";
    
    UILabel *lineL1 = [[UILabel alloc]initWithFrame:CGRectMake(14, CGRectGetMaxY(self.logoImageV.frame) + 3, screenWidth - 28, 0.5)];
    lineL1.backgroundColor = GrayColor;
    [self.contentView addSubview:lineL1];
    
    __weak typeof(self) weakSelf = self;
    
    //起始地址
    self.startAddress = [[UILabel alloc]init];
    [self.contentView addSubview:self.startAddress];
    [self.startAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(lineL1.mas_bottom).offset(3);
        make.centerX.equalTo(weakSelf.logoImageV);
        make.size.mas_equalTo(CGSizeMake(100, 20));
        
    }];
    self.startAddress.textAlignment = NSTextAlignmentCenter;
    self.startAddress.font = Font(14);
    self.startAddress.text = @"北京";
    
    //终止地址
    self.endAddress = [[UILabel alloc]init];
    [self.contentView addSubview:self.endAddress];
    [self.endAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(lineL1.mas_bottom).offset(3);
        make.centerX.equalTo(weakSelf.timeL);
        make.size.mas_equalTo(CGSizeMake(100, 20));
        
    }];
    self.endAddress.textAlignment = NSTextAlignmentCenter;
    self.endAddress.font = Font(14);
    self.endAddress.text = @"杭州";
    
    //起始详情地址
    self.startDetailAddress = [[UILabel alloc]init];
    [self.contentView addSubview:self.startDetailAddress];
    [self.startDetailAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.startAddress.mas_bottom).offset(0);
        make.centerX.equalTo(weakSelf.startAddress);
        make.size.mas_equalTo(CGSizeMake(100, 20));
    }];
    self.startDetailAddress.textAlignment = NSTextAlignmentCenter;
    self.startDetailAddress.font = Font(14);
    self.startDetailAddress.text = @"北京八大处";
    
    //终止详情地址
    self.endDetailAddress = [[UILabel alloc]init];
    [self.contentView addSubview:self.endDetailAddress];
    [self.endDetailAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.endAddress.mas_bottom).offset(0);
        make.centerX.equalTo(weakSelf.endAddress);
        make.size.mas_equalTo(CGSizeMake(100, 20));
        
        
    }];
    self.endDetailAddress.textAlignment = NSTextAlignmentCenter;
    self.endDetailAddress.font = Font(14);
    self.endDetailAddress.text = @"杭州西湖";
    
    //起始时间
    
    self.startTimeL = [[UILabel alloc]init];
    [self.contentView addSubview:self.startTimeL];
    [self.startTimeL mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(weakSelf.startDetailAddress.mas_bottom).offset(0);
        make.centerX.equalTo(weakSelf.startDetailAddress);
        make.size.mas_equalTo(CGSizeMake(140, 20));
        
    }];
    self.startTimeL.textAlignment = NSTextAlignmentCenter;
    self.startTimeL.font = Font(14);
    self.startTimeL.text = @"2016-04-10 上午";

    
    //终止时间
    self.endTimeL = [[UILabel alloc]init];
    [self.contentView addSubview:self.endTimeL];
    [self.endTimeL mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(weakSelf.endDetailAddress.mas_bottom).offset(0);
        make.centerX.equalTo(weakSelf.endDetailAddress);
        make.size.mas_equalTo(CGSizeMake(140, 20));
        
    }];
    self.endTimeL.textAlignment = NSTextAlignmentCenter;
    self.endTimeL.font = Font(14);
    self.endTimeL.text = @"2016-04-14 上午";
    
    UILabel *lineL2 = [[UILabel alloc]init];
    [self.contentView addSubview:lineL2];
    [lineL2 mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(weakSelf.startTimeL.mas_bottom).offset(3);
        make.centerX.equalTo(weakSelf.contentView);
        make.size.mas_equalTo(CGSizeMake(screenWidth - 40, 0.5));
    }];
    
    lineL2.backgroundColor = GrayColor;
    
    
    //车型
    self.carStyleL = [[UILabel alloc]init];
    [self.contentView addSubview:self.carStyleL];
    [self.carStyleL mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(lineL2.mas_bottom).offset(3);
        make.left.mas_equalTo(20);
        make.size.mas_equalTo(CGSizeMake(screenWidth - 80, 20));
        
    }];
    
    self.carStyleL.textAlignment = NSTextAlignmentCenter;
    self.carStyleL.font = Font(14);
    self.carStyleL.text = @"沃尔沃XC90,沃尔沃V60,沃尔沃XCV40,沃尔沃S60,沃尔沃XC90(混合)";
    
    
    //个数
    self.numberL = [[UILabel alloc]init];
    [self.contentView addSubview:self.numberL];
    [self.numberL mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(lineL2.mas_bottom).offset(3);
        make.left.mas_equalTo(weakSelf.carStyleL.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(50, 20));

        
    }];
    self.numberL.textAlignment = NSTextAlignmentCenter;
    self.numberL.font = Font(14);
    self.numberL.text = @"共5辆";
    
    
    UILabel *lineL3 = [[UILabel alloc]init];
    [self.contentView addSubview:lineL3];
    [lineL3 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(weakSelf.carStyleL.mas_bottom).offset(4);
        make.centerX.equalTo(weakSelf.contentView);
        make.size.mas_equalTo(CGSizeMake(screenWidth - 28, 0.5));
    }];
    
    lineL3.backgroundColor = GrayColor;
    
    //删除按钮
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.deleteButton setTitle:@"删除" forState:UIControlStateNormal];
    [self.deleteButton setTitleColor:BlackColor forState:UIControlStateNormal];
    [self.contentView addSubview:self.deleteButton];
    [self.deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(lineL3.mas_bottom).offset(0);
        make.size.mas_equalTo(CGSizeMake(screenWidth/3, 35));
        
    }];
    self.deleteButton.titleLabel.font = Font(14);
    
    UIImageView *imageV1 = [[UIImageView alloc]init];
    [self.deleteButton.titleLabel sizeToFit];

    [self.deleteButton addSubview:imageV1];
    [imageV1 mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.right.mas_equalTo(weakSelf.deleteButton.titleLabel.mas_left).offset(-2);
        make.centerY.equalTo(weakSelf.deleteButton);
        make.size.mas_equalTo(CGSizeMake(20, 15));
        
    }];
    imageV1.backgroundColor = [UIColor redColor];
    
    
    
    UILabel *lineL4 = [[UILabel alloc]init];
    [self.contentView addSubview:lineL4];
    [lineL4 mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(weakSelf.deleteButton.mas_right).offset(0);
        make.centerY.equalTo(weakSelf.deleteButton);
        make.size.mas_equalTo(CGSizeMake(0.5, 35));
        
    }];
    lineL4.backgroundColor = GrayColor;
    
    
    //重新发布
    self.repostButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.repostButton setTitle:@"重新发布" forState:UIControlStateNormal];
    [self.repostButton setTitleColor:BlackColor forState:UIControlStateNormal];
    
    [self.contentView addSubview:self.repostButton];
    [self.repostButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(weakSelf.deleteButton.mas_right).offset(0.5);
        make.top.mas_equalTo(lineL3.mas_bottom).offset(0);
        make.size.mas_equalTo(CGSizeMake(screenWidth/3 - 1, 35));
        
    }];
    self.repostButton.titleLabel.font = Font(14);
    
    
    UIImageView *imageV3 = [[UIImageView alloc]init];
    [self.repostButton.titleLabel sizeToFit];
    [self.repostButton addSubview:imageV3];
    [imageV3 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(weakSelf.repostButton.titleLabel.mas_left).offset(-2);
        make.centerY.equalTo(weakSelf.repostButton);
        make.size.mas_equalTo(CGSizeMake(20, 15));
        
    }];
    imageV3.backgroundColor = [UIColor redColor];

    
    
    //详情按钮
    self.detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.detailButton setTitle:@"订单详情" forState:UIControlStateNormal];
    [self.detailButton setTitleColor:BlackColor forState:UIControlStateNormal];
    
    [self.contentView addSubview:self.detailButton];
     [self.detailButton mas_makeConstraints:^(MASConstraintMaker *make) {
    
         make.right.mas_equalTo(0);
         make.top.mas_equalTo(lineL3.mas_bottom).offset(0);
         make.size.mas_equalTo(CGSizeMake(screenWidth/3, 35));
         
    }];
    self.detailButton.titleLabel.font = Font(14);

    
    
    UIImageView *imageV2 = [[UIImageView alloc]init];
    [self.detailButton.titleLabel sizeToFit];
    [self.detailButton addSubview:imageV2];
    [imageV2 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(weakSelf.detailButton.titleLabel.mas_left).offset(-2);
        make.centerY.equalTo(weakSelf.deleteButton);
        make.size.mas_equalTo(CGSizeMake(20, 15));
        
    }];
    imageV2.backgroundColor = [UIColor redColor];

    
    
    UILabel *lineL5 = [[UILabel alloc]init];
    [self.contentView addSubview:lineL5];
    [lineL5 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(weakSelf.detailButton.mas_left).offset(0);
        make.centerY.equalTo(weakSelf.deleteButton);
        make.size.mas_equalTo(CGSizeMake(0.5, 35));
        
    }];
    lineL5.backgroundColor = GrayColor;
    
    
    
}

-(UILabel *)backLabelWithFrame:(CGRect)frame andFont:(int)a
{
    UILabel *label = [[UILabel alloc]init];
    label.frame = frame;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = Font(a);
    [self.contentView addSubview:label];
    
    return  label;
}

-(void)setModel:(MineOrderModel *)model
{
    if (_model != model) {
                

//        self.startAddress.text = [NSString stringWithFormat:@"%@%@",model.departProvinceName,model.departCityName];
//        
//        self.orderNumberL.text = model.orderCode;
//        
//        
//        
//        self.startDetailAddress.text = model.departUnit;
//        
//        self.endAddress.text = [NSString stringWithFormat:@"%@%@",model.receiptProvinceName,model.receiptCityName];
//        self.startDetailAddress.text = model.receiptUnit;
//        
//        self.startTimeL.text = [NSString stringWithFormat:@"%@ %@",[self backDateWithString:model.deliveryDate],model.deliveryTime];
//        self.endTimeL.text = [NSString stringWithFormat:@"%@ %@",[self backDateWithString:model.arriveDate],model.arriveTime];
//        
//        self.numberL.text = [NSString  stringWithFormat:@"共%d辆",model.amount];
//        
//        
//        NSMutableArray *array = [NSMutableArray array];
//        for ( int i = 0; i < model.vehicles.count; i++) {
//            NSString *string = [model.vehicles[i][@"vehicleName"] stringByAppendingString:model.vehicles[i][@"brandName"]];
//            [array addObject:string];
//        }
//        
//        
//        NSString *stringName = [NSString string];
//        for (int i = 0; i < array.count; i ++) {
//            NSString *string = [NSString stringWithFormat:@"%@",array[i]];
//            
//            stringName = [stringName stringByAppendingString:string];
//            
//        }
//        
//        self.carStyleL.text = stringName;
//        
    }
    

}

-(NSString *)backDateWithString:(NSString *)string
{
    NSDate *nd = [NSDate dateWithTimeIntervalSince1970:[string integerValue]/1000];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:nd];
    
    return dateString;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
