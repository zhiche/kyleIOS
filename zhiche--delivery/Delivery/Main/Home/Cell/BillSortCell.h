//
//  BillSortCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/12/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BillSortCell : UITableViewCell
@property (nonatomic,strong) UIImageView * leftImage;
@property (nonatomic,strong) UIImageView * RightImage;
@property (nonatomic,strong) UILabel * titleLabel;
@property (nonatomic,strong) UIView * line;


@end
