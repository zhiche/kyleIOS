//
//  QuoteListCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/6/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewBtn.h"

@interface QuoteListCell : UITableViewCell


@property (nonatomic,strong) UILabel * Destinationlabel;
@property (nonatomic,strong) UILabel * NowPlace;
@property (nonatomic,strong) UILabel * Timelabel;
@property (nonatomic,strong) UILabel * Namelabel;
@property (nonatomic,strong) UILabel * CarName;
@property (nonatomic,strong) UILabel * PhoneNumber;
@property (nonatomic,strong) UILabel * CarType;
@property (nonatomic,strong) UILabel * QuoteList;

@property (nonatomic,strong) NewBtn * QutoteBtn;
-(void)showInfo:(NSDictionary *)dic_info ;


@end
