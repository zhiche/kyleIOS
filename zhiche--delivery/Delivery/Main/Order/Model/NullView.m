//
//  NullView.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/7/19.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "NullView.h"

@implementation NullView

-(id)initWithFrame:(CGRect)frame andTitle:(NSString *)string
{
    if (self = [super init]) {
        
        self.frame = frame;
        
        self.backgroundColor = WhiteColor;
        
        UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMidX(self.frame) - 28, 95 , 56, 53)];
        imageV.image = [UIImage imageNamed:@"null_picture"];
        [self addSubview:imageV];
        
       self.label = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(imageV.frame) + 14, screenWidth, 20)];
        self.label.textColor = BtnTitleColor;
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.font = Font(13);
        [self addSubview:self.label];
        
        
    }
    
    return self;
}


@end
