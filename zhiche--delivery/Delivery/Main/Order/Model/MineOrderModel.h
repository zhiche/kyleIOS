//
//  MineOrderModel.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/19.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MineOrderModel : NSObject

@property (nonatomic,copy) NSString *ID;
@property (nonatomic,copy) NSString *orderCode;//订单号
@property (nonatomic,copy) NSString *departCityName;//发运市名称
@property (nonatomic,copy) NSString *departUnit;//发运单位
@property (nonatomic,copy) NSString *receiptCityName;//目的地市名称
@property (nonatomic,copy) NSString *receiptUnit;//目的地单位

@property (nonatomic,copy) NSString *createTime;//下单时间

@property (nonatomic,copy) NSString *dest;//同城地址
@property (nonatomic,copy) NSString *orgin;//同城地址
@property (nonatomic,assign) int isVeneer; //判断是否是同城

@property (nonatomic,copy) NSString *deliveryDate;//提车日期
@property (nonatomic,copy) NSString *arriveDate;//到达日期
@property (nonatomic,copy) NSString *actualCost;
@property (nonatomic,copy) NSString *depositCost;
@property (nonatomic,copy) NSString *unpaidCost;
@property (nonatomic,copy) NSString *paidCost;
@property (nonatomic,copy) NSString *needPayCost;
@property (nonatomic,copy) NSString *status;
@property (nonatomic,copy) NSString *payStatus;
@property (nonatomic,copy) NSString *payStatusText;
@property (nonatomic,copy) NSArray *vehicles;
@property (nonatomic,copy) NSString *detail;
@property (nonatomic,strong) NSArray *quotes;
@property (nonatomic,copy) NSString *quoteCounts;
@property (nonatomic,copy) NSString *postTimeRemaining;
@property (nonatomic,strong) NSArray *loadPics;
@property (nonatomic,strong) NSArray *unloadPics;
@property (nonatomic,strong) NSDictionary *feemap;
@property (nonatomic,copy) NSString *statusText;
@property (nonatomic,assign) int isValuation;


//
//@property (nonatomic,copy) NSString *orderCost;
//@property (nonatomic) int departAddrId;//发运地址id
//@property (nonatomic,copy) NSString *userId;
//@property (nonatomic,copy) NSString *departProvinceCode;//发运省编码
//@property (nonatomic,copy) NSString *departProvinceName;//发运省名称
//@property (nonatomic,copy) NSString *departCityCode;//发运市编码
//@property (nonatomic,copy) NSString *departCountyCode;//发运区/县编码
//@property (nonatomic,copy) NSString *departCountyName;//发运区/县名称
//
//@property (nonatomic,copy) NSString *departAddr;//发运详细地址
//@property (nonatomic,copy) NSString *departContact;//发运详细联系人
//@property (nonatomic,copy) NSString *departPhone;//发运联系电话
//@property (nonatomic) int receiptAddrId;//目的地地址id
//@property (nonatomic,copy) NSString *receiptProvinceCode;//目的地省编码
//@property (nonatomic,copy) NSString *receiptProvinceName;//目的地省名称
//@property (nonatomic,copy) NSString *receiptCityCode;//目的地市编码
//@property (nonatomic,copy) NSString *receiptCityName;//目的地市名称
//@property (nonatomic,copy) NSString *receiptCountyCode;//目的地区/县编码
//
//@property (nonatomic,copy) NSString *receiptCountyName;//目的地区/县名称
//@property (nonatomic,copy) NSString *receiptAddr;//目的地详细地址
//@property (nonatomic,copy) NSString *receiptContact;//目的地联系人
//@property (nonatomic,copy) NSString *receiptPhone;//目的地电话
//@property(nonatomic) int effectiveTime;//有效时间
//@property (nonatomic,copy) NSString *deliveryTime;//提车时间
//@property (nonatomic,copy) NSString *arriveTime;//到达时间
//
//@property (nonatomic) int amount;//车辆总数
//@property (nonatomic,copy) NSString *distance;//运距
//@property (nonatomic,copy) NSString *isUrgent;//是否加急
//@property (nonatomic,copy) NSString *adviceCost;//建议价格
//@property (nonatomic,copy) NSString *customCost;//自定义价格
//@property (nonatomic,copy) NSString *comment;//	备注
//@property (nonatomic,copy) NSString *createTime;
//@property (nonatomic,copy) NSString *updateTime;
//
//@property (nonatomic,copy) NSString *statusText;
//
//
//
//

@end
