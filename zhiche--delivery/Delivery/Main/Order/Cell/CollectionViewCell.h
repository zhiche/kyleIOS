//
//  CollectionViewCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/28.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *stringLabel;

@end
