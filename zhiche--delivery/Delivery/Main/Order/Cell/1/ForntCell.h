//
//  ForntCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/27.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "OrderRootCell.h"
#import "MineOrderModel.h"

@interface ForntCell : OrderRootCell

@property (nonatomic,strong) UIButton *payButton;//支付订金

@property (nonatomic,strong) UIButton *cancelButton;//取消订单

@property (nonatomic,strong) MineOrderModel *model;


@end
