//
//  ForntCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/27.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ForntCell.h"
#import "Masonry.h"

@implementation ForntCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubviews];
    }
    
    return self;
}

-(void)initSubviews
{
    __weak typeof(self) weakSelf = self;
    
//    CGRect rightFrame = CGRectMake(screenWidth - 70 * kWidth - 14, 180 * kHeight + 7 * kHeight, 70 * kWidth, buttonheight);
//    CGRect leftFrame = CGRectMake(screenWidth - 140 * kWidth - 21, 180 * kHeight + 7 * kHeight, 70 * kWidth, buttonheight);
    
    //用时label
//    self.timeL = [self backLabelWithFrame:CGRectMake(screenWidth - 15 - 50, 0,50 * kWidth,cellHeight) andFont:12];
//    self.timeL.text = @"20分钟";
//    self.timeL.textColor = RedColor;
//    [self.contentView addSubview:self.timeL];
//    
//    
//    UIImageView *timeImage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.timeL.frame) - 15, CGRectGetMidY(self.timeL.frame) - 7.5, 14, 14)];
//    [self.contentView addSubview:timeImage];
//    timeImage.image = [UIImage imageNamed:@"quote_cell_time"];
//    

    self.payButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.lastView addSubview:self.payButton];
    [self.payButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(7 * kWidth);
        make.right.mas_equalTo(-14);
        make.size.mas_equalTo(CGSizeMake(0 * kWidth, buttonheight));
    }];
    
    
    [self.payButton setTitle:@"立即支付" forState:UIControlStateNormal];
    [self.payButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    self.payButton.titleLabel.font = Font(13);
    self.payButton.backgroundColor = YellowColor;
    self.payButton.layer.cornerRadius = 5;
    
    
    
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.lastView addSubview:self.cancelButton];
    [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(7 * kWidth);
        make.right.mas_equalTo(weakSelf.payButton.mas_left).offset(0);
        make.size.mas_equalTo(CGSizeMake(71 * kWidth, buttonheight));
    }];
    
    
    [self.cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [self.cancelButton setTitleColor:carScrollColor forState:UIControlStateNormal];
    self.cancelButton.titleLabel.font = Font(13);
    self.cancelButton.backgroundColor = GrayColor;
    self.cancelButton.layer.cornerRadius = 5;
    
    
//
    
}



-(void)setModel:(MineOrderModel *)model
{
    if (_model != model) {
        
        //订单号
        self.orderNumberL.text = [self backString:model.orderCode];
        
        
        //起始地
        self.startAddress.text = [self backString: model.orgin];
        
//        //状态
//        self.statusLabel.text = [self backString: model.payStatusText];
//         剩余时间
//        self.timeL.text = [NSString stringWithFormat:@"%@分钟",model.postTimeRemaining];
//
        //起始地详细地址
//        self.startDetailAddress.text = [NSString stringWithFormat:@"%@",model.departUnit];
        
        //起始时间
        
        self.startTimeL.text = [self backString: model.deliveryDate];
        
        
//        NSLog(@"model.depaAddr%@model.destAddr%@",model.depaAddr,model.destAddr);
        //终止地
//         int isVeneer = model.isVeneer;
        
        //同城
//        if (isVeneer == 1) {
//            
//            self.endAddress.text = [NSString stringWithFormat:@"%@-%@",model.depaAddr,model.destAddr];
//        }else{
//            self.endAddress.text = [self backString: model.receiptCityName];
//        }
        
        self.endAddress.text = [self backString: model.dest];

        
        //终止地详细地址
//        self.endDetailAddress.text = [NSString stringWithFormat:@"%@",model.receiptUnit];
//
        //终止日期
        self.endTimeL.text = [self backString: model.arriveDate];
//
              
        self.carStyleL.text = [self backString: model.detail];
        self.createTime.text = [self backString:model.createTime];
//        for (int i = 0; i < model.vehicles.count; i ++) {
//            NSString *string = [NSString stringWithFormat:@"%@-%@-%@辆",model.vehicles[i][@"brandName"],model.vehicles[i][@"vehicleName"],model.vehicles[i][@"amount"]];
//            
//            self.carStyleL.text = [self.carStyleL.text stringByAppendingString:string];
//            self.carStyleL.text = [self.carStyleL.text stringByAppendingString:@"   "];
//            
//        }
//        
//        [self.carStyleL sizeToFit];
//        self.carStyleL.frame = CGRectMake(self.carStyleL.frame.origin.x, self.carStyleL.frame.origin.y, self.carStyleL.frame.size.width, cellHeight);
//        
//        self.scroll.contentSize = CGSizeMake(self.carStyleL.frame.size.width, cellHeight);
//
//        
    }
}



-(UILabel *)backLabelWithFrame:(CGRect)frame andFont:(int)a
{
    UILabel *label = [[UILabel alloc]init];
    label.frame = frame;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = Font(a);
    [self.contentView addSubview:label];
    
    return  label;
}



-(NSString *)backDateWithString:(NSString *)string
{
    NSDate *nd = [NSDate dateWithTimeIntervalSince1970:[string integerValue]/1000];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:nd];
    
    return dateString;
}


@end
