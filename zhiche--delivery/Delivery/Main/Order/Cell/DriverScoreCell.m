//
//  DriverScoreCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "DriverScoreCell.h"
#import <Masonry.h>

@implementation DriverScoreCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubviews];
    }
    
    return self;
}

-(void)initSubviews
{
    __weak typeof(self) weakSelf = self;
    self.phoneLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.phoneLabel];
    [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(18);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(22.5 * kHeight);
        
    }];
    self.phoneLabel.font = Font(12);
    self.phoneLabel.textColor = littleBlackColor;
    self.phoneLabel.text = @"123123123123";
    
    self.judgeLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.judgeLabel];
    [self.judgeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.equalTo(weakSelf.phoneLabel);
        make.top.mas_equalTo(weakSelf.phoneLabel.mas_bottom).offset(0);
        make.height.mas_equalTo(22.5 * kHeight);
        make.width.mas_equalTo(screenWidth - 100);
        make.bottom.mas_equalTo(0);
        
    }];
    self.judgeLabel.numberOfLines = 0;
    self.judgeLabel.font = Font(12);
    self.judgeLabel.textColor = carScrollColor;
    self.judgeLabel.text = @"1.衣带渐宽终不悔，为伊消得人憔悴.－柳永《凤栖梧》2 .死生契阔，与子成悦.执子之手，与子偕老.－佚名《诗经邶风击鼓》3 .两情若是久长时，又岂在朝朝暮暮.－秦观《鹊桥仙》4 .相思相见知何日？此时此夜难为情.－李白《三五七言》 5 .有美人兮，见之不忘，一日不见兮，思之如狂.－佚名《凤求凰琴歌》6 .这次我离开你，是风，是雨，是夜晚；你笑了笑，我摆一摆手，一条寂寞的路便展向两头了.－郑愁予《赋别》";
    
    
    self.timeLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(weakSelf.phoneLabel.mas_right).offset(15);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(150);
        make.height.equalTo(weakSelf.phoneLabel);
        
    }];
    self.timeLabel.font = Font(12);
    self.timeLabel.textColor = littleBlackColor;
    self.timeLabel.text = @"08-13 11:22";
    
    self.starImg = [[UIImageView alloc]init];
    self.starImg.image = [UIImage imageNamed:@"idea_product_list_stars_null"];
    [self.contentView addSubview:self.starImg];
    [self.starImg mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.right.mas_equalTo(-18);
        make.centerY.mas_equalTo(weakSelf);
        make.size.mas_equalTo(CGSizeMake(47.5, 7.5));
        
    }];
    
    
}

-(void)setModel:(DriverScoreModel *)model
{

}

+(CGFloat)cellHeightWithModel:(DriverScoreModel *)model
{
    DriverScoreCell *cell = [[DriverScoreCell alloc]init];
    
    cell.judgeLabel.text = model.contact;
    
    CGRect rect = [model.contact boundingRectWithSize:CGSizeMake(screenWidth - 100, 200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12 * kWidth]} context:nil];
    
    if (rect.size.height > 22.5 * kHeight) {
        return  22.5 * kHeight + rect.size.height + 15;
    } else {
        return 45 * kHeight;
    }
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
