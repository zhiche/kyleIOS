//
//  LogisticalCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/7/14.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "LogisticalCell.h"

@implementation LogisticalCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubviews];
    }
    
    return self;
}

-(void)initSubviews
{
    
    self.titleL = [[UILabel alloc]initWithFrame:CGRectMake(14, 12 * kHeight, 300 * kWidth, 14 * kHeight)];
    self.titleL.font = Font(13);
    self.titleL.textColor = carScrollColor;
    [self.contentView addSubview:self.titleL];
    
    self.dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(14,CGRectGetMaxY(self.titleL.frame) + 10 , 65 * kWidth, 10 * kHeight)];
    self.dateLabel.font = Font(10);
    self.dateLabel.textColor = carScrollColor;
    
    self.timeL = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.dateLabel.frame),CGRectGetMaxY(self.titleL.frame) + 10 , 100, 10 * kHeight)];
    self.timeL.font = Font(10);
    self.timeL.textColor = carScrollColor;
    
    [self.contentView addSubview:self.titleL];
    [self.contentView addSubview:self.dateLabel];
    [self.contentView addSubview:self.timeL];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
