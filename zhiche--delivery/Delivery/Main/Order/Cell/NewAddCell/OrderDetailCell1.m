//
//  OrderDetailCell1.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/22.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "OrderDetailCell1.h"
#import <Masonry.h>

@implementation OrderDetailCell1

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
    }
    
    return  self;
    
}

-(void)initSubViews
{
    __weak typeof(self) weakSelf = self;
    
    self.contentView.backgroundColor = GrayColor;
    UIView *view = [[UIView alloc]init];
    [self.contentView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(screenWidth - 10, cellHeight * 3));
        make.left.mas_equalTo(5);
        make.top.mas_equalTo(10);
    }];
    view.backgroundColor = WhiteColor;
    
    
    //图片
    UIImageView *imageV = [[UIImageView alloc]init];
    [view addSubview:imageV];
    
    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(18);
        make.top.mas_equalTo((cellHeight - 13)/2.0);
        make.size.mas_equalTo(CGSizeMake(13, 13.5));
    }];
    imageV.image = [UIImage imageNamed:@"order_number"];
    
    //订单号
    UILabel *lable = [[UILabel alloc]init];
    [view addSubview:lable];
    [lable mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(imageV.mas_right).offset(15);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(70, cellHeight));
        
    }];
    lable.text = @"订单号:";
    lable.textColor = littleBlackColor;
    lable.font = Font(12);
    
    self.orderNumberL = [[UILabel alloc]init];
    [view addSubview:self.orderNumberL];
    [self.orderNumberL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lable.mas_right).offset(0);
        make.centerY.equalTo(lable);
        make.size.mas_equalTo(CGSizeMake(200, cellHeight));
    }];
    self.orderNumberL.textColor = littleBlackColor;
    self.orderNumberL.text = @"123123123";
    self.orderNumberL.font = Font(12);
    
    //状态
    self.statusLabel = [[UILabel alloc]init];
    [view addSubview:self.statusLabel];
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.right.mas_equalTo(-18);
        make.centerY.mas_equalTo(weakSelf.orderNumberL);
        make.size.mas_equalTo(CGSizeMake(150, cellHeight));
    }];
    
    self.statusLabel.textColor = RedColor;
    self.statusLabel.text = @"已完成";
    self.statusLabel.font = Font(12);
    self.statusLabel.textAlignment = NSTextAlignmentRight;
    
    UILabel *lineL1 = [[UILabel alloc]init];
    [self.contentView addSubview:lineL1];
    [lineL1 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(weakSelf.orderNumberL.mas_bottom).offset(0);
        make.centerX.equalTo(weakSelf.contentView);
        make.size.mas_equalTo(CGSizeMake(screenWidth - 10, 0.5));
    }];
    
    lineL1.backgroundColor = LineGrayColor;
    
    self.scroll = [[UIScrollView alloc]init];
    [self.contentView addSubview:self.scroll];
    [self.scroll mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(lineL1.mas_bottom).offset(0);
        make.left.mas_equalTo(20 );
        make.size.mas_equalTo(CGSizeMake(screenWidth - 40, cellHeight));
        
    }];
    self.scroll.contentSize = CGSizeMake(screenWidth + 100, cellHeight);
    self.scroll.alwaysBounceHorizontal = YES;
    self.scroll.showsHorizontalScrollIndicator = NO;
    
    
    //车型
    self.carStyleL = [[UILabel alloc]init];
    [self.scroll addSubview:self.carStyleL];
    [self.carStyleL mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(screenWidth + 100, cellHeight));
        
    }];
    
    self.carStyleL.font = Font(13);
    self.carStyleL.textColor = carScrollColor;
    self.carStyleL.text = @"沃尔沃XC90  沃尔沃V60  沃尔沃XCV40  沃尔沃S60";


    
    UILabel *lineL3 = [[UILabel alloc]init];
    [self.contentView addSubview:lineL3];
    [lineL3 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(weakSelf.carStyleL.mas_bottom).offset(0);
        make.centerX.equalTo(weakSelf.contentView);
        make.size.mas_equalTo(CGSizeMake(screenWidth - 10 , 0.5));
    }];
    
    lineL3.backgroundColor = LineGrayColor;
    
    self.lastView = [[UIView alloc]init];
//    self.lastView.backgroundColor = headerBottomColor;
    [self.contentView addSubview:self.lastView];
    [self.lastView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(5);
        make.top.mas_equalTo(lineL3.mas_bottom).offset(0);
        make.size.mas_equalTo(CGSizeMake(screenWidth - 10, cellHeight));
        
    }];

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
