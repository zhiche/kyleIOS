//
//  OrderDetailCell2.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/22.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "OrderDetailCell1.h"

@interface OrderDetailCell2 : OrderDetailCell1

@property (nonatomic,strong) UILabel *carNumber;

@end
