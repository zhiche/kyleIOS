//
//  OrderDetailCell2.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/22.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "OrderDetailCell2.h"
#import  <Masonry.h>


@implementation OrderDetailCell2


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubviews];
    }
    
    return self;
}

-(void)initSubviews
{
  
    __weak typeof(self) weakSelf = self;

    self.carNumber = [[UILabel alloc]init];
    [self.lastView addSubview:self.carNumber];
    [self.carNumber mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-18);
        make.centerY.mas_equalTo(weakSelf.lastView);
        make.size.mas_equalTo(CGSizeMake(30 * kWidth, cellHeight));
        
    }];
    self.carNumber.font = Font(13);
    self.carNumber.textColor = BlackTitleColor;
    self.carNumber.text = @"10辆";
    
    
    UILabel *label = [[UILabel alloc]init];
    [self.lastView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(weakSelf.carNumber);
        make.right.mas_equalTo(weakSelf.carNumber.mas_left).offset(0);
        make.size.mas_equalTo(CGSizeMake(100, cellHeight));
        
    }];
    
    label.textColor = BlackTitleColor;
    label.textAlignment = NSTextAlignmentRight;
    label.text = @"车辆数:";
    label.font = Font(13);
    
    
}
    //    __w
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
