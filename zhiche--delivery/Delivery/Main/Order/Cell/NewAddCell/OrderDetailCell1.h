//
//  OrderDetailCell1.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/22.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailCell1 : UITableViewCell

@property (nonatomic,strong) UILabel *orderNumberL;//订单编号
@property (nonatomic,strong) UILabel *statusLabel;//状态
@property (nonatomic,strong) UIScrollView *scroll;
@property (nonatomic,strong) UILabel *carStyleL;
@property (nonatomic,strong) UIView *lastView;

@end
