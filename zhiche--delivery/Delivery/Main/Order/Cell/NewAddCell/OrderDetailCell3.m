//
//  OrderDetailCell3.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/22.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "OrderDetailCell3.h"
#import <Masonry.h>

@implementation OrderDetailCell3

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubviews];
    }
    
    return self;
}

-(void)initSubviews
{
    
    __weak typeof(self) weakSelf = self;
    
    self.judgeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.lastView addSubview:self.judgeButton];
    [self.judgeButton mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(7 * kWidth);
        make.right.mas_equalTo(-18);
        make.size.mas_equalTo(CGSizeMake(71 * kWidth, buttonheight));
        
    }];
    [self.judgeButton setTitle:@"评价" forState:UIControlStateNormal];
    [self.judgeButton setTitleColor:YellowColor forState:UIControlStateNormal];
    self.judgeButton.layer.cornerRadius = 5;
    self.judgeButton.titleLabel.font = Font(14);
    self.judgeButton.backgroundColor = rearrangeBtnColor;
    self.judgeButton.layer.borderColor = YellowColor.CGColor;
    self.judgeButton.layer.borderWidth = 0.5;
    
    
    self.carNumber = [[UILabel alloc]init];
    [self.lastView addSubview:self.carNumber];
    [self.carNumber mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(weakSelf.judgeButton.mas_left).offset(-18);
        make.centerY.mas_equalTo(weakSelf.lastView);
        make.size.mas_equalTo(CGSizeMake(30 * kWidth, cellHeight));
        
    }];
    self.carNumber.font = Font(13);
    self.carNumber.textColor = BlackTitleColor;
    self.carNumber.text = @"10辆";
    
    
    UILabel *label = [[UILabel alloc]init];
    [self.lastView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(weakSelf.carNumber);
        make.right.mas_equalTo(weakSelf.carNumber.mas_left).offset(0);
        make.size.mas_equalTo(CGSizeMake(100, cellHeight));
        
    }];
    
    label.textColor = BlackTitleColor;
    label.textAlignment = NSTextAlignmentRight;
    label.text = @"车辆数:";
    label.font = Font(13);
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
