//
//  FinishOrderCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/27.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "FinishOrderCell.h"
#import "Masonry.h"

@implementation FinishOrderCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubviews];
    }
    
    return self;
}

-(void)initSubviews
{

    //物流
    self.trackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.lastView addSubview:self.trackButton];
    [self.trackButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(7 * kWidth);
        make.right.mas_equalTo(-14);
        make.size.mas_equalTo(CGSizeMake(71 * kWidth, buttonheight));
    }];
    
    
    [self.trackButton setTitle:@"物流跟踪" forState:UIControlStateNormal];
    [self.trackButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    self.trackButton.titleLabel.font = Font(13);
    self.trackButton.backgroundColor = YellowColor;
    self.trackButton.layer.cornerRadius = 5;

    //交车
    
    self.getButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
//    [self.lastView addSubview:self.getButton];
//    [self.getButton mas_makeConstraints:^(MASConstraintMaker *make) {
////        make.top.mas_equalTo(7 * kWidth);
//        make.centerY.equalTo(self.trackButton);
//        make.right.mas_equalTo(self.trackButton.mas_left).offset(-14);
//        make.size.equalTo(self.trackButton);
////        make.size.mas_equalTo(CGSizeMake(71 * kWidth, buttonheight));
//    }];
    
    
    [self.getButton setTitle:@"交车信息" forState:UIControlStateNormal];
    [self.getButton setTitleColor:YellowColor forState:UIControlStateNormal];
    self.getButton.titleLabel.font = Font(13);
    self.getButton.layer.cornerRadius = 5;
    self.getButton.layer.borderColor = YellowColor.CGColor;
    self.getButton.layer.borderWidth = 0.5;
    
    //提车
    
    self.detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
//    [self.lastView addSubview:self.detailButton];
//    [self.detailButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        //        make.top.mas_equalTo(7 * kWidth);
//        make.centerY.equalTo(self.trackButton);
//        make.right.mas_equalTo(self.getButton.mas_left).offset(-14);
//        make.size.equalTo(self.trackButton);
//        //        make.size.mas_equalTo(CGSizeMake(71 * kWidth, buttonheight));
//    }];
    
    
    [self.detailButton setTitle:@"提车信息" forState:UIControlStateNormal];
    [self.detailButton setTitleColor:YellowColor forState:UIControlStateNormal];
    self.detailButton.titleLabel.font = Font(13);
    self.detailButton.layer.cornerRadius = 5;
    self.detailButton.layer.borderColor = YellowColor.CGColor;
    self.detailButton.layer.borderWidth = 0.5;
    
 
}

-(void)setModel:(MineOrderModel *)model
{
    if (_model != model) {
        
        //订单号
        self.orderNumberL.text = [self backString: model.orderCode];
        //起始地
        self.startAddress.text = [self backString: model.orgin];

        //状态
//        self.statusLabel.text = [self backString: model.statusText];

        
        //起始地详细地址
//        self.startDetailAddress.text = [NSString stringWithFormat:@"%@",model.departUnit];
        
        //起始时间
        
        self.startTimeL.text = [self backString: model.deliveryDate];
        //终止地
//        self.endAddress.text = [self backString: model.receiptCityName];
        self.endAddress.text = [self backString: model.dest];

        //终止地详细地址
//        self.endDetailAddress.text = [NSString stringWithFormat:@"%@",model.receiptUnit];
        
        //终止日期
        self.endTimeL.text = [self backString: model.arriveDate];
        self.carStyleL.text = [self backString: model.detail];
        self.createTime.text = [self backString:model.createTime];

//        //车
        
        
//        self.carStyleL.text = @"";
//        for (int i = 0; i < model.vehicles.count; i ++) {
//            NSString *string = [NSString stringWithFormat:@"%@-%@-%@辆",model.vehicles[i][@"brandName"],model.vehicles[i][@"vehicleName"],model.vehicles[i][@"amount"]];
//            
//            self.carStyleL.text = [self.carStyleL.text stringByAppendingString:string];
//            self.carStyleL.text = [self.carStyleL.text stringByAppendingString:@"   "];
//            
//        }
//        
//        [self.carStyleL sizeToFit];
//        self.carStyleL.frame = CGRectMake(self.carStyleL.frame.origin.x, self.carStyleL.frame.origin.y, self.carStyleL.frame.size.width, cellHeight);
//        
//        self.scroll.contentSize = CGSizeMake(self.carStyleL.frame.size.width, cellHeight);
        
        
    }
}


-(NSString *)backDateWithString:(NSString *)string
{
    NSDate *nd = [NSDate dateWithTimeIntervalSince1970:[string integerValue]/1000];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:nd];
    
    return dateString;
}



@end
