//
//  OrderRootCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/27.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "OrderRootCell.h"
#import "Masonry.h"

#define marWidth 80 * kWidth

@implementation OrderRootCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
        
    }
    
    return  self;
}

-(void)initSubViews
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight)];
    view.backgroundColor = headerBottomColor;
    [self.contentView addSubview:view];
    
    UILabel *numberL = [[UILabel alloc]initWithFrame:CGRectMake(18 , 0, 60, cellHeight)];
    [view addSubview:numberL];
    numberL.font = Font(12);
    numberL.textColor = littleBlackColor;
    numberL.text = @"订单号:";
    
    
    //订单编号
    self.orderNumberL = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(numberL.frame) , 0, 200, cellHeight) andFont:12];
    self.orderNumberL.text = @"OD201604190001";
    self.orderNumberL.textAlignment = NSTextAlignmentLeft;
    self.orderNumberL.textColor = littleBlackColor;

    [view addSubview:self.orderNumberL];
    
    
    
//    self.checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [self.checkButton setTitle:@"评价" forState:UIControlStateNormal];
//    [self.checkButton setTitleColor:WhiteColor forState:UIControlStateNormal];
//    self.checkButton.titleLabel.font = Font(14);
//    self.checkButton.backgroundColor = YellowColor;
//    self.checkButton.layer.cornerRadius = 5;
//    
//    [view addSubview:self.checkButton];
//    [self.checkButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(self.orderNumberL);
//        make.right.mas_equalTo(-14);
//        make.size.mas_equalTo(CGSizeMake(71 * kWidth, buttonheight));
//    }];

    
    
  
//    //用时label
//    self.timeL = [self backLabelWithFrame:CGRectMake(screenWidth - 15 - 50, 0,50 * kWidth,cellHeight) andFont:12];
//    self.timeL.text = @"20分钟";
//    self.timeL.textColor = RedColor;
//    [view addSubview:self.timeL];
//    
//
//    UIImageView *timeImage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.timeL.frame) - 7, (cellHeight - 15)/2.0, 14, 14)];
//    timeImage.backgroundColor = [UIColor cyanColor];
//    [view addSubview:timeImage];
//    timeImage.image = [UIImage imageNamed:@"quote_cell_time"];
    
//    self.statusLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 14 - 60,0,60 * kWidth,cellHeight) andFont:12];
//    self.statusLabel.textColor = RedColor;
//    self.statusLabel.backgroundColor = WhiteColor;
//    [view addSubview:self.statusLabel];
//    self.statusLabel.text = @"未付款";
    
    __weak typeof(self) weakSelf = self;

    
    UILabel *lineL1 = [[UILabel alloc]init];
    [self.contentView addSubview:lineL1];
    [lineL1 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(weakSelf.orderNumberL.mas_bottom).offset(0);
        make.centerX.equalTo(weakSelf.contentView);
        make.size.mas_equalTo(CGSizeMake(screenWidth , 0.5));
    }];
    
    lineL1.backgroundColor = LineGrayColor;


    //起始地址
    self.startAddress = [[UILabel alloc]init];
    [self.contentView addSubview:self.startAddress];
    [self.startAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(view.mas_bottom).offset(9 * kHeight);
        
        make.left.mas_equalTo(10 * kWidth);
        
        make.size.mas_equalTo(CGSizeMake(marWidth + 20 * kWidth, 15 * kHeight));
        
    }];
    self.startAddress.textAlignment = NSTextAlignmentCenter;
    self.startAddress.font = Font(12);
    self.startAddress.textColor = littleBlackColor;
//    self.startAddress.text = @"北京";
    self.startAddress.numberOfLines = 0;
    
    //终止地址
    self.endAddress = [[UILabel alloc]init];

    [self.contentView addSubview:self.endAddress];
    [self.endAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(view.mas_bottom).offset(9 * kHeight);
        make.right.mas_equalTo(-10 * kWidth);
        make.size.mas_equalTo(CGSizeMake(marWidth + 20 * kWidth, 15 * kHeight));
        
    }];
    self.endAddress.textAlignment = NSTextAlignmentCenter;
    self.endAddress.font = Font(12);
    self.endAddress.textColor = littleBlackColor;
    self.endAddress.numberOfLines = 0;
    
//    self.endAddress.text = @"杭州";
    
    //起始时间
    
    self.startTimeL = [[UILabel alloc]init];
    [self.contentView addSubview:self.startTimeL];
    [self.startTimeL mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(weakSelf.startAddress.mas_bottom).offset(9 * kHeight);
        make.left.mas_equalTo(20 * kWidth);
        make.size.mas_equalTo(CGSizeMake(marWidth, 15 * kHeight));
        
    }];
    self.startTimeL.textAlignment = NSTextAlignmentCenter;
    self.startTimeL.font = Font(10);
    self.startTimeL.textColor = littleBlackColor;

//    self.startTimeL.text = @"2016-04-10 ";


    
    //终止时间
    self.endTimeL = [[UILabel alloc]init];
    [self.contentView addSubview:self.endTimeL];
    [self.endTimeL mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(weakSelf.endAddress.mas_bottom).offset(9 * kHeight);
        make.right.mas_equalTo(-20 * kWidth);
        make.size.mas_equalTo(CGSizeMake(marWidth, 15 * kHeight));
        
    }];
    self.endTimeL.textAlignment = NSTextAlignmentCenter;
    self.endTimeL.font = Font(10);
    self.endTimeL.textColor = littleBlackColor;

//    self.endTimeL.text = @"2016-04-14 ";
    

    
  
    
    /*
    //起始详情地址
    self.startDetailAddress = [[UILabel alloc]init];
    self.startDetailAddress.adjustsFontSizeToFitWidth = YES;
    [self.contentView addSubview:self.startDetailAddress];
    [self.startDetailAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.startAddress.mas_bottom).offset(0 * kHeight);
        make.centerX.equalTo(weakSelf.startAddress);
        make.size.mas_equalTo(CGSizeMake(marWidth, 28 * kHeight));
    }];
    self.startDetailAddress.textAlignment = NSTextAlignmentCenter;
    self.startDetailAddress.font = Font(9);
    self.startDetailAddress.textColor = littleBlackColor;
    self.startDetailAddress.numberOfLines = 0;
    self.startDetailAddress.text = @"北京八大处北京八大处";
       
    //终止详情地址
    self.endDetailAddress = [[UILabel alloc]init];
    self.endDetailAddress.adjustsFontSizeToFitWidth = YES;
    [self.contentView addSubview:self.endDetailAddress];
    [self.endDetailAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.endAddress.mas_bottom).offset(0 * kHeight);
        make.centerX.equalTo(weakSelf.endAddress);
        make.size.mas_equalTo(CGSizeMake(marWidth, 28 * kHeight));
        
        
    }];
    self.endDetailAddress.textAlignment = NSTextAlignmentCenter;
    self.endDetailAddress.font = Font(9);
    self.endDetailAddress.textColor = littleBlackColor;
    self.endDetailAddress.text = @"杭州西湖";
    self.endDetailAddress.numberOfLines = 0;
    
     */
    
    
    UIImageView *imageV = [[UIImageView alloc]init];
    imageV.image = [UIImage imageNamed:@"Quote_Cell_logo"];
    [self.contentView addSubview:imageV];
    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.centerX.equalTo(weakSelf.contentView);
//        make.centerY.equalTo(weakSelf.startAddress);
        make.top.mas_equalTo(view.mas_bottom).offset(22 * kHeight);
        make.size.mas_equalTo(CGSizeMake(47, 12.5));
        
    }];
    
    
    
    UILabel *lineL2 = [[UILabel alloc]init];
    [self.contentView addSubview:lineL2];
    [lineL2 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(weakSelf.startTimeL.mas_bottom).offset(8 * kHeight);
        make.centerX.equalTo(weakSelf.contentView);
        make.size.mas_equalTo(CGSizeMake(screenWidth , 0.5));
    }];
    
    lineL2.backgroundColor = LineGrayColor;
    
    
//   self.scroll = [[UIScrollView alloc]init];
//    [self.contentView addSubview:self.scroll];
//    [self.scroll mas_makeConstraints:^(MASConstraintMaker *make) {
//       
//        make.top.mas_equalTo(lineL2.mas_bottom).offset(0);
//        make.left.mas_equalTo(20 );
//        make.size.mas_equalTo(CGSizeMake(screenWidth - 40, cellHeight));
//
//    }];
//    self.scroll.contentSize = CGSizeMake(screenWidth + 100, cellHeight);
//    self.scroll.alwaysBounceHorizontal = YES;
//    self.scroll.showsHorizontalScrollIndicator = NO;
//    
    
    //车型
    self.carStyleL = [[UILabel alloc]init];
    [self.contentView addSubview:self.carStyleL];
    [self.carStyleL mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.top.mas_equalTo(lineL2.mas_bottom).offset(0);
        make.left.mas_equalTo(20 );
        make.size.mas_equalTo(CGSizeMake(screenWidth - 40, cellHeight));
        
//        make.top.mas_equalTo(0);
//        make.left.mas_equalTo(0);
//        make.size.mas_equalTo(CGSizeMake(screenWidth + 100, cellHeight));
        
    }];
    
    self.carStyleL.font = Font(13);
    self.carStyleL.textColor = carScrollColor;
//    self.carStyleL.text = @"3个品牌 10台车";
    
    
    self.createTime = [self createUIlabel:@"" andFont:FontOfSize13 andColor:carScrollColor];
    [self.contentView addSubview:self.createTime];
    [self.createTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.carStyleL.mas_centerY);
        make.right.equalTo(self.contentView.mas_right).with.offset(-15);
    }];

    
    UILabel *lineL3 = [[UILabel alloc]init];
    [self.contentView addSubview:lineL3];
    [lineL3 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(weakSelf.carStyleL.mas_bottom).offset(0);
        make.centerX.equalTo(weakSelf.contentView);
        make.size.mas_equalTo(CGSizeMake(screenWidth , 0.5));
    }];
    
    lineL3.backgroundColor = LineGrayColor;
    
    self.lastView = [[UIView alloc]init];
    self.lastView.backgroundColor = headerBottomColor;
    [self.contentView addSubview:self.lastView];
    [self.lastView mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(lineL3.mas_bottom).offset(0);
        make.size.mas_equalTo(CGSizeMake(screenWidth, cellHeight));
        
    }];
    
        
    
}

-(NSString *)backString:(NSString *)string
{
    
    if ([string isEqual:[NSNull null]]) {
        
        return @"";
        
    } else {
        return  string;
    }
    
}

-(UILabel *)backLabelWithFrame:(CGRect)frame andFont:(int)a
{
    UILabel *label = [[UILabel alloc]init];
    label.frame = frame;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = Font(a);
    [self.contentView addSubview:label];
    
    return  label;
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    // 名字的H
    CGFloat nameH = size.height;
    // 名字的W
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentRight;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
