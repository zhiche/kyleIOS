//
//  ExecuteCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/27.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "OrderRootCell.h"
#import "MineOrderModel.h"

@interface ExecuteCell : OrderRootCell

//@property (nonatomic,strong) UIButton *cancelButton;//取消订单
//@property (nonatomic,strong) UIButton *topayButton;//支付订金
//@property (nonatomic,strong) UIButton *payLastMoneyButton;//支付余额

@property (nonatomic,strong) UIButton *trackButton;//物流跟踪
@property (nonatomic,strong) UIButton *getButton;//交车信息

@property (nonatomic,strong) UIButton *confirmButton;//提车信息

@property (nonatomic,strong) MineOrderModel *model;

@end
