//
//  ExecuteCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/27.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ExecuteCell.h"
#import "Masonry.h"

@implementation ExecuteCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubviews];
    }
    
    return self;
}

-(void)initSubviews
{
//    __weak typeof(self) weakSelf = self;
//    
//    CGRect rightFrame = CGRectMake(screenWidth - 70 * kWidth - 14, 180 * kHeight + 7 * kHeight, 70 * kWidth, buttonheight);
//    CGRect leftFrame = CGRectMake(screenWidth - 140 * kWidth - 21, 180 * kHeight + 7 * kHeight, 70 * kWidth, buttonheight);

    
//    //取消
//    self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.cancelButton.frame = leftFrame;
//    
//    [self.cancelButton setTitle:@"取消订单" forState:UIControlStateNormal];
//    [self.cancelButton setTitleColor:Color_RGB(149, 149, 149, 1) forState:UIControlStateNormal];
//    self.cancelButton.titleLabel.font = Font(14);
//    self.cancelButton.backgroundColor = Color_RGB(240, 240, 240, 1);
//    self.cancelButton.layer.borderColor = Color_RGB(149, 149, 149, 1).CGColor;
//    self.cancelButton.layer.borderWidth = 0.5;
//    self.cancelButton.layer.cornerRadius = 5;
//    
//      
//    //支付订金
//
//    self.topayButton = [UIButton buttonWithType:UIButtonTypeCustom];
//
//    self.topayButton.frame = rightFrame;
//    
//    [self.topayButton setTitle:@"支付订金" forState:UIControlStateNormal];
//    [self.topayButton setTitleColor:WhiteColor forState:UIControlStateNormal];
//    self.topayButton.titleLabel.font = Font(14);
//    self.topayButton.backgroundColor = YellowColor;
//    self.topayButton.layer.cornerRadius = 5;
//    
//    //支付余额
//
//    self.payLastMoneyButton = [UIButton buttonWithType:UIButtonTypeCustom];
//
//    self.payLastMoneyButton.frame = rightFrame;
//    
//    [self.payLastMoneyButton setTitle:@"支付余额" forState:UIControlStateNormal];
//    [self.payLastMoneyButton setTitleColor:WhiteColor forState:UIControlStateNormal];
//    self.payLastMoneyButton.titleLabel.font = Font(14);
//    self.payLastMoneyButton.backgroundColor = YellowColor;
//    self.payLastMoneyButton.layer.cornerRadius = 5;
//    //
    
//物流
    self.trackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.lastView addSubview:self.trackButton];
    [self.trackButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(7 * kWidth);
        make.right.mas_equalTo(-14);
        make.size.mas_equalTo(CGSizeMake(71 * kWidth, buttonheight));
    }];
    
    
    [self.trackButton setTitle:@"物流跟踪" forState:UIControlStateNormal];
    [self.trackButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    self.trackButton.titleLabel.font = Font(13);
    self.trackButton.backgroundColor = YellowColor;
    self.trackButton.layer.cornerRadius = 5;
    
    //交车
    
    self.getButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
//    [self.lastView addSubview:self.getButton];
//    [self.getButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(self.trackButton);
//        make.size.equalTo(self.trackButton);
////        make.top.mas_equalTo(7 * kWidth);
//        make.right.mas_equalTo(self.trackButton.mas_left).offset(-14);
////        make.size.mas_equalTo(CGSizeMake(71 * kWidth, buttonheight));
//    }];
    
    
    [self.getButton setTitle:@"交车信息" forState:UIControlStateNormal];
    [self.getButton setTitleColor:YellowColor forState:UIControlStateNormal];
    self.getButton.layer.borderColor = YellowColor.CGColor;
    self.getButton.layer.borderWidth = 0.5;
    self.getButton.titleLabel.font = Font(13);
    self.getButton.layer.cornerRadius = 5;
    
    //提车信息
    
    self.confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
//    [self.lastView addSubview:self.confirmButton];
//    [self.confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(self.trackButton);
//        make.size.equalTo(self.trackButton);
//        //        make.top.mas_equalTo(7 * kWidth);
//        make.right.mas_equalTo(self.getButton.mas_left).offset(-14);
//        //        make.size.mas_equalTo(CGSizeMake(71 * kWidth, buttonheight));
//    }];
    
    
    [self.confirmButton setTitle:@"提车信息" forState:UIControlStateNormal];
    [self.confirmButton setTitleColor:YellowColor forState:UIControlStateNormal];
    self.confirmButton.titleLabel.font = Font(13);
    self.confirmButton.layer.borderWidth = 0.5;
    self.confirmButton.layer.borderColor = YellowColor.CGColor;
    self.confirmButton.layer.cornerRadius = 5;

}

-(void)setModel:(MineOrderModel *)model
{
    if (_model != model) {
        
                    
//            if ([model.statusText isEqualToString:@"已确定"]) {
//            
//                [self.contentView addSubview:self.cancelButton];
//                [self.contentView addSubview:self.topayButton];
//
//            
//            } else {
//                [self.contentView addSubview:self.payLastMoneyButton];
//                UILabel *label = [self.contentView viewWithTag:503];
//                label.hidden = YES;
//            }

        
        //订单号
        self.orderNumberL.text = [self backString: model.orderCode];
        //起始地
        self.startAddress.text = [self backString: model.orgin];
        

        //起始地详细地址
//        self.startDetailAddress.text = [NSString stringWithFormat:@"%@",model.departUnit];
        
        //状态
//        self.statusLabel.text = [self backString: model.statusText];
        
        //起始时间
        
        self.startTimeL.text = [self backString: model.deliveryDate];
        //终止地
//        self.endAddress.text = [self backString: model.receiptCityName];
        self.endAddress.text = [self backString: model.dest];

        //终止地详细地址
//        self.endDetailAddress.text = [NSString stringWithFormat:@"%@",model.receiptUnit];
        
        //终止日期
        self.endTimeL.text = [self backString: model.arriveDate];
        
        self.carStyleL.text = [self backString: model.detail];
        self.createTime.text = [self backString:model.createTime];

//        //车
        
//        self.carStyleL.text = @"";
//        for (int i = 0; i < model.vehicles.count; i ++) {
//            NSString *string = [NSString stringWithFormat:@"%@-%@-%@辆",model.vehicles[i][@"brandName"],model.vehicles[i][@"vehicleName"],model.vehicles[i][@"amount"]];
//            
//            self.carStyleL.text = [self.carStyleL.text stringByAppendingString:string];
//            self.carStyleL.text = [self.carStyleL.text stringByAppendingString:@"   "];
//            
//        }
//        
//        [self.carStyleL sizeToFit];
//        self.carStyleL.frame = CGRectMake(self.carStyleL.frame.origin.x, self.carStyleL.frame.origin.y, self.carStyleL.frame.size.width, cellHeight);
//        
//        self.scroll.contentSize = CGSizeMake(self.carStyleL.frame.size.width, cellHeight);

        
    }
}


-(NSString *)backDateWithString:(NSString *)string
{
    NSDate *nd = [NSDate dateWithTimeIntervalSince1970:[string integerValue]/1000];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:nd];
    
    return dateString;
}



@end
