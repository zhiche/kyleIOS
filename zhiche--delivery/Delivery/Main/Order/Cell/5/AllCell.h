//
//  AllCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/9/23.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "OrderRootCell.h"
#import "MineOrderModel.h"


@interface AllCell : OrderRootCell
@property (nonatomic,strong) UIButton *trackButton;//物流跟踪
@property (nonatomic,strong) UIButton *getButton;//交车信息

@property (nonatomic,strong) UIButton *giveButton;//提车信息
@property (nonatomic,strong) UIButton *payButton;//支付按钮
@property (nonatomic,strong) UIButton *cancelButton;//取消

@property (nonatomic,strong) UIButton *deleteButton;//删除
@property (nonatomic,strong) UIButton *publishButton;//发布
@property (nonatomic,strong) UIButton *republishButton;//重新发布


@property (nonatomic,strong) MineOrderModel *model;
@end
