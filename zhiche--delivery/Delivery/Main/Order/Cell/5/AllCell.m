//
//  AllCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/9/23.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "AllCell.h"
#import <Masonry.h>

@implementation AllCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubviews];
    }
    
    return self;
}

-(void)initSubviews
{
   
    
    CGRect rightFrame = CGRectMake(screenWidth - 71 * kWidth - 14,  6.5 * kHeight, 71 * kWidth, buttonheight);
     CGRect middleFrame = CGRectMake(screenWidth - (71 * kWidth + 14) * 2,  6.5 * kHeight, 71 * kWidth, buttonheight);
     CGRect leftFrame = CGRectMake(screenWidth - (71 * kWidth + 14) * 3,  6.5 * kHeight, 71 * kWidth, buttonheight);
    
    
    self.getButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.getButton.frame = middleFrame;

    [self.getButton setTitle:@"交车信息" forState:UIControlStateNormal];
    [self.getButton setTitleColor:YellowColor forState:UIControlStateNormal];
    self.getButton.layer.borderColor = YellowColor.CGColor;
    self.getButton.layer.borderWidth = 0.5;
    self.getButton.titleLabel.font = Font(13);
    self.getButton.layer.cornerRadius = 5;
    
    
    
    self.giveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.giveButton.frame = leftFrame;
    
    [self.giveButton setTitle:@"提车信息" forState:UIControlStateNormal];
    [self.giveButton setTitleColor:YellowColor forState:UIControlStateNormal];
    self.giveButton.layer.borderColor = YellowColor.CGColor;
    self.giveButton.layer.borderWidth = 0.5;
    self.giveButton.titleLabel.font = Font(13);
    self.giveButton.layer.cornerRadius = 5;
    
    
    
    self.trackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.trackButton.frame = rightFrame;
    
    [self.trackButton setTitle:@"物流跟踪" forState:UIControlStateNormal];
    [self.trackButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    self.trackButton.titleLabel.font = Font(13);
    self.trackButton.backgroundColor = YellowColor;
    self.trackButton.layer.cornerRadius = 5;
//
    
    self.payButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.payButton.frame = rightFrame;
    
    [self.payButton setTitle:@"立即支付" forState:UIControlStateNormal];
    [self.payButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    self.payButton.titleLabel.font = Font(13);
    self.payButton.backgroundColor = YellowColor;
    self.payButton.layer.cornerRadius = 5;
    
    
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.cancelButton.frame = rightFrame;
    
    [self.cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [self.cancelButton setTitleColor:carScrollColor forState:UIControlStateNormal];
//    self.cancelButton.layer.borderColor = YellowColor.CGColor;
//    self.cancelButton.layer.borderWidth = 0.5;
    self.cancelButton.backgroundColor = GrayColor;
    self.cancelButton.titleLabel.font = Font(13);
    self.cancelButton.layer.cornerRadius = 5;
    
    
    
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.deleteButton.frame = rightFrame;
    
    [self.deleteButton setTitle:@"删除" forState:UIControlStateNormal];
    [self.deleteButton setTitleColor:carScrollColor forState:UIControlStateNormal];
//        self.cancelButton.layer.borderColor = YellowColor.CGColor;
//        self.cancelButton.layer.borderWidth = 0.5;
    self.deleteButton.backgroundColor = GrayColor;
    self.deleteButton.titleLabel.font = Font(13);
    self.deleteButton.layer.cornerRadius = 5;
    
    
    self.publishButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.publishButton.frame = middleFrame;
    
    [self.publishButton setTitle:@"发布" forState:UIControlStateNormal];
    [self.publishButton setTitleColor:YellowColor forState:UIControlStateNormal];
        self.publishButton.layer.borderColor = YellowColor.CGColor;
        self.publishButton.layer.borderWidth = 0.5;
    self.publishButton.titleLabel.font = Font(13);
    self.publishButton.layer.cornerRadius = 5;
    
    self.republishButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.republishButton.frame = middleFrame;
    
    [self.republishButton setTitle:@"重新发布" forState:UIControlStateNormal];
    [self.republishButton setTitleColor:YellowColor forState:UIControlStateNormal];
        self.republishButton.layer.borderColor = YellowColor.CGColor;
        self.republishButton.layer.borderWidth = 0.5;
    self.republishButton.backgroundColor = GrayColor;
    self.republishButton.titleLabel.font = Font(13);
    self.republishButton.layer.cornerRadius = 5;

    
    
    
    
}


-(void)setModel:(MineOrderModel *)model
{
    if (_model != model) {
        
        //10  取消
        // 0 100 发布 删除
        //取消
        if ([model.status integerValue] == 10 ) {
            
//            [self.lastView addSubview:self.payButton];
            [self.lastView addSubview:self.cancelButton];
            
        } else if([model.status integerValue] == 0){
            
            [self.lastView addSubview:self.publishButton];
            [self.lastView addSubview:self.deleteButton];
            
        }else if ([model.status integerValue] == 100) {
            
            [self.lastView addSubview:self.republishButton];
            [self.lastView addSubview:self.deleteButton];
            
        } else
        
        {
        
//            [self.lastView addSubview:self.getButton];
//            [self.lastView addSubview:self.giveButton];
            [self.lastView addSubview:self.trackButton];
        }
         
        //订单号
        self.orderNumberL.text = [self backString: model.orderCode];
        //起始地
        self.startAddress.text = [self backString: model.orgin];

        //状态
//                self.statusLabel.text = [self backString: model.statusText];
        
        
        //起始地详细地址
        //        self.startDetailAddress.text = [NSString stringWithFormat:@"%@",model.departUnit];
        
        //起始时间
        
        
        self.startTimeL.text = [self backString:  model.deliveryDate];
        
        //终止地
//        self.endAddress.text = [self backString: model.receiptCityName];
        self.endAddress.text = [self backString: model.dest];

        //终止地详细地址
        //        self.endDetailAddress.text = [NSString stringWithFormat:@"%@",model.receiptUnit];
        
        //终止日期
        
        self.endTimeL.text = [self backString: model.arriveDate];
        self.carStyleL.text = [self backString: model.detail];
        self.createTime.text = [self backString:model.createTime];

        
        
    }
}


-(NSString *)backDateWithString:(NSString *)string
{
    NSDate *nd = [NSDate dateWithTimeIntervalSince1970:[string integerValue]/1000];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:nd];
    
    return dateString;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
