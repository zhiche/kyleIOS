//
//  DriverScoreCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DriverScoreModel.h"


@interface DriverScoreCell : UITableViewCell

@property (nonatomic,strong) UILabel *phoneLabel;
@property (nonatomic,strong) UILabel *timeLabel;
@property (nonatomic,strong) UILabel *judgeLabel;
@property (nonatomic,strong) UIImageView *starImg;
@property (nonatomic,strong) DriverScoreModel *model;

+(CGFloat)cellHeightWithModel:(DriverScoreModel *)model;


@end
