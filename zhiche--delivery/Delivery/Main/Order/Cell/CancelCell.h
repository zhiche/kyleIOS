//
//  CancelCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/6/28.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CancelCell : UITableViewCell

@property  (nonatomic,strong) UILabel *reasonLabel;
@property (nonatomic,strong) UIImageView *selectImg;

@end
