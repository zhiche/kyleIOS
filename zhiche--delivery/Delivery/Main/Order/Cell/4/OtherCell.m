//
//  OtherCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/27.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "OtherCell.h"
#import "Masonry.h"

@implementation OtherCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubviews];
    }
    
    return self;
}

-(void)initSubviews
{
    
//    self.lastView.backgroundColor = [UIColor clearColor];
//    [self.lastView removeFromSuperview];
    
//    __weak typeof(self) weakSelf = self;
//
//    CGRect rightFrame = CGRectMake(screenWidth - 70 * kWidth - 14, 180 * kHeight + 7 * kHeight, 70 * kWidth, buttonheight);
//    CGRect leftFrame = CGRectMake(screenWidth - 140 * kWidth - 21, 180 * kHeight + 7 * kHeight, 70 * kWidth, buttonheight);
    
    CGRect middleFrame = CGRectMake(screenWidth - (71 * kWidth + 14) * 2,  6.5 * kHeight, 71 * kWidth, buttonheight);

    self.checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.checkButton setTitle:@"未评价" forState:UIControlStateNormal];
    [self.checkButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    self.checkButton.titleLabel.font = Font(14);
    self.checkButton.backgroundColor = YellowColor;
    self.checkButton.layer.cornerRadius = 5;
//
    self.checkButton.frame = middleFrame;
    [self.lastView addSubview:self.checkButton];
//    [self.checkButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(7 * kWidth);
//        make.right.mas_equalTo(-14);
//        make.size.mas_equalTo(CGSizeMake(71 * kWidth, buttonheight));
//    }];

    /*
     
    
    
    
    self.complainButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.lastView addSubview:self.complainButton];
    [self.complainButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(7 * kWidth);
        make.centerY.equalTo(self.checkButton);
        make.right.mas_equalTo(self.checkButton.mas_left).offset(-14);
        make.size.equalTo(self.checkButton);
//        make.size.mas_equalTo(CGSizeMake(71 * kWidth, buttonheight));
    }];
    
    
    [self.complainButton setTitle:@"投诉" forState:UIControlStateNormal];
    [self.complainButton setTitleColor:YellowColor forState:UIControlStateNormal];
    self.complainButton.titleLabel.font = Font(14);
    self.complainButton.layer.borderWidth = 0.5;
    self.complainButton.layer.borderColor = YellowColor.CGColor;
    self.complainButton.layer.cornerRadius = 5;
*/
    
    
    //物流
    self.trackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.lastView addSubview:self.trackButton];
    [self.trackButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(7 * kWidth);
        make.right.mas_equalTo(-14);
        make.size.mas_equalTo(CGSizeMake(71 * kWidth, buttonheight));
    }];
    
    
    [self.trackButton setTitle:@"物流跟踪" forState:UIControlStateNormal];
    [self.trackButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    self.trackButton.titleLabel.font = Font(13);
    self.trackButton.backgroundColor = YellowColor;
    self.trackButton.layer.cornerRadius = 5;
    
    //交车
    
    self.getButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
//    [self.lastView addSubview:self.getButton];
//    [self.getButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        //        make.top.mas_equalTo(7 * kWidth);
//        make.centerY.equalTo(self.trackButton);
//        make.right.mas_equalTo(self.trackButton.mas_left).offset(-14);
//        make.size.equalTo(self.trackButton);
//        //        make.size.mas_equalTo(CGSizeMake(71 * kWidth, buttonheight));
//    }];
    
    
    [self.getButton setTitle:@"交车信息" forState:UIControlStateNormal];
    [self.getButton setTitleColor:YellowColor forState:UIControlStateNormal];
    self.getButton.titleLabel.font = Font(13);
    self.getButton.layer.cornerRadius = 5;
    self.getButton.layer.borderColor = YellowColor.CGColor;
    self.getButton.layer.borderWidth = 0.5;
    
    //提车
    
    self.detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
//    [self.lastView addSubview:self.detailButton];
//    [self.detailButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        //        make.top.mas_equalTo(7 * kWidth);
//        make.centerY.equalTo(self.trackButton);
//        make.right.mas_equalTo(self.getButton.mas_left).offset(-14);
//        make.size.equalTo(self.trackButton);
//        //        make.size.mas_equalTo(CGSizeMake(71 * kWidth, buttonheight));
//    }];
    
    
    [self.detailButton setTitle:@"提车信息" forState:UIControlStateNormal];
    [self.detailButton setTitleColor:YellowColor forState:UIControlStateNormal];
    self.detailButton.titleLabel.font = Font(13);
    self.detailButton.layer.cornerRadius = 5;
    self.detailButton.layer.borderColor = YellowColor.CGColor;
    self.detailButton.layer.borderWidth = 0.5;
    

    
}


-(void)setModel:(MineOrderModel *)model
{
    if (_model != model) {
        
//        if (![model.statusText isEqualToString:@"已保存"]) {
//           
//            [self.contentView addSubview:self.checkButton];
//           
//        } else {
//            [self.contentView addSubview:self.checkButton];
////            [self.contentView addSubview:self.repostButton];
//        }
        
        
        //订单号
        self.orderNumberL.text = [self backString: model.orderCode];
        //起始地
        self.startAddress.text = [self backString: model.orgin];

        if (model.isValuation == 1) {
            [self.checkButton setTitle:@"查看评价" forState:UIControlStateNormal];

        }else{
            [self.checkButton setTitle:@"评价" forState:UIControlStateNormal];
        }
        
        
        
        //状态
//        self.statusLabel.text = [self backString: model.statusText];

        
        //起始地详细地址
//        self.startDetailAddress.text = [NSString stringWithFormat:@"%@",model.departUnit];
        
        //起始时间
        

        self.startTimeL.text = [self backString: model.deliveryDate];

        //终止地
//        self.endAddress.text = [self backString: model.receiptCityName];
        self.endAddress.text = [self backString: model.dest];

        //终止地详细地址
//        self.endDetailAddress.text = [NSString stringWithFormat:@"%@",model.receiptUnit];
        
        //终止日期

        self.endTimeL.text = [self backString: model.arriveDate];
        self.carStyleL.text = [self backString: model.detail];
        self.createTime.text = [self backString:model.createTime];

     
//        //车
        
//        self.carStyleL.text = @"";
//        for (int i = 0; i < model.vehicles.count; i ++) {
//            NSString *string = [NSString stringWithFormat:@"%@-%@-%@辆",model.vehicles[i][@"brandName"],model.vehicles[i][@"vehicleName"],model.vehicles[i][@"amount"]];
//            
//            self.carStyleL.text = [self.carStyleL.text stringByAppendingString:string];
//            self.carStyleL.text = [self.carStyleL.text stringByAppendingString:@"   "];
//            
//        }
//        
//        [self.carStyleL sizeToFit];
//        self.carStyleL.frame = CGRectMake(self.carStyleL.frame.origin.x, self.carStyleL.frame.origin.y, self.carStyleL.frame.size.width, cellHeight);
//        
//        self.scroll.contentSize = CGSizeMake(self.carStyleL.frame.size.width, cellHeight);
        
        
    }
}


-(NSString *)backDateWithString:(NSString *)string
{
    NSDate *nd = [NSDate dateWithTimeIntervalSince1970:[string integerValue]/1000];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:nd];
    
    return dateString;
}



@end
