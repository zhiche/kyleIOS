//
//  LogisticalCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/7/14.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogisticalCell : UITableViewCell

@property (nonatomic,strong) UILabel *titleL;
@property (nonatomic,strong) UILabel *dateLabel;
@property (nonatomic,strong) UILabel *timeL;
@end
