//
//  CollectionReusableView.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/28.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionReusableView : UICollectionReusableView
@property (nonatomic,retain) UILabel *titleLabel;

@end
