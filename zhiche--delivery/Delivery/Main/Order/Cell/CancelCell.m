//
//  CancelCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/6/28.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "CancelCell.h"

@implementation CancelCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubviews];
    }
    
    return self;
}

-(void)initSubviews
{
    self.reasonLabel = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, 150, cellHeight)];
    self.reasonLabel.font = Font(14);
    self.reasonLabel.textColor = littleBlackColor;
    [self.contentView addSubview:self.reasonLabel];
    
    
    self.selectImg = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth - 29, (cellHeight - 15)/2.0, 15, 15)];
    self.selectImg.image = [UIImage imageNamed:@"pay_unselectd"];
    [self.contentView addSubview:self.selectImg];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
