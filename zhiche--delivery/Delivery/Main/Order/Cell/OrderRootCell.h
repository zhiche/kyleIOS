//
//  OrderRootCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/27.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderRootCell : UITableViewCell

@property (nonatomic,strong) UILabel *orderNumberL;//订单编号
@property (nonatomic,strong) UILabel *timeL;//时间

@property (nonatomic,strong) UILabel *statusLabel;

@property (nonatomic,strong) UILabel *startAddress;//起始地点
//@property (nonatomic,strong) UILabel *startDetailAddress;//起始详细地点

@property (nonatomic,strong) UILabel *endAddress;//终止地点
//@property (nonatomic,strong) UILabel *endDetailAddress;//终止详细地点

@property (nonatomic,strong) UILabel *startTimeL;//起始时间
@property (nonatomic,strong) UILabel *endTimeL;//终止时间

@property (nonatomic,strong) UILabel *carStyleL;//车型
@property (nonatomic,strong) UILabel *createTime;//创建时间
@property (nonatomic,strong) UIView *lastView;//放button

//@property (nonatomic,strong) UIScrollView *scroll;
-(NSString *)backString:(NSString *)string;

@end
