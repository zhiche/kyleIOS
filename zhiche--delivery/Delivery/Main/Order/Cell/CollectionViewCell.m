//
//  CollectionViewCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/28.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

//    self.imageView.layer.cornerRadius = 5;
//    self.imageView.layer.masksToBounds = YES;
    
    self.stringLabel.font = Font(13);
    self.stringLabel.textColor = fontGrayColor;

}

@end
