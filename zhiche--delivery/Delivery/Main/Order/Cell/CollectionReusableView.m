//
//  CollectionReusableView.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/28.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "CollectionReusableView.h"

@implementation CollectionReusableView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addAllView];
    }
    return self;
}

-(void)addAllView
{
    self.backgroundColor = WhiteColor;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0,  - 0.5, screenWidth, 0.5)];
    label.backgroundColor = LineGrayColor;
    [self addSubview:label];

    
    self.titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(18, 0,screenWidth, cellHeight)];
    _titleLabel.font = Font(14);
    _titleLabel.textColor = littleBlackColor;
    [self addSubview:_titleLabel];
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight - 0.5, screenWidth, 0.5)];
    label1.backgroundColor = LineGrayColor;
    [self addSubview:label1];
}
@end
