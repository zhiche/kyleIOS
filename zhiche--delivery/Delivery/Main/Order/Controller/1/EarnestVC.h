//
//  EarnestVC.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/6/28.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EarnestVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>



@property (nonatomic,strong) UIScrollView *scrollView;

@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) NSMutableDictionary *imageDictionary;
@property (nonatomic,copy) NSString *orderId;



/**
 订单
 */
@property (nonatomic,strong) UILabel *numberLabel;//订单编号
@property (nonatomic,strong) UILabel *statusLabel;//状态
@property (nonatomic,strong) UILabel *startLabel;//起始地
@property (nonatomic,strong) UILabel *startDetailLabel;//详细地址
@property (nonatomic,strong) UILabel *startDate;//起始日期
@property (nonatomic,strong) UILabel *endLabel;//终止地址
@property (nonatomic,strong) UILabel *endDetailLabel;//详细地址
@property (nonatomic,strong) UILabel *endDateLabel;//终止日期
@property (nonatomic,strong) UILabel *carLabel;

/**
 承运人
 */
@property (nonatomic,strong) UILabel *distanceLabel;//行程
@property (nonatomic,strong) UILabel *nameLabel;//姓名
@property (nonatomic,strong) UILabel *carNumberLabel;//车牌
@property (nonatomic,strong) UILabel *phoneLabel;//电话
@property (nonatomic,strong) UILabel *carStyleLabel;//车型

/**
 运费
 */

@property (nonatomic,strong) UIView *priceView;

@property (nonatomic,strong) UILabel *payLabel;//已付定金

@property (nonatomic,strong) UIButton *logisticalButton;//查看物流信息

@property (nonatomic,strong) UIScrollView *scrollV;




@end
