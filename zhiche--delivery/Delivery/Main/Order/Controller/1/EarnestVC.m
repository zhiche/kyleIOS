//
//  EarnestVC.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/6/28.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "EarnestVC.h"
#import "TopBackView.h"
#import "RootViewController.h"
#import "CollectionReusableView.h"
#import "CollectionViewCell.h"
#import <UIImageView+WebCache.h>
#import "LogisticalViewController.h"
#import "ScrollImageViewController.h"

#define labelHeight 15 * kHeight
#define labelWidth 60 * kWidth
#define leftMargn 18


@interface EarnestVC ()



@end

@implementation EarnestVC

- (void)viewDidLoad {
    [super viewDidLoad];

    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"订单详情"];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;

    self.imageDictionary = [NSMutableDictionary dictionary];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = GrayColor;
    
    [self initSubviews];

    
}

-(void)initSubviews
{
    
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight)];
    self.scrollView.contentSize = CGSizeMake(screenWidth, screenHeight + 1);
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = GrayColor;
    
    /**
     
     订单信息
     */
    UIView *orderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 177 * kHeight)];
    [self.scrollView addSubview:orderView];
    orderView.backgroundColor = WhiteColor;
    
    UIView *orderV1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight)];
    [orderView addSubview:orderV1];
    orderV1.backgroundColor = headerBottomColor;
    
    
    //订单号
    UILabel *orderL = [self backLabelWithFrame:CGRectMake(leftMargn, 0, 60, cellHeight) andString:@"订单号:"];
    orderL.textColor = littleBlackColor;
    [orderV1 addSubview:orderL];
    
    self.numberLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(orderL.frame) + 5, CGRectGetMinY(orderL.frame), 200, cellHeight) andString:@""];
    self.numberLabel.textColor = littleBlackColor;
    [orderV1 addSubview:self.numberLabel];
    
    //状态栏
    self.statusLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 14 - 100, CGRectGetMinY(orderV1.frame), 100, cellHeight) ];
    self.statusLabel.textColor = RedColor;
    self.statusLabel.text = @"已交车";
    self.statusLabel.textAlignment = NSTextAlignmentRight;
    //    [orderView addSubview:self.statusLabel];
    
    
    UILabel *labe = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight, screenWidth, 0.5)];
    labe.backgroundColor = LineGrayColor;
    [orderView addSubview:labe];
    
    //起始日期
    self.startDate = [self backLabelWithFrame:CGRectMake(20 * kWidth, CGRectGetMaxY(labe.frame) + 12*kHeight, labelWidth, labelHeight)];
    self.startDate.text = @"";
    self.startDate.textColor = littleBlackColor;
    self.startDate.font = Font(10);
    [orderView addSubview:self.startDate];
    
    
    //起始地址
    self.startLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMinX(self.startDate.frame) - 20,  CGRectGetMaxY(self.startDate.frame) + 9*kHeight, labelWidth + 40, labelHeight) ];
    self.startLabel.text = @"";
    self.startLabel.font = Font(14)
    [orderView addSubview:self.startLabel];
    
    //起始详情地址
    self.startDetailLabel = [self backLabelWithFrame:CGRectMake(20 * kHeight, CGRectGetMaxY(self.startLabel.frame) , labelWidth, 28 * kHeight)];
    self.startDetailLabel.text = @"";
    self.startDetailLabel.adjustsFontSizeToFitWidth = YES;
    [orderView addSubview:self.startDetailLabel];
    self.startDetailLabel.font = Font(9);
    self.startDetailLabel.numberOfLines = 0;
    
    
    //图片
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMidX(self.view.frame) - 30, CGRectGetMidY(self.startLabel.frame) - 13, 47, 12.5)];
    [orderView addSubview:imageView];
    imageView.image = [UIImage imageNamed:@"Quote_Cell_logo"];
    
    
    
    //目的日期
    self.endDateLabel = [self backLabelWithFrame:CGRectMake(screenWidth - labelWidth  - 20 * kWidth, CGRectGetMaxY(labe.frame) + 12*kHeight, labelWidth, labelHeight)];
    self.endDateLabel.text = @"";
    [orderView addSubview:self.endDateLabel];
    self.endDateLabel.font = Font(10);
    
    //目的地
    self.endLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMinX(self.endDateLabel.frame) - 20, CGRectGetMaxY(self.endDateLabel.frame) + 9*kHeight, labelWidth + 40, labelHeight)];
    [orderView addSubview:self.endLabel];
    self.endLabel.text = @"";
    self.endLabel.font = Font(14);
    
    
    //目的详情地址
    self.endDetailLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMinX(self.endDateLabel.frame), CGRectGetMaxY(self.endLabel.frame) +  0 * kHeight, labelWidth, 28 * kHeight)];
    self.endDetailLabel.adjustsFontSizeToFitWidth = YES;
    self.endDetailLabel.text = @"";
    [orderView addSubview:self.endDetailLabel];
    self.endDetailLabel.font = Font(9);
    self.endDetailLabel.numberOfLines = 0;
    
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.endDetailLabel.frame) + 12 * kHeight, screenWidth , 0.5)];
    label.backgroundColor = LineGrayColor;
    [orderView addSubview:label];
    
    
    self.scrollV = [[UIScrollView alloc]initWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(label.frame), screenWidth - leftMargn * 2, cellHeight)];
    self.scrollV.contentSize = CGSizeMake(screenWidth + 140, cellHeight);
    self.scrollV.alwaysBounceHorizontal = YES;
    self.scrollV.showsHorizontalScrollIndicator = NO;
    [orderView addSubview:self.scrollV];
    
    self.carLabel = [self backLabelWithFrame:CGRectMake(0, 0, screenWidth + 140, cellHeight) ];
    self.carLabel.text = @"";
    self.carLabel.textColor = carScrollColor;
    self.carLabel.textAlignment = NSTextAlignmentLeft;
    self.carLabel.font = Font(14);
    [self.scrollV addSubview:self.carLabel];
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.carLabel.frame), screenWidth, 0.5)];
    [orderView addSubview:label1];
    label1.backgroundColor = LineGrayColor;
    
    
    /**
     
     承运人信息
     */
    
    
    UIView *deleverView = [self backViewWithFrame:CGRectMake(0, CGRectGetMaxY(orderView.frame) + 10, screenWidth, cellHeight * 5)];
    [self.scrollView addSubview:deleverView];
    deleverView.backgroundColor = WhiteColor;
    
    UIView *delever1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight)];
    [deleverView addSubview:delever1];
    delever1.backgroundColor = headerBottomColor;
    
    //查看物流信息
    self.logisticalButton = [UIButton buttonWithType:UIButtonTypeCustom];
//
    _logisticalButton.frame = CGRectMake(screenWidth - 15 - 60 * kWidth, 11 * kHeight, 60 * kWidth, cellHeight - 22*kHeight);
    [_logisticalButton setTitle:@"物流信息" forState:UIControlStateNormal];
    _logisticalButton.titleLabel.font = Font(11);
    [delever1 addSubview:_logisticalButton];
    [_logisticalButton setTitleColor:carScrollColor forState:UIControlStateNormal];
    _logisticalButton.layer.borderColor = Color_RGB(173, 173, 173, 1).CGColor;
    _logisticalButton.layer.borderWidth = 0.5;
    _logisticalButton.layer.cornerRadius = 5;
    [_logisticalButton addTarget:self action:@selector(logisticalButtons) forControlEvents:UIControlEventTouchUpInside];
    
    
    UILabel *deleverLabel = [self backLabelWithFrame:CGRectMake(leftMargn, 0, 100 * kWidth, cellHeight) andString:@"承运人"];
    deleverLabel.font = Font(14);
    deleverLabel.textColor = RGBACOLOR(34, 34, 34, 1);
    [delever1 addSubview:deleverLabel];
    
    
    UILabel *deleverL1 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(deleverLabel.frame), screenWidth, 0.5)];
    deleverL1.backgroundColor = LineGrayColor;
    [delever1 addSubview:deleverL1];
    
    //姓名
    
    UILabel *nameL = [self backLabelWithFrame:CGRectMake(leftMargn,  CGRectGetMaxY(deleverL1.frame), 60, cellHeight) andString:@"姓名"];
    [deleverView addSubview:nameL];
    nameL.textColor = fontGrayColor;
    
    self.nameLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 114, CGRectGetMinY(nameL.frame), 100, cellHeight) andString:@""];
    self.nameLabel.textAlignment = NSTextAlignmentRight;
    self.nameLabel.textColor = Color_RGB(34, 34, 34, 1);
    [deleverView addSubview:self.nameLabel];
    
    
    
    UILabel *deleverL2 = [self backLineLabelWithFloat: CGRectGetMaxY(nameL.frame)];
    
    [deleverView addSubview:deleverL2];
    
    
    //车牌
    UILabel *carNumberL = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(nameL.frame), 60, cellHeight) andString:@"车牌"];
    [deleverView addSubview:carNumberL];
    carNumberL.textColor = fontGrayColor;
    
    self.carNumberLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 114, CGRectGetMinY(carNumberL.frame) + 1, 100, cellHeight) andString:@""];
    self.carNumberLabel.textAlignment = NSTextAlignmentRight;
    self.carNumberLabel.textColor = Color_RGB(34, 34, 34, 1);
    [deleverView addSubview:self.carNumberLabel];
    
    
    UILabel *deleverL3 = [self backLineLabelWithFloat:CGRectGetMaxY(carNumberL.frame)];
    
    [deleverView addSubview:deleverL3];
    
    //电话
    
    UILabel *phoneL = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(carNumberL.frame), 60, cellHeight) andString:@"电话"];
    [deleverView addSubview:phoneL];
    phoneL.textColor = fontGrayColor;
    
    self.phoneLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 214, CGRectGetMinY(phoneL.frame) + 1, 200, cellHeight) andString:@""];
    self.phoneLabel.textAlignment = NSTextAlignmentRight;
    self.phoneLabel.textColor = Color_RGB(34, 34, 34, 1);
    [deleverView addSubview:self.phoneLabel];
    
    
    UILabel *deleverL4 = [self backLineLabelWithFloat:CGRectGetMaxY(phoneL.frame) ];
    [deleverView addSubview:deleverL4];
    
    
    
    //车型
    
    UILabel *carStyle = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(phoneL.frame), 60, cellHeight) andString:@"车型"];
    [deleverView addSubview:carStyle];
    carStyle.textColor = fontGrayColor;
    
    self.carStyleLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 114, CGRectGetMinY(carStyle.frame), 100, cellHeight) andString:@""];
    self.carStyleLabel.textColor = Color_RGB(34, 34, 34, 1);
    self.carStyleLabel.textAlignment = NSTextAlignmentRight;
    [deleverView addSubview:self.carStyleLabel];
    

    //运费
    self.priceView = [self backViewWithFrame:CGRectMake(0, CGRectGetMaxY(deleverView.frame) + 10, screenWidth, cellHeight * 2)];
    [self.scrollView addSubview:self.priceView];
    self.priceView.backgroundColor = WhiteColor;
    
    UIView *priceV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight)];
    priceV.backgroundColor = headerBottomColor;
    [self.priceView addSubview:priceV];
    
    UILabel *priceL = [self backLabelWithFrame:CGRectMake(leftMargn, 0, 100 * kWidth, cellHeight) andString:@"运费"];
    priceL.font = Font(14);
    priceL.textColor = RGBACOLOR(34, 34, 34, 1);
    [priceV addSubview:priceL];
    
    UILabel *priceL1 = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight, screenWidth, 0.5)];
    priceL1.backgroundColor = LineGrayColor;
    
    [priceV addSubview:priceL1];

    
    UILabel *payLabel = [self backLabelWithFrame:CGRectMake(leftMargn, cellHeight, 60, cellHeight) andString:@"已支付"];
    payLabel.font = Font(13);
    [self.priceView addSubview:payLabel];
    
    
    self.payLabel = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 15 - 80 * kWidth , cellHeight, 80 * kWidth, cellHeight)];
    self.payLabel.font = Font(13);
    self.payLabel.textAlignment = NSTextAlignmentRight;
    self.payLabel.textColor = YellowColor;
    self.payLabel.text = @"￥";
    [self.priceView addSubview:self.payLabel];
    
      
    [self initCollectionView];
    
    
}


//初始化collectionview
-(void)initCollectionView
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(75 , 105 );
    
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.priceView.frame) + 10, screenWidth, [self.imageDictionary.allKeys count] * (cellHeight + 130 )) collectionViewLayout:layout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CollectionViewCell"];
    
    [self.collectionView registerClass:[CollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView"];
    
    self.collectionView.backgroundColor = WhiteColor;
    self.scrollView.contentSize = CGSizeMake(screenWidth, screenHeight );
    
    
    [self.scrollView addSubview:self.collectionView];
}


-(UILabel *)backLineLabelWithFloat:(CGFloat)y
{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, y + 1, screenWidth - 18, 0.5)];
    label.backgroundColor = LineGrayColor;
    return label;
}


-(UIView *)backViewWithFrame:(CGRect)frame
{
    UIView *view = [[UIView alloc]initWithFrame:frame];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, frame.size.height - 0.5, screenWidth , 0.5)];
    label.backgroundColor = littleBlackColor;
    //    [view addSubview:label];
    
    return view;
}



-(UILabel *)backLabelWithFrame:(CGRect)frame andString:(NSString *)string
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.text = string;
    label.textColor = fontGrayColor;
    label.font = Font(13);
    
    
    return label;
    
    
}

-(UILabel *)backLabelWithFrame:(CGRect)frame
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.textColor = littleBlackColor;
    label.font = Font(12);
    label.textAlignment = NSTextAlignmentCenter;
    
    return label;
    
    
}
#pragma mark UICollectionView Delegate DataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
#warning 判断给一个还是两个分区
    
    return [[self.imageDictionary allKeys] count];
//        return 2;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 2;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCell" forIndexPath:indexPath];
    cell.backgroundColor = WhiteColor;
    cell.imageView.image = [UIImage imageNamed:@"order_picture"];
    

    if ([[self.imageDictionary allKeys] count] > 0) {
        
        NSString *string;
        NSURL *url ;
        
        if (indexPath.section == 0) {
           string = [NSString stringWithFormat:@"%@",self.imageDictionary[@"提车照片"][indexPath.row][@"picKey"]];
            url = [NSURL URLWithString:string];
    
            cell.stringLabel.text = [NSString stringWithFormat:@"%@",self.imageDictionary[@"提车照片"][indexPath.row][@"typeText"]];

        } else {
            
            string = [NSString stringWithFormat:@"%@",self.imageDictionary[@"交车照片"][indexPath.row][@"picKey"]];
            url = [NSURL URLWithString:string];
            
             cell.stringLabel.text = [NSString stringWithFormat:@"%@",self.imageDictionary[@"交车照片"][indexPath.row][@"typeText"]];

        }

       
        [cell.imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"image_holder"]];
        
        
//        [cell.imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"image_holder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        
//            if (error != nil) {
//                
//                cell.imageView.image = [UIImage imageNamed:@"bad_picture"];
//            }
//            
//            
//        }];

//
    }
    
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    CGSize size = {screenWidth,cellHeight};
    
    return size;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
    
}


-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    CollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView" forIndexPath:indexPath];
    
    
    if (indexPath.section == 0) {
        headerView.titleLabel.text = @"发车照片";
    } else {
        headerView.titleLabel.text = @"卸车照片";
    }
    
    
    return  headerView;
    
    
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(12.5, 18, 4.5, 18);
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *string;
    NSURL *url ;
    
    NSMutableArray *arr = [NSMutableArray array];
    
    if (indexPath.section == 0) {
        
        for (int i = 0; i < [self.imageDictionary[@"提车照片"] count]; i++) {
         
            string = [NSString stringWithFormat:@"%@",self.imageDictionary[@"提车照片"][i][@"picKey"]];
            
            
            url = [NSURL URLWithString:string];
            [arr addObject:url];
            
        }
        
    
    } else {
        
        for (int i = 0; i < [self.imageDictionary[@"提车照片"] count]; i++) {
            
            string = [NSString stringWithFormat:@"%@",self.imageDictionary[@"交车照片"][i][@"picKey"]];
            url = [NSURL URLWithString:string];
            [arr addObject:url];
            
        }

    }

    ScrollImageViewController *scrollV = [[ScrollImageViewController alloc]init];
    scrollV.imageArr = arr;
    
    
    [self.navigationController pushViewController:scrollV animated:YES];
    
}


-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
}



-(void)logisticalButtons{
    
    LogisticalViewController *logisticalVC = [[LogisticalViewController alloc]init];
    logisticalVC.orderId = self.orderId;
    [self.navigationController pushViewController:logisticalVC animated:YES];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
