//
//  OrderRootViewController.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/26.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderRootViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong) UIScrollView *scrollView;

@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) NSMutableDictionary *imageDictionary;
@property (nonatomic,copy) NSString *orderId;


@property (nonatomic,strong) NSString * orderTitleString;
/**
 订单
 */
@property (nonatomic,strong) UILabel *numberLabel;//订单编号
@property (nonatomic,strong) UILabel *statusLabel;//状态
@property (nonatomic,strong) UILabel *startLabel;//起始地
@property (nonatomic,strong) UILabel *startDetailLabel;//详细地址
@property (nonatomic,strong) UILabel *startDate;//起始日期
@property (nonatomic,strong) UILabel *endLabel;//终止地址
@property (nonatomic,strong) UILabel *endDetailLabel;//详细地址
@property (nonatomic,strong) UILabel *endDateLabel;//终止日期
@property (nonatomic,strong) UILabel *carLabel;
@property (nonatomic,strong) UILabel *cityLabel;//定位城市

/**
 承运人
 */
@property (nonatomic,strong) UILabel *distanceLabel;//行程
@property (nonatomic,strong) UILabel *nameLabel;//姓名
@property (nonatomic,strong) UILabel *carNumberLabel;//车牌
@property (nonatomic,strong) UILabel *phoneLabel;//电话
@property (nonatomic,strong) UILabel *carStyleLabel;//车型

/**
 运费
 */

@property (nonatomic,strong) UIView *priceView;


@property (nonatomic,strong) UILabel *totalLabel;//提车费
@property (nonatomic,strong) UILabel *scoreLabel;//运输费
@property (nonatomic,strong) UILabel *payLabel;//交车费
@property (nonatomic,strong) UILabel *unpayLabel;//保险费
@property (nonatomic,strong) UILabel *pingLabel;//平台服务费

@property (nonatomic,strong) UIView *lastPayView;
@property (nonatomic,strong) UILabel *lastPayL;//支付余额

@property (nonatomic,strong) UIScrollView *scrollV;



@end
