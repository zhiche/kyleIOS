//
//  LogisticalViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/7/14.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "LogisticalViewController.h"
#import "TopBackView.h"
#import <Masonry.h>
#import "LogisticalCell.h"
#import "Common.h"
#import "WKProgressHUD.h"

@interface LogisticalViewController ()<UITableViewDelegate,UITableViewDataSource>
@property  (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) UIImageView *imageV ;


@property (nonatomic,strong) NSMutableDictionary *dataDictionay;
@end

@implementation LogisticalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [NSMutableArray array];
    self.dataDictionay = [NSMutableDictionary dictionary];
    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"物流详情"];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = GrayColor;
    [self initDataSource];
    [self initSubViews];
}


-(void)initDataSource
{
    
    NSString *string = [NSString stringWithFormat:@"%@?orderid=%@",logistics_info,self.orderId];
    [Common requestWithUrlString:string contentType:application_json finished:^(id responseObj) {
        
        
        self.dataDictionay = [NSMutableDictionary dictionaryWithDictionary:responseObj[@"data"]];
        
        if ([self.dataDictionay[@"message"] isKindOfClass:[NSNull class]]) {
            
        } else {
           
          self.dataArray = self.dataDictionay[@"message"];

        }
        
        if ([[self.dataDictionay allKeys] count] >0) {
            [self initSubViews];

        }
        
        
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
    
}



-(void)initSubViews
{
    self.imageV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 14.5, 14.5)];
    self.imageV.image = [UIImage imageNamed:@"circle"];
    
    
    UIView *firstView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, cellHeight * 2)];
    
    firstView.backgroundColor = WhiteColor;
    [self.view addSubview:firstView];
    
    //  订单号
    UILabel *orderL = [self backLabelWithFrame:CGRectMake(18, 0, 60, cellHeight) andString:@"运单号:" andInteger:11 andColor:Color_RGB(34, 34, 34, 1)];
    [firstView addSubview:orderL];
    
    NSString *orderS;
    if ([self.dataDictionay[@"orderCode"] isEqual:[NSNull null]]) {
        orderS = @"";
    } else {
        orderS = self.dataDictionay[@"orderCode"];
    }
    
    UILabel *orderNumberL = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(orderL.frame), 0, 250, cellHeight) andString:[NSString stringWithFormat:@"%@",orderS] andInteger:11 andColor:Color_RGB(34, 34, 34, 1)];
    [firstView addSubview:orderNumberL];
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, cellHeight, screenWidth - 18, 0.5)];
    label.backgroundColor = LineGrayColor;
    [firstView addSubview:label];
    
    //承运人
    UILabel *chengyunL = [self backLabelWithFrame:CGRectMake(18, cellHeight + 1, 60, cellHeight) andString:@"承运人" andInteger:13 andColor:carScrollColor];
    [firstView addSubview:chengyunL];
    
    NSString *nameS;
    if ([self.dataDictionay[@"carrier"] isEqual:[NSNull null]]) {
        nameS = @"";
    } else {
        nameS = self.dataDictionay[@"carrier"];
    }

    
    UILabel *nameL = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(chengyunL.frame), cellHeight + 1, 250, cellHeight) andString:[NSString stringWithFormat:@"%@",nameS] andInteger:13 andColor:Color_RGB(34, 34, 34, 1)];
    [firstView addSubview:nameL];
    
    
    UIView *secondView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(firstView.frame) + 10, screenWidth, cellHeight)];
    [self.view addSubview:secondView];
    
    UILabel *wuliuL = [self backLabelWithFrame:CGRectMake(18, 0, 100, cellHeight) andString:@"物流跟踪" andInteger:14 andColor:BlackTitleColor];
    [secondView addSubview:wuliuL];
    secondView.backgroundColor = WhiteColor;
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(18, cellHeight - 0.5, screenWidth - 18, 0.5)];
    label1.backgroundColor = LineGrayColor;
    [secondView addSubview:label1];

    [self initWuliuView];
}



-(void)initWuliuView
{
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(30, cellHeight * 3 + 74, screenWidth - 30, 60 * kHeight * self.dataArray.count - 1) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.scrollEnabled = NO;
    [self.view addSubview:self.tableView];
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMinY(self.tableView.frame), 30, 60 * kHeight * self.dataArray.count)];
    [self.view addSubview:view];
    view.backgroundColor = WhiteColor;
    
    for (int i = 0; i < self.dataArray.count; i++) {
        
        UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(19, 14 * kHeight + i * 60*kHeight, 8, 8)];
        imageV.layer.cornerRadius = 4;
        imageV.layer.masksToBounds = YES;
        
        imageV.backgroundColor = LineGrayColor;
        [view addSubview:imageV];
        
        
        if (i == 0) {

            self.imageV.center = imageV.center;
            [view addSubview:self.imageV];


        }
        
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMidX(imageV.frame), CGRectGetMaxY(imageV.frame), 0.5, 60 * kHeight)];
        label.backgroundColor = LineGrayColor;
        
        if (i != self.dataArray.count - 1) {
            [view addSubview:label];

        }
    
    }
    
    
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
//    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *string = @"wuliu";
    LogisticalCell *cell = [tableView dequeueReusableCellWithIdentifier:string];
    if (!cell) {
        cell = [[LogisticalCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:string];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if (self.dataArray.count > 0) {
                
        cell.titleL.text = [NSString stringWithFormat:@"%@",self.dataArray[indexPath.row][@"message"]];
        NSArray *array = [self.dataArray[indexPath.row][@"date"] componentsSeparatedByString:@" "];
        
        cell.dateLabel.text = [NSString stringWithFormat:@"%@",array[0]];
        cell.timeL.text = [NSString stringWithFormat:@"%@",array[1]];
    }
//
    
//    cell.titleL.text = [NSString stringWithFormat:@"%@",self.dataArray[indexPath.row][@"message"]];
//    NSString *date = [NSString stringWithFormat:@"%@",self.dataArray[indexPath.row][@"date"]];
//    NSArray *array = [ date componentsSeparatedByString:@" "];
//    
//    cell.dateLabel.text =[NSString stringWithFormat:@"%@",array[0]];
//    cell.timeL.text = [NSString stringWithFormat:@"%@",array[1]];
    
    if (indexPath.row == 0) {
        cell.titleL.textColor = YellowColor;
        cell.timeL.textColor = YellowColor;
        cell.dateLabel.textColor = YellowColor;
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60 * kHeight;
}


-(UILabel *)backLabelWithFrame:(CGRect)frame andString:(NSString *)string andInteger:(NSInteger)integer andColor:(UIColor *)color
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.text = string;
    label.textColor = color;
    label.font = Font(integer);
    
    return label;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
