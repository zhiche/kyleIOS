//
//  MineOrderVC.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/9.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//


//待付款
#define order_pendpaying @"pendpaying"
////待发车
//
//#define order_waitdeparture @"waitdeparture"

//待接单
#define order_waiting @"waiting"

//执行中
//intransit
#define order_intransit @"intransit"
//已交车

#define order_achieve @"completed"
//已完成
#define order_done @"done"

//全部
#define order_all @"all"


#define kNumber 5
#define defaultShow 10

#import "MineOrderVC.h"
#import <Masonry.h>
#import "RootViewController.h"
#import "TopBackView.h"
#import "Common.h"
#import "MineOrderModel.h"
#import "LoginViewController.h"
#import "NewTrackVC.h"

#import "OtherCell.h"//其他
#import "FinishOrderCell.h"//完成
#import "ExecuteCell.h"//执行中
#import "ForntCell.h"//付款
#import "AllCell.h"//全部
#import "OrderCommentVC.h"//定价评价

#import "CancelViewController.h"
#import "PayViewController.h"
#import "PlaceOrderVC.h"

#import "ExecuteViewController.h"//待发车
#import "Earnest_PayViewController.h"//待付款
#import "FinishViewController.h"//运输中
#import "DeleverViewController.h"//已送达

#import "RLNetworkHelper.h"//网络判断
#import "WKProgressHUD.h"//提示框
#import <MJRefresh.h>//下拉刷新
#import "NullView.h"

#import "ReadyPayDetailVC.h"//立即支付
#import "OtherDetailVC.h"
#import "GiveOrTakeCarInfoVC.h"
#import "TrackViewController.h"
#import "EvaluateViewController.h"
#import "TrackVC.h"
#import "ComplaintViewController.h"

@interface MineOrderVC ()<UITableViewDelegate,UITableViewDataSource>

//@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *buttonArray;
@property (nonatomic,strong) NSMutableArray *imageArray;
@property (nonatomic,strong) NSMutableArray *dataArray;



@property (nonatomic) NSInteger number;
@property (nonatomic) int pageNo;
@property (nonatomic) NSInteger pageTotal;
@property (nonatomic) int pageSize;

@property (nonatomic,strong) UIView *netView;
@property (nonatomic,strong) WKProgressHUD *hud;
@property (nonatomic,strong) NullView *nullView;

@end

@implementation MineOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GrayColor;
    _number = 0;
    self.pageNo = 1;
    self.pageSize = defaultShow;
    
     self.nullView = [[NullView  alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight  - 45 * kHeight) andTitle:@""];
    self.nullView.backgroundColor = GrayColor;
    
    Common *c = [[Common alloc]init];
    //创建无网络状态view
    self.netView = [c noInternet:@"网络连接失败，请检查网络设备" and:self action:@selector(NoNetPressBtn) andCGreck:CGRectMake(0, 0, Main_Width, Main_height-64)];

    self.buttonArray = [NSMutableArray array];
    self.dataArray = [NSMutableArray array];
    
    self.imageArray = [NSMutableArray arrayWithObjects:@"1_press",@"1_unpress",@"3_press",@"3_unpress",@"4_press",@"4_unpress",@"2_press",@"2_unpress",@"5_press",@"5_unpress", nil];
    
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;

    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"我的订单"];
    
    if ([self.hide isEqualToString:@"1"]) {
        topBackView.leftButton.hidden = NO;
        
        if (_isEva) {
            _number = 2;
        }

    } else {
        topBackView.leftButton.hidden = YES;

    }
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];

    self.view.backgroundColor = WhiteColor;
    
    [self initSubViews];
//    [self present];
//    [self initDataSource];
    

}

-(void)initDataSource

{
    if (self.dataArray.count > 0) {
        [self.dataArray removeAllObjects];
    }
    
    /*
//    假数据
        NSDictionary *dic = [NSDictionary dictionary];
        dic =  [Common txtChangToJsonWithString:@"dingdanliebiao"];
    
    
        if ([[dic[@"data"] allKeys] count] > 0) {
    
            
            NSArray *array = dic[@"data"][@"orders"];
            
            for (id obj in array) {
                MineOrderModel *model = [[MineOrderModel alloc]init];
                
                [model setValuesForKeysWithDictionary:obj];
                
                [self.dataArray addObject:model];
            }

//            [self getValueString];
            [self.tableView reloadData];
        }
    
    */
    
    
  
    
    if (_number == 0) {
        self.nullView.label.text = @"暂无订单";
    } else if (_number == 1){
          self.nullView.label.text = @"暂无执行中订单";
        
    } else if (_number == 2){
          self.nullView.label.text = @"暂无已交车订单";
        
    } else if(_number == 3){
          self.nullView.label.text = @"暂无已完成订单";
        
    } else {
        self.nullView.label.text = @"暂无全部订单";

    }
    
   
    NSString *string;
    
    if (_number == 0) {
        string = order_waiting;
    }
    
    if (_number == 1) {
        string = order_intransit;
    }
    
    if (_number == 2) {
        string = order_achieve;
    }
    
    if (_number == 3) {
        string = order_done;
    }
    
    if (_number == 4) {
        string = order_all;
    }
    
    [WKProgressHUD showInView:self.view withText:@"" animated:YES];


    //订单状态接口
    NSString *urlString = [NSString stringWithFormat:@"%@order/%@?pageNo=%d&pageSize=%d",Main_interface,string,self.pageNo,self.pageSize];
    
    
    [Common requestWithUrlString:urlString contentType:application_json finished:^(id responseObj) {
                
        [self.hud dismiss:YES];
        [WKProgressHUD dismissAll:YES];
        
        self.pageTotal = [responseObj[@"data"][@"page"][@"totalPage"] integerValue];
        
        if (self.dataArray.count > 0 ) {
            [self.dataArray removeAllObjects];
        }

        
        if ([responseObj[@"success"] boolValue]) {
            
            [WKProgressHUD dismissAll:YES];

            [self.hud dismiss:YES];
            
            NSArray *array = responseObj[@"data"][@"orders"];
            for (id obj in array) {
                MineOrderModel *model = [[MineOrderModel alloc]init];
                
                [model setValuesForKeysWithDictionary:obj];
                
                [self.dataArray addObject:model];
            }
            
            
            if (self.dataArray.count >0) {
                
                [self.nullView removeFromSuperview];

            } else {
                
              [self.tableView addSubview:self.nullView];
                
            }
            
            [self.tableView reloadData];

        } else {
            
            [WKProgressHUD popMessage:responseObj[@"message"] inView:self.view duration:1.5 animated:YES];

        }
        

    } failed:^(NSString *errorMsg) {
        
        
        [WKProgressHUD dismissAll:YES];

        [self.hud dismiss:YES];

        NSLog(@"%@",errorMsg);

    }];


}

-(void)initSubViews
{
    
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, 45 * kHeight)];
 
    
    NSArray *titleArray = @[@"待接单",
                            @"执行中",
                            @"已交车",
                            @"已完成",
                            @"全部"];
    
    for (int i = 0; i < titleArray.count; i++) {
        UIButton *topButton = [self backButtonWithString:titleArray[i] andInteger:i andView:topView];
        [topButton addTarget:self action:@selector(topButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        topButton.tag = i + 300;
        
        [self.buttonArray addObject:topButton];
        
        if (i == _number) {
            for (UILabel *lable in [topButton subviews]) {
                if ([lable isKindOfClass:[UILabel class]]) {
                    lable.textColor = YellowColor;
                    for (UIImageView *imageV in [lable subviews]) {
                        if ([imageV isKindOfClass:[UIImageView class]]) {
                            [UIView animateWithDuration:0.2 animations:^{
                                imageV.hidden = NO;
                                
                            }];
                        }
                    }
                }
            }
            for (UIImageView *img in [topButton subviews]) {
                if ([img isKindOfClass:[UIImageView class]]) {
                    img.image = [UIImage imageNamed:self.imageArray[_number]];
                }
            }
        }
    }
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 45 * kHeight - 0.5, screenWidth, 0.5)];
    label.backgroundColor = LineGrayColor;
    [topView addSubview:label];

    
    [self.view addSubview:topView];
    
    if ([self.hide isEqualToString:@"1"]) {
        
        self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64 + 45 * kHeight, screenWidth, screenHeight - (64 + 45 * kHeight)) style:UITableViewStyleGrouped];
    } else {
        self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64 + 45 * kHeight, screenWidth, screenHeight - 64  - (45 * kHeight) - 48 * kHeight) style:UITableViewStyleGrouped];

    }
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = GrayColor;
    [self.view addSubview:self.tableView];

    //mj
    [self.tableView.mj_header beginRefreshing];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefresh)];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(upRefresh)];
    
    //无网络状态
    [self createNetWorkAndStatue];

}


-(void)createNetWorkAndStatue{
    
    //判断是否有网
    [self judgeNetWork];
    //创建加载状态
}

//判断是否有网
-(void)judgeNetWork{
    if (![RLNetworkHelper isConnectedToNetwork]) {
        [self.tableView addSubview:self.netView];
        self.hud = [WKProgressHUD showInView:self.view withText:@"" animated:YES];

    }
    else{
        [self.netView removeFromSuperview];
        [self initDataSource];
    }
}

//无网络状态
-(void)NoNetPressBtn{
    if ([RLNetworkHelper isConnectedToNetwork]) {
        [self.netView removeFromSuperview];
        [self initDataSource];
    } else {
        [self.hud dismiss:YES];
    }
}

-(void)downRefresh{
    
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    self.pageNo = 1;
    
    [self judgeNetWork];
}
-(void)upRefresh{
    
    if (self.pageNo != (int)self.pageTotal) {
        self.pageNo ++;
        [self judgeNetWork];
        [self.tableView.mj_footer endRefreshing];
    }else{
        self.tableView.mj_footer.state = MJRefreshStateNoMoreData;
    }
    [self.tableView.mj_header endRefreshing];
}




-(void)topButtonAction:(UIButton *)sender
{
    
    for (int i = 0; i < self.buttonArray.count; i++) {
        UIButton *button = (UIButton *)self.buttonArray[i];
        
        if (button.tag == sender.tag) {
                        
            for (UIImageView *img in [sender subviews]) {
                if ([img isKindOfClass:[UIImageView class]]) {
                    img.image = [UIImage imageNamed:self.imageArray[(sender.tag - 300) * 2]];
                }
            }
            
            for (UILabel *lable in [sender subviews]) {
                
                if ([lable isKindOfClass:[UILabel class]]) {
                    
                    lable.textColor = YellowColor;
                    
                    for (UIImageView *imageV in [lable subviews]) {
                        
                        if ([imageV isKindOfClass:[UIImageView class]]) {
                            [UIView animateWithDuration:0.2 animations:^{
                                imageV.hidden = NO;
                            }];
                        }
                    }
                }
            }
        } else {
            
            for (UIImageView *img in [button subviews]) {
                if ([img isKindOfClass:[UIImageView class]]) {
                    img.image = [UIImage imageNamed:self.imageArray[i * 2 + 1]];
                }
            }
            
            for (UILabel *lable in [button subviews]) {
                if ([lable isKindOfClass:[UILabel class]]) {
                    
                    lable.textColor = littleBlackColor;
                    
                    for (UIImageView *imageV in [lable subviews]) {
                        
                        if ([imageV isKindOfClass:[UIImageView class]]) {
                            [UIView animateWithDuration:0.2 animations:^{
                                imageV.hidden = YES;
                                
                            }];
                        }
                    }
                }
            }
        }
    }
    
    [self present];
    
    _number = sender.tag - 300;
    
    [self.tableView reloadData];
    
    [self initDataSource];

    
}

-(UIButton *)backButtonWithString:(NSString *)string andInteger:(int)a andView:(UIView *)view
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake( 15 + (screenWidth - 30)/kNumber * a, 0,(screenWidth - 30)/kNumber, 45 * kHeight);
    
    
    for (UIView *view in [button subviews]) {
        [view removeFromSuperview];
    }
    
    UIImageView *logoImageV = [[UIImageView alloc]init];

    [button addSubview:logoImageV];
    [logoImageV mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.centerX.equalTo(button);
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.top.mas_equalTo(7 * kHeight);
        
    }];
    logoImageV.image = [UIImage imageNamed:self.imageArray[2 * a + 1]];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    [button addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.centerX.equalTo(button);
        make.width.equalTo(button);
        make.height.mas_equalTo(41 * kHeight - 20);
        make.top.mas_equalTo(logoImageV.mas_bottom).offset(-2 * kHeight);
        
    }];
    
    titleLabel.font = Font(9);
    titleLabel.textColor = BlackTitleColor;
    titleLabel.text = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleLabel sizeToFit];

//    CGRect rect = titleLabel.frame;
    
    //下面黑线
    UIImageView *imageV = [[UIImageView alloc]init];
    [titleLabel addSubview:imageV];
    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(titleLabel.mas_bottom).offset(-2 * kHeight);
        make.centerX.equalTo(titleLabel);
        make.height.mas_equalTo(1.5 * kHeight);
        make.width.mas_equalTo((screenWidth - 30)/4.0);
        
    }];
    
    imageV.backgroundColor = YellowColor;
    imageV.hidden = YES;
    
    [view addSubview:button];
    
    
    if (a == 3) {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 45 * kHeight - 0.5, screenWidth, 1)];
        label.textColor = Color_RGB(200, 199, 204, 1);

        [view addSubview:label];

    }
    
    
    return button;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    return 3;
    
    return self.dataArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //待接单
    if (_number == 0) {
        
        NSString *string = @"fontCell";
        ForntCell *cell = [tableView dequeueReusableCellWithIdentifier:string];
        if (!cell) {
            cell = [[ForntCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:string];
        }
        
        if (self.dataArray.count > 0) {
            cell.model = self.dataArray[indexPath.section];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
//        [cell.payButton addTarget: self action:@selector(payButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//        cell.payButton.tag = indexPath.section + 200;
        
        [cell.cancelButton addTarget:self action:@selector(cancelButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.cancelButton.tag = indexPath.section + 1000;
        
        return cell;
        
    }
    
    else
        if (_number == 1) {//执行中
        
        NSString *str = @"executeCell";
        //
        ExecuteCell * cell= [[ExecuteCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:str];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell.trackButton addTarget:self action:@selector(trackButton2Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.trackButton.tag = indexPath.section + 300;
//        [cell.getButton addTarget:self action:@selector(getButton2Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.getButton.tag = indexPath.section + 500;
//        [cell.confirmButton addTarget:self action:@selector(confirmButton2Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.confirmButton.tag = indexPath.section + 700;
        if (self.dataArray.count > 0) {
            cell.model = self.dataArray[indexPath.section];
            
        }
        
        
        return cell;
        
    }
    
    else if (_number == 2) {//已交车
        
        NSString *str = @"finishCell";
        FinishOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
        if (!cell) {
            cell = [[FinishOrderCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:str];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        [cell.trackButton addTarget:self action:@selector(trackButton2Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.trackButton.tag = indexPath.section + 300;
//        [cell.getButton addTarget:self action:@selector(getButton2Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.getButton.tag = indexPath.section + 500;
//        [cell.detailButton addTarget:self action:@selector(confirmButton2Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.detailButton.tag = indexPath.section + 700;
        
        if (self.dataArray.count > 0) {
            cell.model = self.dataArray[indexPath.section];
            
        }
        
        return cell;
        
    }
    
    else  if(_number == 3) {//已完成
        
        NSString *str = @"otherCell";
        OtherCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
        if (!cell) {
            cell = [[OtherCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:str];
            
        }
       
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
//        [cell.checkButton addTarget:self action:@selector(checkButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.tag = indexPath.section + 1000;
        
//        [cell.complainButton addTarget:self action:@selector(complainButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//        cell.complainButton.tag = indexPath.section + 1500;
        
        [cell.trackButton addTarget:self action:@selector(trackButton2Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.trackButton.tag = indexPath.section + 300;
//        [cell.getButton addTarget:self action:@selector(getButton2Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.getButton.tag = indexPath.section + 500;
//        [cell.detailButton addTarget:self action:@selector(confirmButton2Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.detailButton.tag = indexPath.section + 700;
        cell.checkButton.tag = indexPath.section + 800;

        
        
        [cell.checkButton addTarget:self action:@selector(pressCheckBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if (self.dataArray.count > 0) {
            
            cell.model = _dataArray[indexPath.section];
            NSLog(@"%@",cell.model.orderCode);
            

        }
        return cell;
        
    } else {
        //全部
        NSString *str = @"allcell";
//        AllCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
//        if (!cell) {
          AllCell  *cell = [[AllCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:str];
            
//        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (self.dataArray.count > 0) {
            cell.model = self.dataArray[indexPath.section];
        }

        
        [cell.trackButton addTarget:self action:@selector(trackButton2Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.trackButton.tag = indexPath.section + 300;
//        [cell.getButton addTarget:self action:@selector(getButton2Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.getButton.tag = indexPath.section + 500;
//        [cell.giveButton addTarget:self action:@selector(confirmButton2Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.giveButton.tag = indexPath.section + 700;
        
        
        [cell.cancelButton addTarget:self action:@selector(cancelButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.cancelButton.tag = indexPath.section + 1000;
        
        [cell.deleteButton addTarget: self action:@selector(deleteButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.deleteButton.tag = indexPath.section + 1200;
        
        [cell.publishButton addTarget:self action:@selector(publishButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.publishButton.tag = indexPath.section + 1400;
        
        [cell.republishButton addTarget:self action:@selector(republishButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.republishButton.tag = indexPath.section + 1600;
        
        return cell;

    }
    
    
}

//评价
-(void)pressCheckBtn:(UIButton*)sender{
    
    MineOrderModel *model = [[MineOrderModel alloc]init];
    
    model = self.dataArray[sender.tag - 800];
    

    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    [dic setObject:model.orderCode forKey:@"orderCode"];
    [dic setObject:model.deliveryDate forKey:@"deliveryDate"];
    [dic setObject:model.arriveDate forKey:@"arriveDate"];
    [dic setObject:model.orgin forKey:@"orgin"];
    [dic setObject:model.dest forKey:@"dest"];


    
    
    OrderCommentVC * order = [[OrderCommentVC alloc]init];
    
    order.orderId = model.ID;

    order.dataDictionary = dic;
    
    order.isValuation = model.isValuation;
    
    [self.navigationController pushViewController:order animated:YES];
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    
        //全部里面有代付款
        MineOrderModel *model = self.dataArray[indexPath.section];    
            OtherDetailVC *otherVC = [[OtherDetailVC alloc]init];
            otherVC.integer = _number;
            otherVC.orderId = model.ID;
            
            [self.navigationController pushViewController:otherVC animated:YES];

    
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  188 * kHeight;
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}


//#pragma mark 左滑事件
//-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//}
//
//-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//    
//    UITableViewRowAction *action2 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"disenable" handler:nil];
//    
//    UITableViewRowAction *action3 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"emjoy👍" handler:nil];
//    
//
//    return @[action2,action3];
//}


#pragma mark 取消订单
-(void)cancelButtonAction:(UIButton *)sender
{
    
    MineOrderModel *model = [[MineOrderModel alloc]init];
    model = self.dataArray[sender.tag - 1000];
    
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:@"是否确定取消订单" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        NSMutableDictionary * dicUrl = [[NSMutableDictionary alloc]init];
        [dicUrl setObject:model.ID forKey:@"orderid"];
        [dicUrl setObject:@"" forKey:@"reason"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            [Common afPostRequestWithUrlString:Mine_Order_Cancel parms:dicUrl finishedBlock:^(id responseObj) {
                
                NSMutableDictionary * dicObj =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];
                NSString * string = [NSString stringWithFormat:@"%@",dicObj[@"success"]];
                if ([string isEqualToString:@"1"]) {
                    
                    
                    //延迟执行
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        self.pageNo = 1;
                        
                        [self initDataSource];
                        
                    });
                    
                }
                
                [WKProgressHUD popMessage:dicObj[@"message"] inView:self.view duration:1.5 animated:YES];

                
            } failedBlock:^(NSString *errorMsg) {
            }];
            
        });
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
    
}


#pragma mark 支付
-(void)payAction:(UIButton *)sender
{
    Earnest_PayViewController *orderVC = [[Earnest_PayViewController alloc]init];
    
    MineOrderModel *model = [[MineOrderModel alloc]init];
    model = self.dataArray[sender.tag - 2000];
    
    orderVC.orderId = model.ID ;
    [self.navigationController pushViewController:orderVC animated:YES];
   
}

//#pragma mark 待提车
//
//-(void)goToTiche:(UIButton *)sender
//{
//    ExecuteViewController *executeVC = [[ExecuteViewController alloc]init];
//    
//    MineOrderModel *model = [[MineOrderModel alloc]init];
//    model = self.dataArray[sender.tag - 1000];
//    
//    executeVC.orderId = model.ID ;
//    [self.navigationController pushViewController:executeVC animated:YES];
//}

/*
#pragma mark 我要评价

-(void)pingjia:(UIButton *)sender
{
    ExecuteViewController *executeVC = [[ExecuteViewController alloc]init];
    
    MineOrderModel *model = [[MineOrderModel alloc]init];
    model = self.dataArray[sender.tag - 1000];
    
    executeVC.orderId = model.ID ;
    [self.navigationController pushViewController:executeVC animated:YES];
}


#pragma mark 确认收车

-(void)checkButton:(UIButton *)sender
{
    MineOrderModel *model = [[MineOrderModel alloc]init];
        model = self.dataArray[sender.tag - 4000];
    
    DeleverViewController *deleverVC = [[DeleverViewController alloc]init];
    
            deleverVC.orderId = model.ID;
    
    
    [self.navigationController pushViewController:deleverVC animated:YES];
}


-(void)detailButton:(UIButton *)sender
{
    MineOrderModel *model = [[MineOrderModel alloc]init];
    model = self.dataArray[sender.tag - 4000];
    
       FinishViewController  *finishVC = [[FinishViewController alloc]init];
        finishVC.orderId = model.ID;
        [self.navigationController pushViewController:finishVC animated:YES];
  }

*/
#pragma mark 准备支付
-(void)payButtonAction:(UIButton *)sender
{
    
    MineOrderModel *model = [[MineOrderModel alloc]init];
    model = self.dataArray[sender.tag - 200];
    ReadyPayDetailVC *reayVC = [[ReadyPayDetailVC alloc]init];
    reayVC.orderId = model.ID;
    
    [self.navigationController pushViewController:reayVC animated:YES];
}

#pragma mark 交车信息
-(void)getButton2Action:(UIButton *)sender
{
    GiveOrTakeCarInfoVC *giveVC = [[GiveOrTakeCarInfoVC alloc]init];

    
    MineOrderModel *model = [[MineOrderModel alloc]init];
    
    model = self.dataArray[sender.tag - 500];
    giveVC.orderId = model.ID;
    giveVC.titleString = @"交车信息";
    giveVC.number = self.number;
    
    [self.navigationController pushViewController:giveVC animated:YES];
}


#pragma mark 提车信息
-(void)confirmButton2Action:(UIButton *)sender
{
    GiveOrTakeCarInfoVC *takeVC = [[GiveOrTakeCarInfoVC alloc]init];
    MineOrderModel *model = [[MineOrderModel alloc]init];
    

    model = self.dataArray[sender.tag - 700];

    takeVC.titleString = @"提车信息";
    takeVC.orderId = model.ID;

    [self.navigationController pushViewController:takeVC animated:YES];
}

#pragma mark 物流跟踪
-(void)trackButton2Action:(UIButton *)sender
{
//    TrackVC *trackVC = [[TrackVC alloc]init];
    NewTrackVC * trackVC = [[NewTrackVC alloc]init];
    MineOrderModel *model = [[MineOrderModel alloc]init];
    
    model = self.dataArray[sender.tag - 300];
    trackVC.orderId = model.ID;

    [self.navigationController pushViewController:trackVC animated:YES];
}

#pragma mark 删除
-(void)deleteButtonAction:(UIButton *)sender
{

    MineOrderModel *model = [[MineOrderModel alloc]init];
    model = self.dataArray[sender.tag - 1200];
    NSDictionary *dic = @{@"id":model.ID};

    NSString *urlString = [NSString stringWithFormat:@"%@order/del",Main_interface];
    
    Common *c = [[Common alloc]init];
    [c afPostRequestWithUrlString:urlString parms:dic finishedBlock:^(id responseObj) {
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableLeaves error:nil];

    
    if ([dictionary[@"success"] boolValue]) {
        
        // dicionary[@"message"] 提示删除是否成功
        
#warning 可以添加提示框
        [self.dataArray removeObjectAtIndex:sender.tag - 1200];
        
        [self.tableView beginUpdates];
        
        //删除一个section
        [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sender.tag - 1200] withRowAnimation:UITableViewRowAnimationLeft];
        [self.tableView endUpdates];
        
        
        //延迟执行
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self initDataSource];
            
        });
        
    }
    
    [WKProgressHUD popMessage:dictionary[@"message"] inView:self.view duration:1.5 animated:YES];
    

} failedBlock:^(NSString *errorMsg) {
    NSLog(@"%@",errorMsg);
}];
    
    
   
}


#pragma mark 发布
-(void)publishButtonAction:(UIButton *)sender
{
    MineOrderModel *model = [[MineOrderModel alloc]init];
    model = self.dataArray[sender.tag - 1400];
    
    NSString *urlString = [NSString stringWithFormat:@"%@order/publish",Main_interface];
    
        NSDictionary *dic = @{@"id":model.ID};
    
    Common *c = [[Common alloc]init];
    
    [c afPostRequestWithUrlString:urlString parms:dic finishedBlock:^(id responseObj) {
        
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableLeaves error:nil];
        
        if ([dictionary[@"success"] boolValue]) {
            
            //延迟执行
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [self initDataSource];
                
            });
            
        }
        
        [WKProgressHUD popMessage:dictionary[@"message"] inView:self.view duration:1.5 animated:YES];

        
        
    } failedBlock:^(NSString *errorMsg) {
        
    }];
    
    
}



#pragma mark 重新发布
-(void)republishButtonAction:(UIButton *)sender
{
    MineOrderModel *model = [[MineOrderModel alloc]init];
    model = self.dataArray[sender.tag - 1600];
    
    NSString *urlString = [NSString stringWithFormat:@"%@order/republish",Main_interface];
    
    NSDictionary *dic = @{@"id":model.ID};
    
    Common *c = [[Common alloc]init];
    
    [c afPostRequestWithUrlString:urlString parms:dic finishedBlock:^(id responseObj) {
        
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableLeaves error:nil];
        
        if ([dictionary[@"success"] boolValue]) {
            
            //延迟执行
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [self initDataSource];
                
            });
            
        }
        
        [WKProgressHUD popMessage:dictionary[@"message"] inView:self.view duration:1.5 animated:YES];
        
        
        
    } failedBlock:^(NSString *errorMsg) {
        
    }];
    

}

-(void)checkButtonAction:(UIButton *)sender
{
    EvaluateViewController *evaluateVC = [[EvaluateViewController alloc]init];
    [self.navigationController pushViewController:evaluateVC animated:YES];
    
}


/*
#pragma  mark 评价

#pragma mark 投诉
-(void)complainButtonAction:(UIButton *)sender
{
    ComplaintViewController *compliaintVC = [[ComplaintViewController alloc]init];
    [self.navigationController pushViewController:compliaintVC animated:YES];
}

*/


//#pragma mark 其他－删除
//



-(void)viewWillAppear:(BOOL)animated
{
    
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    
//    NSString *string = [userDefaults objectForKey:@"login"];
//    
//    if (string == nil ) {
//        
//        LoginViewController *loginVC = [[LoginViewController alloc]init];
//        [self presentViewController:loginVC animated:YES completion:nil];
//        
//    } else {

    
    [self.navigationController setNavigationBarHidden:YES];
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    
    if ([self.hide isEqualToString:@"1"]) {
        
        [rootVC setTabBarHidden:YES];

    } else {
        
        [rootVC setTabBarHidden:NO];
 
    }
    
    [self initDataSource];
    
    
}

-(void)present
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *string = [userDefaults objectForKey:@"login"];
    
    if (string == nil ) {
        
        LoginViewController *loginVC = [[LoginViewController alloc]init];
        loginVC.topBackView.leftButton.hidden = YES;
        UINavigationController * loginNC = [[UINavigationController alloc]initWithRootViewController:loginVC];
        loginVC.backToFirst = @"1";
        
        [self presentViewController:loginNC animated:YES completion:nil];
        
    }
  
}



-(void)viewDidDisappear:(BOOL)animated
{
    
}



-(void)viewDidAppear:(BOOL)animated
{
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    if ([rootVC.rootString isEqualToString:@"1"]) {
        
        [self present];
        
        rootVC.rootString = @"2";

    }
    
    
    if ([self.hide isEqualToString:@"1"]) {
        
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;

    } else {
        
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;

        
    }
    
    

    
}


-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
