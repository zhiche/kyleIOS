//
//  OrderRootViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/26.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "OrderRootViewController.h"
#import "TopBackView.h"
#import "RootViewController.h"
#import "CollectionReusableView.h"
#import "CollectionViewCell.h"
#import <UIImageView+WebCache.h>

#define labelHeight 15 * kHeight
#define labelWidth 60 * kWidth
#define leftMargn 18

@interface OrderRootViewController ()

@end

@implementation OrderRootViewController


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.orderTitleString = [[NSString alloc]init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    NSLog(@"%@",self.orderTitleString);
    NSString * title = nil;
    if (self.orderTitleString.length>0) {
        title = self.orderTitleString;
    }else{
        title = @"确定支付";
    }
    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:title];
    
    self.imageDictionary = [NSMutableDictionary dictionary];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = GrayColor;
    
    [self initSubviews];
    
}


-(void)initSubviews
{
    
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight)];
    self.scrollView.contentSize = CGSizeMake(screenWidth, screenHeight * 1.2);
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = GrayColor;


    /**
     
     订单信息
     */
    UIView *orderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 177 * kHeight)];
    [self.scrollView addSubview:orderView];
    orderView.backgroundColor = WhiteColor;
    //订单号
    
    UIView *orderV1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight)];
    [orderView addSubview:orderV1];
    orderV1.backgroundColor = headerBottomColor;

    
    
    UILabel *orderL = [self backLabelWithFrame:CGRectMake(leftMargn, 0, 60, cellHeight) andString:@"订单号:"];
    orderL.textColor = littleBlackColor;
    [orderV1 addSubview:orderL];
    
    self.numberLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(orderL.frame) + 5, CGRectGetMinY(orderL.frame), 200, cellHeight) andString:@""];
    self.numberLabel.textColor = littleBlackColor;
    [orderV1 addSubview:self.numberLabel];
    
    //状态栏
    self.statusLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 14 - 100, CGRectGetMinY(orderL.frame), 100, cellHeight) ];
    self.statusLabel.textColor = RedColor;
    self.statusLabel.text = @"已交车";
    self.statusLabel.textAlignment = NSTextAlignmentRight;
//    [orderView addSubview:self.statusLabel];
    
    
    UILabel *labe = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight, screenWidth, 0.5)];
    labe.backgroundColor = LineGrayColor;
    [orderV1 addSubview:labe];
    
    //起始日期
    self.startDate = [self backLabelWithFrame:CGRectMake(20 * kWidth, CGRectGetMaxY(orderV1.frame) + 12*kHeight, labelWidth, labelHeight)];
    self.startDate.text = @"";
    self.startDate.textColor = littleBlackColor;
    self.startDate.font = Font(10);
    [orderView addSubview:self.startDate];

    
    //起始地址
    self.startLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMinX(self.startDate.frame) - 20,  CGRectGetMaxY(self.startDate.frame) + 9*kHeight, labelWidth + 40, labelHeight) ];
    self.startLabel.text = @"";
    self.startLabel.font = Font(14)
    [orderView addSubview:self.startLabel];
    
    //起始详情地址
    self.startDetailLabel = [self backLabelWithFrame:CGRectMake(20 * kHeight, CGRectGetMaxY(self.startLabel.frame) , labelWidth, 28 * kHeight)];
    self.startDetailLabel.text = @"";
    self.startDetailLabel.adjustsFontSizeToFitWidth = YES;
    [orderView addSubview:self.startDetailLabel];
    self.startDetailLabel.font = Font(9);
    self.startDetailLabel.numberOfLines = 0;
    
    
    //图片
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMidX(self.view.frame) - 30, CGRectGetMidY(self.startLabel.frame) - 13, 47, 12.5)];
    [orderView addSubview:imageView];
    imageView.image = [UIImage imageNamed:@"Quote_Cell_logo"];
    
    
    
    //目的日期
    self.endDateLabel = [self backLabelWithFrame:CGRectMake(screenWidth - labelWidth  - 20 * kWidth, CGRectGetMaxY(labe.frame) + 12*kHeight, labelWidth, labelHeight)];
    self.endDateLabel.text = @"";
    [orderView addSubview:self.endDateLabel];
    self.endDateLabel.font = Font(10);
    
    //目的地
    self.endLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMinX(self.endDateLabel.frame) - 20, CGRectGetMaxY(self.endDateLabel.frame) + 9*kHeight, labelWidth + 40, labelHeight)];
    [orderView addSubview:self.endLabel];
    self.endLabel.text = @"";
    self.endLabel.font = Font(14);
    
    
    //目的详情地址
    self.endDetailLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMinX(self.endDateLabel.frame), CGRectGetMaxY(self.endLabel.frame) + 0, labelWidth, 28 * kHeight)];
    self.endDetailLabel.adjustsFontSizeToFitWidth = YES;
    self.endDetailLabel.text = @"";
    [orderView addSubview:self.endDetailLabel];
    self.endDetailLabel.font = Font(9);
    self.endDetailLabel.numberOfLines = 0;
    
   
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.endDetailLabel.frame) + 12 * kHeight, screenWidth , 0.5)];
    label.backgroundColor = LineGrayColor;
    [orderView addSubview:label];
    
    
    self.scrollV = [[UIScrollView alloc]initWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(label.frame), screenWidth - leftMargn * 2, cellHeight)];
    self.scrollV.contentSize = CGSizeMake(screenWidth + 100, cellHeight);
    self.scrollV.alwaysBounceHorizontal = YES;
    self.scrollV.showsHorizontalScrollIndicator = NO;
    [orderView addSubview:self.scrollV];
    
    self.carLabel = [self backLabelWithFrame:CGRectMake(0, 0, screenWidth + 100, cellHeight) ];
    self.carLabel.text = @"";
    self.carLabel.textColor = carScrollColor;
    self.carLabel.textAlignment = NSTextAlignmentLeft;
    self.carLabel.font = Font(13);
    [self.scrollV addSubview:self.carLabel];
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.carLabel.frame), screenWidth, 0.5)];
    [orderView addSubview:label1];
    label1.backgroundColor = LineGrayColor;
    
    
    /**
    
     承运人信息
     */
   

    UIView *deleverView = [self backViewWithFrame:CGRectMake(0, CGRectGetMaxY(orderView.frame) + 10, screenWidth, cellHeight * 5)];
//    [self.scrollView addSubview:deleverView];
    deleverView.backgroundColor = WhiteColor;
    
    UIView *delever1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight)];
    [deleverView addSubview:delever1];
    delever1.backgroundColor = headerBottomColor;
 
    UILabel *deleverLabel = [self backLabelWithFrame:CGRectMake(leftMargn, 0, 100 * kWidth, cellHeight) andString:@"当前定位城市:"];
    deleverLabel.font = Font(14);
    deleverLabel.textColor = littleBlackColor;
    [delever1 addSubview:deleverLabel];
    
    self.cityLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(deleverLabel.frame), 0, 150, cellHeight) andString:@""];
    self.cityLabel.font = Font(14);
    self.cityLabel.textColor = littleBlackColor;
    [delever1 addSubview:self.cityLabel];
    
    
    UILabel *deleverL1 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(deleverLabel.frame), screenWidth, 0.5)];
    deleverL1.backgroundColor = LineGrayColor;
    [deleverView addSubview:deleverL1];

    //姓名
    
    UILabel *nameL = [self backLabelWithFrame:CGRectMake(leftMargn,  CGRectGetMaxY(deleverL1.frame), 60, cellHeight) andString:@"姓名"];
    [deleverView addSubview:nameL];
    nameL.textColor = fontGrayColor;
    
    self.nameLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 114, CGRectGetMinY(nameL.frame), 100, cellHeight) andString:@""];
    self.nameLabel.textAlignment = NSTextAlignmentRight;
    self.nameLabel.textColor = Color_RGB(34, 34, 34, 1);
    [deleverView addSubview:self.nameLabel];

    
    
    UILabel *deleverL2 = [self backLineLabelWithFloat: CGRectGetMaxY(nameL.frame)];

    [deleverView addSubview:deleverL2];


    //车牌
    UILabel *carNumberL = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(nameL.frame), 60, cellHeight) andString:@"车牌"];
    [deleverView addSubview:carNumberL];
    carNumberL.textColor = fontGrayColor;
    
    self.carNumberLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 114, CGRectGetMinY(carNumberL.frame) + 1, 100, cellHeight) andString:@""];
    self.carNumberLabel.textAlignment = NSTextAlignmentRight;
    self.carNumberLabel.textColor = Color_RGB(34, 34, 34, 1);
    [deleverView addSubview:self.carNumberLabel];
    
    
    UILabel *deleverL3 = [self backLineLabelWithFloat:CGRectGetMaxY(carNumberL.frame)];
    
    [deleverView addSubview:deleverL3];
    
    //电话
    
    UILabel *phoneL = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(carNumberL.frame), 60, cellHeight) andString:@"电话"];
    [deleverView addSubview:phoneL];
    phoneL.textColor = fontGrayColor;
    
    self.phoneLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 114, CGRectGetMinY(phoneL.frame) + 1, 100, cellHeight) andString:@""];
    self.phoneLabel.textAlignment = NSTextAlignmentRight;
    self.phoneLabel.textColor = Color_RGB(34, 34, 34, 1);
    [deleverView addSubview:self.phoneLabel];
    
    
    UILabel *deleverL4 = [self backLineLabelWithFloat:CGRectGetMaxY(phoneL.frame) ];
       [deleverView addSubview:deleverL4];
    

    
    //车型
    
    UILabel *carStyle = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(phoneL.frame), 60, cellHeight) andString:@"车型"];
    [deleverView addSubview:carStyle];
    carStyle.textColor = fontGrayColor;
    
    self.carStyleLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 114, CGRectGetMinY(carStyle.frame), 100, cellHeight) andString:@""];
    self.carStyleLabel.textColor = Color_RGB(34, 34, 34, 1);
    self.carStyleLabel.textAlignment = NSTextAlignmentRight;
    [deleverView addSubview:self.carStyleLabel];

    
    /**
     运费信息
     */
    

    self.priceView = [self backViewWithFrame:CGRectMake(0, CGRectGetMaxY(orderView.frame) + 10, screenWidth, cellHeight * 5 + 5)];

    [self.scrollView addSubview:self.priceView];
    self.priceView.backgroundColor = WhiteColor;
    
    UILabel *priceL1 = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight, screenWidth, 0.5)];
    priceL1.backgroundColor = LineGrayColor;
    
    [self.priceView addSubview:priceL1];
    

    UIView *priceV1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight)];
    [self.priceView addSubview:priceV1];
    priceV1.backgroundColor = headerBottomColor;

    
    UILabel *priceLabel = [self backLabelWithFrame:CGRectMake(leftMargn, 0, 100, cellHeight) andString:@"费用"];
    priceLabel.font = Font(14);
    priceLabel.textColor = Color_RGB(34, 34, 34, 1);
    [priceV1 addSubview:priceLabel];
    //提车费
    
    CGFloat marWidth = 65 * kWidth;
    
    UILabel *priceL = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(priceV1.frame) , marWidth, cellHeight) andString:@"提车费"];
    [self.priceView addSubview:priceL];
    
    self.totalLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 214, CGRectGetMinY(priceL.frame), 200, cellHeight) andString:@""];
    self.totalLabel.textAlignment = NSTextAlignmentRight;
    self.totalLabel.textColor = Color_RGB(34, 34, 34, 1);
    [self.priceView addSubview:self.totalLabel];

    
    UILabel *priceL2 = [self backLineLabelWithFloat:CGRectGetMaxY(priceL.frame)];
   
    [self.priceView addSubview:priceL2];
    
    
    //交车费
    UILabel *payL = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(priceL2.frame), marWidth, cellHeight) andString:@"交车费"];
    [self.priceView addSubview:payL];
    
    self.payLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 214, CGRectGetMinY(payL.frame), 200, cellHeight) andString:@""];
    self.payLabel.textAlignment = NSTextAlignmentRight;
    self.payLabel.textColor = Color_RGB(34, 34, 34, 1);
    [self.priceView addSubview:self.payLabel];

    
    UILabel *priceL3 = [self backLineLabelWithFloat:CGRectGetMaxY(payL.frame)];
       [self.priceView addSubview:priceL3];

    //运输费
    UILabel *scoreL = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(priceL3.frame), marWidth, cellHeight) andString:@"运输费"];
    [self.priceView addSubview:scoreL];
    
    self.scoreLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 214, CGRectGetMinY(scoreL.frame), 200, cellHeight) andString:@""];
    self.scoreLabel.textAlignment = NSTextAlignmentRight;
    self.scoreLabel.textColor = Color_RGB(34, 34, 34, 1);
    [self.priceView addSubview:self.scoreLabel];
    
    UILabel *priceL4 = [self backLineLabelWithFloat:CGRectGetMaxY(scoreL.frame)];
   
    [self.priceView addSubview:priceL4];
    
    //保险费

    UILabel *unpayL = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(priceL4.frame),marWidth, cellHeight) andString:@"保险费"];
    [self.priceView addSubview:unpayL];
    
    self.unpayLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 214, CGRectGetMinY(unpayL.frame), 200, cellHeight) andString:@""];
    self.unpayLabel.textColor = YellowColor;
    self.unpayLabel.textAlignment = NSTextAlignmentRight;
    self.unpayLabel.textColor = Color_RGB(34, 34, 34, 1);
    [self.priceView addSubview:self.unpayLabel];
    
    UILabel *priceL5 = [self backLineLabelWithFloat:CGRectGetMaxY(unpayL.frame)];
   
    [self.priceView addSubview:priceL5];
    
    //平台服务费
    
//    UILabel *pingL = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(priceL5.frame), marWidth, cellHeight) andString:@"平台服务费"];
//    [self.priceView addSubview:pingL];
//    self.pingLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 214, CGRectGetMinY(pingL.frame), 200, cellHeight) andString:@""];
//    self.pingLabel.textAlignment = NSTextAlignmentRight;
//    self.pingLabel.textColor = Color_RGB(34, 34, 34, 1);
//    [self.priceView addSubview:self.pingLabel];
    
       
}

-(UILabel *)backLineLabelWithFloat:(CGFloat)y
{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, y + 1, screenWidth - 18, 0.5)];
    label.backgroundColor = LineGrayColor;
    return label;
}

/*
//初始化collectionview
-(void)initCollectionView
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(75 * kWidth, 75 * kHeight);
    
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.priceView.frame) + 10, screenWidth, 0 * (cellHeight + 100 * kHeight)) collectionViewLayout:layout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CollectionViewCell"];
    
    [self.collectionView registerClass:[CollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView"];
    
    
    self.collectionView.backgroundColor = [UIColor cyanColor];
    
        self.scrollView.contentSize = CGSizeMake(screenWidth, screenHeight * 1.5);
    
    [self.scrollView addSubview:self.collectionView];
}


-(void)initLastPayView
{
    self.lastPayView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.collectionView.frame) - 10, screenWidth, cellHeight)];
    self.lastPayView.backgroundColor = WhiteColor;
    
    [self.scrollView addSubview:self.lastPayView];
    
    
    //支付余款
    self.lastPayL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 100, 0, 80, cellHeight)];
    
    self.lastPayL.textColor = YellowColor;
    self.lastPayL.text = self.unpayLabel.text;
    self.lastPayL.font = [UIFont boldSystemFontOfSize:16 * kHeight];
    [self.lastPayView addSubview:self.lastPayL];
    
    UILabel *lastL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 300, 0, 200, cellHeight)];
    lastL.text = @"支付余额:";
    lastL.textAlignment = NSTextAlignmentRight;
    lastL.textColor = YellowColor;｀
    lastL.font = [UIFont boldSystemFontOfSize:16 * kHeight];
    [self.lastPayView addSubview:lastL];

    
}

 */
-(UIView *)backViewWithFrame:(CGRect)frame
{
    UIView *view = [[UIView alloc]initWithFrame:frame];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, frame.size.height - 0.5, screenWidth , 0.5)];
    label.backgroundColor = littleBlackColor;
//    [view addSubview:label];
    
    return view;
}



-(UILabel *)backLabelWithFrame:(CGRect)frame andString:(NSString *)string
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.text = string;
    label.textColor = fontGrayColor;
    label.font = Font(13);

    
    return label;
    
    
}

-(UILabel *)backLabelWithFrame:(CGRect)frame
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.textColor = littleBlackColor;
    label.font = Font(12);
    label.textAlignment = NSTextAlignmentCenter;
    
    return label;
    
    
}

/*

#pragma mark UICollectionView Delegate DataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
#warning 判断给一个还是两个分区
    
    return [[self.imageDictionary allKeys] count];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 2;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCell" forIndexPath:indexPath];
    
    
    if ([[self.imageDictionary allKeys] count] > 0) {
        
        NSString *string;
        NSURL *url ;
        
        if (indexPath.section == 0) {
            string = [NSString stringWithFormat:@"%@",self.imageDictionary[@"提车照片"][@"picKey"]];
            url = [NSURL URLWithString:string];
            
        } else {
            
            string = [NSString stringWithFormat:@"%@",self.imageDictionary[@"交车照片"][@"picKey"]];
            url = [NSURL URLWithString:string];
            
        }
        
        
        [cell.imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"image_holder"]];
        
        cell.stringLabel.text = @"车照照片";
        
    }

    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    CGSize size = {screenWidth,cellHeight};
    
    return size;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
    
}


-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    CollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView" forIndexPath:indexPath];
    
    
    if (indexPath.section == 0) {
        headerView.titleLabel.text = @"提车照片";
    } else {
        headerView.titleLabel.text = @"交车照片";
    }
    
    
    return  headerView;
    
    
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(12.5, 18, 12.5, 18);
}

*/
-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
