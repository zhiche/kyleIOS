//
//  FeeCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/10/15.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeeCell : UITableViewCell

@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UILabel *feeLabel;
@property (nonatomic,strong) UILabel *lineLabel;

@end
