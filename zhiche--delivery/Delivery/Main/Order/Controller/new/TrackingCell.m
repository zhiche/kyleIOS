//
//  TrackingCell.m
//  动态布局
//
//  Created by LeeBruce on 16/8/31.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "TrackingCell.h"
#import "Masonry.h"

//物流跟踪



@implementation TrackingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
        
    }
    
    return  self;
}

-(void)initSubViews
{
    __weak typeof(self) weakSelf = self;
    //提车／交车信息
    UILabel *label = [[UILabel alloc]init];
    [self.contentView addSubview:label];
    label.text = @"状态";
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(18);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(100,cellHeight));
    }];
    label.textColor = carScrollColor;
    label.font = Font(14);
    
    
    UILabel *label1 = [self backLabel];
    [self.contentView addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(18);
        make.height.mas_equalTo(0.5);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(label.mas_bottom).offset(0);
        
    }];
    
    //状态栏
    self.statusLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.statusLabel];
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(-18);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(80, cellHeight));
        
    }];
    self.statusLabel.font = Font(14);
    self.statusLabel.textColor = RedColor;
    self.statusLabel.textAlignment = NSTextAlignmentRight;
//    self.statusLabel.text = @"已提车";
    
    //车型
    
    UILabel *label3 = [[UILabel alloc]init];;
    [self.contentView addSubview:label3];
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(18);
        make.height.mas_equalTo(28 * kHeight);
        make.width.mas_equalTo(100);
        make.top.mas_equalTo(label1.mas_bottom).offset(0);
        
    }];
    label3.text = @"品牌/车型";
    label3.textColor = carScrollColor;
    label3.font = Font(14);
    
    
    
    self.carStyleLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.carStyleLabel];
    [self.carStyleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-18);
        make.top.and.height.equalTo(label3);
        make.left.mas_equalTo(label3.mas_right).offset(0);
        
    }];
    self.carStyleLabel.textColor = littleBlackColor;
    self.carStyleLabel.font = Font(14);
//    self.carStyleLabel.text = @"兰博基尼－大牛";
    self.carStyleLabel.textAlignment = NSTextAlignmentRight;
    
    
    //vin
    
    UILabel *label4 = [[UILabel alloc]init];;
    [self.contentView addSubview:label4];
    [label4 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(18);
        make.height.mas_equalTo(28 * kHeight);
        make.width.mas_equalTo(100);
        make.top.mas_equalTo(label3.mas_bottom).offset(0);
        
    }];
    label4.text = @"VIN";
    label4.textColor = carScrollColor;
    label4.font = Font(14);
    
    
    
    self.vinLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.vinLabel];
    [self.vinLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.width.height.equalTo(weakSelf.carStyleLabel);
        make.top.mas_equalTo(weakSelf.carStyleLabel.mas_bottom).offset(0);
    }];
    self.vinLabel.textColor = littleBlackColor;
    self.vinLabel.font = Font(14);
//    self.vinLabel.text = @"123123123123123121";
    self.vinLabel.textAlignment = NSTextAlignmentRight;
    
    
    
    UILabel *label2 = [self backLabel];
    [self.contentView addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(18);
        make.height.mas_equalTo(0.5);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(weakSelf.vinLabel.mas_bottom).offset(0);
        
    }];
    
    
    
    //辆数
    
    UILabel *label5 = [[UILabel alloc]init];;
    [self.contentView addSubview:label5];
    [label5 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(18);
        make.height.mas_equalTo(cellHeight);
        make.width.mas_equalTo(80);
        make.top.mas_equalTo(label2.mas_bottom).offset(0);
        
    }];
    label5.text = @"当前位置:";
    label5.textColor = littleBlackColor;
    label5.font = Font(13);
    
    
    
    self.locationLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.locationLabel];
    
    [self.locationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.and.height.equalTo(label5);
        make.width.mas_equalTo(150);
        make.left.mas_equalTo(label5.mas_right).offset(-4);
    }];
    self.locationLabel.textColor = littleBlackColor;
    self.locationLabel.font = Font(13);
//    self.locationLabel.text = @"杭州";
    
    
    
    //查看详情
    self.detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:self.detailButton];
    [self.detailButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(-18);
        make.centerY.equalTo(label5);
        make.size.mas_equalTo(CGSizeMake(100 * kWidth, 30 * kHeight));
        
    }];
    [self.detailButton setBackgroundColor:YellowColor];
    [self.detailButton setTitle:@"查看物流详情" forState:UIControlStateNormal];
    [self.detailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.detailButton.layer.cornerRadius = 5;
    self.detailButton.titleLabel.font = Font(14);
    
}

-(UILabel *)backLabel
{
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = GrayColor;
    return  label;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
