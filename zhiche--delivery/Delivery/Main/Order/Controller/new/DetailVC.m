
//
//  DetailVC.m
//  动态布局
//
//  Created by LeeBruce on 16/9/1.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "DetailVC.h"
#import "TopBackView.h"
#import "CollectionReusableView.h"
#import "CollectionViewCell.h"
#import <UIImageView+WebCache.h>
#import "ScrollImageViewController.h"
#import "Common.h"
#import "WKProgressHUD.h"


@interface DetailVC ()
@property (nonatomic,strong) UILabel *nameLabel;//交车联系人
@property (nonatomic,strong) UILabel *phoneLabel;//电话
@property (nonatomic,strong) UILabel *addressLabel;//地址

@property (nonatomic,strong) UILabel *driverLabel;//交车司机
@property (nonatomic,strong) UILabel *driverPhoneLabel;//电话
@property (nonatomic,strong) UILabel *idLabel;//身份证
@property (nonatomic,strong) UILabel *carNumberLabel;//车牌号

@property (nonatomic,strong) UIScrollView *scrollView;//

@property (nonatomic,strong) NSMutableDictionary *dataDictionary;//
@property (nonatomic,strong) WKProgressHUD *hud;



@end

@implementation DetailVC

- (void)viewDidLoad {
    [super viewDidLoad];

    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"详情"];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    
    self.dataDictionary = [NSMutableDictionary dictionary];
    
    [self initLabels];
    [self initSubViews];
    
    [self initDataSources];


}

-(void)initDataSources
{
    /*
    //假数据
    NSDictionary *dic = [NSDictionary dictionary];
    dic =  [Common txtChangToJsonWithString:@"jiao_xiangqing"];
    
    
    if ([[dic[@"data"] allKeys] count] > 0) {
        
        self.dataDictionary = [NSMutableDictionary dictionaryWithDictionary:dic[@"data"]];
        
        [self getValueString];
    }
    */
    
    
    //接口数据
    NSString *urlString = nil;

    //交车信息详情
    if ([self.titleString isEqualToString:@"交车照片"]) {
        urlString = [NSString stringWithFormat:@"%@?id=%ld",order_deinfo,[self.orderId integerValue]];

    } else {
        //提车信息详情

        urlString = [NSString stringWithFormat:@"%@?id=%ld",order_exinfo,[self.orderId integerValue]];

    }
    
     [Common requestWithUrlString:urlString contentType:application_json finished:^(id responseObj) {
     
     
     [self.hud dismiss:YES];
     
     if ([responseObj[@"success"] boolValue]) {
    
         self.dataDictionary = [NSMutableDictionary dictionaryWithDictionary:responseObj[@"data"]];
         
         if ([self.dataDictionary[@"pics"] isEqual:[NSNull null]] ) {
             
         } else {
             
             if ([self.dataDictionary[@"pics"] count] > 0) {
                 
                 [self.collectionView reloadData];

             } else {
                 self.collectionView.hidden = YES;
             }
         
         }

         
        [self getValueString];
     } else {
     
     [WKProgressHUD popMessage:[NSString stringWithFormat:@"%@",responseObj[@"message"]] inView:self.view duration:1.5 animated:YES];
     
     }
     
     
     } failed:^(NSString *errorMsg) {
     [self.hud dismiss:YES];
        
     
     NSLog(@"%@",errorMsg);
     }];
    

}

-(void)getValueString
{
    
    //交车联系人
    self.nameLabel.text = [self backString:self.dataDictionary[@"contact"]];
    //电话
    self.phoneLabel.text = [self backString: self.dataDictionary[@"phone"]];
    //地址
    self.addressLabel.text = [self backString:self.dataDictionary[@"addr"]];
    //交车司机
    self.driverLabel.text = [self backString:self.dataDictionary[@"drivername"]];
    //交车司机电话
    self.driverPhoneLabel.text = [self backString:self.dataDictionary[@"driverphone"]];
    //身份证号
    self.idLabel.text = [self backString:self.dataDictionary[@"drivercardid"]];
    //车牌号
    self.carNumberLabel.text = [self backString:self.dataDictionary[@"tracknumber"]];

}

-(NSString *)backString:(NSString *)string
{
    
    if ([string isEqual:[NSNull null]]) {
        return @"";
    } else {
        return string;
    }
}


-(void)initSubViews
{
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64)];
    self.scrollView.backgroundColor = GrayColor;
    [self.view addSubview:self.scrollView];
    
    /*
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, 100, cellHeight)];
    label.text = @"详情";
    label.font = Font(14);
    label.textColor = littleBlackColor;
    [view addSubview:label];
    
    UILabel *lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight - 0.5, screenWidth, 0.5)];
    lineLabel.backgroundColor = GrayColor;
    [view addSubview:lineLabel];
    
    [self.scrollView addSubview:view];
    */
    //提车联系人
    
    NSString *string1 ;
    if ([self.titleString isEqualToString:@"交车照片"]) {
        string1 = @"交车联系人";
    } else {
        string1 = @"提车联系人";

    }
    
    UIView *firstView = [self backViewWithOriginY:0 andString:string1 andLabel:self.nameLabel];
    [self.scrollView addSubview:firstView];
    
    //电话
    UIView *secondView = [self backViewWithOriginY:cellHeight  andString:@"电话" andLabel:self.phoneLabel ];
    [self.scrollView addSubview:secondView];
    
    //地址
    UIView *thirdView = [self backViewWithOriginY:cellHeight * 2 andString:@"地址" andLabel:self.addressLabel];
//    self.addressLabel.text = @"了解对方了解到了对肌肤来说 \n 理解啊来打飞机啦大家发来的警方";
    [self.scrollView addSubview:thirdView];
    
    //提车司机
    NSString *string2 ;
    if ([self.titleString isEqualToString:@"交车照片"]) {
        string2 = @"交车司机";
    } else {
        string2 = @"提车司机";
        
    }

    UIView *fourthView = [self backViewWithOriginY:cellHeight * 3 + 10  andString:string2 andLabel:self.driverLabel];
    [self.scrollView addSubview:fourthView];
    
    //电话
    
    UIView *fifthView = [self backViewWithOriginY:cellHeight * 4 + 10 andString:@"电话" andLabel:self.driverPhoneLabel];
    [self.scrollView addSubview:fifthView];
    //身份证
    UIView *sixView = [self backViewWithOriginY:cellHeight * 5 + 10 andString:@"身份证" andLabel:self.idLabel];
    [self.scrollView addSubview:sixView];

    //车牌号
    UIView *sevenView = [self backViewWithOriginY:cellHeight * 6 + 10 andString:@"车牌号" andLabel:self.carNumberLabel];
    [self.scrollView addSubview:sevenView];

    [self initCollectionView];
    
}

-(void)initLabels{
    
    self.nameLabel = [[UILabel alloc]init];
    self.phoneLabel = [[UILabel alloc]init];
    self.addressLabel = [[UILabel alloc]init];
    
    self.driverLabel = [[UILabel alloc]init];
    self.driverPhoneLabel = [[UILabel alloc]init];
    self.idLabel = [[UILabel alloc]init];
    self.carNumberLabel = [[UILabel alloc]init];
    

}


//初始化collectionview
-(void)initCollectionView
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(75 , 105 );
    
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, cellHeight * 7 + 20, screenWidth,  (cellHeight + 130 )) collectionViewLayout:layout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CollectionViewCell"];
    
    [self.collectionView registerClass:[CollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView"];
    
    self.collectionView.backgroundColor = WhiteColor;
    self.scrollView.contentSize = CGSizeMake(screenWidth, screenHeight + 1);
    
    [self.scrollView addSubview:self.collectionView];
}

-(UIView *)backViewWithOriginY:(CGFloat)y andString:(NSString *)string andLabel:(UILabel *)label{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, y,screenWidth , cellHeight)];
    view.backgroundColor = [UIColor whiteColor];

    UILabel *leftLable = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, 100, cellHeight)];
    leftLable.text = string;
    leftLable.textColor = carScrollColor;
    leftLable.font = Font(13);
    [view addSubview:leftLable];
    
    label.frame = CGRectMake(130, 0, screenWidth - 130 - 18, cellHeight);
    label.textColor = littleBlackColor;
    label.font = Font(13);
    label.numberOfLines = 0;
    [view addSubview:label];
    label.textAlignment = NSTextAlignmentRight;
    
    UILabel *bottomLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, cellHeight - 0.5, screenWidth - 18, 0.5)];
    bottomLabel.backgroundColor = GrayColor;
    [view addSubview:bottomLabel];
    
    return view;
 
}

#pragma mark UICollectionView Delegate DataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
#warning 判断给一个还是两个分区
    
//    return [[self.imageDictionary allKeys] count];
            return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 2;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    

    
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCell" forIndexPath:indexPath];
    cell.backgroundColor = WhiteColor;
    cell.imageView.image = [UIImage imageNamed:@"order_picture"];
    cell.stringLabel.text = @"";
    
    NSString *string;
    NSURL *url ;
    if ([self.dataDictionary[@"pics"]  count] > 0) {
        
            string = [NSString stringWithFormat:@"%@",self.dataDictionary[@"pics"][indexPath.row][@"picurl"]];
            url = [NSURL URLWithString:string];
        
        [cell.imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"image_holder"]];
    } else {
        
//        self.collectionView.hidden = YES;
        
    }

//            cell.stringLabel.text = [NSString stringWithFormat:@"%@",self.imageDictionary[@"提车照片"][indexPath.row][@"typeText"]];
        
//        } else {
//            
//            string = [NSString stringWithFormat:@"%@",self.dataDictionary[@"pics"][indexPath.row][@"picurl"]];
//            url = [NSURL URLWithString:string];
        
//            cell.stringLabel.text = [NSString stringWithFormat:@"%@",self.imageDictionary[@"交车照片"][indexPath.row][@"typeText"]];
            
//        }
        
        


        
        //        [cell.imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"image_holder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        //
        //            if (error != nil) {
        //
        //                cell.imageView.image = [UIImage imageNamed:@"bad_picture"];
        //            }
        //
        //
        //        }];
        
        //
    
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    CGSize size = {screenWidth,cellHeight};
    
    return size;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
    
}


-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    CollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView" forIndexPath:indexPath];
    
    headerView.titleLabel.text = self.titleString;
    
    return  headerView;
    
    
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(12.5, 18, 4.5, 18);
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *string;
    NSURL *url ;
    
    NSMutableArray *arr = [NSMutableArray array];
    
    
    if ([self.dataDictionary[@"pics"]  count] > 0) {
        
        for (int i = 0; i < [self.dataDictionary[@"pics"]  count]; i ++) {
            
            string = [NSString stringWithFormat:@"%@",self.dataDictionary[@"pics"][i][@"picurl"]];
            url = [NSURL URLWithString:string];
            
            [arr addObject:url];
            
        }
                
    }

    
    /*
    if (indexPath.section == 0) {
        
        for (int i = 0; i < [self.imageDictionary[@"提车照片"] count]; i++) {
            
            string = [NSString stringWithFormat:@"%@",self.imageDictionary[@"提车照片"][i][@"picKey"]];
            
            
            url = [NSURL URLWithString:string];
            [arr addObject:url];
            
        }
        
        
    } else {
        
        for (int i = 0; i < [self.imageDictionary[@"提车照片"] count]; i++) {
            
            string = [NSString stringWithFormat:@"%@",self.imageDictionary[@"交车照片"][i][@"picKey"]];
            url = [NSURL URLWithString:string];
            [arr addObject:url];
            
        }
        
    }
     */
    
   ScrollImageViewController *scrollV = [[ScrollImageViewController alloc]init];
    scrollV.imageArr = arr;
    
    [self.navigationController pushViewController:scrollV animated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
