//
//  DetailVC.h
//  动态布局
//
//  Created by LeeBruce on 16/9/1.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) NSString *titleString;
@property (nonatomic,copy) NSString *orderId;

@end
