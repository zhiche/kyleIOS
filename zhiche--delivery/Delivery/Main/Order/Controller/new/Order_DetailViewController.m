//
//  Order_DetailViewController.m
//  动态布局
//
//  Created by LeeBruce on 16/9/5.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#define labelHeight 15 * kHeight
#define labelWidth 70 * kWidth
#define leftMargn 18

#import "Order_DetailViewController.h"
#import "CarInfoCell.h"
#import "TopBackView.h"
#import "FinishOrderDetailVC.h"
#import "FeeCell.h"

@interface Order_DetailViewController ()
@end

@implementation Order_DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    self.dataDictionary = [NSMutableDictionary dictionary];
    self.dataArr = [NSMutableArray array];
    self.heightArray = [NSMutableArray array];
    
    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"订单详情"];
    _flag = NO;
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = WhiteColor;
    
    [self initSubviews];
    
    self.rotateButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rotateButton.frame = CGRectMake(0, 0, screenWidth, cellHeight);
//    [self.rotateButton setImage:[UIImage imageNamed:@"btn_rotate"] forState:UIControlStateNormal];
    self.arrowImageView = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth - 18 -11.5, (cellHeight - 18.5)/2.0, 11.5, 18.5)];
    self.arrowImageView.image = [UIImage imageNamed:@"btn_rotate"];
    [self.rotateButton addSubview:self.arrowImageView];
}

-(void)initSubviews
{
    
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight)];
    self.scrollView.contentSize = CGSizeMake(screenWidth, screenHeight * 1.5);
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = GrayColor;
    
    
    /**
     
     订单信息
     */
    UIView *orderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 113 * kHeight)];
    [self.scrollView addSubview:orderView];
    orderView.backgroundColor = WhiteColor;
    //订单号
    
    UIView *orderV1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 50*kHeight)];
    [orderView addSubview:orderV1];
    orderV1.backgroundColor = headerBottomColor;
    
    
    
    UILabel *orderL = [self backLabelWithFrame:CGRectMake(leftMargn, 0, 60, cellHeight) andString:@"订单号:"];
    orderL.textColor = AddCarColor;
    [orderV1 addSubview:orderL];
    
    self.numberLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(orderL.frame) + 5, CGRectGetMinY(orderL.frame), 300, cellHeight) andString:@""];
    self.numberLabel.textColor = littleBlackColor;
    [orderV1 addSubview:self.numberLabel];
    self.createTime = [self backLabelWithFrame:CGRectMake(leftMargn, 18*kHeight, 300, cellHeight) andString:@""];
    self.createTime.font = Font(FontOfSize10);
    self.createTime.textColor = littleBlackColor;
    [orderV1 addSubview:self.createTime];
    
    //状态栏
    self.statusLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 14 - 100, CGRectGetMinY(orderL.frame)+7, 100, cellHeight) ];
    self.statusLabel.textColor = RedColor;
//    self.statusLabel.text = @"已交车";
    self.statusLabel.textAlignment = NSTextAlignmentRight;
    //    [orderView addSubview:self.statusLabel];
    
    
    UILabel *labe = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight+7*kHeight, screenWidth, 0.5)];
    labe.backgroundColor = LineGrayColor;
    [orderV1 addSubview:labe];
    
    //起始地址
    self.startLabel = [self backLabelWithFrame:CGRectMake(10 ,  CGRectGetMaxY(orderV1.frame) + 6*kHeight, labelWidth + 20 * kWidth, labelHeight*2 + 6 * kHeight) ];
//    self.startLabel.text = @"阿道夫";
    self.startLabel.font = Font(13)
//    self.startLabel.adjustsFontSizeToFitWidth = YES;
    [orderView addSubview:self.startLabel];
    self.startLabel.numberOfLines = 0;
    
    //起始日期
    self.startDate = [self backLabelWithFrame:CGRectMake(20 * kWidth, CGRectGetMaxY(self.startLabel.frame) , labelWidth, labelHeight)];
//    self.startDate.text = @"2010-01-01";
    self.startDate.textColor = littleBlackColor;
    self.startDate.font = Font(10);
    [orderView addSubview:self.startDate];
    
    
   
    
//    //起始详情地址
//    self.startDetailLabel = [self backLabelWithFrame:CGRectMake(20 * kHeight, CGRectGetMaxY(self.startLabel.frame) , labelWidth, 28 * kHeight)];
//    self.startDetailLabel.text = @"";
//    self.startDetailLabel.adjustsFontSizeToFitWidth = YES;
//    [orderView addSubview:self.startDetailLabel];
//    self.startDetailLabel.font = Font(9);
//    self.startDetailLabel.numberOfLines = 0;
    
    
    //图片
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMidX(self.view.frame) - 30, CGRectGetMidY(self.startLabel.frame) + 3, 47, 12.5)];
    [orderView addSubview:imageView];
    imageView.image = [UIImage imageNamed:@"Quote_Cell_logo"];
    
    //目的地
    self.endLabel = [self backLabelWithFrame:CGRectMake(screenWidth - labelWidth - 20 * kWidth - 10, CGRectGetMaxY(labe.frame) + 6*kHeight, labelWidth + 20 * kWidth, labelHeight *2 + 6 * kHeight)];
    [orderView addSubview:self.endLabel];
//    self.endLabel.text = @"阿斯顿发多";
    self.endLabel.font = Font(13);
    self.endLabel.numberOfLines = 0;
    
    //目的日期
    self.endDateLabel = [self backLabelWithFrame:CGRectMake(screenWidth - labelWidth  - 20 * kWidth, CGRectGetMaxY(self.endLabel.frame) , labelWidth, labelHeight)];
//    self.endDateLabel.text = @"2015-01-01";
    [orderView addSubview:self.endDateLabel];
    self.endDateLabel.font = Font(10);
    
  
    
    
//    //目的详情地址
//    self.endDetailLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMinX(self.endDateLabel.frame), CGRectGetMaxY(self.endLabel.frame) + 0, labelWidth, 28 * kHeight)];
//    self.endDetailLabel.adjustsFontSizeToFitWidth = YES;
//    self.endDetailLabel.text = @"";
//    [orderView addSubview:self.endDetailLabel];
//    self.endDetailLabel.font = Font(9);
//    self.endDetailLabel.numberOfLines = 0;
    
    
//    
//    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.endDetailLabel.frame) + 12 * kHeight, screenWidth , 0.5)];
//    label.backgroundColor = LineGrayColor;
//    [orderView addSubview:label];
//    
//    
//    self.scrollV = [[UIScrollView alloc]initWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(label.frame), screenWidth - leftMargn * 2, cellHeight)];
//    self.scrollV.contentSize = CGSizeMake(screenWidth + 100, cellHeight);
//    self.scrollV.alwaysBounceHorizontal = YES;
//    self.scrollV.showsHorizontalScrollIndicator = NO;
//    [orderView addSubview:self.scrollV];
//    
//    self.carLabel = [self backLabelWithFrame:CGRectMake(0, 0, screenWidth + 100, cellHeight) ];
//    self.carLabel.text = @"";
//    self.carLabel.textColor = carScrollColor;
//    self.carLabel.textAlignment = NSTextAlignmentLeft;
//    self.carLabel.font = Font(13);
//    [self.scrollV addSubview:self.carLabel];
//    
//    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.carLabel.frame), screenWidth, 0.5)];
//    [orderView addSubview:label1];
//    label1.backgroundColor = LineGrayColor;
//    
    
/*
    
    /*
     
     承运人信息
     */
    
//    
//    UIView *deleverView = [self backViewWithFrame:CGRectMake(0, CGRectGetMaxY(orderView.frame) + 10, screenWidth, cellHeight * 5)];
//    //    [self.scrollView addSubview:deleverView];
//    deleverView.backgroundColor = WhiteColor;
//    
//    UIView *delever1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight)];
//    [deleverView addSubview:delever1];
//    delever1.backgroundColor = headerBottomColor;
//    
//    UILabel *deleverLabel = [self backLabelWithFrame:CGRectMake(leftMargn, 0, 100 * kWidth, cellHeight) andString:@"当前定位城市:"];
//    deleverLabel.font = Font(14);
//    deleverLabel.textColor = littleBlackColor;
//    [delever1 addSubview:deleverLabel];
//    
//    self.cityLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(deleverLabel.frame), 0, 150, cellHeight) andString:@""];
//    self.cityLabel.font = Font(14);
//    self.cityLabel.textColor = littleBlackColor;
//    [delever1 addSubview:self.cityLabel];
//    
//    
//    UILabel *deleverL1 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(deleverLabel.frame), screenWidth, 0.5)];
//    deleverL1.backgroundColor = LineGrayColor;
//    [deleverView addSubview:deleverL1];
//    
//    //姓名
//    
//    UILabel *nameL = [self backLabelWithFrame:CGRectMake(leftMargn,  CGRectGetMaxY(deleverL1.frame), 60, cellHeight) andString:@"姓名"];
//    [deleverView addSubview:nameL];
//    nameL.textColor = fontGrayColor;
//    
//    self.nameLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 114, CGRectGetMinY(nameL.frame), 100, cellHeight) andString:@""];
//    self.nameLabel.textAlignment = NSTextAlignmentRight;
//    self.nameLabel.textColor = Color_RGB(34, 34, 34, 1);
//    [deleverView addSubview:self.nameLabel];
//    
//    
//    
//    UILabel *deleverL2 = [self backLineLabelWithFloat: CGRectGetMaxY(nameL.frame)];
//    
//    [deleverView addSubview:deleverL2];
//    
//    
//    //车牌
//    UILabel *carNumberL = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(nameL.frame), 60, cellHeight) andString:@"车牌"];
//    [deleverView addSubview:carNumberL];
//    carNumberL.textColor = fontGrayColor;
//    
//    self.carNumberLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 114, CGRectGetMinY(carNumberL.frame) + 1, 100, cellHeight) andString:@""];
//    self.carNumberLabel.textAlignment = NSTextAlignmentRight;
//    self.carNumberLabel.textColor = Color_RGB(34, 34, 34, 1);
//    [deleverView addSubview:self.carNumberLabel];
//    
//    
//    UILabel *deleverL3 = [self backLineLabelWithFloat:CGRectGetMaxY(carNumberL.frame)];
//    
//    [deleverView addSubview:deleverL3];
//    
//    //电话
//    
//    UILabel *phoneL = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(carNumberL.frame), 60, cellHeight) andString:@"电话"];
//    [deleverView addSubview:phoneL];
//    phoneL.textColor = fontGrayColor;
//    
//    self.phoneLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 114, CGRectGetMinY(phoneL.frame) + 1, 100, cellHeight) andString:@""];
//    self.phoneLabel.textAlignment = NSTextAlignmentRight;
//    self.phoneLabel.textColor = Color_RGB(34, 34, 34, 1);
//    [deleverView addSubview:self.phoneLabel];
//    
//    
//    UILabel *deleverL4 = [self backLineLabelWithFloat:CGRectGetMaxY(phoneL.frame) ];
//    [deleverView addSubview:deleverL4];
//    
//    
//    
//    //车型
//    
//    UILabel *carStyle = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(phoneL.frame), 60, cellHeight) andString:@"车型"];
//    [deleverView addSubview:carStyle];
//    carStyle.textColor = fontGrayColor;
//    
//    self.carStyleLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 114, CGRectGetMinY(carStyle.frame), 100, cellHeight) andString:@""];
//    self.carStyleLabel.textColor = Color_RGB(34, 34, 34, 1);
//    self.carStyleLabel.textAlignment = NSTextAlignmentRight;
//    [deleverView addSubview:self.carStyleLabel];
//
    
    [self initTableView];
    
    /**
     运费信息
     */
    
    self.feeTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.tableView.frame) + 10, screenWidth, cellHeight) style:UITableViewStylePlain];
    self.feeTableView.delegate = self;
    self.feeTableView.dataSource = self;
    self.feeTableView.tableFooterView = [[UIView alloc]init];
    self.feeTableView.scrollEnabled = NO;
    self.feeTableView.separatorStyle = NO;
    [self.scrollView addSubview:self.feeTableView];
    
    /*
    self.priceView = [self backViewWithFrame:CGRectMake(0, CGRectGetMaxY(self.tableView.frame) + 10, screenWidth, cellHeight * 5 + 5)];
    
    [self.scrollView addSubview:self.priceView];
    self.priceView.backgroundColor = WhiteColor;
    
    UILabel *priceL1 = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight, screenWidth, 0.5)];
    priceL1.backgroundColor = LineGrayColor;
    
    [self.priceView addSubview:priceL1];
    
    
    UIView *priceV1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight)];
    [self.priceView addSubview:priceV1];
    priceV1.backgroundColor = headerBottomColor;
    
    
    UILabel *priceLabel = [self backLabelWithFrame:CGRectMake(leftMargn, 0, 100, cellHeight) andString:@"费用"];
    priceLabel.font = Font(13);
    priceLabel.textColor = AddCarColor;
    [priceV1 addSubview:priceLabel];
    //提车费
    
    CGFloat marWidth = 65 * kWidth;
    
    UILabel *priceL = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(priceV1.frame) , marWidth, cellHeight) andString:@"提车费"];
    [self.priceView addSubview:priceL];
    
    self.totalLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 214, CGRectGetMinY(priceL.frame), 200, cellHeight) andString:@""];
    self.totalLabel.textAlignment = NSTextAlignmentRight;
    self.totalLabel.textColor = AddCarColor;
    [self.priceView addSubview:self.totalLabel];
    
    
    UILabel *priceL2 = [self backLineLabelWithFloat:CGRectGetMaxY(priceL.frame)];
    
    [self.priceView addSubview:priceL2];
    
    
    //交车费
    UILabel *payL = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(priceL2.frame), marWidth, cellHeight) andString:@"交车费"];
    [self.priceView addSubview:payL];
    
    self.payLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 214, CGRectGetMinY(payL.frame), 200, cellHeight) andString:@""];
    self.payLabel.textAlignment = NSTextAlignmentRight;
    self.payLabel.textColor = AddCarColor;
    [self.priceView addSubview:self.payLabel];
    
    
    UILabel *priceL3 = [self backLineLabelWithFloat:CGRectGetMaxY(payL.frame)];
    [self.priceView addSubview:priceL3];
    
    
    
    //保险费
    
    UILabel *unpayL = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(priceL3.frame),marWidth, cellHeight) andString:@"保险费"];
    [self.priceView addSubview:unpayL];
    
    self.unpayLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 214, CGRectGetMinY(unpayL.frame), 200, cellHeight) andString:@""];
    self.unpayLabel.textColor = YellowColor;
    self.unpayLabel.textAlignment = NSTextAlignmentRight;
    self.unpayLabel.textColor = AddCarColor;
    [self.priceView addSubview:self.unpayLabel];

    
    //运输费
    
    UILabel *priceL4 = [self backLineLabelWithFloat:CGRectGetMaxY(unpayL.frame)];
    [self.priceView addSubview:priceL4];

    UILabel *scoreL = [self backLabelWithFrame:CGRectMake(leftMargn, CGRectGetMaxY(priceL4.frame), marWidth, cellHeight) andString:@"运输费"];
    [self.priceView addSubview:scoreL];
    
    self.scoreLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 214, CGRectGetMinY(scoreL.frame), 200, cellHeight) andString:@""];
    self.scoreLabel.textAlignment = NSTextAlignmentRight;
    self.scoreLabel.textColor = AddCarColor;
    [self.priceView addSubview:self.scoreLabel];
    
    */
    
    
//    UILabel *priceL5 = [self backLineLabelWithFloat:CGRectGetMaxY(priceL4.frame)];
//    
//    [self.priceView addSubview:priceL5];
    
    self.amountLabel = [[UILabel alloc]init];
    self.carStautsLabel = [[UILabel alloc]init];
    
      
    
}

-(UILabel *)backLineLabelWithFloat:(CGFloat)y
{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, y + 1, screenWidth - 18, 0.5)];
    label.backgroundColor = LineGrayColor;
    return label;
}

-(void)initTableView{
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 116 * kHeight, screenWidth, cellHeight * 3 ) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.scrollEnabled = NO;
    
    
    [self.scrollView addSubview:self.tableView];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (self.tableView == tableView) {
        
        return (_flag)?self.dataArr.count:0 ;

    } else {
        return ([self.dataDictionary[@"feeDetail"] count] > 0) ? [self.dataDictionary[@"feeDetail"] count]:0;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return cellHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    Model *model = [[Model alloc]init];
//    model = self.dataArr[indexPath.row];
//    
//    NSString *string = model.vinList[0][@"vin"];
//    for (int i = 1; i < model.vinList.count ; i ++) {
// 
//        
//        string = [string stringByAppendingString:[NSString stringWithFormat:@"\n%@ ",model.vinList[i][@"vin"]]];
//
//        
//    }
//
//     model.contact = string;
//    
//    CGFloat height = [CarInfoCell cellHeightWithModel:model];

    if (tableView == self.tableView) {
        return  56 * kHeight ;

    } else {
        return  cellHeight;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (tableView == self.tableView) {
        return  _footHeight;

    } else {
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == self.tableView) {
        
    
    NSString *str = @"newCell";
    CarInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    if (!cell) {
        cell = [[CarInfoCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:str];
    }
    
//    if (_integer == 3) {
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        
//    } else {
//    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    if (self.dataArr.count > 0) {
        cell.model = self.dataArr[indexPath.row];

    }
    
    return cell;
        
    } else {
       
        NSString *str = @"feeCell";
        FeeCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
        if (!cell) {
            cell = [[FeeCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:str];
        }
        
   
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        
        if (indexPath.row == [self.dataDictionary[@"feeDetail"] count] - 1) {
            cell.lineLabel.hidden = YES;
        }
        
        
        if ([self.dataDictionary[@"feeDetail"] count] > 0) {
            
            cell.nameLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"feeDetail"][indexPath.row][@"feeName"]];
            
            CGFloat price = [self.dataDictionary[@"feeDetail"][indexPath.row][@"actualAmount"] floatValue];
            
            cell.feeLabel.text = [NSString stringWithFormat:@"￥%.2f",price];

            
        }
        
        return cell;
        
    }
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectZero];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, 100, cellHeight)];
    
    if (tableView == self.tableView) {
        label.text = @"车辆信息";

    } else {
        label.text = @"费用";

    }
    
    label.textColor = AddCarColor;
    label.font = Font(13);
    [view addSubview:label];
    
    [view addSubview:self.rotateButton];

    
    UILabel *labe = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight - 0.5, screenWidth, 0.5)];
    labe.backgroundColor = LineGrayColor;
    [view addSubview:labe];
    
    return view;
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectZero];
    
    //分割线
    UILabel *labe = [[UILabel alloc]initWithFrame:CGRectMake(18,  -0.5, screenWidth, 0.5)];
    labe.backgroundColor = LineGrayColor;
    [view addSubview:labe];

    //汽车状态
    UILabel *label = [self backLabelWithFrame:CGRectMake(18, cellHeight, 80, cellHeight) andString:@"汽车状态"];
  
    [view addSubview:label];
    
    self.carStautsLabel.frame = CGRectMake(CGRectGetMaxX(label.frame), cellHeight, 200, cellHeight);
    self.carStautsLabel.font = Font(13);
    self.carStautsLabel.textColor = YellowColor;
    [view addSubview:self.carStautsLabel];
//    self.carStautsLabel.text = @"不能动  新车";
    

    
    UILabel *lineL = [[UILabel alloc]initWithFrame:CGRectMake(18, cellHeight , screenWidth, 0.5)];
    lineL.backgroundColor = LineGrayColor;
    [view addSubview:lineL];

    //汽车总数
    UILabel *amount = [self backLabelWithFrame:CGRectMake(18, 0, 80, cellHeight) andString:@"车辆总数"];
    
    [view addSubview:amount];
    
   self.amountLabel.frame = CGRectMake(CGRectGetMaxX(amount.frame), 0, 172, cellHeight);
//    self.amountLabel.text = @"10辆";
    self.amountLabel.textColor = YellowColor;
    self.amountLabel.font = Font(13);
    [view addSubview:self.amountLabel];
    
        return view;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    FinishOrderDetailVC *otherVC = [[FinishOrderDetailVC alloc]init];
//    [self.navigationController pushViewController:otherVC animated:YES];
}




-(UIView *)backViewWithFrame:(CGRect)frame
{
    UIView *view = [[UIView alloc]initWithFrame:frame];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, frame.size.height - 0.5, screenWidth , 0.5)];
    label.backgroundColor = littleBlackColor;
    //    [view addSubview:label];
    
    return view;
}



-(UILabel *)backLabelWithFrame:(CGRect)frame andString:(NSString *)string
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.text = string;
    label.textColor = fontGrayColor;
    label.font = Font(13);
    return label;
    
}

-(UILabel *)backLabelWithFrame:(CGRect)frame
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.textColor = littleBlackColor;
    label.font = Font(12);
    label.textAlignment = NSTextAlignmentCenter;
    
    return label;

}

-(void)backAction{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self.navigationController setNavigationBarHidden:YES];
//
//    RootViewController *rootVC = [RootViewController defaultsTabBar];
//    [rootVC setTabBarHidden:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
