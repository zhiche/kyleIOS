//
//  Model.m
//  动态布局
//
//  Created by LeeBruce on 16/9/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "Model.h"

@implementation Model

- (void)setValue:(id)value forUndefinedKey:(NSString *)key  {
    if([key isEqualToString:@"id"])
        self.ID = value;
}

@end
