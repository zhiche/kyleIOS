//
//  Model.h
//  动态布局
//
//  Created by LeeBruce on 16/9/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Model : NSObject
@property (nonatomic,copy) NSString *contact;
@property (nonatomic,copy) NSString *vehicleName;//车系
@property (nonatomic,copy) NSString *brandName;//品牌
@property (nonatomic,copy) NSString *brandlogo;//logo
@property (nonatomic,copy) NSString *amount;
@property (nonatomic,copy) NSString *vin;//vin
@property (nonatomic,copy) NSString *ID;
@property (nonatomic,copy) NSString *createTime;
@property (nonatomic,copy) NSString *info;
@property (nonatomic,copy) NSString *status;



@end
