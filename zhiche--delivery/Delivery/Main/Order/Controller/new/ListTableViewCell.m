//
//  ListTableViewCell.m
//  动态布局
//
//  Created by LeeBruce on 16/9/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ListTableViewCell.h"
#import "Masonry.h"



@implementation ListTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
        
    }
    
    return  self;
}

//-(void)initSubViews
//{
//    
////    CGRect rect = [@"啊啦大家发来的警方了李峰家\n大发生\n史蒂夫" boundingRectWithSize:CGSizeMake(screenWidth - 20, 200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12 * kWidth]} context:nil];
//
//
//    self.titleL = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, screenWidth - 20, 10)];
//    [self.contentView addSubview:self.titleL];
////    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
////       
////        make.top.left.mas_equalTo(10);
////        make.width.mas_equalTo(screenWidth - 20);
//////        make.bottom.mas_equalTo(self.dateLabel.mas_top).offset(0);
////        make.height.mas_equalTo(10);
////        
////    }];
//    self.titleL.font = Font(12);
//    self.titleL.textColor = YellowColor;
//    self.titleL.numberOfLines = 0;
//    
//    self.dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.titleL.frame), 100, 22.5 * kHeight)];
//    [self.contentView addSubview:self.dateLabel];
////    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
////       
////        make.left.mas_equalTo(10);
////        make.top.mas_equalTo(self.titleL.mas_bottom).offset(0);
////        make.size.mas_equalTo(CGSizeMake(100, 22.5 * kHeight));
////    }];
//    self.dateLabel.font = Font(12);
//    self.dateLabel.textColor = YellowColor;
//    
//    self.timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxY(self.dateLabel.frame), CGRectGetMinY(self.dateLabel.frame), 100, 22.5 * kHeight)];
//    [self.contentView addSubview:self.timeLabel];
//    
////    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
////        make.left.mas_equalTo(self.dateLabel.mas_right).offset(10);
////        make.top.height.width.equalTo(self.dateLabel);
////    }];
//    self.timeLabel.font = Font(12);
//    self.timeLabel.textColor = YellowColor;
//    
//    
//}


-(void)initSubViews
{
    
    //    CGRect rect = [@"啊啦大家发来的警方了李峰家\n大发生\n史蒂夫" boundingRectWithSize:CGSizeMake(screenWidth - 20, 200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12 * kWidth]} context:nil];
    
    
    self.titleL = [[UILabel alloc]init];
    [self.contentView addSubview:self.titleL];
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
    
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(5);
            make.width.mas_equalTo(screenWidth - 58);
    //        make.bottom.mas_equalTo(self.dateLabel.mas_top).offset(0);
            make.height.mas_equalTo(22.5 * kHeight);
    
        }];
    self.titleL.font = Font(12);
    self.titleL.textColor = YellowColor;
    self.titleL.numberOfLines = 0;
//    self.titleL.adjustsFontSizeToFitWidth = YES;
    
    self.dateLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.dateLabel];
        [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
    
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.titleL.mas_bottom).offset(0);
            make.size.mas_equalTo(CGSizeMake(screenWidth - 58, 22.5 * kHeight));
        }];
    self.dateLabel.font = Font(12);
    self.dateLabel.textColor = YellowColor;
    
//    self.timeLabel = [[UILabel alloc]init];
//    [self.contentView addSubview:self.timeLabel];
//    
//        [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(self.dateLabel.mas_right).offset(10);
//            make.top.height.width.equalTo(self.dateLabel);
//        }];
//    self.timeLabel.font = Font(12);
//    self.timeLabel.textColor = YellowColor;
    
    
}


-(void)setModel:(Model *)model
{
    if (_model != model) {
        
        CGRect rect = [model.contact boundingRectWithSize:CGSizeMake(screenWidth - 58, 200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12 * kWidth]} context:nil];
        
        CGFloat height = 0;
        if (rect.size.height > 22.5 * kHeight) {
            height = rect.size.height + 10;
        } else {
            height = 22.5 * kHeight;
        }
        
        
        [self.titleL mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(height);
        }];
        
        
    }
}


+(CGFloat)cellHeightWithModel:(Model *)model
{
    CGRect rect = [model.info boundingRectWithSize:CGSizeMake(screenWidth - 58, 200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12 * kWidth]} context:nil];
    
    if (rect.size.height > 22.5 * kHeight) {
        return  22.5 * kHeight + rect.size.height + 20;
    } else {
        return 45 * kHeight + 10;
    }
    
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
