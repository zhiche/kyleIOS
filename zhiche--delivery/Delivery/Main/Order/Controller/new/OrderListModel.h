//
//  OrderListModel.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/9/19.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderListModel : NSObject

@property (nonatomic,copy) NSString *waybillId;
@property (nonatomic,copy) NSString *statusText;
@property (nonatomic,copy) NSString *amount;
@property (nonatomic,strong) NSArray *sorders;
@end
