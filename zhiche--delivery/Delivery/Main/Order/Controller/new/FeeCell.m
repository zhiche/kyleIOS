//
//  FeeCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/10/15.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "FeeCell.h"
#import <Masonry.h>
@implementation FeeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
        
    }
    
    return  self;
}

-(void)initSubViews
{
   
   
    self.nameLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(18);
        make.size.mas_equalTo(CGSizeMake(200, cellHeight));
    }];
    self.nameLabel.font = Font(13);
    self.nameLabel.textColor = fontGrayColor;
    //    self.carStyleLabel.text = @"阿斯顿马丁－阿斯顿马丁";
    
    
    
    self.feeLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.feeLabel];
    [self.feeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(-18);
        make.size.mas_equalTo(CGSizeMake(100, cellHeight));
    }];
    self.feeLabel.font = Font(13);
    self.feeLabel.textAlignment = NSTextAlignmentRight;
    self.feeLabel.textColor = AddCarColor;
    //    self.carStyleLabel.text = @"阿斯顿马丁－阿斯顿马丁";

    self.lineLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.lineLabel];
    [self.lineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(18);
        make.top.mas_equalTo(self.feeLabel.mas_bottom).offset(-0.5);
        make.size.mas_equalTo(CGSizeMake(screenWidth - 18, 0.5));
        
    }];
    
    self.lineLabel.backgroundColor = LineGrayColor;
    
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
