//
//  CarInfoCell.m
//  动态布局
//
//  Created by LeeBruce on 16/8/31.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//



#import "CarInfoCell.h"
#import "Masonry.h"

//车辆信息
@implementation CarInfoCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
        
    }
    
    return  self;
}

-(void)initSubViews
{
  //品牌／车型
    UILabel *label1 = [[UILabel alloc]init];
    [self.contentView addSubview:label1];
    label1.text = @"品牌/车型";
    label1.font = Font(12);
    label1.textColor = carScrollColor;
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(18);
        make.width.mas_equalTo(100);
        make.top.mas_equalTo(5 * kWidth);
//        make.height.equalTo(self.contentView.mas_height ).multipliedBy(0.4);//设置高度为self.view高度的一半
        make.height.mas_offset(23.5  * kHeight);

        
    }];
    
    self.carStyleLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.carStyleLabel];
    [self.carStyleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(5 * kWidth);
        make.right.mas_equalTo(-18);
        make.left.mas_equalTo(label1.mas_right).offset(0);
//        make.height.equalTo(self.contentView.mas_height).multipliedBy(0.4);
        make.height.mas_offset(23.5  * kHeight);

    }];
    self.carStyleLabel.font = Font(12);
    self.carStyleLabel.textAlignment = NSTextAlignmentRight;
    self.carStyleLabel.textColor = BlackTitleColor;
//    self.carStyleLabel.text = @"阿斯顿马丁－阿斯顿马丁";
    
    
    //vin
    UILabel *label2 = [[UILabel alloc]init];
    [self.contentView addSubview:label2];
    label2.text = @"VIN";
    label2.font = Font(12);
    label2.textColor = carScrollColor;
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(18);
        make.width.mas_equalTo(100);
        make.top.mas_equalTo(label1.mas_bottom).offset(0);
        make.height.equalTo(label1);
        
    }];
//    
    self.vinLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.vinLabel];
    [self.vinLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(label1.mas_bottom).offset(0);
//        make.width.mas_equalTo(screenWidth - 136);
        make.right.mas_equalTo(-18);

        make.left.mas_equalTo(label2.mas_right).offset(0);
//        make.height.equalTo(self.contentView.mas_height).multipliedBy(0.4);
        make.height.mas_equalTo(23.5 * kHeight);
    }];
    self.vinLabel.font = Font(12);
    self.vinLabel.textAlignment = NSTextAlignmentRight;
    self.vinLabel.textColor = BlackTitleColor;
    self.vinLabel.numberOfLines = 0;

}

-(void)setModel:(Model *)model
{
    if (_model != model) {
        
        self.carStyleLabel.text = [NSString stringWithFormat:@"%@-%@",model.brandName,model.vehicleName];
        
        NSString *string = model.vin;
        
        
//        for (int i = 1; i < model.vinList.count ; i ++) {
//            
//            string = [string stringByAppendingString:[NSString stringWithFormat:@"\n%@ ",model.vinList[i][@"vin"]]];
//            
//        }
        
        self.vinLabel.text = string;
        
//        CGRect rect = [string boundingRectWithSize:CGSizeMake(screenWidth - 136, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12 * kWidth]} context:nil];
//        
//        CGFloat height = 0;
//        
//        if (rect.size.height > 23.5 * kHeight) {
//            
//            height = rect.size.height + 10;
//            
//        } else {
//          height = 23.5 * kHeight;
//        }
//        
//        [self.vinLabel mas_updateConstraints:^(MASConstraintMaker *make) {
//            
//            make.height.mas_equalTo(height);
//        }];

    }
}

+(CGFloat)cellHeightWithModel:(Model *)model
{

    CGRect rect = [model.contact boundingRectWithSize:CGSizeMake(screenWidth - 136, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12 * kWidth]} context:nil];
    
    if (rect.size.height > 23.5 * kHeight) {
        
        return  33.5 * kHeight + rect.size.height +10;
        
    } else {
        return 56 * kHeight;
    }

    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
