//
//  NewTrackingCell.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 17/9/5.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewBtn.h"
@interface NewTrackingCell : UITableViewCell

@property (nonatomic,strong) UILabel *statusLabel;
@property (nonatomic,strong) UILabel *carStyleLabel;
@property (nonatomic,strong) UILabel *vinLabel;
@property (nonatomic,strong) UILabel *carPickTimeLabel;
@property (nonatomic,strong) UILabel *carPeopleLabel;
@property (nonatomic,strong) UILabel *carOnTimeLabel;
@property (nonatomic,strong) UILabel *carHandTimeLabel;

@property (nonatomic,strong) UILabel *locationLabel;
//@property (nonatomic,strong) UIButton *detailButton;

@property (nonatomic,strong) NewBtn *carPickBtn;
@property (nonatomic,strong) NewBtn *carOnBtn;
@property (nonatomic,strong) NewBtn *carHandBtn;



@end
