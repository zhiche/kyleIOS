//
//  ListTableViewCell.h
//  动态布局
//
//  Created by LeeBruce on 16/9/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@interface ListTableViewCell : UITableViewCell

@property (nonatomic,strong) UILabel *titleL;
@property (nonatomic,strong) UILabel *dateLabel;
@property (nonatomic,strong) UILabel *timeLabel;
@property (nonatomic,strong) Model *model;

+(CGFloat)cellHeightWithModel:(Model *)model;

@end
