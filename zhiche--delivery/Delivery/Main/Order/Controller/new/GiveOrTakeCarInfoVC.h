//
//  GiveOrTakeCarInfoVC.h
//  动态布局
//
//  Created by LeeBruce on 16/9/1.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GiveOrTakeCarInfoVC : UIViewController

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,copy) NSString *titleString;
@property (nonatomic,copy) NSString *orderId;

@property (nonatomic) NSInteger number;

@end
