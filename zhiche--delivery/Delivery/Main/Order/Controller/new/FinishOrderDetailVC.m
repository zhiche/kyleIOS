//
//  FinishOrderDetailVC.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/9/9.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "FinishOrderDetailVC.h"
#import "TopBackView.h"

#import "TrackViewController.h"//物流跟踪
#import "DetailVC.h"//交车／提车信息

@interface FinishOrderDetailVC ()
@property (nonatomic,strong) UIScrollView *scrollView;

@end

@implementation FinishOrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@""];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = WhiteColor;
    
    
    NSArray *arr = @[@"物流信息",
                     @"交车信息",
                     @"提车信息"];
    
    for (int i = 0;  i < arr.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:arr[i] forState:UIControlStateNormal];
        button.titleLabel.font = Font(14);
        button.frame = CGRectMake(i * (screenWidth - 100)/3.0 + 50, 27, (screenWidth - 100)/3.0, 30);
        [button setTitleColor:littleBlackColor forState:UIControlStateNormal];
        button.layer.borderWidth = 0.5;
        button.layer.borderColor = WhiteColor.CGColor;
        
        
        [topBackView addSubview:button];
        button.tag = 400 + i;
        [button addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    }

    
    [self initButton];
}

-(void)initButton{
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight)];
    [self.view addSubview:self.scrollView];
    
}

-(void)selectAction:(UIButton *)sender
{
    for (UIView *view in self.scrollView.subviews) {
        [view removeFromSuperview];
    }
    
    [sender setTitleColor:RedColor forState:UIControlStateNormal];

    
    if (sender.tag == 400) {
        TrackViewController *trackVC = [[TrackViewController alloc]init];
        trackVC.view.frame = CGRectMake(0, -64, screenWidth, screenHeight);
        [self.scrollView addSubview:trackVC.view];
        
    } else if(sender.tag == 401){
        
        DetailVC *detailVC = [[DetailVC alloc]init];
        [self addChildViewController:detailVC];

        detailVC.view.frame = CGRectMake(0, -64, screenWidth , screenHeight);
        detailVC.titleString = @"交车信息";
        [self.scrollView addSubview:detailVC.view];
        
    } else {
    
        DetailVC *detailVC = [[DetailVC alloc]init];
        [self addChildViewController:detailVC];
        detailVC.view.frame = CGRectMake(0, -64, screenWidth , screenHeight);
        detailVC.titleString = @"提车信息";

        [self.scrollView addSubview:detailVC.view];
    }
}


-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
