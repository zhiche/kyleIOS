//
//  OtherDetailVC.m
//  动态布局
//
//  Created by LeeBruce on 16/9/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "OtherDetailVC.h"
#import "RootViewController.h"
#import "Common.h"
#import "Model.h"
#import "CarInfoCell.h"
#import "WKProgressHUD.h"



#define labelHeight 32 * kHeight

@interface OtherDetailVC ()
@property (nonatomic,strong) UILabel *totalL;//总运价
@property (nonatomic,strong) UILabel *scoreL;//积分抵用
@property (nonatomic,strong) UILabel *payL;//实付费用
@property (nonatomic,strong) UILabel *saleL;//折扣价

@property (nonatomic,strong) UILabel *eventL;//活动信息
@property (nonatomic,strong) WKProgressHUD *hud;

@property (nonatomic,strong) UILabel *cityLabel;//同城使用


@end

@implementation OtherDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self initSecondView];
    [self initDataSource];
    
    [self.rotateButton addTarget:self action:@selector(rotateButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    

}

//按钮旋转
-(void)rotateButtonAction:(UIButton *)sender
{
    if (self.flag) {
        //合并
        [UIView animateWithDuration:0.2 animations:^{
            self.arrowImageView.transform = CGAffineTransformMakeRotation(0);
            
        } completion:^(BOOL finished) {
            self.flag = NO;
            
            [self changeViewFrameWithInteger:0];
            
        }];
    } else {
        //展开
        [UIView animateWithDuration:0.2 animations:^{
            self.arrowImageView.transform = CGAffineTransformMakeRotation(M_PI_2);
            
        } completion:^(BOOL finished) {
            self.flag = YES;
            
            [self changeViewFrameWithInteger:1];
        }];
    }
    
    
}


-(void)initDataSource
{
    //假数据
//    NSDictionary *dic = [NSDictionary dictionary];
//    dic =  [Common txtChangToJsonWithString:@"dingdan"];
//    
//    if ([[dic[@"data"] allKeys] count] > 0) {
//        
//        self.dataDictionary = [NSMutableDictionary dictionaryWithDictionary:dic[@"data"]];
//        
//        
//        [self getValueString];
//    }
    
    
    
    //接口数据
    
     NSString *urlString = nil;
     urlString = [NSString stringWithFormat:@"%@/%ld",order_detail,[self.orderId integerValue]];
     
     [Common requestWithUrlString:urlString contentType:application_json finished:^(id responseObj) {
         
          
     [self.hud dismiss:YES];
     
     if ([responseObj[@"success"] boolValue]) {
     self.dataDictionary = [NSMutableDictionary dictionaryWithDictionary:responseObj[@"data"]];
     
        [self getValueString];
         
     } else {
     
     
     [WKProgressHUD popMessage:[NSString stringWithFormat:@"%@",responseObj[@"message"]] inView:self.view duration:1.5 animated:YES];
     
     }
     
     
     } failed:^(NSString *errorMsg) {
     [self.hud dismiss:YES];
     
     NSLog(@"%@",errorMsg);
     }];
}

//赋值
-(void)getValueString
{
    
    if ([self.dataDictionary[@"svehicles"] isEqual:[NSNull null]]) {
        
    } else {
        
        for (int i = 0; i < [self.dataDictionary[@"svehicles"] count]; i ++) {
            Model *model = [[Model alloc]init];
            [model setValuesForKeysWithDictionary:self.dataDictionary[@"svehicles"][i]];
            
            [self.dataArr addObject:model];
        }
        
        
        //判断是否同城
        if ([self.dataDictionary[@"isVeneer"] integerValue] == 1) {
            //同城
            self.footHeight = cellHeight;
            
            self.tableView.frame = CGRectMake(0, 116 * kHeight, screenWidth, cellHeight * 2 );

            
        } else {
            //城际
            
            self.footHeight = cellHeight * 2;
            
            self.tableView.frame = CGRectMake(0, 116 * kHeight, screenWidth, cellHeight * 3 );
        }
        [self.tableView reloadData];
    }
    
   
//    self.endAddress.text = [self backString: model.dest];

    //订单编号
    self.numberLabel.text = [self backString:self.dataDictionary[@"orderCode"]];
    NSString * createTime = [NSString stringWithFormat:@"下单时间：%@",[self backString:self.dataDictionary[@"createTime"]]];
    //创建时间
    self.createTime.text = createTime;

    //起始地
//    self.startLabel.text = [self backString:self.dataDictionary[@"departCityName"]];
    self.startLabel.text = [self backString:self.dataDictionary[@"orgin"]];

    //目的地
//    self.endLabel.text = [self backString:self.dataDictionary[@"receiptCityName"]];
    self.endLabel.text = [self backString:self.dataDictionary[@"dest"]];

    //出发时间
    self.startDate.text = [self backString:self.dataDictionary[@"deliveryDate"]];
    //结束时间
    self.endDateLabel.text = [self backString:self.dataDictionary[@"arriveDate"]];
    
    //汽车状态
    //汽车状态
    NSString *string ;
    
    
    if ([self.dataDictionary[@"isSencondhand"] boolValue]) {
        string = @"二手车";
        
    } else {
        string = @"新车";
        
    }
    
    if ([self.dataDictionary[@"isMobile"] boolValue]) {
        
        string = [string stringByAppendingString:@"   能动"];
        
    } else {
        string = [string stringByAppendingString:@"   不能动"];
        
    }

    
    self.carStautsLabel.text = string;
    //车辆总数
    int number = 0;
    
    if ([self.dataDictionary[@"svehicles"] isEqual:[NSNull null]]) {
        
    } else {
    
    for (int i = 0; i < [self.dataDictionary[@"svehicles"] count]; i ++) {
        
        number += [self.dataDictionary[@"svehicles"][i][@"amount"] intValue];
        
    }
    }
    
    self.amountLabel.text = [NSString stringWithFormat:@"%d辆",number];
    
    if ([self.dataDictionary[@"feeDetail"] count] > 0) {
        
        self.feeTableView.frame = CGRectMake(0, CGRectGetMaxY(self.tableView.frame) + 10, screenWidth, cellHeight * ([self.dataDictionary[@"feeDetail"] count] + 1));
        
        [self.feeTableView reloadData];
        
        [self initSecondView];
        
//        [self initThirdView];
        
    }

    
    /*
    
    for (int i = 0; i < [self.dataDictionary[@"feeDetail"] count]; i ++) {
        if ([self.dataDictionary[@"feeDetail"][i][@"feeName"] isEqualToString:@"提车费"]) {
            //提车费
            self.totalLabel.text = [self backMoney:self.dataDictionary[@"feeDetail"][i][@"cost"]];
        }
        
        if ([self.dataDictionary[@"feeDetail"][i][@"feeName"] isEqualToString:@"交车费"]) {
            //交车费
            self.payLabel.text = [self backMoney:self.dataDictionary[@"feeDetail"][i][@"cost"]];
            
        }
        
        if ([self.dataDictionary[@"feeDetail"][i][@"feeName"] isEqualToString:@"保险费"]) {
            //保险费
            self.unpayLabel.text = [self backMoney:self.dataDictionary[@"feeDetail"][i][@"cost"]];
        }
        
        if ([self.dataDictionary[@"feeDetail"][i][@"feeName"] isEqualToString:@"运输费"]) {
            //运输费
            self.scoreLabel.text = [self backMoney:self.dataDictionary[@"feeDetail"][i][@"cost"]];
        }
    }
     
     */
    
    
    //总运价
    self.totalL.text = [self backMoney:self.dataDictionary[@"orderCost"]];
    //折扣价
    self.saleL.text = [self backMoney:self.dataDictionary[@"discount"][@"cost"]];
    
    self.cityLabel.text = [self backMoney:self.dataDictionary[@"orderCost"]];
    
    
    if ([self.dataDictionary[@"discount"][@"discount"] boolValue]) {
    
        self.totalL.textColor = fontGrayColor;
        
        //添加删除线
        NSMutableAttributedString *newPrice = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",self.totalL.text]];
        [newPrice addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:NSMakeRange(0, newPrice.length)];
        self.totalL.attributedText = newPrice;
        
    }
    
    

    
    
    
    //积分抵用
    
        /*
    if ([self.dataDictionary[@"scores"][@"amount"] isEqual:[NSNull null]]) {
        
        self.scoreL.text = @"";


    } else {
        
        self.scoreL.text = [self backMoney:self.dataDictionary[@"scores"][@"amount"]];

    }
    
    //实付费用
    NSString *priceS = @"实付费用 ";
    
    self.payL.text = [priceS stringByAppendingString:[self backMoney:self.dataDictionary[@"actualCost"]]];
    //活动
//    self.eventL.text = [self backString:self.dataDictionary[@"discountdesc"]];
    
*/
    
}


-(NSString *)backString:(NSString *)string
{
    
    if ([string isEqual:[NSNull null]]) {
        return @"";
    } else {
        return string;
    }
}

-(NSString *)backMoney:(NSString *)string
{
    NSString   *str = [NSString stringWithFormat:@"%.2f",[string floatValue]];
    
    if ([str isEqual:[NSNull null]]) {
        return @"";
    } else {
        return [NSString stringWithFormat:@"￥%@",str];
    }
    
    
}


//根据table的展开来改变页面布局
-(void)changeViewFrameWithInteger:(NSInteger)integer
{
    
    //1、计算每个cell的高度 存储到self.heightArray中
    for (int i = 0;  i < self.dataArr.count; i++) {
        
//        Model *model = [[Model alloc]init];
//        model = self.dataArr[i];
//        
//        NSString *string = model.vinList[0][@"vin"];
//        for (int i = 1; i < model.vinList.count ; i ++) {
//            
//            string = [string stringByAppendingString:[NSString stringWithFormat:@"\n%@ ",model.vinList[i][@"vin"]]];
//            
//        }
//        
//        model.contact = string;
//        
//        CGFloat height = [CarInfoCell cellHeightWithModel:model];
        
        [self.heightArray addObject:@(56 * kHeight )];
    }
    
    
    [self.tableView reloadData];
    
    //2、cell展开时候计算总高度  关闭时候高度为0
    CGFloat height = 0;
    
    if (integer == 0) {
        
        [self.heightArray removeAllObjects];
        
    }
    
    for (int j = 0; j < self.heightArray.count; j++) {
        
        height += [self.heightArray[j] floatValue];
        
    }
    
    //3、改变view的frame
    
    if (self.footHeight > cellHeight) {
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, screenWidth, cellHeight * 3 + height);

    } else {
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, screenWidth, cellHeight * 2 + height);

    }
    
    
    self.feeTableView.frame = CGRectMake(0, CGRectGetMaxY(self.tableView.frame) + 10, screenWidth, self.feeTableView.frame.size.height);
    
    UIView *view0 = [self.view viewWithTag:700];
    view0.frame = CGRectMake(0, CGRectGetMaxY(self.feeTableView.frame) + 10 , screenWidth, view0.frame.size.height);
    
    
    UIView *view1 = [self.view viewWithTag:800];
    view1.frame = CGRectMake(0, CGRectGetMaxY(self.feeTableView.frame)  , screenWidth, view1.frame.size.height);
    
//    UIView *view = [self.view viewWithTag:701];
//    view.frame = CGRectMake(0, CGRectGetMaxY(view0.frame) + 10, screenWidth, view.frame.size.height);
//    
//    UIButton *button = [self.view viewWithTag:702];
//    button.frame = CGRectMake(button.frame.origin.x, CGRectGetMaxY(view.frame) + 25, button.frame.size.width, button.frame.size.height);
    
    if (view0) {
        self.scrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(view0.frame) + 20 + 64);

    } else {
        
        CGFloat height;
        
        if (CGRectGetMaxY(view1.frame) + 64 > screenHeight) {
            
            height = CGRectGetMaxY(view1.frame) + 64;
        } else {
            height = screenHeight + 1;
        }
    
    self.scrollView.contentSize = CGSizeMake(screenWidth,  height);
    }
}



-(void)initSecondView
{
    
    
    if (self.footHeight > cellHeight) {
        //城际
    
        if ([self.dataDictionary[@"discount"][@"discount"] boolValue]) {
            
            //有折扣的时候
            [self useScoreView];
            
        } else {
            
            //没有折扣的时候
            [self noUserScoreView];
        }

    } else {
        //同城
        [self initThirdView];
    }
    
}

-(void)initThirdView
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.feeTableView.frame), screenWidth, cellHeight)];
    view.backgroundColor = WhiteColor;
    view.tag = 800;
    
    UILabel *lineL = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 0.5)];
    lineL.backgroundColor = LineGrayColor;
    [view addSubview:lineL];
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth/2 - 18, 0, screenWidth/4, cellHeight)];
    label1.text = @"合计费用:";
    label1.textAlignment = NSTextAlignmentRight;
    label1.font = Font(13);
    label1.textColor = AddCarColor;
    self.cityLabel = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth/4 * 3 - 18, 0, screenWidth/4, cellHeight)];
    self.cityLabel.textColor = YellowColor;
    self.cityLabel.font = Font(13);
    self.cityLabel.textAlignment = NSTextAlignmentRight;
    [view addSubview:label1];
    [view addSubview:self.cityLabel];
    
    [self.scrollView addSubview:view];
    
    self.scrollView.contentSize = CGSizeMake(screenWidth, screenHeight + 1);
}



-(void)useScoreView
{

     UIView *secondView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.feeTableView.frame)  + 10,screenWidth , cellHeight * 2)];
     secondView.backgroundColor = [UIColor whiteColor];
     [self.scrollView addSubview:secondView];
     secondView.tag = 700;
     
     //总运价
     UILabel *totalL = [self backLabelWithFrame:CGRectMake(18, 0, 100, cellHeight) andString:@"总运价"];
     [secondView addSubview:totalL];
     
     self.totalL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 100 - 18, 0, 100, cellHeight)];
     self.totalL.font = Font(11);
     self.totalL.textColor = AddCarColor;
     //    self.totalL.text = @"￥1234.00";
     self.totalL.textAlignment = NSTextAlignmentRight;
     [secondView addSubview:self.totalL];
     
     UILabel *labelF = [[UILabel alloc]initWithFrame:CGRectMake(18, CGRectGetMaxY(self.totalL.frame), screenWidth - 18, 0.5)];
     labelF.backgroundColor = LineGrayColor;
     [secondView addSubview:labelF];
     
     //折扣价
     UILabel *saleL = [self backLabelWithFrame:CGRectMake(18, cellHeight , 100, cellHeight) andString:@"折扣价"];
     [secondView addSubview:saleL];
     
     self.saleL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 100 - 18, cellHeight , 100, cellHeight)];
     self.saleL.font = Font(13);
     self.saleL.textColor = YellowColor;
     //    self.saleL.text = @"￥1200.00";
     self.saleL.textAlignment = NSTextAlignmentRight;
     
     [secondView addSubview:self.saleL];
     
    
    /*
     UILabel *labelS = [[UILabel alloc]initWithFrame:CGRectMake(18, CGRectGetMaxY(self.saleL.frame), screenWidth - 18, 0.5)];
     labelS.backgroundColor = LineGrayColor;
     [secondView addSubview:labelS];
     
     
     //优惠
     //    self.eventL = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight , screenWidth - 18, 10)];
     //    self.eventL.textAlignment = NSTextAlignmentRight;
     ////    self.eventL.text = @"优惠:活动期间价格8折，8月1日－12月31日";
     //    self.eventL.textColor = fontGrayColor;
     //    self.eventL.font = Font(10);
     //    [secondView addSubview:self.eventL];
     //积分抵用
     UILabel *scoreL = [self backLabelWithFrame:CGRectMake(18, labelHeight * 2, 100, labelHeight) andString:@"积分抵用"];
     [secondView addSubview:scoreL];
     
     self.scoreL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 100 - 18, labelHeight * 2, 100, labelHeight)];
     self.scoreL.font = Font(13);
     self.scoreL.textColor = AddCarColor;
     //    self.scoreL.text = @"-￥10.00";
     self.scoreL.textAlignment = NSTextAlignmentRight;
     
     [secondView addSubview:self.scoreL];
     
     UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, labelHeight * 3 - 0.5, screenWidth - 18, 0.5)];
     label.backgroundColor = LineGrayColor;
     [secondView addSubview:label];
     
     //实付费用
     UILabel *payL = [self backLabelWithFrame:CGRectMake(18,labelHeight * 3, 100, cellHeight) andString:@"实付费用"];
     //    [secondView addSubview:payL];
     
     self.payL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 250 - 18,labelHeight * 3, 250, cellHeight )];
     self.payL.font = Font(15);
     self.payL.textColor = YellowColor;
     //    self.payL.text = @"￥1190.00";
     self.payL.textAlignment = NSTextAlignmentRight;
     
     [secondView addSubview:self.payL];
     */
     
     self.scrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(secondView.frame)  + 64);
     
}

-(void)noUserScoreView
{
    
     UIView *secondView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.feeTableView.frame)  + 10,screenWidth , cellHeight + labelHeight * 2)];
     secondView.backgroundColor = [UIColor whiteColor];
     [self.scrollView addSubview:secondView];
     secondView.tag = 700;
     
     //总运价
     UILabel *totalL = [self backLabelWithFrame:CGRectMake(18, 0, 100, labelHeight) andString:@"总运价"];
     [secondView addSubview:totalL];
     
     self.totalL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 100 - 18, 0, 100, labelHeight)];
     self.totalL.font = Font(11);
     self.totalL.textColor = AddCarColor;
     //    self.totalL.text = @"￥1234.00";
     self.totalL.textAlignment = NSTextAlignmentRight;
     [secondView addSubview:self.totalL];
     
     UILabel *labelF = [[UILabel alloc]initWithFrame:CGRectMake(18, CGRectGetMaxY(self.totalL.frame), screenWidth - 18, 0.5)];
     labelF.backgroundColor = LineGrayColor;
     [secondView addSubview:labelF];
     
     //折扣价
     UILabel *saleL = [self backLabelWithFrame:CGRectMake(18, labelHeight , 100, labelHeight) andString:@"折扣价"];
//     [secondView addSubview:saleL];
    
     self.saleL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 100 - 18, labelHeight , 100, labelHeight)];
     self.saleL.font = Font(13);
     self.saleL.textColor = YellowColor;
     //    self.saleL.text = @"￥1200.00";
     self.saleL.textAlignment = NSTextAlignmentRight;
     
//     [secondView addSubview:self.saleL];
    
     
     UILabel *labelS = [[UILabel alloc]initWithFrame:CGRectMake(18, CGRectGetMaxY(self.saleL.frame), screenWidth - 18, 0.5)];
     labelS.backgroundColor = LineGrayColor;
     [secondView addSubview:labelS];
     
     
     //优惠
     //    self.eventL = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight , screenWidth - 18, 10)];
     //    self.eventL.textAlignment = NSTextAlignmentRight;
     ////    self.eventL.text = @"优惠:活动期间价格8折，8月1日－12月31日";
     //    self.eventL.textColor = fontGrayColor;
     //    self.eventL.font = Font(10);
     //    [secondView addSubview:self.eventL];
    
    /*
     //积分抵用
     UILabel *scoreL = [self backLabelWithFrame:CGRectMake(18, labelHeight , 100, labelHeight) andString:@"积分抵用"];
     [secondView addSubview:scoreL];
     
     self.scoreL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 100 - 18, labelHeight , 100, labelHeight)];
     self.scoreL.font = Font(13);
     self.scoreL.textColor = AddCarColor;
     //    self.scoreL.text = @"-￥10.00";
     self.scoreL.textAlignment = NSTextAlignmentRight;
     
     [secondView addSubview:self.scoreL];
     
     UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, labelHeight * 2 - 0.5, screenWidth - 18, 0.5)];
     label.backgroundColor = LineGrayColor;
     [secondView addSubview:label];
     
     //实付费用
     UILabel *payL = [self backLabelWithFrame:CGRectMake(18,labelHeight * 2, 100, cellHeight) andString:@"实付费用"];
     //    [secondView addSubview:payL];
     
     self.payL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 250 - 18,labelHeight * 2, 250, cellHeight )];
     self.payL.font = Font(15);
     self.payL.textColor = YellowColor;
     //    self.payL.text = @"￥1190.00";
     self.payL.textAlignment = NSTextAlignmentRight;
     
     [secondView addSubview:self.payL];
     */

     self.scrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(secondView.frame) + 20 + 64);
    
}




-(UILabel *)backLabelWithFrame:(CGRect)frame andString:(NSString *)string
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.text = string;
    label.textColor = fontGrayColor;
    label.font = Font(13);
    
    return label;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
