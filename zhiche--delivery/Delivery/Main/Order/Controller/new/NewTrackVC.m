//
//  NewTrackVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 17/9/5.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#define leftMargn 18
#define labelHeight 15 * kHeight
#define labelWidth 60 * kWidth

#import "MineOrderModel.h"
#import "GiveOrTakeCarInfoVC.h"
#import "NewTrackVC.h"
#import "TopBackView.h"
#import "NewBtn.h"
#import "DetailVC.h"
//#import "TrackingCell.h"
#import "TrackViewController.h"
#import "RootViewController.h"
#import "Common.h"
#import "NewTrackingCell.h"
@interface NewTrackVC ()<UITableViewDelegate,UITableViewDataSource>


@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UIScrollView *scrollView;

@property (nonatomic,strong) UILabel *numberLabel;//订单编号
@property (nonatomic,strong) UILabel *statusLabel;//状态
@property (nonatomic,strong) UILabel *startLabel;//起始地
//@property (nonatomic,strong) UILabel *startDetailLabel;//详细地址
@property (nonatomic,strong) UILabel *startDate;//起始日期
@property (nonatomic,strong) UILabel *endLabel;//终止地址
//@property (nonatomic,strong) UILabel *endDetailLabel;//详细地址
@property (nonatomic,strong) UILabel *endDateLabel;//终止日期

@property (nonatomic,strong) NSMutableDictionary *dataDictionary;

@property (nonatomic,strong) NSMutableDictionary *imageDictionary;


@end

@implementation NewTrackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataDictionary = [NSMutableDictionary dictionary];
    self.imageDictionary = [NSMutableDictionary dictionary];
    
    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"物流跟踪"];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    self.view.backgroundColor = GrayColor;
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    
    [self initTopView];
    [self initTableView];
    [self initDataSources];
}

-(void)initDataSources
{
    //假数据
    //    NSDictionary *dic = [NSDictionary dictionary];
    //    dic =  [Common txtChangToJsonWithString:@"wuliugenzong"];
    //
    //
    //    if ([[dic[@"data"] allKeys] count] > 0) {
    //
    //        self.dataDictionary = [NSMutableDictionary dictionaryWithDictionary:dic[@"data"]];
    //
    //        [self getValueString];
    //        [self.tableView reloadData];
    //    }
    
    //网路数据
    
    NSString *urlString = [NSString stringWithFormat:@"%@?id=%@",order_sorder,self.orderId];
    
    [Common requestWithUrlString:urlString contentType:application_json finished:^(id responseObj){
        
        
        if ([responseObj[@"data"] isEqual:[NSNull null]]) {
            
        } else {
            
            self.dataDictionary = [NSMutableDictionary dictionaryWithDictionary:responseObj[@"data"]];
            
            self.tableView.frame = CGRectMake(0, 106 * kHeight + 10, screenWidth,cellHeight - 10 * kHeight + 165 * kHeight * [self.dataDictionary[@"sorders"] count]);
            
            if (CGRectGetMaxY(self.tableView.frame) + 106  * kHeight > screenHeight) {
                
                self.scrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(self.tableView.frame) + 10 + 64);
                
            } else {
                self.scrollView.contentSize = CGSizeMake(screenWidth, screenHeight + 1);
                
            }

            
            
            [self getValueString];
            [self.tableView reloadData];
        }
        
        
    
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
    
}

-(void)getValueString
{
    
    //订单号
    self.numberLabel.text = [NSString stringWithFormat:@"%@",_dataDictionary[@"orderCode"]];
    //开始日期
    self.startDate.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"deliveryDate"]];
    
    //结束日期
    self.endDateLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"arriveDate"]];
    
    //起点
    self.startLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"orgin"]];
    //终点
    self.endLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"dest"]];
    
    
}


-(void)initTopView
{
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight)];
    self.scrollView.contentSize = CGSizeMake(screenWidth, screenHeight * 1.5);
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = GrayColor;
    
    
    /**
     
     订单信息
     */
    UIView *orderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 106 * kHeight)];
    [self.scrollView addSubview:orderView];
    orderView.backgroundColor = WhiteColor;
    //订单号
    
    UIView *orderV1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight)];
    [orderView addSubview:orderV1];
    orderV1.backgroundColor = headerBottomColor;
    
    
    
    UILabel *orderL = [self backLabelWithFrame:CGRectMake(leftMargn, 0, 60, cellHeight) andString:@"订单号:"];
    orderL.textColor = littleBlackColor;
    [orderV1 addSubview:orderL];
    
    self.numberLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(orderL.frame) + 5, CGRectGetMinY(orderL.frame), 200, cellHeight)];
    //    self.numberLabel.text = @"123122453464576";
    self.numberLabel.textColor = littleBlackColor;
    [orderV1 addSubview:self.numberLabel];
    
    
    //状态栏
    self.statusLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 14 - 100, CGRectGetMinY(orderL.frame), 100, cellHeight)  andString:@"已交车"];
    self.statusLabel.textColor = RedColor;
    
    self.statusLabel.textAlignment = NSTextAlignmentRight;
    //    [orderView addSubview:self.statusLabel];
    
    
    UILabel *labe = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight, screenWidth, 0.5)];
    labe.backgroundColor = LineGrayColor;
    [orderV1 addSubview:labe];
    
    //起始地址
    self.startLabel = [self backLabelWithFrame:CGRectMake(20 * kWidth - 20,  CGRectGetMaxY(orderV1.frame) + 12*kHeight, labelWidth + 40, labelHeight)];
    self.startLabel.font = Font(12)
    //    self.startLabel.text =  @"阿道夫大风大风";
    self.startLabel.adjustsFontSizeToFitWidth = YES;
    [orderView addSubview:self.startLabel];
    
    
    //起始日期
    self.startDate = [self backLabelWithFrame:CGRectMake(20 * kWidth, CGRectGetMaxY(self.startLabel.frame) + 9*kHeight, labelWidth, labelHeight) ];
    //    self.startDate.text = @"2010-1-1";
    self.startDate.textColor = littleBlackColor;
    self.startDate.font = Font(10);
    [orderView addSubview:self.startDate];
    
    
    
    //    //起始详情地址
    //    self.startDetailLabel = [self backLabelWithFrame:CGRectMake(20 * kHeight, CGRectGetMaxY(self.startLabel.frame) , labelWidth, 28 * kHeight)];
    //    self.startDetailLabel.text = @"";
    //    self.startDetailLabel.adjustsFontSizeToFitWidth = YES;
    //    [orderView addSubview:self.startDetailLabel];
    //    self.startDetailLabel.font = Font(9);
    //    self.startDetailLabel.numberOfLines = 0;
    
    
    //图片
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMidX(self.view.frame) - 30, CGRectGetMinY(self.startLabel.frame) + 4.5 * kHeight + 6, 47, 12.5)];
    [orderView addSubview:imageView];
    imageView.image = [UIImage imageNamed:@"Quote_Cell_logo"];
    
    
    //目的地
    self.endLabel = [self backLabelWithFrame:CGRectMake(screenWidth - labelWidth  - 20 * kWidth - 20 * kHeight, CGRectGetMaxY(labe.frame) + 12*kHeight, labelWidth + 40, labelHeight) ];
    [orderView addSubview:self.endLabel];
    self.endLabel.adjustsFontSizeToFitWidth = YES;
    self.endLabel.font = Font(12);
    
    //目的日期
    self.endDateLabel = [self backLabelWithFrame:CGRectMake(screenWidth - labelWidth  - 20 * kWidth, CGRectGetMaxY(self.endLabel.frame) + 9*kHeight, labelWidth, labelHeight) ];
    [orderView addSubview:self.endDateLabel];
    self.endDateLabel.font = Font(10);
    
    
}

-(void)initTableView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 106 * kHeight + 10, screenWidth, screenHeight - 64) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]init];
    self.tableView.scrollEnabled = NO;
    
    [self.scrollView addSubview:self.tableView];
    
    self.scrollView.contentSize = CGSizeMake(screenWidth, screenHeight * 1.2);
    
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = GrayColor;
    
    if (section == 0) {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, 100, cellHeight)];
        label.text = @"车辆信息";
        label.textColor = littleBlackColor;
        label.font = Font(14);
        [view addSubview:label];
        
        UILabel *lineL = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight - 0.5, screenWidth, 0.5)];
        lineL.backgroundColor = LineGrayColor;
        //        [view addSubview:lineL];
        
        view.backgroundColor = WhiteColor;
        
    }
    
    return view;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([self.dataDictionary[@"sorders"] count] > 0) {
        return [_dataDictionary[@"sorders"] count];
    } else {
        return 0;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 1;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return (section == 0)? cellHeight : 10;
}




-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str = @"giveOrTakeCell";
    NewTrackingCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    if (!cell) {
        cell = [[NewTrackingCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:str];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
//    [cell.detailButton addTarget:self action:@selector(detailButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//    cell.detailButton.tag = indexPath.section + 600;
    
    if ([[self.dataDictionary allKeys] count] > 0) {
        
        cell.carStyleLabel.text = self.dataDictionary[@"sorders"][indexPath.section][@"name"];
        cell.vinLabel.text = self.dataDictionary[@"sorders"][indexPath.section][@"vin"];
        cell.carPickTimeLabel.text =[self backString:self.dataDictionary[@"sorders"][indexPath.section][@"deliveryDate"]];
        cell.carOnTimeLabel.text = [self backString:_dataDictionary[@"sorders"][indexPath.section][@"onOrderDate"]];
        cell.carHandTimeLabel.text = [self backString: self.dataDictionary[@"sorders"][indexPath.section][@"arriveDate"]];
        cell.statusLabel.text = [self backString:self.dataDictionary[@"sorders"][indexPath.section][@"statustext"]];
        
        cell.locationLabel.text =  [self backString:self.dataDictionary[@"sorders"][indexPath.section][@"location"]];
        cell.carPeopleLabel.text = [self backString:self.dataDictionary[@"sorders"][indexPath.section][@"deliveryName"]];
//
        
        
        
        [cell.carPickBtn addTarget:self action:@selector(pressPickBtn:) forControlEvents:UIControlEventTouchUpInside];
        [cell.carOnBtn addTarget:self action:@selector(pressOnBtn:) forControlEvents:UIControlEventTouchUpInside];
        [cell.carHandBtn addTarget:self action:@selector(pressHandBtn:) forControlEvents:UIControlEventTouchUpInside];

        
        
        
    }
    
    return cell;
}



-(void)pressPickBtn:(NewBtn*)sender{
    
//    GiveOrTakeCarInfoVC *takeVC = [[GiveOrTakeCarInfoVC alloc]init];
//    
//    
//    takeVC.titleString = @"提车信息";
//    takeVC.orderId = self.dataDictionary[@"sorders"][sender.indexpath.section][@"deliveryWaybillId"];
//    
//    [self.navigationController pushViewController:takeVC animated:YES];
//

    DetailVC *vc = [[DetailVC alloc]init];
    vc.titleString = @"提车照片";
    
    vc.orderId = self.dataDictionary[@"sorders"][sender.indexpath.section][@"deliveryWaybillId"];
    
    [self.navigationController pushViewController:vc animated:YES];

    
    
}

-(void)pressOnBtn:(NewBtn*)sender{
    
    TrackViewController *trackVC = [[TrackViewController alloc]init];
    
    trackVC.numberString = self.dataDictionary[@"orderCode"];
    trackVC.orderId = _dataDictionary[@"sorders"][sender.indexpath.section][@"id"];
    
    NSLog(@"%@",trackVC.orderId);
    [self.navigationController pushViewController:trackVC animated:YES];

    
    
}


-(void)pressHandBtn:(NewBtn*)sender{
    
//    GiveOrTakeCarInfoVC *giveVC = [[GiveOrTakeCarInfoVC alloc]init];
//    
//    
//    giveVC.orderId = self.dataDictionary[@"sorders"][sender.indexpath.section][@"arriveWaybillId"];
//    giveVC.titleString = @"交车信息";
//    
////    giveVC.number = self.number;
//    
//    [self.navigationController pushViewController:giveVC animated:YES];

    DetailVC *vc = [[DetailVC alloc]init];
    vc.titleString = @"交车信息";
    
    vc.orderId = self.dataDictionary[@"sorders"][sender.indexpath.section][@"arriveWaybillId"];
    
    [self.navigationController pushViewController:vc animated:YES];
    

    
}





-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 165 * kHeight;
}



-(void)detailButtonAction:(UIButton *)sender
{
    
    
    TrackViewController *trackVC = [[TrackViewController alloc]init];
    
    trackVC.numberString = self.dataDictionary[@"orderCode"];
    trackVC.orderId = self.dataDictionary[@"sorders"][sender.tag - 600][@"id"];
    
    [self.navigationController pushViewController:trackVC animated:YES];
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
}

-(UILabel *)backLabelWithFrame:(CGRect)frame andString:(NSString *)string
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.text = string;
    label.textColor = fontGrayColor;
    label.font = Font(13);
    return label;
    
}

-(UILabel *)backLabelWithFrame:(CGRect)frame
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.textColor = littleBlackColor;
    label.font = Font(12);
    label.textAlignment = NSTextAlignmentCenter;
    
    return label;
    
    
}

-(NSString *)backString:(NSString *)string
{
    //    NSString * str = [NSString stringWithFormat:@"%@",string];
    if ([string isEqual:[NSNull null]]) {
        return @"";
    } else {
        return string;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
