//
//  Order_DetailViewController.h
//  动态布局
//
//  Created by LeeBruce on 16/9/5.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Order_DetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UIScrollView *scrollView;

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UITableView *feeTableView;;

@property (nonatomic,strong) NSMutableDictionary *imageDictionary;
@property (nonatomic,copy) NSString *orderId;
@property (nonatomic) NSInteger integer;

@property (nonatomic) CGFloat footHeight;




@property (nonatomic,strong) NSString * orderTitleString;
/**
 订单
 */
@property (nonatomic,strong) UILabel *numberLabel;//订单编号
@property (nonatomic,strong) UILabel *createTime;//创建时间
@property (nonatomic,strong) UILabel *statusLabel;//状态
@property (nonatomic,strong) UILabel *startLabel;//起始地
//@property (nonatomic,strong) UILabel *startDetailLabel;//详细地址
@property (nonatomic,strong) UILabel *startDate;//起始日期
@property (nonatomic,strong) UILabel *endLabel;//终止地址
//@property (nonatomic,strong) UILabel *endDetailLabel;//详细地址
@property (nonatomic,strong) UILabel *endDateLabel;//终止日期
@property (nonatomic,strong) UILabel *carLabel;

/**
 承运人
 */
//@property (nonatomic,strong) UILabel *distanceLabel;//行程
//@property (nonatomic,strong) UILabel *nameLabel;//姓名
//@property (nonatomic,strong) UILabel *carNumberLabel;//车牌
//@property (nonatomic,strong) UILabel *phoneLabel;//电话
//@property (nonatomic,strong) UILabel *carStyleLabel;//车型

/**
汽车信息
 */

@property (nonatomic,strong) UILabel *carStautsLabel;//汽车状态
@property (nonatomic,strong) UILabel *amountLabel;//汽车总数

/**
 运费
 */

@property (nonatomic,strong) UIView *priceView;


@property (nonatomic,strong) UILabel *totalLabel;//提车费
@property (nonatomic,strong) UILabel *scoreLabel;//运输费
@property (nonatomic,strong) UILabel *payLabel;//交车费
@property (nonatomic,strong) UILabel *unpayLabel;//保险费
@property (nonatomic,strong) UILabel *pingLabel;//平台服务费

@property (nonatomic,strong) UIView *lastPayView;
@property (nonatomic,strong) UILabel *lastPayL;//支付余额

@property (nonatomic,strong) UIButton *rotateButton;//旋转button
@property (nonatomic,strong) UIImageView *arrowImageView;

@property (nonatomic) BOOL flag;//标示table是否展开

@property (nonatomic,strong) NSMutableArray *heightArray;//存储cell的高度
@property (nonatomic,strong) NSMutableDictionary *dataDictionary;//数据源
@property (nonatomic,strong) NSMutableArray *dataArr;//车辆信息


@end
