//
//  TrackViewController.m
//  动态布局
//
//  Created by LeeBruce on 16/9/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "TrackViewController.h"
#import "ListTableViewCell.h"
#import "Model.h"
#import "TopBackView.h"
#import "Common.h"
#import "NullView.h"


@interface TrackViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *heightArray;
@property (nonatomic,strong) UIView *leftView;
@property (nonatomic,strong) UIImageView *imageV;

@property (nonatomic,strong) NSMutableDictionary *dataDictionary;
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NullView *nullView;
@property (nonatomic,strong) NSString * bidderType;


@end

@implementation TrackViewController

-(void)initTopView
{
    self.view.backgroundColor = GrayColor;

    UIView *firstView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, cellHeight * 2)];
    firstView.backgroundColor = WhiteColor;
    
    UILabel *orderL = [self backLabelWithFrame:CGRectMake(18, 0, 50, cellHeight) andFont:11 andColor:littleBlackColor];
    orderL.text = @"订单号:";
    [firstView addSubview:orderL];
    
    UILabel *orderLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(orderL.frame), 0, 300, cellHeight) andFont:11 andColor:littleBlackColor];
    orderLabel.text = self.numberString;
    [firstView addSubview:orderLabel];
    
    UILabel *line= [[UILabel alloc]initWithFrame:CGRectMake(18, cellHeight - 0.5, screenWidth - 18, 0.5)];
    line.backgroundColor = LineGrayColor;
    [firstView addSubview:line];
    
    UIButton *callButton = [UIButton buttonWithType:UIButtonTypeCustom];
    callButton.frame = CGRectMake(18, cellHeight, screenWidth - 36, cellHeight);
    [callButton setTitle:@"客服电话  400-6706-082" forState:UIControlStateNormal];
    callButton.titleLabel.font = Font(13);
    [callButton setTitleColor:littleBlackColor forState:UIControlStateNormal];
    callButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

    [firstView addSubview:callButton];
    
    
    UIView *secondView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(firstView.frame) + 10, screenWidth, cellHeight)];
    secondView.backgroundColor = WhiteColor;
    UILabel *label = [self backLabelWithFrame:CGRectMake(18, 0, 100, cellHeight) andFont:14 andColor:littleBlackColor];
    label.text = @"物流跟踪";
    [secondView addSubview:label];
    
    UILabel *lineL = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight - 0.5, screenWidth, 0.5)];
    lineL.backgroundColor = LineGrayColor;
    [secondView addSubview:lineL];
    
    [self.view addSubview:firstView];
    [self.view addSubview:secondView];
    
    
    
}

-(void)initTableView
{
//    NSArray *arr = @[@"啊啦大家发来的警方了李峰家\n大发生\n史蒂夫",@"啊啦大家发来的警方了李峰家",@"啊啦大家发来的警方了李峰家\n2 \n大发生阿萨德冯绍峰",@"啊啦大家发来的警方了李峰家都是方法\n大发生\n史蒂夫史蒂夫舒服"];
//    
//    for (int i = 0; i < arr.count; i++) {
//        Model *model = [[Model alloc]init];
//
//        model.contact = arr[i];
//        
//        [self.dataArray addObject:model];
//    }
    
   
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(40, 0, screenWidth, screenHeight - 64) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]init];
    self.tableView.scrollEnabled = NO;
    
    
    [self.scrollView addSubview:self.tableView];
    
    
}




-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str = @"giveOrTakeCell";
    ListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    if (!cell) {
        cell = [[ListTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:str];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
//    CGRect rect = [self.tableView rectForRowAtIndexPath:indexPath];
//    [self.heightArray addObject:@(rect.size.height)];

    Model *model = [[Model alloc]init];
    
    if (self.dataArray.count > 0) {
        

//    model = self.dataArray[indexPath.row];

    if (self.dataArray.count > 0) {
        
        if (indexPath.row ==self.dataArray.count - 1) {
            
            if ([self.bidderType isEqualToString:@"10"]) {
                
                model.contact = [NSString stringWithFormat:@"%@\n司机:%@  电话:%@",self.dataArray[indexPath.row][@"info"],self.dataDictionary[@"driver"],self.dataDictionary[@"phone"]];
            }else{
                model.contact = [NSString stringWithFormat:@"%@\n承运公司:%@  司机:%@  电话:%@",self.dataArray[indexPath.row][@"info"],self.dataDictionary[@"carrier"],self.dataDictionary[@"driver"],self.dataDictionary[@"phone"]];
            }
            
        } else {
            
            model.contact = [NSString stringWithFormat:@"%@",self.dataArray[indexPath.row][@"info"]];

        }
        
        cell.model = model;

    }
    
    cell.titleL.text = model.contact;
    cell.dateLabel.text = self.dataArray[indexPath.row][@"createTime"];
    
   
    }
    
    return cell;
}



//创建左边试图
-(void)initLeftView
{
    
    self.imageV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 14.5, 14.5)];
    self.imageV.image = [UIImage imageNamed:@"circle"];
    
    CGFloat height = 0.0f;
    for (int i = 0; i < self.heightArray.count; i++) {
        height += [self.heightArray[i] floatValue];
    }
    
    self.tableView.frame = CGRectMake(40,0 , screenWidth, height );
    self.scrollView.frame  = CGRectMake(0, cellHeight * 3 + 10 + 64, screenWidth, screenHeight - cellHeight * 3 - 10 - 64 );
    self.scrollView.contentSize = CGSizeMake(screenWidth, height );
    self.scrollView.bounces = NO;
    
    self.leftView = [[UIView alloc]initWithFrame:CGRectMake(0,  0, 40, height)];
    self.leftView.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:self.leftView];
    
    for (int i = 0;  i < self.heightArray.count; i++) {
        
        CGFloat h = 0.0;
        if (i == 0) {
            h = 14 * kHeight;
        } else {
            h = 14 * kHeight + [self backHeightWith:i - 1];
        }
        
        
        UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(19, h, 8, 8)];
        imageV.layer.cornerRadius = 4;
        imageV.layer.masksToBounds = YES;
        imageV.backgroundColor = LineGrayColor;
        
        [self.leftView addSubview:imageV];
        
        if (i == 0) {
            
            self.imageV.center = imageV.center;
            [self.leftView addSubview:self.imageV];
            
        }
        
    
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMidX(imageV.frame), CGRectGetMaxY(imageV.frame) , 0.5, [self.heightArray[i] integerValue])];
        label.backgroundColor = LineGrayColor;
        
        if (i != self.heightArray.count -1) {
            [self.leftView addSubview:label];
        }
    }
}

-(CGFloat)backHeightWith:(int) a
{
    
    CGFloat height = 0.0;
    for (int i = 0; i <= a; i++) {
        height += [self.heightArray[i] floatValue];
    }
    
    return height;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Model *model = [[Model alloc]init];
    
    
    if (self.dataArray.count > 0) {
        
        if (indexPath.row ==self.dataArray.count - 1) {
            
            if ([self.bidderType isEqualToString:@"10"]) {
                
                model.contact = [NSString stringWithFormat:@"%@\n司机:%@  电话:%@",self.dataDictionary[@"carrier"],self.dataDictionary[@"driver"],self.dataDictionary[@"phone"]];
            }else{
                model.contact = [NSString stringWithFormat:@"%@\n承运公司:%@  司机:%@  电话:%@",self.dataArray[indexPath.row][@"info"],self.dataDictionary[@"carrier"],self.dataDictionary[@"driver"],self.dataDictionary[@"phone"]];
            }
        } else {
            
            model.contact = [NSString stringWithFormat:@"%@",self.dataArray[indexPath.row][@"info"]];
            
        }
    }

//    return  [ListTableViewCell cellHeightWithModel:model];
    return [self.heightArray[indexPath.row] floatValue];
}



- (void)viewDidLoad {
    [super viewDidLoad];

    self.dataArray = [NSMutableArray array];
    self.heightArray = [NSMutableArray array];
    self.dataDictionary = [NSMutableDictionary dictionary];
    self.bidderType = [[NSString alloc]init];
    
    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"物流详情"];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];

    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, cellHeight * 3 + 10 + 64, screenWidth, screenHeight - cellHeight * 3 - 10 - 64)];
    self.scrollView.backgroundColor = GrayColor;
    [self.view addSubview:self.scrollView];
    
    self.nullView = [[NullView  alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight - cellHeight * 3 - 10 - 64) andTitle:@""];
    self.nullView.label.text = @"暂无物流信息";

    self.nullView.backgroundColor = GrayColor;

    
    [self initDataSources];
    [self initTopView];
    [self initTableView];

}

-(void)initDataSources
{
    /*
    //假数据
    NSDictionary *dic = [NSDictionary dictionary];
    dic =  [Common txtChangToJsonWithString:@"wuliuxiangqing"];
    
    
    if ([[dic[@"data"] allKeys] count] > 0) {
        
        self.dataDictionary = [NSMutableDictionary dictionaryWithDictionary:dic[@"data"]];

        self.dataArray = [NSMutableArray arrayWithArray:self.dataDictionary[@"trails"]];
        
        for (int i = 0; i < self.dataArray.count ; i++) {
            if (i == self.dataArray.count - 1) {
                [self.heightArray addObject:@(68.3 * kHeight)];
            } else {
                [self.heightArray addObject:@(53.6 * kHeight)];
            }
        }
        
        [self initLeftView];
        

        [self.tableView reloadData];
    }
     */
    
    //网路数据
    
     NSString *urlString = [NSString stringWithFormat:@"%@?serviceorderid=%@",order_logistiks,self.orderId];
    
    
     [Common requestWithUrlString:urlString contentType:application_json finished:^(id responseObj){
         
     if ([responseObj[@"data"] isEqual:[NSNull null]]) {
     
     } else {
     
     self.dataDictionary = [NSMutableDictionary dictionaryWithDictionary:responseObj[@"data"]];
     
                
//     [self getValueString];
         
         if ([responseObj[@"success"] boolValue]) {
             

         if ([self.dataDictionary[@"trails"] isEqual:[NSNull null]]) {
             
             [self.scrollView addSubview:self.nullView];

             
         } else {
             self.bidderType = [NSString stringWithFormat:@"%@",self.dataDictionary[@"bidderType"]];
             self.dataArray = [NSMutableArray arrayWithArray:self.dataDictionary[@"trails"]];
             
             
             [self.nullView removeFromSuperview];

             for (NSDictionary *dic in self.dataDictionary[@"trails"]) {
                 
                 Model *model = [[Model alloc]init];
                 [model  setValuesForKeysWithDictionary:dic];
                 
                 CGFloat h = [ListTableViewCell cellHeightWithModel:model];
                 [self.heightArray addObject:@(h)];

                 
             }
             
             [self.heightArray replaceObjectAtIndex:_heightArray.count - 1 withObject:@(75.3 * kHeight)];
             
             
             
             for (int i = 0; i < self.dataArray.count; i++) {
                 
                 
//                 
//                 if (i == self.dataArray.count -1) {
//                     [self.heightArray addObject:@(68.3 * kHeight)];
//                 } else {
//                     [self.heightArray addObject:@(53.6 * kHeight)];
//                 }
             }
             
             [self initLeftView];
             
             
             [self.tableView reloadData];
         }
         
    
     }
     }
     
     } failed:^(NSString *errorMsg) {
     NSLog(@"%@",errorMsg);
     }];
         
     
    
}

-(UILabel *)backLabelWithFrame:(CGRect)frame andFont:(NSInteger)integer andColor:(UIColor *)color
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.textColor = color;
    label.font = Font(integer);
    
    
    return label;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
