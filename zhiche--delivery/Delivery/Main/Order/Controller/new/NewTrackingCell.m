//
//  NewTrackingCell.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 17/9/5.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import "NewTrackingCell.h"
#import <Masonry.h>
#import "Header.h"
#import "Common.h"
@implementation NewTrackingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
        
    }
    
    return  self;
}

-(void)initSubViews
{
    __weak typeof(self) weakSelf = self;
    
    
    Common * com = [[Common alloc]init];
    
    //车型
    UILabel *label1 = [com createUIlabel:@"品牌/车型" andFont:13 andColor:carScrollColor];;
    [self.contentView addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(18);
        make.height.mas_equalTo(28 * kHeight);
        make.top.mas_equalTo(self.contentView.mas_top).offset(10);
        
    }];
    
    
    self.carStyleLabel = [com createUIlabel:@"" andFont:13 andColor:littleBlackColor];
    [self.contentView addSubview:self.carStyleLabel];
    [self.carStyleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(label1.mas_right).with.offset(8*kWidth);
        make.centerY.mas_equalTo(label1.mas_centerY);
        
    }];
    self.carStyleLabel.textColor = littleBlackColor;
    self.carStyleLabel.font = Font(14);
    //    self.carStyleLabel.text = @"兰博基尼－大牛";
    self.carStyleLabel.textAlignment = NSTextAlignmentRight;
    
    
    //vin
    
    UILabel *label2 = [com createUIlabel:@"VIN" andFont:13 andColor:carScrollColor];;
    [self.contentView addSubview:label2];
    label2.textAlignment = NSTextAlignmentLeft;
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(label1.mas_left);
        make.top.mas_equalTo(label1.mas_bottom).offset(0);
        
    }];

    UIView * viewline = [[UIView alloc]init];
    viewline.backgroundColor = LineGrayColor;
    [self.contentView addSubview:viewline];
    [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right);
        make.bottom.mas_equalTo(label2.mas_bottom).with.offset(5*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    
    
    //状态栏
    self.statusLabel = [com createUIlabel:@"" andFont:13 andColor:RedColor];
    [self.contentView addSubview:self.statusLabel];
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(-18);
        make.centerY.mas_equalTo(self.carStyleLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(80, cellHeight));
        
    }];

    self.statusLabel.textAlignment = NSTextAlignmentRight;
    
    
    
    
    self.vinLabel = [com createUIlabel:@"" andFont:13 andColor:littleBlackColor];
    [self.contentView addSubview:self.vinLabel];
    [self.vinLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.width.height.equalTo(weakSelf.carStyleLabel);
        make.top.mas_equalTo(weakSelf.carStyleLabel.mas_bottom).offset(0);
    }];
    self.vinLabel.textColor = littleBlackColor;
    self.vinLabel.font = Font(14);
    //    self.vinLabel.text = @"123123123123123121";
    self.vinLabel.textAlignment = NSTextAlignmentRight;
    
    
    
    UILabel * label3 = [com createUIlabel:@"提车信息" andFont:13 andColor:carScrollColor];
    [self.contentView addSubview:label3];
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(weakSelf.contentView.mas_left).with.offset(18);
        make.top.mas_equalTo(label2.mas_bottom).offset(15*kHeight);
    }];
    
    UIView * viewline1 = [[UIView alloc]init];
    viewline1.backgroundColor = LineGrayColor;
    [self.contentView addSubview:viewline1];
    [viewline1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right);
        make.bottom.mas_equalTo(label3.mas_bottom).with.offset(10*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    self.carPickTimeLabel = [com createUIlabel:@"" andFont:13 andColor:littleBlackColor];
    [self.contentView addSubview:self.carPickTimeLabel];
    [self.carPickTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(label3.mas_centerY);
        make.left.mas_equalTo(self.carStyleLabel.mas_left);
    }];

    self.carPeopleLabel = [com createUIlabel:@"" andFont:13 andColor:littleBlackColor];
    [self.contentView addSubview:self.carPeopleLabel];
    [self.carPeopleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(weakSelf.statusLabel.mas_left);
        make.centerY.mas_equalTo(label3.mas_centerY);
    }];
    
    //    personal_arrow@2x.png
    //    personal_arrow@3x.png  7*11
    

    
    UIImageView * image0 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"personal_arrow"]];
    [self.contentView addSubview:image0];
    [image0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right).with.offset(-14*kWidth);
        make.centerY.mas_equalTo(self.carPeopleLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(7*kWidth, 11*kHeight));
    }];

    
    self.carPickBtn = [NewBtn buttonWithType:UIButtonTypeCustom];
//    self.carPickBtn.backgroundColor = [UIColor cyanColor];
    [self.contentView addSubview:self.carPickBtn];
    
    [self.carPickBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right);
        make.centerY.mas_equalTo(self.carPeopleLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(60*kWidth, 30*kHeight));
    }];
    
    
    
    
    

    
    UILabel * label4 = [com createUIlabel:@"在途跟踪" andFont:13 andColor:carScrollColor];
    [self.contentView addSubview:label4];
    [label4 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(weakSelf.contentView.mas_left).with.offset(18);
        make.top.mas_equalTo(label3.mas_bottom).offset(15*kHeight);
    }];
    
    UIView * viewline2 = [[UIView alloc]init];
    viewline2.backgroundColor = LineGrayColor;
    [self.contentView addSubview:viewline2];
    [viewline2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right);
        make.bottom.mas_equalTo(label4.mas_bottom).with.offset(10*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];

    
    
    
    
    self.carOnTimeLabel = [com createUIlabel:@"" andFont:13 andColor:littleBlackColor];
    [self.contentView addSubview:self.carOnTimeLabel];
    
    [self.carOnTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.carStyleLabel.mas_left);
        make.centerY.mas_equalTo(label4.mas_centerY);
    }];
    
    

    self.locationLabel = [com createUIlabel:@"" andFont:13 andColor:littleBlackColor];
    [self.contentView addSubview:self.locationLabel];
    
    [self.locationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(self.carOnTimeLabel.mas_centerY);
        make.left.mas_equalTo(self.statusLabel.mas_left);
    }];

    
    UIImageView * image1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"personal_arrow"]];
    [self.contentView addSubview:image1];
    [image1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right).with.offset(-14*kWidth);
        make.centerY.mas_equalTo(self.locationLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(7*kWidth, 11*kHeight));
    }];
    

    self.carOnBtn = [NewBtn buttonWithType:UIButtonTypeCustom];
//    self.carOnBtn.backgroundColor = [UIColor cyanColor];
    [self.contentView addSubview:self.carOnBtn];
    
    [self.carOnBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right);
        make.centerY.mas_equalTo(self.locationLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(60*kWidth, 30*kHeight));
    }];

    
    
    
    UILabel * label5 = [com createUIlabel:@"交车信息" andFont:13 andColor:carScrollColor];
    [self.contentView addSubview:label5];
    [label5 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(weakSelf.contentView.mas_left).with.offset(18);
        make.top.mas_equalTo(label4.mas_bottom).offset(15*kHeight);
    }];
    
    self.carHandTimeLabel = [com createUIlabel:@"" andFont:13 andColor:littleBlackColor];
    [self.contentView addSubview:self.carHandTimeLabel];
    [self.carHandTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(weakSelf.carOnTimeLabel.mas_left);
        make.centerY.mas_equalTo(label5.mas_centerY);
    }];

    UIImageView * image2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"personal_arrow"]];
    [self.contentView addSubview:image2];
    [image2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right).with.offset(-14*kWidth);
        make.centerY.mas_equalTo(self.carHandTimeLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(7*kWidth, 11*kHeight));
    }];

    self.carHandBtn = [NewBtn buttonWithType:UIButtonTypeCustom];
//    self.carHandBtn.backgroundColor = [UIColor cyanColor];
    [self.contentView addSubview:self.carHandBtn];
    
    [self.carHandBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right);
        make.centerY.mas_equalTo(self.carHandTimeLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(60*kWidth, 30*kHeight));
    }];

    

    

    

    
    
    //查看详情
//    self.detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [self.contentView addSubview:self.detailButton];
//    [self.detailButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.right.mas_equalTo(-18);
//        make.centerY.equalTo(label5);
//        make.size.mas_equalTo(CGSizeMake(100 * kWidth, 30 * kHeight));
//        
//    }];
//    [self.detailButton setBackgroundColor:YellowColor];
//    [self.detailButton setTitle:@"查看物流详情" forState:UIControlStateNormal];
//    [self.detailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    self.detailButton.layer.cornerRadius = 5;
//    self.detailButton.titleLabel.font = Font(14);
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
