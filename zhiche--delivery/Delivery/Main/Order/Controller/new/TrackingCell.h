//
//  TrackingCell.h
//  动态布局
//
//  Created by LeeBruce on 16/8/31.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackingCell : UITableViewCell
//@property (nonatomic,strong) UILabel *firstLabel;
@property (nonatomic,strong) UILabel *statusLabel;
@property (nonatomic,strong) UILabel *carStyleLabel;
@property (nonatomic,strong) UILabel *vinLabel;
@property (nonatomic,strong) UILabel *locationLabel;
@property (nonatomic,strong) UIButton *detailButton;
@end
