//
//  ComplaintViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/9/8.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ComplaintViewController.h"
#import "TopBackView.h"
#import "RootViewController.h"
#import "Common.h"

@interface ComplaintViewController ()<UITextViewDelegate,UITextFieldDelegate>

@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) UITextView *textView;//订单号
@property (nonatomic,strong) UILabel *orderLabel;//主题
@property (nonatomic,strong) UITextField *detailField;//描述
@property (nonatomic,strong) UILabel *placeholdLabel;

@property (nonatomic,retain) UILabel *sumLabel;

@property (nonatomic,retain) UILabel *countLabel;

@end

@implementation ComplaintViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"投诉"];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = WhiteColor;
    
    [self initTopView];
    
}

-(void)initTopView
{
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight)];
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = WhiteColor;
    
    
    UIView *firstV = [self backViewWithOriginY:0 andHidden:NO];
    [self.scrollView addSubview:firstV];
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, 50 * kWidth, cellHeight)];
    label1.text = @"订单号:";
    label1.textColor = littleBlackColor;
    label1.font = Font(14);
    [firstV addSubview:label1];
    
    self.orderLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(label1.frame), 0, 200, cellHeight)];
    self.orderLabel.text = @"45645876745624625";
    self.orderLabel.textColor = littleBlackColor;
    self.orderLabel.font = Font(14);
    [firstV addSubview:self.orderLabel];
    
    
    UIView *secondV = [self backViewWithOriginY:cellHeight andHidden:NO];
    [self.scrollView addSubview:secondV];
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, 70 * kWidth, cellHeight)];
    label2.text = @"投诉主题:";
    label2.textColor = littleBlackColor;
    label2.font = Font(14);
    [secondV addSubview:label2];
    
    self.detailField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(label2.frame), 0, 200 * kWidth, cellHeight)];
    self.detailField.placeholder = @"请输入主题";
    self.detailField.font = Font(14);
    self.detailField.returnKeyType = UIReturnKeyDone;
    [secondV addSubview:self.detailField];
    self.detailField.delegate = self;
    
    UIView *thirdV = [self backViewWithOriginY:cellHeight * 2 andHidden:YES];
    [self.scrollView addSubview:thirdV];
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, 200 * kWidth, cellHeight)];
    label3.text = @"详细内容";
    label3.textColor = littleBlackColor;
    label3.font = Font(14);
    [thirdV addSubview:label3];

    [self initTextView];
    
}

-(void)initTextView
{
    
     self.textView = [[UITextView alloc]initWithFrame:CGRectMake(40, cellHeight * 3 + 20, screenWidth - 80, 155 * kHeight)];
     self.textView.layer.cornerRadius = 5;
     self.textView.layer.borderColor = carScrollColor.CGColor;
     self.textView.layer.borderWidth = 0.5;
     self.textView.textColor = Color_RGB(181, 181, 184, 1);
     self.textView.font = Font(14);
     self.textView.delegate = self;
     [self.scrollView addSubview:self.textView];
     self.textView.returnKeyType = UIReturnKeyDone;
     
     
     self.placeholdLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 200, 30)];
     self.placeholdLabel.font = Font(12);
     self.placeholdLabel.textColor = Color_RGB(181, 181, 184, 1);
     self.placeholdLabel.text = @"请输入1-500字描述";
     [self.placeholdLabel sizeToFit];
     self.placeholdLabel.tag = 506;
     [self.textView addSubview:self.placeholdLabel];
    
    
    UIButton *publishButton = [UIButton buttonWithType:UIButtonTypeCustom];
    publishButton.frame = CGRectMake(18, CGRectGetMaxY(self.textView.frame) + 40, screenWidth - 36, Button_Height);
    publishButton.backgroundColor = YellowColor;
    publishButton.layer.cornerRadius = 5;
    [publishButton setTitle:@"提交" forState:UIControlStateNormal];
    [publishButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    [self.scrollView addSubview:publishButton];
    [publishButton addTarget:self action:@selector(publishButton) forControlEvents:UIControlEventTouchUpInside];
    publishButton.tag = 505;
    
    self.scrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(publishButton.frame) + 100);

    self.countLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.textView.frame) - 80 , CGRectGetMaxY(self.textView.frame) - 25 , 35 , 21 )];
    self.countLabel.textAlignment = NSTextAlignmentRight;
    
    self.sumLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.textView.frame) - 45 , CGRectGetMaxY(self.textView.frame) - 25 , 35 , 21 )];
    self.sumLabel.text = @"/500";
    self.countLabel.text = @"0";
    self.countLabel.font = [UIFont systemFontOfSize:15.0];
    self.sumLabel.font = [UIFont systemFontOfSize:15.0];
    
    self.sumLabel.tintColor = [UIColor redColor];
    self.countLabel.tintColor = [UIColor redColor];
    
    [self.scrollView addSubview:self.sumLabel];
    [self.scrollView addSubview:self.countLabel];

}


-(UIView *)backViewWithOriginY:(CGFloat)y andHidden:(BOOL)show
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, y, screenWidth, cellHeight)];
    view.backgroundColor = WhiteColor;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight - 0.5, screenWidth, 0.5)];
    label.backgroundColor = LineGrayColor;
    [view addSubview:label];
    label.hidden = show;
    
    return view;
}

#pragma mark UITextViewDelegate

//如果输入超过规定的字数500，就不再让输入
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location>=500 || [ @"\n" isEqualToString: text])
    {
        [textView resignFirstResponder];
        
        return  NO;
    }
    else
    {
        return YES;
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.placeholdLabel removeFromSuperview];
    
    
    UIButton *button = [self.view viewWithTag:505];
    CGFloat height = CGRectGetMaxY(button.frame);
    
    CGFloat h = height + 64 - (screenHeight - 280);
    
    
    if (h > 0) {
        
        [UIView animateWithDuration:0.35 animations:^{
            self.scrollView.contentInset = UIEdgeInsetsMake(-h, 0, 0, 0);
            
        }];
        
    }
    
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    
    [UIView animateWithDuration:0.35 animations:^{
        self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        if (textView.text.length == 0) {
            [self.textView addSubview:self.placeholdLabel];
        }
        
    }];
}


//因此使用textViewDidChange对TextView里面的字数进行判断
-(void)textViewDidChange:(UITextView *)textView
{
    
    int remainNum = 0;
    
    if (textView.text.length < 500) {
        _textView.selectedRange=NSMakeRange(0,0) ;   //起始位置
        
        _textView.selectedRange=NSMakeRange(_textView.text.length,0);
        
        int surplusNum = (int)textView.text.length ;
        remainNum =  surplusNum;
        _countLabel.text = [NSString stringWithFormat:@"%d",remainNum];
//        NSLog(@"%@",_countLabel.text);
        
    }
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIButton *button = [self.view viewWithTag:505];
    CGFloat height = CGRectGetMaxY(button.frame);
    
    CGFloat h = height + 64 - (screenHeight - 280);
    
    
    if (h > 0) {
        
        [UIView animateWithDuration:0.35 animations:^{
            self.scrollView.contentInset = UIEdgeInsetsMake(-h, 0, 0, 0);
            
        }];
        
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.35 animations:^{
        self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)aTextfield {
     [self.detailField resignFirstResponder];//关闭键盘
    return YES;
}


-(void)publishButton
{
    Common *c = [[Common alloc]init];
    NSDictionary *dic = @{@"id":self.orderId,
                          @"title":self.detailField.text,
                          @"content":self.textView.text};
    [c afPostRequestWithUrlString:complain_create parms:dic finishedBlock:^(id responseObj) {
        
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableLeaves error:nil];
        NSLog(@"%@",dictionary);
        
        
    } failedBlock:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
