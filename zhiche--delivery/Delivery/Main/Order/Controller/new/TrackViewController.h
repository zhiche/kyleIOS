//
//  TrackViewController.h
//  动态布局
//
//  Created by LeeBruce on 16/9/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackViewController : UIViewController
@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) NSString *orderId;
@property (nonatomic,strong) NSString *numberString;
@end
