//
//  PayFinishVC.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/7/16.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "PayFinishVC.h"
#import "TopBackView.h"


@interface PayFinishVC ()

@end

@implementation PayFinishVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"支付订单"];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = GrayColor;
    
    [self initSubViews];
}


-(void)initSubViews
{
    UIView *firstView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, cellHeight)];
    firstView.backgroundColor = WhiteColor;
    [self.view addSubview:firstView];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, 50 * kWidth, cellHeight)];
    label.text = @"订单号:";
    label.font = Font(12);
    label.backgroundColor = WhiteColor;
    [firstView addSubview:label];
    
    UILabel *orderL = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(label.frame), 0, 200, cellHeight) andString:self.orderN andInteger:12];
    [firstView addSubview:orderL];
    
    UILabel *lineL = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight - 0.5, screenWidth, 0.5)];
    lineL.backgroundColor = LineGrayColor;
    [firstView addSubview:lineL];
    
    
    UIView *secondV = [[UIView alloc]initWithFrame:CGRectMake(0, 64 + cellHeight, screenWidth, 150 * kHeight)];
    [self.view addSubview:secondV];
    secondV.backgroundColor = WhiteColor;
    
    UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth/2.0 - 18, 32 * kHeight, 36, 36)];
    imageV.image = [UIImage imageNamed:@"pay_success"];
    [secondV addSubview:imageV];
    
//    
    UILabel *successL = [self backLabelWithFrame:CGRectMake(0, CGRectGetMaxY(imageV.frame) + 11 , screenWidth, 20) andString:@"支付成功" andInteger:17];
    successL.textAlignment = NSTextAlignmentCenter;
    [secondV addSubview:successL];


    
    UILabel *moneyL = [self backLabelWithFrame:CGRectMake(0, CGRectGetMaxY(successL.frame) + 15 * kHeight, screenWidth, 20) andString:[NSString stringWithFormat:@"成功支付金额:%@",self.moneyS] andInteger:13];
    moneyL.textAlignment = NSTextAlignmentCenter;
    NSString *contentStr = moneyL.text;
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:contentStr];
    //设置：在0-7个单位长度内的内容显示成红色
    [str addAttribute:NSForegroundColorAttributeName value:YellowColor range:NSMakeRange(7, self.moneyS.length)];
    moneyL.attributedText = str;
    
    [secondV addSubview:moneyL];
    
    
    UIButton *checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    checkButton.frame = CGRectMake(18, CGRectGetMaxY(secondV.frame) + 30 , screenWidth - 36, Button_Height);
    [checkButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    [checkButton setBackgroundColor:YellowColor];
    [checkButton setTitle:@"完成" forState:UIControlStateNormal];
    checkButton.layer.cornerRadius = 5;
    [checkButton addTarget:self action:@selector(checkButton) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.view addSubview:checkButton];
    
}


-(UILabel *)backLabelWithFrame:(CGRect)frame andString:(NSString *)string andInteger:(NSInteger)inter
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.text = string;
    label.textColor = littleBlackColor;
    label.font = Font(inter);
    
    return label;
    
}

-(void)checkButton
{
    [self.navigationController popToRootViewControllerAnimated:YES];

}


-(void)backAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
