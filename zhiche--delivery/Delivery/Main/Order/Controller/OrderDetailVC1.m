//
//  OrderDetailVC1.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/22.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "OrderDetailVC1.h"
#import "TopBackView.h"
#import "OrderDetailCell2.h"
#import "RootViewController.h"
#import "OrderDetailCell3.h"
#import "EvaluateViewController.h"

@interface OrderDetailVC1 () <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UIScrollView *scrollView;

@end

@implementation OrderDetailVC1

- (void)viewDidLoad {
    [super viewDidLoad];

    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"订单详情"];
    
    topBackView.rightButton.hidden = YES;
   
    
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = GrayColor;
    
    [self initTableView];

    
}

-(void)initTableView{
    
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight)];
    [self.view addSubview:self.scrollView];
    
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight * 2 + 10 + (cellHeight * 3 + 10) * 3) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.scrollView addSubview:self.tableView];
    self.tableView.backgroundColor = GrayColor;
    
    self.scrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(self.tableView.frame) + 30 + 64);

    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    if (self.number == 3) {
        //已完成的cell
        NSString *string = @"orderDetailCell3";
        OrderDetailCell3 *cell = [tableView dequeueReusableCellWithIdentifier:string];
        if (!cell) {
            cell = [[OrderDetailCell3 alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:string];
        }
        
        [cell.judgeButton addTarget:self action:@selector(judgeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.judgeButton.tag = 700 + indexPath.row;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;

    } else {
        NSString *string = @"orderDetailCell2";
        OrderDetailCell2 *cell = [tableView dequeueReusableCellWithIdentifier:string];
        if (!cell) {
            cell = [[OrderDetailCell2 alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:string];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell;

    }
    
  
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectZero];
    
    UILabel *orderL = [self backLabelWithFrame:CGRectMake(18, 0, 60, cellHeight) andColor:littleBlackColor andFont:12] ;
    orderL.text = @"订单号:";
    [view addSubview:orderL];
    
    UILabel *orderLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(orderL.frame), 0, 200, cellHeight) andColor:littleBlackColor andFont:12];
    [view addSubview:orderLabel];
    orderLabel.text = @"987654312123456";
    
    UILabel *addressLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 100 - 18, 0, 100, cellHeight) andColor:littleBlackColor andFont:12];
    addressLabel.text = @"郑州-南宁";
    addressLabel.textAlignment = NSTextAlignmentRight;
    [view addSubview:addressLabel];
    
    view.backgroundColor = WhiteColor;
    return view;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    UIView *backView = [[UIView alloc]initWithFrame:CGRectZero];
    backView.backgroundColor = GrayColor;
    
    
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 10, screenWidth, cellHeight)];
    [backView addSubview:view];
    
    UILabel *label = [self backLabelWithFrame:CGRectMake(95 * kWidth, 0, 65 * kWidth, cellHeight) andColor:YellowColor andFont:14];
    label.text = @"车辆总数:";
    [view addSubview:label];
    
    UILabel *carLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(label.frame), 0, 35 *kWidth, cellHeight) andColor:YellowColor andFont:14];
    carLabel.text = @"10辆";
    [view addSubview:carLabel];
    
    UILabel *moneyL = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(carLabel.frame) + 15, 0, 40 * kWidth, cellHeight) andColor:YellowColor andFont:14];
    moneyL.text = @"运费:";
    [view addSubview:moneyL];
    
    
    UILabel *priceLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(moneyL.frame), 0, 100, cellHeight) andColor:YellowColor andFont:14];
    priceLabel.text = @"12000.00";
    
    [view addSubview:priceLabel];
    view.backgroundColor = WhiteColor;
    return backView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return cellHeight + 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return cellHeight ;
}


-(UILabel *)backLabelWithFrame:(CGRect)frame andColor:(UIColor *)color andFont:(NSInteger)integer
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.textColor = color;
    label.font = Font(integer);
    
    return label;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight * 3 + 10;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
}

#pragma mark 评价
-(void)judgeButtonAction:(UIButton *)sender
{
    EvaluateViewController *evaluateVC = [[EvaluateViewController alloc]init];
    [self.navigationController pushViewController:evaluateVC animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
