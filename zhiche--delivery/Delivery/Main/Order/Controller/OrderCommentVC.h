//
//  OrderCommentVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 17/9/1.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface OrderCommentVC : NavViewController

@property (nonatomic,strong) NSString *orderId;
@property (nonatomic,strong) NSMutableDictionary *dataDictionary;
@property (nonatomic,assign) int isValuation;
@end
