//
//  DriverScoreVC.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "DriverScoreVC.h"
#import "TopBackView.h"
#import "DriverScoreCell.h"
#import "DriverScoreModel.h"

@interface DriverScoreVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@end

@implementation DriverScoreVC

- (void)viewDidLoad {
    [super viewDidLoad];
    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"师傅"];
    
    topBackView.rightButton.hidden = YES;
    
    
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = GrayColor;
    
    [self initTableView];
}

-(void)initTableView{

    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = GrayColor;
    
    self.tableView.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:self.tableView];
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //已完成的cell
    NSString *string = @"driverScoreCell3";
    DriverScoreCell *cell = [tableView dequeueReusableCellWithIdentifier:string];
    if (!cell) {
        cell = [[DriverScoreCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:string];
    }
    
  
    
    return cell;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DriverScoreModel *model = [[DriverScoreModel alloc]init];
    model.contact = @"1.衣带渐宽终不悔，为伊消得人憔悴.－柳永《凤栖梧》2 .死生契阔，与子成悦.执子之手，与子偕老.－佚名《诗经邶风击鼓》3 .两情若是久长时，又岂在朝朝暮暮.－秦观《鹊桥仙》4 .相思相见知何日？此时此夜难为情.－李白《三五七言》 5 .有美人兮，见之不忘，一日不见兮，思之如狂.－佚名《凤求凰琴歌》6 .这次我离开你，是风，是雨，是夜晚；你笑了笑，我摆一摆手，一条寂寞的路便展向两头了.－郑愁予《赋别》";
    
//    DriverScoreCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    return [DriverScoreCell cellHeightWithModel:model];
}


-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
