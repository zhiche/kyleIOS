//
//  StarCell.h
//  test
//
//  Created by 王亚陆 on 17/8/31.
//  Copyright © 2017年 sayimba. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "MovieStar.h"
@interface StarCell : UITableViewCell


@property (nonatomic,strong) UILabel * starLabel;
//@property (nonatomic,strong) MovieStar * star;
@property (nonatomic,strong) UILabel * stateLabel;


@end
