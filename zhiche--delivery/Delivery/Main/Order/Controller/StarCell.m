//
//  StarCell.m
//  test
//
//  Created by 王亚陆 on 17/8/31.
//  Copyright © 2017年 sayimba. All rights reserved.
//

#import "StarCell.h"
#import "Header.h"
#import <Masonry.h>
#define screenWidth [UIScreen mainScreen].bounds.size.width

@implementation StarCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        
        
        [self initSubViews];
        
    }
    
    return  self;
}

-(void)initSubViews
{
   
   
    
    self.starLabel = [self createUIlabel:@"提车评价" andFont:14 andColor:BlackColor];
    [self.contentView addSubview:self.starLabel];
    

    [self.starLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
        make.left.mas_equalTo(self.contentView.mas_left).with.offset(14*kHeight);
    }];

    
    
    self.stateLabel = [self createUIlabel:@"" andFont:14 andColor:BlackColor];
    [self.contentView addSubview:self.stateLabel];
    [self.stateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
        make.right.mas_equalTo(self.contentView.mas_right).with.offset(-18*kWidth);
    }];


    
}

-(UILabel *)backLabelWithOriginY:(CGFloat)y andString:(NSString *)str
{
    UILabel *speedLabel = [[UILabel alloc]initWithFrame:CGRectMake( (screenWidth - 74)/2.0 , y, 74, 20)];
    speedLabel.textAlignment = NSTextAlignmentCenter;
    speedLabel.text = str;
    speedLabel.textColor = [UIColor blackColor];
    
    return speedLabel;
    
}

//text自适应宽高
-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor *)color{
    
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];
    
    CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    // 名字的H
    CGFloat nameH = size.height;
    // 名字的W
    CGFloat nameW = size.width;
    
    label.textAlignment = NSTextAlignmentLeft;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font)
    return label;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
