//
//  MineOrderVC.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/9.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineOrderVC : UIViewController

@property (nonatomic,copy) NSString *hide;//用来判断是从设置里面推出还是初始化tabbar存在的
@property (nonatomic) BOOL isEva;//是否评价
@property (nonatomic,copy) NSString *orderId;


@end
