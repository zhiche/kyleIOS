//
//  CancelViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/27.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "CancelViewController.h"
#import "TopBackView.h"
#import "Common.h"
#import "CancelCell.h"



@interface CancelViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSString *cancelString;



@end

@implementation CancelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataArray= [NSMutableArray array];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;

    
    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"取消订单"];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = WhiteColor;
    
    [self initSubviews];
}

-(void)initOrderSource
{
    
//    self.cancelString = [self.cancelString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
//    NSString *urlString = [NSString stringWithFormat:@"%@order/cancel",Main_interface];
    
    NSDictionary *diction = @{@"orderid":@(self.integer),
                              @"reason":self.cancelString};
    
    
    Common *c = [[Common alloc]init];
    [c afPostRequestWithUrlString:Mine_Order_Cancel parms:diction finishedBlock:^(id responseObj) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableLeaves error:nil];
        
        
        if ([dic[@"success"] boolValue]) {
            [self alertControllerWith:dic[@"message"] andInteger:1];
        }
        
        
    } failedBlock:^(NSString *errorMsg) {
        
        NSLog(@"%@",errorMsg);
    }];
    
    
}


-(void)initSubviews
{
    self.dataArray = [NSMutableArray arrayWithObjects:@"运费太贵",@"不想运了",@"时间不对",@"其它", nil];
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64 ) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = GrayColor;
    [self.view addSubview:self.tableView];
    
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame = CGRectMake(18, (self.dataArray.count + 1) * cellHeight + 30 * kHeight, screenWidth - 36, Button_Height);
    [cancelButton setTitle:@"取消订单" forState:UIControlStateNormal];
    cancelButton.titleLabel.font = Font(16);
    cancelButton.backgroundColor = YellowColor;
    [cancelButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    cancelButton.layer.cornerRadius = 5;
    [cancelButton addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.tableView addSubview:cancelButton];
}

-(void)cancelAction:(UIButton *)sender
{
    if (self.cancelString.length <= 0) {
        [self alertControllerWith:@"请选择原因" andInteger:0];
        
    } else {
        [self initOrderSource];
    }
}


-(void)alertControllerWith:(NSString *)string andInteger:(NSInteger)integer
{
    
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"提示" message:string preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:nil];
    [alertC addAction:action];
    
    [self presentViewController:alertC animated:YES completion:nil];
    
    if (integer == 1) {
        //延迟执行
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [alertC dismissViewControllerAnimated:YES completion:nil];
            
            [self backAction];
            
        });
    }
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *string = @"cancalString";
    CancelCell *cell = [tableView dequeueReusableCellWithIdentifier:string];
    if (!cell) {
        cell = [[CancelCell alloc]initWithStyle:UITableViewCellStyleDefault
                                     reuseIdentifier:string];
    }
    
    cell.reasonLabel.text = self.dataArray[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setEditing:YES];
    
    return cell;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight)];
    view.backgroundColor = headerBottomColor;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, 100, cellHeight)];
    label.text = @"取消理由";
    label.font = Font(14);
    label.textColor = BlackTitleColor;
    [view addSubview:label];
    
    
    return view;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return cellHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CancelCell *cell = [tableView cellForRowAtIndexPath:indexPath];

    cell.selectImg.image = [UIImage imageNamed:@"pay_selectd"];
    
    self.cancelString = cell.reasonLabel.text;
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CancelCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    cell.selectImg.image = [UIImage imageNamed:@"pay_unselectd"];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
