//
//  EvaluateViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/11.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "EvaluateViewController.h"
#import "TopBackView.h"
#import "MovieStar.h"
#import <Masonry.h>
#import "RootViewController.h"
#import "Common.h"

@interface EvaluateViewController ()
@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) UILabel *orderLabel;

@property (nonatomic,strong) NSMutableArray *arr;

@property (nonatomic) NSInteger score1;
@property (nonatomic) NSInteger score2;
@property (nonatomic) NSInteger score3;
@property (nonatomic) NSInteger score4;
@property (nonatomic) NSInteger score5;
@property (nonatomic) NSInteger score6;

//@property (nonatomic,strong) UILabel *testLabel1;
//@property (nonatomic,strong) UILabel *testLabel2;

//@property (nonatomic,strong) UITextView *textView;
//@property (nonatomic,strong) UILabel *placeholdLabel;

//@property (nonatomic,strong) UIImageView *personalPicture;
//@property (nonatomic,strong) UILabel *nameLabel;
//@property (nonatomic,strong) UILabel *carStyleLabel;

//@property (nonatomic,strong) UIImageView *starImg;
//@property (nonatomic,strong) UILabel *scoreLabel;


@end

@implementation EvaluateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.view.backgroundColor = WhiteColor;
    self.arr = [NSMutableArray array];
    
    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"评价"];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    
    [self initDateSources];
    
   
    [self initSubViews];
}

-(void)initDateSources
{
    NSString *urlString = [NSString stringWithFormat:@"%@?id=%@",score_list,self.orderId];
    [Common requestWithUrlString:urlString contentType:application_json finished:^(id responseObj) {
       
        self.arr = [NSMutableArray arrayWithArray:responseObj[@"data"][@"scores"]];
        
        if ([responseObj[@"success"] boolValue]) {
            
            //说明评价过
            
            
            
        }
        
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
}

-(void)initSubViews
{
    
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight)];
    [self.view addSubview:self.scrollView];
    
    [self initFirstView];
//    [self initPersonalView];
    
    //提车评价
    
    UILabel *speedLabel = [self backLabelWithOriginY:75 * kHeight andString:@"提车评价"];
    
    [self.scrollView addSubview:speedLabel];
    
    UILabel *label1 = [self backLabelWithOriginX:40 andOriginY:CGRectGetMidY(speedLabel.frame)];
    [self.scrollView addSubview:label1];
    
 
    UILabel *label2 = [self backLabelWithOriginX:CGRectGetMaxX(speedLabel.frame)  andOriginY:CGRectGetMidY(speedLabel.frame)];
    [self.scrollView addSubview:label2];
    
    
    UILabel *speedL = [self backLabel2WithOriginY:CGRectGetMaxY(speedLabel.frame) + 30 andString:@"送货速度"];
    [self.scrollView addSubview:speedL];
    
    MovieStar *movieStar1 = [[MovieStar alloc]initWithFrame:CGRectMake( CGRectGetMaxX(speedL.frame) + 15, CGRectGetMaxY(speedLabel.frame) + 29 ,236, 22) andCount:0];
    [self.scrollView addSubview:movieStar1];
    
    UILabel *timeL1 = [self backLabel2WithOriginY:CGRectGetMaxY(movieStar1.frame) + 15 andString:@"时       效"];
    [self.scrollView addSubview:timeL1];
    
    MovieStar *movieStar2 = [[MovieStar alloc]initWithFrame:CGRectMake( CGRectGetMaxX(timeL1.frame) + 15, CGRectGetMidY(timeL1.frame) - 11 ,236, 22) andCount:0];
    [self.scrollView addSubview:movieStar2];
    
    
    
    
//    self.testLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(movieStar1.frame) + 14, screenWidth, 20)];
//    self.testLabel1.font = Font(11);
//    self.testLabel1.textColor = YellowColor;
//    self.testLabel1.textAlignment = NSTextAlignmentCenter;
//    self.testLabel1.text = @"完成匿名评价";
//    [self.scrollView addSubview:self.testLabel1];
    
    
    
    movieStar1.call_backValue = ^(NSInteger a){
        
        self.score1 = a;
    };
    
    movieStar2.call_backValue = ^(NSInteger a){
        
        self.score2 = a;
    };

    
    
    
    
    //运输评价
    
    
    UILabel *label3 = [self backLabelWithOriginX:40 andOriginY:CGRectGetMaxY(label1.frame) + 135];
    [self.scrollView addSubview:label3];
    
    UILabel *serviceLabel = [self backLabelWithOriginY:CGRectGetMaxY(label3.frame) - 10 andString:@"运输评价"];
    
    [self.scrollView addSubview:serviceLabel];
    
    UILabel *label4 = [self backLabelWithOriginX:CGRectGetMaxX(speedLabel.frame)  andOriginY:CGRectGetMaxY(label1.frame) + 135];
    [self.scrollView addSubview:label4];

    //运输安排合理性
    UILabel *arrangeL = [self backLabel2WithOriginY:CGRectGetMaxY(serviceLabel.frame) + 30 andString:@"运输安排合理性"];
    [self.scrollView addSubview:arrangeL];
    
    MovieStar *movieStar3 = [[MovieStar alloc]initWithFrame:CGRectMake(CGRectGetMaxX(arrangeL.frame) + 15, CGRectGetMaxY(serviceLabel.frame) + 29, 236, 22) andCount:0];
    [self.scrollView addSubview:movieStar3];

    
    //时效
    
    UILabel *timeL2 = [self backLabel2WithOriginY:CGRectGetMaxY(movieStar3.frame) + 15 andString:@"时       效"];
    [self.scrollView addSubview:timeL2];
    
    
    MovieStar *movieStar4 = [[MovieStar alloc]initWithFrame:CGRectMake(CGRectGetMaxX(timeL2.frame) + 15, CGRectGetMidY(timeL2.frame) - 11, 236, 22) andCount:0];
    [self.scrollView addSubview:movieStar4];
    
    movieStar3.call_backValue = ^(NSInteger a){
        
        self.score3 = a;
    };
    
    movieStar4.call_backValue = ^(NSInteger a){
        
        self.score4 = a;
    };


    
    //交车评价
    
    UILabel *label5 = [self backLabelWithOriginX:40 andOriginY:CGRectGetMaxY(label3.frame) + 135];
    [self.scrollView addSubview:label5];
    
    UILabel *giveLabel = [self backLabelWithOriginY:CGRectGetMaxY(label5.frame) - 10 andString:@"交车评价"];
    
    [self.scrollView addSubview:giveLabel];
    
    UILabel *label6 = [self backLabelWithOriginX:CGRectGetMaxX(speedLabel.frame)  andOriginY:CGRectGetMaxY(label3.frame) + 135];
    [self.scrollView addSubview:label6];
    
    //服务态度
    UILabel *serviceL = [self backLabel2WithOriginY:CGRectGetMaxY(giveLabel.frame) + 30 andString:@"服务态度"];
    [self.scrollView addSubview:serviceL];
    
    MovieStar *movieStar5 = [[MovieStar alloc]initWithFrame:CGRectMake(CGRectGetMaxX(serviceL.frame) + 15, CGRectGetMidY(serviceL.frame) - 11, 236, 22) andCount:0];
    [self.scrollView addSubview:movieStar5];
    
    
    //时效
    
    UILabel *timeL3 = [self backLabel2WithOriginY:CGRectGetMaxY(movieStar5.frame) + 15 andString:@"时       效"];
    [self.scrollView addSubview:timeL3];
    
    
    MovieStar *movieStar6 = [[MovieStar alloc]initWithFrame:CGRectMake(CGRectGetMaxX(timeL3.frame) + 15, CGRectGetMidY(timeL3.frame) - 11, 236, 22) andCount:0];
    [self.scrollView addSubview:movieStar6];
    

    
//    self.testLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(movieStar2.frame) + 14, screenWidth, 20)];
//    self.testLabel2.font = Font(11);
//    self.testLabel2.textColor = YellowColor;
//    self.testLabel2.textAlignment = NSTextAlignmentCenter;
//    self.testLabel2.text = @"完成匿名评价";
//    [self.scrollView addSubview:self.testLabel2];
    
    movieStar5.call_backValue = ^(NSInteger a){
        
        self.score5 = a;
    };
    
    movieStar6.call_backValue = ^(NSInteger a){
        
        self.score6 = a;
    };
 
    /*
    self.textView = [[UITextView alloc]initWithFrame:CGRectMake(40, CGRectGetMaxY(self.testLabel2.frame) + 20, screenWidth - 80, 54 * kHeight)];
    self.textView.layer.cornerRadius = 5;
    self.textView.layer.borderColor = carScrollColor.CGColor;
    self.textView.layer.borderWidth = 0.5;
    self.textView.textColor = Color_RGB(181, 181, 184, 1);
    self.textView.font = Font(12);
    self.textView.delegate = self;
    [self.scrollView addSubview:self.textView];
    self.textView.returnKeyType = UIReturnKeyDone;
    
    
    self.placeholdLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 200, 30)];
    self.placeholdLabel.font = Font(12);
    self.placeholdLabel.textColor = Color_RGB(181, 181, 184, 1);
    self.placeholdLabel.text = @"请输入1-50字描述";
    [self.placeholdLabel sizeToFit];
    self.placeholdLabel.tag = 506;
    [self.textView addSubview:self.placeholdLabel];
     */
    
    UIButton *publishButton = [UIButton buttonWithType:UIButtonTypeCustom];
    publishButton.frame = CGRectMake(18, CGRectGetMaxY(movieStar6.frame) + 40, screenWidth - 36, Button_Height);
    publishButton.backgroundColor = YellowColor;
    publishButton.layer.cornerRadius = 5;
    [publishButton setTitle:@"提交评价" forState:UIControlStateNormal];
    [publishButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    [self.scrollView addSubview:publishButton];
    [publishButton addTarget:self action:@selector(publishButton) forControlEvents:UIControlEventTouchUpInside];
    publishButton.tag = 505;
    
    self.scrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(publishButton.frame) + 100);
    
}

-(void)initFirstView
{
    UIView *firstView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight)];
    firstView.backgroundColor = headerBottomColor;
    [self.scrollView addSubview:firstView];
    
    UILabel *orderL = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, 60, cellHeight)];
    orderL.text = @"订单号:";
    orderL.textColor = carScrollColor;
    orderL.font = Font(14);
    [firstView addSubview:orderL];
    
    self.orderLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(orderL.frame), 0, 300, cellHeight)];
    self.orderLabel.textColor = carScrollColor;
    self.orderLabel.text = @"zc131412342343413241234";
    self.orderLabel.font = Font(14);
    [firstView addSubview:self.orderLabel];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight - 0.5, screenWidth, 0.5)];
    label.backgroundColor = GrayColor;
    [firstView addSubview:label];
    
    
}

/*
-(void)initPersonalView
{
    self.personalPicture = [[UIImageView alloc]initWithFrame:CGRectMake(15, 24, 40 * kWidth, 40 * kWidth)];
    self.personalPicture.layer.cornerRadius = 20 * kWidth;
    self.personalPicture.layer.masksToBounds = YES;
    self.personalPicture.image = [UIImage imageNamed:@"personal_picture"];
    
    [self.scrollView addSubview:self.personalPicture];
    
    //姓名 ＋ 车牌
    self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.personalPicture.frame) + 16, CGRectGetMinY(self.personalPicture.frame) - 10, 200, 22 * kWidth)];
    self.nameLabel.textColor = littleBlackColor;
    self.nameLabel.font = Font(14);
    [self.scrollView addSubview:self.nameLabel];
    self.nameLabel.text = @"周师傅 ● 京QH1234";
    
    self.carStyleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame), 200, 22 * kWidth)];
    self.carStyleLabel.textColor = fontGrayColor;
    self.carStyleLabel.text = @"车型 ● 框架";
    [self.scrollView addSubview:self.carStyleLabel];
    self.carStyleLabel.font = Font(11);
    
    //星星
    self.starImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"idea_product_list_stars_null"]];
    self.starImg.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.carStyleLabel.frame) + 5 * kHeight, 47.5, 7.5);
    [self.scrollView addSubview:self.starImg];
    
    
    //评分
    self.scoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.starImg.frame) + 10, CGRectGetMaxY(self.carStyleLabel.frame) + 2.5 * kHeight, 20, 12)];
    self.scoreLabel.font = Font(10);
    self.scoreLabel.backgroundColor = YellowColor;
    self.scoreLabel.textColor = WhiteColor;
    self.scoreLabel.layer.cornerRadius = 5;
    [self.scrollView addSubview:self.scoreLabel];
    self.scoreLabel.text = @"4.8";
    self.scoreLabel.textAlignment = NSTextAlignmentCenter;
 
}
 */

-(void)publishButton
{
    for (int i = 0; i < self.arr.count; i ++) {
        
        NSDictionary *dic = self.arr[i];
        
        switch (i) {
            case 0:
                [dic setValue:@(self.score1) forKey:@"score"];

                break;
                
            case 1:
                [dic setValue:@(self.score2) forKey:@"score"];
                
                break;
            case 2:
                [dic setValue:@(self.score3) forKey:@"score"];
                
                break;
            case 3:
                [dic setValue:@(self.score4) forKey:@"score"];
                
                break;
            case 4:
                [dic setValue:@(self.score5) forKey:@"score"];
                
                break;
            case 5:
                [dic setValue:@(self.score6) forKey:@"score"];
                
                break;
                
            default:
                break;
        }
        
        
        [self.arr replaceObjectAtIndex:i withObject:dic];
        
    }
    
    //评分
    Common *c = [[Common alloc]init];
    
    [c afPostRequestWithUrlString:score_create parms:@{@"id":self.orderId,@"scores":self.arr} finishedBlock:^(id responseObj) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableLeaves error:nil];
        
        NSLog(@"%@",dic);
        
        
    } failedBlock:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
}

/*
#pragma mark UITextViewDelegate

//如果输入超过规定的字数50，就不再让输入
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location>=50 || [ @"\n" isEqualToString: text])
    {
        [textView resignFirstResponder];

        return  NO;
    }
    else
    {
        return YES;
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.placeholdLabel removeFromSuperview];
    
    
    UIButton *button = [self.view viewWithTag:505];
    CGFloat height = CGRectGetMaxY(button.frame);
    
    CGFloat h = height + 64 - (screenHeight - 280);
    
    
    if (h > 0) {
        
        [UIView animateWithDuration:0.35 animations:^{
            self.scrollView.contentInset = UIEdgeInsetsMake(-h, 0, 0, 0);

        }];
        
    }

}

-(void)textViewDidEndEditing:(UITextView *)textView
{

    [UIView animateWithDuration:0.35 animations:^{
        self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        if (textView.text.length == 0) {
            [self.textView addSubview:self.placeholdLabel];
        }
        
    }];
}


*/

-(UILabel *)backLabelWithOriginX:(CGFloat)x andOriginY:(CGFloat)y
{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(x, y, (screenWidth - 154)/2.0, 1)];
    label.backgroundColor = GrayColor;
    
    return label;
    
}

-(UILabel *)backLabelWithOriginY:(CGFloat)y andString:(NSString *)str
{
    UILabel *speedLabel = [[UILabel alloc]initWithFrame:CGRectMake( (screenWidth - 74)/2.0 , y, 74, 20)];
    speedLabel.textAlignment = NSTextAlignmentCenter;
    speedLabel.text = str;
    speedLabel.font = Font(13);
    speedLabel.textColor = carScrollColor;
    
    return speedLabel;
    
}

-(UILabel *)backLabel2WithOriginY:(CGFloat)originY andString:(NSString *)str1
{
    
    UILabel *speedLabel = [[UILabel alloc]initWithFrame:CGRectMake( 0 , originY, 108 * kWidth, 20)];
    speedLabel.textAlignment = NSTextAlignmentRight;
    speedLabel.text = str1;
    speedLabel.font = Font(11);
    speedLabel.textColor = carScrollColor;
    
    return speedLabel;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
}
-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
