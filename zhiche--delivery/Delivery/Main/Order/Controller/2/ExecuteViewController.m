//
//  ExecuteViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/27.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ExecuteViewController.h"
#import "CollectionViewCell.h"
#import "CollectionReusableView.h"
#import "PayViewController.h"
#import "Common.h"
#import "WKProgressHUD.h"

//actualCost 实际价格
//depositCost 订金
//unpaidCost 未付金额
//paidCost 以付金额
//needPayCost 需要支付金额


@interface ExecuteViewController ()

@property (nonatomic,strong) NSMutableDictionary *dataDictionary;
@property (nonatomic,strong) WKProgressHUD *hud;


@end

@implementation ExecuteViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.dataDictionary = [NSMutableDictionary dictionary];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self initDataSource];
    [self initCollectionViews];

    self.hud = [WKProgressHUD showInView:self.view withText:@"" animated:YES];

}


-(void)initDataSource
{
    NSString *urlString = [NSString stringWithFormat:@"%@/%ld",order_detail,[self.orderId integerValue]];
    
    [Common requestWithUrlString:urlString contentType:application_json finished:^(id responseObj) {
        
        [self.hud dismiss:YES];
        
        if ([responseObj[@"success"] boolValue]) {
            
            self.dataDictionary = [NSMutableDictionary dictionaryWithDictionary:responseObj[@"data"]];
            
            
//            "loadPics": [{ //提车照片
//                "id": 17,
//                "type": 1010,
//                "typeText": "单据照片",
//                "picKey": "FtZSKtDXcr58SRlZkzZ8-KsqqMjP"
//            }, {
//                "id": 18,
//                "type": 1020,
//                "typeText": "装车照片",
//                "picKey": "FoXOruOT6HaSh4oclUfqLJq9dUc7"
//            }],
//            "unloadPics": [{ //交车照片
//                "id": 19,
//                "type": 2010,
//                "typeText": "单据照片",
//                "picKey": "Fq9lrTuOTcvx_nAyDtW8XKEWav6x"
//            }, {
//                "id": 20,
//                "type": 2020,
//                "typeText": "卸车照片",
//                "picKey": "FtZSKtDXcr58SRlZkzZ8-KsqqMjP"
//            }]
            
            
            if ([self.dataDictionary[@"receiptPics"] count] >0) {
                [self.imageDictionary setObject:self.dataDictionary[@"receiptPics"] forKey:@"提车照片"];
            }
            
            if ([self.dataDictionary[@"receivePics"] count] >0) {
                [self.imageDictionary setObject:self.dataDictionary[@"receivePics"] forKey:@"交车照片"];
            }
            [self getValueString];
            
            [self initCollectionViews];

            
        } else {
            
            [WKProgressHUD popMessage:responseObj[@"message"] inView:self.view duration:1.5 animated:YES];

            
        }
        
    } failed:^(NSString *errorMsg) {
        [self.hud dismiss:YES];
        
        NSLog(@"%@",errorMsg);
    }];
}


-(void)getValueString
{
    /**
     订单
     */
    
    //订单
    self.numberLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"orderCode"]];
    
    //起始地
    
    self.startLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"departCityName"]];
    //起始详情地址
    
    self.startDetailLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"departUnit"]];
    //起始日期
    
    self.startDate.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"deliveryDate"]];
    
    //目的地
    
    self.endLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"receiptCityName"]];
    //
    
    self.endDetailLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"receiptUnit"]];
    self.endDateLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"arriveDate"]];
    
    //车辆
    
    self.carLabel.text = @"";
    
    for (int i = 0; i < [self.dataDictionary[@"vehicles"] count]; i ++) {
        NSString *string = [NSString stringWithFormat:@"%@-%@-%@辆",self.dataDictionary[@"vehicles"][i][@"brandName"],self.dataDictionary[@"vehicles"][i][@"vehicleName"],self.dataDictionary[@"vehicles"][i][@"amount"]];
        
        self.carLabel.text = [self.carLabel.text stringByAppendingString:string];
        self.carLabel.text = [self.carLabel.text stringByAppendingString:@"   "];
        
    }
    
    [self.carLabel sizeToFit];
    self.carLabel.frame = CGRectMake(self.carLabel.frame.origin.x, self.carLabel.frame.origin.y, self.carLabel.frame.size.width, cellHeight);
    
    self.scrollV.contentSize = CGSizeMake(self.carLabel.frame.size.width, cellHeight);
    
    /**
     承运人
     */
    
    
//        //行程
//         self.distanceLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"quotes"][0][@"destination"]];
    
            //姓名
            self.nameLabel.text = [self backString:self.dataDictionary[@"driverInfo"][@"name"]];
            //车牌
            self.carNumberLabel.text = [self backString:self.dataDictionary[@"driverInfo"][@"truckNo"]];
            
            //        //电话
            self.phoneLabel.text = [self backString:self.dataDictionary[@"driverInfo"][@"phone"]];
            
            //车型
            self.carStyleLabel.text = [self backString:self.dataDictionary[@"driverInfo"][@"truckTypeName"]];
            
            
            //已支付
            self.payLabel.text = [NSString stringWithFormat:@"￥%@",self.dataDictionary[@"actualCost"]];
    

  }

-(NSString *)backStringWith:(NSString *)string
{
    
    if ([string isEqual:[NSNull null]]) {

        return @"";
        
    } else {
        return  string;
    }

}


-(void)initCollectionViews
{
    
    CGFloat height;
    
    if ([[self.imageDictionary allKeys]count ] > 0) {
        
        height = screenHeight  + [[self.imageDictionary allKeys]count ] * (cellHeight + 130 );
        
    } else {
        height = screenHeight ;
    }
    
    self.scrollView.contentSize = CGSizeMake(screenWidth, height);
    

    //重新更新UI的frame
    self.collectionView.frame = CGRectMake(0, CGRectGetMaxY(self.priceView.frame) + 10, screenWidth, [[self.imageDictionary allKeys]count ] * (cellHeight + 130 )) ;
    [self.collectionView reloadData];
    
    UIButton *confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [confirmButton setFrame:CGRectMake(20, CGRectGetMaxY(self.collectionView.frame) + 30, screenWidth - 40, Button_Height)];
    [confirmButton setTitle:@"立即评价" forState:UIControlStateNormal];
    [confirmButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    confirmButton.backgroundColor = YellowColor;
    [confirmButton addTarget:self action:@selector(confirmbutton) forControlEvents:UIControlEventTouchUpInside];
    confirmButton.layer.cornerRadius = 5;
    
//    [self.scrollView addSubview:confirmButton];

}

-(void)confirmbutton
{
    
}



-(void)payButtonAction
{

    PayViewController *payVC = [[PayViewController alloc]init];
    payVC.orderid = self.orderId;
    [self.navigationController pushViewController:payVC animated:YES];
}


-(NSString *)backDateWithString:(NSString *)string
{
    NSDate *nd = [NSDate dateWithTimeIntervalSince1970:[string integerValue]/1000];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:nd];
    
    return dateString;
}

-(NSString *)backString:(NSString *)string
{
    
    if ([string isEqual:[NSNull null]]) {
        return @"";
    } else {
        return string;
    }
}

-(NSString *)backMoney:(NSString *)string
{
    NSString   *str = [NSString stringWithFormat:@"%@",string];
    
    if ([str isEqual:[NSNull null]]) {
        return @"";
    } else {
        return [NSString stringWithFormat:@"￥%@",str];
    }
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
