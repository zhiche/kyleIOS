//
//  OrderCommentVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 17/9/1.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#define leftMargn 18
#define labelHeight 15 * kHeight
#define labelWidth 60 * kWidth

#import "OrderCommentVC.h"
#import "StarCell.h"
#import "RootViewController.h"
#import "Header.h"
#import "Common.h"
#import <Masonry.h>
#import "MovieStar.h"
#import "WKProgressHUD.h"
@interface OrderCommentVC ()<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UIScrollViewDelegate>
{
    UIImageView * nav;
    UITableView * TableView;
    NSMutableArray * StarArr;
    Common * com;
    RootViewController * TabBar;

}

@property (nonatomic,strong) UITextView * field;
@property (nonatomic,strong) UIView *orderView;
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UIScrollView *scrollView;

@property (nonatomic,strong) UILabel *numberLabel;//订单编号
@property (nonatomic,strong) UILabel *statusLabel;//状态
@property (nonatomic,strong) UILabel *startLabel;//起始地
//@property (nonatomic,strong) UILabel *startDetailLabel;//详细地址
@property (nonatomic,strong) UILabel *startDate;//起始日期
@property (nonatomic,strong) UILabel *endLabel;//终止地址
//@property (nonatomic,strong) UILabel *endDetailLabel;//详细地址
@property (nonatomic,strong) UILabel *endDateLabel;//终止日期



@property (nonatomic,strong) NSString * comment;
@property (nonatomic,strong) NSString * starString;
@property (nonatomic,strong) UILabel * textNumber;
@property (nonatomic,strong) NSMutableArray * starArr;
@property (nonatomic,strong) NSMutableArray * commentArr;


@property (nonatomic,strong) NSMutableArray * imageArr;
@property (nonatomic,strong) NSMutableArray * labelArr;
@property (nonatomic,strong) NSMutableArray * postArr;


@end

@implementation OrderCommentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    nav = [self createNav:@"订单评价"];
    [self.view addSubview:nav];
    
    
    self.view.backgroundColor = GrayColor;
    
    TabBar = [RootViewController defaultsTabBar];

    
    com = [[Common alloc]init];
    
    self.imageArr = [[NSMutableArray alloc]init];
    self.labelArr = [[NSMutableArray alloc]init];
    self.starArr = [[NSMutableArray alloc]init];
    self.commentArr = [[NSMutableArray alloc]init];
    self.postArr = [[NSMutableArray alloc]init];
    
    
    [self initTopView];
    
    

    
    
    NSString * stringUrl = [NSString stringWithFormat:@"%@?queryByOrderId",valuationStatus_Url];
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    [dic setObject:self.orderId forKey:@"orderId"];
    [com afPostRequestWithUrlString:stringUrl parms:dic finishedBlock:^(id responseObj) {
        NSMutableDictionary * dicObj =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];

        NSLog(@"%@",dicObj);

        if ([dicObj[@"success"] boolValue]) {
          
            NSMutableArray * arr = dicObj[@"data"];
            for (int i = 0; i<[arr count]; i++) {
                
                NSString * type = [NSString stringWithFormat:@"%@",arr[i][@"type"]];
                if ([type isEqualToString:@"10"]) {
                    [self.starArr addObject:arr[i]];
                }else{
                    [self.commentArr addObject:arr[i]];
                }
            }
        
            if (self.isValuation == 1) {
                self.field.text = self.commentArr[0][@"resultValue"];

                UIImageView * image1 = self.imageArr[0];
                UIImageView * image2 = self.imageArr[1];
                UIImageView * image3 = self.imageArr[2];
                UILabel * label1 = self.labelArr[0];
                UILabel * label2 = self.labelArr[1];
                UILabel * label3 = self.labelArr[2];

                int resultValue = [self.commentArr[1][@"resultValue"] intValue];
                switch (resultValue) {
                    case 10:
                        image1.image = [UIImage imageNamed:@"starPress"];
                        image2.image = [UIImage imageNamed:@"UnstarPress"];
                        image3.image = [UIImage imageNamed:@"UnstarPress"];
                        label1.textColor = RedColor;
                        label2.textColor = CommentTitleColor;
                        label3.textColor = CommentTitleColor;
                        break;
                    case 20:
                        image1.image = [UIImage imageNamed:@"UnstarPress"];
                        image2.image = [UIImage imageNamed:@"starPress"];
                        image3.image = [UIImage imageNamed:@"UnstarPress"];
                        label1.textColor = CommentTitleColor;
                        label2.textColor = RedColor;
                        label3.textColor = CommentTitleColor;
                        break;
                    case 30:
                        image1.image = [UIImage imageNamed:@"UnstarPress"];
                        image2.image = [UIImage imageNamed:@"UnstarPress"];
                        image3.image = [UIImage imageNamed:@"starPress"];
                        label1.textColor = CommentTitleColor;
                        label2.textColor = CommentTitleColor;
                        label3.textColor = RedColor;
                        break;
                    default:
                        break;
                }
            }else{
                self.starString = @"30";
            }

            [TableView reloadData];
        
        }

    } failedBlock:^(NSString *errorMsg) {

    }];
    [self initTable];

    [self getValueString];
}


-(void)getValueString
{
    
    //订单号
    self.numberLabel.text = [NSString stringWithFormat:@"%@",_dataDictionary[@"orderCode"]];
    //开始日期
    self.startDate.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"deliveryDate"]];
    
    //结束日期
    self.endDateLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"arriveDate"]];
    
    //起点
    self.startLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"orgin"]];
    //终点
    self.endLabel.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"dest"]];
    
    
}


-(void)initTopView
{
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight)];
    self.scrollView.contentSize = CGSizeMake(screenWidth, screenHeight);
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = GrayColor;
    
    
    /**
     
     订单信息
     */
    self.orderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 106 * kHeight)];
    [self.scrollView addSubview:self.orderView];
    self.orderView.backgroundColor = WhiteColor;
    //订单号
    
    UIView *orderV1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight)];
    [self.orderView addSubview:orderV1];
    orderV1.backgroundColor = headerBottomColor;
    
    
    
    UILabel *orderL = [self backLabelWithFrame:CGRectMake(leftMargn, 0, 60, cellHeight) andString:@"订单号:"];
    orderL.textColor = littleBlackColor;
    [orderV1 addSubview:orderL];
    
    self.numberLabel = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(orderL.frame) + 5, CGRectGetMinY(orderL.frame), 200, cellHeight)];
    //    self.numberLabel.text = @"123122453464576";
    self.numberLabel.textColor = littleBlackColor;
    [orderV1 addSubview:self.numberLabel];
    
    
    //状态栏
    self.statusLabel = [self backLabelWithFrame:CGRectMake(screenWidth - 14 - 100, CGRectGetMinY(orderL.frame), 100, cellHeight)  andString:@"已交车"];
    self.statusLabel.textColor = RedColor;
    
    self.statusLabel.textAlignment = NSTextAlignmentRight;
    //    [orderView addSubview:self.statusLabel];
    
    
    UILabel *labe = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight, screenWidth, 0.5)];
    labe.backgroundColor = LineGrayColor;
    [orderV1 addSubview:labe];
    
    //起始地址
    self.startLabel = [self backLabelWithFrame:CGRectMake(20 * kWidth - 20,  CGRectGetMaxY(orderV1.frame) + 12*kHeight, labelWidth + 40, labelHeight)];
    self.startLabel.font = Font(12)
    //    self.startLabel.text =  @"阿道夫大风大风";
    self.startLabel.adjustsFontSizeToFitWidth = YES;
    [self.orderView addSubview:self.startLabel];
    
    
    //起始日期
    self.startDate = [self backLabelWithFrame:CGRectMake(20 * kWidth, CGRectGetMaxY(self.startLabel.frame) + 9*kHeight, labelWidth, labelHeight) ];
    //    self.startDate.text = @"2010-1-1";
    self.startDate.textColor = littleBlackColor;
    self.startDate.font = Font(10);
    [self.orderView addSubview:self.startDate];
    
    
    //    //起始详情地址
    //    self.startDetailLabel = [self backLabelWithFrame:CGRectMake(20 * kHeight, CGRectGetMaxY(self.startLabel.frame) , labelWidth, 28 * kHeight)];
    //    self.startDetailLabel.text = @"";
    //    self.startDetailLabel.adjustsFontSizeToFitWidth = YES;
    //    [orderView addSubview:self.startDetailLabel];
    //    self.startDetailLabel.font = Font(9);
    //    self.startDetailLabel.numberOfLines = 0;
    
    
    //图片
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMidX(self.view.frame) - 30, CGRectGetMinY(self.startLabel.frame) + 4.5 * kHeight + 6, 47, 12.5)];
    [self.orderView addSubview:imageView];
    imageView.image = [UIImage imageNamed:@"Quote_Cell_logo"];
    
    
    //目的地
    self.endLabel = [self backLabelWithFrame:CGRectMake(screenWidth - labelWidth  - 20 * kWidth - 20 * kHeight, CGRectGetMaxY(labe.frame) + 12*kHeight, labelWidth + 40, labelHeight) ];
    [self.orderView addSubview:self.endLabel];
    self.endLabel.adjustsFontSizeToFitWidth = YES;
    self.endLabel.font = Font(12);
    
    //目的日期
    self.endDateLabel = [self backLabelWithFrame:CGRectMake(screenWidth - labelWidth  - 20 * kWidth, CGRectGetMaxY(self.endLabel.frame) + 9*kHeight, labelWidth, labelHeight) ];
    [self.orderView addSubview:self.endDateLabel];
    self.endDateLabel.font = Font(10);
    
    
}


-(void)initTable{
    
    

    __weak typeof(self) weakSelf = self;
    
    TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 65*kHeight, Main_Width,Main_height-65*kHeight) style:UITableViewStylePlain];
    [TableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    TableView.scrollEnabled = NO;
    TableView.delegate = weakSelf;
    TableView.dataSource = weakSelf;
    [self.scrollView addSubview:TableView];
    [TableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.scrollView.mas_left);
        make.top.mas_equalTo(self.orderView.mas_bottom).with.offset(7*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 1*45*kHeight));
    }];
    
    
    NSString * str = @"0";
    StarArr = [[NSMutableArray alloc]init];
    [StarArr addObject:str];
    [StarArr addObject:str];
    [StarArr addObject:str];
    
    [TableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(45*(StarArr.count+1)*kHeight);
    }];
    
    
    
    
    _textNumber = [com createUIlabel:@"剩余140字" andFont:FontOfSize12 andColor:littleBlackColor];
    
    self.field =[[UITextView alloc]init];
    //    field.frame = CGRectMake(94*kWidth, 0, Main_Width-200*kWidth, 43*kWidth);
    self.field.frame = CGRectMake(14*kWidth, 64*kHeight, Main_Width-28*kWidth, 100*kWidth);
    self.field.delegate = self;
    self.field.userInteractionEnabled = YES;
    
    //    field.backgroundColor = GrayColor;
    self.field.backgroundColor = WhiteColor;
    self.field.font = Font(FontOfSize12);
    self.field.textAlignment = NSTextAlignmentLeft;
    self.field.returnKeyType = UIReturnKeyDone;
    
    if (self.isValuation != 1) {
        self.field.text = @"关于订单的评论信息，请在这里留言（限140字）。";
        self.comment = @"";
    }else{
        _textNumber.text = @"";
        self.field.userInteractionEnabled = NO;
    }
    
    [self.scrollView addSubview:self.field];
    
    [self.field mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).with.offset(0*kWidth);
        make.top.mas_equalTo(TableView.mas_bottom).with.offset(7*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width-0*kWidth, 100*kHeight));
    }];
    
    [self.scrollView addSubview:_textNumber];
    //    _textNumber.backgroundColor = [UIColor cyanColor];
    [_textNumber mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.field.mas_right).offset(-5);
        make.bottom.mas_equalTo(self.field.mas_bottom).offset(-5);
        
    }];
    
    
    UIImageView * image1 = [[UIImageView alloc]init];
    image1.image = [UIImage imageNamed:@"UnstarPress"];
    [self.scrollView addSubview:image1];
    UILabel * label1 = [com createUIlabel:@"差评" andFont:12 andColor:CommentTitleColor];
    [self.scrollView addSubview:label1];
    
    [image1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).with.offset(45*kWidth);
        make.top.mas_equalTo(self.field.mas_bottom).with.offset(20*kHeight);
        make.size.mas_equalTo(CGSizeMake(15*kWidth, 14*kHeight));
    }];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(image1.mas_right).with.offset(10*kWidth);
        make.centerY.mas_equalTo(image1.mas_centerY);
    }];

    
    
    
    
    UIImageView * image2 = [[UIImageView alloc]init];
    image2.image = [UIImage imageNamed:@"UnstarPress"];
    [self.scrollView addSubview:image2];
    UILabel * label2 = [com createUIlabel:@"中评" andFont:12 andColor:CommentTitleColor];
    [self.scrollView addSubview:label2];
    
    [image2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.view.mas_centerX).with.offset(-5*kWidth);
        make.top.mas_equalTo(self.field.mas_bottom).with.offset(20*kHeight);
        make.size.mas_equalTo(CGSizeMake(15*kWidth, 14*kHeight));
    }];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_centerX).with.offset(5*kWidth);
        make.centerY.mas_equalTo(image1.mas_centerY);
    }];

    
    

    UIImageView * image3 = [[UIImageView alloc]init];
    image3.image = [UIImage imageNamed:@"starPress"];
    [self.scrollView addSubview:image3];
    UILabel * label3 = [com createUIlabel:@"好评" andFont:12 andColor:RedColor];
    [self.scrollView addSubview:label3];
    
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.view.mas_right).with.offset(-45*kWidth);
        make.centerY.mas_equalTo(image1.mas_centerY);
    }];

    [image3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(label3.mas_left).with.offset(-10*kWidth);
        make.centerY.mas_equalTo(image1.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(15*kWidth, 14*kHeight));
    }];

    
    
    [self.imageArr addObject:image1];
    [self.imageArr addObject:image2];
    [self.imageArr addObject:image3];

    [self.labelArr addObject:label1];
    [self.labelArr addObject:label2];
    [self.labelArr addObject:label3];

    
    
    
    
    UIButton * btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.tag = 100;
    [btn1 addTarget:self action:@selector(pressBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:btn1];
    [btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).with.offset(20*kWidth);
        make.centerY.mas_equalTo(image1.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(70*kWidth, 40*kHeight));
    }];

    
    
    
    UIButton * btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.tag = 200;
    [btn2 addTarget:self action:@selector(pressBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:btn2];
    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.centerY.mas_equalTo(image1.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(70*kWidth, 40*kHeight));
    }];

    
    
    UIButton * btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn3.tag = 300;

    [btn3 addTarget:self action:@selector(pressBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:btn3];
    [btn3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.view.mas_right).with.offset(-20*kWidth);
        make.centerY.mas_equalTo(image1.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(70*kWidth, 40*kHeight));
    }];

    
    
    
    
    UIButton * CommentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [CommentBtn setTitle:@"提交评论" forState:UIControlStateNormal];
    [CommentBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
    [CommentBtn addTarget:self action:@selector(pressCommentBtn:) forControlEvents:UIControlEventTouchUpInside];
    CommentBtn.layer.cornerRadius = 5;
    CommentBtn.layer.borderWidth = 0.5;
    CommentBtn.layer.borderColor = YellowColor.CGColor;
    CommentBtn.backgroundColor = YellowColor;
    [self.scrollView addSubview:CommentBtn];
    [CommentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(TableView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(293*kWidth, 40*kHeight));
        make.top.mas_equalTo(btn3.mas_bottom).with.offset(10*kHeight);
    }];

    if (self.isValuation == 1) {
        self.scrollView.userInteractionEnabled = NO;
        CommentBtn.hidden = YES;
        
        btn1.userInteractionEnabled = NO;
        btn2.userInteractionEnabled = NO;
        btn3.userInteractionEnabled = NO;

    }

    
    
}


-(void)pressBtn:(UIButton*)sender{
    
    
    switch (sender.tag) {
        case 100:{
            
            UIImageView * image1 = self.imageArr[0];
            UIImageView * image2 = self.imageArr[1];
            UIImageView * image3 = self.imageArr[2];
            UILabel * label1 = self.labelArr[0];
            UILabel * label2 = self.labelArr[1];
            UILabel * label3 = self.labelArr[2];

            image1.image = [UIImage imageNamed:@"starPress"];
            image2.image = [UIImage imageNamed:@"UnstarPress"];
            image3.image = [UIImage imageNamed:@"UnstarPress"];
            label1.textColor = RedColor;
            label2.textColor = CommentTitleColor;
            label3.textColor = CommentTitleColor;
//            "comment": "单选，10:差评、20:中评、30:好评",

            self.starString = @"10";
        }
            
            break;
        case 200:{
            UIImageView * image1 = self.imageArr[0];
            UIImageView * image2 = self.imageArr[1];
            UIImageView * image3 = self.imageArr[2];
            UILabel * label1 = self.labelArr[0];
            UILabel * label2 = self.labelArr[1];
            UILabel * label3 = self.labelArr[2];
            
            image1.image = [UIImage imageNamed:@"UnstarPress"];
            image2.image = [UIImage imageNamed:@"starPress"];
            image3.image = [UIImage imageNamed:@"UnstarPress"];
            label1.textColor = CommentTitleColor;
            label2.textColor = RedColor;
            label3.textColor = CommentTitleColor;
            self.starString = @"20";


        }
            
            break;
        case 300:{
         
            UIImageView * image1 = self.imageArr[0];
            UIImageView * image2 = self.imageArr[1];
            UIImageView * image3 = self.imageArr[2];
            UILabel * label1 = self.labelArr[0];
            UILabel * label2 = self.labelArr[1];
            UILabel * label3 = self.labelArr[2];
            
            image1.image = [UIImage imageNamed:@"UnstarPress"];
            image2.image = [UIImage imageNamed:@"UnstarPress"];
            image3.image = [UIImage imageNamed:@"starPress"];
            label1.textColor = CommentTitleColor;
            label2.textColor = CommentTitleColor;
            label3.textColor = RedColor;
            self.starString = @"30";

        }
            
            break;

            
        default:
            break;
    }
    
    
    
}



-(void)pressCommentBtn:(UIButton*)sender{
    
    
    
    NSLog(@"提交评论");
    
    [self createOrderUIAlertController:@"是否提交评论?"];
    
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:@"关于订单的评论信息，请在这里留言（限140字）。"]) {
        
        textView.text = @"";
        
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.scrollView.contentOffset = CGPointMake(0, 110*kHeight);
    } completion:nil];

    
    
}

-(void)textViewDidChange:(UITextView *)textView{
    
    
    NSInteger number = [textView.text length];
    _comment = textView.text;
    
    
    
    if (number > 140) {
        
        number = 140;
        textView.text = [textView.text substringToIndex:number];
        [self createUIAlertController:@"评论限140字"];
    }
    _textNumber.text = [NSString stringWithFormat:@"剩余%ld字",140-number];
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (textView.text.length == 0) {
        
        textView.text = @"关于订单的评论信息，请在这里留言（限140字）。";
        
    }
    [UIView animateWithDuration:0.3 animations:^{
        self.scrollView.contentOffset = CGPointMake(0, 0*kHeight);
    } completion:nil];

    
}



-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    static NSString *str=@"str";
    StarCell * cell =  [TableView dequeueReusableCellWithIdentifier:str];

    if (cell ==nil) {
        cell = [[StarCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.starLabel.text = _starArr[indexPath.row][@"name"];
    
    __weak StarCell *weakCell = cell;

    
    NSString * number = [self backString:_starArr[indexPath.row][@"resultValue"] ];

    NSString * resultValue = [self backString:_starArr[indexPath.row][@"resultValue"] ];
    NSString * orderId = [self backString:_starArr[indexPath.row][@"orderId"] ];
    NSString * itemId = [self backString:_starArr[indexPath.row][@"itemId"] ];
 
//    "orderId": 1773,
//    "itemId": "5",
//    "resultValue": "30"


    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    [dic setObject:resultValue forKey:@"resultValue"];
    [dic setObject:orderId forKey:@"orderId"];
    [dic setObject:itemId forKey:@"itemId"];
    [self.postArr addObject:dic];
    
    

    NSInteger Num = [number integerValue];
    
    NSLog(@"%ld",(long)Num);
    
    MovieStar * star = [[MovieStar alloc]initWithFrame:CGRectMake( CGRectGetMaxX(cell.textLabel.frame) + 115, CGRectGetMaxY(cell.textLabel.frame) ,150, 22) andCount:Num];
    [cell.contentView addSubview:star];
    
    [star mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(cell.contentView.mas_centerY);
        make.left.mas_equalTo(cell.starLabel.mas_right).with.offset(14*kWidth);
        make.size.mas_equalTo(CGSizeMake(150*kWidth, 22*kHeight));
    }];

    switch (Num) {
        case 0:
            weakCell.stateLabel.text = @"";
            break;
        case 1:
            weakCell.stateLabel.text = @"非常差";
            break;
        case 2:
            weakCell.stateLabel.text = @"差";
            break;
        case 3:
            weakCell.stateLabel.text = @"一般";
            break;
        case 4:
            weakCell.stateLabel.text = @"好";
            break;
        case 5:
            weakCell.stateLabel.text = @"非常好";
            break;
            
        default:
            break;
    }


    
    star.call_backValue = ^(NSInteger a){
        
        
        NSString * string = [NSString stringWithFormat:@"%ld",(long)a];
        [StarArr replaceObjectAtIndex:indexPath.row withObject:string];


        switch (a) {
            case 1:
                weakCell.stateLabel.text = @"非常差";
                [self.postArr[indexPath.row] setObject:@"1" forKey:@"resultValue"];
                break;
            case 2:
                weakCell.stateLabel.text = @"差";
                [self.postArr[indexPath.row] setObject:@"2" forKey:@"resultValue"];

                break;
            case 3:
                weakCell.stateLabel.text = @"一般";
                [self.postArr[indexPath.row] setObject:@"3" forKey:@"resultValue"];

                break;
            case 4:
                weakCell.stateLabel.text = @"好";
                [self.postArr[indexPath.row] setObject:@"4" forKey:@"resultValue"];

                break;
            case 5:
                weakCell.stateLabel.text = @"非常好";
                [self.postArr[indexPath.row] setObject:@"5" forKey:@"resultValue"];

                break;

            default:
                break;
        }
        
    };

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"%ld",(long)indexPath.row);
    NSLog(@"%@",StarArr);
    
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45*kHeight;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _starArr.count;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = GrayColor;
    
    if (section == 0) {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, 100, cellHeight)];
        label.text = @"服务评价";
        label.textColor = littleBlackColor;
        label.font = Font(14);
        [view addSubview:label];
        
        UILabel *lineL = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight - 0.5, screenWidth, 0.5)];
        lineL.backgroundColor = LineGrayColor;
        //        [view addSubview:lineL];
        
        view.backgroundColor = WhiteColor;
        
    }
    
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return  cellHeight;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)createOrderUIAlertController:(NSString*)title{
    
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"提交" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    
        [self postDate];

    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];

    
}


-(void)postDate{
    
    
//    "orderId": 1773,
//    "itemId": "5",
//    "resultValue": "30"

    
    for (int i = 0; i<self.postArr.count; i++) {
        
        NSString * string = [NSString stringWithFormat:@"%@",self.postArr[i][@"resultValue"]];
        
        if (string.length == 0) {
            
            [self.postArr[i] setObject:@"0" forKey:@"resultValue"];
            
        }
        
        
    }
    
    
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    [dic setObject:self.commentArr[0][@"orderId"] forKey:@"orderId"];
    [dic setObject:self.commentArr[0][@"itemId"] forKey:@"itemId"];
    [dic setObject:self.comment forKey:@"resultValue"];

    
    NSMutableDictionary * dic1 = [[NSMutableDictionary alloc]init];
    [dic1 setObject:self.commentArr[1][@"orderId"] forKey:@"orderId"];
    [dic1 setObject:self.commentArr[1][@"itemId"] forKey:@"itemId"];
    [dic1 setObject:self.starString forKey:@"resultValue"];

    
    
    [self.postArr addObject:dic];
    [self.postArr addObject:dic1];

    
    
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:self.postArr options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"%@",jsonString);
    WKProgressHUD * hud = [WKProgressHUD showInView:self.view withText:@"请稍等" animated:YES];
    
    NSMutableDictionary * postdic = [[NSMutableDictionary alloc]init];
    [postdic setObject:jsonString forKey:@"lists"];
    
    [com afPostRequestWithUrlString:saveValuation_Url parms:postdic finishedBlock:^(id responseObj) {
        NSMutableDictionary * dicObj =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];
        
        if ([dicObj[@"success"] boolValue]) {
            [WKProgressHUD showInView:self.view withText:dic[@"message"] animated:YES];
            [hud dismiss:YES];
            
            //延迟执行
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [self.navigationController popViewControllerAnimated:YES];
            });
        };
    } failedBlock:^(NSString *errorMsg) {
        [hud dismiss:YES];

    }];


}


-(void)createUIAlertController:(NSString*)title
{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}


-(UILabel *)backLabelWithFrame:(CGRect)frame andString:(NSString *)string
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.text = string;
    label.textColor = fontGrayColor;
    label.font = Font(13);
    return label;
    
}

-(UILabel *)backLabelWithFrame:(CGRect)frame
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.textColor = littleBlackColor;
    label.font = Font(12);
    label.textAlignment = NSTextAlignmentCenter;
    
    return label;
    
}

-(NSString *)backString:(NSString *)string
{
    //    NSString * str = [NSString stringWithFormat:@"%@",string];
    if ([string isEqual:[NSNull null]]) {
        return @"";
    } else {
        return string;
    }
}




- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ( [ @"\n" isEqualToString: text])
    {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            [textView resignFirstResponder];
            
        }];
        
        return  NO;
    }
    else
    {
        return YES;
    }
}



-(void)viewWillAppear:(BOOL)animated{
    [TabBar setTabBarHidden:YES];
}

@end
