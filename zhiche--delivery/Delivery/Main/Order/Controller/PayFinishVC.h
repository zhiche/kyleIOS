//
//  PayFinishVC.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/7/16.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayFinishVC : UIViewController
@property (nonatomic,copy) NSString *orderN;
@property (nonatomic,copy) NSString *moneyS;
@end
