//
//  PayViewController.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/28.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

//#import <UIKit/UIKit.h>
#import "OrderRootViewController.h"

@interface PayViewController : UIViewController

@property (nonatomic,copy) NSString *orderid;
@property (nonatomic,strong) NSString *orderNumber;
@property  (nonatomic,copy) NSString *moneyString;
@property (nonatomic)BOOL isScore;
@property (nonatomic,strong) NSString * quoteid;
@end
