//
//  DepartModel.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/19.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "DepartModel.h"

@implementation DepartModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key  {
    if([key isEqualToString:@"id"])
        self.ID = value;
}
@end
