//
//  DepartModel.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/19.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DepartModel : NSObject
@property (nonatomic,copy) NSString *address;
@property (nonatomic,copy) NSString *addressType;
@property (nonatomic,copy) NSString *cityName;
@property (nonatomic,copy) NSString *cityCode;
@property (nonatomic,copy) NSString *comment;
@property (nonatomic,copy) NSString *contact;//用户名
@property (nonatomic,copy) NSString *countyCode;
@property (nonatomic,copy) NSString *countyName;
@property (nonatomic,copy) NSString *createTime;
@property (nonatomic,copy) NSString *isDefault;
@property (nonatomic,copy) NSString *phone;
@property (nonatomic,copy) NSString *provinceCode;
@property (nonatomic,copy) NSString *provinceName;
@property (nonatomic,copy) NSString *unitName;
@property (nonatomic,copy) NSString *updateTime;
@property (nonatomic,copy) NSString *userId;
@property (nonatomic,copy) NSString *ID;

@end
