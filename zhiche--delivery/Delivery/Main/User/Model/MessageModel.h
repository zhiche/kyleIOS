//
//  MessageModel.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/31.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageModel : NSObject
//id = 5;
//msgClass = 10;
//msgContent = "\U8ba2\U5355\U53d1\U5e03";
//msgTime = 1464675731000;
//msgType = 1010;
//orderId = 10;
//userId = 10;
//userName = Aaron;

@property (nonatomic,copy) NSString *ID;
@property (nonatomic,copy) NSString *msgContent;
@property (nonatomic,copy) NSString  *msgClass;
@property (nonatomic,copy) NSString  *msgTime;
@property (nonatomic,copy) NSString  *msgType;
@property (nonatomic,copy) NSString  *orderId;
@property (nonatomic,copy) NSString  *userId;
@property (nonatomic,copy) NSString  *userName;

@end
