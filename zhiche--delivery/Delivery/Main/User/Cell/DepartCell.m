//
//  DepartCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "DepartCell.h"
#import <Masonry.h>

@implementation DepartCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        
        
        [self initSubViews];
        
    }
    
    return  self;
}

-(void)initSubViews
{
  
    __weak typeof(self) weakSelf = self;

    
    //起始地址
    self.startLabel = [[UILabel alloc]initWithFrame:CGRectMake(18, 14, 100 , 15 * kHeight)];
    [self.contentView addSubview:self.startLabel];

    self.startLabel.font = Font(13);
    self.startLabel.textColor = BlackTitleColor;
    self.startLabel.text = @"";
    
    
    //起始详情地址
    self.addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.startLabel.frame) + 14, CGRectGetMinY(self.startLabel.frame), 200, CGRectGetHeight(self.startLabel.frame))];
    [self.contentView addSubview:self.addressLabel];

    self.addressLabel.font = Font(13);
    self.addressLabel.textColor = BlackTitleColor;
    self.addressLabel.text = @"";

    
    //详细地址
    
    self.addressDetailLabel = [[UILabel alloc]initWithFrame:CGRectMake(18, CGRectGetMaxY(self.startLabel.frame) + 7*kHeight, screenWidth - 35, CGRectGetHeight(self.startLabel.frame))];
    [self.contentView addSubview:self.addressDetailLabel];

    self.addressDetailLabel.font = Font(11);
    self.addressDetailLabel.textColor = littleBlackColor;
    self.addressDetailLabel.text = @"";
    
    
    //姓名 电话
    self.infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(18, CGRectGetMaxY(self.addressDetailLabel.frame) + 7*kHeight, 60, CGRectGetHeight(self.startLabel.frame))];
    [self.contentView addSubview:self.infoLabel];

    self.infoLabel.font = Font(11);
    self.infoLabel.textColor = littleBlackColor;
    self.infoLabel.text = @"";
    
    self.phoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.infoLabel.frame) + 14, CGRectGetMinY(self.infoLabel.frame), 200, CGRectGetHeight(self.startLabel.frame))];
    [self.contentView addSubview:self.phoneLabel];

    self.phoneLabel.textColor = YellowColor;
    self.phoneLabel.font = Font(11);
    
    
    
    UILabel *lineL3 = [[UILabel alloc]init];
    [self.contentView addSubview:lineL3];
    [lineL3 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(weakSelf.infoLabel.mas_bottom).offset(7 * kHeight);
        make.centerX.equalTo(weakSelf.contentView);
        make.size.mas_equalTo(CGSizeMake(screenWidth , 0.5));
    }];
    
    lineL3.backgroundColor = GrayColor;
    
    //删除按钮
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.deleteButton setTitle:@"删除" forState:UIControlStateNormal];
    [self.deleteButton setTitleColor:littleBlackColor forState:UIControlStateNormal];
    [self.contentView addSubview:self.deleteButton];
    self.deleteButton.backgroundColor = GrayColor;
    [self.deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(lineL3.mas_bottom).offset(9 * kHeight);
        make.size.mas_equalTo(CGSizeMake(70 * kWidth, 25 * kHeight));
        
    }];
    self.deleteButton.titleLabel.font = Font(13);
    self.deleteButton.layer.borderColor = RGBACOLOR(186, 186, 186, 1).CGColor;
    self.deleteButton.layer.borderWidth = 0.5;
    self.deleteButton.layer.cornerRadius = 5;
    
    
    //编辑按钮
    self.editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.editButton setTitle:@"编辑" forState:UIControlStateNormal];
    [self.editButton setTitleColor:YellowColor forState:UIControlStateNormal];
//    self.editButton.backgroundColor = rearrangeBtnColor;
    [self.contentView addSubview:self.editButton];
    [self.editButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(weakSelf.deleteButton.mas_left).offset(-12);

        make.centerY.equalTo(weakSelf.deleteButton);
        make.size.mas_equalTo(CGSizeMake(70 * kWidth, 25 * kHeight));
        
    }];
    self.editButton.titleLabel.font = Font(13);
    self.editButton.layer.borderWidth = 0.5;
    self.editButton.layer.borderColor = YellowColor.CGColor;
    self.editButton.layer.cornerRadius = 5;
    
    
    //设为默认
    self.defaultButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.defaultButton setBackgroundImage:[UIImage imageNamed:@"regist_unselected"] forState:UIControlStateNormal];
    [self.defaultButton setBackgroundImage:[UIImage imageNamed:@"regist_selected"] forState:UIControlStateSelected];
    
    
    [self.contentView addSubview:self.defaultButton];
    [self.defaultButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(18);
        make.centerY.equalTo(weakSelf.deleteButton);
        make.size.mas_equalTo(CGSizeMake(14.5, 14.5));
        
    }];

    UILabel *label = [[UILabel alloc]init];
    [self.contentView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(weakSelf.defaultButton.mas_right).offset(7);
        make.centerY.equalTo(weakSelf.deleteButton);
        make.size.mas_equalTo(CGSizeMake(150, 25 * kHeight));
        
    }];
    
    label.textColor = littleBlackColor;
    label.font = Font(11);
    label.text = @"设为默认地址";
    
    
}

-(UILabel *)backLabelWithFrame:(CGRect)frame andFont:(int)a
{
    UILabel *label = [[UILabel alloc]init];
    label.frame = frame;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = Font(a);
    [self.contentView addSubview:label];
    
    return  label;
}

-(void)setModel:(DepartModel *)model
{

    
    if (model != _model) {
        
        if ([model.isDefault boolValue]) {
            
            self.defaultButton.selected = YES;

        } else {
            
            self.defaultButton.selected = NO;
        }
        
//        NSLog(@"%@ %@ %@",model.provinceName,model.cityName,model.countyName);
        
        //拼接起始地地址
        NSString *statString;
        if ([model.provinceName isEqualToString:model.cityName]) {
           statString = [NSString stringWithFormat:@"%@-%@",model.provinceName,model.countyName];
 
        } else {
            statString = [NSString stringWithFormat:@"%@-%@-%@",model.provinceName,model.cityName,model.countyName];
 
        }
        
        self.startLabel.text = statString;
        [self.startLabel sizeToFit];
        self.addressLabel.frame = CGRectMake(CGRectGetMaxX(self.startLabel.frame) + 14, self.addressLabel.frame.origin.y, self.addressLabel.frame.size.width, self.addressLabel.frame.size.height);
        
        //拼接address
//        NSString *addressString = [NSString stringWithFormat:@"%@",model.unitName];
        
//        self.addressLabel.text = addressString;
        
        self.addressDetailLabel.text = model.address;
        
        NSString *infoString = [NSString stringWithFormat:@"%@",model.contact];
        
        self.infoLabel.text = infoString;
        [self.infoLabel sizeToFit];
        self.phoneLabel.frame = CGRectMake(CGRectGetMaxX(self.infoLabel.frame) + 14, self.phoneLabel.frame.origin.y, self.phoneLabel.frame.size.width, self.phoneLabel.frame.size.height);
        
        self.phoneLabel.text = [NSString stringWithFormat:@"%@",model.phone];
        
             
    }
    
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
