//
//  DepartCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DepartModel.h"

@interface DepartCell : UITableViewCell

@property (nonatomic,strong) UILabel *startLabel;
@property (nonatomic,strong) UILabel *addressLabel;
@property (nonatomic,strong) UILabel *addressDetailLabel;
@property (nonatomic,strong) UILabel *infoLabel;
@property (nonatomic,strong) UILabel *phoneLabel;

@property (nonatomic,strong) UIButton *deleteButton;
@property (nonatomic,strong) UIButton *defaultButton;
@property (nonatomic,strong) UIButton *editButton;

@property (nonatomic,strong) DepartModel *model;

@end
