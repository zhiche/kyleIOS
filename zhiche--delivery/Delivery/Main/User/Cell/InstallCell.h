//
//  InstallCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InstallCell : UITableViewCell

@property (nonatomic,strong) UIView *bottomView;

@property (nonatomic,strong) UIImageView *pictureImageV;//头像
@property (nonatomic,strong) UIButton *pictureButton;
@property (nonatomic,strong) UILabel *scoreL;//积分
@property (nonatomic,strong) UILabel *nameLabel;//账户名

@property (nonatomic,strong) UILabel *paymentL;//待付款
@property (nonatomic,strong) UILabel *responseL;//待应答
@property (nonatomic,strong) UILabel *putL;//待提车
@property (nonatomic,strong) UILabel *giveL;//待交车
@property (nonatomic,strong) UILabel *confirmL;//待回单



@end
