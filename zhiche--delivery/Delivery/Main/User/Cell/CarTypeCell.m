//
//  CarTypeCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "CarTypeCell.h"
#import <Masonry.h>

@implementation CarTypeCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        
        
        [self initSubViews];
        
    }
    
    return  self;
}

-(void)initSubViews
{
    
    __weak typeof(self)  weakSelf = self;
    
    self.logoImageV = [[UIImageView alloc]init];
    [self.contentView addSubview:self.logoImageV];

    [self.logoImageV mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(18);
        make.centerY.equalTo(weakSelf.contentView);
        make.size.mas_equalTo(CGSizeMake(40, 40));
        
    }];
    self.logoImageV.contentMode = UIViewContentModeScaleAspectFit;//图片缩放适应imageView大小
    
    self.brandLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.brandLabel];
    [self.brandLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(weakSelf.logoImageV.mas_right).offset(20);
        make.top.mas_equalTo(10 * kHeight);
        make.size.mas_equalTo(CGSizeMake(200 * kWidth, 15 * kHeight));
        
    }];
    self.brandLabel.font = Font(13);
    self.brandLabel.text = @"宝马－X5";
    self.brandLabel.textAlignment = NSTextAlignmentLeft;
    self.brandLabel.textColor = littleBlackColor;
    
    
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.deleteButton setTitle:@"删除" forState:UIControlStateNormal];
    [self.deleteButton setTitleColor:littleBlackColor forState:UIControlStateNormal];
    self.deleteButton.backgroundColor = GrayColor;
    self.deleteButton.titleLabel.font = Font(13);
    [self.contentView addSubview:self.deleteButton];
    self.deleteButton.layer.borderColor = fontGrayColor.CGColor;
    self.deleteButton.layer.borderWidth = 0.5;
    self.deleteButton.layer.cornerRadius = 5;
    [self.deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.right.mas_equalTo(-18);
        make.centerY.equalTo(weakSelf.contentView);
        make.size.mas_equalTo(CGSizeMake(60 * kWidth, 26 * kHeight));
        
    }];
    
   
    
    self.typeLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.typeLabel];
    [self.typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(weakSelf.logoImageV.mas_right).offset(20);
        make.top.mas_equalTo(weakSelf.brandLabel.mas_bottom).offset(6 * kHeight);
        make.size.mas_equalTo(CGSizeMake(100, 13 * kHeight));
        
    }];
    self.typeLabel.font = Font(10);
    self.typeLabel.text = @"商务轿车";
    self.typeLabel.textAlignment = NSTextAlignmentLeft;
    self.typeLabel.textColor = fontGrayColor;

}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
