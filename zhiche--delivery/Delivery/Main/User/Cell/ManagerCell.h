//
//  ManagerCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/9.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManagerCell : UITableViewCell
@property (nonatomic,strong) UIButton *pictureButton;
@property (nonatomic,strong) UIImageView *pictureImageV;

@end
