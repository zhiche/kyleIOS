//
//  InstallCell1.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/7/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "InstallCell1.h"

@implementation InstallCell1

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        
        
        [self initSubViews];
        
    }
    
    return  self;
}

-(void)initSubViews
{
    
    //头像
    self.pictureButton = [UIButton buttonWithType:UIButtonTypeCustom ];
    self.pictureButton.frame = CGRectMake(18, 14 * kHeight, 50 * kWidth, 50 * kHeight);
    self.pictureButton.backgroundColor = [UIColor clearColor];
    self.pictureButton.layer.cornerRadius = 25 * kWidth;
    [self.contentView addSubview:self.pictureButton];
    
    
   //登录按钮
    self.loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.loginButton.frame = CGRectMake(CGRectGetMaxX(self.pictureButton.frame) + 8, CGRectGetMidY(self.pictureButton.frame) - 15, 100, 30);
    [self.loginButton setTitleColor:littleBlackColor forState:UIControlStateNormal];
    [self.loginButton setTitle:@"登录" forState:UIControlStateNormal];
    self.loginButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

//    self.loginButton.titleLabel.font = Font(14);
    self.loginButton.titleLabel.font = [UIFont boldSystemFontOfSize:14 * kWidth];
    
    [self.contentView addSubview:self.loginButton];
    
    //箭头
    UIImageView *arrowImg = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth - 26 , CGRectGetMidY(self.pictureButton.frame) - 5.5, 6.5, 11)];
    arrowImg.image = [UIImage imageNamed:@"personal_arrow"];
    [self.contentView addSubview:arrowImg];

    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
