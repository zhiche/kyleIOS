//
//  TopBackView.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/11.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopBackView : UIView

@property (nonatomic,strong) UIButton *rightButton;
@property (nonatomic,strong) UIButton *leftButton;

-(id)initViewWithFrame:(CGRect)frame andTitle:(NSString *)string;

@end
