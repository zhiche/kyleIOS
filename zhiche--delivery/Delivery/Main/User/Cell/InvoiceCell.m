//
//  InvoiceCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "InvoiceCell.h"
#import <Masonry.h>

@implementation InvoiceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
        
    }
    
    return  self;
}



-(void)initSubViews
{
    //企业logo
    self.statusImageV = [[UIImageView alloc]initWithFrame:CGRectMake(14, 5, 20, 20)];
    self.statusImageV.layer.cornerRadius = 10;
    self.statusImageV.layer.masksToBounds = YES;
//    self.statusImageV.backgroundColor = [UIColor cyanColor];
    [self.contentView addSubview:self.statusImageV];
    
    //订单编号
    
    UILabel *numberL = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(self.statusImageV.frame) , 5, 60, 20) andFont:15];
    numberL.text = @"订单号:";
    [self.contentView addSubview:numberL];
    
    self.orderNumberL = [self backLabelWithFrame:CGRectMake(CGRectGetMaxX(numberL.frame), 5, 250, 20) andFont:15];
    self.orderNumberL.text = @"OD201604190001";
    self.orderNumberL.textAlignment = NSTextAlignmentLeft;
    self.orderNumberL.textColor = [UIColor blueColor];
    
    
    __weak typeof(self) weakSelf = self;
    
    //起始地址
    self.startAddress = [[UILabel alloc]init];
    [self.contentView addSubview:self.startAddress];
    [self.startAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(numberL.mas_bottom).offset(10);
        make.centerX.equalTo(numberL);
        make.size.mas_equalTo(CGSizeMake(screenWidth/3.0, 20));
        
    }];
    self.startAddress.textAlignment = NSTextAlignmentCenter;
    self.startAddress.font = Font(14);
    self.startAddress.text = @"北京";
    
    //终止地址
    self.endAddress = [[UILabel alloc]init];
    [self.contentView addSubview:self.endAddress];
    [self.endAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(numberL.mas_bottom).offset(10);
//        make.centerX.equalTo(weakSelf.timeL);
        make.left.mas_equalTo(screenWidth/3.0 * 2);
        make.size.mas_equalTo(CGSizeMake(screenWidth/3.0, 20));
        
    }];
    self.endAddress.textAlignment = NSTextAlignmentCenter;
    self.endAddress.font = Font(14);
    self.endAddress.text = @"杭州";
    
    //起始详情地址
    self.startDetailAddress = [[UILabel alloc]init];
    [self.contentView addSubview:self.startDetailAddress];
    [self.startDetailAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.startAddress.mas_bottom).offset(0);
        make.centerX.equalTo(weakSelf.startAddress);
        make.size.mas_equalTo(CGSizeMake(screenWidth/3.0, 20));
    }];
    self.startDetailAddress.textAlignment = NSTextAlignmentCenter;
    self.startDetailAddress.font = Font(14);
    self.startDetailAddress.text = @"北京八大处";
    
    
    UIImageView *imageV = [[UIImageView alloc]init];
    [self.contentView addSubview:imageV];
    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.centerY.equalTo(weakSelf.startDetailAddress);
        make.centerX.equalTo(weakSelf.contentView);
        make.size.mas_equalTo(CGSizeMake(60, 30));
        
        
    }];
    
    imageV.backgroundColor = [UIColor cyanColor];
    
    
    
    
    //终止详情地址
    self.endDetailAddress = [[UILabel alloc]init];
    [self.contentView addSubview:self.endDetailAddress];
    [self.endDetailAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.endAddress.mas_bottom).offset(0);
        make.centerX.equalTo(weakSelf.endAddress);
        make.size.mas_equalTo(CGSizeMake(screenWidth/3.0, 20));
        
        
    }];
    self.endDetailAddress.textAlignment = NSTextAlignmentCenter;
    self.endDetailAddress.font = Font(14);
    self.endDetailAddress.text = @"杭州西湖";
    
    //起始时间
    
    self.startTimeL = [[UILabel alloc]init];
    [self.contentView addSubview:self.startTimeL];
    [self.startTimeL mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(weakSelf.startDetailAddress.mas_bottom).offset(0);
        make.centerX.equalTo(weakSelf.startDetailAddress);
        make.size.mas_equalTo(CGSizeMake(screenWidth/3.0, 20));
        
    }];
    self.startTimeL.textAlignment = NSTextAlignmentCenter;
    self.startTimeL.font = Font(14);
    self.startTimeL.text = @"2016-04-10";
    
    
    //终止时间
    self.endTimeL = [[UILabel alloc]init];
    [self.contentView addSubview:self.endTimeL];
    [self.endTimeL mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(weakSelf.endDetailAddress.mas_bottom).offset(0);
        make.centerX.equalTo(weakSelf.endDetailAddress);
        make.size.mas_equalTo(CGSizeMake(screenWidth/3.0, 20));
        
    }];
    self.endTimeL.textAlignment = NSTextAlignmentCenter;
    self.endTimeL.font = Font(14);
    self.endTimeL.text = @"2016-04-14";
    
    UILabel *lineL2 = [[UILabel alloc]init];
    [self.contentView addSubview:lineL2];
    [lineL2 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(weakSelf.startTimeL.mas_bottom).offset(3);
        make.centerX.equalTo(weakSelf.contentView);
        make.size.mas_equalTo(CGSizeMake(screenWidth - 40, 0.5));
    }];
    
    lineL2.backgroundColor = GrayColor;
    
    UILabel *priceL = [[UILabel alloc]init];
    priceL.text = @"金额:";
    [self.contentView addSubview:priceL];
    [priceL  mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(screenWidth/3 * 2 + 10);
        make.top.mas_equalTo(lineL2.mas_bottom).offset(5);
        make.size.mas_equalTo(CGSizeMake(40, 20));
        
    }];
    priceL.font = Font(14);
    
    self.priceLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
    
        make.left.mas_equalTo(priceL.mas_right).offset(0);
        make.centerY.equalTo(priceL);
        make.size.mas_equalTo(CGSizeMake(100, 20));
        
        
    }];
    self.priceLabel.font = Font(14);
    self.priceLabel.text = @"￥10000";
    
    
}

-(UILabel *)backLabelWithFrame:(CGRect)frame andFont:(int)a
{
    UILabel *label = [[UILabel alloc]init];
    label.frame = frame;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = Font(a);
    [self.contentView addSubview:label];
    
    return  label;
}

-(void)setModel:(MineOrderModel *)model
{
    if (_model != model) {
        
        //订单号
        self.orderNumberL.text = [NSString stringWithFormat:@"%@",model.orderCode];
        //起始地
        self.startAddress.text = [NSString stringWithFormat:@"%@",model.departCityName];
        
        //起始地详细地址
        self.startDetailAddress.text = [NSString stringWithFormat:@"%@",model.departUnit];
        
        
        //起始时间
        
        self.startTimeL.text = [NSString stringWithFormat:@"%@",model.deliveryDate];
        //终止地
//        self.endAddress.text = [NSString stringWithFormat:@"%@",model.receiptCityName];
//        
//        //终止地详细地址
//        self.endDetailAddress.text = [NSString stringWithFormat:@"%@",model.receiptUnit];
//        
        //终止日期
        self.endTimeL.text = [NSString stringWithFormat:@"%@",model.arriveDate];
        
        self.priceLabel.text = [NSString stringWithFormat:@"￥%@",model.actualCost];
        
//        //车
//        NSMutableArray *array = [NSMutableArray array];
//        self.carStyleL.text = @"";
//        
//        for (int i = 0; i < model.vehicles.count; i ++) {
//            NSString *string = [NSString stringWithFormat:@"%@ %@辆",model.vehicles[i][@"brandName"],model.vehicles[i][@"amount"]];
//            
//            self.carStyleL.text = [self.carStyleL.text stringByAppendingString:string];
//            self.carStyleL.text = [self.carStyleL.text stringByAppendingString:@"   "];
//            
//            [array addObject:string];
//            
//        }
        
        
    }
}



-(void)setChecked:(BOOL)checked
{
    if (checked) {
       _statusImageV.backgroundColor = [UIColor redColor];
        
    } else {
        _statusImageV.backgroundColor = [UIColor cyanColor];
        
    }
    
    _checked = checked;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
