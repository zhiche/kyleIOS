//
//  InvoiceCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MineOrderModel.h"

@interface InvoiceCell : UITableViewCell
@property (nonatomic,strong) UIImageView *statusImageV;
@property (nonatomic,strong) UILabel *orderNumberL;//订单编号

@property (nonatomic,strong) UILabel *startAddress;//起始地点
@property (nonatomic,strong) UILabel *startDetailAddress;//起始详细地点

@property (nonatomic,strong) UILabel *endAddress;//终止地点
@property (nonatomic,strong) UILabel *endDetailAddress;//终止详细地点

@property (nonatomic,strong) UILabel *startTimeL;//起始时间
@property (nonatomic,strong) UILabel *endTimeL;//终止时间
@property (nonatomic,strong) UILabel *priceLabel;//金额

@property (nonatomic) BOOL checked;

@property (nonatomic,strong) MineOrderModel *model;

@end
