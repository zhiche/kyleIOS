//
//  InstallCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//
#define kNumber 5

#import "InstallCell.h"

@implementation InstallCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
       
        
        
        [self initSubViews];
        
    }
    
    return  self;
}

-(void)initSubViews
{
    
    //头像
    self.pictureButton = [UIButton buttonWithType:UIButtonTypeCustom ];
    self.pictureButton.frame = CGRectMake(18, 14 * kHeight, 50 * kWidth, 50 * kHeight);
    self.pictureButton.backgroundColor = [UIColor clearColor];
    self.pictureButton.layer.cornerRadius = 50 * kWidth/2.0;
    [self.contentView addSubview:self.pictureButton];
    
    
    //账户名
    self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.pictureButton.frame) + 15, CGRectGetMinY(self.pictureButton.frame) + 5 * kHeight, 200, 15 * kHeight)];
    self.nameLabel.text = @"手机号";
    [self.contentView addSubview:self.nameLabel];
    self.nameLabel.font = Font(14);
    self.nameLabel.textColor = littleBlackColor;
    //积分
    self.scoreL = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + 10 * kHeight, 200, 15 * kHeight)];
//    self.scoreL.text = @"积分";
    [self.contentView addSubview:self.scoreL];
    self.scoreL.font = Font(14);
    self.scoreL.textColor = littleBlackColor;
    
//    //账户管理
//    UILabel *manageL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 140, CGRectGetMidY(self.pictureButton.frame) - 10, 100, 20)];
//    manageL.text = @"账户管理";
//    manageL.font = Font(15);
//    manageL.textAlignment = NSTextAlignmentCenter;
//    [self.contentView addSubview:manageL];
//
    //箭头
    UIImageView *arrowImg = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth - 21.5 , CGRectGetMidY(self.pictureButton.frame) - 5.5, 6.5, 11)];
    arrowImg.image = [UIImage imageNamed:@"personal_arrow"];
    [self.contentView addSubview:arrowImg];

}


-(UIView *)backViewWithFrame:(CGRect)frame andArray:(NSArray *)array
{
    UIView *view = [[UIView alloc]initWithFrame:frame];
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 0.5)];
    label1.backgroundColor = LineGrayColor;
    [view addSubview:label1];
    
    
    for (int i = 0; i< array.count ; i++) {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth/array.count * (i + 1), 0, 0.5, frame.size.height)];
        label.backgroundColor = LineGrayColor;
        
        UILabel *titleL = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth/array.count * i, 10 + (frame.size.height - 20)/2, screenWidth/array.count, (frame.size.height - 20)/2)];
        titleL.font = Font(15);
        titleL.textAlignment = NSTextAlignmentCenter;
        titleL.text = array[i];
        
        [view addSubview:titleL];
        [view addSubview:label];
        
    }

    
    return view;
}

-(UILabel *)backLabelWithInteger:(int)i andArray:(NSArray *)array
{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth/array.count * i, 10 , screenWidth/array.count, (self.bottomView.frame.size.height - 20)/2)];
    label.font = Font(15);
    label.textAlignment = NSTextAlignmentCenter;
    [self.bottomView addSubview:label];

    return label;

}




- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
