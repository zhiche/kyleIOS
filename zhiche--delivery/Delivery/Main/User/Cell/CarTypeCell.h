//
//  CarTypeCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarTypeCell : UITableViewCell

@property (nonatomic,strong) UIImageView *logoImageV;
@property (nonatomic,strong) UILabel *brandLabel;
@property (nonatomic,strong) UILabel *typeLabel;
@property (nonatomic,strong) UIButton *deleteButton;

@end
