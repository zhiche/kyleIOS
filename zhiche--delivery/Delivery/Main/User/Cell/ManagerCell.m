//
//  ManagerCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/9.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ManagerCell.h"

@implementation ManagerCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        
        
        [self initSubViews];
        
    }
    
    return  self;
}

-(void)initSubViews
{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, 20, 100, 20)];
    label.text = @"头像:";
    label.font = Font(15);
    [self.contentView addSubview:label];
    
    //头像
    self.pictureButton = [UIButton buttonWithType:UIButtonTypeCustom ];
    self.pictureButton.frame = CGRectMake(14, 10, 50, 50);
    self.pictureButton.backgroundColor = [UIColor clearColor];
    self.pictureButton.layer.cornerRadius = 5;
    [self.contentView addSubview:self.pictureButton];
    
    self.pictureImageV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    self.pictureImageV.backgroundColor = [UIColor redColor];
    self.userInteractionEnabled = YES;
    self.pictureImageV.layer.cornerRadius = 5;
    self.pictureImageV.layer.masksToBounds = YES;
    [self.pictureButton addSubview:self.pictureImageV];

    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
