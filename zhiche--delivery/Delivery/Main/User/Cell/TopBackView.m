//
//  TopBackView.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/11.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "TopBackView.h"
#import <Masonry.h>

@implementation TopBackView

-(id)initViewWithFrame:(CGRect)frame andTitle:(NSString *)string
{
    if (self = [super init]) {
        
        self.frame = frame;
        self.backgroundColor = YellowColor;
        
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake((screenWidth - 250)/2, 34, 250, 16)];
        titleLabel.text = string;
        titleLabel.tag = 1111;
        titleLabel.textColor = WhiteColor;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:titleLabel];
        titleLabel.font = Font(15);

        
        UIImageView *lineImageV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 63.5, screenWidth, 0.5)];
        //        lineImageV.image = [UIImage imageNamed:@"common_top_line"];
        [self addSubview:lineImageV];
        
        //客服
        self.rightButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [self addSubview:self.rightButton];
        [self.rightButton setTitle:@"按钮" forState:UIControlStateNormal];
        [self.rightButton setTintColor:[UIColor blackColor]];
        
        self.rightButton.frame = CGRectMake(screenWidth - 100 -18, 30, 100, 25);
        self.rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        self.rightButton.titleLabel.font = Font(15);
//        if (17 * kWidth > 17) {
//            
//            titleLabel.font = [UIFont systemFontOfSize:17];
//            self.rightButton.titleLabel.font = [UIFont systemFontOfSize:17];
//
//            
//        } else {
//            
//            titleLabel.font = Font(17);
//            self.rightButton.titleLabel.font = Font(17);
//
//
//        }
              //
        
        __weak typeof(self) weakSelf = self;
        
        UIImageView *phoneImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"common_top_icon_phone"]];
        [self.rightButton addSubview:phoneImg];
        [phoneImg mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.right.mas_equalTo(weakSelf.rightButton.mas_left).offset(10);
            make.size.mas_equalTo(CGSizeMake(10, 12.5));
            make.centerY.equalTo(weakSelf.rightButton);
            
        }];
        
        
        //返回
        
        self.leftButton = [UIButton buttonWithType:UIButtonTypeSystem];
//        [self.leftButton setBackgroundImage:[UIImage imageNamed:@"common_back_btn"] forState:UIControlStateNormal];
        [self addSubview:self.leftButton];
        [self.leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.size.mas_equalTo(CGSizeMake(46, 30));
            make.left.mas_equalTo(0);
            make.centerY.equalTo(titleLabel);
            
        }];

        
        UIImageView *backImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"common_back_btn"]];
        [self.leftButton addSubview:backImage];
        [backImage mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.size.mas_equalTo(CGSizeMake(10.5, 17.5));
            make.center.equalTo(_leftButton);
            
        }];
        

        
        
    }
    return self;
}


@end
