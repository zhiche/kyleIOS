//
//  MessageCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/25.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "MessageCell.h"

@implementation MessageCell

- (void)awakeFromNib {
    [super awakeFromNib];

    self.messageLabel.font = Font(15);
    self.dateLabel.font = Font(15);
    self.timeLabel.font = Font(15);
    self.advanceLabel.font = Font(15);
    self.orderNumberLabel.font = Font(15);


}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
