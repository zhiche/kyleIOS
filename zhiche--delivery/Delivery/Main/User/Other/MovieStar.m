//
//  MovieStar.m
//  MoveStar
//
//  Created by 旮旯网络 on 16/2/24.
//  Copyright © 2016年 yang. All rights reserved.
//

#import "MovieStar.h"

#define kMargin 18
#define kMargin_brigre 12

@implementation MovieStar
- (id)initWithFrame:(CGRect)frame andCount:(NSInteger)count

{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        self.v_star = [[UIView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:self.v_star];
        
//        self.v_star.backgroundColor = [UIColor redColor];
        
        self.img_star1 = [[UIImageView alloc]initWithFrame:CGRectMake(3, 2, kMargin,kMargin)];
        self.img_star2 = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.img_star1.frame) + kMargin_brigre, 2, kMargin, kMargin)];
        self.img_star3 = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.img_star2.frame) + kMargin_brigre , 2, kMargin, kMargin)];
        self.img_star4 = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.img_star3.frame) + kMargin_brigre, 2, kMargin, kMargin)];
        self.img_star5 = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.img_star4.frame) + kMargin_brigre, 2, kMargin, kMargin)];
        
        
        NSArray *arr = [NSArray arrayWithObjects:self.img_star1,self.img_star2,self.img_star3,self.img_star4,self.img_star5, nil];
        
        
        
        for (int i = 0; i < arr.count; i ++) {
            UIImageView *imageV = arr[i];
            
            if (i < count) {
                [imageV setImage:[UIImage imageNamed:@"StarSelected"]];
            } else {
                [imageV setImage:[UIImage imageNamed:@"StarUnSelect"]];
                
            }
            
        }

//
//        [self.img_star1 setImage:[UIImage imageNamed:@"StarUnSelect"]];
//        [self.img_star2 setImage:[UIImage imageNamed:@"StarUnSelect"]];
//        [self.img_star3 setImage:[UIImage imageNamed:@"StarUnSelect"]];
//        [self.img_star4 setImage:[UIImage imageNamed:@"StarUnSelect"]];
//        [self.img_star5 setImage:[UIImage imageNamed:@"StarUnSelect"]];
        
        
        [self.v_star addSubview:self.img_star1];
        [self.v_star addSubview:self.img_star2];
        [self.v_star addSubview:self.img_star3];
        [self.v_star addSubview:self.img_star4];
        [self.v_star addSubview:self.img_star5];

        
    }
    return self;
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self.v_star];
    if((point.x>0 && point.x<self.v_star.frame.size.width)&&(point.y>0 && point.y<self.v_star.frame.size.height)){
        self.canAddStar = YES;
        [self changeStarForegroundViewWithPoint:point];
        
    }else{
        self.canAddStar = NO;
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if(self.canAddStar){
        UITouch *touch = [touches anyObject];
        CGPoint point = [touch locationInView:self.v_star];
        [self changeStarForegroundViewWithPoint:point];
        
    }
    
    
    return;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if(self.canAddStar){
        UITouch *touch = [touches anyObject];
        CGPoint point = [touch locationInView:self.v_star];
        [self changeStarForegroundViewWithPoint:point];
        
    }
    
    self.canAddStar = NO;
    //    if (!self.canTouch) {
    //        return;
    //    }
    //    UITouch *touch = [touches anyObject];
    //    CGPoint point = [touch locationInView:self];
    //    __weak HYBCommentStarView *weekSelf = self;
    //
    //    [UIView transitionWithView:self.starForegroundView
    //                      duration:0.2
    //                       options:UIViewAnimationOptionCurveEaseInOut
    //                    animations:^ {
    //                        [weekSelf changeStarForegroundViewWithPoint:point];
    //                    }
    //                    completion:nil];
    return;
}


-(void)changeStarForegroundViewWithPoint:(CGPoint)point{
    if(self.v_count.hidden){
        self.v_count.hidden = NO;
    }
    NSInteger count = 0;
    count = count + [self changeImg:point.x image:self.img_star1];
    count = count + [self changeImg:point.x image:self.img_star2];
    count = count + [self changeImg:point.x image:self.img_star3];
    count = count + [self changeImg:point.x image:self.img_star4];
    count = count + [self changeImg:point.x image:self.img_star5];
    if(count==0){
        count = 0;
        [self.img_star1 setImage:[UIImage imageNamed:@"StarUnSelect"]];
    }
    self.count = count;
    
    
    [self checkCount:count];

}

-(void)checkCount:(NSInteger)count{
    
    
    if (count%2 == 0) {
        self.integer = count/2;
    } else {
        self.integer = count/2 + 1;
    }
    
//    NSString *str;
//    
//    if (self.integer == 0) {
//        str = @"很很很不好";
//    }
//    
//    
//    if (self.integer == 1) {
//        str = @"很很不好";
//    }
//    
//    if (self.integer == 2) {
//        str = @"很不好";
//    }
//    
//    
//    if (self.integer == 3) {
//        str = @"良好";
//    }
//    
//    if (self.integer == 4) {
//        str = @"好";
//    }
//    
//    if (self.integer == 5) {
//        str = @"优秀";
//    }
//    
//    NSLog(@"%ld  %ld",self.integer,count);
    if (self.call_backValue) {
        self.call_backValue(self.integer);

    }
    
}

-(NSInteger)changeImg:(float)x image:(UIImageView*)img{
    if(x> img.frame.origin.x + img.frame.size.width/2){
        [img setImage:[UIImage imageNamed:@"StarSelected"]];
        return 2;
    }else if(x> img.frame.origin.x && x < img.frame.origin.x + img.frame.size.width/2){
        [img setImage:[UIImage imageNamed:@"StarSelected"]];
        return 1;
        //        [self setImageAnimation:img];
    }else{
        [img setImage:[UIImage imageNamed:@"StarUnSelect"]];
        return 0;
    }
}

-(void)setImageAnimation:(UIView *)v{
    CGRect rec = v.frame;
    [UIView animateWithDuration:0.1 animations:^{
        v.frame = CGRectMake(v.frame.origin.x, v.frame.origin.y -3, v.frame.size.width, v.frame.size.height);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            v.frame = rec;
        } completion:^(BOOL finished) {
            v.frame = rec;
        }];
    }];
}



@end
