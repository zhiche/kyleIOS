//
//  UIColor+Hex.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/7/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)
-(UIColor *)colorWithHexString:(NSString *)stringToConvert;
@end
