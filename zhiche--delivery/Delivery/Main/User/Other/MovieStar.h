//
//  MovieStar.h
//  MoveStar
//
//  Created by 旮旯网络 on 16/2/24.
//  Copyright © 2016年 yang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieStar : UIView

@property (strong,nonatomic) UIView *v_addcomment;

@property(strong,nonatomic) IBOutlet UIView *v_count;
@property(strong,nonatomic) IBOutlet UIView *v_main;
@property(strong,nonatomic) IBOutlet UILabel *lbl_count;
@property(strong,nonatomic) IBOutlet UILabel *lbl_counttext;
//


@property (nonatomic,strong) UIView *v_star;
@property (nonatomic,strong) UIImageView *img_star1;
@property (nonatomic,strong) UIImageView *img_star2;
@property (nonatomic,strong) UIImageView *img_star3;
@property (nonatomic,strong) UIImageView *img_star4;
@property (nonatomic,strong) UIImageView *img_star5;

@property (nonatomic) NSInteger integer;
@property (nonatomic,copy) void (^call_backValue)(NSInteger a);

@property NSInteger count;
@property BOOL canAddStar;

- (id)initWithFrame:(CGRect)frame andCount:(NSInteger)count;

@end
