//
//  ShowViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/19.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ShowViewController.h"
#import "TopBackView.h"
#import <WebKit/WebKit.h>

@interface ShowViewController ()<WKUIDelegate,WKNavigationDelegate>

@end

@implementation ShowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"积分规则"];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    self.view.backgroundColor = WhiteColor;
    
    [self.view addSubview:topBackView];
    
    [self initWebViews];

}

-(void)initWebViews
{
    
    NSString *jScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
    
    WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    WKUserContentController *wkUController = [[WKUserContentController alloc] init];
    [wkUController addUserScript:wkUScript];
    
    WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
    wkWebConfig.userContentController = wkUController;
    
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64) configuration:wkWebConfig];
    
    NSURL  *url = [NSURL URLWithString:score_rule];
    
//    UIWebView *webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64)];
    //隐藏数字下面的横线
//    webView.dataDetectorTypes = UIDataDetectorTypeNone;

    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    webView.navigationDelegate = self;
    webView.backgroundColor = [UIColor whiteColor];
    
    [webView loadRequest:request];
    
    [self.view addSubview:webView];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
