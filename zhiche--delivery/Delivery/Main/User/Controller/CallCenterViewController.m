//
//  CallCenterViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/25.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "CallCenterViewController.h"
#import "TopBackView.h"
#import "RootViewController.h"
#import "NewViewController.h"//新手上路
#import "RefundViewController.h"//退款规则
#import "InstroctionViewController.h"//运输说明
#import "AboutViewController.h"//关于页面
#import "InvoiceShowViewController.h"//开取发票
#import "ServeViewController.h"//服务
#import "AboutVC.h"

@interface CallCenterViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataArray;//网络数据

@end

@implementation CallCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [NSMutableArray array];
    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"关于慧运车"];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = WhiteColor;
    
    self.dataArray = [NSMutableArray arrayWithObjects:@"关于慧运车",@"客服热线  400－670－6082", nil];
    
    [self initSubViews];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *callString = @"callCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:callString];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:callString];
    }
    
    
    if (indexPath.row != self.dataArray.count - 1) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    
    cell.textLabel.text = self.dataArray[indexPath.row];
    cell.textLabel.font = Font(13);
    cell.textLabel.textColor = littleBlackColor;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    if (indexPath.row == 0) {
//        //运输说明
//        InstroctionViewController *instroctionVC = [[InstroctionViewController alloc]init];
//        [self.navigationController pushViewController:instroctionVC animated:YES];
//    }
//    
//    if (indexPath.row == 1) {
//        
//        //服务
//        ServeViewController *serveVC = [[ServeViewController alloc]init];
//        
//        [self.navigationController pushViewController:serveVC animated:YES];
//        
//    }
    
    
    if (indexPath.row == 0) {
        
        //服务
        AboutVC *aboutV = [[AboutVC alloc]init];
        
        [self.navigationController pushViewController:aboutV animated:YES];
        
    }
    
    if (indexPath.row == 1) {
        
//        [self showCallPhone];
    }
    
    
    
    

//    if (indexPath.row == 2) {
//        //退款规则
//        RefundViewController *refundVC = [[RefundViewController alloc]init];
//        [self.navigationController pushViewController:refundVC animated:YES];
//    }
//
//    
//    if (indexPath.row == 3) {
//       //开票规则
//        InvoiceShowViewController *invoiceVC = [[InvoiceShowViewController alloc]init];
//        
//        [self.navigationController pushViewController:invoiceVC animated:YES];
//        
//    }
//
//    
//    if (indexPath.row == 4) {
//        //新手上路
//        NewViewController *newVC = [[NewViewController alloc]init];
//        
//        [self.navigationController pushViewController:newVC animated:YES];
//    }
//
//    if (indexPath.row == 5) {
//        //关于页面
//        AboutViewController *aboutVC = [[AboutViewController alloc]init];
//        [self.navigationController pushViewController:aboutVC animated:YES];
//        
//    }

    
    
    
}


//打电话
-(void)showCallPhone
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"呼叫客服电话:400－6706082" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"呼叫" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iphoneNumber]];
        
    }];
    
    [alert addAction:action1];
    [alert addAction:action2];
    
    [self presentViewController:alert animated:YES completion:nil];

}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return cellHeight;

}



-(void)initSubViews
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64 ) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = GrayColor;
    [self.view addSubview:self.tableView];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, screenHeight - 150, screenWidth, 20)];
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    // app名称
//    NSString *app_Name = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    // app版本
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    // app build版本
//    NSString *app_build = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    label.text = [NSString stringWithFormat:@"当前版本号:%@",app_Version];
//    @"当前版本号:2.0";
    label.textAlignment = NSTextAlignmentCenter;
    label.font = Font(14);
    [self.tableView addSubview:label];
    label.textColor = littleBlackColor;
    
}


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
    
    
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
