//
//  ServeViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/6/3.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ServeViewController.h"
#import "Common.h"
#import "TopBackView.h"
#import <WebKit/WebKit.h>

@interface ServeViewController ()<WKUIDelegate,WKNavigationDelegate>

@end

@implementation ServeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;

    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"车辆运输服务协议"];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = WhiteColor;
    
    [self initWebViews];
}

-(void)initWebViews
{
    
    NSString *jScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
    
    WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    WKUserContentController *wkUController = [[WKUserContentController alloc] init];
    [wkUController addUserScript:wkUScript];
    
    WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
    wkWebConfig.userContentController = wkUController;
    
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64) configuration:wkWebConfig];
//

    
            NSURL  *url = [NSURL URLWithString:transport_agreement];
            
    
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
//            webView.delegate = self;

            webView.backgroundColor = [UIColor whiteColor];
            
            [webView loadRequest:request];
            
            [self.view addSubview:webView];
//        }
//        
//        
//        
//    } failed:^(NSString *errorMsg) {
//        NSLog(@"%@",errorMsg);
//    }];
    
    
    
    
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
