//
//  InvoiceShowViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/6/3.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "InvoiceShowViewController.h"
#import "Common.h"
#import "TopBackView.h"

@interface InvoiceShowViewController ()<UIWebViewDelegate>


@end

@implementation InvoiceShowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;

    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"开票规则"];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = WhiteColor;
    
    [self initWebViews];
}
-(void)initWebViews
{
    
//    NSString *valueString = [NSString stringWithFormat:@"%@templates/cms/invoice-explain.html",Main_interface];
    
//    [Common requestWithUrlString:valueString contentType:application_json finished:^(id responseObj) {
//        
//        NSLog(@"%@",responseObj);
//        
//        if ([responseObj[@"success"] boolValue]) {
    
            NSURL  *url = [NSURL URLWithString:invoice_explain];
            
            UIWebView *webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64)];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            webView.delegate = self;
            webView.backgroundColor = [UIColor whiteColor];
            
            [webView loadRequest:request];
            
            [self.view addSubview:webView];
//        }
//        
//        
//        
//    } failed:^(NSString *errorMsg) {
//        NSLog(@"%@",errorMsg);
//    }];
    
    
    
    
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
