//
//  InvoiceDetailViewController.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InvoiceDetailViewController : UIViewController

@property (nonatomic,copy) NSString *price;
@property (nonatomic,copy) NSString *idString;

@end
