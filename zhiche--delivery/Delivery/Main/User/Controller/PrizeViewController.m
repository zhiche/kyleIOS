//
//  PrizeViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/19.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "PrizeViewController.h"
#import "TopBackView.h"
@interface PrizeViewController ()<UIWebViewDelegate>

@end

@implementation PrizeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"积分抽奖"];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    self.view.backgroundColor = WhiteColor;
    
    [self.view addSubview:topBackView];
    
//    [self initWebViews];
}

//-(void)initWebViews
//{
//    
//    NSURL  *url = [NSURL URLWithString:aboutUS_web_Url];
//    
//    UIWebView *webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64)];
//    
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    webView.delegate = self;
//    webView.backgroundColor = [UIColor whiteColor];
//    
//    [webView loadRequest:request];
//    
//    [self.view addSubview:webView];
//      
//}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
