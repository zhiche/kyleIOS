//
//  InvoiceViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "InvoiceViewController.h"
#import "TopBackView.h"
#import "RootViewController.h"
#import "InvoiceCell.h"
#import "InvoiceDetailViewController.h"
#import "Common.h"
#import "MineOrderModel.h"

@interface InvoiceViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataArray;//网络数据
@property (nonatomic,strong) NSMutableArray *markArray;//标记是否选中
@property (nonatomic,strong) NSMutableArray *resultArray;

@property (nonatomic,strong) UILabel *numberL;
@property (nonatomic,strong) UILabel *priceL;
@property (nonatomic,strong) UIButton *nextButton;
@property (nonatomic) int pageNo;

@end

@implementation InvoiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataArray = [NSMutableArray array];
    self.markArray = [NSMutableArray array];
    self.resultArray = [NSMutableArray array];
    self.pageNo = 1;
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;

    
    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"开取发票"];
    
    topBackView.rightButton.hidden = YES;
     [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = WhiteColor;
    [self initDataSource];
    [self initSubView];
    
}

-(void)initDataSource
{
    NSString *urlString = [NSString stringWithFormat:@"%@order/uninvoiced?pageNo=%d&pageSize=%d",Main_interface,self.pageNo,10];
    
    
    [Common requestWithUrlString:urlString contentType:application_json finished:^(id responseObj) {
        
        
            if ([responseObj[@"success"] boolValue]) {
                
                NSArray *array = responseObj[@"data"][@"orders"];
                
                for (id obj in array) {
                    MineOrderModel *model = [[MineOrderModel alloc]init];
                    
                    
                    [model setValuesForKeysWithDictionary:obj];
                    
                    [self.dataArray addObject:model];
                }
                
                
                [self.tableView reloadData];
                
            }
    
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
    
    

}

-(void)initSubView
{
    
//    self.dataArray = [NSMutableArray arrayWithObjects:@"2030",@"2440",@"3110",@"4000",@"5310",@"6250", nil];
  

    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64 - 60) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    
    UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight - 60, screenWidth, 60)];
    [self.view addSubview:bottomView];
    
    UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 0.5)];
    imageV.image = [UIImage imageNamed:@"common_top_line"];
    [bottomView addSubview:imageV];
    
    self.numberL = [[UILabel alloc]initWithFrame:CGRectMake(20, 10, 100, 15)];
    self.numberL.font = Font(13);
    self.numberL.text = @"0个订单";
    self.numberL.textAlignment = NSTextAlignmentCenter;
    [bottomView addSubview:self.numberL];
    
    
    self.priceL = [[UILabel alloc]initWithFrame:CGRectMake(20, 35, 100, 15)];
    self.priceL.font = Font(13);
    self.priceL.text = @"合计0元";
    self.priceL.textAlignment = NSTextAlignmentCenter;
    [bottomView addSubview:self.priceL];
    
    
    self.nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.nextButton.frame = CGRectMake(screenWidth - 100 - 20, 10, 100, 40);
    self.nextButton.backgroundColor = GrayColor;
    [self.nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    [self.nextButton setTintColor:WhiteColor];
    [self.nextButton addTarget:self action:@selector(nextButtonAction) forControlEvents:UIControlEventTouchUpInside];
    self.nextButton.titleLabel.font = Font(15);
    self.nextButton.userInteractionEnabled = NO;
    [bottomView addSubview:self.nextButton];
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *string = @"invoiceCell";
    InvoiceCell *cell = [tableView dequeueReusableCellWithIdentifier:string];
    if (!cell) {
        cell = [[InvoiceCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:string];
    }
    
    if (self.dataArray.count > 0) {
        
        for (int i = 0; i<self.dataArray.count; i++) {
            [self.markArray addObject:@(NO)];
        }
        
        cell.model = self.dataArray[indexPath.section];
        
    }
    
   
    
    
//    cell.priceLabel.text = [NSString stringWithFormat:@"￥%@",self.dataArray[indexPath.section]];
    
    cell.checked = [self.markArray[indexPath.section] boolValue];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    InvoiceCell *cell = (InvoiceCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    
    if ([self.markArray[indexPath.section] boolValue] == NO) {
        
        
        [self.markArray replaceObjectAtIndex:indexPath.section withObject:@(YES)];
        
        cell.checked = YES;
        
        [self.resultArray addObject:self.dataArray[indexPath.section]];
        
        
    } else {
        [self.markArray replaceObjectAtIndex:indexPath.section withObject:@(NO)];
        
        cell.checked = NO;
        
        [self.resultArray removeObject:self.dataArray[indexPath.section]];
    }
    
    [self getValueString];
//
    
}

-(void)getValueString
{
   
    if (self.resultArray.count > 0) {
        
        self.nextButton.backgroundColor = [UIColor redColor];
        self.nextButton.userInteractionEnabled = YES;
        
        
        self.numberL.text = [NSString stringWithFormat:@"%ld个行程",self.resultArray.count];
        
        NSString *priceString = @"0";
        
               
        for (int i = 0; i < self.resultArray.count; i ++) {
            
            MineOrderModel *model = [[MineOrderModel alloc]init];
            model = self.resultArray[i];
            
            priceString = [NSString stringWithFormat:@"%ld",[priceString integerValue] + [model.actualCost integerValue] ];
        }
        
        self.priceL.text = [NSString stringWithFormat:@"合计%@元",priceString];
        
    } else {
        
        self.nextButton.backgroundColor = GrayColor;
        self.nextButton.userInteractionEnabled = NO;
        
        self.numberL.text = [NSString stringWithFormat:@"0个行程"];
        self.priceL.text = [NSString stringWithFormat:@"合计0元"];

    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 135;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
    
    
}

-(void)nextButtonAction
{
    /**
    
     跳转开票详情页面
     */
    
    InvoiceDetailViewController *detailVC = [[InvoiceDetailViewController alloc]init];
    
    NSString *string = [self.priceL.text stringByReplacingOccurrencesOfString:@"合计" withString:@""];
    
    string = [string stringByReplacingOccurrencesOfString:@"元" withString:@""];
    
    detailVC.price = string;
    
    
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < self.resultArray.count; i++) {
        MineOrderModel *model = self.resultArray[i];
        
        [array addObject:model.ID];
        
    }
    
    detailVC.idString = [array componentsJoinedByString:@","];
    
    NSLog(@"%@",detailVC.idString);
    
    
    [self.navigationController pushViewController:detailVC animated:YES];
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
