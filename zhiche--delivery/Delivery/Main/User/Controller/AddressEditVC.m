//
//  AddressEditVC.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/20.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "AddressEditVC.h"
#import "Common.h"
#import "ActionSheetPicker.h"
#import "area.h"
#import <Masonry.h>
#import "WKProgressHUD.h"
#import "QueryAddressVC.h"//选择城市


#define AreaBtnTag 100
#define KeepBtnTag 200
#define FieldTag 500
#define MySetValue(dict, value) ((!dict[value] || [dict[value] isKindOfClass:[NSNull class]])?@"":dict[value])


@interface AddressEditVC ()
{
    UIImageView * nav;
    Common * Com;
    area * AREA;
    NSMutableDictionary * dic;
    
}

@property (nonatomic,strong) UIButton * AreaBtn;
@property (nonatomic, strong) NSMutableArray *provienceArray;
@property (nonatomic, strong) NSArray *cityArray;
@property (nonatomic, strong) NSArray *areaArray;
@property (nonatomic,assign) BOOL store;
@end

@implementation AddressEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;

    self.view.backgroundColor = GrayColor;
    Com = [[Common alloc]init];
    AREA = [[area alloc]init];
    dic=[[NSMutableDictionary alloc]init];
    if ([_addresstype isEqualToString:@"1"]) {
        nav = [self createNav:@"送达地址编辑"];
        [dic setObject:@"1" forKey:@"addresstype"]; //地址类型(0:发车地址，1:送达地址)
        
        
    }else{
        nav = [self createNav:@"发车地址编辑"];
        [dic setObject:@"0" forKey:@"addresstype"]; //地址类型(0:发车地址，1:送达地址)
        
    }
    [self.view addSubview:nav];
    [self createUI];
    [dic setObject:@"" forKey:@"id"];
    [dic setObject:@"" forKey:@"provincecode"];//省编码
    [dic setObject:@"" forKey:@"provincename"]; //省名称
    [dic setObject:@"" forKey:@"citycode"];//市编码
    [dic setObject:@"" forKey:@"cityname"]; //市名称
    [dic setObject:@"" forKey:@"countycode"];//县编码
    [dic setObject:@"" forKey:@"countyname"]; //县名称
    
    
    [dic setObject:@"" forKey:@"contact"];//联系人
    [dic setObject:@"" forKey:@"phone"]; //联系电话
//    [dic setObject:@"" forKey:@"unitname"];//单位名称
    [dic setObject:@"" forKey:@"address"];//详细地址
//    [dic setObject:@"F" forKey:@"isdefault"];//是否默认(F:否，T:是)
    [dic setObject:@"" forKey:@"comment"];//备注
    
    
}



-(void)createUI{
    NSArray * arrName = nil;
    NSArray * FieldName =nil;
    if ([_addresstype isEqualToString:@"0"]) {
        arrName = @[@"区域",
                    @"详细地址",
                    @"联系人",
                    @"联系电话"];
        FieldName = @[self.address,self.contact,self.phone];
    }else{
        arrName = @[@"区域",
                    @"详细地址",
                    @"联系人",
                    @"联系电话"];
        FieldName = @[self.address,self.contact,self.phone];

        
    }
    
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, 4 * cellHeight)];
    view.backgroundColor = WhiteColor;
    [self.view addSubview:view];
    
    for (int i=0; i<arrName.count; i++) {
        
        UILabel * label = [self createUIlabel:arrName[i] andFont:14 andColor:fontGrayColor];
        [view addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.equalTo(self.view.mas_left).with.offset(60*kWidth);
            make.left.mas_equalTo(18);
            make.width.mas_equalTo(70* kWidth);
            make.centerY.mas_equalTo(nav.mas_bottom).with.offset(43*kHeight*i+21.5*kHeight);
        }];
        
        UIView * Hline =[[UIView alloc]init];
        Hline.backgroundColor = LineGrayColor;
        [view addSubview:Hline];
        if (i == 3) {
            [Hline mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.view.mas_left).offset(18 );
                make.centerY.mas_equalTo(nav.mas_bottom).with.offset(43*kWidth*(i));
                make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
            }];

        }else{
            [Hline mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.view.mas_left).offset(18 );
                make.centerY.mas_equalTo(nav.mas_bottom).with.offset(43*kWidth*(i+1));
                make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
            }];

        }
        if (i==0) {
            _AreaBtn = [self createBtn:self.zoneName andTag:AreaBtnTag];
            _AreaBtn.titleLabel.font = Font(FontOfSize14);
            [_AreaBtn addTarget:self action:@selector(pressBtnArea) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:_AreaBtn];
            [_AreaBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(label.mas_right).with.offset(20*kWidth);
                make.centerY.mas_equalTo(label.mas_centerY);
                make.right.mas_equalTo(0);
                make.height.mas_equalTo(25 * kHeight);
//                make.size.mas_equalTo(CGSizeMake(Main_Width/2, 25*kHeight));
            }];
            
            UIImageView * RightImage =[[UIImageView alloc]init];
            RightImage.image = [UIImage imageNamed:@"common_list_arrows_more"];
            RightImage.tag = 1000;
            [view addSubview:RightImage];
            [RightImage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self.view.mas_right).with.offset(-15*kWidth);
                make.centerY.mas_equalTo(_AreaBtn.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(8*kWidth, 13*kHeight));
            }];
        }
        else{
            //500 501 502 503
            UITextField * field = [self createField:FieldName[i-1] andTag:FieldTag+i-1 andFont:13];
            [view addSubview:field];
            [field mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(_AreaBtn.mas_left).with.offset(0);
                make.centerY.mas_equalTo(label.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(Main_Width/2 + 30, 25*kHeight));
            }];
        }
    }
    
    UITextField * field = (UITextField*)[self.view viewWithTag:502];
    UIButton * ConfirmBtn = [self createBtn:@"保存" andTag:KeepBtnTag];
    ConfirmBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [ConfirmBtn addTarget:self action:@selector(pressStoreBtn) forControlEvents:UIControlEventTouchUpInside];
    ConfirmBtn.backgroundColor = YellowColor;
    [ConfirmBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
//    ConfirmBtn.layer.borderColor = RGBACOLOR(149, 149, 149, 1).CGColor;
    [self.view addSubview:ConfirmBtn];
    [ConfirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(screenWidth - 40, Button_Height));
        make.top.mas_equalTo(field.mas_bottom).with.offset(30*kHeight);
    }];
}
-(void)pressBtnArea{
    
    QueryAddressVC *addressVC = [[QueryAddressVC alloc]init];
    addressVC.addresstype = self.addresstype;
    //发车地址
    if ([self.addresstype isEqualToString:@"0"]) {
        
        addressVC.callAddressBack = ^(NSDictionary *dictionry,NSString *addresstype){
        
            [dic setObject:dictionry[@"provinceCode"] forKey:@"provincecode"];//省编码
            [dic setObject:dictionry[@"provinceName"] forKey:@"provincename"]; //省名称
            [dic setObject:dictionry[@"cityCode"] forKey:@"citycode"];//市编码
            [dic setObject:dictionry[@"cityName"] forKey:@"cityname"]; //市名称
            [dic setObject:dictionry[@"countyCode"] forKey:@"countycode"];//县编码
            [dic setObject:dictionry[@"countyName"] forKey:@"countyname"]; //县名称

            [_AreaBtn setTitle:[NSString stringWithFormat:@"%@%@%@",dic[@"provincename"],dic[@"cityname"],dic[@"countyname"]] forState:UIControlStateNormal];

            
        };
        
    //收车地址
    } else {
        
        addressVC.callAddressBack = ^(NSDictionary *dictionry,NSString *addresstype){
            NSLog(@"%@",dictionry);
            [dic setObject:dictionry[@"provinceCode"] forKey:@"provincecode"];//省编码
            [dic setObject:dictionry[@"provinceName"] forKey:@"provincename"]; //省名称
            [dic setObject:dictionry[@"cityCode"] forKey:@"citycode"];//市编码
            [dic setObject:dictionry[@"cityName"] forKey:@"cityname"]; //市名称
            [dic setObject:dictionry[@"countyCode"] forKey:@"countycode"];//县编码
            [dic setObject:dictionry[@"countyName"] forKey:@"countyname"]; //县名称

            [_AreaBtn setTitle:[NSString stringWithFormat:@"%@%@%@",dic[@"provincename"],dic[@"cityname"],dic[@"countyname"]] forState:UIControlStateNormal];

        };
    }
    
    
    [self.navigationController pushViewController:addressVC animated:YES];
    
}
//
//    for (UITextField * field in self.view.subviews) {
//        
//        [field resignFirstResponder];
//        
//    }
//
//    
//    _provienceArray = nil;
//    _cityArray = nil;
//    _areaArray = nil;
//    
//    _provienceArray = [AREA selectAreaWithFArea:nil withTarea:nil withFarea:1];
//    _cityArray = [AREA selectAreaWithFArea:@"北京市" withTarea:nil withFarea:2];
////    _areaArray = [AREA selectAreaWithFArea:@"北京市" withTarea:@"市辖区" withFarea:3];
//    
//    
//    UIImageView * image = (UIImageView*)[self.view viewWithTag:1000];
//    image.transform = CGAffineTransformMakeRotation(M_PI_2);
//    UIPickerView *education = [[UIPickerView alloc] init];
//    ActionSheetPicker *actionSheet=[[ActionSheetPicker alloc]initWithTarget:self successAction:@selector(selectOK:) cancelAction:nil origin:education];
//    
//    CGRect pickerFrame = CGRectMake(0, 40, actionSheet.viewSize.width, 216);
//    [education setFrame:pickerFrame];
//    education.delegate = self;
//    education.dataSource = self;
//    //pickview  左选择按钮的文字
//    UIButton *leftBtn =[UIButton buttonWithType:UIButtonTypeCustom];
//    [leftBtn setTitle:@"取消"forState:UIControlStateNormal];
//    leftBtn.frame=CGRectMake(0, 0, 50, 45);
//    [leftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    UIBarButtonItem *leftIte=[[UIBarButtonItem alloc]initWithCustomView:leftBtn];
//    //pickview    右选择按钮的文字
//    UIButton *rightBtn =[UIButton buttonWithType:UIButtonTypeCustom];
//    [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
//    rightBtn.frame=CGRectMake(0, 0, 50, 45);
//    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    UIBarButtonItem *rightIte=[[UIBarButtonItem alloc]initWithCustomView:rightBtn];
//    //将按钮添加到pickerview 上
//    [actionSheet setDoneButton:rightIte];
//    [actionSheet setCancelButton:leftIte];
//    actionSheet.pickerView = education;
//    actionSheet.title = @"地址选择";
//    [actionSheet showActionSheetPicker];
//    NSLog(@"点击的是地区选择");
//}
/*
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component

{
    NSInteger ss = [pickerView selectedRowInComponent:0];
    NSInteger dd = [pickerView selectedRowInComponent:1];
    
    switch (component) {
        case 0:
        {
            NSDictionary *dict = [_provienceArray objectAtIndex:row];
            NSString *code = MySetValue(dict, @"provinceName");
            if (code.length > 0)
            {
                _cityArray = [AREA selectAreaWithFArea:code withTarea:nil withFarea:2];
                [pickerView reloadComponent:1];
                NSDictionary *cityDict = [_cityArray objectAtIndex:dd];
                NSString * cityFcode = MySetValue(cityDict, @"cityName");
                if (cityFcode.length > 0)
                {
//                    _areaArray = [AREA selectAreaWithFArea:code withTarea:cityFcode withFarea:3];
//                    [pickerView reloadComponent:2];
                }
            }
        }
            break;
        case 1:
        {
            NSDictionary *dict = [_cityArray objectAtIndex:row];
            NSString *code = MySetValue(dict, @"cityName");
            
            NSDictionary * dics = [_provienceArray objectAtIndex:ss];
            NSString * str = MySetValue(dics, @"provinceName");
     
            if (code.length > 0) {
//                _areaArray = [AREA selectAreaWithFArea:str withTarea:code withFarea:3];
//                [pickerView reloadComponent:2];
            }
        }
            break;
        case 2:{
            
        }
            break;
            
        default:
            break;
    }
}


- (void)selectOK:(id)sender{
    
    
    UIPickerView *pick = sender;
    NSInteger row = [pick selectedRowInComponent:0];
    NSDictionary *dict = [_provienceArray objectAtIndex:row];
    NSString *provinceName = MySetValue(dict, @"provinceName");//省名称
    NSString *provinceCode = MySetValue(dict, @"provinceCode");//省编码
    
    
    NSInteger row1 = [pick selectedRowInComponent:1];
    NSDictionary *dict1 = [_cityArray objectAtIndex:row1];
    NSString *cityName = MySetValue(dict1, @"cityName");//市名称
    NSString *citycode = MySetValue(dict1, @"cityCode");//市编码
    
//    NSInteger row2 = [pick selectedRowInComponent:2];
//    NSDictionary *dict2 = [_areaArray objectAtIndex:row2];
//    NSString *countyName = MySetValue(dict2, @"countyName");//县名称
//    NSString * countycode = MySetValue(dict2, @"countyCode");//县编码
    
    
    [dic setObject:provinceCode forKey:@"provincecode"];//省编码
    [dic setObject:provinceName forKey:@"provincename"]; //省名称
    [dic setObject:citycode forKey:@"citycode"];//市编码
    [dic setObject:cityName forKey:@"cityname"]; //市名称
//    [dic setObject:countycode forKey:@"countycode"];//县编码
//    [dic setObject:countyName forKey:@"countyname"]; //县名称
    
    NSString *provinceName1 = [NSString stringWithFormat:@"%@",provinceName];
    NSString *cityName1 = [NSString stringWithFormat:@"%@",cityName];
    
    NSString * btnName = nil;
    if ([provinceName1 isEqualToString:cityName1]) {
        btnName = [NSString stringWithFormat:@"%@",provinceName1];
    }else{
        btnName = [NSString stringWithFormat:@"%@-%@",provinceName1,cityName1];
    }

//    NSString * btnName = [NSString stringWithFormat:@"%@%@",provinceName,cityName];
    [_AreaBtn setTitle:btnName forState:UIControlStateNormal];
    UIImageView * image = (UIImageView*)[self.view viewWithTag:1000];
    image.transform = CGAffineTransformMakeRotation(0);
}

//  跳出的pickerview  显示列数
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}


//显示pickerview 每个视图的行数
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component

{
    switch (component) {
        case 0:
            return _provienceArray.count;
            break;
        case 1:
            return _cityArray.count;
            break;
//        case 2:
//            return _areaArray.count;
//            break;
        default:
            break;
    }
    return 0;
}

//显示每个pickerview  每行的具体内容

-(UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view

{
    
    
    UILabel *lable  = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
    lable.adjustsFontSizeToFitWidth = YES;
    lable.textAlignment = NSTextAlignmentCenter;
    
    switch (component) {
        case 0:{
            NSDictionary *dict = [_provienceArray objectAtIndex:row];
            lable.text = MySetValue(dict, @"provinceName");
        }
            
            break;
        case 1:{
            
            NSDictionary *dict = [_cityArray objectAtIndex:row];
            lable.text = MySetValue(dict, @"cityName");
        }
            
            break;
            
//        case 2:{
//            
////            NSDictionary *dict = [_areaArray objectAtIndex:row];
////            lable.text = MySetValue(dict, @"countyName");
//        }
//
//            break;
        default:
            break;
    }
    return lable;
}
 
 */

-(void)pressStoreBtn{
    
    __weak typeof(self)  weakSelf = self;
    weakSelf.store = YES;
    
    /*
    UITextField *unit = (UITextField *)[self.view viewWithTag:500];
    NSString *string = [NSString string];
    if ([_addresstype isEqualToString:@"1"])
    {
        string = @"您没有填写收车单位";
    } else {
        string = @"您没有填写发车单位";
    }

    
    if (unit.text.length <= 0) {
        
       [self createUIAlertController:string];

    }
     
     */
    
    UITextField *address = (UITextField *)[self.view viewWithTag:500];
    if (address.text.length <= 0) {
        
       [self createUIAlertController:@"您没有填写详细地址"];
        
    }
    
    UITextField *contact = (UITextField *)[self.view viewWithTag:501];
    if (contact.text.length <= 0) {
        
      [self createUIAlertController:@"您没有填写联系人"];
        
    }
    
    UITextField *phone = (UITextField *)[self.view viewWithTag:502];
    
    
    if (phone.text.length <= 0) {
        
       [self createUIAlertController:@"您没有填写联系电话"];
        
    }
    
    
    if (weakSelf.store) {
        
        
        NSDictionary *dictionary = [NSDictionary dictionary];
        
        if ([dic[@"provincecode"] length] <= 0) {
            dictionary = @{@"id":self.orderId,
                           @"provincecode":self.provinceCode,
                           @"provincename":self.provinceName,
                           @"citycode":self.cityCode,
                           @"cityname":self.cityName,
                           @"countycode":self.countyCode,
                           @"countyname":self.countyName,
//                           @"unitname":unit.text,
                           @"address":address.text,
                           @"contact":contact.text,
                           @"phone":phone.text,
                           @"addresstype":self.addresstype,
                           @"comment":@""};

        } else {
        
            
            dictionary = @{@"id":self.orderId,
                           @"provincecode":dic[@"provincecode"],
                           @"provincename":dic[@"provincename"],
                           @"citycode":dic[@"citycode"],
                           @"cityname":dic[@"cityname"],
                           @"countycode":dic[@"countycode"],
                           @"countyname":dic[@"countyname"],
//                           @"unitname":unit.text,
                           @"address":address.text,
                           @"contact":contact.text,
                           @"phone":phone.text,
                           @"addresstype":self.addresstype,
                           @"comment":@""};
                }
        
        
        [Com afPostRequestWithUrlString:userAddress_update parms:dictionary finishedBlock:^(id responseObj) {
            
            NSMutableDictionary * dicObj =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];
            if (dicObj[@"success"]) {
                [WKProgressHUD popMessage:@"保存成功" inView:self.view duration:1.5 animated:YES];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    [hud dismiss:YES];
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }else{
                [self createUIAlertController:dicObj[@"message"]];
            }
            
        } failedBlock:^(NSString *errorMsg) {
            
        }];
        
    }
}

-(void)createUIAlertController:(NSString*)title

{
    
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}



-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor *)color{
    
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    // 名字的H
    CGFloat nameH = size.height;
    // 名字的W
    CGFloat nameW = size.width;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font)
    return label;
}

-(UIButton*)createBtn:(NSString*)title andTag:(int)tag{
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:BlackColor forState:UIControlStateNormal];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btn.tag = tag;
    btn.layer.cornerRadius = 5;
    
    
    return btn;
}
-(void)textFieldWithText:(UITextField *)textField{
    //500 501 502 503
    UITextField * field = (UITextField*)textField;
  
    
    switch (textField.tag) {
        case 499://发车单位/收车单位
//            [dic setObject:field.text forKey:@"unitname"]; //送达单位、收车单位
            break;
        case 500: //详细地址
            [dic setObject:field.text forKey:@"address"];
            break;
        case 501://联系人
            [dic setObject:field.text forKey:@"contact"];
            break;
        case 502://联系电话
            [dic setObject:field.text forKey:@"phone"];
            break;
        default:
            break;
    }
    
    /*
    //发车单位
    if (textField.tag == 500) {
        
        if (textField.text.length > 20) {
            textField.text = [textField.text substringToIndex:20];
            
            [self createUIAlertController:@"单位输入不能超过20个字符"];
            
        }
        
    }
    
    //发车地址
    if (textField.tag == 501) {
        if (textField.text.length > 30) {
            textField.text = [textField.text substringToIndex:30];
            
            [self createUIAlertController:@"详细地址输入不能超过30个字符"];
            
        }
    }
     */
    
    //联系人
    if (textField.tag == 501) {
        
        
        if (textField.text.length > 10) {
            
            [self createUIAlertController:@"联系人输入不能超过10个字符"];
            textField.text = [textField.text substringToIndex:10];
            
            
        }
    }

}


-(UITextField*)createField:(NSString*)placeholder andTag:(int)tag andFont:(double)font{
    
    UITextField * field =[[UITextField alloc]init];
    field.frame = CGRectMake(0, 0, Main_Width-31, 50);
    field.delegate = self;
    field.userInteractionEnabled = YES;
    field.textAlignment = NSTextAlignmentLeft;
    field.tag = tag;
    field.text = placeholder;
    field.font = Font(font);
//    [field setFont:[UIFont fontWithName:@"STHeitiSC" size:font]];
    [field addTarget:self action:@selector(textFieldWithText:) forControlEvents:UIControlEventEditingChanged];
    field.textColor = littleBlackColor;
    
    return field;
    
}


//触摸方法

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    
    for (UITextField * field in self.view.subviews) {
        
        [field resignFirstResponder];
        
    }
    
}


//点击return按钮 键盘隐藏 这是协议中的方法

-(BOOL)textFieldShouldReturn:(UITextField *)textField

{
    //键盘隐藏 也就是让键盘取消第一响应者身份
    
    return [textField resignFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
