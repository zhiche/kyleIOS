//
//  ChoiceCarTypeVC.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoiceCarTypeVC : UIViewController


/*
 str  车系＋车型
 str1 类别（SUV 或者商务）
 str2 长宽高
 str3 车系logo
 
 */
@property (nonatomic,copy) void (^callBack)(NSString *str,NSString *str1,NSString *str2,NSString *str3);


/*
 str  车系ID
 str1 车型ID
 str2 车系名称
 str3 品牌名称
 
 */

@property (nonatomic,copy) void (^callBackId)(NSString *str,NSString *str1,NSString *str2,NSString *str3);

@end
