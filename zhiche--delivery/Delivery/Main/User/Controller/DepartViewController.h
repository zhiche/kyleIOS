//
//  DepartViewController.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DepartViewController : UIViewController

@property (nonatomic,copy) NSString *topTitle;
@property (nonatomic,copy) NSString *cancelBtn;

@end
