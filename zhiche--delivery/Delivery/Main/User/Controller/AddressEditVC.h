//
//  AddressEditVC.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/20.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface AddressEditVC :  NavViewController  <UITextFieldDelegate,UIActionSheetDelegate,UIAlertViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIScrollViewDelegate>

@property (nonatomic,strong) NSString * addresstype;  //0:发货地址，1:收货地址
@property (nonatomic,copy) NSString *orderId;
@property (nonatomic,copy) NSString *isDefault;
@property (nonatomic,copy) NSString *comment;

@property (nonatomic,copy) NSString *zoneName;
//@property (nonatomic,copy) NSString *unitName;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,copy) NSString *contact;
@property (nonatomic,copy) NSString *phone;
@property (nonatomic,copy) NSString *provinceCode;
@property (nonatomic,copy) NSString *provinceName;
@property (nonatomic,copy) NSString *cityCode;
@property (nonatomic,copy) NSString *cityName;
@property (nonatomic,copy) NSString *countyCode;
@property (nonatomic,copy) NSString *countyName;


@end
