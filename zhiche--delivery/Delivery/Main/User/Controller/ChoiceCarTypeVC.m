//
//  ChoiceCarTypeVC.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/12.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ChoiceCarTypeVC.h"
#import "TopBackView.h"
#import "RootViewController.h"
#import "Common.h"


@interface ChoiceCarTypeVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *leftTableView;
@property (nonatomic,strong) UITableView *rightTableView;

@property (nonatomic,strong) NSMutableArray *leftDataArray;
@property (nonatomic,strong) NSMutableArray *rightDataArray;

@property (nonatomic,copy) NSString *carBrandString;
@property (nonatomic,copy) NSString *carBrandID;
@property (nonatomic,copy) NSString *carTypeID;
@property (nonatomic,copy) NSString *careTypeString;
@property (nonatomic,copy) NSString *logoString;


@end

@implementation ChoiceCarTypeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.leftDataArray = [NSMutableArray array];
    self.rightDataArray = [NSMutableArray array];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;

    self.view.backgroundColor = WhiteColor;
    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"车型选择"];
    
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    topBackView.rightButton.hidden = YES;
    
    [self.view addSubview:topBackView];
    
    [self initDataSource];
    
    [self initTableView];
}


-(void)initDataSource
{


    [Common requestWithUrlString:brand_list contentType:application_json finished:^(id responseObj) {
        
        
        self.leftDataArray = [NSMutableArray arrayWithArray:responseObj[@"data"]];
        if (self.leftDataArray.count > 0) {
            [self.leftTableView reloadData];
            [self initRightDataWith:self.leftDataArray[0][@"id"]];
        }

    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
    
}


-(void)initRightDataWith:(NSString *)string
{
    
    
    NSString * url = [NSString stringWithFormat:@"%@?brandid=%@",vehicle_list,string];

    [Common requestWithUrlString:url contentType:application_json finished:^(id responseObj) {
        if (self.rightDataArray.count > 0) {
            [self.rightDataArray removeAllObjects];
        }
        self.rightDataArray = [NSMutableArray arrayWithArray:responseObj[@"data"]];
        
        if (self.rightDataArray.count > 0) {
            [self.rightTableView reloadData];
        }
        
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
    

}

-(void)initTableView
{
    
    self.leftTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, screenWidth/2.0 - 16, screenHeight - 64) style:UITableViewStylePlain];
    self.leftTableView.delegate = self;
    self.leftTableView.dataSource = self;
    self.leftTableView.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:self.leftTableView];
    
    UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.leftTableView.frame) + 16, 64, 0.5,  screenHeight - 64)];
    imageV.image = [UIImage imageNamed:@"project_shaixuan_left_line1"];
    
    [self.view addSubview:imageV];
    
    
    self.rightTableView = [[UITableView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imageV.frame), 64, screenWidth/2.0 - 16, screenHeight - 64) style:UITableViewStylePlain];
    self.rightTableView.delegate = self;
    self.rightTableView.dataSource = self;
    self.rightTableView.tableFooterView = [[UIView alloc]init];

    [self.view addSubview:self.rightTableView];
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView == self.leftTableView) {
    
        return (self.leftDataArray.count > 0) ? self.leftDataArray.count : 0;
    } else {
        return  (self.rightDataArray.count > 0) ? self.rightDataArray.count : 0;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.leftTableView) {
        
        NSString *cellString = @"cartypeString";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellString];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellString];
        }
        
        if (indexPath.row == 0) {
            cell.selected = YES;
        }
        
        if (self.leftDataArray.count > 0) {
            cell.textLabel.text = self.leftDataArray[indexPath.row][@"brandName"];
 
        }
        cell.textLabel.font = Font(14);
        
        //默认选中第一行
        if (self.leftDataArray.count > 0) {
            
            if (indexPath.row == 0) {
                
                [self.leftTableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
                [self tableView:self.leftTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];

            }
            
                  }
        
       
        
        return cell;
    } else {
    
    
    NSString *cellString = @"cartypeString1";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellString];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellString];
    }
    
        if (self.rightDataArray.count > 0) {
            cell.textLabel.text = self.rightDataArray[indexPath.row][@"vehicleName"];

            
            
        }
        cell.textLabel.font = Font(14);

        //默认选中第一行
//        [self tableView:self.rightTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    
    return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.leftTableView) {
        
        if (self.leftDataArray.count > 0) {
            
            self.carBrandString = self.leftDataArray[indexPath.row][@"brandName"];//车系
            self.logoString = self.leftDataArray[indexPath.row][@"brandLogo"];//车系logo
            self.carBrandID = self.leftDataArray[indexPath.row][@"id"];//车系id
            
            [self initRightDataWith:self.leftDataArray[indexPath.row][@"id"]];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //Update UI in UI thread here
            [self.rightTableView reloadData];
            
        });
        
    } else {
        
        self.careTypeString = self.rightDataArray[indexPath.row][@"vehicleName"];//车型
        NSString *brandString = [self.carBrandString stringByAppendingString:self.careTypeString];//车系＋车型
        
        self.carTypeID = self.rightDataArray[indexPath.row][@"id"];//车型id
        
        //延迟执行
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if (self.callBack) {
                
                
                /*
                 str  车系＋车型
                 str1 类别（SUV 或者商务）
                 str2 长宽高
                 str3 车系logo
                 
                 */
                self.callBack(brandString,self.rightDataArray[indexPath.row][@"vehicleClass"],self.carBrandID,self.carTypeID);
            
            }
            
            if (self.carTypeID) {
                /*
                 str  车系ID
                 str1 车型ID
                 str2 车系名称
                 str3 品牌名称
                 
                 */
                
                self.callBackId(self.carBrandID,self.carTypeID,self.carBrandString,self.careTypeString);

            }
            
            [self.navigationController popViewControllerAnimated:YES];

            

        });
        
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}




-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
