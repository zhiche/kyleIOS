//
//  MessageViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/25.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "MessageViewController.h"
#import "TopBackView.h"
#import "RootViewController.h"
#import "MessageCell.h"
#import "Common.h"


@interface MessageViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic)int pageNo;
//@property (nonatomic) int pageSize;
@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic) NSInteger integer;//翻页参数

@property (nonatomic,strong) NSMutableArray *dataArray;//网络数据

@end

@implementation MessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _integer = 1;
    
    self.pageNo = 1;
    self.dataArray = [NSMutableArray array];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;

    TopBackView *topBackView= [[TopBackView alloc]initViewWithFrame:CGRectMake(0, 0, screenWidth, 64) andTitle:@"消息中心"];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = WhiteColor;
    
    [self initDataSource];
    
    [self initSubViews];
}



-(void)initDataSource
{
    NSString *urlString = [NSString stringWithFormat:@"%@msg/list?pageNo=%d&pageSize=10",Main_interface,self.pageNo];
    
    [Common requestWithUrlString:urlString contentType:application_json finished:^(id responseObj) {
        NSLog(@"%@",responseObj);
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
    
    

}

-(void)initSubViews
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64 ) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    return self.dataArray.count;
    return 5;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   NSString *message = @"messageCell";
    
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:message];
    if (!cell) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"MessageCell" owner:nil options:nil].lastObject;
    }
    
    
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 66;
}



-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
    
    
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
