//
//  AppDelegate.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

static NSString *appKey = @"6f4106828ac68a3bb79d7324";
static NSString *channel = @"App Store";
static BOOL isProduction = TRUE;


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

-(void)reachabilityChanged:(NSNotification *)note;
-(void)updateInterfaceWithReachability:(Reachability *)curReach;



@end

