//
//  RLNetworkHelper.h
//  test internet
//
//  Created by 王亚陆 on 15/7/13.
//  Copyright (c) 2015年 王亚陆. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
@interface RLNetworkHelper : NSObject
/**
 * 网络是否连通。
 *
 * @return 网络状态枚举值
 * @retval YES 网络通畅
 * @retval NO 网络不通
 */
+ (BOOL)isConnectedToNetwork;

/**
 * 检查网络状态。
 *
 *  //Apple NetworkStatus Constant Names.
 *  NotReachable     = kNotReachable, // 不可到达
 *  ReachableViaWiFi = kReachableViaWiFi, //通过WiFi可到达
 *  ReachableViaWWAN = kReachableViaWWAN //通过无线广域网可到达(WWAN，即Wireless Wide Area Network，无线广域网。)，就是3G或2G
 *
 * @return 网络状态枚举值
 * @retval NotReachable 无网络
 * @retval ReachableViaWiFi WIFI网络
 * @retval ReachableViaWWAN 3G/2G网络
 */
+ (NetworkStatus)checkNetworkStatus;

@end
